<VillageServer>


function n206_cleric_jake_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,117) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_117_love_money.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9463) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc01_9463_orb_to_find.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n206_cleric_jake_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,117) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_117_love_money.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9463) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc01_9463_orb_to_find.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
