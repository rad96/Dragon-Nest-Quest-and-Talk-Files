<VillageServer>


function n1405_guild_board_new_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "stage_1" then
		if api_quest_UserHasQuest(userObjID,3081) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3081_n402_worship_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3082) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3082_n402_worship_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3083) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3083_n402_worship_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3084) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3084_n402_worship_hard.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_1", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_2" then
		if api_quest_UserHasQuest(userObjID,3091) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3091_n403_catacomb_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3092) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3092_n403_catacomb_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3093) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3093_n403_catacomb_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3094) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3094_n403_catacomb_hard.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_2", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_3" then
		if api_quest_UserHasQuest(userObjID,3101) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3101_n400_fairy_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3102) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3102_n400_fairy_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3103) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3103_n400_fairy_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3104) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3104_n400_fairy_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3105) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3105_n400_fairy_boss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_3", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_4" then
		if api_quest_UserHasQuest(userObjID,3111) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3111_n401_hall_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3112) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3112_n401_hall_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3113) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3113_n401_hall_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3114) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3114_n401_hall_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3115) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3115_n401_hall_boss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_4", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1405_guild_board_new_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "stage_1" then
		if api_quest_UserHasQuest( pRoom, userObjID,3081) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3081_n402_worship_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3082) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3082_n402_worship_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3083) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3083_n402_worship_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3084) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3084_n402_worship_hard.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_1", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_2" then
		if api_quest_UserHasQuest( pRoom, userObjID,3091) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3091_n403_catacomb_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3092) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3092_n403_catacomb_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3093) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3093_n403_catacomb_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3094) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3094_n403_catacomb_hard.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_2", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_3" then
		if api_quest_UserHasQuest( pRoom, userObjID,3101) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3101_n400_fairy_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3102) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3102_n400_fairy_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3103) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3103_n400_fairy_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3104) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3104_n400_fairy_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3105) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3105_n400_fairy_boss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_3", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_4" then
		if api_quest_UserHasQuest( pRoom, userObjID,3111) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3111_n401_hall_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3112) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3112_n401_hall_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3113) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3113_n401_hall_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3114) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3114_n401_hall_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3115) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3115_n401_hall_boss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_4", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
