<VillageServer>
function n286_treasure_box_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,554) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_554_pirate_treasure3.xml");
		else				
		if api_quest_UserHasQuest(userObjID,553) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_553_pirate_treasure2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,552) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_552_pirate_treasure1.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</VillageServer>

<GameServer>
function n286_treasure_box_OnTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(pRoom, userObjID,554) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq11_554_pirate_treasure3.xml");
		else				
		if api_quest_UserHasQuest(pRoom, userObjID,553) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq11_553_pirate_treasure2.xml");
		else				
		if api_quest_UserHasQuest(pRoom, userObjID,552) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq11_552_pirate_treasure1.xml");
		else				
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</GameServer>
