<VillageServer>


function n055_elite_soldier_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,207) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_207_scattered_components.xml");
		else				
		if api_quest_UserHasQuest(userObjID,218) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_218_awakened_guardian.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9473) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc03_9473_wreckage_of_the_ship.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9477) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc03_9477_cleanup.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9478) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc04_9478_castle_of_saintheaven.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9590) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc04_9590_disappointing_palace_entrance.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n055_elite_soldier_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,207) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_207_scattered_components.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,218) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_218_awakened_guardian.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9473) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc03_9473_wreckage_of_the_ship.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9477) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc03_9477_cleanup.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9478) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc04_9478_castle_of_saintheaven.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9590) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc04_9590_disappointing_palace_entrance.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
