<VillageServer>


function n1775_book_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9562) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc16_9562_elf_and_knight.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9563) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc16_9563_tears_of_dragon.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9565) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc16_9565_the_man_who_changed_fate.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9566) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc16_9566_twisted_fate.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1775_book_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9562) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc16_9562_elf_and_knight.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9563) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc16_9563_tears_of_dragon.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9565) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc16_9565_the_man_who_changed_fate.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9566) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc16_9566_twisted_fate.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
