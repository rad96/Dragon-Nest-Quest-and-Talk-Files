<VillageServer>


function n724_karahan_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,732) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_732_medicines.xml");
		else				
		if api_quest_UserHasQuest(userObjID,731) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_731_can_not_understand.xml");
		else				
		if api_quest_UserHasQuest(userObjID,713) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_713_across_mind.xml");
		else				
		if api_quest_UserHasQuest(userObjID,712) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_712_karahan_recipe.xml");
		else				
		if api_quest_UserHasQuest(userObjID,711) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_711_disease.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9622) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9622_epidemic.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9623) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9623_sorceress_of_the_question.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9624) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9624_another_power.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9625) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9625_complex_heart.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9627) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9627_incorrect_convince.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n724_karahan_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,732) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_732_medicines.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,731) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_731_can_not_understand.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,713) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_713_across_mind.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,712) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_712_karahan_recipe.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,711) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_711_disease.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9622) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9622_epidemic.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9623) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9623_sorceress_of_the_question.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9624) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9624_another_power.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9625) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9625_complex_heart.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9627) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9627_incorrect_convince.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
