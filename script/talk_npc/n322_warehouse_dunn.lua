<VillageServer>


function n322_warehouse_dunn_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "warehouse_001" then
		if api_guild_HasGuildWare(userObjID) == 1 then				
		api_ui_OpenGuildWareHouse (userObjID);
		else				
		api_ui_OpenWareHouse(userObjID);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n322_warehouse_dunn_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "warehouse_001" then
		if api_guild_HasGuildWare( pRoom, userObjID) == 1 then				
		api_ui_OpenGuildWareHouse ( pRoom, userObjID);
		else				
		api_ui_OpenWareHouse( pRoom, userObjID);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
