<VillageServer>


function n1877_gold_mad_bard_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4701) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc17_4701_13_alpaca1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4722) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc17_4722_memories_of_mirage4.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1877_gold_mad_bard_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4701) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc17_4701_13_alpaca1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4722) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc17_4722_memories_of_mirage4.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
