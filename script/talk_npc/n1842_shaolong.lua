<VillageServer>


function n1842_shaolong_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9571) > -1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "prologue_makina01", npc_talk_target);
		end

	elseif npc_talk_index == "next" then
		api_quest_AddQuest(userObjID,9571, 2);
		api_quest_SetQuestStep(userObjID, 9571, 1);
		api_quest_SetJournalStep(userObjID, 9571, 1);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1842_shaolong_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9571) > -1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "prologue_makina01", npc_talk_target);
		end

	elseif npc_talk_index == "next" then
		api_quest_AddQuest( pRoom, userObjID,9571, 2);
		api_quest_SetQuestStep( pRoom, userObjID, 9571, 1);
		api_quest_SetJournalStep( pRoom, userObjID, 9571, 1);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
