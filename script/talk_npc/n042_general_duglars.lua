<VillageServer>


function n042_general_duglars_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_GetQuestStep(userObjID, 408) == 1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 6801) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_talk" then
		if api_quest_GetQuestStep(userObjID, 408) == 1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 6801) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_GetQuestStep(userObjID, 408) == 1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 6801) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( userObjID, 42 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( userObjID, 42 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( userObjID, 42 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( userObjID, 42 );
		api_npc_NextTalk(userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_quest_GetQuestStep(userObjID, 408) == 1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 6801) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_gift" then
		if api_quest_GetQuestStep(userObjID, 408) == 1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 6801) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 42 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n042_general_duglars_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_GetQuestStep( pRoom, userObjID, 408) == 1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 6801) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_talk" then
		if api_quest_GetQuestStep( pRoom, userObjID, 408) == 1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 6801) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_GetQuestStep( pRoom, userObjID, 408) == 1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 6801) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 42 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 42 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 42 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( pRoom,  userObjID, 42 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_quest_GetQuestStep( pRoom, userObjID, 408) == 1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 6801) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_gift" then
		if api_quest_GetQuestStep( pRoom, userObjID, 408) == 1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 6801) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 42 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
