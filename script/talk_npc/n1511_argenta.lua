<VillageServer>


function n1511_argenta_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9412) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9412_meeting.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9413) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9413_imperfect_dragon.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9422) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9422_desitny.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9423) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9423_madness.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9424) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9424_resentment.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9425) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9425_power.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1511_argenta_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9412) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9412_meeting.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9413) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9413_imperfect_dragon.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9422) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9422_desitny.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9423) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9423_madness.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9424) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9424_resentment.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9425) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9425_power.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
