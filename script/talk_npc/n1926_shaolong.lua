<VillageServer>


function n1926_shaolong_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9655) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9655_instead_say_goodbye.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9656) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9656_again_st_haven.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1926_shaolong_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9655) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9655_instead_say_goodbye.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9656) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9656_again_st_haven.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
