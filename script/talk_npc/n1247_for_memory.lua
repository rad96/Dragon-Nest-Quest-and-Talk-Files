<VillageServer>


function n1247_for_memory_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4425) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq34_4425_distorted_memory_1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4428) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq34_4428_distorted_memory_2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4431) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq34_4431_recipe_of_love.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1247_for_memory_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4425) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq34_4425_distorted_memory_1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4428) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq34_4428_distorted_memory_2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4431) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq34_4431_recipe_of_love.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
