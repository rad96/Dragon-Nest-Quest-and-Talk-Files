<VillageServer>


function n408_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "stage_17" then
		if api_quest_UserHasQuest(userObjID,3051) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3051_n408_death_forest_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3052) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3052_n408_death_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3053) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3053_n408_death_forest_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3054) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3054_n408_death_forest_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3059) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3059_n408_death_forest_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_17", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_24" then
		if api_quest_UserHasQuest(userObjID,3131) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3131_n408_basin_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3132) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3132_n408_basin_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3133) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3133_n408_basin_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3134) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3134_n408_basin_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3135) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3135_n408_basin_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3140) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3140_n408_basin_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3139) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3139_n408_basin_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_24", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3051) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3051_n408_death_forest_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3052) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3052_n408_death_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3053) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3053_n408_death_forest_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3054) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3054_n408_death_forest_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3059) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3059_n408_death_forest_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3131) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3131_n408_basin_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3132) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3132_n408_basin_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3133) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3133_n408_basin_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3134) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3134_n408_basin_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3135) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3135_n408_basin_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3140) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3140_n408_basin_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3139) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3139_n408_basin_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_17" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_24" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n408_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "stage_17" then
		if api_quest_UserHasQuest( pRoom, userObjID,3051) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3051_n408_death_forest_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3052) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3052_n408_death_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3053) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3053_n408_death_forest_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3054) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3054_n408_death_forest_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3059) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3059_n408_death_forest_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_17", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_24" then
		if api_quest_UserHasQuest( pRoom, userObjID,3131) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3131_n408_basin_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3132) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3132_n408_basin_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3133) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3133_n408_basin_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3134) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3134_n408_basin_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3135) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3135_n408_basin_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3140) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3140_n408_basin_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3139) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3139_n408_basin_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_24", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3051) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3051_n408_death_forest_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3052) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3052_n408_death_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3053) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3053_n408_death_forest_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3054) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3054_n408_death_forest_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3059) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3059_n408_death_forest_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3131) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3131_n408_basin_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3132) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3132_n408_basin_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3133) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3133_n408_basin_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3134) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3134_n408_basin_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3135) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3135_n408_basin_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3140) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3140_n408_basin_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3139) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3139_n408_basin_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_17" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_24" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
