<VillageServer>


function n259_argenta_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,401) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_401_castle_sainthaven.xml");
		else				
		if api_quest_UserHasQuest(userObjID,432) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_432_here_sainthaven.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9300) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_9300_arrive_sainthaven.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9478) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc04_9478_castle_of_saintheaven.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9590) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc04_9590_disappointing_palace_entrance.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n259_argenta_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,401) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_401_castle_sainthaven.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,432) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_432_here_sainthaven.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9300) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_9300_arrive_sainthaven.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9478) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc04_9478_castle_of_saintheaven.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9590) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc04_9590_disappointing_palace_entrance.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
