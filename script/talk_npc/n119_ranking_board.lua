<VillageServer>


function n119_ranking_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "no_ranking", npc_talk_target);
	elseif npc_talk_index == "ranking" then
		api_ui_OpenMonsterBattleRankBoard( userObjID, 12702 );
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n119_ranking_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "no_ranking", npc_talk_target);
	elseif npc_talk_index == "ranking" then
		api_ui_OpenMonsterBattleRankBoard( pRoom,  userObjID, 12702 );
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
