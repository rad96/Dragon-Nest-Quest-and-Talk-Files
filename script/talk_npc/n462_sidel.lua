<VillageServer>


function n462_sidel_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,702) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_702_witch_of_swamp.xml");
		else				
		if api_quest_UserHasQuest(userObjID,703) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_703_wild_cat_girl.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9331) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9331_old_fellowship.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9332) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9332_friends_likes.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9503) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9503_meet_the_karakule.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9616) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9616_witch_karakule.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9617) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9617_strange_rumors.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n462_sidel_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,702) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_702_witch_of_swamp.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,703) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_703_wild_cat_girl.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9331) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9331_old_fellowship.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9332) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9332_friends_likes.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9503) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9503_meet_the_karakule.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9616) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9616_witch_karakule.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9617) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9617_strange_rumors.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
