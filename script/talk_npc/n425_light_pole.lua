<VillageServer>


function n425_light_pole_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,636) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_636_unallowable_love.xml");
		else				
		if api_quest_UserHasQuest(userObjID,643) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_643_new_clue.xml");
		else				
		if api_quest_UserHasQuest(userObjID,657) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_657_this_witch.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n425_light_pole_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,636) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_636_unallowable_love.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,643) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_643_new_clue.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,657) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_657_this_witch.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
