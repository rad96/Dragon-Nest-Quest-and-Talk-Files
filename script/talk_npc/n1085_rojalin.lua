<VillageServer>


function n1085_rojalin_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9288) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9288_forest_that_has_lost_the_origin.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9295) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9295_imperfect_information.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9278) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9278_dangerous_advice.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1085_rojalin_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9288) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9288_forest_that_has_lost_the_origin.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9295) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9295_imperfect_information.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9278) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9278_dangerous_advice.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
