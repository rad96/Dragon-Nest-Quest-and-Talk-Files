<VillageServer>
function n067_lenas_hankie_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,452) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_452_meet_lena.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</VillageServer>

<GameServer>
function n067_lenas_hankie_OnTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(pRoom, userObjID,452) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq11_452_meet_lena.xml");
		else				
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	else
		api_npc_NextTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</GameServer>
