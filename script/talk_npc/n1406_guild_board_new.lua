<VillageServer>


function n1406_guild_board_new_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "stage_1" then
		if api_quest_UserHasQuest(userObjID,3121) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3121_n405_unreturn_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3122) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3122_n405_unreturn_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3123) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3123_n405_unreturn_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3124) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3124_n405_unreturn_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3125) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3125_n405_unreturn_boss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_1", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_2" then
		if api_quest_UserHasQuest(userObjID,3131) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3131_n408_basin_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3132) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3132_n408_basin_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3133) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3133_n408_basin_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3134) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3134_n408_basin_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3135) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3135_n408_basin_boss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_2", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_3" then
		if api_quest_UserHasQuest(userObjID,3141) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3141_n406_center_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3142) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3142_n406_center_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3143) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3143_n406_center_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3144) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3144_n406_center_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3145) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3145_n406_center_boss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_3", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_4" then
		if api_quest_UserHasQuest(userObjID,3151) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3151_n407_pray_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3152) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3152_n407_pray_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3153) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3153_n407_pray_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3154) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3154_n407_pray_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3155) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3155_n407_pray_boss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_4", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1406_guild_board_new_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "stage_1" then
		if api_quest_UserHasQuest( pRoom, userObjID,3121) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3121_n405_unreturn_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3122) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3122_n405_unreturn_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3123) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3123_n405_unreturn_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3124) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3124_n405_unreturn_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3125) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3125_n405_unreturn_boss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_1", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_2" then
		if api_quest_UserHasQuest( pRoom, userObjID,3131) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3131_n408_basin_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3132) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3132_n408_basin_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3133) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3133_n408_basin_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3134) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3134_n408_basin_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3135) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3135_n408_basin_boss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_2", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_3" then
		if api_quest_UserHasQuest( pRoom, userObjID,3141) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3141_n406_center_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3142) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3142_n406_center_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3143) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3143_n406_center_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3144) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3144_n406_center_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3145) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3145_n406_center_boss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_3", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_4" then
		if api_quest_UserHasQuest( pRoom, userObjID,3151) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3151_n407_pray_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3152) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3152_n407_pray_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3153) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3153_n407_pray_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3154) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3154_n407_pray_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3155) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3155_n407_pray_boss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_4", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
