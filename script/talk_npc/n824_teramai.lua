<VillageServer>


function n824_teramai_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 8 then				
		if api_quest_UserHasQuest(userObjID,9531) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9531_who_is_a_liar.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9532) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9532_reason_for_the_loss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		end

		end

		else				
		if api_quest_UserHasQuest(userObjID,9211) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9211_awkward_encounter.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9212) > -1 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		else				
		if api_quest_UserHasQuest(userObjID,9364) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9364_reunions.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9365) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9365_pitiful_reality.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n824_teramai_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		if api_quest_UserHasQuest( pRoom, userObjID,9531) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9531_who_is_a_liar.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9532) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9532_reason_for_the_loss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		end

		end

		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9211) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9211_awkward_encounter.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9212) > -1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9364) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9364_reunions.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9365) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9365_pitiful_reality.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
