<VillageServer>


function n411_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
	elseif npc_talk_index == "stage_30" then
		if api_quest_UserHasQuest(userObjID,3181) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3181_n411_island_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3182) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3182_n411_island_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3183) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3183_n411_island_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3184) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3184_n411_island_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3185) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3185_n411_island_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3186) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3186_n411_island_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3187) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3187_n411_island_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3190) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3190_n411_island_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3189) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3189_n411_island_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_30", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_33" then
		if api_quest_UserHasQuest(userObjID,3261) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3261_n411_icenter_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3262) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3262_n411_icenter_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3263) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3263_n411_icenter_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3264) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3264_n411_icenter_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3265) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3265_n411_icenter_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3266) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3266_n411_icenter_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3267) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3267_n411_icenter_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3270) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3270_n411_icenter_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3269) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3269_n411_icenter_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_33", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_46" then
		if api_quest_UserHasQuest(userObjID,3291) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3291_n411_sunken_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3292) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3292_n411_sunken_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3293) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3293_n411_sunken_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3294) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3294_n411_sunken_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3295) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3295_n411_sunken_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3296) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3296_n411_sunken_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3297) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3297_n411_sunken_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3300) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3300_n411_sunken_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3299) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3299_n411_sunken_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_46", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_39" then
		if api_quest_UserHasQuest(userObjID,3361) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3361_n411_cave_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3362) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3362_n411_cave_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3363) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3363_n411_cave_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3364) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3364_n411_cave_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3365) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3365_n411_cave_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3366) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3366_n411_cave_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3367) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3367_n411_cave_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3370) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3370_n411_cave_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3369) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3369_n411_cave_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_39", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3181) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3181_n411_island_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3182) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3182_n411_island_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3183) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3183_n411_island_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3184) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3184_n411_island_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3185) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3185_n411_island_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3186) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3186_n411_island_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3187) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3187_n411_island_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3190) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3190_n411_island_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3189) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3189_n411_island_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3261) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3261_n411_icenter_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3262) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3262_n411_icenter_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3263) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3263_n411_icenter_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3264) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3264_n411_icenter_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3265) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3265_n411_icenter_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3266) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3266_n411_icenter_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3267) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3267_n411_icenter_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3270) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3270_n411_icenter_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3269) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3269_n411_icenter_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3291) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3291_n411_sunken_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3292) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3292_n411_sunken_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3293) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3293_n411_sunken_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3294) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3294_n411_sunken_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3295) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3295_n411_sunken_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3296) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3296_n411_sunken_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3297) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3297_n411_sunken_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3300) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3300_n411_sunken_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3299) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3299_n411_sunken_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3361) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3361_n411_cave_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3362) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3362_n411_cave_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3363) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3363_n411_cave_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3364) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3364_n411_cave_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3365) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3365_n411_cave_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3366) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3366_n411_cave_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3367) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3367_n411_cave_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3370) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3370_n411_cave_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3369) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3369_n411_cave_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_30" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_33" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_46" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n411_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
	elseif npc_talk_index == "stage_30" then
		if api_quest_UserHasQuest( pRoom, userObjID,3181) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3181_n411_island_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3182) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3182_n411_island_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3183) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3183_n411_island_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3184) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3184_n411_island_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3185) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3185_n411_island_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3186) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3186_n411_island_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3187) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3187_n411_island_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3190) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3190_n411_island_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3189) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3189_n411_island_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_30", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_33" then
		if api_quest_UserHasQuest( pRoom, userObjID,3261) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3261_n411_icenter_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3262) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3262_n411_icenter_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3263) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3263_n411_icenter_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3264) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3264_n411_icenter_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3265) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3265_n411_icenter_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3266) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3266_n411_icenter_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3267) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3267_n411_icenter_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3270) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3270_n411_icenter_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3269) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3269_n411_icenter_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_33", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_46" then
		if api_quest_UserHasQuest( pRoom, userObjID,3291) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3291_n411_sunken_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3292) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3292_n411_sunken_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3293) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3293_n411_sunken_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3294) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3294_n411_sunken_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3295) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3295_n411_sunken_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3296) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3296_n411_sunken_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3297) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3297_n411_sunken_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3300) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3300_n411_sunken_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3299) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3299_n411_sunken_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_46", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_39" then
		if api_quest_UserHasQuest( pRoom, userObjID,3361) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3361_n411_cave_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3362) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3362_n411_cave_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3363) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3363_n411_cave_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3364) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3364_n411_cave_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3365) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3365_n411_cave_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3366) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3366_n411_cave_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3367) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3367_n411_cave_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3370) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3370_n411_cave_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3369) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3369_n411_cave_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_39", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3181) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3181_n411_island_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3182) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3182_n411_island_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3183) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3183_n411_island_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3184) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3184_n411_island_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3185) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3185_n411_island_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3186) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3186_n411_island_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3187) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3187_n411_island_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3190) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3190_n411_island_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3189) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3189_n411_island_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3261) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3261_n411_icenter_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3262) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3262_n411_icenter_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3263) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3263_n411_icenter_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3264) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3264_n411_icenter_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3265) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3265_n411_icenter_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3266) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3266_n411_icenter_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3267) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3267_n411_icenter_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3270) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3270_n411_icenter_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3269) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3269_n411_icenter_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3291) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3291_n411_sunken_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3292) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3292_n411_sunken_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3293) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3293_n411_sunken_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3294) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3294_n411_sunken_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3295) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3295_n411_sunken_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3296) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3296_n411_sunken_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3297) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3297_n411_sunken_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3300) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3300_n411_sunken_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3299) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3299_n411_sunken_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3361) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3361_n411_cave_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3362) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3362_n411_cave_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3363) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3363_n411_cave_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3364) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3364_n411_cave_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3365) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3365_n411_cave_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3366) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3366_n411_cave_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3367) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3367_n411_cave_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3370) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3370_n411_cave_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3369) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3369_n411_cave_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_30" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_33" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_46" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
