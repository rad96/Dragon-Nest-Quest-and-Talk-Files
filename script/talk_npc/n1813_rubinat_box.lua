<VillageServer>


function n1813_rubinat_box_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9701) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc17_9701_go_mistland2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9702) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc17_9702_successor_of_protector.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_quest" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1813_rubinat_box_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9701) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc17_9701_go_mistland2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9702) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc17_9702_successor_of_protector.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_quest" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
