<VillageServer>


function n482_charty_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,709) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_709_rescue_natives.xml");
		else				
		if api_quest_UserHasQuest(userObjID,730) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_730_rescue_natives.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9512) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9512_monster_of_dream.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9620) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9620_aboriginal_sick.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n482_charty_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,709) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_709_rescue_natives.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,730) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_730_rescue_natives.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9512) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9512_monster_of_dream.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9620) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9620_aboriginal_sick.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
