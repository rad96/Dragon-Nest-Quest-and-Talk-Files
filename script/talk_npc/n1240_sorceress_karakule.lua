<VillageServer>


function n1240_sorceress_karakule_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9394) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9394_efforts_for_legend.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4432) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq34_4432_his_penitence.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1240_sorceress_karakule_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9394) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9394_efforts_for_legend.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4432) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq34_4432_his_penitence.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
