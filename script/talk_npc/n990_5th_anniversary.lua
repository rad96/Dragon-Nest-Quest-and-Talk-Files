<VillageServer>


function n990_5th_anniversary_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 2379) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 2378) == 1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","eq_2379_5th_anniversary.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2379) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","eq_2379_5th_anniversary.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2378) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","eq_2378_5th_anniversary.xml");
		else				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","eq_2378_5th_anniversary.xml");
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n990_5th_anniversary_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 2379) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 2378) == 1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","eq_2379_5th_anniversary.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2379) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","eq_2379_5th_anniversary.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2378) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","eq_2378_5th_anniversary.xml");
		else				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","eq_2378_5th_anniversary.xml");
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
