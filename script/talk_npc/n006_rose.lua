<VillageServer>
function n006_rose_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,17) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq017_black_knight_tracking.xml");
		else				
		if api_quest_UserHasQuest(userObjID,16) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq016_rescue_girl.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</VillageServer>

<GameServer>
function n006_rose_OnTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(pRoom, userObjID,17) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","mq017_black_knight_tracking.xml");
		else				
		if api_quest_UserHasQuest(pRoom, userObjID,16) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","mq016_rescue_girl.xml");
		else				
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</GameServer>
