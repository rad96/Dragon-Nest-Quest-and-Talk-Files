<VillageServer>


function n735_darklair_mocha_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4311) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4311_new_power_talisman.xml");
		else				
		if api_user_GetUserLevel(userObjID) >= 50 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

	elseif npc_talk_index == "shop_open_3" then
		api_ui_OpenShop(userObjID,28001,100);
	elseif npc_talk_index == "shop_open_4" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,29001,100);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,29002,100);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,29003,100);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,29004,100);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,29005,100);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,29006,100);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,29007,100);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,29008,100);
		else				
		api_ui_OpenShop(userObjID,29009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "starshop" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,52001,105);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,52002,105);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,52003,105);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,52004,105);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,52005,105);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,52006,105);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,52007,105);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,52008,105);
		else				
		api_ui_OpenShop(userObjID,52009,105);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back" then
		if api_quest_UserHasQuest(userObjID,4311) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4311_new_power_talisman.xml");
		else				
		if api_user_GetUserLevel(userObjID) >= 50 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back" then
		if api_quest_UserHasQuest(userObjID,4311) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4311_new_power_talisman.xml");
		else				
		if api_user_GetUserLevel(userObjID) >= 50 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n735_darklair_mocha_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4311) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4311_new_power_talisman.xml");
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 50 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

	elseif npc_talk_index == "shop_open_3" then
		api_ui_OpenShop( pRoom, userObjID,28001,100);
	elseif npc_talk_index == "shop_open_4" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,29001,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,29002,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,29003,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,29004,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,29005,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,29006,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,29007,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,29008,100);
		else				
		api_ui_OpenShop( pRoom, userObjID,29009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "starshop" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,52001,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,52002,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,52003,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,52004,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,52005,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,52006,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,52007,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,52008,105);
		else				
		api_ui_OpenShop( pRoom, userObjID,52009,105);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back" then
		if api_quest_UserHasQuest( pRoom, userObjID,4311) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4311_new_power_talisman.xml");
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 50 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back" then
		if api_quest_UserHasQuest( pRoom, userObjID,4311) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4311_new_power_talisman.xml");
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 50 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
