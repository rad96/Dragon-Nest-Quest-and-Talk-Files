<VillageServer>


function n1059_yuvenciel_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 2 then				
		if api_quest_UserHasQuest(userObjID,9272) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9272_night_of_conclave.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9273) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9273_seeking_the_teresia.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9274) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9274_disappeared_candidate.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9275) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9275_clue_that_she_left.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9278) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9278_dangerous_advice.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1059_yuvenciel_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		if api_quest_UserHasQuest( pRoom, userObjID,9272) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9272_night_of_conclave.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9273) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9273_seeking_the_teresia.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9274) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9274_disappeared_candidate.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9275) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9275_clue_that_she_left.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9278) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9278_dangerous_advice.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
