<VillageServer>


function n835_teramai_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 8 then				
		if api_quest_UserHasQuest(userObjID,9538) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9538_intended_ending.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		if api_quest_UserHasQuest(userObjID,9221) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9221_unpleased_successor.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9222) > -1 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		else				
		if api_quest_UserHasQuest(userObjID,9372) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9372_show_the_cloven_hoof.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n835_teramai_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		if api_quest_UserHasQuest( pRoom, userObjID,9538) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9538_intended_ending.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9221) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9221_unpleased_successor.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9222) > -1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9372) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9372_show_the_cloven_hoof.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
