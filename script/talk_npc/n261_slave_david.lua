<VillageServer>


function n261_slave_david_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,419) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_419_where_argenta.xml");
		else				
		if api_quest_UserHasQuest(userObjID,403) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_403_meet_argenta_again.xml");
		else				
		if api_quest_UserHasQuest(userObjID,418) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_418_curious_david.xml");
		else				
		if api_quest_UserHasQuest(userObjID,417) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_417_deserted_david.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9481) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc04_9481_encounter_legends.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9590) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc04_9590_disappointing_palace_entrance.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9606) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9606_people_left.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n261_slave_david_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,419) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_419_where_argenta.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,403) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_403_meet_argenta_again.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,418) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_418_curious_david.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,417) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_417_deserted_david.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9481) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc04_9481_encounter_legends.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9590) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc04_9590_disappointing_palace_entrance.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9606) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9606_people_left.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
