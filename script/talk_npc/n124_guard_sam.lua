<VillageServer>


function n124_guard_sam_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_009_please_save_my_girl.xml");
		else				
		if api_quest_UserHasQuest(userObjID,8) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_008_whereabouts_girl2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,19) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_019_whereabouts_girl.xml");
		else				
		if api_quest_UserHasQuest(userObjID,20) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_020_please_save_my_girl.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n124_guard_sam_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_009_please_save_my_girl.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,8) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_008_whereabouts_girl2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,19) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_019_whereabouts_girl.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,20) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_020_please_save_my_girl.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
