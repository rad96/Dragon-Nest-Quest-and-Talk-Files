<VillageServer>


function n156_cleric_jake_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,107) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_107_internal_enemy.xml");
		else				
		if api_quest_UserHasQuest(userObjID,102) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_102_evidence_of_pagan.xml");
		else				
		if api_quest_UserHasQuest(userObjID,122) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_122_comeback2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,112) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_112_doubt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,113) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_113_orb_of_vision1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,127) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_127_evidence_of_pagan.xml");
		else				
		if api_quest_UserHasQuest(userObjID,128) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_128_orb_of_vision1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,131) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_131_internal_enemy.xml");
		else				
		if api_quest_UserHasQuest(userObjID,136) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_136_comeback2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,137) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_137_doubt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,138) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_138_orb_of_vision1.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n156_cleric_jake_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,107) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_107_internal_enemy.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,102) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_102_evidence_of_pagan.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,122) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_122_comeback2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,112) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_112_doubt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,113) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_113_orb_of_vision1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,127) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_127_evidence_of_pagan.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,128) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_128_orb_of_vision1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,131) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_131_internal_enemy.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,136) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_136_comeback2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,137) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_137_doubt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,138) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_138_orb_of_vision1.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
