<VillageServer>


function n353_cataract_warp_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_GetQuestStep(userObjID, 374) == 1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq08_374_cataract_warp.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "warp" then
		api_ui_OpenWarpListDlg(userObjID);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n353_cataract_warp_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_GetQuestStep( pRoom, userObjID, 374) == 1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq08_374_cataract_warp.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "warp" then
		api_ui_OpenWarpListDlg( pRoom, userObjID);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
