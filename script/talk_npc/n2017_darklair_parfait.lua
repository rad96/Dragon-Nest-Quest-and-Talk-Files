<VillageServer>


function n2017_darklair_parfait_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserJobID(userObjID) == 80 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		api_user_SetSecondJobSkill( userObjID, 81 );
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "qu_06" then
		api_quest_ForceCompleteQuest(userObjID, 997, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 239, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 341, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 764, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 765, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 766, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 767, 0, 1, 1, 0);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n2017_darklair_parfait_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserJobID( pRoom, userObjID) == 80 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		api_user_SetSecondJobSkill( pRoom,  userObjID, 81 );
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "qu_06" then
		api_quest_ForceCompleteQuest( pRoom, userObjID, 997, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 239, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 341, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 764, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 765, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 766, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 767, 0, 1, 1, 0);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
