<VillageServer>


function n412_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
	elseif npc_talk_index == "stage_28" then
		if api_quest_UserHasQuest(userObjID,3191) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3191_n412_dominion_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3192) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3192_n412_dominion_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3193) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3193_n412_dominion_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3194) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3194_n412_dominion_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3195) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3195_n412_dominion_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3196) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3196_n412_dominion_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3197) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3197_n412_dominion_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3200) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3200_n412_dominion_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3199) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3199_n412_dominion_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_28", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_43" then
		if api_quest_UserHasQuest(userObjID,3221) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3221_n412_guard_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3222) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3222_n412_guard_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3223) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3223_n412_guard_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3224) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3224_n412_guard_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3225) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3225_n412_guard_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3226) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3226_n412_guard_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3227) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3227_n412_guard_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3230) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3230_n412_guard_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3229) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3229_n412_guard_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_43", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_36" then
		if api_quest_UserHasQuest(userObjID,3251) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3251_n412_ground_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3252) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3252_n412_ground_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3253) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3253_n412_ground_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3254) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3254_n412_ground_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3255) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3255_n412_ground_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3256) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3256_n412_ground_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3257) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3257_n412_ground_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3260) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3260_n412_ground_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3259) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3259_n412_ground_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_36", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_40" then
		if api_quest_UserHasQuest(userObjID,3301) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3301_n412_dtower_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3302) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3302_n412_dtower_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3303) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3303_n412_dtower_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3304) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3304_n412_dtower_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3305) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3305_n412_dtower_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3306) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3306_n412_dtower_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3307) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3307_n412_dtower_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3310) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3310_n412_dtower_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3309) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3309_n412_dtower_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_40", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3191) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3191_n412_dominion_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3192) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3192_n412_dominion_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3193) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3193_n412_dominion_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3194) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3194_n412_dominion_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3195) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3195_n412_dominion_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3196) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3196_n412_dominion_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3197) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3197_n412_dominion_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3200) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3200_n412_dominion_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3199) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3199_n412_dominion_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3221) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3221_n412_guard_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3222) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3222_n412_guard_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3223) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3223_n412_guard_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3224) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3224_n412_guard_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3225) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3225_n412_guard_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3226) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3226_n412_guard_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3227) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3227_n412_guard_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3230) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3230_n412_guard_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3229) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3229_n412_guard_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3251) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3251_n412_ground_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3252) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3252_n412_ground_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3253) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3253_n412_ground_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3254) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3254_n412_ground_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3255) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3255_n412_ground_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3256) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3256_n412_ground_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3257) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3257_n412_ground_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3260) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3260_n412_ground_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3259) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3259_n412_ground_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3301) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3301_n412_dtower_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3302) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3302_n412_dtower_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3303) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3303_n412_dtower_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3304) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3304_n412_dtower_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3305) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3305_n412_dtower_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3306) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3306_n412_dtower_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3307) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3307_n412_dtower_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3310) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3310_n412_dtower_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3309) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3309_n412_dtower_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_28" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_43" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_36" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n412_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
	elseif npc_talk_index == "stage_28" then
		if api_quest_UserHasQuest( pRoom, userObjID,3191) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3191_n412_dominion_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3192) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3192_n412_dominion_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3193) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3193_n412_dominion_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3194) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3194_n412_dominion_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3195) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3195_n412_dominion_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3196) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3196_n412_dominion_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3197) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3197_n412_dominion_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3200) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3200_n412_dominion_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3199) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3199_n412_dominion_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_28", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_43" then
		if api_quest_UserHasQuest( pRoom, userObjID,3221) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3221_n412_guard_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3222) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3222_n412_guard_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3223) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3223_n412_guard_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3224) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3224_n412_guard_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3225) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3225_n412_guard_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3226) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3226_n412_guard_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3227) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3227_n412_guard_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3230) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3230_n412_guard_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3229) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3229_n412_guard_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_43", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_36" then
		if api_quest_UserHasQuest( pRoom, userObjID,3251) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3251_n412_ground_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3252) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3252_n412_ground_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3253) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3253_n412_ground_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3254) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3254_n412_ground_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3255) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3255_n412_ground_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3256) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3256_n412_ground_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3257) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3257_n412_ground_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3260) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3260_n412_ground_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3259) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3259_n412_ground_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_36", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_40" then
		if api_quest_UserHasQuest( pRoom, userObjID,3301) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3301_n412_dtower_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3302) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3302_n412_dtower_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3303) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3303_n412_dtower_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3304) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3304_n412_dtower_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3305) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3305_n412_dtower_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3306) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3306_n412_dtower_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3307) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3307_n412_dtower_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3310) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3310_n412_dtower_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3309) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3309_n412_dtower_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_40", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3191) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3191_n412_dominion_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3192) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3192_n412_dominion_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3193) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3193_n412_dominion_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3194) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3194_n412_dominion_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3195) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3195_n412_dominion_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3196) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3196_n412_dominion_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3197) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3197_n412_dominion_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3200) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3200_n412_dominion_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3199) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3199_n412_dominion_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3221) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3221_n412_guard_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3222) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3222_n412_guard_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3223) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3223_n412_guard_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3224) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3224_n412_guard_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3225) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3225_n412_guard_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3226) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3226_n412_guard_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3227) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3227_n412_guard_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3230) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3230_n412_guard_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3229) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3229_n412_guard_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3251) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3251_n412_ground_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3252) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3252_n412_ground_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3253) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3253_n412_ground_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3254) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3254_n412_ground_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3255) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3255_n412_ground_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3256) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3256_n412_ground_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3257) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3257_n412_ground_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3260) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3260_n412_ground_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3259) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3259_n412_ground_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3301) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3301_n412_dtower_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3302) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3302_n412_dtower_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3303) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3303_n412_dtower_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3304) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3304_n412_dtower_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3305) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3305_n412_dtower_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3306) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3306_n412_dtower_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3307) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3307_n412_dtower_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3310) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3310_n412_dtower_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3309) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3309_n412_dtower_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_28" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_43" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_36" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
