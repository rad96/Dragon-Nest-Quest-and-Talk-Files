<VillageServer>
function n061_kidnaped_trader_dorin_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,458) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_458_amazing_memory.xml");
		else				
		if api_quest_UserHasQuest(userObjID,457) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_457_missing_dorin.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</VillageServer>

<GameServer>
function n061_kidnaped_trader_dorin_OnTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(pRoom, userObjID,458) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq11_458_amazing_memory.xml");
		else				
		if api_quest_UserHasQuest(pRoom, userObjID,457) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq11_457_missing_dorin.xml");
		else				
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</GameServer>
