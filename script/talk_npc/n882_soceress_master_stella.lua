<VillageServer>


function n882_soceress_master_stella_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4125) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4125_history_of_future.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4126) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4126_history_of_future.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4127) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4127_history_of_future.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4128) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4128_history_of_future.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4129) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4129_history_of_future.xml");
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n882_soceress_master_stella_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4125) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4125_history_of_future.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4126) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4126_history_of_future.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4127) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4127_history_of_future.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4128) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4128_history_of_future.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4129) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4129_history_of_future.xml");
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
