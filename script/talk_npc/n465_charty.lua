<VillageServer>


function n465_charty_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,703) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_703_wild_cat_girl.xml");
		else				
		if api_quest_UserHasQuest(userObjID,726) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_726_boy_and_girl.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9333) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9333_hatred_of_charty.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9615) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9615_home_lotusmarsh.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9617) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9617_strange_rumors.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9618) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9618_unexpected_visitor.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9619) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9619_trusty.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n465_charty_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,703) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_703_wild_cat_girl.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,726) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_726_boy_and_girl.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9333) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9333_hatred_of_charty.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9615) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9615_home_lotusmarsh.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9617) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9617_strange_rumors.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9618) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9618_unexpected_visitor.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9619) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9619_trusty.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
