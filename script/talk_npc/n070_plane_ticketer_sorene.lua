<VillageServer>


function n070_plane_ticketer_sorene_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "newbie", npc_talk_target);
		end

	elseif npc_talk_index == "ticketing" then
		if api_user_GetUserLevel(userObjID) >= 40 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "completecutscene" then
		api_user_ChangeMap(userObjID,8,1);
	elseif npc_talk_index == "back_quest" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "newbie", npc_talk_target);
		end

	elseif npc_talk_index == "back_go_prairie" then
		if api_user_GetUserLevel(userObjID) >= 40 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "move_1" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID,1,1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_go_manaridge" then
		if api_user_GetUserLevel(userObjID) >= 40 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "move_2" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID,5,1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_go_cataract" then
		if api_user_GetUserLevel(userObjID) >= 40 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "move_3" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_PlayCutScene(userObjID,npcObjID,95,true);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_go_lotusmarsh" then
		if api_user_GetUserLevel(userObjID) >= 40 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "move_4" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID,15,1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n070_plane_ticketer_sorene_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "newbie", npc_talk_target);
		end

	elseif npc_talk_index == "ticketing" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 40 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "completecutscene" then
		api_user_ChangeMap( pRoom, userObjID,8,1);
	elseif npc_talk_index == "back_quest" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "newbie", npc_talk_target);
		end

	elseif npc_talk_index == "back_go_prairie" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 40 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "move_1" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID,1,1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_go_manaridge" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 40 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "move_2" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID,5,1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_go_cataract" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 40 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "move_3" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_PlayCutScene( pRoom, userObjID,npcObjID,95,true);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_go_lotusmarsh" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 40 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "move_4" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID,15,1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
