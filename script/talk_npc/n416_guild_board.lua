<VillageServer>


function n416_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "check_mutant" then
		if api_quest_UserHasQuest(userObjID,3424) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3424_n416_mutant_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3429) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3429_n416_mutant_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3430) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3430_n416_mutant_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_mutant", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_meteor" then
		if api_quest_UserHasQuest(userObjID,3434) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3434_n416_meteor_borderland_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3439) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3439_n416_meteor_borderland_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3440) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3440_n416_meteor_borderland_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_meteor", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_demon" then
		if api_quest_UserHasQuest(userObjID,3444) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3444_n416_demon_shade_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3449) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3449_n416_demon_shade_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3450) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3450_n416_demon_shade_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_demon", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_m_center" then
		if api_quest_UserHasQuest(userObjID,3464) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3464_n416_meteor_center_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3469) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3469_n416_meteor_center_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3470) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3470_n416_meteor_center_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_m_center", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_encorched" then
		if api_quest_UserHasQuest(userObjID,3454) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3454_n416_encroached_ruin_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3459) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3459_n416_encroached_ruin_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3460) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3460_n416_encroached_ruin_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_encroached", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest(userObjID,3424) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3424_n416_mutant_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3429) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3429_n416_mutant_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3430) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3430_n416_mutant_hidden.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3434) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3434_n416_meteor_borderland_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3439) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3439_n416_meteor_borderland_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3440) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3440_n416_meteor_borderland_hidden.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3444) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3444_n416_demon_shade_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3449) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3449_n416_demon_shade_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3450) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3450_n416_demon_shade_hidden.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3454) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3454_n416_encroached_ruin_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3459) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3459_n416_encroached_ruin_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3460) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3460_n416_encroached_ruin_hidden.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3464) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3464_n416_meteor_center_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3469) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3469_n416_meteor_center_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3470) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3470_n416_meteor_center_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n416_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "check_mutant" then
		if api_quest_UserHasQuest( pRoom, userObjID,3424) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3424_n416_mutant_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3429) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3429_n416_mutant_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3430) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3430_n416_mutant_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_mutant", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_meteor" then
		if api_quest_UserHasQuest( pRoom, userObjID,3434) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3434_n416_meteor_borderland_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3439) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3439_n416_meteor_borderland_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3440) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3440_n416_meteor_borderland_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_meteor", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_demon" then
		if api_quest_UserHasQuest( pRoom, userObjID,3444) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3444_n416_demon_shade_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3449) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3449_n416_demon_shade_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3450) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3450_n416_demon_shade_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_demon", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_m_center" then
		if api_quest_UserHasQuest( pRoom, userObjID,3464) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3464_n416_meteor_center_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3469) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3469_n416_meteor_center_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3470) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3470_n416_meteor_center_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_m_center", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_encorched" then
		if api_quest_UserHasQuest( pRoom, userObjID,3454) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3454_n416_encroached_ruin_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3459) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3459_n416_encroached_ruin_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3460) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3460_n416_encroached_ruin_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_encroached", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest( pRoom, userObjID,3424) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3424_n416_mutant_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3429) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3429_n416_mutant_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3430) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3430_n416_mutant_hidden.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3434) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3434_n416_meteor_borderland_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3439) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3439_n416_meteor_borderland_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3440) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3440_n416_meteor_borderland_hidden.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3444) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3444_n416_demon_shade_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3449) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3449_n416_demon_shade_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3450) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3450_n416_demon_shade_hidden.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3454) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3454_n416_encroached_ruin_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3459) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3459_n416_encroached_ruin_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3460) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3460_n416_encroached_ruin_hidden.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3464) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3464_n416_meteor_center_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3469) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3469_n416_meteor_center_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3470) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3470_n416_meteor_center_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
