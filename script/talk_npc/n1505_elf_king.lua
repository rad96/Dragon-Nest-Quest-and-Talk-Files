<VillageServer>


function n1505_elf_king_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9410) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9410_the_outbreak_of_war.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9426) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9426_the_man_who_open_the_door.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9427) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9427_invasion.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9428) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9428_conflict.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9457) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9457_say_goodbye.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1505_elf_king_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9410) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9410_the_outbreak_of_war.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9426) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9426_the_man_who_open_the_door.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9427) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9427_invasion.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9428) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9428_conflict.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9457) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9457_say_goodbye.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
