<VillageServer>


function n367_cart_ticketer_irnene_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel(userObjID) >= 40 then				
		api_npc_NextTalk(userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 401) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 432) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_1" then
		if api_user_GetUserLevel(userObjID) >= 40 then				
		api_npc_NextTalk(userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 401) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 432) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "move_1" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID,1,1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_3" then
		if api_user_GetUserLevel(userObjID) >= 40 then				
		api_npc_NextTalk(userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 401) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 432) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "move_3" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID,8,1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_4" then
		if api_user_GetUserLevel(userObjID) >= 40 then				
		api_npc_NextTalk(userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 401) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 432) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "move_4" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID,11,1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_5" then
		if api_user_GetUserLevel(userObjID) >= 40 then				
		api_npc_NextTalk(userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 401) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 432) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "move_5" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID,15,1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n367_cart_ticketer_irnene_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 40 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 401) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 432) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_1" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 40 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 401) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 432) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "move_1" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID,1,1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_3" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 40 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 401) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 432) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "move_3" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID,8,1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_4" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 40 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 401) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 432) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "move_4" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID,11,1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "back_5" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 40 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 401) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 432) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "move_5" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID,15,1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
