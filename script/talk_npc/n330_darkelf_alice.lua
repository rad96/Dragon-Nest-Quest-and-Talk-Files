<VillageServer>


function n330_darkelf_alice_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,627) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_627_lost_crown.xml");
		else				
		if api_quest_UserHasQuest(userObjID,628) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_628_urgent_message.xml");
		else				
		if api_quest_UserHasQuest(userObjID,632) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_632_far_away_kingdom.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n330_darkelf_alice_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,627) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_627_lost_crown.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,628) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_628_urgent_message.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,632) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_632_far_away_kingdom.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
