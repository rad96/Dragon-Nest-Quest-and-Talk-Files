<VillageServer>


function n210_argenta_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,403) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_403_meet_argenta_again.xml");
		else				
		if api_quest_UserHasQuest(userObjID,436) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_436_not_same.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9481) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc04_9481_encounter_legends.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9593) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc04_9593_argentas_whereabouts.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n210_argenta_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,403) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_403_meet_argenta_again.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,436) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_436_not_same.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9481) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc04_9481_encounter_legends.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9593) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc04_9593_argentas_whereabouts.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
