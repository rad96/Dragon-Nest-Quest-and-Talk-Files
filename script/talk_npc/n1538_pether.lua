<VillageServer>


function n1538_pether_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9422) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9422_desitny.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9424) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9424_resentment.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1538_pether_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9422) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9422_desitny.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9424) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9424_resentment.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
