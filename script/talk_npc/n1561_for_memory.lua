<VillageServer>


function n1561_for_memory_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9432) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9432_nervousness.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9452) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9452_time_traveler.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9453) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9453_time_traveler.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1561_for_memory_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9432) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9432_nervousness.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9452) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9452_time_traveler.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9453) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9453_time_traveler.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
