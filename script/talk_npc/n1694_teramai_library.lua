<VillageServer>


function n1694_teramai_library_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4605) > -1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_UserHasQuest(userObjID,4606) > -1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_UserHasQuest(userObjID,9505) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9505_secret_helper.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9508) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9508_catch_ignasio.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9509) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9509_regained_the_prophet.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9521) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc07_9521_waking_up_to_prophet.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1694_teramai_library_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4605) > -1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4606) > -1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9505) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9505_secret_helper.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9508) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9508_catch_ignasio.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9509) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9509_regained_the_prophet.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9521) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc07_9521_waking_up_to_prophet.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
