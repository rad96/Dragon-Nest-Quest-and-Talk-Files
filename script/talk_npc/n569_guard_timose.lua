<VillageServer>


function n569_guard_timose_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,27) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_027_revealed_secret_promise.xml");
		else				
		if api_quest_UserHasQuest(userObjID,28) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_028_rescue_prophet.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9573) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc01_9573_misunderstanding.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9574) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc01_9574_antagonism.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n569_guard_timose_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,27) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_027_revealed_secret_promise.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,28) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_028_rescue_prophet.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9573) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc01_9573_misunderstanding.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9574) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc01_9574_antagonism.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
