<VillageServer>


function n762_karahan_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,764) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_764_stray_hearts.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9335) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9335_karahans_words.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9511) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9511_weirdo.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9514) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9514_birth_of_tragedy.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n762_karahan_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,764) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_764_stray_hearts.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9335) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9335_karahans_words.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9511) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9511_weirdo.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9514) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9514_birth_of_tragedy.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
