<VillageServer>


function n1206_lunaria_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 31) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		api_quest_AddQuest(userObjID,31, 2);
		api_quest_SetQuestStep(userObjID, 31, 1);
		api_quest_SetJournalStep(userObjID, 31, 1);
		api_quest_ForceCompleteQuest(userObjID, 24, 0, 1, 1, 0);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		api_quest_AddQuest(userObjID,31, 2);
		api_quest_SetQuestStep(userObjID, 31, 1);
		api_quest_SetJournalStep(userObjID, 31, 1);
		api_quest_ForceCompleteQuest(userObjID, 24, 0, 1, 1, 0);
		end

		end

	elseif npc_talk_index == "back" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1206_lunaria_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 31) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		api_quest_AddQuest( pRoom, userObjID,31, 2);
		api_quest_SetQuestStep( pRoom, userObjID, 31, 1);
		api_quest_SetJournalStep( pRoom, userObjID, 31, 1);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 24, 0, 1, 1, 0);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		api_quest_AddQuest( pRoom, userObjID,31, 2);
		api_quest_SetQuestStep( pRoom, userObjID, 31, 1);
		api_quest_SetJournalStep( pRoom, userObjID, 31, 1);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 24, 0, 1, 1, 0);
		end

		end

	elseif npc_talk_index == "back" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
