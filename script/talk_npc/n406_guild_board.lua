<VillageServer>


function n406_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "stage_15" then
		if api_quest_UserHasQuest(userObjID,3041) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3041_n406_forest_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3042) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3042_n406_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3043) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3043_n406_forest_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3044) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3044_n406_forest_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3049) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3049_n406_forest_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_15", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_25" then
		if api_quest_UserHasQuest(userObjID,3141) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3141_n406_center_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3142) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3142_n406_center_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3144) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3144_n406_center_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3145) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3145_n406_center_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3147) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3147_n406_center_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3150) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3150_n406_center_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3149) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3149_n406_center_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_25", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3041) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3041_n406_forest_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3042) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3042_n406_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3043) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3043_n406_forest_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3044) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3044_n406_forest_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3049) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3049_n406_forest_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3141) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3141_n406_center_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3142) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3142_n406_center_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3144) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3144_n406_center_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3145) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3145_n406_center_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3147) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3147_n406_center_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3150) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3150_n406_center_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3149) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3149_n406_center_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_15" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_25" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n406_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "stage_15" then
		if api_quest_UserHasQuest( pRoom, userObjID,3041) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3041_n406_forest_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3042) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3042_n406_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3043) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3043_n406_forest_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3044) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3044_n406_forest_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3049) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3049_n406_forest_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_15", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_25" then
		if api_quest_UserHasQuest( pRoom, userObjID,3141) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3141_n406_center_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3142) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3142_n406_center_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3144) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3144_n406_center_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3145) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3145_n406_center_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3147) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3147_n406_center_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3150) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3150_n406_center_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3149) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3149_n406_center_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_25", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3041) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3041_n406_forest_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3042) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3042_n406_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3043) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3043_n406_forest_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3044) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3044_n406_forest_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3049) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3049_n406_forest_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3141) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3141_n406_center_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3142) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3142_n406_center_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3144) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3144_n406_center_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3145) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3145_n406_center_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3147) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3147_n406_center_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3150) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3150_n406_center_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3149) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3149_n406_center_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_15" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_25" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
