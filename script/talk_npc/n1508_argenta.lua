<VillageServer>


function n1508_argenta_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9411) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9411_entrance_of_monolith.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9412) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9412_meeting.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9416) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9416_memory.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9425) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9425_power.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1508_argenta_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9411) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9411_entrance_of_monolith.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9412) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9412_meeting.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9416) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9416_memory.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9425) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9425_power.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
