<VillageServer>


function n754_cian_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,724) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_724_lotusmarsh.xml");
		else				
		if api_quest_UserHasQuest(userObjID,725) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_725_karakule.xml");
		else				
		if api_quest_UserHasQuest(userObjID,727) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_727_mystery_man.xml");
		else				
		if api_quest_UserHasQuest(userObjID,728) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_728_whos_watching_me.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_UserHasQuest(userObjID,724) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_724_lotusmarsh.xml");
		else				
		if api_quest_UserHasQuest(userObjID,725) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_725_karakule.xml");
		else				
		if api_quest_UserHasQuest(userObjID,727) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_727_mystery_man.xml");
		else				
		if api_quest_UserHasQuest(userObjID,728) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_728_whos_watching_me.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n754_cian_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,724) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_724_lotusmarsh.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,725) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_725_karakule.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,727) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_727_mystery_man.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,728) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_728_whos_watching_me.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_UserHasQuest( pRoom, userObjID,724) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_724_lotusmarsh.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,725) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_725_karakule.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,727) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_727_mystery_man.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,728) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_728_whos_watching_me.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
