<VillageServer>


function n400_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,165) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq05_165_enter_adventure.xml");
		else				
		if api_user_GetUserLevel(userObjID) >= 8 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	elseif npc_talk_index == "stage_12" then
		if api_quest_UserHasQuest(userObjID,3036) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3036_n400_base_hungry.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3037) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3037_n400_base_enermy_plan.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3038) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3038_n400_base_tara_anxiety.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_12", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "stage_21" then
		if api_quest_UserHasQuest(userObjID,3101) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3101_n400_fairy_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3102) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3102_n400_fairy_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3103) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3103_n400_fairy_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3104) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3104_n400_fairy_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3105) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3105_n400_fairy_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3109) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3109_n400_fairy_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3110) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3110_n400_fairy_rank.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_21", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3011) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq05_3011_n400_marion_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3012) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq05_3012_n400_marion_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3036) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3036_n400_base_hungry.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3037) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3037_n400_base_enermy_plan.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3038) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3038_n400_base_tara_anxiety.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3101) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3101_n400_fairy_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3102) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3102_n400_fairy_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3103) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3103_n400_fairy_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3104) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3104_n400_fairy_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3105) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3105_n400_fairy_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3109) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3109_n400_fairy_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3110) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3110_n400_fairy_rank.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_11" then
		if api_quest_UserHasQuest(userObjID,165) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq05_165_enter_adventure.xml");
		else				
		if api_user_GetUserLevel(userObjID) >= 8 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_stage_12" then
		if api_quest_UserHasQuest(userObjID,165) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq05_165_enter_adventure.xml");
		else				
		if api_user_GetUserLevel(userObjID) >= 8 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_stage_21" then
		if api_quest_UserHasQuest(userObjID,165) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq05_165_enter_adventure.xml");
		else				
		if api_user_GetUserLevel(userObjID) >= 8 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n400_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,165) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq05_165_enter_adventure.xml");
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 8 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	elseif npc_talk_index == "stage_12" then
		if api_quest_UserHasQuest( pRoom, userObjID,3036) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3036_n400_base_hungry.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3037) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3037_n400_base_enermy_plan.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3038) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3038_n400_base_tara_anxiety.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_12", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "stage_21" then
		if api_quest_UserHasQuest( pRoom, userObjID,3101) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3101_n400_fairy_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3102) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3102_n400_fairy_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3103) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3103_n400_fairy_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3104) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3104_n400_fairy_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3105) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3105_n400_fairy_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3109) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3109_n400_fairy_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3110) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3110_n400_fairy_rank.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_21", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3011) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq05_3011_n400_marion_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3012) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq05_3012_n400_marion_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3036) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3036_n400_base_hungry.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3037) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3037_n400_base_enermy_plan.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3038) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3038_n400_base_tara_anxiety.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3101) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3101_n400_fairy_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3102) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3102_n400_fairy_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3103) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3103_n400_fairy_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3104) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3104_n400_fairy_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3105) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3105_n400_fairy_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3109) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3109_n400_fairy_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3110) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3110_n400_fairy_rank.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_11" then
		if api_quest_UserHasQuest( pRoom, userObjID,165) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq05_165_enter_adventure.xml");
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 8 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_stage_12" then
		if api_quest_UserHasQuest( pRoom, userObjID,165) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq05_165_enter_adventure.xml");
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 8 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_stage_21" then
		if api_quest_UserHasQuest( pRoom, userObjID,165) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq05_165_enter_adventure.xml");
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 8 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
