<VillageServer>


function n351_farm_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back_talk" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "skill_1" then
		if api_npc_CheckSecondarySkill( userObjID, 2101) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "no_skill", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( userObjID, 665) == 1 then				
		api_npc_CreateSecondarySkill( userObjID, 2101);
		api_npc_NextTalk(userObjID, npcObjID, "grow_skill", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "noquest_1", npc_talk_target);
		end

		end

	elseif npc_talk_index == "skill_2" then
		if api_npc_CheckSecondarySkill( userObjID,3101) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "no_skill", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( userObjID, 668) == 1 then				
		api_npc_CreateSecondarySkill( userObjID, 3101);
		api_npc_NextTalk(userObjID, npcObjID, "fishing_skill", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "noquest_2", npc_talk_target);
		end

		end

	elseif npc_talk_index == "skill_3" then
		if api_npc_CheckSecondarySkill( userObjID,1101) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "no_skill", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( userObjID,671) == 1 then				
		api_npc_CreateSecondarySkill( userObjID, 1101);
		api_npc_NextTalk(userObjID, npcObjID, "cook_skill", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "noquest_3", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_skill" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "farmshop" then
		api_ui_OpenShop(userObjID,45001,100);
	elseif npc_talk_index == "cookshop" then
		api_ui_OpenShop(userObjID,45002,100);
	elseif npc_talk_index == "back_buy" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n351_farm_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back_talk" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "skill_1" then
		if api_npc_CheckSecondarySkill( pRoom,  userObjID, 2101) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "no_skill", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom,  userObjID, 665) == 1 then				
		api_npc_CreateSecondarySkill( pRoom,  userObjID, 2101);
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "grow_skill", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "noquest_1", npc_talk_target);
		end

		end

	elseif npc_talk_index == "skill_2" then
		if api_npc_CheckSecondarySkill( pRoom,  userObjID,3101) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "no_skill", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom,  userObjID, 668) == 1 then				
		api_npc_CreateSecondarySkill( pRoom,  userObjID, 3101);
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "fishing_skill", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "noquest_2", npc_talk_target);
		end

		end

	elseif npc_talk_index == "skill_3" then
		if api_npc_CheckSecondarySkill( pRoom,  userObjID,1101) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "no_skill", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom,  userObjID,671) == 1 then				
		api_npc_CreateSecondarySkill( pRoom,  userObjID, 1101);
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "cook_skill", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "noquest_3", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_skill" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "farmshop" then
		api_ui_OpenShop( pRoom, userObjID,45001,100);
	elseif npc_talk_index == "cookshop" then
		api_ui_OpenShop( pRoom, userObjID,45002,100);
	elseif npc_talk_index == "back_buy" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
