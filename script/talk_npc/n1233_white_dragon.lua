<VillageServer>


function n1233_white_dragon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9392) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9392_small_union.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9396) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9396_for_hope.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9397) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9397_for_hope.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9398) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9398_for_hope.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9399) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9399_for_hope.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9400) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9400_for_hope.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9401) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9401_for_hope.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9402) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9402_for_hope.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4433) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq34_4433_dragons_memory.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1233_white_dragon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9392) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9392_small_union.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9396) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9396_for_hope.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9397) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9397_for_hope.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9398) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9398_for_hope.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9399) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9399_for_hope.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9400) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9400_for_hope.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9401) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9401_for_hope.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9402) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9402_for_hope.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4433) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq34_4433_dragons_memory.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
