<VillageServer>


function n219_velskud_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,430) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_430_trace_apostle.xml");
		else				
		if api_quest_UserHasQuest(userObjID,429) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_429_guardian_geraint.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9500) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9500_faith_shifted.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9501) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9501_meaning_of_the_letter.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9612) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9612_crossroads.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9613) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9613_lost_prophets.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n219_velskud_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,430) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_430_trace_apostle.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,429) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_429_guardian_geraint.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9500) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9500_faith_shifted.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9501) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9501_meaning_of_the_letter.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9612) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9612_crossroads.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9613) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9613_lost_prophets.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
