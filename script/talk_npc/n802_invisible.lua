<VillageServer>


function n802_invisible_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9202) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9202_lament_for_farewell3.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9355) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9355_telling_farewell3.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9525) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc08_9525_corrupted_ruins.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9640) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc08_9640_ancient_artifacts.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n802_invisible_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9202) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9202_lament_for_farewell3.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9355) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9355_telling_farewell3.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9525) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc08_9525_corrupted_ruins.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9640) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc08_9640_ancient_artifacts.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
