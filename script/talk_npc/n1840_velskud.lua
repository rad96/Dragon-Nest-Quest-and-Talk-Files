<VillageServer>


function n1840_velskud_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4695) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc16_4695_red_dragon_nest.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9700) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc17_9700_go_mistland1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9701) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc17_9701_go_mistland2.xml");
		else		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1840_velskud_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4695) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc16_4695_red_dragon_nest.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9700) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc17_9700_go_mistland1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9701) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc17_9701_go_mistland2.xml");
		else		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
