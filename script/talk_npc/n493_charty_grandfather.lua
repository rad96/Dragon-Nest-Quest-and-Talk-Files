<VillageServer>


function n493_charty_grandfather_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,714) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_714_occupied_territory_of_ds.xml");
		else				
		if api_quest_UserHasQuest(userObjID,733) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_733_charty_grandfather.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9340) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9340_chaotic_voice.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9514) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9514_birth_of_tragedy.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9626) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9626_testament.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n493_charty_grandfather_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,714) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_714_occupied_territory_of_ds.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,733) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_733_charty_grandfather.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9340) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9340_chaotic_voice.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9514) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9514_birth_of_tragedy.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9626) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9626_testament.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
