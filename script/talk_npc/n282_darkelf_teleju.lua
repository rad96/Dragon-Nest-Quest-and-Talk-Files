<VillageServer>


function n282_darkelf_teleju_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,490) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_490_darkelf_girl.xml");
		else				
		if api_quest_UserHasQuest(userObjID,494) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_494_darkelf_teleju4.xml");
		else				
		if api_quest_UserHasQuest(userObjID,521) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_521_i_know_you.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n282_darkelf_teleju_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,490) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_490_darkelf_girl.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,494) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_494_darkelf_teleju4.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,521) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_521_i_know_you.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
