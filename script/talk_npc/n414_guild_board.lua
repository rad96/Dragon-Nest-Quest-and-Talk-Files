<VillageServer>


function n414_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "check_library" then
		if api_quest_UserHasQuest(userObjID,3374) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3374_n414_library_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3379) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3379_n414_library_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3380) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3380_n414_library_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_52", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_east" then
		if api_quest_UserHasQuest(userObjID,3394) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3394_n414_east_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3399) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3399_n414_east_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3400) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3400_n411_east_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_55", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_west" then
		if api_quest_UserHasQuest(userObjID,3414) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3414_n414_west_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3419) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3419_n414_west_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3420) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3420_n414_west_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_56", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest(userObjID,3374) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3374_n414_library_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3379) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3379_n414_library_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3380) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3380_n414_library_hidden.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3394) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3394_n414_east_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3399) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3399_n414_east_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3400) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3400_n411_east_hidden.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3414) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3414_n414_west_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3419) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3419_n414_west_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3420) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3420_n414_west_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n414_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "check_library" then
		if api_quest_UserHasQuest( pRoom, userObjID,3374) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3374_n414_library_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3379) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3379_n414_library_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3380) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3380_n414_library_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_52", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_east" then
		if api_quest_UserHasQuest( pRoom, userObjID,3394) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3394_n414_east_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3399) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3399_n414_east_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3400) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3400_n411_east_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_55", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_west" then
		if api_quest_UserHasQuest( pRoom, userObjID,3414) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3414_n414_west_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3419) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3419_n414_west_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3420) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3420_n414_west_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_56", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest( pRoom, userObjID,3374) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3374_n414_library_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3379) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3379_n414_library_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3380) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3380_n414_library_hidden.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3394) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3394_n414_east_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3399) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3399_n414_east_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3400) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3400_n411_east_hidden.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3414) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3414_n414_west_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3419) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3419_n414_west_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3420) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3420_n414_west_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
