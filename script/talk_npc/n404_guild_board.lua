<VillageServer>


function n404_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "stage_13" then
		if api_quest_UserHasQuest(userObjID,3021) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3021_n404_haunt_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3022) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3022_n404_haunt_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3023) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3023_n404_haunt_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3029) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3029_n404_haunt_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_13", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_18" then
		if api_quest_UserHasQuest(userObjID,3071) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3071_n404_ambush_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3072) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3072_n404_ambush_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3073) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3073_n404_ambush_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3074) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3074_n404_ambush_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3080) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3080_n404_ambush_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3079) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3079_n404_ambush_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_18", npc_talk_target);
		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3021) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3021_n404_haunt_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3022) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3022_n404_haunt_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3023) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3023_n404_haunt_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3029) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3029_n404_haunt_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3071) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3071_n404_ambush_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3072) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3072_n404_ambush_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3073) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3073_n404_ambush_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3074) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3074_n404_ambush_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3080) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3080_n404_ambush_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3079) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3079_n404_ambush_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_13" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_18" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n404_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "stage_13" then
		if api_quest_UserHasQuest( pRoom, userObjID,3021) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3021_n404_haunt_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3022) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3022_n404_haunt_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3023) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3023_n404_haunt_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3029) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3029_n404_haunt_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_13", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_18" then
		if api_quest_UserHasQuest( pRoom, userObjID,3071) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3071_n404_ambush_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3072) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3072_n404_ambush_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3073) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3073_n404_ambush_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3074) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3074_n404_ambush_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3080) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3080_n404_ambush_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3079) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3079_n404_ambush_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_18", npc_talk_target);
		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3021) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3021_n404_haunt_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3022) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3022_n404_haunt_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3023) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3023_n404_haunt_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3029) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3029_n404_haunt_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3071) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3071_n404_ambush_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3072) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3072_n404_ambush_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3073) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3073_n404_ambush_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3074) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3074_n404_ambush_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3080) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3080_n404_ambush_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3079) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3079_n404_ambush_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_13" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_18" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
