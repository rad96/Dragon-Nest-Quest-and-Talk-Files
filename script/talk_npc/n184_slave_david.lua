<VillageServer>


function n184_slave_david_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,260) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq08_260_argenta_flower.xml");
		else				
		if api_quest_UserHasQuest(userObjID,376) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq08_376_meet_david.xml");
		else				
		if api_quest_UserHasQuest(userObjID,249) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq08_249_meet_david.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n184_slave_david_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,260) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq08_260_argenta_flower.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,376) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq08_376_meet_david.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,249) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq08_249_meet_david.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
