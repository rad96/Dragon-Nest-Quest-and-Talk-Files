<VillageServer>


function n2016_darklair_praline_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4765) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_quide_4765_endless_nightmare.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "starshop" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,52001,105);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,52002,105);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,52003,105);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,52004,105);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,52005,105);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,52006,105);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,52007,105);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,52008,105);
		else				
		api_ui_OpenShop(userObjID,52009,105);
		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n2016_darklair_praline_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4765) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_quide_4765_endless_nightmare.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "starshop" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,52001,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,52002,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,52003,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,52004,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,52005,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,52006,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,52007,105);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,52008,105);
		else				
		api_ui_OpenShop( pRoom, userObjID,52009,105);
		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
