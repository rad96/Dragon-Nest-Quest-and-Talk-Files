<VillageServer>


function n1116_wanderer_elf_riana_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4326) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq34_4326_past_of_ridiculous.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4330) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq34_4330_memeory_embedded2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4333) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq34_4333_crossing_request.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1116_wanderer_elf_riana_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4326) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq34_4326_past_of_ridiculous.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4330) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq34_4330_memeory_embedded2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4333) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq34_4333_crossing_request.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
