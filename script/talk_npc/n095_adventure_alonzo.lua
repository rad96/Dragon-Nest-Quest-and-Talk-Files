<VillageServer>
function n095_adventure_alonzo_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "buy" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,50);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,51);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,52);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,53);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</VillageServer>

<GameServer>
function n095_adventure_alonzo_OnTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "buy" then
		if api_user_GetUserClassID(pRoom, userObjID) == 1 then				
		api_ui_OpenShop(pRoom, userObjID,50);
		else				
		if api_user_GetUserClassID(pRoom, userObjID) == 2 then				
		api_ui_OpenShop(pRoom, userObjID,51);
		else				
		if api_user_GetUserClassID(pRoom, userObjID) == 3 then				
		api_ui_OpenShop(pRoom, userObjID,52);
		else				
		if api_user_GetUserClassID(pRoom, userObjID) == 4 then				
		api_ui_OpenShop(pRoom, userObjID,53);
		else				
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</GameServer>
