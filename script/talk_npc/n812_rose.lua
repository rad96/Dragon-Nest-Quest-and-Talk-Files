<VillageServer>


function n812_rose_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9208) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9208_reunited.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9361) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9361_that_girl.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9362) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9362_last_sleep.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n812_rose_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9208) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9208_reunited.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9361) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9361_that_girl.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9362) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9362_last_sleep.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
