<VillageServer>


function n1558_gaharam_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9431) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9431_start_of_journey.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9432) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9432_nervousness.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9433) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9433_whos_story.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9435) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9435_one_step_again.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9436) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9436_one_step_again.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9437) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9437_promise.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9438) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9438_second_desperation.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9441) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9441_empty_hope.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9443) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9443_truth.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9444) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9444_thrid_desperation.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9445) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9445_forgiveness.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9446) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9446_shadow_prophetess.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9447) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9447_shadow_prophetess.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1558_gaharam_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9431) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9431_start_of_journey.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9432) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9432_nervousness.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9433) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9433_whos_story.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9435) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9435_one_step_again.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9436) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9436_one_step_again.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9437) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9437_promise.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9438) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9438_second_desperation.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9441) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9441_empty_hope.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9443) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9443_truth.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9444) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9444_thrid_desperation.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9445) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9445_forgiveness.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9446) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9446_shadow_prophetess.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9447) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9447_shadow_prophetess.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
