<VillageServer>


function n800_native_taruun_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9201) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9201_lament_for_farewell2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9354) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9354_telling_farewell2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9639) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc08_9639_prevent_him.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n800_native_taruun_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9201) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9201_lament_for_farewell2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9354) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9354_telling_farewell2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9639) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc08_9639_prevent_him.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
