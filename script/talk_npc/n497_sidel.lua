<VillageServer>


function n497_sidel_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,716) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_716_reason_to_be_strong.xml");
		else				
		if api_quest_UserHasQuest(userObjID,736) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_736_prepared.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9628) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9628_belated_news.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n497_sidel_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,716) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_716_reason_to_be_strong.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,736) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_736_prepared.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9628) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9628_belated_news.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
