<VillageServer>


function n1403_guild_board_new_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "stage_1" then
		if api_quest_UserHasQuest(userObjID,3021) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3021_n404_haunt_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3022) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3022_n404_haunt_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3023) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3023_n404_haunt_drop.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_1", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "stage_2" then
		if api_quest_UserHasQuest(userObjID,3031) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3031_n405_canyon_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3032) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3032_n405_canyon_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3033) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3033_n405_canyon_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3034) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3034_n405_canyon_hard.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_2", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_3" then
		if api_quest_UserHasQuest(userObjID,3051) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3051_n408_death_forest_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3052) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3052_n408_death_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3053) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3053_n408_death_forest_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3054) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3054_n408_death_forest_hard.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_3", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_4" then
		if api_quest_UserHasQuest(userObjID,3071) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3071_n404_ambush_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3072) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3072_n404_ambush_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3073) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3073_n404_ambush_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3074) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3074_n404_ambush_hard.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_4", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_1" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back_2" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back_3" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back_4" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1403_guild_board_new_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "stage_1" then
		if api_quest_UserHasQuest( pRoom, userObjID,3021) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3021_n404_haunt_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3022) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3022_n404_haunt_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3023) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3023_n404_haunt_drop.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_1", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "stage_2" then
		if api_quest_UserHasQuest( pRoom, userObjID,3031) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3031_n405_canyon_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3032) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3032_n405_canyon_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3033) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3033_n405_canyon_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3034) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3034_n405_canyon_hard.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_2", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_3" then
		if api_quest_UserHasQuest( pRoom, userObjID,3051) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3051_n408_death_forest_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3052) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3052_n408_death_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3053) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3053_n408_death_forest_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3054) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3054_n408_death_forest_hard.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_3", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_4" then
		if api_quest_UserHasQuest( pRoom, userObjID,3071) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3071_n404_ambush_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3072) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3072_n404_ambush_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3073) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3073_n404_ambush_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3074) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3074_n404_ambush_hard.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_4", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_1" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back_2" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back_3" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back_4" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
