<VillageServer>


function n433_light_pole_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,662) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_662_old_promise.xml");
		else				
		if api_quest_UserHasQuest(userObjID,639) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_639_lucifina_soul_jar.xml");
		else				
		if api_quest_UserHasQuest(userObjID,646) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_646_destroyed_sealing_box.xml");
		else				
		if api_quest_UserHasQuest(userObjID,660) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_660_find_the_seal.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n433_light_pole_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,662) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_662_old_promise.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,639) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_639_lucifina_soul_jar.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,646) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_646_destroyed_sealing_box.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,660) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_660_find_the_seal.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
