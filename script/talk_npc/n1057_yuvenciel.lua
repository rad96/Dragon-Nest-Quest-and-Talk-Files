<VillageServer>


function n1057_yuvenciel_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 2 then				
		if api_quest_UserHasQuest(userObjID,9269) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9269_strange_step.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_quest_UserHasQuest(userObjID,9287) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9287_the_men_of_question.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1057_yuvenciel_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		if api_quest_UserHasQuest( pRoom, userObjID,9269) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9269_strange_step.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9287) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9287_the_men_of_question.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
