<VillageServer>
function n902_lily_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "quest" then
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_901_trial_quest.xml");
	elseif npc_talk_index == "keyboard" then
		api_user_ChangeMap(userObjID,13511,1);
	elseif npc_talk_index == "joypad" then
		api_user_ChangeMap(userObjID,13501,1);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</VillageServer>

<GameServer>
function n902_lily_OnTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "quest" then
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq_901_trial_quest.xml");
	elseif npc_talk_index == "keyboard" then
		api_user_ChangeMap( pRoom, userObjID,13511,1);
	elseif npc_talk_index == "joypad" then
		api_user_ChangeMap( pRoom, userObjID,13501,1);
	else
		api_npc_NextTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</GameServer>
