<VillageServer>


function n405_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "stage_14" then
		if api_quest_UserHasQuest(userObjID,3031) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3031_n405_canyon_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3032) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3032_n405_canyon_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3033) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3033_n405_canyon_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3034) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3034_n405_canyon_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3039) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3039_n405_canyon_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_14", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_23" then
		if api_quest_UserHasQuest(userObjID,3121) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3121_n405_unreturn_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3122) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3122_n405_unreturn_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3123) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3123_n405_unreturn_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3124) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3124_n405_unreturn_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3125) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3125_n405_unreturn_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3130) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3130_n405_unreturn_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3129) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3129_n405_unreturn_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_23", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3031) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3031_n405_canyon_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3032) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3032_n405_canyon_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3033) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3033_n405_canyon_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3034) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3034_n405_canyon_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3039) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3039_n405_canyon_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3121) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3121_n405_unreturn_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3122) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3122_n405_unreturn_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3123) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3123_n405_unreturn_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3124) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3124_n405_unreturn_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3125) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3125_n405_unreturn_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3130) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3130_n405_unreturn_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3129) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3129_n405_unreturn_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_14" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "bakc_stage_23" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n405_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "stage_14" then
		if api_quest_UserHasQuest( pRoom, userObjID,3031) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3031_n405_canyon_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3032) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3032_n405_canyon_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3033) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3033_n405_canyon_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3034) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3034_n405_canyon_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3039) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3039_n405_canyon_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_14", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_23" then
		if api_quest_UserHasQuest( pRoom, userObjID,3121) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3121_n405_unreturn_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3122) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3122_n405_unreturn_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3123) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3123_n405_unreturn_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3124) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3124_n405_unreturn_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3125) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3125_n405_unreturn_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3130) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3130_n405_unreturn_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3129) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3129_n405_unreturn_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_23", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3031) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3031_n405_canyon_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3032) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3032_n405_canyon_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3033) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3033_n405_canyon_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3034) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3034_n405_canyon_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3039) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3039_n405_canyon_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3121) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3121_n405_unreturn_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3122) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3122_n405_unreturn_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3123) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3123_n405_unreturn_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3124) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3124_n405_unreturn_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3125) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3125_n405_unreturn_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3130) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3130_n405_unreturn_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3129) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3129_n405_unreturn_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_14" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "bakc_stage_23" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
