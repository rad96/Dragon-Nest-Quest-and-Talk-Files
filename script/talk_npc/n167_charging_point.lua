<VillageServer>


function n167_charging_point_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,208) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_208_depleting_fuel.xml");
		else				
		if api_quest_UserHasQuest(userObjID,209) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_209_red_eyes_darkelf.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9474) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc03_9474_darkelf_elena.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n167_charging_point_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,208) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_208_depleting_fuel.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,209) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_209_red_eyes_darkelf.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9474) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc03_9474_darkelf_elena.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
