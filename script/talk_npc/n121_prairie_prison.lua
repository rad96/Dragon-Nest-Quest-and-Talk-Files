<VillageServer>


function n121_prairie_prison_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_004_red_emblem1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_003_red_emblem1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,15) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_015_red_emblem1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,16) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_016_red_emblem1.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n121_prairie_prison_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_004_red_emblem1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_003_red_emblem1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,15) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_015_red_emblem1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,16) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_016_red_emblem1.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
