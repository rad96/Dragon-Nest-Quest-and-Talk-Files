<VillageServer>


function n287_blackdragons_wing_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,424) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_424_retrieval_darkcore.xml");
		else				
		if api_quest_UserHasQuest(userObjID,417) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_417_deserted_david.xml");
		else				
		if api_quest_UserHasQuest(userObjID,391) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_391_black_medicine.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9321) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_9321_fight_fire_with_fire.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9492) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9492_new_hope.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9606) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9606_people_left.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n287_blackdragons_wing_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,424) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_424_retrieval_darkcore.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,417) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_417_deserted_david.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,391) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_391_black_medicine.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9321) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_9321_fight_fire_with_fire.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9492) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9492_new_hope.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9606) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9606_people_left.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
