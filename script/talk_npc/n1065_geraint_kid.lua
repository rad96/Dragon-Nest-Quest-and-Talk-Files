<VillageServer>


function n1065_geraint_kid_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9272) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9272_night_of_conclave.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9287) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9287_the_men_of_question.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9288) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9288_forest_that_has_lost_the_origin.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9289) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9289_gold_dragon_returns.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9290) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9290_the_secret_of_anu_arendel.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9291) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9291_blood_red_conclave.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9296) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9296_division_foe.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9410) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9410_the_outbreak_of_war.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9411) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9411_entrance_of_Monolith.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9458) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9458_the_story_of_prophecy.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1065_geraint_kid_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9272) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9272_night_of_conclave.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9287) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9287_the_men_of_question.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9288) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9288_forest_that_has_lost_the_origin.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9289) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9289_gold_dragon_returns.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9290) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9290_the_secret_of_anu_arendel.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9291) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9291_blood_red_conclave.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9296) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9296_division_foe.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9410) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9410_the_outbreak_of_war.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9411) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9411_entrance_of_Monolith.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9458) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9458_the_story_of_prophecy.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
