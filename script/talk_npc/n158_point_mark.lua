<VillageServer>


function n158_point_mark_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,102) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_102_evidence_of_pagan.xml");
		else				
		if api_quest_UserHasQuest(userObjID,112) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_112_doubt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,127) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_127_evidence_of_pagan.xml");
		else				
		if api_quest_UserHasQuest(userObjID,137) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_137_doubt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9461) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc01_9461_proof.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n158_point_mark_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,102) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_102_evidence_of_pagan.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,112) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_112_doubt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,127) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_127_evidence_of_pagan.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,137) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_137_doubt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9461) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc01_9461_proof.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
