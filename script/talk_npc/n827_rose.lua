<VillageServer>


function n827_rose_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9221) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9221_unpleased_successor.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9215) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9215_looking_for_reunion.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9213) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9213_weakened_power.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9212) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9212_to_the_evil_shadow.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9365) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9365_pitiful_reality.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9366) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9366_missing_link.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9372) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9372_show_the_cloven_hoof.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9532) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9532_reason_for_the_loss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9533) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9533_kaye_complaint.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9534) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9534_she_wishes.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9535) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9535_weird_elf.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9536) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9536_crack_the_starting.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9538) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9538_intended_ending.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n827_rose_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9221) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9221_unpleased_successor.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9215) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9215_looking_for_reunion.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9213) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9213_weakened_power.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9212) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9212_to_the_evil_shadow.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9365) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9365_pitiful_reality.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9366) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9366_missing_link.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9372) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9372_show_the_cloven_hoof.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9532) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9532_reason_for_the_loss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9533) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9533_kaye_complaint.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9534) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9534_she_wishes.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9535) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9535_weird_elf.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9536) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9536_crack_the_starting.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9538) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9538_intended_ending.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
