<VillageServer>


function n813_teramai_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 8 then				
		if api_quest_UserHasQuest(userObjID,9528) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc08_9528_traces_of_comrades.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9529) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc08_9529_purification_failed.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9530) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9530_pope_teramai.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		else				
		if api_quest_UserHasQuest(userObjID,9209) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9209_eternal_rest.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9208) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9208_reunited.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9207) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9208_reunited.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9361) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9361_that_girl.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9362) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9362_last_sleep.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n813_teramai_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		if api_quest_UserHasQuest( pRoom, userObjID,9528) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc08_9528_traces_of_comrades.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9529) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc08_9529_purification_failed.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9530) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9530_pope_teramai.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9209) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9209_eternal_rest.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9208) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9208_reunited.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9207) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9208_reunited.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9361) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9361_that_girl.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9362) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9362_last_sleep.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
