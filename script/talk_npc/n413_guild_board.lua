<VillageServer>


function n413_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
	elseif npc_talk_index == "stage_31" then
		if api_quest_UserHasQuest(userObjID,3171) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3171_n413_wilden_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3172) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3172_n413_wilden_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3173) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3173_n413_wilden_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3174) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3174_n413_wilden_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3175) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3175_n413_wilden_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3176) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3176_n413_wilden_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3177) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3177_n413_wilden_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3180) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3180_n413_wilden_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3179) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3179_n413_wilden_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_31", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_35" then
		if api_quest_UserHasQuest(userObjID,3231) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3231_n413_marisa_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3232) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3232_n413_marisa_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3233) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3233_n413_marisa_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3234) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3234_n413_marisa_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3235) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3235_n413_marisa_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3236) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3236_n413_marisa_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3237) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3237_n413_marisa_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3240) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3240_n413_marisa_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3239) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3239_n413_marisa_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_35", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_41" then
		if api_quest_UserHasQuest(userObjID,3311) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3311_n413_explosion_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3312) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3312_n413_explosion_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3313) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3313_n413_explosion_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3314) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3314_n413_explosion_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3315) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3315_n413_explosion_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3316) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3316_n413_explosion_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3317) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3317_n413_explosion_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3320) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3320_n413_explosion_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3319) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3319_n413_explosion_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_41", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_45" then
		if api_quest_UserHasQuest(userObjID,3341) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3341_n413_gtown_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3342) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3342_n413_gtown_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3343) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3343_n413_gtown_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3344) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3344_n413_gtown_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3345) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3345_n413_gtown_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3346) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3346_n413_gtown_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3347) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3347_n413_gtown_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3350) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3350_n413_gtown_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3349) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3349_n413_gtown_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_45", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3171) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3171_n413_wilden_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3172) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3172_n413_wilden_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3173) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3173_n413_wilden_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3174) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3174_n413_wilden_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3175) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3175_n413_wilden_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3176) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3176_n413_wilden_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3177) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3177_n413_wilden_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3180) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3180_n413_wilden_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3179) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3179_n413_wilden_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3231) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3231_n413_marisa_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3232) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3232_n413_marisa_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3233) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3233_n413_marisa_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3234) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3234_n413_marisa_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3235) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3235_n413_marisa_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3236) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3236_n413_marisa_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3237) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3237_n413_marisa_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3240) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3240_n413_marisa_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3239) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3239_n413_marisa_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3311) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3311_n413_explosion_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3312) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3312_n413_explosion_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3313) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3313_n413_explosion_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3314) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3314_n413_explosion_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3315) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3315_n413_explosion_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3316) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3316_n413_explosion_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3317) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3317_n413_explosion_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3320) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3320_n413_explosion_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3319) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3319_n413_explosion_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3341) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3341_n413_gtown_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3342) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3342_n413_gtown_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3343) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3343_n413_gtown_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3344) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3344_n413_gtown_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3345) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3345_n413_gtown_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3346) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3346_n413_gtown_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3347) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3347_n413_gtown_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3350) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3350_n413_gtown_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3349) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3349_n413_gtown_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_31" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_35" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_41" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n413_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
	elseif npc_talk_index == "stage_31" then
		if api_quest_UserHasQuest( pRoom, userObjID,3171) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3171_n413_wilden_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3172) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3172_n413_wilden_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3173) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3173_n413_wilden_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3174) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3174_n413_wilden_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3175) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3175_n413_wilden_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3176) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3176_n413_wilden_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3177) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3177_n413_wilden_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3180) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3180_n413_wilden_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3179) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3179_n413_wilden_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_31", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_35" then
		if api_quest_UserHasQuest( pRoom, userObjID,3231) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3231_n413_marisa_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3232) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3232_n413_marisa_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3233) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3233_n413_marisa_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3234) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3234_n413_marisa_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3235) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3235_n413_marisa_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3236) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3236_n413_marisa_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3237) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3237_n413_marisa_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3240) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3240_n413_marisa_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3239) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3239_n413_marisa_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_35", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_41" then
		if api_quest_UserHasQuest( pRoom, userObjID,3311) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3311_n413_explosion_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3312) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3312_n413_explosion_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3313) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3313_n413_explosion_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3314) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3314_n413_explosion_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3315) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3315_n413_explosion_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3316) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3316_n413_explosion_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3317) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3317_n413_explosion_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3320) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3320_n413_explosion_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3319) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3319_n413_explosion_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_41", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_45" then
		if api_quest_UserHasQuest( pRoom, userObjID,3341) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3341_n413_gtown_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3342) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3342_n413_gtown_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3343) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3343_n413_gtown_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3344) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3344_n413_gtown_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3345) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3345_n413_gtown_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3346) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3346_n413_gtown_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3347) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3347_n413_gtown_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3350) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3350_n413_gtown_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3349) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3349_n413_gtown_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_45", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3171) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3171_n413_wilden_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3172) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3172_n413_wilden_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3173) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3173_n413_wilden_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3174) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3174_n413_wilden_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3175) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3175_n413_wilden_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3176) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3176_n413_wilden_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3177) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3177_n413_wilden_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3180) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3180_n413_wilden_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3179) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3179_n413_wilden_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3231) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3231_n413_marisa_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3232) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3232_n413_marisa_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3233) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3233_n413_marisa_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3234) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3234_n413_marisa_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3235) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3235_n413_marisa_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3236) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3236_n413_marisa_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3237) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3237_n413_marisa_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3240) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3240_n413_marisa_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3239) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3239_n413_marisa_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3311) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3311_n413_explosion_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3312) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3312_n413_explosion_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3313) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3313_n413_explosion_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3314) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3314_n413_explosion_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3315) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3315_n413_explosion_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3316) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3316_n413_explosion_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3317) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3317_n413_explosion_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3320) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3320_n413_explosion_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3319) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3319_n413_explosion_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3341) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3341_n413_gtown_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3342) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3342_n413_gtown_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3343) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3343_n413_gtown_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3344) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3344_n413_gtown_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3345) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3345_n413_gtown_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3346) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3346_n413_gtown_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3347) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3347_n413_gtown_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3350) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3350_n413_gtown_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3349) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3349_n413_gtown_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_31" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_35" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_41" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
