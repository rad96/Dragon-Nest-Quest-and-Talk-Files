<VillageServer>


function n402_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,63) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq01_063_enter_adventure.xml");
		else				
		if api_user_GetUserLevel(userObjID) >= 8 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	elseif npc_talk_index == "stage_19" then
		if api_quest_UserHasQuest(userObjID,3081) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3081_n402_worship_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3082) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3082_n402_worship_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3083) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3083_n402_worship_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3084) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3084_n402_worship_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3090) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3090_n402_worship_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3089) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3089_n402_worship_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_19", npc_talk_target);
		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3081) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3081_n402_worship_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3082) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3082_n402_worship_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3083) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3083_n402_worship_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3084) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3084_n402_worship_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3090) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3090_n402_worship_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3089) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3089_n402_worship_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_5" then
		if api_quest_UserHasQuest(userObjID,63) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq01_063_enter_adventure.xml");
		else				
		if api_user_GetUserLevel(userObjID) >= 8 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_stage_19" then
		if api_quest_UserHasQuest(userObjID,63) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq01_063_enter_adventure.xml");
		else				
		if api_user_GetUserLevel(userObjID) >= 8 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n402_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,63) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq01_063_enter_adventure.xml");
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 8 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	elseif npc_talk_index == "stage_19" then
		if api_quest_UserHasQuest( pRoom, userObjID,3081) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3081_n402_worship_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3082) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3082_n402_worship_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3083) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3083_n402_worship_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3084) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3084_n402_worship_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3090) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3090_n402_worship_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3089) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3089_n402_worship_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_19", npc_talk_target);
		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3081) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3081_n402_worship_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3082) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3082_n402_worship_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3083) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3083_n402_worship_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3084) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3084_n402_worship_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3090) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3090_n402_worship_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3089) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3089_n402_worship_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_5" then
		if api_quest_UserHasQuest( pRoom, userObjID,63) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq01_063_enter_adventure.xml");
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 8 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_stage_19" then
		if api_quest_UserHasQuest( pRoom, userObjID,63) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq01_063_enter_adventure.xml");
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 8 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
