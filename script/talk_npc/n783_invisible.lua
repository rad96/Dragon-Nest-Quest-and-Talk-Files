<VillageServer>


function n783_invisible_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
	elseif npc_talk_index == "changemap" then
		api_quest_SetJournalStep(userObjID, 378, 3);
		api_user_ChangeMap(userObjID,13016,1);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n783_invisible_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
	elseif npc_talk_index == "changemap" then
		api_quest_SetJournalStep( pRoom, userObjID, 378, 3);
		api_user_ChangeMap( pRoom, userObjID,13016,1);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
