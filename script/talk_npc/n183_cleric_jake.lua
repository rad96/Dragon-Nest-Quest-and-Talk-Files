<VillageServer>


function n183_cleric_jake_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,285) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq08_285_test_cleric2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,284) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq08_284_test_cleric1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,241) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq08_241_change1_cleric.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n183_cleric_jake_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,285) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq08_285_test_cleric2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,284) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq08_284_test_cleric1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,241) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq08_241_change1_cleric.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
