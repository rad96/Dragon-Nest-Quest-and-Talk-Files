<VillageServer>


function n213_slave_david_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,701) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_701_lotusmarsh.xml");
		else				
		if api_quest_UserHasQuest(userObjID,431) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_431_next_town.xml");
		else				
		if api_quest_UserHasQuest(userObjID,422) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_422_question_of_dragon.xml");
		else				
		if api_quest_UserHasQuest(userObjID,418) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_418_curious_david.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9614) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9614_homecoming.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9615) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9615_home_lotusmarsh.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n213_slave_david_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,701) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_701_lotusmarsh.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,431) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_431_next_town.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,422) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_422_question_of_dragon.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,418) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_418_curious_david.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9614) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9614_homecoming.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9615) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9615_home_lotusmarsh.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
