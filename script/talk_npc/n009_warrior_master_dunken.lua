<VillageServer>


function n009_warrior_master_dunken_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "skill" then
		api_ui_OpenSkillShop(userObjID);
	elseif npc_talk_index == "back1" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "back2" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "back3" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n009_warrior_master_dunken_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "skill" then
		api_ui_OpenSkillShop( pRoom, userObjID);
	elseif npc_talk_index == "back1" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "back2" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "back3" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
