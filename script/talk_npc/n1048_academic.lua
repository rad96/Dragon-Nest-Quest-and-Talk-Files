<VillageServer>


function n1048_academic_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9257) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9257_turning_point_of_history.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9387) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9387_choice_of_history.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9539) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9539_he_was_not.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9555) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc10_9555_terminus_of_the_future.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9501) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9501_meaning_of_the_letter.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1048_academic_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9257) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9257_turning_point_of_history.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9387) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9387_choice_of_history.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9539) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9539_he_was_not.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9555) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc10_9555_terminus_of_the_future.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9501) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9501_meaning_of_the_letter.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
