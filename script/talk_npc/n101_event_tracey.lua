<VillageServer>


function n101_event_tracey_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,2120) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2120_jumping_event_1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2121) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2121_jumping_event_2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2122) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2122_jumping_event_3.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2123) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2123_jumping_event_4.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2124) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2124_jumping_event_5.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2125) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2125_jumping_event_6.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2126) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2126_jumping_event_7.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2127) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2127_jumping_event_8.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2128) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2128_jumping_event_9.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2129) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2129_jumping_event_10.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1000) == 1 then				
		if api_user_GetUserLevel(userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9258) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9263) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_user_GetUserLevel(userObjID) >= 50 then				
		if api_user_GetUserLevel(userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9258) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9263) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "quest_check" then
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_npc_NextTalk(userObjID, npcObjID, "assassin", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9224) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "skip1", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "cant_skip", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_cantskip" then
		if api_quest_UserHasQuest(userObjID,2120) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2120_jumping_event_1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2121) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2121_jumping_event_2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2122) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2122_jumping_event_3.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2123) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2123_jumping_event_4.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2124) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2124_jumping_event_5.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2125) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2125_jumping_event_6.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2126) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2126_jumping_event_7.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2127) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2127_jumping_event_8.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2128) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2128_jumping_event_9.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2129) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2129_jumping_event_10.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1000) == 1 then				
		if api_user_GetUserLevel(userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9258) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9263) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_user_GetUserLevel(userObjID) >= 50 then				
		if api_user_GetUserLevel(userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9258) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9263) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_assassin" then
		if api_quest_UserHasQuest(userObjID,2120) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2120_jumping_event_1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2121) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2121_jumping_event_2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2122) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2122_jumping_event_3.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2123) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2123_jumping_event_4.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2124) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2124_jumping_event_5.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2125) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2125_jumping_event_6.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2126) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2126_jumping_event_7.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2127) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2127_jumping_event_8.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2128) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2128_jumping_event_9.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2129) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2129_jumping_event_10.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1000) == 1 then				
		if api_user_GetUserLevel(userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9258) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9263) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_user_GetUserLevel(userObjID) >= 50 then				
		if api_user_GetUserLevel(userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9258) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9263) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back001" then
		if api_quest_UserHasQuest(userObjID,2120) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2120_jumping_event_1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2121) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2121_jumping_event_2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2122) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2122_jumping_event_3.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2123) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2123_jumping_event_4.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2124) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2124_jumping_event_5.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2125) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2125_jumping_event_6.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2126) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2126_jumping_event_7.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2127) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2127_jumping_event_8.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2128) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2128_jumping_event_9.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2129) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2129_jumping_event_10.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1000) == 1 then				
		if api_user_GetUserLevel(userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9258) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9263) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_user_GetUserLevel(userObjID) >= 50 then				
		if api_user_GetUserLevel(userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9258) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9263) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_end002" then
		if api_quest_UserHasQuest(userObjID,2120) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2120_jumping_event_1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2121) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2121_jumping_event_2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2122) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2122_jumping_event_3.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2123) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2123_jumping_event_4.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2124) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2124_jumping_event_5.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2125) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2125_jumping_event_6.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2126) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2126_jumping_event_7.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2127) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2127_jumping_event_8.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2128) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2128_jumping_event_9.xml");
		else				
		if api_quest_UserHasQuest(userObjID,2129) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_2129_jumping_event_10.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1000) == 1 then				
		if api_user_GetUserLevel(userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9258) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9263) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_user_GetUserLevel(userObjID) >= 50 then				
		if api_user_GetUserLevel(userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9258) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 9263) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "go_skip" then
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_quest_ForceSkipQuest(userObjID, "9241,9242,9243,9244,9245,9251,9252,9253,9261,9262,9263", 1);
		else				
		api_quest_ForceSkipQuest(userObjID, "9225,9226,9227,9228,9229,9246,9247,9248,9256,9257,9258", 1);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n101_event_tracey_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,2120) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2120_jumping_event_1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2121) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2121_jumping_event_2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2122) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2122_jumping_event_3.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2123) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2123_jumping_event_4.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2124) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2124_jumping_event_5.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2125) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2125_jumping_event_6.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2126) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2126_jumping_event_7.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2127) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2127_jumping_event_8.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2128) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2128_jumping_event_9.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2129) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2129_jumping_event_10.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1000) == 1 then				
		if api_user_GetUserLevel( pRoom, userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9258) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9263) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 50 then				
		if api_user_GetUserLevel( pRoom, userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9258) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9263) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "quest_check" then
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "assassin", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9224) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "skip1", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "cant_skip", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back_cantskip" then
		if api_quest_UserHasQuest( pRoom, userObjID,2120) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2120_jumping_event_1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2121) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2121_jumping_event_2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2122) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2122_jumping_event_3.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2123) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2123_jumping_event_4.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2124) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2124_jumping_event_5.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2125) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2125_jumping_event_6.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2126) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2126_jumping_event_7.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2127) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2127_jumping_event_8.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2128) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2128_jumping_event_9.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2129) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2129_jumping_event_10.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1000) == 1 then				
		if api_user_GetUserLevel( pRoom, userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9258) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9263) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 50 then				
		if api_user_GetUserLevel( pRoom, userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9258) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9263) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_assassin" then
		if api_quest_UserHasQuest( pRoom, userObjID,2120) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2120_jumping_event_1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2121) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2121_jumping_event_2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2122) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2122_jumping_event_3.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2123) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2123_jumping_event_4.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2124) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2124_jumping_event_5.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2125) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2125_jumping_event_6.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2126) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2126_jumping_event_7.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2127) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2127_jumping_event_8.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2128) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2128_jumping_event_9.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2129) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2129_jumping_event_10.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1000) == 1 then				
		if api_user_GetUserLevel( pRoom, userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9258) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9263) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 50 then				
		if api_user_GetUserLevel( pRoom, userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9258) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9263) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back001" then
		if api_quest_UserHasQuest( pRoom, userObjID,2120) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2120_jumping_event_1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2121) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2121_jumping_event_2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2122) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2122_jumping_event_3.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2123) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2123_jumping_event_4.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2124) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2124_jumping_event_5.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2125) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2125_jumping_event_6.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2126) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2126_jumping_event_7.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2127) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2127_jumping_event_8.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2128) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2128_jumping_event_9.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2129) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2129_jumping_event_10.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1000) == 1 then				
		if api_user_GetUserLevel( pRoom, userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9258) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9263) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 50 then				
		if api_user_GetUserLevel( pRoom, userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9258) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9263) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_end002" then
		if api_quest_UserHasQuest( pRoom, userObjID,2120) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2120_jumping_event_1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2121) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2121_jumping_event_2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2122) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2122_jumping_event_3.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2123) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2123_jumping_event_4.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2124) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2124_jumping_event_5.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2125) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2125_jumping_event_6.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2126) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2126_jumping_event_7.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2127) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2127_jumping_event_8.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2128) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2128_jumping_event_9.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2129) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_2129_jumping_event_10.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1000) == 1 then				
		if api_user_GetUserLevel( pRoom, userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9258) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9263) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 50 then				
		if api_user_GetUserLevel( pRoom, userObjID) >= 60 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9258) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9263) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_skip", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "go_skip" then
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_quest_ForceSkipQuest( pRoom, userObjID, "9241,9242,9243,9244,9245,9251,9252,9253,9261,9262,9263", 1);
		else				
		api_quest_ForceSkipQuest( pRoom, userObjID, "9225,9226,9227,9228,9229,9246,9247,9248,9256,9257,9258", 1);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
