<VillageServer>


function n369_cart_ticketer_karene_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "move_1" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID,1,1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "move_2" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID,5,1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "move_3" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID,8,1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "move_4" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID,11,1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n369_cart_ticketer_karene_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "move_1" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID,1,1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "move_2" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID,5,1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "move_3" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID,8,1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "move_4" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID,11,1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
