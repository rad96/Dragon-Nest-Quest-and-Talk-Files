<VillageServer>


function n026_trader_may_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "buy" then
		api_ui_OpenShop(userObjID,30,100);
	elseif npc_talk_index == "back_talk" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_talk1" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "buy_90" then
		api_ui_OpenShop(userObjID,30,100);
	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( userObjID, 26 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "buy_60" then
		api_ui_OpenShop(userObjID,30,100);
	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( userObjID, 26 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "buy_0" then
		api_ui_OpenShop(userObjID,30,100);
	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( userObjID, 26 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( userObjID, 26 );
		api_npc_NextTalk(userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_gift" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 26 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "buy_30" then
		api_ui_OpenShop(userObjID,30,100);
	elseif npc_talk_index == "present_30" then
		api_ui_OpenGiveNpcPresent( userObjID, 26 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n026_trader_may_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "buy" then
		api_ui_OpenShop( pRoom, userObjID,30,100);
	elseif npc_talk_index == "back_talk" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_talk1" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "buy_90" then
		api_ui_OpenShop( pRoom, userObjID,30,100);
	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 26 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "buy_60" then
		api_ui_OpenShop( pRoom, userObjID,30,100);
	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 26 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "buy_0" then
		api_ui_OpenShop( pRoom, userObjID,30,100);
	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 26 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( pRoom,  userObjID, 26 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_gift" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 5101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 26 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "buy_30" then
		api_ui_OpenShop( pRoom, userObjID,30,100);
	elseif npc_talk_index == "present_30" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 26 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
