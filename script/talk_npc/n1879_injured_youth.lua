<VillageServer>


function n1879_injured_youth_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4707) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc17_4707_request_of_sarah.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4709) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc17_4709_not_reach_letter.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1879_injured_youth_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4707) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc17_4707_request_of_sarah.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4709) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc17_4709_not_reach_letter.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
