<VillageServer>


function n004_guard_steave_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "quest_001" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "quest_90" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( userObjID, 4 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "quest_60" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( userObjID, 4 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "quest_30" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "present_30" then
		api_ui_OpenGiveNpcPresent( userObjID, 4 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "quest_0" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( userObjID, 4 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( userObjID, 4 );
		api_npc_NextTalk(userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_gift" then
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_talk" then
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "quest_m0" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "present_m0" then
		api_ui_OpenGiveNpcPresent( userObjID, 4 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "back_mkn" then
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 4 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n004_guard_steave_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "quest_001" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "quest_90" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 4 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "quest_60" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 4 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "quest_30" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "present_30" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 4 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "quest_0" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 4 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( pRoom,  userObjID, 4 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_gift" then
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_talk" then
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "quest_m0" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_quest", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "present_m0" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 4 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "back_mkn" then
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 4 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "makina_0", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
