<VillageServer>


function n455_velskud_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,755) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_755_shoadow_of_dark_night.xml");
		else				
		if api_quest_UserHasQuest(userObjID,756) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_756_turn_defeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4687) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_changejob_4687_start_of_cracks.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4688) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_changejob_4688_illusion_leading_to_light.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n455_velskud_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,755) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_755_shoadow_of_dark_night.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,756) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_756_turn_defeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4687) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_changejob_4687_start_of_cracks.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4688) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_changejob_4688_illusion_leading_to_light.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
