<VillageServer>


function n837_rambert_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9223) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9223_lost_objective.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9375) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9375_au_revoir.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9544) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9544_truth_like_lies.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n837_rambert_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9223) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9223_lost_objective.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9375) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9375_au_revoir.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9544) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9544_truth_like_lies.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
