<VillageServer>


function n858_place_of_trail_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9220) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9220_reveal_the_way.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9369) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9369_where_of_truth.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9370) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9370_testimony.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n858_place_of_trail_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9220) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9220_reveal_the_way.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9369) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9369_where_of_truth.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9370) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9370_testimony.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
