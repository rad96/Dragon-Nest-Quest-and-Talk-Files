<VillageServer>


function n360_repair_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "repair" then
		if api_user_RepairItem(userObjID, 0) == 2 then				
		api_npc_NextTalk(userObjID, npcObjID, "complete_repair", npc_talk_target);
		else				
		if api_user_RepairItem(userObjID, 0) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "no_money", npc_talk_target);
		else				
		if api_user_RepairItem(userObjID, 0) == 0 then				
		api_npc_NextTalk(userObjID, npcObjID, "no_item", npc_talk_target);
		else		end

		end

		end

	elseif npc_talk_index == "repair_all" then
		if api_user_RepairItem(userObjID, 1) == 2 then				
		api_npc_NextTalk(userObjID, npcObjID, "complete_repair", npc_talk_target);
		else				
		if api_user_RepairItem(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "no_money_all", npc_talk_target);
		else				
		if api_user_RepairItem(userObjID, 1) == 0 then				
		api_npc_NextTalk(userObjID, npcObjID, "no_item", npc_talk_target);
		else		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n360_repair_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "repair" then
		if api_user_RepairItem( pRoom, userObjID, 0) == 2 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "complete_repair", npc_talk_target);
		else				
		if api_user_RepairItem( pRoom, userObjID, 0) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "no_money", npc_talk_target);
		else				
		if api_user_RepairItem( pRoom, userObjID, 0) == 0 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "no_item", npc_talk_target);
		else		end

		end

		end

	elseif npc_talk_index == "repair_all" then
		if api_user_RepairItem( pRoom, userObjID, 1) == 2 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "complete_repair", npc_talk_target);
		else				
		if api_user_RepairItem( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "no_money_all", npc_talk_target);
		else				
		if api_user_RepairItem( pRoom, userObjID, 1) == 0 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "no_item", npc_talk_target);
		else		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
