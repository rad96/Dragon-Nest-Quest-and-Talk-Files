<VillageServer>


function n014_cleric_cedric_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 4 then				
		if api_quest_UserHasQuest(userObjID,106) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_106_find_the_orb.xml");
		else				
		if api_quest_UserHasQuest(userObjID,130) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_130_find_the_orb.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n014_cleric_cedric_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		if api_quest_UserHasQuest( pRoom, userObjID,106) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_106_find_the_orb.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,130) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_130_find_the_orb.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
