<VillageServer>


function n785_invisible_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,725) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_725_karakule.xml");
		else				
		if api_quest_UserHasQuest(userObjID,726) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_726_boy_and_girl.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n785_invisible_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,725) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_725_karakule.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,726) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_726_boy_and_girl.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
