<VillageServer>


function n407_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "stage_16" then
		if api_quest_UserHasQuest(userObjID,3061) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3061_n407_sleep_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3062) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3062_n407_sleep_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3063) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3063_n407_sleep_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3064) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3064_n407_sleep_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3070) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3070_n407_sleep_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3069) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3069_n407_sleep_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_16", npc_talk_target);
		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_26" then
		if api_quest_UserHasQuest(userObjID,3151) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3151_n407_pray_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3152) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3152_n407_pray_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3154) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3154_n407_pray_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3155) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3155_n407_pray_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3157) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3157_n407_pray_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3160) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3160_n407_pray_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3159) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3159_n407_pray_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_26", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3061) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3061_n407_sleep_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3062) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3062_n407_sleep_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3063) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3063_n407_sleep_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3064) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3064_n407_sleep_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3070) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3070_n407_sleep_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3069) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3069_n407_sleep_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3151) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3151_n407_pray_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3152) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3152_n407_pray_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3154) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3154_n407_pray_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3155) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3155_n407_pray_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3157) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3157_n407_pray_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3160) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3160_n407_pray_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3159) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3159_n407_pray_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_16" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_26" then
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n407_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "stage_16" then
		if api_quest_UserHasQuest( pRoom, userObjID,3061) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3061_n407_sleep_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3062) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3062_n407_sleep_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3063) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3063_n407_sleep_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3064) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3064_n407_sleep_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3070) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3070_n407_sleep_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3069) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3069_n407_sleep_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_16", npc_talk_target);
		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_26" then
		if api_quest_UserHasQuest( pRoom, userObjID,3151) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3151_n407_pray_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3152) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3152_n407_pray_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3154) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3154_n407_pray_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3155) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3155_n407_pray_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3157) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3157_n407_pray_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3160) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3160_n407_pray_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3159) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3159_n407_pray_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_26", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3061) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3061_n407_sleep_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3062) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3062_n407_sleep_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3063) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3063_n407_sleep_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3064) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3064_n407_sleep_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3070) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3070_n407_sleep_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3069) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3069_n407_sleep_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3151) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3151_n407_pray_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3152) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3152_n407_pray_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3154) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3154_n407_pray_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3155) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3155_n407_pray_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3157) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3157_n407_pray_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3160) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3160_n407_pray_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3159) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3159_n407_pray_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_16" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_26" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
