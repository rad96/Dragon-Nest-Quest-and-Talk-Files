<VillageServer>


function n886_lost_boy_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 4188) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_UserHasQuest(userObjID,4188) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4188_road_to_home.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 4187) == 1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4188_road_to_home.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4187) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4187_precious_thing.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n886_lost_boy_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 4188) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4188) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4188_road_to_home.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 4187) == 1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4188_road_to_home.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4187) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4187_precious_thing.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
