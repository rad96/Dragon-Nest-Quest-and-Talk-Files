<VillageServer>


function n410_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
	elseif npc_talk_index == "stage_32" then
		if api_quest_UserHasQuest(userObjID,3211) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3211_n410_ruin_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3212) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3212_n410_ruin_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3213) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3213_n410_ruin_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3214) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3214_n410_ruin_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3215) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3215_n410_ruin_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3216) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3216_n410_ruin_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3217) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3217_n410_ruin_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3220) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3220_n410_ruin_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3219) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3219_n410_ruin_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_32", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_34" then
		if api_quest_UserHasQuest(userObjID,3241) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3241_n410_flood_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3242) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3242_n410_flood_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3243) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3243_n410_flood_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3244) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3244_n410_flood_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3245) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3245_n410_flood_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3246) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3246_n410_flood_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3247) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3247_n410_flood_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3250) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3250_n410_flood_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3249) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3249_n410_flood_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_34", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_47" then
		if api_quest_UserHasQuest(userObjID,3331) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3331_n410_timeruin_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3332) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3332_n410_timeruin_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3333) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3333_n410_timeruin_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3334) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3334_n410_timeruin_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3335) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3335_n410_timeruin_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3336) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3336_n410_timeruin_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3337) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3337_n410_timeruin_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3340) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3340_n410_timeruin_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3339) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3339_n410_timeruin_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_47", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3211) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3211_n410_ruin_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3212) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3212_n410_ruin_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3213) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3213_n410_ruin_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3214) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3214_n410_ruin_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3215) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3215_n410_ruin_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3216) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3216_n410_ruin_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3217) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3217_n410_ruin_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3220) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3220_n410_ruin_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3219) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3219_n410_ruin_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3241) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3241_n410_flood_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3242) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3242_n410_flood_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3243) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3243_n410_flood_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3244) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3244_n410_flood_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3245) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3245_n410_flood_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3246) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3246_n410_flood_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3247) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3247_n410_flood_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3250) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3250_n410_flood_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3249) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3249_n410_flood_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3271) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3271_n410_etomb_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3272) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3272_n410_etomb_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3273) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3273_n410_etomb_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3274) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3274_n410_etomb_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3275) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3275_n410_etomb_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3276) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3276_n410_etomb_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3277) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3277_n410_etomb_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3280) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3280_n410_etomb_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3279) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3279_n410_etomb_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3331) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3331_n410_timeruin_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3332) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3332_n410_timeruin_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3333) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3333_n410_timeruin_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3334) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3334_n410_timeruin_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3335) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3335_n410_timeruin_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3336) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3336_n410_timeruin_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3337) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3337_n410_timeruin_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3340) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3340_n410_timeruin_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3339) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3339_n410_timeruin_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_32" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_34" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_37" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n410_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
	elseif npc_talk_index == "stage_32" then
		if api_quest_UserHasQuest( pRoom, userObjID,3211) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3211_n410_ruin_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3212) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3212_n410_ruin_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3213) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3213_n410_ruin_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3214) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3214_n410_ruin_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3215) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3215_n410_ruin_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3216) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3216_n410_ruin_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3217) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3217_n410_ruin_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3220) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3220_n410_ruin_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3219) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3219_n410_ruin_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_32", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_34" then
		if api_quest_UserHasQuest( pRoom, userObjID,3241) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3241_n410_flood_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3242) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3242_n410_flood_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3243) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3243_n410_flood_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3244) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3244_n410_flood_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3245) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3245_n410_flood_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3246) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3246_n410_flood_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3247) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3247_n410_flood_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3250) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3250_n410_flood_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3249) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3249_n410_flood_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_34", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_47" then
		if api_quest_UserHasQuest( pRoom, userObjID,3331) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3331_n410_timeruin_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3332) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3332_n410_timeruin_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3333) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3333_n410_timeruin_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3334) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3334_n410_timeruin_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3335) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3335_n410_timeruin_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3336) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3336_n410_timeruin_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3337) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3337_n410_timeruin_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3340) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3340_n410_timeruin_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3339) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3339_n410_timeruin_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_47", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3211) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3211_n410_ruin_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3212) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3212_n410_ruin_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3213) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3213_n410_ruin_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3214) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3214_n410_ruin_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3215) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3215_n410_ruin_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3216) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3216_n410_ruin_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3217) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3217_n410_ruin_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3220) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3220_n410_ruin_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3219) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3219_n410_ruin_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3241) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3241_n410_flood_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3242) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3242_n410_flood_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3243) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3243_n410_flood_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3244) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3244_n410_flood_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3245) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3245_n410_flood_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3246) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3246_n410_flood_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3247) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3247_n410_flood_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3250) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3250_n410_flood_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3249) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3249_n410_flood_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3271) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3271_n410_etomb_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3272) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3272_n410_etomb_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3273) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3273_n410_etomb_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3274) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3274_n410_etomb_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3275) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3275_n410_etomb_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3276) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3276_n410_etomb_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3277) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3277_n410_etomb_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3280) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3280_n410_etomb_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3279) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3279_n410_etomb_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3331) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3331_n410_timeruin_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3332) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3332_n410_timeruin_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3333) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3333_n410_timeruin_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3334) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3334_n410_timeruin_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3335) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3335_n410_timeruin_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3336) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3336_n410_timeruin_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3337) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3337_n410_timeruin_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3340) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3340_n410_timeruin_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3339) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3339_n410_timeruin_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_32" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_34" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_37" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
