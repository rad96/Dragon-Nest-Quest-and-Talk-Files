<VillageServer>


function n001_elder_harold_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 26) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_001_lost_girl1.xml");
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 24) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 1) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 26) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_001_lost_girl1.xml");
		end

		end

		end

		else				
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "back_town_1" then
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "back_quest" then
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "party" then
		if api_user_GetPartymemberCount(userObjID) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "gogotuto", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "notalone", npc_talk_target);
		end

	elseif npc_talk_index == "back_notuto" then
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "back_notalone" then
		if api_user_GetUserClassID(userObjID) == 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "gogogo" then
		api_user_ChangeMap(userObjID,13511,1);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n001_elder_harold_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 26) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_001_lost_girl1.xml");
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 24) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 1) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 26) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_001_lost_girl1.xml");
		end

		end

		end

		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "back_town_1" then
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "back_quest" then
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "party" then
		if api_user_GetPartymemberCount( pRoom, userObjID) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "gogotuto", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "notalone", npc_talk_target);
		end

	elseif npc_talk_index == "back_notuto" then
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "back_notalone" then
		if api_user_GetUserClassID( pRoom, userObjID) == 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "gogogo" then
		api_user_ChangeMap( pRoom, userObjID,13511,1);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
