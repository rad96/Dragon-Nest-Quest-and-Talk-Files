<VillageServer>


function n1050_academic_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 8 then				
		if api_quest_UserHasQuest(userObjID,9554) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc10_9554_future_in_the_hand.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		else				
		if api_quest_UserHasQuest(userObjID,9256) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9256_decision_for_future.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9258) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9258_restart.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9386) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9386_shadow_for_you.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9387) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9387_choice_of_history.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1050_academic_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		if api_quest_UserHasQuest( pRoom, userObjID,9554) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc10_9554_future_in_the_hand.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9256) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9256_decision_for_future.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9258) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9258_restart.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9386) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9386_shadow_for_you.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9387) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9387_choice_of_history.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
