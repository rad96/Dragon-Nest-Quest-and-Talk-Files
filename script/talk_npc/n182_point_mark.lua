<VillageServer>


function n182_point_mark_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,202) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_202_rescue_girl.xml");
		else				
		if api_quest_UserHasQuest(userObjID,187) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_187_lavor_in_vain.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9469) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc02_9469_for_her.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9578) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc02_9578_rescue_a_girl.xml");
		else				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_202_rescue_girl.xml");
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n182_point_mark_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,202) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_202_rescue_girl.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,187) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_187_lavor_in_vain.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9469) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc02_9469_for_her.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9578) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc02_9578_rescue_a_girl.xml");
		else				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_202_rescue_girl.xml");
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
