<VillageServer>


function n1179_sorceress_karakule_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9341) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9341_crisscross.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9351) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9351_stray_sheep.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9352) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9352_i_need_you.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1179_sorceress_karakule_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9341) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9341_crisscross.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9351) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9351_stray_sheep.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9352) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9352_i_need_you.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
