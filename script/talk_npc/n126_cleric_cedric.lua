<VillageServer>


function n126_cleric_cedric_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 135) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 125) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_quest_ForceCompleteQuest(userObjID, 124, 0, 1, 1, 0);
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		api_quest_AddQuest(userObjID,135, 2);
		api_quest_SetQuestStep(userObjID, 135,1);
		api_quest_SetJournalStep(userObjID, 135, 1);
		else				
		api_quest_ForceCompleteQuest(userObjID, 123, 0, 1, 1, 0);
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		api_quest_AddQuest(userObjID,125, 2);
		api_quest_SetQuestStep(userObjID, 125,1);
		api_quest_SetJournalStep(userObjID, 125, 1);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n126_cleric_cedric_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 135) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 125) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_quest_ForceCompleteQuest( pRoom, userObjID, 124, 0, 1, 1, 0);
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		api_quest_AddQuest( pRoom, userObjID,135, 2);
		api_quest_SetQuestStep( pRoom, userObjID, 135,1);
		api_quest_SetJournalStep( pRoom, userObjID, 135, 1);
		else				
		api_quest_ForceCompleteQuest( pRoom, userObjID, 123, 0, 1, 1, 0);
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		api_quest_AddQuest( pRoom, userObjID,125, 2);
		api_quest_SetQuestStep( pRoom, userObjID, 125,1);
		api_quest_SetJournalStep( pRoom, userObjID, 125, 1);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
