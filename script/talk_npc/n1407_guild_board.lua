<VillageServer>


function n1407_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "altar", npc_talk_target);
	elseif npc_talk_index == "check_mist" then
		if api_quest_UserHasQuest(userObjID,3528) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3528_n1407_mist_ruin_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3529) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3529_n1407_mist_ruin_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3530) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3530_n1407_mist_ruin_repeat.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "mist_ruin", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_shadow" then
		if api_quest_UserHasQuest(userObjID,3538) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3538_n1407_shadow_tomb_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3539) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3539_n1407_shadow_tomb_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3540) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3540_n1407_shadow_tomb_repeat.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "shadow_tomb", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_flame_valley" then
		if api_quest_UserHasQuest(userObjID,3548) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3548_n1407_flame_valley_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3549) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3549_n1407_flame_valley_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3550) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3550_n1407_flame_valley_repeat.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "flame_valley", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_sirocco" then
		if api_quest_UserHasQuest(userObjID,3558) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3558_n1407_pit_of_sirocco_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3559) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3559_n1407_pit_of_sirocco_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3560) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3560_n1407_pit_of_sirocco_repeat.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "sirocco", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest(userObjID,3528) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3528_n1407_mist_ruin_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3529) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3529_n1407_mist_ruin_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3530) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3530_n1407_mist_ruin_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3538) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3538_n1407_shadow_tomb_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3539) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3539_n1407_shadow_tomb_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3540) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3540_n1407_shadow_tomb_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3548) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3548_n1407_flame_valley_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3549) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3549_n1407_flame_valley_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3550) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3550_n1407_flame_valley_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3558) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3558_n1407_pit_of_sirocco_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3559) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3559_n1407_pit_of_sirocco_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3560) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3560_n1407_pit_of_sirocco_repeat.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_002" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "tribute" then
		api_ui_OpenShop(userObjID,61001,100);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1407_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "altar", npc_talk_target);
	elseif npc_talk_index == "check_mist" then
		if api_quest_UserHasQuest( pRoom, userObjID,3528) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3528_n1407_mist_ruin_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3529) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3529_n1407_mist_ruin_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3530) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3530_n1407_mist_ruin_repeat.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "mist_ruin", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_shadow" then
		if api_quest_UserHasQuest( pRoom, userObjID,3538) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3538_n1407_shadow_tomb_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3539) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3539_n1407_shadow_tomb_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3540) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3540_n1407_shadow_tomb_repeat.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "shadow_tomb", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_flame_valley" then
		if api_quest_UserHasQuest( pRoom, userObjID,3548) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3548_n1407_flame_valley_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3549) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3549_n1407_flame_valley_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3550) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3550_n1407_flame_valley_repeat.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "flame_valley", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_sirocco" then
		if api_quest_UserHasQuest( pRoom, userObjID,3558) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3558_n1407_pit_of_sirocco_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3559) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3559_n1407_pit_of_sirocco_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3560) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3560_n1407_pit_of_sirocco_repeat.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "sirocco", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest( pRoom, userObjID,3528) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3528_n1407_mist_ruin_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3529) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3529_n1407_mist_ruin_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3530) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3530_n1407_mist_ruin_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3538) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3538_n1407_shadow_tomb_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3539) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3539_n1407_shadow_tomb_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3540) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3540_n1407_shadow_tomb_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3548) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3548_n1407_flame_valley_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3549) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3549_n1407_flame_valley_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3550) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3550_n1407_flame_valley_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3558) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3558_n1407_pit_of_sirocco_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3559) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3559_n1407_pit_of_sirocco_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3560) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3560_n1407_pit_of_sirocco_repeat.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_002" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "tribute" then
		api_ui_OpenShop( pRoom, userObjID,61001,100);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
