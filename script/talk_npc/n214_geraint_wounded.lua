<VillageServer>


function n214_geraint_wounded_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,408) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_UserHasQuest(userObjID,409) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_409_remainder_works.xml");
		else				
		if api_quest_UserHasQuest(userObjID,442) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_442_soaring_and_falling.xml");
		else				
		if api_quest_UserHasQuest(userObjID,443) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_443_settlement.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9311) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_9311_secret_area.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9310) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_9310_end_of_raide.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9487) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc04_9487_chasing_a_trail.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n214_geraint_wounded_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,408) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_408_soaring_black_arkmonarch.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,409) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_409_remainder_works.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,442) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_442_soaring_and_falling.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,443) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_443_settlement.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9311) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_9311_secret_area.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9310) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_9310_end_of_raide.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9487) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc04_9487_chasing_a_trail.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
