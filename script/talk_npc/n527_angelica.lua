<VillageServer>


function n527_angelica_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,394) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_394_deep_anxiety.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9498) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9498_persuade.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9610) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9610_impossible_to_convince.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n527_angelica_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,394) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_394_deep_anxiety.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9498) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9498_persuade.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9610) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9610_impossible_to_convince.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
