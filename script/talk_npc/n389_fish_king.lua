<VillageServer>


function n389_fish_king_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 668) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 4633) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_guide_4633_sea_fishing.xml");
		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "fish_king" then
		api_ui_OpenFishingRankingBoard(userObjID);
	elseif npc_talk_index == "next_reward" then
		api_ui_Open_FishRankingReward(userObjID);
	elseif npc_talk_index == "fish_shop" then
		api_ui_OpenShop(userObjID,45003,100);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n389_fish_king_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 668) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 4633) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_guide_4633_sea_fishing.xml");
		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "fish_king" then
		api_ui_OpenFishingRankingBoard( pRoom, userObjID);
	elseif npc_talk_index == "next_reward" then
		api_ui_Open_FishRankingReward( pRoom, userObjID);
	elseif npc_talk_index == "fish_shop" then
		api_ui_OpenShop( pRoom, userObjID,45003,100);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
