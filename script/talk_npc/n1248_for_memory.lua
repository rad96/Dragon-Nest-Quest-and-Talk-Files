<VillageServer>


function n1248_for_memory_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4426) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq34_4426_poisoning_2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4429) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq34_4429_illusion_of_memory_1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4430) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq34_4430_illusion_of_memory_2.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1248_for_memory_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4426) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq34_4426_poisoning_2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4429) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq34_4429_illusion_of_memory_1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4430) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq34_4430_illusion_of_memory_2.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
