<VillageServer>


function n056_guard_pesent_dalcy_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,8) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_008_whereabouts_girl2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,317) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq08_317_find_the_grandson2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,19) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq01_019_whereabouts_girl.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n056_guard_pesent_dalcy_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,8) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_008_whereabouts_girl2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,317) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq08_317_find_the_grandson2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,19) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq01_019_whereabouts_girl.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
