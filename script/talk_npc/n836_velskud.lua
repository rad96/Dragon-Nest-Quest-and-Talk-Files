<VillageServer>


function n836_velskud_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9262) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9262_moment_of_turning_point.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9257) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9257_turning_point_of_history.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4122) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4122_third_riddle.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9224) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9224_resurrection.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9222) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9222_fall_of_hero.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9373) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9373_twilight_of_the_past.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9377) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9377_farewell_once_more.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9387) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9387_choice_of_history.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9539) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9539_he_was_not.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9555) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc10_9555_terminus_of_the_future.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9656) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9656_again_st_haven.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n836_velskud_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9262) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9262_moment_of_turning_point.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9257) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9257_turning_point_of_history.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4122) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4122_third_riddle.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9224) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9224_resurrection.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9222) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9222_fall_of_hero.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9373) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9373_twilight_of_the_past.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9377) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9377_farewell_once_more.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9387) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9387_choice_of_history.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9539) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9539_he_was_not.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9555) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc10_9555_terminus_of_the_future.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9656) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9656_again_st_haven.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
