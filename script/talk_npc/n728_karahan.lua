<VillageServer>


function n728_karahan_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,722) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_722_regret.xml");
		else				
		if api_quest_UserHasQuest(userObjID,743) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_743_destiny.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9350) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9350_finding_lunaria.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9520) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc07_9520_feeling_sinister.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9636) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc07_9636_unbearable_anger.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n728_karahan_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,722) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_722_regret.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,743) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_743_destiny.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9350) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9350_finding_lunaria.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9520) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc07_9520_feeling_sinister.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9636) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc07_9636_unbearable_anger.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
