<VillageServer>


function n1015_academic_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4194) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4194_please_do_not.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4195) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4195_d_day.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9347) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9347_not_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9348) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9348_extinction_of_illusion.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1015_academic_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4194) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4194_please_do_not.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4195) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4195_d_day.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9347) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9347_not_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9348) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9348_extinction_of_illusion.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
