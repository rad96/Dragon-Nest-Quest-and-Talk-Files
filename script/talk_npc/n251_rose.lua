<VillageServer>


function n251_rose_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,413) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_413_dreaming_rose.xml");
		else				
		if api_quest_UserHasQuest(userObjID,411) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_411_rose_in_nightmare.xml");
		else				
		if api_quest_UserHasQuest(userObjID,443) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_443_settlement.xml");
		else				
		if api_quest_UserHasQuest(userObjID,385) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_385_rose_in_dream.xml");
		else				
		if api_quest_UserHasQuest(userObjID,396) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_396_last_hope.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9311) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_9311_secret_area.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9315) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_9315_in_the_dream.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9601) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc05_9601_dreaming_rose.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n251_rose_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,413) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_413_dreaming_rose.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,411) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_411_rose_in_nightmare.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,443) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_443_settlement.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,385) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_385_rose_in_dream.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,396) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_396_last_hope.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9311) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_9311_secret_area.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9315) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_9315_in_the_dream.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9601) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc05_9601_dreaming_rose.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
