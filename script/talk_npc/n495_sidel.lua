<VillageServer>


function n495_sidel_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,717) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_717_for_my_friend.xml");
		else				
		if api_quest_UserHasQuest(userObjID,738) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_738_tears.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9516) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9516_true_strength.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9629) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc07_9629_sad_friendship.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n495_sidel_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,717) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_717_for_my_friend.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,738) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_738_tears.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9516) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9516_true_strength.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9629) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc07_9629_sad_friendship.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
