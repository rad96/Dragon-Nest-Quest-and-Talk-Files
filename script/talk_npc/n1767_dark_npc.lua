<VillageServer>


function n1767_dark_npc_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserJobID(userObjID) == 75 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		api_user_SetSecondJobSkill( userObjID, 76 );
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "dark_03" then
		api_quest_ForceCompleteQuest(userObjID, 998, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 238, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 340, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 755, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 756, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 757, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest(userObjID, 758, 0, 1, 1, 0);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1767_dark_npc_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserJobID( pRoom, userObjID) == 75 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		api_user_SetSecondJobSkill( pRoom,  userObjID, 76 );
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "dark_03" then
		api_quest_ForceCompleteQuest( pRoom, userObjID, 998, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 238, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 340, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 755, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 756, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 757, 0, 1, 1, 0);
		api_quest_ForceCompleteQuest( pRoom, userObjID, 758, 0, 1, 1, 0);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
