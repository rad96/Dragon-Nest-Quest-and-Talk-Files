<VillageServer>


function n027_argenta_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,203) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_203_black_knight_tracking.xml");
		else				
		if api_quest_UserHasQuest(userObjID,206) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_206_surprise_attack.xml");
		else				
		if api_quest_UserHasQuest(userObjID,204) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_204_swordwoman_with_siverblond.xml");
		else				
		if api_quest_UserHasQuest(userObjID,224) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_224_she_meets.xml");
		else				
		if api_quest_UserHasQuest(userObjID,223) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_223_work_of_someone.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9470) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc02_9470_black_knight.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9472) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc02_9472_conflict.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9579) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc02_9579_mysterious_silver_hair.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n027_argenta_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,203) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_203_black_knight_tracking.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,206) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_206_surprise_attack.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,204) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_204_swordwoman_with_siverblond.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,224) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_224_she_meets.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,223) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_223_work_of_someone.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9470) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc02_9470_black_knight.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9472) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc02_9472_conflict.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9579) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc02_9579_mysterious_silver_hair.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
