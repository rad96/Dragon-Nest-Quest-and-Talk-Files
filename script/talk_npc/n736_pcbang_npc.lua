<VillageServer>


function n736_pcbang_npc_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetPCCafe(userObjID) >= 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_user_GetPCCafe(userObjID) == 0 then				
		api_npc_NextTalk(userObjID, npcObjID, "nopcbang", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "noplayer", npc_talk_target);
		end

		end

	elseif npc_talk_index == "get_pc_item" then
		if api_user_SetPCCafeItem(userObjID) == 0 then				
		api_npc_NextTalk(userObjID, npcObjID, "set_pc_0", npc_talk_target);
		else				
		if api_user_SetPCCafeItem(userObjID) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "nopcbang", npc_talk_target);
		else				
		if api_user_SetPCCafeItem(userObjID) == 2 then				
		api_npc_NextTalk(userObjID, npcObjID, "set_pc_2", npc_talk_target);
		else				
		if api_user_SetPCCafeItem(userObjID) == 3 then				
		api_npc_NextTalk(userObjID, npcObjID, "set_pc_3", npc_talk_target);
		else				
		if api_user_SetPCCafeItem(userObjID) == 4 then				
		api_npc_NextTalk(userObjID, npcObjID, "set_pc_4", npc_talk_target);
		else				
		if api_user_SetPCCafeItem(userObjID) == 5 then				
		api_npc_NextTalk(userObjID, npcObjID, "set_pc_5", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "noplayer", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n736_pcbang_npc_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetPCCafe( pRoom, userObjID) >= 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_user_GetPCCafe( pRoom, userObjID) == 0 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "nopcbang", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "noplayer", npc_talk_target);
		end

		end

	elseif npc_talk_index == "get_pc_item" then
		if api_user_SetPCCafeItem( pRoom, userObjID) == 0 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "set_pc_0", npc_talk_target);
		else				
		if api_user_SetPCCafeItem( pRoom, userObjID) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "nopcbang", npc_talk_target);
		else				
		if api_user_SetPCCafeItem( pRoom, userObjID) == 2 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "set_pc_2", npc_talk_target);
		else				
		if api_user_SetPCCafeItem( pRoom, userObjID) == 3 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "set_pc_3", npc_talk_target);
		else				
		if api_user_SetPCCafeItem( pRoom, userObjID) == 4 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "set_pc_4", npc_talk_target);
		else				
		if api_user_SetPCCafeItem( pRoom, userObjID) == 5 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "set_pc_5", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "noplayer", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
