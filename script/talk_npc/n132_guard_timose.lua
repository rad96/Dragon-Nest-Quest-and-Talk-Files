<VillageServer>
function n132_guard_timose_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,5) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq005_black_emblem2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4) > -1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest4", npc_talk_target);
		else				
		if api_quest_UserHasQuest(userObjID,3) > -1 then				
		api_npc_NextTalk(userObjID, npcObjID, "quest4", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</VillageServer>

<GameServer>
function n132_guard_timose_OnTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(pRoom, userObjID,5) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","mq005_black_emblem2.xml");
		else				
		if api_quest_UserHasQuest(pRoom, userObjID,4) > -1 then				
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "quest4", npc_talk_target);
		else				
		if api_quest_UserHasQuest(pRoom, userObjID,3) > -1 then				
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "quest4", npc_talk_target);
		else				
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</GameServer>
