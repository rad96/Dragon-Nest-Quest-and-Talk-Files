<VillageServer>


function n409_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
	elseif npc_talk_index == "stage_27" then
		if api_quest_UserHasQuest(userObjID,3161) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3161_n409_mine_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3162) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3162_n409_mine_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3163) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3163_n409_mine_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3164) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3164_n409_mine_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3165) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3165_n409_mine_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3166) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3166_n409_mine_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3167) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3167_n409_mine_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3170) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3170_n409_mine_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3169) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3169_n409_mine_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_27", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_29" then
		if api_quest_UserHasQuest(userObjID,3201) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3201_n409_sink_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3202) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3202_n409_sink_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3203) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3203_n409_sink_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3204) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3204_n409_sink_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3205) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3205_n409_sink_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3206) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3206_n409_sink_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3207) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3207_n409_sink_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3210) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3210_n409_sink_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3209) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3209_n409_sink_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_29", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_38" then
		if api_quest_UserHasQuest(userObjID,3281) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3281_n409_cityfront_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3282) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3282_n409_cityfront_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3283) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3283_n409_cityfront_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3284) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3284_n409_cityfront_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3285) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3285_n409_cityfront_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3286) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3286_n409_cityfront_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3287) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3287_n409_cityfront_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3290) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3290_n409_cityfront_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3289) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3289_n409_cityfront_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_38", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_42" then
		if api_quest_UserHasQuest(userObjID,3321) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3321_n409_citygate_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3322) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3322_n409_citygate_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3323) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3323_n409_citygate_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3324) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3324_n409_citygate_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3325) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3325_n409_citygate_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3326) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3326_n409_citygate_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3327) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3327_n409_citygate_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3330) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3330_n409_citygate_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3329) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3329_n409_citygate_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_42", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_44" then
		if api_quest_UserHasQuest(userObjID,3351) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3351_n409_ntomb_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3352) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3352_n409_ntomb_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3353) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3353_n409_ntomb_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3354) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3354_n409_ntomb_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3355) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3355_n409_ntomb_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3356) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3356_n409_ntomb_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3357) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3357_n409_ntomb_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3360) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3360_n409_ntomb_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3359) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3359_n409_ntomb_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_44", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest(userObjID,3161) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3161_n409_mine_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3162) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3162_n409_mine_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3163) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3163_n409_mine_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3164) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3164_n409_mine_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3165) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3165_n409_mine_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3166) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3166_n409_mine_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3167) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3167_n409_mine_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3170) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3170_n409_mine_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3169) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3169_n409_mine_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3201) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3201_n409_sink_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3202) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3202_n409_sink_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3203) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3203_n409_sink_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3204) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3204_n409_sink_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3205) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3205_n409_sink_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3206) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3206_n409_sink_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3207) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3207_n409_sink_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3210) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3210_n409_sink_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3209) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3209_n409_sink_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3281) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3281_n409_cityfront_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3282) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3282_n409_cityfront_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3283) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3283_n409_cityfront_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3284) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3284_n409_cityfront_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3285) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3285_n409_cityfront_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3286) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3286_n409_cityfront_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3287) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3287_n409_cityfront_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3290) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3290_n409_cityfront_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3289) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3289_n409_cityfront_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3321) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3321_n409_citygate_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3322) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3322_n409_citygate_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3323) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3323_n409_citygate_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3324) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3324_n409_citygate_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3325) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3325_n409_citygate_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3326) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3326_n409_citygate_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3327) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3327_n409_citygate_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3330) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3330_n409_citygate_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3329) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3329_n409_citygate_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3351) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3351_n409_ntomb_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3352) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3352_n409_ntomb_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3353) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3353_n409_ntomb_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3354) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3354_n409_ntomb_hard.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3355) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3355_n409_ntomb_boss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3356) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3356_n409_ntomb_prop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3357) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3357_n409_ntomb_master.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3360) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3360_n409_ntomb_rank.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3359) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq11_3359_n409_ntomb_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_27" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_29" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_38" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_42" then
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n409_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
	elseif npc_talk_index == "stage_27" then
		if api_quest_UserHasQuest( pRoom, userObjID,3161) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3161_n409_mine_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3162) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3162_n409_mine_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3163) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3163_n409_mine_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3164) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3164_n409_mine_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3165) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3165_n409_mine_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3166) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3166_n409_mine_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3167) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3167_n409_mine_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3170) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3170_n409_mine_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3169) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3169_n409_mine_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_27", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_29" then
		if api_quest_UserHasQuest( pRoom, userObjID,3201) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3201_n409_sink_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3202) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3202_n409_sink_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3203) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3203_n409_sink_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3204) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3204_n409_sink_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3205) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3205_n409_sink_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3206) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3206_n409_sink_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3207) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3207_n409_sink_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3210) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3210_n409_sink_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3209) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3209_n409_sink_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_29", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_38" then
		if api_quest_UserHasQuest( pRoom, userObjID,3281) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3281_n409_cityfront_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3282) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3282_n409_cityfront_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3283) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3283_n409_cityfront_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3284) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3284_n409_cityfront_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3285) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3285_n409_cityfront_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3286) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3286_n409_cityfront_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3287) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3287_n409_cityfront_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3290) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3290_n409_cityfront_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3289) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3289_n409_cityfront_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_38", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_42" then
		if api_quest_UserHasQuest( pRoom, userObjID,3321) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3321_n409_citygate_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3322) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3322_n409_citygate_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3323) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3323_n409_citygate_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3324) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3324_n409_citygate_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3325) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3325_n409_citygate_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3326) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3326_n409_citygate_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3327) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3327_n409_citygate_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3330) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3330_n409_citygate_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3329) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3329_n409_citygate_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_42", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "stage_44" then
		if api_quest_UserHasQuest( pRoom, userObjID,3351) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3351_n409_ntomb_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3352) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3352_n409_ntomb_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3353) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3353_n409_ntomb_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3354) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3354_n409_ntomb_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3355) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3355_n409_ntomb_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3356) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3356_n409_ntomb_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3357) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3357_n409_ntomb_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3360) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3360_n409_ntomb_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3359) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3359_n409_ntomb_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_44", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "002" then
		if api_quest_UserHasQuest( pRoom, userObjID,3161) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3161_n409_mine_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3162) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3162_n409_mine_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3163) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3163_n409_mine_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3164) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3164_n409_mine_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3165) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3165_n409_mine_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3166) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3166_n409_mine_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3167) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3167_n409_mine_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3170) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3170_n409_mine_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3169) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3169_n409_mine_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3201) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3201_n409_sink_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3202) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3202_n409_sink_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3203) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3203_n409_sink_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3204) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3204_n409_sink_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3205) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3205_n409_sink_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3206) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3206_n409_sink_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3207) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3207_n409_sink_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3210) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3210_n409_sink_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3209) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3209_n409_sink_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3281) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3281_n409_cityfront_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3282) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3282_n409_cityfront_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3283) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3283_n409_cityfront_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3284) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3284_n409_cityfront_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3285) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3285_n409_cityfront_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3286) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3286_n409_cityfront_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3287) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3287_n409_cityfront_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3290) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3290_n409_cityfront_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3289) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3289_n409_cityfront_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3321) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3321_n409_citygate_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3322) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3322_n409_citygate_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3323) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3323_n409_citygate_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3324) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3324_n409_citygate_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3325) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3325_n409_citygate_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3326) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3326_n409_citygate_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3327) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3327_n409_citygate_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3330) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3330_n409_citygate_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3329) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3329_n409_citygate_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3351) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3351_n409_ntomb_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3352) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3352_n409_ntomb_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3353) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3353_n409_ntomb_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3354) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3354_n409_ntomb_hard.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3355) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3355_n409_ntomb_boss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3356) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3356_n409_ntomb_prop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3357) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3357_n409_ntomb_master.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3360) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3360_n409_ntomb_rank.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3359) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq11_3359_n409_ntomb_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_stage_27" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_29" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_38" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	elseif npc_talk_index == "back_stage_42" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_no_quest", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
