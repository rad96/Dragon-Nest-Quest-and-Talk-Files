<VillageServer>


function n1404_guild_board_new_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "stage_1" then
		if api_quest_UserHasQuest(userObjID,3041) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3041_n406_forest_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3042) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3042_n406_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3043) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3043_n406_forest_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3044) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3044_n406_forest_hard.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_1", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_2" then
		if api_quest_UserHasQuest(userObjID,3061) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3061_n407_sleep_normal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3062) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3062_n407_sleep_hunt.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3063) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3063_n407_sleep_drop.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3064) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq08_3064_n407_sleep_hard.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_2", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_1" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back_2" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1404_guild_board_new_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "stage_1" then
		if api_quest_UserHasQuest( pRoom, userObjID,3041) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3041_n406_forest_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3042) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3042_n406_forest_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3043) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3043_n406_forest_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3044) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3044_n406_forest_hard.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_1", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "stage_2" then
		if api_quest_UserHasQuest( pRoom, userObjID,3061) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3061_n407_sleep_normal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3062) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3062_n407_sleep_hunt.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3063) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3063_n407_sleep_drop.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3064) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq08_3064_n407_sleep_hard.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_2", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_1" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "back_2" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
