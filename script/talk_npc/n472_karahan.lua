<VillageServer>


function n472_karahan_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9337) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9337_the_sibling.xml");
		else				
		if api_quest_UserHasQuest(userObjID,759) > -1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_UserHasQuest(userObjID,705) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_705_doubtful_proposal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,759) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_759_real_cause.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n472_karahan_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9337) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9337_the_sibling.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,759) > -1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,705) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_705_doubtful_proposal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,759) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_759_real_cause.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
