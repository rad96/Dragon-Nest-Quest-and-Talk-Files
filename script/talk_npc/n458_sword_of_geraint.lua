<VillageServer>


function n458_sword_of_geraint_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,757) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_757_stronger_powers.xml");
		else				
		if api_user_GetPartymemberCount(userObjID) == 1 then				
		if api_quest_UserHasQuest(userObjID,756) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_756_turn_defeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,758) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_758_ordeal_and_answer.xml");
		else				
		if api_quest_UserHasQuest(userObjID,754) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_754_as_you_wish.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4688) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_changejob_4688_illusion_leading_to_light.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4689) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq_changejob_4689_dark_red.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "solo", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n458_sword_of_geraint_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,757) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_757_stronger_powers.xml");
		else				
		if api_user_GetPartymemberCount( pRoom, userObjID) == 1 then				
		if api_quest_UserHasQuest( pRoom, userObjID,756) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_756_turn_defeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,758) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_758_ordeal_and_answer.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,754) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_754_as_you_wish.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4688) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_changejob_4688_illusion_leading_to_light.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4689) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq_changejob_4689_dark_red.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "solo", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
