<VillageServer>


function n418_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "check_golden_plains" then
		if api_quest_UserHasQuest(userObjID,3498) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3498_n418_golden_plains_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3499) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3499_n418_golden_plains_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3500) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3500_n418_golden_plains_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_golden_plains", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_bronze_dark_moon" then
		if api_quest_UserHasQuest(userObjID,3508) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3508_n418_bronze_dark_moon_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3509) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3509_n418_bronze_dark_moon_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3510) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3510_n418_bronze_dark_moon_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_bronze_dark_moon", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_valley_of_eclipse" then
		if api_quest_UserHasQuest(userObjID,3518) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3518_n418_valley_of_eclipse_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3519) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3519_n418_valley_of_eclipse_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3520) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3520_n418_valley_of_eclipse_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_valley_of_eclipse", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest(userObjID,3498) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3498_n418_golden_plains_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3499) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3499_n418_golden_plains_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3500) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3500_n418_golden_plains_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3508) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3508_n418_bronze_dark_moon_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3509) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3509_n418_bronze_dark_moon_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3510) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3510_n418_bronze_dark_moon_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3518) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3518_n418_valley_of_eclipse_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3519) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3519_n418_valley_of_eclipse_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3520) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3520_n418_valley_of_eclipse_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n418_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "check_golden_plains" then
		if api_quest_UserHasQuest( pRoom, userObjID,3498) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3498_n418_golden_plains_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3499) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3499_n418_golden_plains_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3500) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3500_n418_golden_plains_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_golden_plains", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_bronze_dark_moon" then
		if api_quest_UserHasQuest( pRoom, userObjID,3508) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3508_n418_bronze_dark_moon_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3509) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3509_n418_bronze_dark_moon_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3510) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3510_n418_bronze_dark_moon_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_bronze_dark_moon", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_valley_of_eclipse" then
		if api_quest_UserHasQuest( pRoom, userObjID,3518) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3518_n418_valley_of_eclipse_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3519) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3519_n418_valley_of_eclipse_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3520) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3520_n418_valley_of_eclipse_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_valley_of_eclipse", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest( pRoom, userObjID,3498) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3498_n418_golden_plains_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3499) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3499_n418_golden_plains_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3500) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3500_n418_golden_plains_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3508) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3508_n418_bronze_dark_moon_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3509) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3509_n418_bronze_dark_moon_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3510) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3510_n418_bronze_dark_moon_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3518) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3518_n418_valley_of_eclipse_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3519) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3519_n418_valley_of_eclipse_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3520) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3520_n418_valley_of_eclipse_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
