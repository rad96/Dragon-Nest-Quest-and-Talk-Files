<VillageServer>


function n1610_royal_magician_kalaen_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4561) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc14_4561_tamara_curiositya.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4570) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc14_4570_viva.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4574) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc14_4574_conflict1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4576) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc14_4576_unexpected_development.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4577) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc14_4577_integration_of_the_prognostic.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9427) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9427_invasion.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1610_royal_magician_kalaen_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4561) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc14_4561_tamara_curiositya.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4570) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc14_4570_viva.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4574) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc14_4574_conflict1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4576) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc14_4576_unexpected_development.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4577) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc14_4577_integration_of_the_prognostic.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9427) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9427_invasion.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
