<VillageServer>


function n1408_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "altar", npc_talk_target);
	elseif npc_talk_index == "check_tower" then
		if api_quest_UserHasQuest(userObjID,3568) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3568_n1408_beholder_tower_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3569) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3569_n1408_beholder_tower_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3570) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3570_n1408_beholder_tower_repeat.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "beholder_tower", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_sorrow" then
		if api_quest_UserHasQuest(userObjID,3578) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3578_n1408_wall_of_sorrow_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3579) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3579_n1408_wall_of_sorrow_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3580) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3580_n1408_wall_of_sorrow_repeat.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "wall_of_sorrow", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_goddess" then
		if api_quest_UserHasQuest(userObjID,3588) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3588_n1408_goddess_eye_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3589) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3589_n1408_goddess_eye_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3590) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3590_n1408_goddess_eye_repeat.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "goddess_eye", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_praying" then
		if api_quest_UserHasQuest(userObjID,3598) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3598_n1408_praying_heart_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3599) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3599_n1408_praying_heart_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3600) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3600_n1408_praying_heart_repeat.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "praying_heart", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest(userObjID,3568) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3568_n1408_beholder_tower_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3569) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3569_n1408_beholder_tower_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3570) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3570_n1408_beholder_tower_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3578) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3578_n1408_wall_of_sorrow_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3579) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3579_n1408_wall_of_sorrow_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3580) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3580_n1408_wall_of_sorrow_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3588) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3588_n1408_goddess_eye_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3589) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3589_n1408_goddess_eye_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3590) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3590_n1408_goddess_eye_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3598) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3598_n1408_praying_heart_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3599) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3599_n1408_praying_heart_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3600) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gqc14_3600_n1408_praying_heart_repeat.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_002" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "tribute" then
		api_ui_OpenShop(userObjID,61001,100);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1408_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "altar", npc_talk_target);
	elseif npc_talk_index == "check_tower" then
		if api_quest_UserHasQuest( pRoom, userObjID,3568) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3568_n1408_beholder_tower_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3569) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3569_n1408_beholder_tower_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3570) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3570_n1408_beholder_tower_repeat.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "beholder_tower", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_sorrow" then
		if api_quest_UserHasQuest( pRoom, userObjID,3578) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3578_n1408_wall_of_sorrow_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3579) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3579_n1408_wall_of_sorrow_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3580) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3580_n1408_wall_of_sorrow_repeat.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "wall_of_sorrow", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_goddess" then
		if api_quest_UserHasQuest( pRoom, userObjID,3588) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3588_n1408_goddess_eye_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3589) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3589_n1408_goddess_eye_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3590) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3590_n1408_goddess_eye_repeat.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "goddess_eye", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_praying" then
		if api_quest_UserHasQuest( pRoom, userObjID,3598) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3598_n1408_praying_heart_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3599) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3599_n1408_praying_heart_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3600) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3600_n1408_praying_heart_repeat.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "praying_heart", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest( pRoom, userObjID,3568) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3568_n1408_beholder_tower_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3569) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3569_n1408_beholder_tower_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3570) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3570_n1408_beholder_tower_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3578) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3578_n1408_wall_of_sorrow_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3579) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3579_n1408_wall_of_sorrow_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3580) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3580_n1408_wall_of_sorrow_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3588) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3588_n1408_goddess_eye_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3589) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3589_n1408_goddess_eye_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3590) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3590_n1408_goddess_eye_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3598) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3598_n1408_praying_heart_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3599) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3599_n1408_praying_heart_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3600) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gqc14_3600_n1408_praying_heart_repeat.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_002" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "tribute" then
		api_ui_OpenShop( pRoom, userObjID,61001,100);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
