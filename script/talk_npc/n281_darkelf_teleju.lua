<VillageServer>


function n281_darkelf_teleju_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,510) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_510_darkelf_teleju6.xml");
		else				
		if api_quest_UserHasQuest(userObjID,626) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_626_meet_teleju_again.xml");
		else				
		if api_quest_UserHasQuest(userObjID,627) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_627_lost_crown.xml");
		else				
		if api_quest_UserHasQuest(userObjID,628) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_628_urgent_message.xml");
		else				
		if api_quest_UserHasQuest(userObjID,629) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_629_retreat_teleju.xml");
		else				
		if api_quest_UserHasQuest(userObjID,630) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_630_determination.xml");
		else				
		if api_quest_UserHasQuest(userObjID,631) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_631_scepter_of_shadow.xml");
		else				
		if api_quest_UserHasQuest(userObjID,632) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_632_far_away_kingdom.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 510) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n281_darkelf_teleju_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,510) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_510_darkelf_teleju6.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,626) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_626_meet_teleju_again.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,627) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_627_lost_crown.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,628) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_628_urgent_message.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,629) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_629_retreat_teleju.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,630) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_630_determination.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,631) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_631_scepter_of_shadow.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,632) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_632_far_away_kingdom.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 510) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
