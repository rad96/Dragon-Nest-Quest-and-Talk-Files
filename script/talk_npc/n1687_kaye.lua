<VillageServer>


function n1687_kaye_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 8 then				
		if api_quest_UserHasQuest(userObjID,9461) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc01_9461_proof.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9463) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc01_9463_orb_to_find.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9468) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc02_9468_cataract.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1687_kaye_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		if api_quest_UserHasQuest( pRoom, userObjID,9461) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc01_9461_proof.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9463) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc01_9463_orb_to_find.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9468) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc02_9468_cataract.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
