<VillageServer>


function n415_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "stage_stage_51" then
		if api_quest_UserHasQuest(userObjID,3384) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3384_n415_riverwart_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3389) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3389_n415_riverwart_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3390) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3390_n415_riverwart_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_51", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_stage_54" then
		if api_quest_UserHasQuest(userObjID,3404) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3404_n415_dragonsycophant_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3409) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3409_n415_dragonsycophant_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3410) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3410_n415_dragonsycophant_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "stage_54", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest(userObjID,3384) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3384_n415_riverwart_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3389) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3389_n415_riverwart_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3390) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3390_n415_riverwart_hidden.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3404) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3404_n415_dragonsycophant_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3409) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3409_n415_dragonsycophant_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3410) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq15_3410_n415_dragonsycophant_hidden.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n415_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "stage_stage_51" then
		if api_quest_UserHasQuest( pRoom, userObjID,3384) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3384_n415_riverwart_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3389) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3389_n415_riverwart_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3390) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3390_n415_riverwart_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_51", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_stage_54" then
		if api_quest_UserHasQuest( pRoom, userObjID,3404) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3404_n415_dragonsycophant_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3409) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3409_n415_dragonsycophant_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3410) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3410_n415_dragonsycophant_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "stage_54", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest( pRoom, userObjID,3384) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3384_n415_riverwart_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3389) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3389_n415_riverwart_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3390) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3390_n415_riverwart_hidden.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3404) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3404_n415_dragonsycophant_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3409) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3409_n415_dragonsycophant_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3410) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq15_3410_n415_dragonsycophant_hidden.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
