<VillageServer>
function n280_saint_guard_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,555) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_555_timid_man1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,556) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_556_timid_man2.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</VillageServer>

<GameServer>
function n280_saint_guard_OnTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(pRoom, userObjID,555) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq11_555_timid_man1.xml");
		else				
		if api_quest_UserHasQuest(pRoom, userObjID,556) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq11_556_timid_man2.xml");
		else				
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</GameServer>
