<VillageServer>


function n831_yuvenciel_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9216) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9216_unreliable_informer.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9370) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9370_testimony.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9535) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc09_9535_weird_elf.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n831_yuvenciel_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9216) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9216_unreliable_informer.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9370) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9370_testimony.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9535) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc09_9535_weird_elf.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
