<VillageServer>


function n011_priest_leonardo_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "back_quest" then
		if api_user_GetUserClassID(userObjID) == 4 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 123) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 101) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_101_lost_sacred_object1.xml");
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "backtuto" then
		if api_user_GetPartymemberCount(userObjID) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "gotuto", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "notalone", npc_talk_target);
		end

	elseif npc_talk_index == "back_tutoend" then
		if api_user_GetUserClassID(userObjID) == 4 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 123) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 101) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_101_lost_sacred_object1.xml");
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "gogo" then
		api_user_ChangeMap(userObjID,13511,1);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n011_priest_leonardo_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "back_quest" then
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 123) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 101) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_101_lost_sacred_object1.xml");
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "backtuto" then
		if api_user_GetPartymemberCount( pRoom, userObjID) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "gotuto", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "notalone", npc_talk_target);
		end

	elseif npc_talk_index == "back_tutoend" then
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 123) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 101) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_101_lost_sacred_object1.xml");
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "gogo" then
		api_user_ChangeMap( pRoom, userObjID,13511,1);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
