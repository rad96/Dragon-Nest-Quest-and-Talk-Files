<VillageServer>


function n087_beggar_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_talk" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( userObjID, 87 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( userObjID, 87 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_30" then
		api_ui_OpenGiveNpcPresent( userObjID, 87 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( userObjID, 87 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( userObjID, 87 );
		api_npc_NextTalk(userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_gift" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_start" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 87 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "get_alt_item" then
		if api_user_SetAlteiaWorldItem(userObjID) == 0 then				
		api_npc_NextTalk(userObjID, npcObjID, "set_alt_0", npc_talk_target);
		else				
		if api_user_SetAlteiaWorldItem(userObjID) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "noclear", npc_talk_target);
		else				
		if api_user_SetAlteiaWorldItem(userObjID) == 2 then				
		api_npc_NextTalk(userObjID, npcObjID, "set_alt_2", npc_talk_target);
		else				
		if api_user_SetAlteiaWorldItem(userObjID) == 3 then				
		api_npc_NextTalk(userObjID, npcObjID, "set_alt_3", npc_talk_target);
		else				
		if api_user_SetAlteiaWorldItem(userObjID) == 4 then				
		api_npc_NextTalk(userObjID, npcObjID, "set_alt_4", npc_talk_target);
		else				
		if api_user_SetAlteiaWorldItem(userObjID) == 5 then				
		api_npc_NextTalk(userObjID, npcObjID, "set_alt_5", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "noclear", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n087_beggar_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_talk" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 87 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 87 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_30" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 87 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 87 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( pRoom,  userObjID, 87 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_gift" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_start" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 8101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 87 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "get_alt_item" then
		if api_user_SetAlteiaWorldItem( pRoom, userObjID) == 0 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "set_alt_0", npc_talk_target);
		else				
		if api_user_SetAlteiaWorldItem( pRoom, userObjID) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "noclear", npc_talk_target);
		else				
		if api_user_SetAlteiaWorldItem( pRoom, userObjID) == 2 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "set_alt_2", npc_talk_target);
		else				
		if api_user_SetAlteiaWorldItem( pRoom, userObjID) == 3 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "set_alt_3", npc_talk_target);
		else				
		if api_user_SetAlteiaWorldItem( pRoom, userObjID) == 4 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "set_alt_4", npc_talk_target);
		else				
		if api_user_SetAlteiaWorldItem( pRoom, userObjID) == 5 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "set_alt_5", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "noclear", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
