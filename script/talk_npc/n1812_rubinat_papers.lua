<VillageServer>


function n1812_rubinat_papers_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9703) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc17_9703_disappeared_of_dragon.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "back" then
		if api_quest_UserHasQuest(userObjID,9703) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc17_9703_disappeared_of_dragon.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1812_rubinat_papers_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9703) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc17_9703_disappeared_of_dragon.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "back" then
		if api_quest_UserHasQuest( pRoom, userObjID,9703) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc17_9703_disappeared_of_dragon.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
