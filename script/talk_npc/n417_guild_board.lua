<VillageServer>


function n417_guild_board_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "check_sanddust" then
		if api_quest_UserHasQuest(userObjID,3478) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3478_n417_sanddust_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3479) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3479_n417_sanddust_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3480) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3480_n417_sanddust_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_sanddust", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_vanishing_crescent" then
		if api_quest_UserHasQuest(userObjID,3488) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3488_n417_vanishing_crescent_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3489) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3489_n417_vanishing_crescent_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3490) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3490_n417_vanishing_crescent_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "quest_vanishing_crescent", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest(userObjID,3478) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3478_n417_sanddust_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3479) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3479_n417_sanddust_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3480) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3480_n417_sanddust_abyss.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3488) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3488_n417_vanishing_crescent_repeat.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3489) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3489_n417_vanishing_crescent_party.xml");
		else				
		if api_quest_UserHasQuest(userObjID,3490) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","gq34_3490_n417_vanishing_crescent_abyss.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n417_guild_board_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "_noquest", npc_talk_target);
	elseif npc_talk_index == "check_sanddust" then
		if api_quest_UserHasQuest( pRoom, userObjID,3478) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3478_n417_sanddust_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3479) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3479_n417_sanddust_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3480) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3480_n417_sanddust_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_sanddust", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_vanishing_crescent" then
		if api_quest_UserHasQuest( pRoom, userObjID,3488) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3488_n417_vanishing_crescent_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3489) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3489_n417_vanishing_crescent_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3490) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3490_n417_vanishing_crescent_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "quest_vanishing_crescent", npc_talk_target);
		end

		end

		end

	elseif npc_talk_index == "check_all" then
		if api_quest_UserHasQuest( pRoom, userObjID,3478) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3478_n417_sanddust_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3479) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3479_n417_sanddust_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3480) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3480_n417_sanddust_abyss.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3488) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3488_n417_vanishing_crescent_repeat.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3489) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3489_n417_vanishing_crescent_party.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,3490) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","gq34_3490_n417_vanishing_crescent_abyss.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
