<VillageServer>


function n872_academic_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9225) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9225_greed_flame.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9227) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9227_dejavu.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9378) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9378_coincident_opportunity.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9380) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9380_but_not_equal.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9379) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_9379_overlap.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9546) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc10_9546_witnessed_murder_doubt.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n872_academic_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9225) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9225_greed_flame.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9227) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9227_dejavu.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9378) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9378_coincident_opportunity.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9380) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9380_but_not_equal.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9379) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_9379_overlap.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9546) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc10_9546_witnessed_murder_doubt.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
