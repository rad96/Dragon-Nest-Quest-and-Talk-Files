<VillageServer>


function n117_trainer_lindsay_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,8606) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","nq32_8606_good_glass.xml");
		else				
		if api_quest_UserHasQuest(userObjID,8605) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","nq32_8605_picnic_together.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n117_trainer_lindsay_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,8606) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","nq32_8606_good_glass.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,8605) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","nq32_8605_picnic_together.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
