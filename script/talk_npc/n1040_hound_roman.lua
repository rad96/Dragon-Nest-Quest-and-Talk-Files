<VillageServer>


function n1040_hound_roman_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4291) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4291_the_duck_in_timewarp.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4292) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4292_memory_for_you.xml");
		else				
		if api_quest_UserHasQuest(userObjID,4295) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4295_vision_of_past.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1040_hound_roman_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4291) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4291_the_duck_in_timewarp.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4292) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4292_memory_for_you.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,4295) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4295_vision_of_past.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
