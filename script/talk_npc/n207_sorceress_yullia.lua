<VillageServer>


function n207_sorceress_yullia_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,115) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_115_orb_of_vision3.xml");
		else				
		if api_quest_UserHasQuest(userObjID,139) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_139_orb_of_vision2.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n207_sorceress_yullia_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,115) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_115_orb_of_vision3.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,139) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_139_orb_of_vision2.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
