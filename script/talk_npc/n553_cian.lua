<VillageServer>


function n553_cian_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,231) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_231_destroy_the_target.xml");
		else				
		if api_quest_UserHasQuest(userObjID,232) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_232_ucan_be_strong.xml");
		else				
		if api_quest_UserHasQuest(userObjID,195) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_195_aggravate_error.xml");
		else				
		if api_quest_UserHasQuest(userObjID,196) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq08_196_stronghold.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n553_cian_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,231) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_231_destroy_the_target.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,232) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_232_ucan_be_strong.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,195) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_195_aggravate_error.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,196) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq08_196_stronghold.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
