<VillageServer>


function n711_warrior_fedro_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		end

		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "skill" then
		api_ui_OpenSkillShop(userObjID);
	elseif npc_talk_index == "back_quest" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		end

		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "skill_90" then
		api_ui_OpenSkillShop(userObjID);
	elseif npc_talk_index == "present_90_wor" then
		api_ui_OpenGiveNpcPresent( userObjID, 711 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "skill_60" then
		api_ui_OpenSkillShop(userObjID);
	elseif npc_talk_index == "present_60_wo" then
		api_ui_OpenGiveNpcPresent( userObjID, 711 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "skill_30" then
		api_ui_OpenSkillShop(userObjID);
	elseif npc_talk_index == "present_30_wo" then
		api_ui_OpenGiveNpcPresent( userObjID, 711 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "skill_0" then
		api_ui_OpenSkillShop(userObjID);
	elseif npc_talk_index == "present_0_wo" then
		api_ui_OpenGiveNpcPresent( userObjID, 711 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( userObjID, 711 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( userObjID, 711 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_30" then
		api_ui_OpenGiveNpcPresent( userObjID, 711 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( userObjID, 711 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( userObjID, 711 );
		api_npc_NextTalk(userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		end

		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_gift" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		end

		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_talk" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		end

		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 711 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n711_warrior_fedro_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		end

		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "skill" then
		api_ui_OpenSkillShop( pRoom, userObjID);
	elseif npc_talk_index == "back_quest" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		end

		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "skill_90" then
		api_ui_OpenSkillShop( pRoom, userObjID);
	elseif npc_talk_index == "present_90_wor" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 711 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "skill_60" then
		api_ui_OpenSkillShop( pRoom, userObjID);
	elseif npc_talk_index == "present_60_wo" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 711 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "skill_30" then
		api_ui_OpenSkillShop( pRoom, userObjID);
	elseif npc_talk_index == "present_30_wo" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 711 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "skill_0" then
		api_ui_OpenSkillShop( pRoom, userObjID);
	elseif npc_talk_index == "present_0_wo" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 711 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 711 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 711 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_30" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 711 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 711 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( pRoom,  userObjID, 711 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		end

		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_gift" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		end

		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_talk" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent_wo", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent_wo", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent_wo", npc_talk_target);
		end

		end

		end

		end

		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 7101) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 711 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "0_percent", npc_talk_target);
		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
