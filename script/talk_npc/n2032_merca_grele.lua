<VillageServer>


function n2032_merca_grele_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9743) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc18_9743_proviso_of_hiver2.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_UserHasQuest(userObjID,9743) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc18_9743_proviso_of_hiver2.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n2032_merca_grele_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9743) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc18_9743_proviso_of_hiver2.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_UserHasQuest( pRoom, userObjID,9743) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc18_9743_proviso_of_hiver2.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
