<VillageServer>


function n1500_gaharam_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9440) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9440_the_man_who_wish_dark.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9446) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9446_shadow_prophetess.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9447) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc15_9447_shadow_prophetess.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1500_gaharam_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9440) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9440_the_man_who_wish_dark.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9446) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9446_shadow_prophetess.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9447) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc15_9447_shadow_prophetess.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
