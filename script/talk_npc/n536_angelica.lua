<VillageServer>


function n536_angelica_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,95) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_095_performance_of_the_search.xml");
		else				
		if api_quest_UserHasQuest(userObjID,96) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_096_traitor_is_on_the_inside.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n536_angelica_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,95) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_095_performance_of_the_search.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,96) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_096_traitor_is_on_the_inside.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
