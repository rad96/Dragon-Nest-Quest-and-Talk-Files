<VillageServer>


function n1504_hound_ghost_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4578) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc14_4578_call_of_hound.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 4578) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "not4578", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back" then
		if api_quest_UserHasQuest(userObjID,4578) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sqc14_4578_call_of_hound.xml");
		else				
		if api_quest_IsMarkingCompleteQuest(userObjID, 4578) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "not4578", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1504_hound_ghost_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4578) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc14_4578_call_of_hound.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 4578) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "not4578", npc_talk_target);
		end

		end

	elseif npc_talk_index == "back" then
		if api_quest_UserHasQuest( pRoom, userObjID,4578) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sqc14_4578_call_of_hound.xml");
		else				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 4578) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "not4578", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
