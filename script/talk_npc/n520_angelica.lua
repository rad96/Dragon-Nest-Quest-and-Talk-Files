<VillageServer>


function n520_angelica_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,437) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_437_cooperatively.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9304) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_9304_enemy_in_heart.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9305) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_9305_girl_alone.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9306) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_9306_plate_of_eggs.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n520_angelica_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,437) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_437_cooperatively.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9304) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_9304_enemy_in_heart.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9305) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_9305_girl_alone.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9306) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_9306_plate_of_eggs.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
