<VillageServer>


function n751_elite_soldier_wounded_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,4278) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq15_4278_memoria_2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9733) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc10_9733_memoria_1.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n751_elite_soldier_wounded_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,4278) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq15_4278_memoria_2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9733) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc10_9733_memoria_1.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
