<VillageServer>


function n356_arena_ticker_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel(userObjID) >= 10 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "level", npc_talk_target);
		end

	elseif npc_talk_index == "gogogo" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeChannel( userObjID, 1 );
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party", npc_talk_target);
		end

	elseif npc_talk_index == "trade_1" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,5001,100);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,5002,100);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,5003,100);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,5004,100);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,5005,100);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,5006,100);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,5007,100);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,5008,100);
		else				
		api_ui_OpenShop(userObjID,5009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "trade_2" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,5101,101);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,5102,101);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,5103,101);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,5104,101);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,5105,101);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,5106,101);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,5107,101);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,5108,101);
		else				
		api_ui_OpenShop(userObjID,5109,101);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "trade_3" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,5201,100);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,5202,100);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,5203,100);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,5204,100);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,5205,100);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,5206,100);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,5207,100);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,5208,100);
		else				
		api_ui_OpenShop(userObjID,5209,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_not70" then
		if api_user_GetUserLevel(userObjID) >= 10 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "level", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n356_arena_ticker_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 10 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "level", npc_talk_target);
		end

	elseif npc_talk_index == "gogogo" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeChannel( pRoom,  userObjID, 1 );
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party", npc_talk_target);
		end

	elseif npc_talk_index == "trade_1" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,5001,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,5002,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,5003,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,5004,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,5005,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,5006,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,5007,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,5008,100);
		else				
		api_ui_OpenShop( pRoom, userObjID,5009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "trade_2" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,5101,101);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,5102,101);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,5103,101);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,5104,101);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,5105,101);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,5106,101);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,5107,101);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,5108,101);
		else				
		api_ui_OpenShop( pRoom, userObjID,5109,101);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "trade_3" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,5201,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,5202,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,5203,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,5204,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,5205,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,5206,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,5207,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,5208,100);
		else				
		api_ui_OpenShop( pRoom, userObjID,5209,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_not70" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 10 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "level", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
