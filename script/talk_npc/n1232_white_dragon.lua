<VillageServer>


function n1232_white_dragon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9391) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9391_hint_of_ancient_times.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9394) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9394_efforts_for_legend.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9395) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq34_9395_end_of_legend.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1232_white_dragon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9391) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9391_hint_of_ancient_times.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9394) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9394_efforts_for_legend.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9395) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq34_9395_end_of_legend.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
