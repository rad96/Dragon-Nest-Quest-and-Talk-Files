<VillageServer>


function n1278_weird_player_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "age_select" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "020", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "024", npc_talk_target);
		end

	elseif npc_talk_index == "morning_a" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk(userObjID, npcObjID, "011", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "012", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "013", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo011" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "012", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "013", npc_talk_target);
		end

	elseif npc_talk_index == "mo012" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "013", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "011", npc_talk_target);
		end

	elseif npc_talk_index == "mo013" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "011", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "012", npc_talk_target);
		end

	elseif npc_talk_index == "morning_g1" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk(userObjID, npcObjID, "021", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "022", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "023", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo021" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "022", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "023", npc_talk_target);
		end

	elseif npc_talk_index == "mo022" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "023", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "021", npc_talk_target);
		end

	elseif npc_talk_index == "mo03" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "021", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "022", npc_talk_target);
		end

	elseif npc_talk_index == "morning_g2" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk(userObjID, npcObjID, "025", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "026", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "027", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo025" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "026", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "027", npc_talk_target);
		end

	elseif npc_talk_index == "mo026" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "027", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "025", npc_talk_target);
		end

	elseif npc_talk_index == "mo027" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "025", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "026", npc_talk_target);
		end

	elseif npc_talk_index == "vels_ment" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "031", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "032", npc_talk_target);
		end

	elseif npc_talk_index == "morning_v" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk(userObjID, npcObjID, "033", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "034", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "035", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo033" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "034", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "035", npc_talk_target);
		end

	elseif npc_talk_index == "mo034" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "035", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "033", npc_talk_target);
		end

	elseif npc_talk_index == "mo035" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "033", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "034", npc_talk_target);
		end

	elseif npc_talk_index == "morning_r" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "025", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "026", npc_talk_target);
		end

	elseif npc_talk_index == "morning_k" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk(userObjID, npcObjID, "051", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "052", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "053", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo051" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "052", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "053", npc_talk_target);
		end

	elseif npc_talk_index == "mo052" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "053", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "051", npc_talk_target);
		end

	elseif npc_talk_index == "mo053" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "051", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "052", npc_talk_target);
		end

	elseif npc_talk_index == "morning_6" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk(userObjID, npcObjID, "061", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "062", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "063", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo061" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "062", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "063", npc_talk_target);
		end

	elseif npc_talk_index == "mo062" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "063", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "061", npc_talk_target);
		end

	elseif npc_talk_index == "mo063" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "061", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "062", npc_talk_target);
		end

	elseif npc_talk_index == "morning_s" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk(userObjID, npcObjID, "071", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "072", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "073", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo071" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "072", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "073", npc_talk_target);
		end

	elseif npc_talk_index == "mo072" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "073", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "071", npc_talk_target);
		end

	elseif npc_talk_index == "mo073" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk(userObjID, npcObjID, "071", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "072", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1278_weird_player_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "age_select" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "020", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "024", npc_talk_target);
		end

	elseif npc_talk_index == "morning_a" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "011", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "012", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "013", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo011" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "012", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "013", npc_talk_target);
		end

	elseif npc_talk_index == "mo012" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "013", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "011", npc_talk_target);
		end

	elseif npc_talk_index == "mo013" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "011", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "012", npc_talk_target);
		end

	elseif npc_talk_index == "morning_g1" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "021", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "022", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "023", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo021" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "022", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "023", npc_talk_target);
		end

	elseif npc_talk_index == "mo022" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "023", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "021", npc_talk_target);
		end

	elseif npc_talk_index == "mo03" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "021", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "022", npc_talk_target);
		end

	elseif npc_talk_index == "morning_g2" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "025", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "026", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "027", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo025" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "026", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "027", npc_talk_target);
		end

	elseif npc_talk_index == "mo026" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "027", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "025", npc_talk_target);
		end

	elseif npc_talk_index == "mo027" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "025", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "026", npc_talk_target);
		end

	elseif npc_talk_index == "vels_ment" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "031", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "032", npc_talk_target);
		end

	elseif npc_talk_index == "morning_v" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "033", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "034", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "035", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo033" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "034", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "035", npc_talk_target);
		end

	elseif npc_talk_index == "mo034" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "035", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "033", npc_talk_target);
		end

	elseif npc_talk_index == "mo035" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "033", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "034", npc_talk_target);
		end

	elseif npc_talk_index == "morning_r" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "025", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "026", npc_talk_target);
		end

	elseif npc_talk_index == "morning_k" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "051", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "052", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "053", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo051" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "052", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "053", npc_talk_target);
		end

	elseif npc_talk_index == "mo052" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "053", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "051", npc_talk_target);
		end

	elseif npc_talk_index == "mo053" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "051", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "052", npc_talk_target);
		end

	elseif npc_talk_index == "morning_6" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "061", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "062", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "063", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo061" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "062", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "063", npc_talk_target);
		end

	elseif npc_talk_index == "mo062" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "063", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "061", npc_talk_target);
		end

	elseif npc_talk_index == "mo063" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "061", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "062", npc_talk_target);
		end

	elseif npc_talk_index == "morning_s" then
		if math.random(1,1000) <= 300 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "071", npc_talk_target);
		else				
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "072", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "073", npc_talk_target);
		end

		end

	elseif npc_talk_index == "mo071" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "072", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "073", npc_talk_target);
		end

	elseif npc_talk_index == "mo072" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "073", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "071", npc_talk_target);
		end

	elseif npc_talk_index == "mo073" then
		if math.random(1,1000) <= 500 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "071", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "072", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
