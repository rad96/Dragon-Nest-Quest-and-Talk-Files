<VillageServer>


function n262_argenta_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,419) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_419_where_argenta.xml");
		else				
		if api_quest_UserHasQuest(userObjID,434) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq11_434_so_cool_girl.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9479) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc04_9479_black_and_gold.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n262_argenta_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,419) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_419_where_argenta.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,434) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq11_434_so_cool_girl.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9479) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc04_9479_black_and_gold.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
