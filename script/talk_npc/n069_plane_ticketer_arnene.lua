<VillageServer>


function n069_plane_ticketer_arnene_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_GetQuestStep(userObjID, 401) == 1 then				
		if api_quest_GetQuestMemo(userObjID, 401, 1) == 2 then				
		api_npc_NextTalk(userObjID, npcObjID, "mq401_1", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_quest_GetQuestStep(userObjID, 432) == 2 then				
		api_npc_NextTalk(userObjID, npcObjID, "mq432_1", npc_talk_target);
		else				
		if api_quest_GetQuestStep(userObjID, 9300) == 2 then				
		api_npc_NextTalk(userObjID, npcObjID, "mq9300_1", npc_talk_target);
		else				
		if api_quest_GetQuestStep(userObjID, 9478) == 2 then				
		api_npc_NextTalk(userObjID, npcObjID, "mq9478_1", npc_talk_target);
		else				
		if api_quest_GetQuestStep(userObjID, 9590) == 2 then				
		api_npc_NextTalk(userObjID, npcObjID, "mq9590_1", npc_talk_target);
		else				
		if api_user_GetUserLevel(userObjID) >= 40 then				
		api_npc_NextTalk(userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_user_GetUserLevel(userObjID) >= 24 then				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "changemap" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_PlayCutScene(userObjID,npcObjID,96,true);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "completecutscene" then
		if api_quest_GetQuestStep(userObjID, 401) == 1 then				
		api_user_ChangeMap(userObjID, 13015, 1);
		else				
		if api_quest_GetQuestStep(userObjID, 432) == 2 then				
		api_user_ChangeMap(userObjID, 13015, 1);
		else				
		if api_quest_GetQuestStep(userObjID, 9300) == 2 then				
		api_user_ChangeMap(userObjID, 13015, 1);
		else				
		if api_quest_GetQuestStep(userObjID, 9478) == 2 then				
		api_user_ChangeMap(userObjID, 13015, 1);
		else				
		if api_quest_GetQuestStep(userObjID, 9590) == 2 then				
		api_user_ChangeMap(userObjID, 13015, 1);
		else				
		api_user_ChangeMap(userObjID,11,1);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "go_saint" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_PlayCutScene(userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "go_saint" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_PlayCutScene(userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "move_saint" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_PlayCutScene(userObjID,npcObjID,96,true);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "move_lotus" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_ChangeMap(userObjID, 15, 1);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "go_saint_q432" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_PlayCutScene(userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "go_saint_q9300" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_PlayCutScene(userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "go_saint_q9478" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_PlayCutScene(userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "go_saint_q9590" then
		if api_user_IsPartymember(userObjID) == 0 then				
		api_user_PlayCutScene(userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "party_member", npc_talk_target);
		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n069_plane_ticketer_arnene_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_GetQuestStep( pRoom, userObjID, 401) == 1 then				
		if api_quest_GetQuestMemo( pRoom, userObjID, 401, 1) == 2 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "mq401_1", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		else				
		if api_quest_GetQuestStep( pRoom, userObjID, 432) == 2 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "mq432_1", npc_talk_target);
		else				
		if api_quest_GetQuestStep( pRoom, userObjID, 9300) == 2 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "mq9300_1", npc_talk_target);
		else				
		if api_quest_GetQuestStep( pRoom, userObjID, 9478) == 2 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "mq9478_1", npc_talk_target);
		else				
		if api_quest_GetQuestStep( pRoom, userObjID, 9590) == 2 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "mq9590_1", npc_talk_target);
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 40 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "004", npc_talk_target);
		else				
		if api_user_GetUserLevel( pRoom, userObjID) >= 24 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "changemap" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_PlayCutScene( pRoom, userObjID,npcObjID,96,true);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "completecutscene" then
		if api_quest_GetQuestStep( pRoom, userObjID, 401) == 1 then				
		api_user_ChangeMap( pRoom, userObjID, 13015, 1);
		else				
		if api_quest_GetQuestStep( pRoom, userObjID, 432) == 2 then				
		api_user_ChangeMap( pRoom, userObjID, 13015, 1);
		else				
		if api_quest_GetQuestStep( pRoom, userObjID, 9300) == 2 then				
		api_user_ChangeMap( pRoom, userObjID, 13015, 1);
		else				
		if api_quest_GetQuestStep( pRoom, userObjID, 9478) == 2 then				
		api_user_ChangeMap( pRoom, userObjID, 13015, 1);
		else				
		if api_quest_GetQuestStep( pRoom, userObjID, 9590) == 2 then				
		api_user_ChangeMap( pRoom, userObjID, 13015, 1);
		else				
		api_user_ChangeMap( pRoom, userObjID,11,1);
		end

		end

		end

		end

		end

	elseif npc_talk_index == "go_saint" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_PlayCutScene( pRoom, userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "go_saint" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_PlayCutScene( pRoom, userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "move_saint" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_PlayCutScene( pRoom, userObjID,npcObjID,96,true);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "move_lotus" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_ChangeMap( pRoom, userObjID, 15, 1);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "go_saint_q432" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_PlayCutScene( pRoom, userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "go_saint_q9300" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_PlayCutScene( pRoom, userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "go_saint_q9478" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_PlayCutScene( pRoom, userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	elseif npc_talk_index == "go_saint_q9590" then
		if api_user_IsPartymember( pRoom, userObjID) == 0 then				
		api_user_PlayCutScene( pRoom, userObjID,npcObjID,97,true);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "party_member", npc_talk_target);
		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
