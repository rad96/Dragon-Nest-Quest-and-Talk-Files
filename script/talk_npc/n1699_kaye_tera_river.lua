<VillageServer>


function n1699_kaye_tera_river_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9509) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9509_regained_the_prophet.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9510) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc06_9510_a_lot_of_work.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1699_kaye_tera_river_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9509) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9509_regained_the_prophet.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9510) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc06_9510_a_lot_of_work.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
