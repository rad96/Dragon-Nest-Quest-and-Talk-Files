<VillageServer>


function n429_edan_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,654) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_654_passion_of_reader.xml");
		else				
		if api_quest_UserHasQuest(userObjID,638) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_638_reason_paganism.xml");
		else				
		if api_quest_UserHasQuest(userObjID,645) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_645_paganish_name.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n429_edan_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,654) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_654_passion_of_reader.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,638) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_638_reason_paganism.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,645) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","sq11_645_paganish_name.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
