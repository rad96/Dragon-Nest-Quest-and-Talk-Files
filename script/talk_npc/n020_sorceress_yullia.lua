<VillageServer>


function n020_sorceress_yullia_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,104) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_104_orb_of_vision2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,105) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_105_orb_of_vision3.xml");
		else				
		if api_quest_UserHasQuest(userObjID,128) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_128_orb_of_vision1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,129) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_129_orb_of_vision2.xml");
		else				
		if api_quest_UserHasQuest(userObjID,138) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_138_orb_of_vision1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,139) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq05_139_orb_of_vision2.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n020_sorceress_yullia_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,104) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_104_orb_of_vision2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,105) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_105_orb_of_vision3.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,128) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_128_orb_of_vision1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,129) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_129_orb_of_vision2.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,138) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_138_orb_of_vision1.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,139) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq05_139_orb_of_vision2.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
