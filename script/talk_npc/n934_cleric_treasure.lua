<VillageServer>


function n934_cleric_treasure_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel(userObjID) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "shop" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,60001,100);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,60002,100);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,60003,100);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,60004,100);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,60005,100);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,60006,100);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,60007,100);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,60008,100);
		else				
		api_ui_OpenShop(userObjID,60009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n934_cleric_treasure_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

	elseif npc_talk_index == "shop" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,60001,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,60002,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,60003,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,60004,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,60005,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,60006,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,60007,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,60008,100);
		else				
		api_ui_OpenShop( pRoom, userObjID,60009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
