<VillageServer>


function n989_super_muscle_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel(userObjID) >= 80 then				
		if api_quest_IsMarkingCompleteQuest(userObjID, 2439) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_UserHasQuest(userObjID,2439) > -1 then				
		api_npc_NextTalk(userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "002_3a1" then
		api_npc_NextTalk(userObjID, npcObjID, "002_3a", npc_talk_target);
		api_quest_AddQuest(userObjID, 2439, 2);
		api_quest_SetJournalStep(userObjID, 2439, 1);
		api_quest_SetQuestStep(userObjID, 2439, 1);
	elseif npc_talk_index == "002_3a2" then
		api_npc_NextTalk(userObjID, npcObjID, "002_3a", npc_talk_target);
		api_quest_AddQuest(userObjID, 2439, 2);
		api_quest_SetJournalStep(userObjID, 2439, 1);
		api_quest_SetQuestStep(userObjID, 2439, 1);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n989_super_muscle_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_user_GetUserLevel( pRoom, userObjID) >= 80 then				
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 2439) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,2439) > -1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "003", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002", npc_talk_target);
		end

		end

		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

	elseif npc_talk_index == "002_3a1" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002_3a", npc_talk_target);
		api_quest_AddQuest( pRoom, userObjID, 2439, 2);
		api_quest_SetJournalStep( pRoom, userObjID, 2439, 1);
		api_quest_SetQuestStep( pRoom, userObjID, 2439, 1);
	elseif npc_talk_index == "002_3a2" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "002_3a", npc_talk_target);
		api_quest_AddQuest( pRoom, userObjID, 2439, 2);
		api_quest_SetJournalStep( pRoom, userObjID, 2439, 1);
		api_quest_SetQuestStep( pRoom, userObjID, 2439, 1);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
