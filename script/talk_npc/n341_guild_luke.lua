<VillageServer>


function n341_guild_luke_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "guild_menu" then
		if api_guild_GetGuildMemberRole(userObjID) == 0 then				
		if api_user_GetUserLevel(userObjID) >= 9 then				
		api_npc_NextTalk(userObjID, npcObjID, "no_guild", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "no_level", npc_talk_target);
		end

		else				
		if api_guild_GetGuildMemberRole(userObjID) == 1 then				
		api_npc_NextTalk(userObjID, npcObjID, "guild_master", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "guild_man", npc_talk_target);
		end

		end

	elseif npc_talk_index == "no_guild_0" then
		api_ui_OpenGuildMgrBox(userObjID, 0);
	elseif npc_talk_index == "guild_man_2" then
		api_ui_OpenGuildMgrBox(userObjID, 2);
	elseif npc_talk_index == "guild_war" then
		if api_guildwar_IsPreparation(userObjID) == 1 then				
		api_ui_OpenGuildMgrBox(userObjID, 5);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "guild_war_fail", npc_talk_target);
		end

	elseif npc_talk_index == "guild_admin" then
		api_ui_OpenGuildMgrBox(userObjID, 1);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n341_guild_luke_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
	elseif npc_talk_index == "guild_menu" then
		if api_guild_GetGuildMemberRole( pRoom, userObjID) == 0 then				
		if api_user_GetUserLevel( pRoom, userObjID) >= 9 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "no_guild", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "no_level", npc_talk_target);
		end

		else				
		if api_guild_GetGuildMemberRole( pRoom, userObjID) == 1 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "guild_master", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "guild_man", npc_talk_target);
		end

		end

	elseif npc_talk_index == "no_guild_0" then
		api_ui_OpenGuildMgrBox( pRoom, userObjID, 0);
	elseif npc_talk_index == "guild_man_2" then
		api_ui_OpenGuildMgrBox( pRoom, userObjID, 2);
	elseif npc_talk_index == "guild_war" then
		if api_guildwar_IsPreparation( pRoom, userObjID) == 1 then				
		api_ui_OpenGuildMgrBox( pRoom, userObjID, 5);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "guild_war_fail", npc_talk_target);
		end

	elseif npc_talk_index == "guild_admin" then
		api_ui_OpenGuildMgrBox( pRoom, userObjID, 1);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
