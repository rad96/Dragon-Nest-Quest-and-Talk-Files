<VillageServer>


function n028_scholar_bailey_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 5301) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "emblem_001" then
		api_ui_OpenCompoundEmblem(userObjID);
	elseif npc_talk_index == "openglyphlift_001" then
		api_ui_OpenGlyphLift(userObjID);
	elseif npc_talk_index == "trade_001" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,35001,100);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,35002,100);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,35003,100);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,35004,100);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,35005,100);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,35006,100);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,35007,100);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,35008,100);
		else				
		api_ui_OpenShop(userObjID,35009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_talk" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 5301) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 5301) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "emblem_90" then
		api_ui_OpenCompoundEmblem(userObjID);
	elseif npc_talk_index == "openglyphlift_90" then
		api_ui_OpenGlyphLift(userObjID);
	elseif npc_talk_index == "trade_90" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,35001,100);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,35002,100);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,35003,100);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,35004,100);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,35005,100);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,35006,100);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,35007,100);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,35008,100);
		else				
		api_ui_OpenShop(userObjID,35009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( userObjID, 28 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "back_gift" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 5301) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( userObjID, 28 );
		api_npc_NextTalk(userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_quest_IsMarkingCompleteQuest(userObjID, 5301) == 1 then				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 90 then				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 60 then				
		api_npc_NextTalk(userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( userObjID, 28 ) >= 30 then				
		api_npc_NextTalk(userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk(userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "emblem_60" then
		api_ui_OpenCompoundEmblem(userObjID);
	elseif npc_talk_index == "openglyphlift_60" then
		api_ui_OpenGlyphLift(userObjID);
	elseif npc_talk_index == "trade_60" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,35001,100);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,35002,100);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,35003,100);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,35004,100);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,35005,100);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,35006,100);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,35007,100);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,35008,100);
		else				
		api_ui_OpenShop(userObjID,35009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( userObjID, 28 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "emblem_30" then
		api_ui_OpenCompoundEmblem(userObjID);
	elseif npc_talk_index == "openglyphlift_30" then
		api_ui_OpenGlyphLift(userObjID);
	elseif npc_talk_index == "trade_30" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,35001,100);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,35002,100);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,35003,100);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,35004,100);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,35005,100);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,35006,100);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,35007,100);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,35008,100);
		else				
		api_ui_OpenShop(userObjID,35009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "present_30" then
		api_ui_OpenGiveNpcPresent( userObjID, 28 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "emblem_0" then
		api_ui_OpenCompoundEmblem(userObjID);
	elseif npc_talk_index == "openglyphlift_0" then
		api_ui_OpenGlyphLift(userObjID);
	elseif npc_talk_index == "trade_0" then
		if api_user_GetUserClassID(userObjID) == 1 then				
		api_ui_OpenShop(userObjID,35001,100);
		else				
		if api_user_GetUserClassID(userObjID) == 2 then				
		api_ui_OpenShop(userObjID,35002,100);
		else				
		if api_user_GetUserClassID(userObjID) == 3 then				
		api_ui_OpenShop(userObjID,35003,100);
		else				
		if api_user_GetUserClassID(userObjID) == 4 then				
		api_ui_OpenShop(userObjID,35004,100);
		else				
		if api_user_GetUserClassID(userObjID) == 5 then				
		api_ui_OpenShop(userObjID,35005,100);
		else				
		if api_user_GetUserClassID(userObjID) == 6 then				
		api_ui_OpenShop(userObjID,35006,100);
		else				
		if api_user_GetUserClassID(userObjID) == 7 then				
		api_ui_OpenShop(userObjID,35007,100);
		else				
		if api_user_GetUserClassID(userObjID) == 8 then				
		api_ui_OpenShop(userObjID,35008,100);
		else				
		api_ui_OpenShop(userObjID,35009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( userObjID, 28 );
		api_npc_NextTalk(userObjID, npcObjID, "present_ui", npc_talk_target);
	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n028_scholar_bailey_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 5301) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "emblem_001" then
		api_ui_OpenCompoundEmblem( pRoom, userObjID);
	elseif npc_talk_index == "openglyphlift_001" then
		api_ui_OpenGlyphLift( pRoom, userObjID);
	elseif npc_talk_index == "trade_001" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,35001,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,35002,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,35003,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,35004,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,35005,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,35006,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,35007,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,35008,100);
		else				
		api_ui_OpenShop( pRoom, userObjID,35009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "back_talk" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 5301) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "back_quest" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 5301) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "emblem_90" then
		api_ui_OpenCompoundEmblem( pRoom, userObjID);
	elseif npc_talk_index == "openglyphlift_90" then
		api_ui_OpenGlyphLift( pRoom, userObjID);
	elseif npc_talk_index == "trade_90" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,35001,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,35002,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,35003,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,35004,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,35005,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,35006,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,35007,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,35008,100);
		else				
		api_ui_OpenShop( pRoom, userObjID,35009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "present_90" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 28 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "back_gift" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 5301) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "gift" then
		api_npc_SendSelectedPresent( pRoom,  userObjID, 28 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "gift", npc_talk_target);
	elseif npc_talk_index == "back_present_ui" then
		if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 5301) == 1 then				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 90 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 60 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "60_percent", npc_talk_target);
		else				
		if api_npc_GetFavorPercent( pRoom,  userObjID, 28 ) >= 30 then				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "30_percent", npc_talk_target);
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "90_percent", npc_talk_target);
		end

		end

		end

		end

	elseif npc_talk_index == "emblem_60" then
		api_ui_OpenCompoundEmblem( pRoom, userObjID);
	elseif npc_talk_index == "openglyphlift_60" then
		api_ui_OpenGlyphLift( pRoom, userObjID);
	elseif npc_talk_index == "trade_60" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,35001,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,35002,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,35003,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,35004,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,35005,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,35006,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,35007,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,35008,100);
		else				
		api_ui_OpenShop( pRoom, userObjID,35009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "present_60" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 28 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "emblem_30" then
		api_ui_OpenCompoundEmblem( pRoom, userObjID);
	elseif npc_talk_index == "openglyphlift_30" then
		api_ui_OpenGlyphLift( pRoom, userObjID);
	elseif npc_talk_index == "trade_30" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,35001,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,35002,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,35003,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,35004,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,35005,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,35006,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,35007,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,35008,100);
		else				
		api_ui_OpenShop( pRoom, userObjID,35009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "present_30" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 28 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	elseif npc_talk_index == "emblem_0" then
		api_ui_OpenCompoundEmblem( pRoom, userObjID);
	elseif npc_talk_index == "openglyphlift_0" then
		api_ui_OpenGlyphLift( pRoom, userObjID);
	elseif npc_talk_index == "trade_0" then
		if api_user_GetUserClassID( pRoom, userObjID) == 1 then				
		api_ui_OpenShop( pRoom, userObjID,35001,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 2 then				
		api_ui_OpenShop( pRoom, userObjID,35002,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 3 then				
		api_ui_OpenShop( pRoom, userObjID,35003,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 4 then				
		api_ui_OpenShop( pRoom, userObjID,35004,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 5 then				
		api_ui_OpenShop( pRoom, userObjID,35005,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 6 then				
		api_ui_OpenShop( pRoom, userObjID,35006,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 7 then				
		api_ui_OpenShop( pRoom, userObjID,35007,100);
		else				
		if api_user_GetUserClassID( pRoom, userObjID) == 8 then				
		api_ui_OpenShop( pRoom, userObjID,35008,100);
		else				
		api_ui_OpenShop( pRoom, userObjID,35009,100);
		end

		end

		end

		end

		end

		end

		end

		end

	elseif npc_talk_index == "present_0" then
		api_ui_OpenGiveNpcPresent( pRoom,  userObjID, 28 );
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "present_ui", npc_talk_target);
	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
