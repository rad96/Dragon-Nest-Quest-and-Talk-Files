<VillageServer>


function n726_velskud_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9518) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc07_9518_uninvited_guests.xml");
		else				
		if api_quest_UserHasQuest(userObjID,740) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_740_past_story.xml");
		else				
		if api_quest_UserHasQuest(userObjID,739) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_739_meet_again.xml");
		else				
		if api_quest_UserHasQuest(userObjID,719) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_719_dark_past.xml");
		else				
		if api_quest_UserHasQuest(userObjID,718) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mq15_718_other_uninvited_guest.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9632) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc07_9632_unwelcome_guest.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9633) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc07_9633_solve_the_problems_yourself.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n726_velskud_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9518) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc07_9518_uninvited_guests.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,740) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_740_past_story.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,739) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_739_meet_again.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,719) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_719_dark_past.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,718) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mq15_718_other_uninvited_guest.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9632) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc07_9632_unwelcome_guest.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9633) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc07_9633_solve_the_problems_yourself.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
