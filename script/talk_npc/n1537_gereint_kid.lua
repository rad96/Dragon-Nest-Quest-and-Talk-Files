<VillageServer>


function n1537_gereint_kid_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,9420) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9420_the_man_of_worry.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9421) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9421_swallowed_word.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9425) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9425_power.xml");
		else				
		if api_quest_UserHasQuest(userObjID,9426) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","mqc14_9426_the_man_who_open_the_door.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</VillageServer>



<GameServer>

function n1537_gereint_kid_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest( pRoom, userObjID,9420) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9420_the_man_of_worry.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9421) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9421_swallowed_word.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9425) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9425_power.xml");
		else				
		if api_quest_UserHasQuest( pRoom, userObjID,9426) > -1 then				
		api_npc_NextScript( pRoom, userObjID, npcObjID, "q_enter","mqc14_9426_the_man_who_open_the_door.xml");
		else				
		api_npc_NextTalk( pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

		end

	else
		api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end

</GameServer>
