<VillageServer>
function n296_butterfly_ring_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog("npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(userObjID,474) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_474_more_important_thing.xml");
		else				
		if api_quest_UserHasQuest(userObjID,569) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_569_more_episode_rings1.xml");
		else				
		if api_quest_UserHasQuest(userObjID,570) > -1 then				
		api_npc_NextScript(userObjID, npcObjID, "q_enter","sq11_570_more_episode_rings2.xml");
		else				
		api_npc_NextTalk(userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</VillageServer>

<GameServer>
function n296_butterfly_ring_OnTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target)

	if npc_talk_index == "" then
		api_log_AddLog( pRoom, "npc_talk_index is null ..\n");
	elseif npc_talk_index == "start" then
		if api_quest_UserHasQuest(pRoom, userObjID,474) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq11_474_more_important_thing.xml");
		else				
		if api_quest_UserHasQuest(pRoom, userObjID,569) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq11_569_more_episode_rings1.xml");
		else				
		if api_quest_UserHasQuest(pRoom, userObjID,570) > -1 then				
		api_npc_NextScript(pRoom, userObjID, npcObjID, "q_enter","sq11_570_more_episode_rings2.xml");
		else				
		api_npc_NextTalk(pRoom, userObjID, npcObjID, "001", npc_talk_target);
		end

		end

		end

	else
		api_npc_NextTalk(pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	end

end
</GameServer>
