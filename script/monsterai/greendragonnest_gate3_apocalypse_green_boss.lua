-- /genmon 236010
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 800.0;
g_Lua_NearValue2 = 1200.0;
g_Lua_NearValue3 = 1700.0;
g_Lua_NearValue4 = 2500.0;
g_Lua_NearValue5 = 5000.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 800;
g_Lua_AssualtTime = 5000;

g_Lua_GlobalCoolTime1 = 45000 
g_Lua_GlobalCoolTime2 = 5000
g_Lua_GlobalCoolTime3 = 11000
g_Lua_GlobalCoolTime4 = 55000

g_Lua_CustomAction = 
{
	CustomAction1 = 
	{
		{"Attack01_Bite_Open",0},
		{"Attack05_Spin_Open",0},
	},
}
g_Lua_Near1 = 
	{
		{ action_name = "Stand",rate = 10, loop = 1 }, 
		{ action_name = "Attack01_Bite_Open",rate = 15, loop = 1,td="FL,FR",selfhppercentrange="0,50" }, 
		{ action_name = "Attack_BasicLeft",rate = 25,td = "FR,RF,RB", loop = 1,cooltime = 6000 }, 
		{ action_name = "Attack_BasicRight",rate = 25,td = "LF,FL,LB", loop = 1,cooltime = 6000 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="50,100",notusedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="50,100" ,notusedskill="31402"}, 
		{ action_name = "Attack16_EyeLaser_Line_Three_Fury",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="25,50",usedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Four",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="25,50" ,usedskill="31402"},
		{ action_name = "Attack16_EyeLaser_Line_Five",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="0,25" ,usedskill="31402"},
		{ action_name = "Attack03_ClawRange_Open_P1",rate = 10,	 loop = 1, globalcooltime=3}, 
		{ action_name = "Attack03_ClawRange_Open_P2",rate = 10,	 loop = 1, globalcooltime=3},
	}
g_Lua_Near2 = 
	{ 
		{ action_name = "Stand",rate = 10, loop = 1 }, 
		-- { action_name = "Attack01_Bite_Open",rate = 10, loop = 1,cooltime=10000,selfhppercentrange="0,50" }, 
		-- { action_name = "Attack_BasicLeft",rate = 25,td = "FR,RF,RB", loop = 1,cooltime = 6000 }, 
		-- { action_name = "Attack_BasicRight",rate = 25,td = "LF,FL,LB", loop = 1,cooltime = 6000 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="50,100",notusedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="50,100" ,notusedskill="31402"}, 
		{ action_name = "Attack16_EyeLaser_Line_Three_Fury",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="25,50",usedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Four",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="25,50" ,usedskill="31402"},
		{ action_name = "Attack16_EyeLaser_Line_Five",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="0,25" ,usedskill="31402"},
		{ action_name = "Attack03_ClawRange_Open_P1",rate = 10,	 loop = 1, globalcooltime=3}, 
		{ action_name = "Attack03_ClawRange_Open_P2",rate = 10,	 loop = 1, globalcooltime=3},
	}
g_Lua_Near3 = 
	{
		{ action_name = "Stand",rate = 10, loop = 1 },
		{ action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="50,100",notusedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="50,100" ,notusedskill="31402"}, 
		{ action_name = "Attack16_EyeLaser_Line_Three_Fury",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="25,50",usedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Four",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="25,50" ,usedskill="31402"},
		{ action_name = "Attack16_EyeLaser_Line_Five",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="0,25" ,usedskill="31402"},
		{ action_name = "Attack03_ClawRange_Open_P1",rate = 10,	 loop = 1, globalcooltime=3}, 
		{ action_name = "Attack03_ClawRange_Open_P2",rate = 10,	 loop = 1, globalcooltime=3},
	}
g_Lua_Near4 = 
	{
		{ action_name = "Stand",rate = 10, loop = 1 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="50,100",notusedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="50,100" ,notusedskill="31402"}, 
		{ action_name = "Attack16_EyeLaser_Line_Three_Fury",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="25,50",usedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Four",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="25,50" ,usedskill="31402"},
		{ action_name = "Attack16_EyeLaser_Line_Five",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="0,25" ,usedskill="31402"},
		{ action_name = "Attack03_ClawRange_Open_P1",rate = 10,	 loop = 1, globalcooltime=3}, 
		{ action_name = "Attack03_ClawRange_Open_P2",rate = 10,	 loop = 1, globalcooltime=3},
	}
g_Lua_Near5 = 
	{ 
		{ action_name = "Stand",rate = 10, loop = 1 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="50,100",notusedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="50,100" ,notusedskill="31402"}, 
		{ action_name = "Attack16_EyeLaser_Line_Three_Fury",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="25,50",usedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Four",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="25,50" ,usedskill="31402"},
		{ action_name = "Attack16_EyeLaser_Line_Five",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="0,25" ,usedskill="31402"},
		{ action_name = "Attack03_ClawRange_Open_P1",rate = 10,	 loop = 1, globalcooltime=3}, 
		{ action_name = "Attack03_ClawRange_Open_P2",rate = 10,	 loop = 1, globalcooltime=3},
	}
g_Lua_Skill = {
   { skill_index = 31400, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4500, next_lua_skill_index = 1,target = 3,selfhppercentrange="50,100",notusedskill="31402"  }, -- "즉사광선" #0
   { skill_index = 31383, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4500, next_lua_skill_index = 2,target = 3,selfhppercentrange="50,100",notusedskill="31402"  }, -- "눈감기" #1
   { skill_index = 31401, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4500, next_lua_skill_index = 3,target = 3,selfhppercentrange="50,100",notusedskill="31402"  }, -- "잠자기" #2
   { skill_index = 31392, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2500, target = 3 },  -- "밀쳐냐기" #3
   { skill_index = 31397, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2500, target = 3 },  -- "점프" #4
   { skill_index = 31396, cooltime = 25000, rate = 100, rangemin = 0 ,rangemax= 5500,target = 3,next_lua_skill_index = 4,multipletarget = "1,8",encountertime=30000,selfhppercentrange="0,75" }, --"호밍빔" : 넥스트액션으로 점프를 실행 #5
   { skill_index = 31385, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 4500, next_lua_skill_index = 0,target = 3,encountertime=60000,globalcooltime=1,selfhppercentrange="50,100",notusedskill="31402"  }, --"잔상공격" : 넥스트액션으로 즉사광선->눈감기->잠자기 실행 #6
   { skill_index = 31391, cooltime = 55000, rate = 50, rangemin = 0, rangemax = 5500, target = 3 ,encountertime=20000 }, -- "외쳐밀쳐내기" #7
   { skill_index = 31402, cooltime = 40000, rate = 100, rangemin = 0, rangemax = 2500, target = 1,selfhppercentrange="0,50",notusedskill="31402"},  --"격노" : 체력 40프로 이하부터 격노상태로 변환. #8
   { skill_index = 31387, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4500, next_lua_skill_index = 10,target = 3,selfhppercentrange="0,50",usedskill="31402"  }, -- "즉사광선" #9
   { skill_index = 31388, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4500, next_lua_skill_index = 11,target = 3,selfhppercentrange="0,50",usedskill="31402"  }, -- "눈감기" #10
   { skill_index = 31399, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4500, next_lua_skill_index = 3,target = 3,selfhppercentrange="0,50",usedskill="31402"  }, -- "잠자기" #11
   { skill_index = 31386, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 4500, next_lua_skill_index = 9,target = 3,encountertime=60000,globalcooltime=1,selfhppercentrange="0,50",usedskill="31402"  }, --"잔상공격" : 넥스트액션으로 즉사광선->눈감기->잠자기 실행 #12
}
   