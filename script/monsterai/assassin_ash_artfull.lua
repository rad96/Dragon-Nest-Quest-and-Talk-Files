
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;


g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|Air", "Down" },
 State2 = {"Move|Attack", "Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Hit","Down","Stiff"},
}


g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 10000

g_Lua_Near1 = 
{ 
	 
}
g_Lua_Near2 = 
{ 
    
}

g_Lua_Near3 = 
{ 
     
}
g_Lua_Near4 = 
{ 
    
}
g_Lua_Near5 = 
{ 
    
}

g_Lua_Assault = { 
   
}

g_Lua_NonDownMeleeDamage = {
     
}

g_Lua_NonDownRangeDamage = {

	
}

g_Lua_Skill = { 
-- 0쉬프트 블로우EX
     { skill_index = 6800, cooltime = 0, rate = -1, rangemin = 0, rangemax = 15000, target = 3},
-- 1이즈나 드롭EX
	 { skill_index = 6801, cooltime = 0, rate = -1, rangemin = 0, rangemax = 200, target = 3},
-- 2아트풀체이서EX
     { skill_index = 6802, cooltime = 0, rate = -1, rangemin = 0, rangemax = 15000, target = 3},
-- 3ordermon스킬	 
	 {skill_index= 6802,cooltime=0,rate=100,rangemin=0,rangemax=1500,target=2,selfhppercent=100,waitorder= 6802, priority=1},	 
}