--GuildWar_Ballista.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 2000;
g_Lua_NearValue2 = 2100;
g_Lua_NearValue3 = 2200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Turn_Left", rate = 5, loop = 1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 5, loop = 1, td = "RF,RB,BR" },
   { action_name = "Attack1", rate = 15, loop = 1, td = "FL,FR", cooltime = 14000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
}
