
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
     { "Walk_Front" },
     { "Move_Front" },
     { "Attack2_Slash_ArcBishop" },
  },
}



g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 1 },
   { action_name = "Walk_Back", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 2 },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Attack1_Chopping", rate = 7, cooltime = 3000, loop = 1  },
   { action_name = "Attack2_Slash_ArcBishop", rate = 7, cooltime = 3000, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "CustomAction1", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "CustomAction1", rate = 2, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "CustomAction1", rate = 2, loop = 1 },
}