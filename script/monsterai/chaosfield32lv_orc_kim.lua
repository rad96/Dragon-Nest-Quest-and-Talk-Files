--AiOrc_Green_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}

g_Lua_Skill = { 
   { skill_index = 32632, cooltime = 15000, rate = 100, rangemin = 0, rangemax = 3500, target = 3, next_lua_skill_index = 1 },
   { skill_index = 32633, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3500, target = 3 },
}