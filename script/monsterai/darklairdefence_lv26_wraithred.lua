--DarkLairDefence_Lv26_WraithRed.lua
--커맨더 모드, 레이스 대빵임
--대장은 뒤쪽에만 있음, 다가가기 어렵고, 다가가도 순간 이동을 많이 하는 편
--공격 빈도가 낮기 때문이 이동을 제외하고는 때리는데 어려운편은 아님
--좌우이동이 높은편

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 7, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 15, loop = 1  },
   { action_name = "Move_Right", rate = 15, loop = 1  },
   { action_name = "Attack1_Cut", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 15, loop = 2  },
   { action_name = "Walk_Right", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 1  },
   { action_name = "Move_Right", rate = 10, loop = 1  },
   { action_name = "Attack3_ThrowScythe", rate = 9, loop = 1, cooltime = 26000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 12, loop = 1  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 17, loop = 2  },
   { action_name = "Walk_Right", rate = 17, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Attack5_DashAttack", rate = 9, loop = 1, cooltime = 19000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 12, loop = 1  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 17, loop = 2  },
   { action_name = "Walk_Right", rate = 17, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
}