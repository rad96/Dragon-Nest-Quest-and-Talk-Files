-- Manticore_Lord Hard A.I
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 500.0;
g_Lua_NearValue2 = 1000.0;
g_Lua_NearValue3 = 1500.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 500;
g_Lua_AssualtTime = 3000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}


g_Lua_CustomAction =
{
	CustomAction1 = 
	{
      			{ "Attack11_LHook_Lord", 0},
      			{ "Walk_Back", 0},
			{ "Attack06_GravityThorn", 0},	 		
	},
	CustomAction2 = 
	{
      			{ "Attack11_RHook_Lord", 0},
      			{ "Walk_Back", 0},
			{ "Attack06_GravityThorn", 0},		
	},
}
g_Lua_Near1 = 
{ 
	{ action_name = "Stand",		rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,RF" },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",		rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,RF" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",		rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,RF" },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",		rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,RF" },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",		rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,RF" },
}

g_Lua_Skill = { 
-- 발구르기30011, 레이즈그라비티30015 30016, 에어그라비티볼 30019
-- 레이즈그라비티1
   { skill_index = 30015,  cooltime = 1000, rate = 80, rangemin = 0, rangemax = 1800, target = 3, td = "FL,FR,RB,BR,BL,LB" },
}
