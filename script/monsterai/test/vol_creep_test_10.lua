--/genmon 51374

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000
g_Lua_NearValue2 = 6000


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 8000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 16, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}


g_Lua_Skill = { 
   { skill_index = 36050, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, encountertime=5000 },
}