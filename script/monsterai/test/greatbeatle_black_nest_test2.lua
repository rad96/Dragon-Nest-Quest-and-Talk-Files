--AiGreatBeatle_Black_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 8, loop = 1, approach = 200 },
}
g_Lua_Skill = { 
   { skill_index = 33117, cooltime = 20000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, encountertime = 5000 },
}