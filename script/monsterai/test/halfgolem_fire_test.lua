--SeaDragonNest_Gate7_HalfGolem_Fire_Boss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1800;
g_Lua_NearValue5 = 15000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
}


g_Lua_Skill = { 
--파이어볼
   { skill_index = 30539,  cooltime = 1000, rate = 100, rangemin = 300, rangemax = 2000, target = 3 },
--이그니션
   { skill_index = 30540,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, blowcheck = "42" },
}
