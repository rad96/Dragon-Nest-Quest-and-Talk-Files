
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|!Air", "Down" },
 State2 = {"!Air", "!Down" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"!Down|Hit","Air|Down"}, 
 State5 = {"!Move","Air"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 평타 시작
  CustomAction6 = {
     { "useskill", lua_skill_index = 17, rate = 100 },
  },
  -- 딥스러쉬 이후 후속타
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 10, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 10000

g_Lua_Near1 = 
{ 
     { action_name = "Attack_Down", rate = 50, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Move_Left", rate = 40, loop = 4, cooltime = 5000 },
     { action_name = "Move_Right", rate = 40, loop = 4, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 40, loop = 7 },
     { action_name = "Move_Right", rate = 40, loop = 7 },
	 { action_name = "Move_Back", rate = 40, loop = 3 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},

}
g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 30, loop = 7 },
     { action_name = "Move_Left", rate = 30, loop = 7 },
     { action_name = "Move_Right", rate = 30, loop = 7 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 30, loop = 7 },
     { action_name = "Move_Left", rate = 30, loop = 7 },
     { action_name = "Move_Right", rate = 30, loop = 7 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 30, loop = 7 },
}

g_Lua_Assault = { 
    
}

g_Lua_NonDownMeleeDamage = {
   
}

g_Lua_NonDownRangeDamage = {
   
}

g_Lua_RandomSkill ={
{
{ -- 트라이앵글샷 패턴 테스트
   { skill_index = 34211, rate= 20 }, 
   { skill_index = 34212, rate= 20 },
   { skill_index = 34213, rate= 15 },
   { skill_index = 34214, rate= 15 },
   { skill_index = 34215, rate= 15 },
   { skill_index = 34216, rate= 15 },
},
}
}

g_Lua_Skill = { 


-- 차지샷
     { skill_index = 96602, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, cancellook = 1 ,  priority= 3 },

	 
	 }