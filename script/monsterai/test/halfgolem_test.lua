--HalfGolem_StoneMk2_Elite_Abyss.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 1500;
g_Lua_NearValue2 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
}
g_Lua_Skill = { 
   -- 굴러라!
   { skill_index = 20766, cooltime = 8000, rate = 100, rangemin = 0, rangemax = 3000, target = 3 }, 
}
