--AiCube_White_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 2  },
}
g_Lua_Skill = { 
   -- Ʈ��
   { skill_index = 31737, cooltime = 15000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "99,100" },
   -- ������ ��ź : ����Ʈ ���
   { skill_index = 31731, cooltime = 15000, rate = 100, rangemin = 800, rangemax = 1500, target = 3, selfhppercentrange = "80,95" },
   -- ���⼺ ��ź : ����Ʈ ���
   { skill_index = 31732, cooltime = 15000, rate = 100, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "80,95" },
   -- ���� : ����Ʈ ������
   { skill_index = 31733, cooltime = 15000, rate = 100, rangemin = 800, rangemax = 1500, target = 3, selfhppercentrange = "50,78" },
   -- ȸ��ź : ����Ʈ ����
   { skill_index = 31734, cooltime = 15000, rate = 100, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "50,78" },
   -- ȥ��
   { skill_index = 31735, cooltime = 15000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "0,45" },
}
