--AiCarbuncle_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Left", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Right", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Back", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Move_Front", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Left", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Right", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Back", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Attack4_Saw", rate = 5, loop = 1, selfhppercentrange = "50,75" },
   { action_name = "Attack2_Teleport", rate = 5, loop = 1, selfhppercentrange = "25,50" },
   { action_name = "Attack3_Bite", rate = 5, loop = 1, selfhppercentrange = "0,25" },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Front", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Left", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Right", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Back", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Move_Front", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Left", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Right", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Back", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Attack4_Saw", rate = 5, loop = 1, selfhppercentrange = "50,75" },
   { action_name = "Attack2_Teleport", rate = 5, loop = 1, selfhppercentrange = "25,50" },
   { action_name = "Attack3_Bite", rate = 5, loop = 1, selfhppercentrange = "0,25" },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Front", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Left", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Right", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Back", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Move_Front", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Left", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Right", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Back", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Attack4_Saw", rate = 5, loop = 1, selfhppercentrange = "50,75" },
   { action_name = "Attack2_Teleport", rate = 5, loop = 1, selfhppercentrange = "25,50" },
   { action_name = "Attack3_Bite", rate = 5, loop = 1, selfhppercentrange = "0,25" },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Left", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Right", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Walk_Back", rate = 5, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Move_Front", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Left", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Right", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Move_Back", rate = 5, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Attack4_Saw", rate = 5, loop = 1, selfhppercentrange = "50,75" },
   { action_name = "Attack2_Teleport", rate = 5, loop = 1, selfhppercentrange = "25,50" },
   { action_name = "Attack3_Bite", rate = 5, loop = 1, selfhppercentrange = "0,25" },
}