--AiCarbuncle_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State =  
 { 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
 State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
 }

g_Lua_CustomAction = {
-- ��ġ�� ���� > ��� ����
   CustomAction1 = {
       { "Attack05_Swoop" },
       { "Attack06_Thrusting" },
   },
}
g_Lua_Near1 = { 
   { action_name = "Attack06_Thrusting", rate = 30, loop = 1, cooltime = 23000, target_condition = "State1" },
   { action_name = "Stand", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 3, loop = 3 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
}

g_Lua_Skill = { 
   { skill_index = 20941,  cooltime = 10000, rate = 80,rangemin = 0, rangemax = 250, target = 3, selfhppercent = 100 },
   { skill_index = 20942,  cooltime = 10000, rate = 80,rangemin = 50, rangemax = 400, target = 3, td = "FL,FR", selfhppercent = 100 },
   { skill_index = 20943,  cooltime = 10000, rate = 80,rangemin = 300, rangemax = 900, target = 3, selfhppercent = 100 },
}