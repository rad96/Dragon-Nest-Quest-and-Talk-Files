--AiCalamary_Red_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1600;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 33102,  cooltime = 10000, rate = 100, rangemin = 1000, rangemax = 5500, target = 3, selfhppercentrange = "0,75" },
--   { skill_index = 33101,  cooltime = 10000, rate = 40, rangemin = 0, rangemax = 500, target = 3, selfhppercentrange = "0,75" },
}
