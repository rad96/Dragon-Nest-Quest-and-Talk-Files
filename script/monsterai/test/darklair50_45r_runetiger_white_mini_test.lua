--RuneTiger_White_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1},
   { action_name = "Walk_Left", rate = 3, loop = 1},
   { action_name = "Walk_Right", rate = 3, loop = 1},
   { action_name = "Walk_Back", rate = 12, loop = 1},
   { action_name = "Attack1_Claw", rate = 22, loop = 1, td = "FL,FR" },
--   { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,BL,LB" },
--   { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1},
   { action_name = "Walk_Left", rate = 5, loop = 1},
   { action_name = "Walk_Right", rate = 5, loop = 1},
   { action_name = "Walk_Front", rate = 10, loop = 1},
   { action_name = "Walk_Back", rate = 5, loop = 1},
   { action_name = "Assault", rate = 27, loop = 1},
--   { action_name = "Turn_Left", rate = 3, loop = -1, td = "LF,BL,LB" },
--   { action_name = "Turn_Right", rate = 3, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 20, loop = 1},
   { action_name = "Walk_Left", rate = 3, loop = 1},
   { action_name = "Walk_Right", rate = 3, loop = 1},
   { action_name = "Walk_Front", rate = 8, loop = 1},
   { action_name = "Walk_Back", rate = 3, loop = 1},
   { action_name = "Assault", rate = 27, loop = 1},
--   { action_name = "Turn_Left", rate = 3, loop = -1, td = "LF,BL,LB" },
--  { action_name = "Turn_Right", rate = 3, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 20, loop = 1},
   { action_name = "Walk_Left", rate = 5, loop = 1},
   { action_name = "Walk_Right", rate = 5, loop = 1},
   { action_name = "Walk_Front", rate = 15, loop = 2},
   { action_name = "Move_Left", rate = 3, loop = 1},
   { action_name = "Move_Right", rate = 3, loop = 1},
   { action_name = "Move_Front", rate = 5, loop = 1},
   { action_name = "Attack2_JumpBite", rate = -1, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 40, loop = 1},
--   { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,BL,LB" },
--   { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1},
   { action_name = "Walk_Left", rate = 10, loop = 1},
   { action_name = "Walk_Right", rate = 10, loop = 1},
   { action_name = "Walk_Front", rate = 10, loop = 2},
   { action_name = "Move_Left", rate = 10, loop = 1},
   { action_name = "Move_Right", rate = 10, loop = 1},
   { action_name = "Move_Front", rate = 20, loop = 1},
--   { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,BL,LB" },
--   { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 10, loop = 1, approach = 250.0  },
}