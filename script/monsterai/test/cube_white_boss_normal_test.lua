--AiCube_White_Boss_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

-- g_Lua_CustomAction = {
   -- CustomAction1 = {
       -- { "Attack9_Spin_Boss_Start_Test" },
       -- { "Attack9_Spin_Boss_Loop_Test" },
       -- { "Attack9_Spin_Boss_End_Test" },
   -- },
-- }


-- g_Lua_State = {
  -- State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
-- }

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
}

g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
}


g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
}


g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
}


g_Lua_Skill = { 
   { skill_index = 31734, cooltime = 12000, rate = 100, rangemin = 0, rangemax = 4500, target = 3 },
   { skill_index = 33053, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 4500, target = 3, selfhppercentrange = "75,99" },
   { skill_index = 20929, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 500, target = 3, selfhppercentrange = "50,75" },
   { skill_index = 33053, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 500, target = 3,}, 
   { skill_index = 33055, cooltime = 5000, rate = -1, rangemin = 400, rangemax = 1500, target = 3, selfhppercentrange = "50,75" },
}
