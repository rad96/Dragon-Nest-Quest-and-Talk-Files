--AiOgre_Black_Normal.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 1500;
g_Lua_NearValue2 = 2000;
-- g_Lua_NearValue3 = 600;
-- g_Lua_NearValue4 = 800;
-- g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   -- { action_name = "Stand_1", rate = 5, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 1  },
   -- { action_name = "Walk_Right", rate = 5, loop = 1  },
   -- { action_name = "Walk_Front", rate = 5, loop = 1  },
   -- { action_name = "Walk_Back", rate = 5, loop = 1  },
   -- { action_name = "Attack_Jinyoung_Test(3Wave)", rate = 20, loop = 1  },
   -- { action_name = "Attack_Jinyoung_Test2(FireWall)", rate = 20, loop = 1  },
   -- { action_name = "Attack_Jinyoung_Test3(Howl)", rate = 20, loop = 1  },
   -- { action_name = "Attack_Jinyoung_Test4(Combo)", rate = 20, loop = 1  },
   -- { action_name = "Attack_Jinyoung_Test5(Start)", rate = 20, loop = 1  },
   -- { action_name = "Attack_Jinyoung_Test6(CrossAttack)", rate = 20, loop = 1  },
   { action_name = "Attack_Jinyoung_Test7(Combo-Shoot1)", rate = 50, loop = 1  },
}
g_Lua_Near2 = { 
   -- { action_name = "Stand_1", rate = 5, loop = 1  },
   -- { action_name = "Walk_Left", rate = 10, loop = 1  },
   -- { action_name = "Walk_Right", rate = 10, loop = 1  },
   -- { action_name = "Walk_Front", rate = 15, loop = 1  },
   -- { action_name = "Walk_Back", rate = 10, loop = 1  },
   -- { action_name = "Attack1_bash", rate = 4, loop = 1  },
   -- { action_name = "Attack_Jinyoung_Test", rate = 50, loop = 1  },
}
-- g_Lua_Near3 = { 
   -- { action_name = "Stand_1", rate = 1, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 1  },
   -- { action_name = "Walk_Right", rate = 5, loop = 1  },
   -- { action_name = "Walk_Front", rate = 10, loop = 2  },
   -- { action_name = "Move_Front", rate = 30, loop = 2  },
   -- { action_name = "Attack13_Dash", rate = 10, loop = 1, cooltime = 20000 },
   -- { action_name = "Assault", rate = 4, loop = 1  },
-- }
-- g_Lua_Near4 = { 
   -- { action_name = "Stand_1", rate = 5, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 1  },
   -- { action_name = "Walk_Right", rate = 5, loop = 1  },
   -- { action_name = "Walk_Front", rate = 20, loop = 2  },
   -- { action_name = "Move_Front", rate = 50, loop = 1  },
   -- { action_name = "Attack7_JumpAttack", rate = -1, loop = 1  },
   -- { action_name = "Attack13_Dash", rate = 14, loop = 1, cooltime = 20000 },
   -- { action_name = "Assault", rate = 6, loop = 1  },
-- }
-- g_Lua_Near5 = { 
   -- { action_name = "Stand_1", rate = 10, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 2  },
   -- { action_name = "Walk_Right", rate = 5, loop = 2  },
   -- { action_name = "Walk_Front", rate = 10, loop = 2  },
   -- { action_name = "Move_Front", rate = 30, loop = 1  },
   -- { action_name = "Attack13_Dash", rate = 12, loop = 1, cooltime = 20000 },
   -- { action_name = "Assault", rate = 4, loop = 1  },
-- }
g_Lua_NonDownRangeDamage = { 
   -- { action_name = "Move_Front", rate = 30, loop = 1  },
   -- { action_name = "Move_Left", rate = 3, loop = 3  },
   -- { action_name = "Move_Right", rate = 3, loop = 3  },
   -- { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Assault = { 
   -- { action_name = "Attack1_bash", rate = 5, loop = 1, approach = 250.0  },
   -- { action_name = "Attack3_Upper", rate = 3, loop = 1, approach = 300.0  },
}
