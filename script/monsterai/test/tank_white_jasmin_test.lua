--AiGreatBeatleBig_Green_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 900;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
   -- { action_name = "Walk_Left", rate = 8, loop = 3 },
   -- { action_name = "Walk_Right", rate = 8, loop = 3 },
   -- { action_name = "Walk_Back", rate = 8, loop = 1 },
   }
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   -- { action_name = "Walk_Front", rate = 10, loop = 1 },
   -- { action_name = "Walk_Left", rate = 5, loop = 3 },
   -- { action_name = "Walk_Right", rate = 5, loop = 3 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   -- { action_name = "Walk_Front", rate = 12, loop = 1 },
   -- { action_name = "Move_Front", rate = 12, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   -- { action_name = "Walk_Front", rate = 8, loop = 1 },
   -- { action_name = "Move_Front", rate = 12, loop = 2 },
}
-- g_Lua_Assault = { 
   -- { action_name = "Stand_1", rate = 8, loop = 1, approach = 300 },
   -- { action_name = "Walk_Left", rate = 4, loop = 1, approach = 300 },
   -- { action_name = "Walk_Right", rate = 4, loop = 1, approach = 300 },
-- }
g_Lua_Skill = { 
   -- { skill_index = 21006,  cooltime = 12000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "80,100"  }, -- 점프
   -- { skill_index = 21007,  cooltime = 12000, rate = 100, rangemin = 700, rangemax = 3500, target = 3, selfhppercentrange = "80,100"  }, -- 산탄
   -- { skill_index = 21008,  cooltime = 12000, rate = 100, rangemin = 700, rangemax = 3500, target = 3, selfhppercentrange = "40,80"  }, -- 포신회전
   -- { skill_index = 21009,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "40,80"  }, -- 호밍 돌진
   -- { skill_index = 21010,  cooltime = 15000, rate = 100, rangemin = 700, rangemax = 3500, target = 3, selfhppercentrange = "0,40"  }, -- 시즈모드(전기포)
   { skill_index = 21011,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 3700, target = 3, selfhppercentrange = "0,100"  }, -- 전기 지뢰
}
