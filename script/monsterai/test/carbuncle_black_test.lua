--AiCarbuncle_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Attack3_Big", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "99,100" },
   { action_name = "Attack5_Crash", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "50,99" },
   { action_name = "Attack1_TailAttack", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "0,50" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Attack3_Big", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "99,100" },
   { action_name = "Attack5_Crash", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "50,99" },
   { action_name = "Attack1_TailAttack", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "0,50" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Attack3_Big", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "99,100" },
   { action_name = "Attack5_Crash", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "50,99" },
   { action_name = "Attack1_TailAttack", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "0,50" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Attack3_Big", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "99,100" },
   { action_name = "Attack5_Crash", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "50,99" },
   { action_name = "Attack1_TailAttack", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "0,50" },
}