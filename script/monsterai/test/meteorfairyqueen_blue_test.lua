--AiMeteorFairyQueen_Blue_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Attack04_IceSwordAttack1", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack2", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack3", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack4", rate = 16, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Attack04_IceSwordAttack1", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack2", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack3", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack4", rate = 16, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Attack04_IceSwordAttack1", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack2", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack3", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack4", rate = 16, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Attack04_IceSwordAttack1", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack2", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack3", rate = 16, loop = 1 },
   { action_name = "Attack04_IceSwordAttack4", rate = 16, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 8, loop = 1, approach = 300 },
}
g_Lua_Skill = { 
   { skill_index = 33126,  cooltime = 15000, rate = -1, rangemin = 400, rangemax = 1500, target = 3, selfhppercentrange = "99,100" },
   { skill_index = 33130,  cooltime = 15000, rate = -1, rangemin = 400, rangemax = 1500, target = 3, selfhppercentrange = "80,99" },
   { skill_index = 33131,  cooltime = 15000, rate = -1, rangemin = 400, rangemax = 1500, target = 3, selfhppercentrange = "70,80" },
   { skill_index = 33132,  cooltime = 15000, rate = -1, rangemin = 400, rangemax = 1500, target = 3, selfhppercentrange = "60,70" },
   { skill_index = 33133,  cooltime = 15000, rate = 100, rangemin = 400, rangemax = 1500, target = 3, selfhppercentrange = "50,60" },
}
