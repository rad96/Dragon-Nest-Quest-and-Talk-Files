--AiCarbuncle_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
--   { action_name = "Attack01_Slash", rate = 6, loop = 1, max_missradian = 30 },
   { action_name = "Attack08_BustBuff", rate = 30, loop = 1},
--   { action_name = "Attack9_BeetleArrows", rate = 6, loop = 1, max_missradian = 30 },
--   { action_name = "Attack10_Guard", rate = 6, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
--   { action_name = "Attack01_Slash", rate = 6, loop = 1, max_missradian = 30 },
   { action_name = "Attack08_BustBuff", rate = 30, loop = 1},
--   { action_name = "Attack9_BeetleArrows", rate = 6, loop = 1, max_missradian = 30 },
--   { action_name = "Attack10_Guard", rate = 6, loop = 1, max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
--   { action_name = "Attack01_Slash", rate = 6, loop = 1, max_missradian = 30 },
   { action_name = "Attack08_BustBuff", rate = 30, loop = 1},
--   { action_name = "Attack9_BeetleArrows", rate = 6, loop = 1, max_missradian = 30 },
--   { action_name = "Attack10_Guard", rate = 6, loop = 1, max_missradian = 30 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 3, loop = 3 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
--   { action_name = "Attack01_Slash", rate = 6, loop = 1, max_missradian = 30 },
--   { action_name = "Attack8_BustBuff", rate = 6, loop = 1, max_missradian = 30 },
--   { action_name = "Attack9_BeetleArrows", rate = 6, loop = 1, max_missradian = 30 },
--   { action_name = "Attack10_Guard", rate = 6, loop = 1, max_missradian = 30 },
}

g_Lua_Skill = { 
   { skill_index = 20872,  cooltime = 10000, rate = 80,rangemin = 500, rangemax = 1000, target = 3, selfhppercent = 100 },
   { skill_index = 20873,  cooltime = 10000, rate = 80,rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100 },
}