--SeaDragonNest_Boss_Golem_Green.lua
--기본공격이 거리가 되는 근거리, 원거리 최대 사거리의 중거리, 그 이상의 원거리
--Assult로 돌진후 회전공격 하도록(특징적인 공격)
--기본공격은 1~3페이즈의 확률을 동일하게, 4페이즈는 확률 상승
--칵퉤꽃 소환 시그널 ID : 2357310, 시그널 받고 10초 후 칵퉤꽃 소환
--구울 소환 시그널 ID : 2357311
--/genmon 235737

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 5000

g_Lua_SkillProcessor = {
   { skill_index = 30515, changetarget = "2000,0" },
}

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 5, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 5, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 5, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack6_SpinAttack_SeaDragon", rate = 30, loop = 1, approach = 250.0  },
}

g_Lua_Skill = { 
--독안개(2페이즈에서만 사용, 칵퉤꽃 소환 시그널)
   { skill_index = 30522, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 3500, target = 3 },
}