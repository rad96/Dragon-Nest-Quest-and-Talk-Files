--AiAsaiShaman_Lich_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 250
g_Lua_AssualtTime = 4000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Attack1_Pierce", rate = 20, loop = 1 , max_missradian = 20 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 7, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
}
g_Lua_Skill = { 
   -- 독양병던지기
   { skill_index = 31721,  cooltime = 15000, rate = 100, rangemin = 300, rangemax = 800, target = 3, selfhppercentrange = "99,100" },
   -- 슬로우댄스
   { skill_index = 31722,  cooltime = 15000, rate = 100, rangemin = 300, rangemax = 800, target = 3, selfhppercent = 80 },
}