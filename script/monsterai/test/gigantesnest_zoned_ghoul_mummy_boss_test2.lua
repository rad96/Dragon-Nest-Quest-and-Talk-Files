--AiGhoul_Blue_Boss_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 600;
g_Lua_NearValue5 = 1000;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
}
g_Lua_Skill = { 
   -- ���� ������
   { skill_index = 31781,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "95,99" },
   -- �޼� ������
   { skill_index = 31782,  cooltime = 15000, rate = 100, rangemin = 800, rangemax = 1500, target = 3, selfhppercentrange = "95,99" },
   -- ����
   { skill_index = 31783,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "80,95" },
   -- ����Ÿ��
   { skill_index = 31784,  cooltime = 15000, rate = 100, rangemin = 800, rangemax = 1500, target = 3, selfhppercentrange = "80,95" },
   -- ���屸�� ��ȯ
   { skill_index = 31786,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "50,75" },
   -- ���� ��ȯ
   { skill_index = 31787,  cooltime = 15000, rate = 100, rangemin = 800, rangemax = 1500, target = 3, selfhppercentrange = "50,75" },
   -- �� ��ȯ
   { skill_index = 31788,  cooltime = 15000, rate = 100, rangemin = 800, rangemax = 1500, target = 3, selfhppercentrange = "0,45" },
   -- ���� ��ġ��
   { skill_index = 31801,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "0,45" },
}
