--HalfGolem_StoneMk2_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 9000;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
  
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
  
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
}
g_Lua_Skill = { 
   -- 스톤펀치
   { skill_index = 31701,  cooltime = 15000, rate = 100, rangemin = 100, rangemax = 700, target = 3, selfhppercentrange = "99,100" }, 
   -- 어스웨이브
   { skill_index = 31702,  cooltime = 15000, rate = 100, rangemin = 800, rangemax = 2500, target = 3, selfhppercentrange = "99,100" },
   -- 충격파
   { skill_index = 31703,  cooltime = 15000, rate = 100, rangemin = 100, rangemax = 500, target = 3, selfhppercentrange = "90,95" },
   -- 바위던지기
   { skill_index = 31704,  cooltime = 15000, rate = 100, rangemin = 500, rangemax = 1500, target = 3, selfhppercentrange = "90,95" },
   -- 구르기
   { skill_index = 31705,  cooltime = 15000, rate = 100, rangemin = 100, rangemax = 500, target = 3, selfhppercent = 80 },
   -- 대폭발
   { skill_index = 31706,  cooltime = 15000, rate = 100, rangemin = 500, rangemax = 1500, target = 3, selfhppercent = 80 },
}