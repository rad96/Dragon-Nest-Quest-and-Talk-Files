--HalfGolem_Iron_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 1000;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 1400;
g_Lua_NearValue4 = 1600;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack14_Stomp_Start", 0 },
      { "Attack14_Stomp_End", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Attack9_HeadAttack", rate = 6, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Attack14_Stomp_Start", rate = 6, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Attack15_SummonBigMushroom", rate = 6, loop = 1, selfhppercentrange = "50,75" },
   { action_name = "Attack5_Earthquake", rate = 6, loop = 1, selfhppercentrange = "0,50" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Attack9_HeadAttack", rate = 6, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Attack14_Stomp_Start", rate = 6, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Attack15_SummonBigMushroom", rate = 6, loop = 1, selfhppercentrange = "50,75" },
   { action_name = "Attack5_Earthquake", rate = 6, loop = 1, selfhppercentrange = "0,50" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Attack9_HeadAttack", rate = 6, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Attack14_Stomp_Start", rate = 6, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Attack15_SummonBigMushroom", rate = 6, loop = 1, selfhppercentrange = "50,75" },
   { action_name = "Attack5_Earthquake", rate = 6, loop = 1, selfhppercentrange = "0,50" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Attack9_HeadAttack", rate = 6, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "Attack14_Stomp_Start", rate = 6, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "Attack15_SummonBigMushroom", rate = 6, loop = 1, selfhppercentrange = "50,75" },
   { action_name = "Attack5_Earthquake", rate = 6, loop = 1, selfhppercentrange = "0,50" },
}