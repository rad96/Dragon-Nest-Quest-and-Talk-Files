
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|Air", "Down" },
 State2 = {"Move|Attack", "Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Hit","Down","Stiff"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
     { "Skill_DashCombo" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
     { "Skill_DashCombo" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 이베이전슬래쉬
  CustomAction6 = {
     { "useskill", lua_skill_index = 28, rate = 100 },
  },
  -- 딥스
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 10, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 10000

g_Lua_Near1 = 
{ 
     { action_name = "Attack_Down", rate = 50, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Move_Left", rate = 40, loop = 4, cooltime = 5000 },
     { action_name = "Move_Right", rate = 40, loop = 4, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 40, loop = 7 },
     { action_name = "Move_Right", rate = 40, loop = 7 },
	 { action_name = "Move_Back", rate = 40, loop = 3 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},

}
g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 30, loop = 7 },
     { action_name = "Move_Left", rate = 30, loop = 7 },
     { action_name = "Move_Right", rate = 30, loop = 7 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 30, loop = 7 },
     { action_name = "Move_Left", rate = 30, loop = 7 },
     { action_name = "Move_Right", rate = 30, loop = 7 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 30, loop = 3 },
}

g_Lua_Assault = { 
     --{ action_name = "SKill_Dash", rate = -1, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Move_Back", rate = 10, loop = 1 },
	 { action_name = "Skill_EvasionSlash", rate = 30, loop = 1, cooltime = 15000,},
     { action_name = "CustomAction2", rate = 10, loop = 1  },
     { action_name = "CustomAction3", rate = 10, loop = 1 },
}

g_Lua_NonDownRangeDamage = {

	 --{ action_name = "Skill_DeepStraight_EX_Dash", rate = 30, loop = 1, cooltime = 14400 },
	 --{ action_name = "useskill", lua_skill_index = 9, rate = 50 },
}

g_Lua_Skill = { 
-- 0임팩트펀치
     { skill_index = 82000, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "1,30,2", combo2 = "5,60,2", combo3 = "1,100,2" },-- 액션프레임끊기 nextcustomaction 시그널
	 
-- 1사이드킥
    { skill_index = 82047, cooltime = 3000, rate = 50, rangemin = 0, rangemax = 600, target = 3, combo1 = "2,40,2", combo2 = "1,30,0", combo3 = "5,70,2"},
-- 2스위핑킥
    { skill_index = 82012, cooltime = 0, rate = -1, rangemin = 0, rangemax = 600, target = 3, cancellook = 1, combo1 = "0,70,2", combo2  = "3,80,0", combo5 = "9,100,2" },-- 케릭터 어그로유지 LocktargetLook
	
-- 3헤비슬래쉬
     { skill_index = 82054, cooltime = 8000, rate = 100, rangemin = 0, rangemax = 600, target = 3, combo1 = "0,70,2" , combo2 = "1,80,2", combo3 = "9,80,0",  target_condition = "State1" },
	 
-- 4드롭킥
     { skill_index = 82006, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 400, target = 3,  },

-- 5평타1
     { skill_index = 82049, cooltime = 1000, rate = 70, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 = "1,20,2", combo2 = "6,100,0",  target_condition = "State3" },
-- 6평타2
     { skill_index = 82050, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 = "0,30,0", combo2 = "3,35,0", combo3 = "7,100,2"},
-- 7평타3
     { skill_index = 82051, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 = "8,100,2" , combo2 = "15,100,0" },
-- 8평타4
     { skill_index = 82052, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, cancellook = 1,  combo1 = "9,70,2" },	

	 
-- 9트리플 슬래쉬 EX
     { skill_index = 82036, cooltime = 10000, rate = 30, rangemin = 0, rangemax = 400, target = 3, cancellook = 1, combo1 = "11,50,2", combo2 = "10,100,0" },
-- 10트리플 슬래쉬 EX_L2
     { skill_index = 82037, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "13,50,2", combo2 = "12,100,0"},
-- 11트리플 슬래쉬 EX_R2
     { skill_index = 82038, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "13,50,2", combo2 = "12,100,0" },
-- 12트리플 슬래쉬 EX_L3
     { skill_index = 82039, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "3,100,2",},
-- 13트리플 슬래쉬 EX_R3
     { skill_index = 82040, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "5,100,2"},
	 

	 }