

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 900;
g_Lua_NearValue2 = 1300;
g_Lua_NearValue3 = 1800;
g_Lua_NearValue4 = 2500;
g_Lua_NearValue5 = 10000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 1000
g_Lua_AssualtTime = 5000

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 5, custom_state1 = "custom_ground", loop = 1 },
	{ action_name = "Fly", rate = 10, custom_state1 = "custom_ground", loop = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB", cooltime = 10000 },
	{ action_name = "Fly_Stand", rate = 30, custom_state1 = "custom_Fly", loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 20, custom_state1 = "custom_ground", loop = 1 },
	{ action_name = "Fly", rate = 10, custom_state1 = "custom_ground", loop = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB", cooltime = 10000 },
	{ action_name = "Fly_Stand", rate = 30, custom_state1 = "custom_Fly", loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 20, custom_state1 = "custom_ground", loop = 1 },
	{ action_name = "Fly", rate = 10, custom_state1 = "custom_ground", loop = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB", cooltime = 10000 },
	{ action_name = "Fly_Stand", rate = 30, custom_state1 = "custom_Fly", loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 20, custom_state1 = "custom_ground", loop = 1 },
	{ action_name = "Fly", rate = 10, custom_state1 = "custom_ground", loop = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB", cooltime = 10000 },
	{ action_name = "Fly_Stand", rate = 30, custom_state1 = "custom_Fly", loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand", rate = 20, custom_state1 = "custom_ground", loop = 1 },
	{ action_name = "Fly", rate = 10, custom_state1 = "custom_ground", loop = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB", cooltime = 10000 },
	{ action_name = "Fly_Stand", rate = 30, custom_state1 = "custom_Fly", loop = 1 },
}


g_Lua_BeHitSkill = { 
   { lua_skill_index = 1, rate = 100, skill_index = 30587  },
}

g_Lua_Skill = { 
-- �巡��극��4
   { skill_index = 30601,  cooltime = 1000, rate = -1, custom_state1 = "custom_fly",	rangemin = 0, rangemax = 10000,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- �巡��극��3-->8
   { skill_index = 30600,  cooltime = 1000, rate = 100, custom_state1 = "custom_fly",	rangemin = 0, rangemax = 10000,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = 1, next_lua_skill_index = 0 },
}