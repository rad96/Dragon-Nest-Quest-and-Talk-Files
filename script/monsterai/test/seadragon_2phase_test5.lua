--AiHalfGolem_Stone_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 30000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}


-- 거리별 액션은 모두 동일하게 잡는다.
g_Lua_Near1 = { 
   { action_name = "Stand_2Phase", rate = 10, loop = 2 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2Phase", rate = 10, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2Phase", rate = 10, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2Phase", rate = 10, loop = 2 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_2Phase", rate = 10, loop = 2 },
}

g_Lua_Skill = { 
-- 드래곤브레스5-1step
   { skill_index = 30605,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 4000,target = 3 },
}