--Mimic_Gold_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
--   { action_name = "Attack01_Punch", rate = 20, loop = 1, cooltime = 7000 },
   --{ action_name = "Walk_Left", rate = 5, loop = 1 },
   --{ action_name = "Walk_Right", rate = 5, loop = 1 },
   --{ action_name = "Walk_Back", rate = 2, loop = 1 },
   }
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
--   { action_name = "Attack01_Punch", rate = 20, loop = 1, cooltime = 7000 },
   --{ action_name = "Walk_Left", rate = 5, loop = 1 },
   --{ action_name = "Walk_Right", rate = 5, loop = 1 },
   --{ action_name = "Walk_Front", rate = 5, loop = 1 },
   --{ action_name = "Walk_Back", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   --{ action_name = "Walk_Left", rate = 5, loop = 1 },
   --{ action_name = "Walk_Right", rate = 5, loop = 1 },
   --{ action_name = "Walk_Front", rate = 5, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
--   { action_name = "Walk_Front", rate = 5, loop = 1 },
}
g_Lua_Skill = { 
   -- 좌그로기
   --{ skill_index = 31831,  cooltime = 7000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "50,100" },
   -- 우그로기
   --{ skill_index = 31832,  cooltime = 7000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "0,50" },
   -- 포탄펀치
   --{ skill_index = 31833,  cooltime = 7000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "99,100" },
   -- 내리치기
--   { skill_index = 31834,  cooltime = 7000, rate = 100, rangemin = 700, rangemax = 3000, target = 3, selfhppercentrange = "99,100" },
   -- 내리치기(연계)
   { skill_index = 31835,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,4"  },
   -- 양손내리치기
--   { skill_index = 31836,  cooltime = 7000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "99,100" },
   -- 포탄 포격
--   { skill_index = 31837,  cooltime = 7000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "80,99" },
   -- 돌진
--   { skill_index = 31838,  cooltime = 7000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "60,79" },
   -- 양발구르기
   --{ skill_index = 31839,  cooltime = 7000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "0,49", next_lua_skill_index=0 },
   -- 자폭장치
--   { skill_index = 31840,  cooltime = 7000, rate = 100, rangemin = 700, rangemax = 3000, target = 3, selfhppercentrange = "40,59" },
   -- 클러스터폭탄
--   { skill_index = 31841,  cooltime = 7000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "40,59" },
   -- 빔포
--   { skill_index = 31842,  cooltime = 7000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "20,39" },
   -- 흡수
--   { skill_index = 31843,  cooltime = 7000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,19" },
   -- 미사일난사
   --{ skill_index = 31844,  cooltime = 7000, rate = 100, rangemin = 700, rangemax = 3000, target = 3, selfhppercentrange = "99,100" },
   -- 미사일난사(전방향)
   --{ skill_index = 31845,  cooltime = 7000, rate = 100, rangemin = 700, rangemax = 3000, target = 3, selfhppercentrange = "99,100" },
}
