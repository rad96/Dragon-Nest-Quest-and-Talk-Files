-- Apocalypse Normal A.I
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 600.0;
g_Lua_NearValue2 = 1000.0;
g_Lua_NearValue3 = 1500.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 500;
g_Lua_AssualtTime = 5000;

g_Lua_CustomAction =
{
	CustomAction1 = 
	{
      			{ "Attack001_ClawMelee1"  },
			{ "Attack003_Assault"  },	 
	},
	CustomAction2 = 
	{
      			{ "Attack002_ClawRange1"  },
	},
	CustomAction3 = 
	{
      			{ "Attack001_ClawMelee1" },
      			{ "Move_Front", 0 },
			{ "Attack003_Assault", 0 },	 		
	},
	CustomAction4 = 
	{
      			{ "Move_Front" },
			{ "Attack003_Assault" },	 
	},
	CustomAction5 = 
	{
      			{ "Attack001_ClawMelee1" },
	},
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand",		rate = 8,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100" },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand",		rate = 8,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",		rate = 8,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100" },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",		rate = 8,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100" },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",		rate = 8,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100" },
}


g_Lua_Skill = { 
-- ������1
   { skill_index = 30208,  cooltime = 1000, rate = 90, 	rangemin = 0, rangemax = 2000, 	target = 3, td = "FL,RF" },
}