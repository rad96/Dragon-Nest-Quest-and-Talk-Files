--AiDarkElf_Red_Elite_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  StateID = {"State", "Action" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
}

g_Lua_Skill = { 
   { skill_index = 20059,  cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1500, target = 3 },
}
