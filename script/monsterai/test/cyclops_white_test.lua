--AiCyclops_Green_Boss_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 30000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand_5",			rate = 10,	loop = 1, td = "FL,FR,RB,BR,BL,LB" },
}
g_Lua_Near2 = { 
	{ action_name = "Stand_5",			rate = 10,	loop = 1, td = "FL,FR,RB,BR,BL,LB" },
}
g_Lua_Near3 = { 
	{ action_name = "Stand_5",			rate = 10,	loop = 1, td = "FL,FR,RB,BR,BL,LB" },
}
g_Lua_Near4 = { 
	{ action_name = "Stand_5",			rate = 10,	loop = 1, td = "FL,FR,RB,BR,BL,LB" },
}
g_Lua_Near5 = { 
	{ action_name = "Stand_5",			rate = 10,	loop = 1, td = "FL,FR,RB,BR,BL,LB" },
}

g_Lua_BeHitSkill = { 
   { lua_skill_index = 1, rate = 100, skill_index = 30315  },
}

g_Lua_Skill = { 
-- 데스허그
   { skill_index = 30315,  cooltime = 1000, rate = 100, rangemin = 0, 	rangemax = 800,  target = 3, td = "FL,FR,RB,BR" },
-- 데스허그2
   { skill_index = 30316,  cooltime = 1000, rate = -1, rangemin = 0, 	rangemax = 800,  target = 3, td = "FL,FR,RB,BR" },
}
