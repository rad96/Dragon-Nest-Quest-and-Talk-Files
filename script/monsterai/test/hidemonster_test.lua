 
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_GlobalCoolTime1 = 5000
 
g_Lua_Near1 = 
{
     { action_name = "Stand", rate = 2, loop = 1 },
	 { action_name = "Attack_Tornado_One_70Stage", rate = 2, loop = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
	 { action_name = "Attack_Tornado_One_70Stage", rate = 2, loop = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
	 { action_name = "Attack_Tornado_One_70Stage", rate = 2, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
	 { action_name = "Attack_Tornado_One_70Stage", rate = 2, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
	 { action_name = "Attack_Tornado_One_70Stage", rate = 2, loop = 1 },
}
g_Lua_Skill = { 
-- 마그마펀치_직선
    -- { skill_index = 21081,  cooltime = 10000, rate = 100, rangemin = 1000, rangemax = 2000, target = 3, globalcooltime = 1, selfhppercentrange = "50,100" },
-- 마그마펀치_스플래쉬
    -- { skill_index = 21082,  cooltime = 10000, rate = 100, rangemin = 300, rangemax = 1000, target = 3, globalcooltime = 1, selfhppercentrange = "50,100"  },
-- 스카이라인
    -- { skill_index = 21083,  cooltime = 10000, rate = 100, rangemin = 1000, rangemax = 2000, target = 3, globalcooltime = 1, selfhppercentrange = "0,50" },
-- 포이즌풀
    -- { skill_index = 21084,  cooltime = 10000, rate = 100, rangemin = 300, rangemax = 1000, target = 3, globalcooltime = 1, selfhppercentrange = "0,50" },
-- 네오소환
    -- { skill_index = 21085,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 400, target = 3 },
-- 버프
    -- { skill_index = 21086,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 300, target = 3, globalcooltime = 1 },
-- 스패너스윙
    -- { skill_index = 21087,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 400, target = 3 },
-- 스패너점프
    -- { skill_index = 21088,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 400, target = 3 },
-- 콤보
    -- { skill_index = 21089,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 400, target = 3 },
}