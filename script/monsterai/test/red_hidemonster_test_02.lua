--/genmon 51362
--Attack_RD_2rd_SkillGround_Fire
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 5000;


g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}

g_Lua_Skill = {
   { skill_index = 300134,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 5000, target = 3,encountertime=5000 },--Attack_RD_2rd_SkillGround_Fire (T221)@
}