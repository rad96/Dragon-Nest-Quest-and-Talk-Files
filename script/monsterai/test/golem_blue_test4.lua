--AiCarbuncle_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 1, loop = 1 },
   { action_name = "Walk_Right", rate = 1, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 1, loop = 1 },
   { action_name = "Walk_Right", rate = 1, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 1, loop = 1 },
   { action_name = "Walk_Right", rate = 1, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 1, loop = 1 },
   { action_name = "Walk_Right", rate = 1, loop = 1 },
}
g_Lua_Skill = { 
-- 물방울
   { skill_index = 31317, rate = 100, rangemin = 0, rangemax = 4500, cooltime = 10000, target = 3, selfhppercentrange = "99,100" }, 
-- 독지대 생성
   { skill_index = 31321, rate = 100, rangemin = 0, rangemax = 4500, cooltime = 10000, target = 3, selfhppercentrange = "75,99" }, 
}
