--AiCarbuncle_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Attack8_Swing_Named", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "75,99" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Attack8_Swing_Named", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "75,99" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Attack8_Swing_Named", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "75,99" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Attack8_Swing_Named", rate = 6, loop = 1, max_missradian = 30, selfhppercentrange = "75,99" },
}
g_Lua_Skill = { 
   { skill_index = 20929,  cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "50,75" },
   { skill_index = 20930,  cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "25,50" },
   { skill_index = 20924,  cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "1,25" },
}
