--/genmon 503761

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 6000;

g_Lua_PatrolBaseTime = 6000
g_Lua_PatrolRandTime = 6000
g_Lua_ApproachValue = 500
g_Lua_AssaultTime = 8000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air"},  
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 2, loop = 1 },
	{ action_name = "Move_Left", rate = 2, loop = 1 },
	{ action_name = "Move_Right", rate = 2, loop = 1 },
	{ action_name = "Move_Front", rate = 13, loop = 1 },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 3, loop = 1 },	
    { action_name = "Move_Front", rate = 13, loop = 1 },
}
g_Lua_Near3 = 
{ 
    { action_name = "Move_Left", rate = 6, loop = 1 },
	{ action_name = "Move_Right", rate = 6, loop = 1 },
   	{ action_name = "Move_Front", rate = 15, loop = 1 },
	{ action_name = "Assault", rate = 13, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Move_Left", rate = 3, loop = 1 },
	{ action_name = "Move_Right", rate = 3, loop = 1 },
	{ action_name = "Move_Front", rate = 15, loop = 1 },
	{ action_name = "Assault", rate = 13, loop = 1 },
}

g_Lua_MeleeDefense = { { lua_skill_index = 5, rate = 100  },  }
g_Lua_RangeDefense = { { lua_skill_index = 5, rate = 100  }, }
g_Lua_Assault = { { lua_skill_index = 3, rate = 5, approach = 200},}


g_Lua_Skill =
{
--0서클
	 {skill_index=98011,  cooltime = 30000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, resetcombo =1 , combo1 = "3,70,2"},
--1서클 실패
	 {skill_index=98012,  cooltime = 5000, rangemin = 0, rangemax = 1200, target = 3, rate = -1, resetcombo =1 },
--2콤보
--	 {skill_index=98013,  cooltime = 10000, rangemin = 0, rangemax = 500, target = 3, rate = 30 , resetcombo =1 , combo1 = "10,70,2", combo2 = "4,100,2",},
--3카운터
	 {skill_index=98014,  cooltime = 10000, rangemin = 0, rangemax = 600, target = 3, rate = 30 , combo1 = "2,70,2", combo2 = "6,100,2", resetcombo =1},
--4스톰프
--	 {skill_index=98015,  cooltime = 12000, rangemin = 0, rangemax = 500, target = 3, rate = 30, combo1 = " 12,70,2", resetcombo =1},
--5익스플로러 스펠
--	 {skill_index=98016,  cooltime = 10000, rangemin = 100, rangemax = 600, target = 3, rate = 1, combo1 ="3, 60,2", combo2 ="1,70,2" , combo3 = "0,80,2", combo4 = "7,100,2", resetcombo =1},
--6화염브레스
--	 {skill_index=98017,  cooltime = 20000, rangemin = 0, rangemax = 1200, target = 3, rate = -1, combo1 = "9,60,2", combo2 =" 5,100,0" , resetcombo =1, target_condition = "State3", priority=3},
--7패링
--	 {skill_index=98018,  cooltime = 15000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1 =" 3,60,2", combo2 ="1,70,2" , combo3 = "5,100,2", resetcombo =1},
--8도발
	 {skill_index=98019,  cooltime = 10000, rangemin = 0, rangemax = 1500, target = 3, rate = 30, combo1 =" 3,100,0", resetcombo =1, priority=3, target_condition = "State3"},
--9퀘이크
     {skill_index=98020,  cooltime = 8000, rangemin = 0, rangemax = 500, target = 3, rate = 40, combo1 =" 2,60,2", combo2 ="12,70,2" , combo3 = "11,100,2", resetcombo =1},
--10쉴드어택
	 {skill_index=98021,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 =" 3,60,2", combo2 ="6,70,2" , combo3 = "3,100,2", resetcombo =1},
--11스윙
	 {skill_index=98022,  cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 =" 10,60,2", combo2 ="9,70,2" , combo3 = "2,100,2", resetcombo =1},
--12테일어택
	 {skill_index=98023,  cooltime = 10000, rangemin = 0, rangemax = 500, target = 3, rate = 30, combo1 ="0,60,2", combo2 ="11,70,2" , combo3 = "4,100,2", resetcombo =1},
--13패링 패시브
--	 {skill_index=98024,  cooltime = 5000, rangemin = 0, rangemax = 700, target = 3, rate = -1, combo1 =" 11,60,2", combo2 ="10,70,2" , combo3 = "2,100,2", resetcombo =1},
	 

} 
  