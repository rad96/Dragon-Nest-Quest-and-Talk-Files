--AiCharty_Shadow_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
}
g_Lua_Skill = { 
   -- 검은가시 포효
   { skill_index = 31751,  cooltime = 15000, rate = 100, rangemin = 800, rangemax = 1500, target = 3, selfhppercentrange = "99,100" },
   -- 밀어내기 포효
   { skill_index = 31752,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "99,100" },
   -- 숨기(트리거)
   { skill_index = 31753,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "80,95" },
   -- 검은 구슬 발사
   { skill_index = 31754,  cooltime = 15000, rate = 100, rangemin = 800, rangemax = 1500, target = 3, selfhppercentrange = "80,95" },
}
