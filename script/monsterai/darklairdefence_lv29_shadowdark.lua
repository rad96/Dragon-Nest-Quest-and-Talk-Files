--DarkLairDefence_Lv29_ShadowDark.lua
--타임아웃 모드
--특성상 중간거리를 유지한채 싸움, 근거리시 뒤로 빠지고 중간거리에서 중거리 스킬을 주로 씀

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 7, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 6, loop = 2  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 6, loop = 2  },
   { action_name = "Move_Right", rate = 6, loop = 2  },
   { action_name = "Attack1_Claw", rate = 15, loop = 1  },
   { action_name = "Attack2_Curve", rate = 15, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },

}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
}
g_Lua_NearNonDownMeleeDamage = { 
   { action_name = "Attack1_Claw", rate = 27, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 20, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack4_Pierce", rate = 30, loop = 1  },
   { action_name = "Move_Left", rate = 15, loop = 2  },
   { action_name = "Move_Right", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20240,  cooltime = 22000, rate = 100, rangemin = 100, rangemax = 1500, target = 3, encountertime = 40000 },
}
