--AiJasmin_Black_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1800;
g_Lua_NearValue5 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 10000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Down|!Attack", "Air" }, 
  State3 = {"!Stay|!Move|!Stiff|!Attack", "Stun" }, 
  State4 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, selfhppercentrange = "60,100" },
   { action_name = "Walk_Left", rate = 5, loop = 1, selfhppercentrange = "60,100" },
   { action_name = "Walk_Right", rate = 5, loop = 1, selfhppercentrange = "60,100" },
   { action_name = "Move_Left", rate = 8, loop = 1, globalcooltime = 2, selfhppercentrange = "60,100" },
   { action_name = "Move_Right", rate = 8, loop = 1, globalcooltime = 2, selfhppercentrange = "60,100" },
   { action_name = "Trumble_Back", rate = 20, loop = 1, target_condition = "State1", cooltime = 10000, selfhppercentrange = "60,100" },
   { action_name = "Attack06_SpannerSwing", rate = 35, loop = 1, cooltime = 15000, selfhppercentrange = "60,100" },
   { action_name = "Stand_1", rate = 1, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Walk_Left", rate = 3, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Walk_Right", rate = 3, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Walk_Front", rate = 10, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Trumble_Left", rate = 15, loop = 1, globalcooltime = 2, selfhppercentrange = "0,60" , target_condition = "State1" },
   { action_name = "Trumble_Right", rate = 15, loop = 1, globalcooltime = 2, selfhppercentrange = "0,60" , target_condition = "State1" },
   { action_name = "Trumble_Back", rate = 25, loop = 1, globalcooltime = 2, selfhppercentrange = "0,60" , target_condition = "State1" },
   { action_name = "Attack06_SpannerSwing", rate = 40, loop = 1, cooltime = 8000, selfhppercentrange = "0,60" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, selfhppercentrange = "60,100" },
   { action_name = "Walk_Left", rate = 5, loop = 1, selfhppercentrange = "60,100" },
   { action_name = "Walk_Right", rate = 5, loop = 1, selfhppercentrange = "60,100" },
   { action_name = "Walk_Back", rate = 2, loop = 1, selfhppercentrange = "60,100" },
   { action_name = "Move_Left", rate = 10, loop = 1, globalcooltime = 2, selfhppercentrange = "60,100" },
   { action_name = "Move_Right", rate = 10, loop = 1, globalcooltime = 2, selfhppercentrange = "60,100" },
   { action_name = "Stand_1", rate = 1, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Walk_Left", rate = 5, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Walk_Right", rate = 5, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Walk_Front", rate = 20, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Move_Left", rate = 12, loop = 1, globalcooltime = 2, selfhppercentrange = "0,60" },
   { action_name = "Move_Right", rate = 12, loop = 1, globalcooltime = 2, selfhppercentrange = "0,60" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 1, globalcooltime = 2 },
   { action_name = "Move_Right", rate = 3, loop = 1, globalcooltime = 2 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
   { action_name = "Assault", rate = 40, loop = 1, selfhppercentrange = "0,60" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 25, loop = 1 },
   { action_name = "Assault", rate = 45, loop = 1, selfhppercentrange = "0,60" },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 40, loop = 1 },
   { action_name = "Assault", rate = 40, loop = 1, selfhppercentrange = "0,60" },
}
g_Lua_Assault = { 
   { action_name = "Attack06_SpannerSwing", rate = 10, loop = 1, approach = 200, custom_state1 = "custom_ground" },
}
g_Lua_Skill = { 
   { skill_index = 21086,  cooltime = 900000, rate = 100,rangemin = 0, rangemax = 300, target = 1, selfhppercentrange = "0,40", limitcount = 1 },
   { skill_index = 21081,  cooltime = 21000, rate = 70, rangemin = 0, rangemax = 1200, target = 3, globalcooltime = 1, selfhppercentrange = "60,100" },
   { skill_index = 21081,  cooltime = 15000, rate = 70, rangemin = 0, rangemax = 1200, target = 3, globalcooltime = 1, selfhppercentrange = "0,60" },
   { skill_index = 21082,  cooltime = 21000, rate = 70, rangemin = 0, rangemax = 600, target = 3, globalcooltime = 1, selfhppercentrange = "60,100" },
   { skill_index = 21082,  cooltime = 15000, rate = 70, rangemin = 0, rangemax = 600, target = 3, globalcooltime = 1, selfhppercentrange = "0,60" },
   { skill_index = 21085,  cooltime = 30000, rate = 100, rangemin = 500, rangemax = 1000, target = 3, selfhppercentrange = "60,100" },
   { skill_index = 21083,  cooltime = 40000, rate = 70, rangemin = 500, rangemax = 3000, target = 3, selfhppercentrange = "0,60" },
   { skill_index = 21084,  cooltime = 30000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, selfhppercentrange = "0,60" },
   -- { skill_index = 21088,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 400, target = 3, target_condition = "State4" },
   { skill_index = 21088,  cooltime = 20000, rate = 70, rangemin = 0, rangemax = 400, target = 3, selfhppercentrange = "0,60" },
   { skill_index = 21089,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 400, target = 3 },
}
