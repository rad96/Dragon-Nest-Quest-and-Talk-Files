--AiWarLord_Red_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 3000 
g_Lua_GlobalCoolTime2 = 4000 

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Back", rate = 4, loop = 1  },
   { action_name = "Attack01_Punch", rate = 5, loop = 1 ,globalcooltime=2 },
   { action_name = "Attack02_Combo", rate = 4, loop = 1 ,globalcooltime=2 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Attack04_Stomp_Long", rate = 12, loop = 1,cooltime = 10000 },
   { action_name = "Attack06_ShotBullet", rate = 15, loop = 1,  globalcooltime=1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 12, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 1, loop = 2 },
}
g_Lua_Skill = { 
   { skill_index = 21036,  cooltime = 40000, rate = 80, rangemin = 0, rangemax = 600, target = 3,selfhppercent = 60 }, --네트
   { skill_index = 21031,  cooltime = 35000, rate = 40, rangemin = 400, rangemax = 1500, target = 3,encountertime=15000 }, -- 돌진
   { skill_index = 21037,  cooltime = 8000, rate = 50, rangemin = 200, rangemax = 800, target = 3}, --네이팜
}