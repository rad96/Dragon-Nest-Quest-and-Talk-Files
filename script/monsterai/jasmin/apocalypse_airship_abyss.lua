--AiApocalypse_Airship_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1800;
g_Lua_NearValue4 = 5000;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 15000
-- g_Lua_GlobalCoolTime3 = 600000
-- g_Lua_GlobalCoolTime4 = 600000
-- g_Lua_GlobalCoolTime5 = 30000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack001_ClawMelee1", rate = 45, loop = 1, td = "FL,FR" },
   { action_name = "Attack018_Bite", rate = 10, loop = 1 },
   { action_name = "Attack020_Jump", rate = 45, loop = 1, selfhppercentrange = "0,50", globalcooltime = 1 },
   -- { action_name = "Attack014_Star", rate = 45, loop = 1, selfhppercentrange = "0,50", globalcooltime = 5 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 1, loop = 1 },
   { action_name = "Walk_Right", rate = 1, loop = 1 },
   { action_name = "Attack002_ClawRange1", rate = 33, loop = 1, td = "FL,FR,LF,RF", globalcooltime = 2 },
   { action_name = "Attack020_Jump", rate = 45, loop = 1, selfhppercentrange = "0,50", globalcooltime = 1 },
   -- { action_name = "Attack014_Star", rate = 45, loop = 1, selfhppercentrange = "0,50", globalcooltime = 5 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = -1, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 1 },
   { action_name = "Walk_Left", rate = 1, loop = 1 },
   { action_name = "Walk_Right", rate = 1, loop = 1 },
   { action_name = "Attack002_ClawRange1", rate = 33, loop = 1, td = "FL,FR,LF,RF", globalcooltime = 2 },
   { action_name = "Attack020_Jump", rate = 45, loop = 1, selfhppercentrange = "0,50", globalcooltime = 1 },
   -- { action_name = "Attack014_Star", rate = 45, loop = 1, selfhppercentrange = "0,50", globalcooltime = 5 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = -1, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 1, loop = 1 },
   { action_name = "Walk_Right", rate = 1, loop = 1 },
}
g_Lua_Skill = { 
   -- { skill_index = 30208,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 5000,  target = 3 },
   -- { skill_index = 30209,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000,  target = 3 },
   { skill_index = 30206,  cooltime = 16000, rate = 70, rangemin = 0, rangemax = 1200,  target = 3 },
   { skill_index = 30205,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,90" },
   -- { skill_index = 30205,  cooltime = 500000, rate = 100, rangemin = 0, rangemax = 5000,target = 3, selfhppercentrange = "60,90" , next_lua_skill_index = 1, globalcooltime = 3, limitcount = 1 },
   -- { skill_index = 30205,  cooltime = 500000, rate = 100, rangemin = 0, rangemax = 5000,target = 3, selfhppercentrange = "0,60" , next_lua_skill_index = 0, globalcooltime = 4, limitcount = 1 },
   -- { skill_index = 30205,  cooltime = 500000, rate = 100, rangemin = 0, rangemax = 5000,target = 3, selfhppercentrange = "0,60" , next_lua_skill_index = 1, globalcooltime = 4, limitcount = 1 },
   { skill_index = 30207,  cooltime = 20000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, selfhppercentrange = "0,60" },
   { skill_index = 30211,  cooltime = 30000, rate = 70, rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "0,60" },
}
