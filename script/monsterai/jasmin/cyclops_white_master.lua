--AiCyclops_White_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack003_JumpAttack", 0 },
      { "Attack004_TurningSlash", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_3", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Attack001_Bash", rate = 30, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_3", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "CustomAction1", rate = 37, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Move_Front", rate = 30, loop = 3 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 2 },
   { action_name = "Walk_Right", rate = 5, loop = 2 },
   { action_name = "Walk_Front", rate = 20, loop = 3 },
   { action_name = "Move_Front", rate = 20, loop = 3 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 3 },
   { action_name = "Move_Front", rate = 20, loop = 3 },
   { action_name = "Assault", rate = 45, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack003_JumpAttack", rate = 10, loop = 1, approach = 250 },
   { action_name = "Attack001_Bash", rate = 10, loop = 1, approach = 250 },
}
g_Lua_Skill = { 
   { skill_index = 30312,  cooltime = 24000, rate = 70,rangemin = 0, rangemax = 1500, target = 3, encountertime = 9000 },
   { skill_index = 30311,  cooltime = 20000, rate = 90, rangemin = 0, rangemax = 1000, target = 3, encountertime = 5000 },
   { skill_index = 30310,  cooltime = 32000, rate = 40, rangemin = 0, rangemax = 600, target = 3 },
}
