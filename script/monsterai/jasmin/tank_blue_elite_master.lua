--AiTank_Blue_Elite_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 900;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 3 },
   { action_name = "Walk_Right", rate = 8, loop = 3 },
   { action_name = "Walk_Back", rate = 8, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 3 },
   { action_name = "Walk_Right", rate = 5, loop = 3 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
}
g_Lua_Skill = { 
   { skill_index = 21001,  cooltime = 16000, rate = 80,rangemin = 0, rangemax = 1200, target = 3 },
   { skill_index = 21002,  cooltime = 32000, rate = 60, rangemin = 700, rangemax = 1500, target = 3 },
   { skill_index = 21003,  cooltime = 9000, rate = 80, rangemin = 300, rangemax = 1000, target = 3, max_missradian = 20 },
   { skill_index = 21004,  cooltime = 23000, rate = 80, rangemin = 0, rangemax = 400, target = 3 },
   { skill_index = 21005,  cooltime = 32000, rate = -1, rangemin = 0, rangemax = 1500, target = 3 },
}
