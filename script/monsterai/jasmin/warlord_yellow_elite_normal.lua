--AiWarLord_Yellow_Elite_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 3000 
g_Lua_GlobalCoolTime2 = 4000 

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Back", rate = 6, loop = 1  },
   { action_name = "Attack01_Punch", rate = 15, koop = 1,globalcooltime=2 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Back", rate = 7, loop = 1  },
   { action_name = "Attack04_Stomp_Short", rate = 15, loop = 1,cooltime = 6000 },
   { action_name = "Attack06_ShotBullet", rate = 18, loop = 1,  globalcooltime=1,selfhppercentrange = "40,100" },
   { action_name = "Attack06_ShotRollingBomb", rate = 18, loop = 1,  globalcooltime=1,selfhppercentrange = "0,40" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Attack06_ShotBullet", rate = 18, loop = 1,  globalcooltime=1,selfhppercentrange = "40,100" },
   { action_name = "Attack06_ShotRollingBomb", rate = 18, loop = 1,  globalcooltime=1,selfhppercentrange = "0,40" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 12, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 12, loop = 2 },
}
g_Lua_Skill = { 
   { skill_index = 21036,  cooltime = 40000, rate = 60, rangemin = 300, rangemax = 900, target = 3,selfhppercent = 80 }, --��Ʈ
   -- { skill_index = 21032,  cooltime = 60000, rate = 50, rangemin = 0, rangemax = 1500, target = 3,selfhppercentrange = "0,40" }, -- ��ź���
   { skill_index = 21034,  cooltime = 60000, rate = 30, rangemin = 0, rangemax = 1500, target = 3,selfhppercentrange = "40,100",encountertime=15000 }, -- ���淿����
   { skill_index = 21031,  cooltime = 35000, rate = 20, rangemin = 400, rangemax = 1500, target = 3,encountertime=15000 }, -- ����
}
