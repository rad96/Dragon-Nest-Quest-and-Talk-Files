--AiTank_White_Elite_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 900;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 3 },
   { action_name = "Walk_Right", rate = 8, loop = 3 },
   { action_name = "Walk_Back", rate = 8, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 3 },
   { action_name = "Walk_Right", rate = 5, loop = 3 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
}
g_Lua_Skill = { 
   { skill_index = 21009,  cooltime = 23000, rate = 90,rangemin = 0, rangemax = 1200, target = 3, globalcooltime = 1 },
   { skill_index = 21006,  cooltime = 23000, rate = 100, rangemin = 500, rangemax = 900, target = 3, globalcooltime = 1 },
   { skill_index = 21010,  cooltime = 32000, rate = 70, rangemin = 700, rangemax = 1500, target = 3 },
   { skill_index = 21007,  cooltime = 28000, rate = 90, rangemin = 200, rangemax = 1000, target = 3 },
   { skill_index = 21008,  cooltime = 50000, rate = 90, rangemin = 0, rangemax = 3000, target = 3 },
   { skill_index = 21011,  cooltime = 41000, rate = 100, rangemin = 0, rangemax = 1500, target = 3 },
}
