--AiJasmin_Sniper_Adept_Boss_Easy.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 30000
g_Lua_GlobalCoolTime2 = 30000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
  State2 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack04_AirShot_1",  },
      { "Attack04_AirShot_2",  },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 7, loop = 1 },
   { action_name = "Walk_Right", rate = 7, loop = 1 },
   { action_name = "Walk_Back", rate = 7, loop = 1 },
   { action_name = "Move_Back", rate = 30, loop = 1, cooltime = 20000, target_condition = "State2" },
   { action_name = "Attack02_MehaHammer", rate = 20, loop = 1, cooltime = 15000 },
   { action_name = "CustomAction1", rate = 12, loop = 1, cooltime = 30000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 7, loop = 1 },
   { action_name = "Walk_Right", rate = 7, loop = 1 },
   { action_name = "Walk_Back", rate = 12, loop = 1 },
   { action_name = "Move_Left", rate = 8, loop = 1, globalcooltime = 2 },
   { action_name = "Move_Right", rate = 8, loop = 1, globalcooltime = 2 },
   { action_name = "Attack01_QuickShot", rate = 22, loop = 1, globalcooltime = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 7, loop = 1 },
   { action_name = "Walk_Right", rate = 7, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 1, globalcooltime = 2 },
   { action_name = "Move_Right", rate = 3, loop = 1, globalcooltime = 2 },
   { action_name = "Attack01_QuickShot", rate = 22, loop = 1, globalcooltime = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 30, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 21067,  cooltime = 40000, rate = 80,rangemin = 600, rangemax = 1500, target = 3 },
   { skill_index = 21068,  cooltime = 47000, rate = 80, rangemin = 0, rangemax = 400, target = 3, selfhppercentrange = "0,50", encountertime = 9000 },
   { skill_index = 21065,  cooltime = 40000, rate = -1, rangemin = 0, rangemax = 400, target = 2, selfhppercentrange = "0,80", encountertime = 9000 },
   { skill_index = 21066,  cooltime = 40000, rate = -1, rangemin = 200, rangemax = 800, target = 3 },
}
