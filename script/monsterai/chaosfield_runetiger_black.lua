--RuneTiger_Black_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 15, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 3, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 3, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
  { action_name = "Stand", rate = 15, loop = 1, td = "FL,FR" },
  { action_name = "Turn_Left", rate = 3, loop = -1, td = "LF,BL,LB" },
  { action_name = "Turn_Right", rate = 3, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 3, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 3, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 20, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 3, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 3, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 3, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 3, loop = -1, td = "RF,RB,BR" },
}

g_Lua_Skill = {
--호밍 미사일
   { skill_index = 32615, cooltime = 25000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, multipletarget = 1 },
}
