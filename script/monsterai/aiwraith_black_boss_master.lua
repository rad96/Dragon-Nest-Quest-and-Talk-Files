--AiWraith_Black_Boss_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 8, loop = 1  },
   { action_name = "Attack1_Cut", rate = 30, loop = 1  },
   { action_name = "Attack4_BackAttack", rate = 45, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 12, loop = 2  },
   { action_name = "Walk_Right", rate = 12, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = -1, loop = 2  },
   { action_name = "Move_Left", rate = 12, loop = 1  },
   { action_name = "Move_Right", rate = 12, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 8, loop = 1  },
   { action_name = "Assault", rate = 75, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 1  },
   { action_name = "Move_Right", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Assault", rate = 60, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 1  },
   { action_name = "Move_Right", rate = 10, loop = 1  },
   { action_name = "Assault", rate = 51, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Cut", rate = 5, loop = 1, approach = 200.0  },
   { action_name = "Attack4_BackAttack", rate = 10, loop = 1, approach = 200.0  },
}
g_Lua_Skill = { 
   { skill_index = 20180,  cooltime = 7000, rate = 100,rangemin = 300, rangemax = 700, target = 3 },
   { skill_index = 20181,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 800, target = 3 },
   { skill_index = 20188,  cooltime = 35000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 50 },
}
