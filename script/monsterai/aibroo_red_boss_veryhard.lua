-- Broo Red Boss VeryHard AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 500.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 1500.0;

g_Lua_LookTargetNearState = 4;

g_Lua_WanderingDistance = 1500.0;

g_Lua_PatrolBaseTime = 5000;

g_Lua_PatrolRandTime = 3000;



g_Lua_Near1 =
{
	{ action_name = "Stand",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 8,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 8,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 20,		loop = 2 },
	{ action_name = "Move_Left",	rate = 7,		loop = 1 },
	{ action_name = "Move_Right",	rate = 7,		loop = 1 },
	{ action_name = "Move_Back",	rate = 15,		loop = 2 },
}

g_Lua_Near2 =
{
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 12,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 12,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 8,		loop = 1 },
	{ action_name = "Move_Right",	rate = 8,		loop = 1 },
	{ action_name = "Move_Back",	rate = 3,		loop = 2 },
	{ action_name = "Attack1_Red",	rate = 48,		loop = 1 },
	{ action_name = "Attack1_Red_MIssL",	rate = 48,		loop = 1 },
	{ action_name = "Attack1_Red_MIssR",	rate = 48,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Move_Left",	rate = 10,		loop = 1 },
	{ action_name = "Move_Right",	rate = 10,		loop = 1 },
	{ action_name = "Move_Front",	rate = 2,		loop = 1 },
	{ action_name = "Attack1_Red",	rate = 48,		loop = 1 },
	{ action_name = "Attack1_Red_MIssL",	rate = 48,		loop = 1 },
	{ action_name = "Attack1_Red_MIssR",	rate = 48,		loop = 1 },
}

g_Lua_Near4 = 
{
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 }, 
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 7,		loop = 1 },
	{ action_name = "Move_Right",	rate = 7,		loop = 1 },
	{ action_name = "Move_Front",	rate = 3,		loop = 1 },
}

g_Lua_Skill=
{
	{ skill_index = 20158, SP = 0, cooltime = 15000, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 100, rate = 50 },
	{ skill_index = 20159, SP = 0, cooltime = 12000, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 50, rate = 50 },
}    