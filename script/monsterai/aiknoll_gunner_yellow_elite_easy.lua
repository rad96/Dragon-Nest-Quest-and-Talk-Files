--AiKnoll_Gunner_Yellow_Elite_Easy.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 10000

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 20, loop = 2  },
   { action_name = "Attack1_Fire", rate = 15.75, loop = 1, max_missradian = 30, globalcooltime=1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 18, loop = 2  },
   { action_name = "Walk_Back", rate = 7, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack1_Fire", rate = 15.75, loop = 1, max_missradian = 30 },
   { action_name = "Attack2_FocusFire", rate = 7.35, loop = 1, max_missradian = 30, globalcooltime=1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 18, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 1  },
   { action_name = "Attack2_FocusFire", rate = 15.75, loop = 1, max_missradian = 20, globalcooltime=1 },
   { action_name = "Attack1_Fire", rate = -1, loop = 1, max_missradian = 20 },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 20, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
   { action_name = "Attack2_FocusFire", rate = -1, loop = 1 , max_missradian = 20, globalcooltime=1 },
}
g_Lua_Skill = { 
   { skill_index = 20321,  cooltime = 60000, rate = 80,rangemin = 300, rangemax = 800, target = 3, selfhppercent = 90 },
}
