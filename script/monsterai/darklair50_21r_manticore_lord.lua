-- Manticore_Lord Hard A.I
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 500.0;
g_Lua_NearValue2 = 1000.0;
g_Lua_NearValue3 = 1500.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 500;
g_Lua_AssualtTime = 3000;

g_Lua_State = 	
{	
      State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
      State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}


g_Lua_Near1 = 
{ 
      { action_name = "Stand", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,RF", existparts="101" },
      { action_name = "Walk_Left", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Right", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Back", rate = 3, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Attack11_LHook_Lord",	rate = 20, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR", existparts="101", selfhppercentrange = "5,60" },
      { action_name = "Attack11_RHook_Lord",	rate = 20, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,RF", existparts="101", selfhppercentrange = "5,60" },
      { action_name = "Turn_Left",	rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="101" },
      { action_name = "Attack04_Tail_L_Lord",	rate = 20, loop = 1, custom_state1 = "custom_ground", td = "LF,LB,BL,BR", existparts="101", selfhppercentrange = "5,100", cooltime = 20000 },
      { action_name = "Turn_Right",	rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="101" },
      { action_name = "Attack05_Tail_R_Lord",	rate = 20, loop = 1, custom_state1 = "custom_ground", td = "RF,RB,BL,BR", existparts="101", selfhppercentrange = "5,100", cooltime = 20000 },
}

g_Lua_Near2 = 
{ 
      { action_name = "Stand", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="101" },
      { action_name = "Walk_Left", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Right", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Front", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Back", rate = 3, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Attack11_LHook_Lord",	rate = 20, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR", existparts="101", selfhppercentrange = "5,60" },
      { action_name = "Attack11_RHook_Lord",	rate = 20, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,RF", existparts="101", selfhppercentrange = "5,60" },
      { action_name = "Turn_Left",	rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="101" },
      { action_name = "Attack04_Tail_L_Lord",	rate = 20, loop = 1, custom_state1 = "custom_ground", td = "LF,LB,BL,BR", existparts="101", selfhppercentrange = "5,100", cooltime = 20000 },
      { action_name = "Turn_Right",	rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="101" },
      { action_name = "Attack05_Tail_R_Lord",	rate = 20, loop = 1, custom_state1 = "custom_ground", td = "RF,RB,BL,BR", existparts="101", selfhppercentrange = "5,100", cooltime = 20000 },
      { action_name = "Attack06_GravityThorn",rate = 20, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="101", selfhppercentrange = "5,60" },
}

g_Lua_Near3 = 
{ 
      { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="101" },
      { action_name = "Walk_Left", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Right", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Front", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Back", rate = 3, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="101" },
      { action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="101" },
      { action_name = "Attack06_GravityThorn",rate = 20, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="101", selfhppercentrange = "5,40" },
}

g_Lua_Near4 = 
{ 
      { action_name = "Stand", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="101" },
      { action_name = "Walk_Left", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Right", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Front", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Back", rate = 3, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="101" },
      { action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="101" },
}

g_Lua_Near5 = 
{ 
      { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="101" },
      { action_name = "Walk_Left", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Right", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Walk_Front", rate = 10, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="101" },
      { action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="101" },
      { action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="101" },
}

g_Lua_Skill = { 
-- �� �׶��Ƽ��
   { skill_index = 30012,  cooltime = 25000, rate = 70, rangemin = 600, rangemax = 2000, target = 3, custom_state1 = "custom_ground", existparts="101", selfhppercent = 100, td = "FL,FR" },
}
