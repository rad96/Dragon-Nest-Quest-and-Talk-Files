--SeaDragonNest_Gate5_Celberos_Undead.lua
--체력 75,50,25에 쉐도우 레드 소환, 30초마다 스킬 사용, 3분 이후에는 30초마다 쓰는 스킬 시간 단위가 아니라 쿨타임으로 사용
--/genmon 235725

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 700;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1550;
g_Lua_NearValue4 = 2500;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 400;
g_Lua_AssualtTime = 500;

g_Lua_CustomAction =
{
   CustomAction1 = 
   {
	{ "Avoid", 0},
	{ "Attack_Jump", 0},	 		
   },
   CustomAction2 = 
   {
	{ "Attack_Down_Left_SeaDragon", 0},
	{ "Attack_Down_Right_SeaDragon", 0},
	{ "Attack_Down_Left_SeaDragon", 0},	 		
   },
   CustomAction3 = 
   {
	{ "Attack_Down_Right_SeaDragon", 0},
	{ "Attack_Down_Left_SeaDragon", 0},
	{ "Attack_Down_Right_SeaDragon", 0},	 		
   },
}
g_Lua_Near1 = 
{
   { action_name = "Stand_2", rate = -1, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left",	rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
   { action_name = "Attack_Punch", rate = 15, loop = 1, cooltime = 5000, encountertime = 1000, td = "FL,FR,LF" },
   { action_name = "CustomAction2", rate = 15, loop = 1, cooltime = 15000, encountertime = 10000, td = "FL,FR", selfhppercent = 95 },
   { action_name = "CustomAction3", rate = 15, loop = 1, cooltime = 15000, encountertime = 10000, td = "FL,FR", selfhppercent = 95 },
   { action_name = "Attack_Bite", rate = 15, loop = 1, cooltime = 8000, encountertime = 10000, td = "FL,FR" },
   { action_name = "Attack_Cry", rate = 15, loop = 1, cooltime = 60000, encountertime = 60000, selfhppercent = 75 },
}
g_Lua_Near2 = 
{
   { action_name = "Stand_2", rate = -1, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 10, loop = 1, td = "FL,FR", cooltime = 13000 },
   { action_name = "Turn_Left",	rate = 25, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 25, loop = -1, td = "RF,RB,BR" },
   { action_name = "CustomAction1", rate = 15, loop = 1, cooltime = 10000, td = "FL,FR", selfhppercent = 50 },
   { action_name = "Attack_Cry", rate = 15, loop = 1, cooltime = 60000, encountertime = 60000, selfhppercent = 75 },
}
g_Lua_Near3 = 
{
   { action_name = "Stand_2", rate = -1, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 15, loop = 2, td = "FL,FR" },
   { action_name = "Turn_Left",	rate = 25, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 25, loop = -1, td = "RF,RB,BR" },
   { action_name = "Attack_Jump", rate = 7, loop = 1, cooltime = 10000, td = "FL,FR", selfhppercent = 50 },
   { action_name = "Assault", rate = 3, loop = 1, td = "FL,FR", selfhppercent = 50 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand_2", rate = -1, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 25, loop = 3, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 5, loop = 2, td = "LF,FL,FR,RF,RB,BR,BL,LB" },
   { action_name = "Walk_Right", rate = 5, loop = 2, td = "LF,FL,FR,RF,RB,BR,BL,LB" },
   { action_name = "Turn_Left",	rate = 25, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 25, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Assault = { 
   { action_name = "Attack_Punch", rate = 5, loop = 1, approach = 400 },
}
g_Lua_Skill=
{
--그림자 함정
   { skill_index = 30529, cooltime = 15000, rangemin = 0, rangemax = 2500, target = 3, rate = 100, encountertime = 30000, selfhppercent = 50 },
--부패의 저주
   { skill_index = 30534, cooltime = 10000, rangemin = 0, rangemax = 2500, target = 3, rate = 100, encountertime = 30000, selfhppercent = 25 },
}