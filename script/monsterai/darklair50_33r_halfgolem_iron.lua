--AiHalfGolem_Iron_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 10000

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Attack2_Blow", rate = 12, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Attack12_EyeBeam", rate = 8, loop = 1, cooltime = 10000, globalcooltime = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Attack12_EyeBeam", rate = 15, loop = 1, cooltime = 10000, globalcooltime = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
}

g_Lua_Skill = { 
   { skill_index = 20752,  cooltime = 13000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 75 },
   { skill_index = 20751,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 75 },
}
