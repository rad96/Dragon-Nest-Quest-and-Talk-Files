--AiCyclops_BlackSmith_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1300;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Attack001_Bash_Normal", rate = 12, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Attack003_JumpAttack_Normal", rate = 12, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
   { action_name = "Assault", rate = 12, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
   { action_name = "Assault", rate = 12, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack001_Bash_Normal", rate = 10, approach = 300 },
   { action_name = "Attack003_JumpAttack_Normal", rate = 5, approach = 500 },
}
g_Lua_Skill = { 
   { skill_index = 30419,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 1500, target = 3 },
   { skill_index = 30418,  cooltime = 20000, rate = 60, rangemin = 0, rangemax = 600, target = 3, next_lua_skill_index = 0 },
   { skill_index = 30422,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1300, target = 3 },
   { skill_index = 30421,  cooltime = 16000, rate = 60, rangemin = 0, rangemax = 400, target = 3,next_lua_skill_index = 2 },
   { skill_index = 30417,  cooltime = 36000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1" },
   { skill_index = 30420,  cooltime = 24000, rate = 70, rangemin = 0, rangemax = 3000, target = 3 },
}
