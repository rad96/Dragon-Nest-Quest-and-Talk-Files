--AiBroo_Blue_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 1  },
   { action_name = "Move_Right", rate = 4, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Attack3_IceSword_Named", rate = 12, loop = 1, max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Attack3_IceSword_Named", rate = 6, loop = 1, max_missradian = 30  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
}
g_Lua_Skill = { 

   { skill_index = 20155,  cooltime = 14000, rate = 50, rangemin = 0, rangemax = 1000, target = 3, encountertime = 7000 },
}
