--GreenDragonNest_Gate1_Golem_Blue.lua


g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 5000

g_Lua_SkillProcessor = {
   { skill_index = 30515, changetarget = "2000,0" },
}

g_Lua_CustomAction =
{
   CustomAction1 = 
   {
      { "Attack8_EarthQuake_GreenDragon_Lv1", 0},
   },
}

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 5, loop = 1, selfhppercentrange = "75,100" },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Attack1_Punch_Blue_Lv1", rate = 20, loop = 1, cooltime = 3000, selfhppercentrange = "50,100" },
   { action_name = "Attack17_RightHand", rate = 20, loop = 1, cooltime = 7000, selfhppercentrange = "50,100" },
   { action_name = "Attack5_HipAttack_GreenDragon", rate = 50, loop = 1, cooltime = 10000, selfhppercentrange = "50,100", td = "RB,BR,BL,LB" },
   { action_name = "CustomAction1", rate = 50, loop = 1, cooltime = 20000, selfhppercentrange = "50,100" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 5, loop = 1, selfhppercentrange = "75,100" },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Attack17_RightHand", rate = 20, loop = 1, cooltime = 7000, selfhppercentrange = "50,100" },
   { action_name = "Attack5_HipAttack_GreenDragon", rate = 50, loop = 1, cooltime = 10000, selfhppercentrange = "50,100", td = "RB,BR,BL,LB" },
   { action_name = "CustomAction1", rate = 50, loop = 1, cooltime = 20000, selfhppercentrange = "50,100" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 5, loop = 1, selfhppercentrange = "75,100" },
   { action_name = "Move_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Assault", rate = 20, loop = 1, cooltime = 27000, hppercent = 95 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 7000 },
}
g_Lua_Assault = { 
   { action_name = "Attack17_RightHand", rate = 30, loop = 1, approach = 250.0  },
}

g_Lua_Skill = {
   { skill_index = 31314, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, usedskill ="33352" },-- ���ɾ��� Lv2
   { skill_index = 31321, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 4500, target = 3, selfhppercentrange = "0,100", encountertime = 45000 }, -- ������ ����
   -- { skill_index = 31319, cooltime = 110000, rate = 100, rangemin = 0, rangemax = 4500, target = 1, selfhppercentrange = "0,50", notusedskill = "31319" ,next_lua_skill_index = 0},-- "�ݳ�" �ؽ�Ʈ�׼����� ���ɾ��ý���.
   -- �ݳ� ��ȭ
   { skill_index = 33352, cooltime = 110000, rate = 100, rangemin = 0, rangemax = 4500, target = 1, selfhppercentrange = "0,50", notusedskill = "33352" ,next_lua_skill_index = 0},-- "�ݳ�" �ؽ�Ʈ�׼����� ���ɾ��ý���.
   { skill_index = 31320, cooltime = 45000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "0,25" },-- ������
   { skill_index = 31316, cooltime = 45000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,25" },-- 3���� ��ġ��
   { skill_index = 31311, cooltime = 15000, rate = 30, rangemin = 0, rangemax = 1000, target = 3, selfhppercentrange = "10,50", td = "FL,FR,LF,RF", usedskill ="333352" },-- ���ָ�ġ�� Lv2
   { skill_index = 31312, cooltime = 15000, rate = 30, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "10,50", td = "FL,FR,LF,RF", usedskill ="33352" },-- �����ָ�ġ�� Lv2
   { skill_index = 31313, cooltime = 20000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "10,50", usedskill ="33352" },-- ������ ġ�� Lv2
   { skill_index = 31315, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "25,50", usedskill ="33352"  },-- ��ġ�� Lv2
   -- { skill_index = 31317, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,4", notusedskill="31319"},-- ����� ��� LV1
   -- { skill_index = 31322, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,8", usedskill="31319" }, -- ����� ��� LV2
   -- ���� Ÿ��ȭ
   { skill_index = 31317, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, notusedskill="33352"},-- ����� ��� LV1
   { skill_index = 31322, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, usedskill="33352" }, -- ����� ��� LV2
   { skill_index = 31318, cooltime = 30000, rate = 40, rangemin = 0, rangemax = 1000, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "0,75",usedskill ="33352" },-- �극��
}