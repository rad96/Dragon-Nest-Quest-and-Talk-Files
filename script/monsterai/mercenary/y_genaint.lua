
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 1,		loop = 1 },
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{

--0슬래쉬
	 {skill_index=96892,  cooltime = 5000, rangemin = 100, rangemax = 600, target = 3, rate = 30, resetcombo =1, combo1 = "3,50,2", combo2 = "1,70,2", combo3 = "2,100,2"	, priority = 0},
--1제레난무
	 {skill_index=96893,  cooltime = 6000, rangemin = 0, rangemax = 300, target = 3, rate = 30, resetcombo =1 , combo1 = "7,100,2", combo2 = "7,100,0" , priority = 1},
--2어퍼
	 {skill_index=96894,  cooltime = 5000, rangemin = 0, rangemax = 250, target = 3, rate = 30 , resetcombo =1, combo1 = "1,70,2", combo2 = "0,100,2" , priority = 2},
--3라인
	 {skill_index=96895,  cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = 30 , combo1 = "4,60,2", resetcombo =1 , priority = 3},
--4디펜스
	 {skill_index=96896,  cooltime = 10000, rangemin = 0, rangemax = 700, target = 3, rate = -1, combo1 = " 6,100,0", resetcombo =1 , priority = 4},
--5레이저
	 {skill_index=96897,  cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 ="3, 70,2", resetcombo =1 , priority = 5},
--6슬래쉬M
	 {skill_index=96898,  cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = -1 , priority = 0},
--7제레난무P
	 {skill_index=96899,  cooltime = 5000, rangemin = 0, rangemax = 700, target = 3, rate = -1, combo1 = "3,70,2", combo2 =" 2,100,2" , resetcombo =1 , priority = 1},
	 
} 
  