
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air|Stiff","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 1,		loop = 1 },
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate =3,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{

--0윈드 슬래쉬
	 {skill_index=96882,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = " 7,80,2", combo2 = " 1,100,2", combo3 = "4,100,0" , resetcombo =1 , priority = 0},
--1아르난무
	 {skill_index=96883,  cooltime = 8000, rangemin = 0, rangemax = 200, target = 3, rate = 30, resetcombo =1, combo1 =" 3,70,2", combo2 =" 0,100,2", combo3 = "4,100,0" , priority = 1},
--2부스터
	 {skill_index=96884,  cooltime = 20000, rangemin = 0, rangemax = 600, target = 3, rate = 30 , resetcombo =1 , priority = 2},
--3엑스 슬래쉬
	 {skill_index=96885,  cooltime = 10000, rangemin = 0, rangemax = 300, target = 3, rate = 30 , resetcombo =1, combo1 ="2,100,2", combo2 ="7,100,0" , priority = 3},
--4덤블링
	 {skill_index=96886,  cooltime = 3000, rangemin = 0, rangemax = 500, target = 3, rate = -1, combo1 =" 1,100,2" , resetcombo =1 , priority = 4},
--5윈드
	 {skill_index=96887,  cooltime = 15000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = "1,100,2", combo2= "4,100,0" , resetcombo =1 , priority = 5},
--6덤블링P
	 {skill_index=96888,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 =" 10,60,2", combo2 = " 0,100,1" , priority = 4},
--7윈드 슬래쉬 A
	 {skill_index=96889,  cooltime = 5000, rangemin = 0, rangemax = 500, target = 3, rate = -1, combo1 ="1,70,2", combo2 = "3,100,2", combo3 = "4,100,0" , resetcombo =1 , priority = 0},

} 
  