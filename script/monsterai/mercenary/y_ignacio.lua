
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{

--0 이그난무
	 {skill_index=96852,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = "1,60,2", combo2 ="2,70,2", combo3 ="4,100,2" , priority = 0},
--1 3단펀치
	 {skill_index=96853,  cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 =" 4,70,2", combo2 = " 0,100,2" , priority = 1},
--2 점프어택
	 {skill_index=96854, cooltime = 10000, rangemin =100, rangemax = 600, target = 3, rate = 30, combo1 = "0,70,2", combo2 ="1,100,2" , priority = 2},
--3 대쉬어택
	 {skill_index=96855, cooltime = 15000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, combo1 = "4,60,2", combo2 = "2,70,2" , priority = 3},
--4 혼 어택
	 {skill_index=96856, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = " 0,60,2", combo2 = " 1,70,2" , priority = 4},	
--5 버서크 어택
	 {skill_index=96857, cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = 30 , priority = 5},	
--6 혼 어택P
	 {skill_index=96858, cooltime = 45000, rangemin = 0, rangemax = 600, target = 3, rate = -1 , priority = 4},

} 
  