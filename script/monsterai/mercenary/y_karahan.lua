
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 3,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{

--0 푸쉬
	 {skill_index=96862,  cooltime = 8000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = "2,70,2" , combo2 = " 5,100,2" , priority = 0},
--1레이지 그라비티
	 {skill_index=96863,  cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = " 0,60,2", combo2 = "3,100,2" , priority = 1},
--2 아이스 니들
	 {skill_index=96864, cooltime = 10000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = " 0,60,0", combo2 = "5,70,2" , priority = 2},
--3 글레이셜 웨이브
	 {skill_index=96865, cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = "0,60,2", combo2 = " 2,70,2", combo3 = "1,100,2" , priority = 3},
--4 빽
	 {skill_index=96866, cooltime = 5000, rangemin = 0, rangemax = 600, target = 3, rate = -1, combo1 = "3,60,2", combo2 = "17,100,2", selfhppercent= 60 , priority = 4},	
--5 파이어 버드
	 {skill_index=96867, cooltime = 15000, rangemin = 0, rangemax = 900, target = 3, rate = 30 , priority = 5},	
--6 뺵P
	 {skill_index=96868, cooltime = 30000, rangemin = 0, rangemax = 600, target = 3, rate = -1, combo1 = "11,60,2" ,combo2 = "14,70,2", combo3 = "16,100,0" , priority = 4},

	 
	 } 
  