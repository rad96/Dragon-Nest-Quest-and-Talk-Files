
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0 콤보
	 {skill_index=96972,  cooltime = 8000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = "5,20,2" , combo2 = "1,70,2", combo3  = "2,100,0", resetcombo =1 , priority = 0},
--1암3단 콤보
	 {skill_index=96973,  cooltime = 8000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = "3,50,2", combo2 = " 2,60,2", combo3 = "4,100,2", resetcombo =1 , priority = 1},
--2 스트레이트 암
	 {skill_index=96974, cooltime = 10000, rangemin = 0, rangemax = 500, target = 3, rate = 20, combo1 =" 1,60,2", combo2 = "0,70,2", combo3 =" 3,100,0" , resetcombo =1 , priority = 2},
--3 슬라이드 차지
	 {skill_index=96975, cooltime = 12000, rangemin = 0, rangemax = 700, target = 3, rate = 20, combo1 = "2,60,2", combo2 = "5,70,2" , priority = 3},
--4 암 푸쉬
	 {skill_index=96976, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = "0,50,2", combo2 = " 1,60,2", combo3 = "2,100,2", resetcombo =1 , priority = 4},	
--5 스테어
	 {skill_index=96977, cooltime = 18000, rangemin = 100, rangemax = 800, target = 3, rate = 40, combo1 = "2,60,2", combo2 = " 3,60,2", combo3 = "0,100,2", resetcombo =1 , priority = 5},	
--6 암 푸쉬P
	 {skill_index=96978, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = -1 , priority = 4},


--23 용병_샤우트
	 {skill_index=96780,cooltime=5000,rate=-1,rangemin=0,rangemax=1000,target=2,selfhppercent=100, priority=3, resetcombo =1},
--24 용병_휠 타이푼
     {skill_index=96781,cooltime=5000,rate=-1,rangemin=0,rangemax=1000,target=2,selfhppercent=100, priority=3, resetcombo =1},
	 }