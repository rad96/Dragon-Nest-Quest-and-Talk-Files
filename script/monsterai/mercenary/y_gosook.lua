
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{

--0어퍼
	 {skill_index=96812,  cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = -1, combo1 = " 1,100, 2", resetcombo =1 , priority = 0},
--1고숙이난무
	 {skill_index=96813,  cooltime = 8000, rangemin = 0, rangemax = 800, target = 3, rate = 30, combo1 = " 7,70,2" , combo2 = "5,100,2" , priority = 1},
--2스톰프
	 {skill_index=96814, cooltime = 10000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = "1,70,2", combo2 ="7,100,0" , priority = 2},
--3파이어 대쉬
	 {skill_index=96815, cooltime = 15000, rangemin = 0, rangemax = 250, target = 3, rate = 30, combo1 = "7,100,0" , priority = 3},
--4롱 점프
	 {skill_index=96816, cooltime = 15000, rangemin = 300, rangemax = 1200, target = 3, rate = 50, combo1 = " 7,100,0" , priority = 4},	
--5마엘 스톰
	 {skill_index=96817, cooltime = 15000, rangemin = 0, rangemax = 200, target = 3, rate = 30,combo1 = " 13,100,0" , priority = 5},	
--6롱 점프P
	 {skill_index=96818, cooltime = 10000, rangemin = 200, rangemax = 600, target = 3, rate = -1,combo1 = " 9,100,0" , priority = 4},
--7슬래쉬
	 {skill_index=96819, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 30,combo1 = " 0,100,2" , resetcombo =1 , priority = 0},	
	 

	 
} 
  