
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air|Stiff","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{

--0슬래쉬
	 {skill_index=96872,  cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = "2,70,2", combo2 = " 3,80,2", combo3 =" 1,100,2", combo4 = "4,100,0",resetcombo =1 , priority = 0},
--1벨스난무
	 {skill_index=96873,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = "2,60,2", combo2 = " 5,70,2", combo3 = "0,100,2", combo4 = "4,100,0", resetcombo =1 , priority = 1},
--2어퍼
	 {skill_index=96874,  cooltime = 8000, rangemin = 0, rangemax = 300, target = 3, rate = 30 , combo1 =" 1,60,2", combo2 = " 5,70,2", combo3 = "4,100,0",resetcombo =1 , priority = 2},
--3브레이크
	 {skill_index=96875,  cooltime = 10000, rangemin = 0, rangemax = 500, target = 3, rate = 30, combo1 =" 0,60,2", combo2 = "1,100,0", resetcombo =1 , priority = 3},
--4에스케이프 스워드
	 {skill_index=96876,  cooltime = 10000, rangemin = 0, rangemax = 200, target = 3, rate = 30 , priority = 4},
--5다크 터치
	 {skill_index=96877,  cooltime = 15000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = " 2,70,0", combo2 = "1,100,0", resetcombo =1 , priority = 5},
--6에스케이프 스워드P
	 {skill_index=96878,  cooltime = 100, rangemin = 0, rangemax = 1500, target = 3, rate = -1, combo1 = " 7,100,0" , usedskill = "96186", resetcombo =1 , priority = 4},
 
} 
  