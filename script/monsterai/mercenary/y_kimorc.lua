
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0푸쉬드
	 {skill_index=96762,  cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = 30, combo1 = " 11,100,2", resetcombo =1 , priority = 0},
--1오크난무
	 {skill_index=96763,  cooltime = 8000, rangemin = 0, rangemax = 200, target = 3, rate = 30, combo1 = "4,60,2", combo2 = "5,70,2", combo3 = "2,100,2" , priority = 1},
--2어퍼
	 {skill_index=96764, cooltime = 5000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = " 1,70,2", combo2 = "4,100,2" , priority = 2},
--3포즈
	 {skill_index=96765, cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = "10,100,2" , resetcombo =1 , priority = 3},
--4어설트스트라이크
	 {skill_index=96766, cooltime = 10000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1 = "2,60,2",combo2 = "11,70,2",  combo3 = "1.100,2" , priority = 4},	
--5하울링 스타트
	 {skill_index=96769, cooltime = 15000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1 = "6,100,2", resetcombo =1 , priority = 5},	
--6하울링
	 {skill_index=96767, cooltime = 15000, rangemin = 0, rangemax = 700, target = 3, rate = -1, combo1 = "4,50,2", resetcombo =1 , priority = 5},
--7어설트 스트라이크P
	 {skill_index=96768, cooltime = 10000, rangemin = 0, rangemax = 300, target = 3, rate = 30 , priority = 4},
--8스톰프
	 {skill_index=96760, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = -1 , priority = 0},
--9에너지파
	 {skill_index=96761, cooltime = 5000, rangemin = 0, rangemax = 1200, target = 3, rate = -1 , priority = 0},
--10포즈A
	 {skill_index=96779, cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = -1, combo1 = " 1,70,2", combo2 = "2,100,2", resetcombo =1 , priority = 3},
--11푸쉬드 - 해비슬래쉬
	 {skill_index=96780,  cooltime = 3000, rangemin = 0, rangemax = 200, target = 3, rate = -1, combo1 = " 1,70,2", combo2 = " 3, 100,2", resetcombo =1 , priority = 0},

} 
  