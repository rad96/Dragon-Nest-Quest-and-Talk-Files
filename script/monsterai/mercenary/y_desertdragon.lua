
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",		rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",		rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",		rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",		rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0샌드 카니발
	 {skill_index=96992,  cooltime = 8000, rangemin = 100, rangemax = 600, target = 3, rate = 30 , combo1 = " 2,60,0", combo2 = "1,100,2", resetcombo =1 , priority = 0},
--1플라워 포커스
	 {skill_index=96993,  cooltime = 10000, rangemin = 100, rangemax = 300, target = 3, rate = 30 , combo1 = "4,60,2", combo2 = "0,80,2", combo3 = "5,100,2", resetcombo =1 , priority = 1},
--2킬링 스트릭
	 {skill_index=96994,  cooltime = 5000, rangemin = 100, rangemax = 300, target = 3, rate = 30, combo1 = " 3,100,2", resetcombo =1 , priority = 2},
--3댄스 코어
	 {skill_index=96995,  cooltime = 5000, rangemin = 50, rangemax = 100, target = 3, rate = 30, combo1 ="0,60,2", combo2 ="1,100,2", resetcombo =1 , priority = 3},
--4써지
	 {skill_index=96996,  cooltime = 8000, rangemin = 0, rangemax = 100, target = 3, rate = 30, combo1 = "2,60,2", combo2 =" 5,100,2" , resetcombo =1 , priority = 4},
--5샌드 래쉬
	 {skill_index=96997,  cooltime = 15000, rangemin = 300, rangemax = 1000, target = 3, rate = 30, combo1 =" 1,60,2", combo2 ="2,80,2" , combo3 = "4,100,2", resetcombo =1 , priority = 5},

} 
  