
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0벨린난무
	 {skill_index=96832,  cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = 30, combo1 = "6,100,2" , combo2 = "4,100,0" , priority = 0},
--1엔빌대쉬
	 {skill_index=96833,  cooltime = 10000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1 = " 0, 70,2", combo2 = "2,100,2", priority = 1},
--2리프어택
	 {skill_index=96834, cooltime = 10000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = " 0,80,2" , priority = 2},
--3서클붐버
	 {skill_index=96835, cooltime = 10000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1 = " 2,70,2", combo2 = "1, 100,2" , priority = 3},
--4덤블링
	 {skill_index=96836, cooltime = 5000, rangemin = 0, rangemax = 1200, target = 3, rate = -1, resetcombo =1 , priority = 4},	
--5휠 윈드
	 {skill_index=96837, cooltime = 15000, rangemin = 0, rangemax = 400, target = 3, rate = 30, resetcombo =1, combo1 ="0,100,2" , priority = 5},	
--6벨린난무2
	 {skill_index=96838, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 = " 7,100,2", combo2 = "4,100,0" , priority = 0},
--7벨린난무3
	 {skill_index=96839, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 = "5,40,2", combo2 = "3, 70,2", combo3 = "1,100,2" , priority = 0},
--8덤블링P
	 {skill_index=96840, cooltime = 10000, rangemin = 0, rangemax = 600, target = 3, rate = -1, combo1 = "10,100,2", combo2 = "6,100,0" , priority = 4},

} 
  