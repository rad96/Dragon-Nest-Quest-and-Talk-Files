
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|Air", "Down" },
 State2 = {"Move|Attack", "Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Hit","Down","Stiff"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
     { "Skill_DashCombo" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
     { "Skill_DashCombo" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 이베이전슬래쉬
  CustomAction6 = {
     { "useskill", lua_skill_index = 28, rate = 100 },
  },
  -- 딥스
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 10, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 10000

g_Lua_Near1 = 
{ 
     { action_name = "Move_Left", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Right", rate = 5, loop = 1, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 20, loop = 1 },
     { action_name = "Move_Right", rate = 20, loop = 1 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}

g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
     --{ action_name = "SKill_Dash", rate = -1, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Move_Back", rate = 10, loop = 1 },
	 { action_name = "Skill_EvasionSlash", rate = 30, loop = 1, cooltime = 15000,},
     { action_name = "CustomAction2", rate = 10, loop = 1  },
     { action_name = "CustomAction3", rate = 10, loop = 1 },
}

g_Lua_NonDownRangeDamage = {

	 { action_name = "Skill_DeepStraight_EX_Dash", rate = 30, loop = 1, cooltime = 14400 },
	 { action_name = "useskill", lua_skill_index = 9, rate = 50 },
}

g_Lua_Skill = { 
-- 0임팩트펀치
     { skill_index = 96702, cooltime = 4800, rate = 30, rangemin = 0, rangemax = 150, target = 3, combo1 = "9,30,2", combo2 = "17,60,2", combo3 = "1,100,2", combo4 = "28,100,0" , priority = 0},
	 
-- 1서클봄버
     { skill_index = 96705, cooltime = 10000, rate = 50, rangemin = 200, rangemax = 700, target = 3, combo1 = "8,70,2", combo2 = "3,100,2" , priority = 3},
-- 2이베이전 슬래쉬
     { skill_index = 96706, cooltime = 8000, rate = 10, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 = "0,60,2", combo2 = "8,70,2", combo3 = "1,100,0" , priority = 4},
	
-- 3라인 드라이브EX
     { skill_index = 96707, cooltime = 15000, rate = 30, rangemin = 0, rangemax = 600, target = 3, combo1 = "1,70,2" , priority = 5},
	 
-- 4평타1
     { skill_index = 96703, cooltime = 3000, rate = 70, rangemin = 0, rangemax = 200, target = 3, cancellook = 1,  combo1 = " 0,20,2", combo2 = "5,100,0" , priority = 1},
-- 5평타2
     { skill_index = 96708, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 =" 0,20,2", combo2 = "6,100,2", combo3 = "2,100,0" , priority = 1},
-- 6평타3
     { skill_index = 96709, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 = "7,100,2" , combo2 = "8,100,0" , priority = 1},
-- 7평타4
     { skill_index = 96710, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, cancellook = 1,  combo1 = "4,40,2",  combo2 = "3,60,2", combo3 = "0,70,2", combo4 = "2,80,0" , priority = 1},	
 
-- 8트리플 슬래쉬 EX
     { skill_index = 96704, cooltime = 10000, rate = 30, rangemin = 0, rangemax = 500, target = 3, cancellook = 1, combo1 = "9,100,0" , priority = 2},
-- 9트리플 슬래쉬 EX_R2
     { skill_index = 96711, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 500, target = 3, cancellook = 1, combo1 = "10,80,2", combo2 = "2,100,0" , priority = 2},
-- 10 트리플 슬래쉬 EX_R3
     { skill_index = 96712, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 500, target = 3, cancellook = 1, combo1 = "4,80,2", combo2 = "2,100,0" , priority = 2},
	 
-- 11이베이전 슬래쉬P
     { skill_index = 96713, cooltime = 0, rate = -1, rangemin = 0, rangemax = 600, target = 3, cancellook = 1 , priority = 4},
	 	 
	 
	 }