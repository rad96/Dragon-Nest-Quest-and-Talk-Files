
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" },
 State2 = {"Move|Attack", "Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Down|!Move","Air"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 이베이전슬래쉬
  CustomAction6 = {
     { "useskill", lua_skill_index = 28, rate = 100 },
  },
  -- 딥스
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 28, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
     { action_name = "Move_Left", rate = 5, loop = 1},
     { action_name = "Move_Right", rate = 5, loop = 1},
	 { action_name = "Move_Front", rate = 5, loop = 1 },
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 15000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 16000, target_condition = "State2" },
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 20, loop = 1 },
     { action_name = "Move_Right", rate = 20, loop = 1 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 15000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 12000, globalcooltime = 2, target_condition = "State2" },
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}

g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
     --{ action_name = "SKill_Dash", rate = -1, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Tumble_Back", rate = 20, loop = 1, globalcooltime = 1 },
	 { action_name = "Skill_ShockWave", rate = 30, loop = 1, cooltime = 15000,},
     { action_name = "CustomAction2", rate = 10, loop = 1  },
     { action_name = "CustomAction3", rate = 10, loop = 1 },
}

g_Lua_NonDownRangeDamage = {

	 { action_name = "useskill", lua_skill_index = 11, rate = 30 },
	 { action_name = "useskill", lua_skill_index = 10, rate = 50 },
}

g_Lua_Skill = { 
-- 0트리플 블로우
     { skill_index = 96690, cooltime = 5000, rate = 30, rangemin = 0, rangemax = 300, target = 3 , combo1 = "1,60,2", combo2 = "3,70,2", combo3 = "4,100,2" , priority = 0},-- 액션프레임끊기 nextcustomaction 시그널
	 
-- 1프리징 소드EX
    { skill_index = 96693, cooltime = 8000, rate = 30, rangemin = 0, rangemax = 600, target = 3, combo1 = "0,60,2", combo2 = "4,70,2", combo3 = "18,50,0" , priority = 1},
-- 2쇼크 웨이브
    { skill_index = 96694, cooltime = 10000, rate = 30, rangemin = 0, rangemax = 300, target = 3, combo1 = "1,70,2", combo2 = "0,100,2" , priority = 2},-- 케릭터 어그로유지 LocktargetLook
-- 3파이어 월
     { skill_index = 96695, cooltime = 12000, rate = 30, rangemin = 0, rangemax = 400, target = 3, combo1 = "0,60,2", combo2 = "1,70,2", combo3 = "5,80,2" , priority = 3},
-- 4프로스트 윈드	 
	 { skill_index = 96696, cooltime = 8000, rate = 20, rangemin = 0, rangemax = 300, target = 3, combo1 = "0,60,2", combo2 = "1,70,2", combo3 = "2,100,2" , priority = 4},-- 인풋으로 발동하는 건 액션마다 테이블 다 등록

-- 5스펙트럼 샤워EX
     { skill_index = 96697, cooltime = 18000, rate = 30, rangemin = 0, rangemax = 500, target = 3, combo1 = "1,60,2", combo2 = "0,70,2", combo3 = "2,100,0" , priority = 5},
-- 6프로스트 윈드P
     { skill_index = 96698, cooltime = 2400, rate = -1, rangemin = 0, rangemax = 70, target = 3, priority = 4},


	 }