-- GreenDragonNest_Gate5_Karahan_FireBird

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 500.0;
g_Lua_NearValue3 = 1500.0;



g_Lua_LookTargetNearState = 2;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 5000;



g_Lua_Near1 = 
{ 
	{ action_name = "Stand",		rate = 10,		loop = 1 },
}


g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",		rate = 10,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",		rate = 10,		loop = 1 },
}

g_Lua_Skill=
{
	{ skill_index = 96869, SP = 0, cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = 100 },
}  