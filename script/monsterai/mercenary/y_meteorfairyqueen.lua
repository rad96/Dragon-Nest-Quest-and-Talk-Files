
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}

g_Lua_GlobalCoolTime1 = 8000

g_Lua_Skill =
{
--0콤보
	 {skill_index=96650,  cooltime = 8000, rangemin = 0, rangemax = 200, target = 3, rate = 30, combo1 = "4,50,2", combo2 = " 5,60,2", combo3 =" 9,70,2", combo4 = "7,100,2" , priority = 1},
--1카운터킥
	 {skill_index=96651, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = -1 , priority = 4},
--2힐어
	 {skill_index=96653, cooltime = 15000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = "7,70,2", combo2 = "4,100,2" , priority = 5},
--3힐어택P
	 {skill_index=96654, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = -1 , priority = 5},
--4사이드킥
	 {skill_index=96658, cooltime = 5000, rangemin = 0, rangemax = 430, target = 3, rate = 30 , combo1 =" 5,70,2",combo2 ="7,100,2" , priority = 0},
--5섬머솔트킥
	 {skill_index=96659, cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = 30, combo1 = "4,70,2", combo2 = " 2,100,2" , priority = 2},
--6쏜스텝
	 {skill_index=96664, cooltime = 10000, rangemin = 0, rangemax = 800, target = 3, rate = 30, combo1 = "7,70,2", combo2 = "5,100,2" , priority = 3},
--7콤보1
	 {skill_index=96668,  cooltime = 8000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = "4,50,2", combo2 = " 5,60,2", combo3 = "0,100,2" , priority = 1},


	 
--16 용병_검은비
     {skill_index=96666,cooltime=0,rate=100,rangemin=0,rangemax=1000,target=2,selfhppercent=100,waitorder=96666, priority=5},
--17 용병_블리자드
     {skill_index=96667,cooltime=0,rate=100,rangemin=0,rangemax=1000,target=2,selfhppercent=100,waitorder=96667, priority=5},


	 
	 } 
  