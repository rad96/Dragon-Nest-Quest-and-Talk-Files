-- Academic_MechaDug.lua

g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1
g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;



g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 1 },
        { action_name = "Attack1_Bite", rate = 30,              loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 5,		loop = 2 },
        { action_name = "Attack1_Bite", rate = 30,              loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
  --    { action_name = "Attack3_Saliva",	rate = 13,		loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
    --  { action_name = "Attack3_Saliva",	rate = 13,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
}

g_Lua_Skill = { 

     {skill_index=260401,cooltime=8000,rate=100,rangemin=0,rangemax=150,target=3,selfhppercent=100,checkpassiveskill="4217,-1", priority= 1},--��ī��ũ

     {skill_index=260601,cooltime=18000,rate=100,rangemin=0,rangemax=150,target=3,selfhppercent=100,checkpassiveskill="4219,-1", priority= 2},--��ī���̷�
 
     {skill_index=260501,cooltime=0,rate=100,rangemin=0,rangemax=1200,target=3,selfhppercent=100,waitorder=260501},--��ī�չ�

     {skill_index=30809,cooltime=4500,rate=100,rangemin=0,rangemax=400,target=3,selfhppercent=100,checkpassiveskill="4352,-1", priority= 1},--��ī�� EX-ħ���

     {skill_index=261701,cooltime=0,rate=100,rangemin=0,rangemax=1200,target=3,selfhppercent=100,waitorder=261701},--��ī�չ�EX

}
