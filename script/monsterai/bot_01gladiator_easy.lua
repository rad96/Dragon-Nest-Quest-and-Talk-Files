
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|Air", "Down" },
 State2 = {"Move|Attack", "Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Hit","Down","Stiff"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
     { "Skill_DashCombo" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
     { "Skill_DashCombo" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 이베이전슬래쉬
  CustomAction6 = {
     { "useskill", lua_skill_index = 28, rate = 100 },
  },
  -- 딥스
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 10, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 10000

g_Lua_Near1 = 
{ 
     { action_name = "Attack_Down", rate = 10, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Move_Left", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Right", rate = 5, loop = 1, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 15000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 16000, target_condition = "State2" },
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 20, loop = 1 },
     { action_name = "Move_Right", rate = 20, loop = 1 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 15000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 16000, target_condition = "State2" },
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}

g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
     --{ action_name = "SKill_Dash", rate = -1, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Move_Back", rate = 10, loop = 1 },
	 { action_name = "Skill_EvasionSlash", rate = 30, loop = 1, cooltime = 15000,},
     { action_name = "CustomAction2", rate = 10, loop = 1  },
     { action_name = "CustomAction3", rate = 10, loop = 1 },
}

g_Lua_NonDownRangeDamage = {

	 { action_name = "Skill_DeepStraight_EX_Dash", rate = 30, loop = 1, cooltime = 14400 },
	 { action_name = "useskill", lua_skill_index = 9, rate = 50 },
}

g_Lua_Skill = { 
-- 0임팩트펀치
     { skill_index = 82000, cooltime = 7200, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,60,1", combo2 = "9,30,1", combo3 = "1,100,1", combo4 = "28,100,0" },-- 액션프레임끊기 nextcustomaction 시그널
	 
-- 1사이드킥
    { skill_index = 82047, cooltime = 0, rate = 50, rangemin = 0, rangemax = 600, target = 3, combo1 = "2,40,1", combo2 = "1,30,0", combo3 = "17,50,1", combo4 = "29,100,1" },
-- 2스위핑킥
    { skill_index = 82012, cooltime = 0, rate = -1, rangemin = 0, rangemax = 600, target = 3, cancellook = 1, combo1 = "0,70,0", combo2 = "4,70,1",combo3 = "22,70,1", combo4 = "29,100,1", combo5 = "28,100,0" },-- 케릭터 어그로유지 LocktargetLook
	
-- 3헤비슬래쉬
     { skill_index = 82054, cooltime = 12000, rate = 100, rangemin = 0, rangemax = 600, target = 3, combo1 = "0,60,1" , combo2 = "9,60,0", combo3 = "29,80,1", combo4 = "28,100,0",  target_condition = "State1" },
	 
-- 4라이징 슬래쉬	 
	 { skill_index = 82001, cooltime = 24000, rate = 10, rangemin = 0, rangemax = 600, target = 3, cancellook = 1, combo1 = "5,100,0", resetcombo =1},-- 인풋으로 발동하는 건 액션마다 테이블 다 등록
-- 5라이징 슬래쉬part2
     { skill_index = 82048, cooltime = 16000, rate = -1, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 = "6,100,0" , resetcombo =1},
-- 6라이징 슬래쉬part3
     { skill_index = 82053, cooltime = 16000, rate = -1, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 = "17,100,1", combo2 = "10,50,1", combo3 = "22,100,1" , resetcombo =1},
	 
-- 7서클브레이크
     { skill_index = 82003, cooltime = 36000, rate = 50, rangemin = 0, rangemax = 400, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", globalcooltime = 2, combo1 ="0,70,1", target_condition = "State3"  },
-- 8드롭킥
     { skill_index = 82006, cooltime = 7200, rate = -1, rangemin = 0, rangemax = 400, target = 3,  },
-- 9문라이트 스프리터
     { skill_index = 82063, cooltime = 10800, rate = 30, rangemin = 0, rangemax = 700, target = 3, combo1 = "0,50,0", combo2 = "4,50,0", combo3 = "10,60,0"  },
-- 10이클립스
     { skill_index = 82023, cooltime = 24000, rate = 20, rangemin = 0, rangemax = 400, target = 3, combo1= "28,70,0", combo2 = "4,100,1" , target_condition = "State3" },

	 
-- 11딥스트레이트EX
     { skill_index = 82042, cooltime = 21600, rate = 0, rangemin = 100, rangemax = 650, target = 3, cancellook = 1, combo1 = "12,0,1", combo2 = "13,100,0", resetcombo =1 },-- resetcombo명령어는 combo로 설정된 스킬 시전전에 모션이 캔슬될 경우 이후 combo리스트를 초기화 시키는 명령어
-- 12딥스트레이트EX_L
     { skill_index = 82043, cooltime = 14400, rate = -1, rangemin = 0, rangemax = 1000, target = 3,  cancellook = 1, resetcombo =1 },
-- 13딥스트레이트EX_R
     { skill_index = 82044, cooltime = 14400, rate = -1, rangemin = 0, rangemax = 1000, target = 3,  cancellook = 1, combo1 = "17,100,1", resetcombo =1 },	 

	 
-- 14라인드라이브EX
     { skill_index = 82046, cooltime = 54000, rate = 30, rangemin = 0, rangemax = 900, target = 3, combo1 = "27,100,1" },
-- 15브레이브
     { skill_index = 82034, cooltime = 42000, rate = 20, rangemin = 0, rangemax = 400, target = 3 , combo1= "9,50,1", combo2 = "27,100,1", combo3 = "3,60,1", combo4 = "4,100,0", target_condition = "State3" },
-- 16하프문슬래쉬
     { skill_index = 82024, cooltime = 54000, rate = 30, rangemin = 0, rangemax = 900, target = 3, combo1 = "14,100,1" },

-- 17평타1
     { skill_index = 82049, cooltime = 1000, rate = 70, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 = "1,30,1", combo2 = "18,100,0",  target_condition = "State3" },
-- 18평타2
     { skill_index = 82050, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 = "19,100,1", combo2 = "4,50,0", combo3 = "3,50,0", combo4 = "28,100,0"  },
-- 19평타3
     { skill_index = 82051, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 = "20,100,1" , combo3 = "28,90,0", combo4 = "22,100,0" },
-- 20평타4
     { skill_index = 82052, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, cancellook = 1, combo1 = "1,80,1", combo2 = "22,70,0", combo3 = "4,50,1", combo4 = "9,30,1", combo5 = "17,30,1", combo6 = "28,100,0" },	

-- 21해킹 스탠스 EX
     { skill_index = 82045, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 700, target = 3, combo1 = "45,100,0"},
	 
-- 22 트리플 슬래쉬 EX
     { skill_index = 82036, cooltime = 20300, rate = 30, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "23,50,1", combo2 = "24,100,0" , target_condition = "State3" },
-- 23트리플 슬래쉬 EX_L2
     { skill_index = 82037, cooltime = 13600, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "26,80,1", combo2 = "25,20,1", combo3 = "28,30,0" },
-- 24트리플 슬래쉬 EX_R2
     { skill_index = 82038, cooltime = 13600, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "26,80,1", combo2 = "25,20,1", combo3 = "28,30,0" },
-- 25트리플 슬래쉬 EX_L3
     { skill_index = 82039, cooltime = 13600, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "11,100,1", combo2 = "17,180,1", combo3 = "28,100,0" },
-- 26 트리플 슬래쉬 EX_R3
     { skill_index = 82040, cooltime = 13600, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "11,60,1", combo2 = "17,80,1", combo3 = "28,100,0"},
	 
-- 27 피니쉬 어택
     { skill_index = 82041, cooltime = 36000, rate = 30, rangemin = 0, rangemax = 500, target = 3, combo1 = "0,90,1",combo2= "11,60,0", combo3 = "28,100,0" },
-- 28 이베이전 슬래쉬
     { skill_index = 82035, cooltime = 18000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "4,50,1", combo2 = "3,60,1", combo3 = "0,80,1", combo4 ="14,100,1", combo5= "10,50,0", combo6 = "9,100,0" },
-- 29 사이클론 슬래쉬
     { skill_index = 82016, cooltime = 16800, rate = 10, rangemin = 50, rangemax = 1200, target = 3, combo1 = "30,70,0", combo2 = "9,70,0", combo3 = "15,100,0"},
-- 30 해머리지
     { skill_index = 82025, cooltime = 26000, rate = 10, rangemin = 0, rangemax = 400, target = 3, combo1 = "0,80,1", combo2 = "28,100,0", combo3 = "1,100,1" },	
-- 31 에리얼이베이전
     { skill_index = 82010, cooltime = 21600, rate = -1, rangemin = 400, rangemax = 900, target = 3, combo1 = "37,100,0"},	
-- 32 카운터엑자일
     { skill_index = 82031, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 =" 46,40,0", combo2 = "47,50,0" },--몬스터는 점프시그널이 안뎀 그래서 벨로시티시그널 써야뎀, 몬스터는 크로스헤어로 타겟잡으면 안뎀
-- 33 릴리브
     { skill_index = 82007, cooltime = 20000, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 = "28,100,0" },
-- 34 대쉬
     { skill_index = 82008, cooltime = 3000, rate = 70, rangemin = 200, rangemax = 1200, target = 3,  approch = 200 , combo1 = "35,30,0", combo2 = "10,30,0", combo3= "46,30,0", combo4= "47,30,0", combo5 = "28,40,0", combo6 = "11,100,0" },
-- 35 대쉬슬래쉬
     { skill_index = 82014, cooltime = 12000, rate = -1, rangemin = 300, rangemax = 500, target = 3, combo1 = "0,60,1", combo2 = "36,40,0", combo3 = "28,100,0" },	
-- 36 대쉬콤보
     { skill_index = 82028, cooltime = 12000, rate = -1, rangemin = 0, rangemax = 500, target = 3, combo1 ="4,100,1", combo2 = "28,100,0" },
-- 37 공중 평타
     { skill_index = 82058, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "38,100,0", combo2 = "8,100,0" },	 
-- 38 에어리얼 콤보1
     { skill_index = 82033, cooltime = 24000, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 ="39,100,0", resetcombo =1 },
-- 39 에어리얼 콤보2
     { skill_index = 82056, cooltime = 16000, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 ="40,100,0", resetcombo =1 },
-- 40 에어리얼 콤보3
     { skill_index = 82057, cooltime = 16000, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "0,70,1", combo2 = "28,100,0"  },	 
	 
-- 41 점프
     { skill_index = 82055, cooltime = 24000, rate = 70, rangemin = 300, rangemax = 600, target = 3 , priority= 3 ,combo1 = "37,100,0"},	
-- 42해킹 스탠스 EX_L
     { skill_index = 82059, cooltime = 100, rate = -1, rangemin = 0, rangemax = 1500, target = 3, combo1 = "43,100,1" , combo2 = "45,100,0",  usedskill = "82045" }, -- Loop에 넥스트랜덤스킬 시그널 넣어야뎀
-- 43해킹 스탠스 EX_L2
     { skill_index = 82062, cooltime = 100, rate = -1, rangemin = 0, rangemax = 1500, target = 3, combo1 = "42,100,1" , combo2 = "45,100,0",  usedskill = "82045" },	 	 
-- 44해킹 스탠스 EX_R
     { skill_index = 82060, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 700, target = 3, combo1 = "17,100,0" },	 
-- 45해킹 스탠스 EX_Dash
     { skill_index = 82061, cooltime = 100, rate = -1, rangemin = 0, rangemax = 1500, target = 3, combo1 = " 43,100,0" , approach = 10,  usedskill = "82045" },	 --루프가 들어간 스킬들은 ai 설정시 확인 해야 할 사항이 많음./루프형태보다 스탠스체인쥐 형태가 작업하기 용이함	 
-- 46덤블링R
     { skill_index = 82064, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "34,100,0" },	 
-- 47덤블링L
     { skill_index = 82004, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "34,100,0"  },	 
-- 48카운터슬래쉬
     { skill_index = 82029, cooltime = 24000, rate = -1, rangemin = 0, rangemax = 400, target = 3 },	 
-- 49카운터웨이브
     { skill_index = 82030, cooltime = 24000, rate = -1, rangemin = 0, rangemax = 900, target = 3 },	
-- 50웨이크업어택
     { skill_index = 82005, cooltime = 24000, rate = -1, rangemin = 0, rangemax = 200, target = 3 },
-- 51패링 스탠스
     { skill_index = 82032, cooltime = 60000, rate = 50, rangemin = 300, rangemax = 1200, target = 3, selfhppercent= 60 },	 	 	 
	 }