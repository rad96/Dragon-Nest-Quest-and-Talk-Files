--AiGoblin_Green_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Stand2", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack1_Slash", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Stand2", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack1_Slash", rate = 10, loop = 1 },
   { action_name = "Attack7_Nanmu", rate = 10, loop = 1, max_missradian = 10 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   --{ action_name = "Walk_Left", rate = 2, loop = 1  },
   --{ action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Assault", rate = 14, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Assault", rate = 14, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Upper", rate = 5, loop = 1 },
   { action_name = "Attack1_Slash", rate = 3, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 20003,  cooltime = 3000, rate = 80,rangemin = 300, rangemax = 1000, target = 3, selfhppercent = 90, max_missradian = 30 },
}