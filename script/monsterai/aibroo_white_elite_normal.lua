--AiBroo_White_Elite_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 9, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 60, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 45, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 2  },
   { action_name = "Walk_Left", rate = 9, loop = 2  },
   { action_name = "Walk_Right", rate = 9, loop = 2  },
   { action_name = "Walk_Back", rate = 30, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 9, loop = 2  },
   { action_name = "Attack1", rate = 6, loop = 1  },
   { action_name = "Attack1_MIssL", rate = 6, loop = 1  },
   { action_name = "Attack1_MissR", rate = 6, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 2  },
   { action_name = "Walk_Left", rate = 8, loop = 3  },
   { action_name = "Walk_Right", rate = 8, loop = 3  },
   { action_name = "Walk_Front", rate = 30, loop = 3  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
   { action_name = "Attack1", rate = 6, loop = 1  },
   { action_name = "Attack1_MIssL", rate = 6, loop = 1  },
   { action_name = "Attack1_MIssR", rate = 6, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 9, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20151,  cooltime = 15000, rate = 56,rangemin = 0, rangemax = 400, target = 3, selfhppercent = 100 },
   { skill_index = 20151,  cooltime = 15000, rate = -1, rangemin = 0, rangemax = 400, target = 3, selfhppercent = 100 },
   { skill_index = 20152,  cooltime = 15000, rate = 56, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 100 },
   { skill_index = 20152,  cooltime = 15000, rate = -1, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 100 },
}
