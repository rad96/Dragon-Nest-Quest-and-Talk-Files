--AiManticore_Yellow_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1800;
g_Lua_NearValue5 = 3500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,RF", existparts="1" },
   { action_name = "Walk_Left", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Right", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Back", rate = 8, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
   { action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
   { action_name = "Attack9_Jump", rate = 60, loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", existparts="1", randomtarget=1.1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
   { action_name = "Walk_Left", rate = 5, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Right", rate = 5, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Front", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Back", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Turn_Left", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
   { action_name = "Turn_Right", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
   { action_name = "Attack9_Jump", rate = 60, loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", existparts="1", randomtarget=1.1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = -1, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
   { action_name = "Walk_Left", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Right", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Front", rate = 30, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Back", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Turn_Left", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
   { action_name = "Turn_Right", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
   { action_name = "Attack9_Jump", rate = 60, loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", existparts="1", randomtarget=1.1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = -1, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
   { action_name = "Walk_Left", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Right", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Front", rate = 20, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Back", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Turn_Left", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
   { action_name = "Turn_Right", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
   { action_name = "Attack9_Jump", rate = 45, loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", existparts="1", randomtarget=1.1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
   { action_name = "Walk_Left", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Right", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Front", rate = 20, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Turn_Left", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
   { action_name = "Turn_Right", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
   { action_name = "Attack9_Jump", rate = 60, loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", existparts="1", randomtarget=1.1 },
}
