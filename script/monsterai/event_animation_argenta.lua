g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 950;
g_Lua_NearValue5 = 1250;
g_Lua_NearValue6 = 1550;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Stand_1", rate = 2, loop = 1 },
     { action_name = "Walk_Left", rate = 4, loop = 2 },
     { action_name = "Walk_Right", rate = 4, loop = 2 },
     { action_name = "Move_Back", rate = 2, loop = 1 },
	 
}

g_Lua_Near2 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Stand_1", rate = 2, loop = 1 },
     { action_name = "Move_Front", rate = 6, loop = 1 },
     { action_name = "Move_Left", rate = 4, loop = 1 },
     { action_name = "Move_Right", rate = 4, loop = 1 },
}

g_Lua_Near3 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 8, loop = 2 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
}

g_Lua_Near4 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 8, loop = 4 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
     { action_name = "Assault", rate = 4, loop = 1 },
}

g_Lua_Near5 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 3 },
     { action_name = "Move_Left", rate = 3, loop = 2 },
     { action_name = "Move_Right", rate = 3, loop = 2 },
     { action_name = "Assault", rate = 4, loop = 1 },
}

g_Lua_Near6 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 5 },
     { action_name = "Assault", rate = 4, loop = 1 },
}

g_Lua_Assault = { 
   { action_name = "Skill_WindSlash_Start", rate = 16, approach = 400 },
}



g_Lua_Skill =
{
--0난무
	 {skill_index=96201,  cooltime = 18000, rangemin = 0, rangemax = 300, target = 3, rate = 80 },
--6슬래쉬
	 {skill_index=96207,  cooltime = 21000, rangemin = 0, rangemax = 400, target = 3, rate = 60 },
--8어퍼
	 {skill_index=96209,  cooltime = 14000, rangemin = 0, rangemax = 600, target = 3, rate = 50 },
--10윈드 슬래쉬
	 {skill_index=96211, cooltime = 8000, rangemin = 0, rangemax = 300, target = 3, rate = 60 },
--12엑스 슬래쉬
	 {skill_index=96213,  cooltime = 25000, rangemin = 0, rangemax = 150, target = 3, rate = 70 },
} 
  