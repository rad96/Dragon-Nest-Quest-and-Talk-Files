--AiGoblin_Black_Assault.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 1, loop = 1  },
   { action_name = "Attack1_Slash", rate = 10, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Front", rate = 1, loop = 1  },
   { action_name = "Attack2_Upper", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Attack4_FastJumpAttack", rate = 3, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Assault", rate = 3, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack4_FastJumpAttack", rate = 1, loop = 2  },
}
