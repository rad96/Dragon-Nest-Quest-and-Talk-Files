--AiSkeleton_White_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_10", rate = 3, loop = 1  },
   { action_name = "Attack1_Chop", rate = 6, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_10", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Front", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
   { action_name = "Assault", rate = 17, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Walk_Front", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
   { action_name = "Assault", rate = 32, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Chop", rate = 5, loop = 1, cancellook = 0, approach = 150.0  },
}
g_Lua_Skill = { 
   { skill_index = 20046,  cooltime = 5000, rate = 90,rangemin = 000, rangemax = 250, target = 3 },
   { skill_index = 20045,  cooltime = 12000, rate = 100, rangemin = 400, rangemax = 800, target = 3 },
   { skill_index = 20044,  cooltime = 14000, rate = 80, rangemin = 100, rangemax = 600, target = 3 },
}
