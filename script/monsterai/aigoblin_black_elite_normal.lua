--AiGoblin_Black_Elite_Normal.lua

g_Lua_NearTableCount = 7

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 300;
g_Lua_NearValue4 = 500;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;
g_Lua_NearValue7 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Slash", rate = 4, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 7, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Attack1_Slash", rate = 7, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "stand_1", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 7, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack2_Upper", rate = 7, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "stand_1", rate = 5, loop = 2  },
   { action_name = "Stand2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 7, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 1, loop = 2  },
   { action_name = "Attack4_FastJumpAttack", rate = 7, loop = 1  },
   { action_name = "Assault", rate = 4, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "stand_1", rate = 5, loop = 2  },
   { action_name = "Stand2", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Attack4_FastJumpAttack", rate = 2, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Near7 = { 
   { action_name = "stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Upper", rate = 5, loop = 1 },
   { action_name = "Attack1_Slash", rate = 3, loop = 1 },
}
