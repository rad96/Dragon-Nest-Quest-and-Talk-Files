--AiHarpy_Red_Elite_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 25, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 20, loop = 3  },
   { action_name = "Attack1_Throw_R_Named", rate = 12, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 34, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 15, loop = 3  },
   { action_name = "Walk_Right", rate = 15, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 3  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 3  },
   { action_name = "Attack1_Throw_R_Named", rate = 27, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 22, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 15, loop = 3  },
   { action_name = "Walk_Right", rate = 15, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
   { action_name = "Attack1_Throw_R_Named", rate = 50, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 3  },
   { action_name = "Move_Right", rate = 10, loop = 3  },
   { action_name = "Attack1_Throw_R_Named", rate = 90, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20212,  cooltime = 15000, rate = 50,rangemin = 0, rangemax = 1000, target = 3 },
   { skill_index = 20215,  cooltime = 18000, rate = 50, rangemin = 0, rangemax = 1000, target = 2, selfhppercent = 30 },
}
