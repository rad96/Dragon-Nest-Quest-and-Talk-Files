--AiRhinoMan_Gray_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 30, loop = 1  },
   { action_name = "Attack1_bash", rate = 18, loop = 1  },
   { action_name = "Attack14_TripleSwing", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 12, loop = 1  },
   { action_name = "Walk_Right", rate = 12, loop = 1  },
   { action_name = "Walk_Back", rate = 20, loop = 1  },
   { action_name = "Attack1_bash", rate = 10, loop = 1  },
   { action_name = "Attack14_TripleSwing", rate = 6, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 1  },
   { action_name = "Move_Right", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 23, loop = 1  },
   { action_name = "Attack10_Shot", rate = 20, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 35, loop = 1  },
   { action_name = "Attack10_Shot", rate = 20, loop = 1  },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 7, loop = 1  },
   { action_name = "Move_Right", rate = 7, loop = 1  },
   { action_name = "Move_Front", rate = 25, loop = 1  },
   { action_name = "Attack10_Shot", rate = 20, loop = 1  },
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 8, loop = 1, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 30301,  cooltime = 16000, rate = 80,rangemin = 500, rangemax = 1500, target = 3, selfhppercent = 90 },
   { skill_index = 30302,  cooltime = 19200, rate = 50, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 50 },
   { skill_index = 30303,  cooltime = 25600, rate = 50, rangemin = 800, rangemax = 1500, target = 3, selfhppercent = 50 },
}
