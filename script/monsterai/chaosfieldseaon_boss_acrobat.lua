
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
 State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_CustomAction = {
-- ���� ���̵� ����
  CustomAction1 = {
      { "Move_Back" },
      { "Shoot_Stand_SmallBow" },
  },
-- ���� ���̵� ����
  CustomAction2 = {
      { "Move_Front" },
      { "Shoot_Stand_SmallBow" },
  },
-- ��������������
  CustomAction3 = {
     { "Tumble_Left" },
     { "Shoot_Stand_SmallBow" },
  },
-- ��������������
  CustomAction4 = {
     { "Tumble_Right" },
     { "Shoot_Stand_SmallBow" },
  },
}

g_Lua_GlobalCoolTime1 = 8000
g_Lua_GlobalCoolTime2 = 18000

g_Lua_Near1 = 
{
     { action_name = "Attack_Down", rate = 30, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Stand", rate = 1, loop = 1 },
     { action_name = "Move_Left", rate = 3, loop = 1, cooltime = 7000 },
     { action_name = "Move_Right", rate = 3, loop = 1, cooltime = 7000 },
     { action_name = "Move_Back", rate = 1, loop = 1 },
     { action_name = "Attack_SideKick", rate = 12, loop = 1, cooltime = 30000, globalcooltime = 1 },
     { action_name = "CustomAction1", rate = 8, loop = 1 },
     { action_name = "Shoot_Stand_SmallBow", rate = 10, loop = 1, cooltime = 3000},
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 3, loop = 1, cooltime = 7000 },
     { action_name = "Move_Right", rate = 3, loop = 1, cooltime = 7000 },
     { action_name = "Move_Front", rate = 5, loop = 1 },
     { action_name = "Shoot_Stand_SmallBow", rate = 8, loop = 2, cooltime = 3000 },
     { action_name = "CustomAction1", rate = 5, loop = 1 },
     { action_name = "CustomAction2", rate = 5, loop = 1 },
     { action_name = "CustomAction3", rate = 8, loop = 1, globalcooltime = 1 },
     { action_name = "CustomAction4", rate = 8, loop = 1, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 3, loop = 1, cooltime = 7000 },
     { action_name = "Move_Right", rate = 3, loop = 1, cooltime = 7000 },
     { action_name = "Move_Front", rate = 5, loop = 2 },
     { action_name = "Shoot_Stand_CrossBow", rate = 15, loop = 3, cooltime = 3000 },
     { action_name = "CustomAction2", rate = 4, loop = 1 },
     { action_name = "CustomAction3", rate = 8, loop = 1, globalcooltime = 1 },
     { action_name = "CustomAction4", rate = 8, loop = 1, globalcooltime = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 3 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Assault",  rate = 20,  loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 10, loop = 4 },
 }

g_Lua_Skill = {
-- ���Ǹ� �ν�Ʈ
   { skill_index = 31057, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 1000, target = 2, selfhppercent = 100, globalcooltime = 2 },
-- ����Ŭ��ű 23000
   { skill_index = 31054, cooltime = 20000, rate = 60, rangemin = 0, rangemax = 400, target = 3, selfhppercent = 30, globalcooltime = 2 },
-- �����̷����ؽ� 23000
   { skill_index = 31053, cooltime = 20000, rate = 60, rangemin = 0, rangemax = 300, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercent = 30, globalcooltime = 2 }, 
-- ��Ŭ�� 16000
   { skill_index = 31055, cooltime = 24000, rate = 40, rangemin = 0, rangemax = 400, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercent = 70, globalcooltime = 2 },
-- ű�ؼ� 9000
   { skill_index = 31051, cooltime = 12000, rate = 60, rangemin = 0, rangemax = 600, target = 3, td = "FL,FR", globalcooltime = 2 },
-- ���ε��� 13000
   { skill_index = 31052, cooltime = 21000, rate = 40, rangemin = 300, rangemax = 600, target = 3, td = "FL,FR", globalcooltime = 2 },


-- Ʈ���� 4500 (600+80+200)
--   { skill_index = 31031,  cooltime = 20000, rate = 30, rangemin = 0, rangemax = 800, target = 3 },
-- ���ο콺��ű 12000
   { skill_index = 31034,  cooltime = 20000, rate = 20, rangemin = 0, rangemax = 300, target = 3 },
-- ��Ƽ�� 28000(�����Ÿ� ����X)
   { skill_index = 31032,  cooltime = 35000, rate = 30, rangemin = 0, rangemax = 500, target = 3 },
-- �����ַο� 13000 (600+500+0)
   { skill_index = 31033,  cooltime = 15000, rate = 40, rangemin = 0, rangemax = 1100, target = 3 },
}