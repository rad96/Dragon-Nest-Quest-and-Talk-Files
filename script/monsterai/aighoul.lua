-- Ghoul AI

g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 0.0;
g_Lua_NearValue2 = 150.0;
g_Lua_NearValue3 = 250.0;
g_Lua_NearValue4 = 400.0;
g_Lua_NearValue5 = 600.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 5000;

g_Lua_CustomAction =
{
	CustomAction1 = 
	{
	 	{ "Attack3", 2},
                { "Move_Back", 1},
	 	{ "Attack1", 1},
	},

        CustomAction2 = 
	{
	 	{ "Walk_Right", 1},
	 	{ "Walk_Left", 1},
                { "Walk_Right", 1},
                { "Attack1", 1},
	}
}



g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Attack1_ArmAttack",	rate = 5,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Attack1_ArmAttack",	rate = 10,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
            { action_name = "Attack2_ArmSlash",	rate = 5,		loop = 1 },
	{ action_name = "Attack3_DashBite",	rate = 5,		loop = 1 },
}

g_Lua_Near4 = 
{ 
            { action_name = "Stand",	rate = 5,		loop = 2 },
            { action_name = "Walk_Front",	rate = 5,		loop = 3 },
            { action_name = "Move_Front",	rate = 5,		loop = 2 },
            { action_name = "Attack4_JumpBite",	rate = 5,		loop = 1 },
            { action_name = "Attack7_Sliding",	rate = 5,		loop = 1, cancellook = 1 },
}

g_Lua_Near5 = 
{ 
            { action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
            { action_name = "Move_Front",	rate = 5,		loop = 2 },
	{ action_name = "Attack6_HighJumpBite",	rate = 1,		loop = 1 },
}

g_Lua_Near6 = 
{ 
            { action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
            { action_name = "Move_Front",	rate = 1,		loop = 3 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_ArmAttack",	rate = 5,	loop = 1, cancellook = 0, approach = 300.0 },
            { action_name = "Attack2_ArmSlash",	rate = 5,	loop = 1, cancellook = 0, approach = 300.0 },
            { action_name = "Attack7_Sliding",	rate = 5,	loop = 1, cancellook = 0, approach = 450.0 },
}
       