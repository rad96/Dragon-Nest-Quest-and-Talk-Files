--Transfer1st_Alfredo.lua

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 900;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 300;
g_Lua_AssualtTime = 5000;

g_Lua_Near1 = 
{
	{ action_name = "Attack1",		rate = 15,		loop = 1 }, 
	{ action_name = "Attack2",		rate = 10,		loop = 1 }, 
	{ action_name = "Attack3",		rate = 10,		loop = 1 }, 
	{ action_name = "Stand",		rate = 5,		loop = 1 }, 
	{ action_name = "Move_Front",		rate = 5,		loop = 1 }, 
}

g_Lua_Near2 = 
{
	{ action_name = "Attack1",		rate = 10,		loop = 1 }, 
	{ action_name = "Attack2",		rate = 10,		loop = 1 }, 
	{ action_name = "Attack3",		rate = 10,		loop = 1 }, 
	{ action_name = "Stand",		rate = 5,		loop = 1 }, 
	{ action_name = "Move_Front",		rate = 10,		loop = 1 }, 
	{ action_name = "Walk_Front",		rate = 5,		loop = 1 }, 
}

g_Lua_Near3 = 
{
	{ action_name = "Stand",		rate = 5,		loop = 1 }, 
	-- { action_name = "Walk_Front",		rate = 10,		loop = 1 }, 
	{ action_name = "Move_Front",		rate = 15,		loop = 1 }, 
}

g_Lua_Near4 = 
{
	{ action_name = "Stand",		rate = 5,		loop = 1 }, 
	-- { action_name = "Walk_Front",		rate = 10,		loop = 1 }, 
	{ action_name = "Move_Front",		rate = 15,		loop = 1 }, 
}

g_Lua_Skill=
{
    {skill_index=250001,cooltime=20000,rate=100,rangemin=0,rangemax=200,target=3,selfhppercent=100},--알프레도스톰프Lv1
    {skill_index=250101,cooltime=50000,rate=100,rangemin=0,rangemax=1000,target=1,selfhppercent=100},--알프레도버서커Lv1
    {skill_index=250201,cooltime=15000,rate=100,rangemin=0,rangemax=600,target=3,selfhppercent=100},--알프레도휠윈드Lv1
    {skill_index=250301,cooltime=55000,rate=100,rangemin=0,rangemax=1000,target=3,selfhppercent=100},--데미지트렌지션Lv1
}