-- Skeleton AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 200.0;
g_Lua_NearValue2 = 700.0;
g_Lua_NearValue3 = 1200.0;
g_Lua_NearValue4 = 1500.0;

g_Lua_LookTargetNearState = 4;

g_Lua_WanderingDistance = 2000.0;

g_Lua_PatrolBaseTime = 5000;

g_Lua_PatrolRandTime = 3000;


g_Lua_CustomAction =
{
	CustomAction1 = 
	{
	 	{ "Attack1",    1},
	 	{ "Attack2",    1},
	},
	 
	CustomAction2 = 
	{
	 	{ "Attack2",    1},
		{ "Move_Front", 1},
	 	{ "Attack2",    1},
	}
}

g_Lua_Near1 = 
{ 
	{ action_name = "CustomAction1",	rate = 10,	loop = 1 }, 
	{ action_name = "CustomAction2",	rate = 10,	loop = 1 },
	{ action_name = "Stand",		rate = 1,	loop = 1 },
	{ action_name = "Walk_Front",		rate = 5,	loop = 2 },
	{ action_name = "Walk_Left",		rate = 5,	loop = 3 },
	{ action_name = "Walk_Right",		rate = 5,	loop = 3 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",		rate = 1,	loop = 2 },
	{ action_name = "Walk_Left",		rate = 5,	loop = 2 },
	{ action_name = "Walk_Right",		rate = 5,	loop = 2 },
	{ action_name = "Walk_Front",		rate = 10,	loop = 2 },
	{ action_name = "Move_Left",		rate = 10,	loop = 3 },
	{ action_name = "Move_Right",		rate = 10,	loop = 3 },
	{ action_name = "Move_Front",		rate = 15,	loop = 3 },
	{ action_name = "Assault",		rate = 30,	loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",		rate = 1,	loop = 2 },
	{ action_name = "Walk_Left",		rate = 5,	loop = 3 },
	{ action_name = "Walk_Right",		rate = 5,	loop = 3 },
	{ action_name = "Walk_Front",		rate = 10,	loop = 3 },
	{ action_name = "Move_Left",		rate = 5,	loop = 3 },
	{ action_name = "Move_Right",		rate = 5,	loop = 3 },
	{ action_name = "Move_Front",		rate = 15,	loop = 3 },
	{ action_name = "Assault",		rate = 30,	loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",		rate = 1,	loop = 1 },
	{ action_name = "Walk_Left",		rate = 5,	loop = 2 },
	{ action_name = "Walk_Right",		rate = 5,	loop = 2 },
	{ action_name = "Walk_Front",		rate = 20,	loop = 2 },
	{ action_name = "Move_Left",		rate = 10,	loop = 3 },
	{ action_name = "Move_Right",		rate = 10,	loop = 3 },
	{ action_name = "Move_Front",		rate = 50,	loop = 3 },
}
