-- Golem AI

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 500.0;
g_Lua_NearValue2 = 800.0;
g_Lua_NearValue3 = 1200.0;
g_Lua_NearValue4 = 1500.0;
g_Lua_NearValue5 = 2000.0;

g_Lua_LookTargetNearState = 5;

g_Lua_WanderingDistance = 1800.0;

g_Lua_PatrolBaseTime = 5000;

g_Lua_PatrolRandTime = 3000;

g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 2 },
            { action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Attack1",	rate = 20,		loop = 1 },
	{ action_name = "Attack3_Breath",	rate = 10,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Attack6_SpinAttack",	rate = 20,		loop = 1 },
            { action_name = "Attack3_Breath",	rate = 15,		loop = 1 },

}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
            { action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 1 },
	{ action_name = "Attack2",	rate = 20,		loop = 1 },
	{ action_name = "Attack5_JumpAttack",	rate = 20,		loop = 1 },
            { action_name = "Attack4_CallBee",	rate = 20,		loop = 1 },
            { action_name = "Assault",	rate = 40,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 1 },
            { action_name = "Attack5_JumpAttack",	rate = 15,		loop = 1 },
            { action_name = "Attack4_CallBee",	rate = 15,		loop = 1 },
            { action_name = "Assault",	rate = 40,		loop = 1 },

}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 20,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 1 },
}
