--AiMinotauros_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 350;
g_Lua_NearValue3 = 500;
g_Lua_NearValue4 = 150;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Attack2_Slash_N", rate = 19, loop = 1  },
   { action_name = "Attack1_bash", rate = 19, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Attack1_bash", rate = 25, loop = 1  },
   { action_name = "Attack2_Slash_N", rate = 25, loop = 1  },
   { action_name = "Attack2_Slash", rate = 80, loop = 1,target_condition = "State1"   },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 23, loop = 1  },
   { action_name = "Attack2_Slash_N", rate = 35, loop = 1  },
   { action_name = "Attack2_Slash", rate = 80, loop = 1,target_condition = "State1"   },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 35, loop = 1  },
   { action_name = "Assault", rate = 25, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 8, loop = 1, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 20206,  cooltime = 20000, rate = 80,rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 80 },
}