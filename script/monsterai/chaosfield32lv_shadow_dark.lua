--AiShadow_Dark_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 450;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 2, loop = 1  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack1_Claw", rate = 10, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 11, loop = 1  },
   { action_name = "Walk_Back", rate = 26, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 13, loop = 1  },
   { action_name = "Attack1_Claw", rate = 27, loop = 1  },
   { action_name = "Attack2_Curve", rate = 27, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Attack1_Claw", rate = 20, loop = 1  },
   { action_name = "Attack4_Pierce", rate = 27, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
}
g_Lua_NearNonDownMeleeDamage = { 
   { action_name = "Attack1_Claw", rate = 27, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack4_Pierce", rate = 30, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20240,  cooltime = 22000, rate = 80,rangemin = 500, rangemax = 1000, target = 3 },
   { skill_index = 20241,  cooltime = 30000, rate = 50, rangemin = 50, rangemax = 400, target = 3, selfhppercent = 70 },
}
