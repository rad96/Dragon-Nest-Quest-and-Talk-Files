--AiOgre_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_3", rate = 4, loop = 1},
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_3", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 20, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 13, loop = 1  },
}

g_Lua_Assault = { 
   { lua_skill_index = 1, rate = 16, approach = 300 },
}

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Skill = {
-- 콤보(0:라이트닝/1:어설트용/2:기본용)
   { skill_index = 33795, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 33799, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 500, target = 3, combo1 = "0,100,1" },
   { skill_index = 33799, cooltime = 16000, rate = 40, rangemin = 0, rangemax = 500, target = 3, combo1 = "0,100,1", td = "FR,FL" },
-- 헤븐스저지먼트(라이트닝,대쉬,헤븐)
   { skill_index = 33801, cooltime = 60000, priority = 50, rate = 100, rangemin = 0, rangemax = 19000, target = 3, selfhppercent = 75, next_lua_skill_index = 4 },
   { skill_index = 33802, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 7000, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 5 },
   { skill_index = 33792, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 차지볼트
   { skill_index = 33793, cooltime = 22000, rate = 60, rangemin = 0, rangemax = 400, target = 3 },
-- 점프 차지볼트
   { skill_index = 33794, cooltime = 20000, rate = 100, rangemin = 400, rangemax = 800, target = 3, randomtarget = "1.6,0,1", td = "LF,RF,LB,RB" },
-- 서먼렐릭 - 라이트닝 / 홀드
   { skill_index = 33796, cooltime = 50000, globalcooltime = 1, rate = 70, priority = 30, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 40 },
-- 홀리 버스트
   { skill_index = 33798, cooltime = 32000, rate = 80, rangemin = 0, rangemax = 3000, target = 3 },
-- 홀리 크로스
   { skill_index = 33791, cooltime = 50000, globalcooltime = 1, rate = 65, priority = 40, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 50 },
-- 라이트닝 볼트
   { skill_index = 33797, cooltime = 18000, rate = 70, rangemin = 0, rangemax = 1100, target = 3, td = "FL,FR" },
-- 대쉬
   { skill_index = 33800, cooltime = 22000, rate = 50, rangemin = 0, rangemax = 1100, target = 3, randomtarget = "1.6,0,1", td = "FR,FL,LF,RF,LB,RB" },
-- 아이템 드랍
   { skill_index = 33810, cooltime = 1000, priority = 90, rate = 100, rangemin = 0, rangemax = 19000, target = 3, limitcount = 1, selfhppercent = 76 },
   { skill_index = 33811, cooltime = 1000, priority = 90, rate = 100, rangemin = 0, rangemax = 19000, target = 3, limitcount = 1, selfhppercent = 51 },
   { skill_index = 33812, cooltime = 1000, priority = 90, rate = 100, rangemin = 0, rangemax = 19000, target = 3, limitcount = 1, selfhppercent = 26 },
}


