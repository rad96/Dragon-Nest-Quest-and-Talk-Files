--AiCube_White_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Attack2_Bit_Named", rate = 7, loop = 1  },
   { action_name = "Attack8_Swing", rate = 7, loop = 1, randomtarget = 1.1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
   { action_name = "Attack8_Swing", rate = 7, loop = 1, randomtarget = 1.1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 1, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 1, loop = 1  },
}
g_Lua_Skill = { 
-- ����Ȧ
   { skill_index = 32417, cooltime = 28000, rate = 100, rangemin = 0, rangemax = 4500, target = 3, selfhppercent = 75, next_lua_skill_index = 2, limitcount = 1 },
-- ����Ȧ
   { skill_index = 32417, cooltime = 28000, rate = 100, rangemin = 0, rangemax = 4500, target = 3, selfhppercent = 25, next_lua_skill_index = 2, limitcount = 1 },
-- ����
   { skill_index = 20930, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4500, target = 3 },
-- �п�
   { skill_index = 32405, rate = 100, cooltime = 1000, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 50, limitcount = 1 },
-- ����Ʈ��
   { skill_index = 20922, cooltime = 17000, rate = 70, rangemin = 0, rangemax = 1200, target = 3 },
-- ���̽� ȣ��
   { skill_index = 32416, cooltime = 26000, rate = 100, rangemin = 400, rangemax = 1500, target = 3, selfhppercent = 75, randomtarget = 1.1 },
}