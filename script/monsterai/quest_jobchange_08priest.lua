g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 120;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}



g_Lua_Near1 = 
{ 
-- 0 ~ 120 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Back",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near2 = 
{ 
-- 120 ~ 500 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Back",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near3 = 
{ 
-- 500 ~ 800 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near4 = 
{ 
-- 800 ~ 1200 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near5 = 
{ 
-- 1200 ~ 10000 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Skill = { 
-- ������ ����
   { skill_index = 31121,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "RB,BR,BL,LB"  },
-- ������ ����
   { skill_index = 31122,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FL,FR,LF,RF"  },
-- ������ ������
   { skill_index = 31123,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FL,LF,RB,BR,BL"  },
-- ������ ����
   { skill_index = 31124,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FR,RF,RB,BR,BL"  },
-- ����
   { skill_index = 31091,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 1000,target = 1  },
-- ������ο�
   { skill_index = 31092,  cooltime = 6000, rate = 100, rangemin = 0, rangemax = 300,target = 3  },
-- ������޺�
   { skill_index = 31093,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 300,target = 3  },
-- ��
   { skill_index = 31094,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 400,target = 1  },
-- ������Ʈ
   { skill_index = 31094,  cooltime = 16000, rate = 100, rangemin = 0, rangemax = 1000,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },

-- ����Ʈ�׺�Ʈ
   { skill_index = 31111,  cooltime = 18000, rate = 100, rangemin = 0, rangemax = 800,target = 3, td = "FL,FR" },
-- �����������Ʈ��
   { skill_index = 31112,  cooltime = 10000, rate = 100, rangemin = 300, rangemax = 800,target = 3, td = "FL,FR"  },
-- ü�ζ���Ʈ��
   { skill_index = 31113,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 500,target = 3  },
-- Ȧ������Ʈ
   { skill_index = 31114,  cooltime = 40000, rate = 100, rangemin = 0, rangemax = 800,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB"  },
-- �׷���ũ�ν�
   { skill_index = 31115,  cooltime = 28000, rate = 100, rangemin = 0, rangemax = 800,target = 3, td = "FL,FR"  },
}