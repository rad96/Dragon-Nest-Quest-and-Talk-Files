
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1100;
g_Lua_NearValue4 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;


g_Lua_CustomAction = {
-- 기본 공격 2연타 145프레임
  CustomAction1 = {
     { "Attack1_Scimitar" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction2 = {
     { "Attack1_Scimitar" },
     { "Attack2_Scimitar" },
  },
-- 기본 공격 4연타 313프레임
  CustomAction3 = {
     { "Attack1_Scimitar" },
     { "Attack2_Scimitar" },
     { "Attack3_Scimitar" },
  },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 6000
g_Lua_GlobalCoolTime3 = 25000

g_Lua_Near1 = 
{
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 6, loop = 1 },
     { action_name = "Move_Right", rate = 6, loop = 1 },
     { action_name = "Move_Back", rate = 2, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Back", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 3 },
     { action_name = "Move_Left", rate = 1, loop = 1 },
     { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 4 },
}

g_Lua_Skill = { 
-- 1아웃 브레이크
	{ skill_index = 262431, cooltime = 30000, rate = 20, rangemin = 0, rangemax = 500, target = 3},
-- 2링 스트라이크 
	{ skill_index = 262462, cooltime = 30000, rate = 200, rangemin = 0, rangemax = 200, target = 3},
-- 3플라즈마 버스터
	{ skill_index = 262492, cooltime = 30000, rate = 200, rangemin = 0, rangemax = 500, target = 3 },
-- 4라인오브 다크니스
	{ skill_index = 262522, cooltime = 30000, rate = 20, rangemin = 0, rangemax = 600, target = 3 },	  
}