--AiKoboldArcher_Yellow_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Attack1", rate = 14, loop = 1  },
   { action_name = "Attack1_Miss1", rate = 10, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Attack1", rate = 22, loop = 1  },
   { action_name = "Attack1_Miss1", rate = 17, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Attack1", rate = 22, loop = 1  },
   { action_name = "Attack1_Miss2", rate = 19, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Attack1", rate = 8, loop = 1  },
   { action_name = "Attack1_Miss2", rate = 9, loop = 1  },
}
