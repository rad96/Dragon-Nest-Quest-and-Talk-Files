-- Academic_Alfredo

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 350;
g_Lua_NearValue3 = 500;
g_Lua_NearValue4 = 600;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_NoAggroOwnerFollowInterval=100;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_Near1 = 
{
	{ action_name = "Attack1_S",		rate = 15,		loop = 1, approach = 150.0 }, 
	{ action_name = "Attack2",		rate = 10,		loop = 1, approach = 150.0 }, 
	{ action_name = "Attack3_S",		rate = 15,		loop = 1, approach = 150.0 }, 
	{ action_name = "Walk_Right",		rate = 2,		loop = 1 },
	{ action_name = "Walk_Left",		rate = 2,		loop = 1 },
	{ action_name = "Move_Back",		rate = 15,		loop = 1 }, 
}

g_Lua_Near2 = 
{
	{ action_name = "Attack1_M",		rate = 15,		loop = 1, approach = 300.0}, 
	{ action_name = "Attack3_M",		rate = 10,		loop = 1, approach = 300.0}, 
	{ action_name = "Walk_Right",		rate = 2,		loop = 1 },
	{ action_name = "Walk_Left",		rate = 2,		loop = 1 },
	{ action_name = "Move_Front",		rate = 15,		loop = 1 }, 
}

g_Lua_Near3 = 
{
	{ action_name = "Attack1_L",	rate = 20,		loop = 1, approach = 300.0 },
	{ action_name = "Attack3_L",	rate = 30,		loop = 1, approach = 300.0 },
	{ action_name = "Move_Front",		rate = 15,		loop = 1 }, 
}

g_Lua_Near4 = 
{
	{ action_name = "Move_Front",		rate = 15,		loop = 1 }, 
}

g_Lua_Skill=
{
	{skill_index = 30633, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 550, target = 3, selfhppercent = 100, checkpassiveskill = "1606,-1"}, --팔콘 스트라이크
	{skill_index = 30634, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 250, target = 3, selfhppercent = 100, checkpassiveskill = "1612,-1"}, --팔콘 라이즈
}