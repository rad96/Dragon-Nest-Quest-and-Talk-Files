-- Goblin AI

g_Lua_NearTableCount = 7;

g_Lua_NearValue1 = 0.0;
g_Lua_NearValue2 = 100.0;
g_Lua_NearValue3 = 200.0;
g_Lua_NearValue4 = 300.0;
g_Lua_NearValue5 = 600.0;
g_Lua_NearValue6 = 800.0;
g_Lua_NearValue7 = 3000.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 120;
g_Lua_AssualtTime = 3000;

g_Lua_CustomAction =
{
	CustomAction1 = 
	{
	 		{ "Attack3", 1},
      { "Move_Back", 1},
	 		{ "Attack1", 1},
	},

  CustomAction2 = 
	{
	 	{ "Walk_Right", 1},
	 	{ "Walk_Left", 1},
    { "Attack1", 1},
	}
}



g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Back",	rate = 5,		loop = 2 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_1",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Back",	rate = 5,		loop = 2 },
	{ action_name = "Attack1_Slash",	rate = 10,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 1,		loop = 1 },
	{ action_name = "Move_Right",	rate = 1,		loop = 1 },
            { action_name = "Move_Back",	rate = 3,		loop = 1 },
            { action_name = "Attack2_Upper",	rate = 5,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
            { action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 1,		loop = 2 },
	{ action_name = "Move_Right",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 1,		loop = 2 },
            { action_name = "Move_Back",	rate = 3,		loop = 2 },
            { action_name = "Attack7_Nanmu",	rate = 5,	loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 1,		loop = 2 },
	{ action_name = "Move_Right",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 5,		loop = 2 },
	{ action_name = "Assault",	rate = 5,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 3 },
	{ action_name = "Move_Right",	rate = 3,		loop = 3 },
	{ action_name = "Move_Front",	rate = 10,		loop = 3 },
            { action_name = "Assault",	rate = 5,		loop = 1 },
}

g_Lua_Near7 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 3 },
	{ action_name = "Move_Right",	rate = 3,		loop = 3 },
	{ action_name = "Move_Front",	rate = 10,		loop = 3 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack2_Upper",	rate = 5,	loop = 1, cancellook = 0, approach = 250.0 },
            { action_name = "Attack2_Upper",	rate = 5,	loop = 1, cancellook = 1, approach = 250.0 },
}
       