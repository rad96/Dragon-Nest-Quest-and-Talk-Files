--AiBoarManGunner_Gray_Normal.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 25, loop = 2  },
   { action_name = "Attack01_Shot", rate = 12, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Back", rate = 15, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 7, loop = 1  },
   { action_name = "Move_Back", rate = 4, loop = 1  },
   { action_name = "Attack01_Shot", rate = 23, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 15, loop = 2  },
   { action_name = "Walk_Right", rate = 15, loop = 2  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack01_Shot", rate = -1, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20342,  cooltime = 35000, rate = 100,rangemin = 0, rangemax = 500, target = 3, selfhppercent = 50  },
   { skill_index = 20341,  cooltime = 20000, rate = 100, rangemin = 500, rangemax = 1000, target = 3, selfhppercent = 100 },
}
