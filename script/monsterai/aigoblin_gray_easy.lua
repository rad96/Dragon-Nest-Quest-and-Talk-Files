--AiGoblin_Gray_Easy.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack2_Boom", 0 },
      { "Move_Back", 1 },
  },
  CustomAction2 = {
      { "Attack2_Boom_2", 0 },
      { "Move_Back", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Slash", rate = 2, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "stand2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 7, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Attack1_Slash", rate = 2, loop = 1  },
   { action_name = "CustomAction2", rate = 1, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "stand2", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 7, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack2_Boom_Range", rate = 1, loop = 1  },
   { action_name = "CustomAction2", rate = 2, loop = 1  },
   { action_name = "Assault", rate = 2, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 7, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 1, loop = 2  },
   { action_name = "Attack2_Boom_Range", rate = 1, loop = 1  },
   { action_name = "CustomAction2", rate = 2, loop = 1  },
   { action_name = "Assault", rate = 2, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand2", rate = 7, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 12, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Slash", rate = 5, loop = 1 },
}
