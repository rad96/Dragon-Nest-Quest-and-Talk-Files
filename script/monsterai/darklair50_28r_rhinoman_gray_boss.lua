--AiRhinoMan_Gray_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
   { action_name = "Attack1_bash", rate = 6, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
   { action_name = "Attack1_bash", rate = 6, loop = 1  },
   { action_name = "Attack14_TripleSwing", rate = 4, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 1  },
   { action_name = "Move_Right", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 8, loop = 1  },
   { action_name = "Attack10_Shot", rate = 8, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 2 },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 3 },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 8, loop = 1, approach = 250.0  },
}
g_Lua_Skill = { 
-- 부채꼴
   { skill_index = 30301,  cooltime = 15000, rate = 25,rangemin = 500, rangemax = 1500, target = 3 },
-- 전체
   { skill_index = 30302,  cooltime = 19200, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 50, limitcount = 1 },
-- 정면
   { skill_index = 30303,  cooltime = 15000, rate = 25, rangemin = 800, rangemax = 1500, target = 3 },
}
