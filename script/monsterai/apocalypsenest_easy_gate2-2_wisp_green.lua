g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 50, loop = 3  },
}
g_Lua_Skill = { 
   { skill_index = 20282,  cooltime = 7000, rate = 100,rangemin = 000, rangemax = 2000, target = 1 },
   { skill_index = 20272,  cooltime = 30000, rate = 80, rangemin = 300, rangemax = 600, target = 3 },
   { skill_index = 20273,  cooltime = 7000, rate = 80, rangemin = 000, rangemax = 300, target = 3 },
}
