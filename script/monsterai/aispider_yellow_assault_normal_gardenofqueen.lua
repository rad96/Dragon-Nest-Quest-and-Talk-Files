-- AiSpider_Yellow_Assault_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Attack2_Bite", rate = 4, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Front", rate = 6, loop = 1  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 8, loop = 4  },
}
g_Lua_Skill = { 
   { skill_index = 20094,  cooltime = 1000, rate = 100,rangemin = 0, rangemax = 5000, target = 1, limitcount = 1 },
   { skill_index = 20090,  cooltime = 23000, rate = 100,rangemin = 100, rangemax = 600, target = 3, max_missradian = 30 },
   { skill_index = 20091,  cooltime = 15000, rate = -1, rangemin = 100, rangemax = 600, target = 3, max_missradian = 30 },
}
