--AiMinotauros_White_Armor_Boss_Nest_Minotauros.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 350;
g_Lua_NearValue3 = 500;
g_Lua_NearValue4 = 750;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 30, loop = 2  },
   { action_name = "Attack1_bash_Nest_Minotauros", rate = 35, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 6, loop = 2  },
   { action_name = "Walk_Right", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Attack1_bash_Nest_Minotauros", rate = 27, loop = 1  },
   { action_name = "Attack3_DashAttack_Nest_Minotauros", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 23, loop = 1  },
   { action_name = "Attack3_DashAttack_Nest_Minotauros", rate = 45, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 35, loop = 2  },
   { action_name = "Assault", rate = 20, loop = 1  },
   { action_name = "Attack3_DashAttack_Nest_Minotauros", rate = 60, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 7, loop = 2  },
   { action_name = "Move_Right", rate = 7, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
   { action_name = "Attack3_DashAttack_Nest_Minotauros", rate = 22, loop = 1  },
   { action_name = "Assault", rate = 22, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash_Nest_Minotauros", rate = 8, loop = 1, approach = 300.0  },
}
g_Lua_Skill = { 
-- 발구르기
   { skill_index = 20202,  cooltime = 15000, rate = -1,rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100 },
-- 외침
   { skill_index = 20203,  cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 60 },
-- 버프체크
   { skill_index = 20198,  cooltime = 3000, rate = 100, rangemin = 0, rangemax = 5000, target = 1, selfhppercent = 75 },
}
