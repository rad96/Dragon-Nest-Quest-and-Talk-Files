--AiGhoul_Blue_Boss_Abyss.lua

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 300.0;
g_Lua_NearValue3 = 500.0;
g_Lua_NearValue4 = 800.0;
g_Lua_NearValue5 = 1200.0;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssaultTime = 3000;

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Move_Front_NotTarget", 2 },
      { "Stand", 0 },
      { "Move_Back", 0 },
  },
  CustomAction2 = {
      { "Move_Left", 2 },
  },
  CustomAction3 = {
      { "Move_Right", 2 },
  },
  CustomAction4 = {
      { "Move_Back", 2 },
  },
  CustomAction5 = {
      { "Move_Back", 3 },
      { "Stand", 0 },
      { "Move_Back", 0 },
  },
  CustomAction6 = {
      { "Move_Left", 3 },
  },
  CustomAction7 = {
      { "Move_Right", 3 },
  },
  CustomAction8 = {
      { "Move_Back", 3 },
  },
  CustomAction9= {
      { "Move_Back", 3 },
      { "Stand", 0 },
  },
  CustomAction10= {
      { "Move_Back", 1 },
      { "Stand", 0 },
  },
}

g_Lua_Near1 = 
{ 
	{ action_name = "CustomAction5",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction8",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction1",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction4",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction9",	rate = 30,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "CustomAction1",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 15,		loop = 1 },
	{ action_name = "CustomAction3",	rate = 15,		loop = 1 },	
	{ action_name = "CustomAction6",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction7",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction10",	rate = 30,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 15,		loop = 1 },
	{ action_name = "CustomAction3",	rate = 20,		loop = 1 },
	
}
g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "CustomAction2",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction3",	rate = 5,		loop = 1 },	
}
g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
}