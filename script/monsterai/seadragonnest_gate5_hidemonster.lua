--SeaDragonNest_Gate5_HideMonster.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Attack_DestroySignal_SeaDragon", rate = 30, loop = 3 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Attack_DestroySignal_SeaDragon", rate = 30, loop = 3 },
}
g_Lua_Skill = { 
   { skill_index = 30524,  cooltime = 999000, rate = 100, rangemin = 0, rangemax = 1200, target = 3 },
}