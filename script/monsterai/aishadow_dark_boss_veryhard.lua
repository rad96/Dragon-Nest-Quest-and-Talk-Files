-- Shadow Dark Boss Master AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 200.0;
g_Lua_NearValue2 = 450.0;
g_Lua_NearValue3 = 700.0;
g_Lua_NearValue4 = 1000.0;


g_Lua_LookTargetNearState = 4;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;



g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" },
}	

g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Move_Back",	rate = 15,		loop = 2 },
	{ action_name = "Attack1_Claw",	rate = 20,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 15,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 15,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 15,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 1 },
	{ action_name = "Move_Left",	rate = 15,		loop = 1 },
	{ action_name = "Move_Right",	rate = 15,		loop = 1 },
	{ action_name = "Move_Back",	rate = 15,		loop = 1 },
	{ action_name = "Attack1_Claw",	rate = 45,		loop = 1 },
	{ action_name = "Attack2_Curve",rate = 60,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 8,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 15,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 15,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 8,		loop = 2 },
	{ action_name = "Move_Front",	rate = 8,		loop = 1 },
	{ action_name = "Move_Left",	rate = 15,		loop = 1 },
	{ action_name = "Move_Right",	rate = 15,		loop = 1 },
	{ action_name = "Move_Back",	rate = 8,		loop = 1 },
	{ action_name = "Attack1_Claw",	rate = 40,		loop = 1 },
	{ action_name = "Attack4_Pierce",rate = 55,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Move_Front",	rate = -1,		loop = 2 },
	{ action_name = "Move_Left",	rate = 10,		loop = 2 },
	{ action_name = "Move_Right",	rate = 10,		loop = 2 },
	{ action_name = "Move_Back",	rate = -1,		loop = 2 },
}

g_Lua_NonDownMeleeDamage = 				
{ 				
	{ action_name = "Attack1_Claw",	rate = 30,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
	{ action_name = "Move_Front",	rate = 20,		loop = 2 },
}	

g_Lua_NonDownRangeDamage = 				
{ 				
	{ action_name = "Attack4_Pierce",	rate = 40,		loop = 1 },
	{ action_name = "Move_Left",	rate = 15,		loop = 2 },
	{ action_name = "Move_Right",	rate = 15,		loop = 2 },
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
}

g_Lua_Skill=
{
	{ skill_index = 20240, SP = 0, cooltime = 12000, rangemin = 500, rangemax = 1500, target = 3, selfhppercent = 90, rate = 90 },
	{ skill_index = 20241, SP = 0, cooltime = 20000, rangemin = 50, rangemax = 400, target = 3, selfhppercent = 50, rate = 60 },
}
    