--AiGoblin_Green_Abyss.lua

g_Lua_NearTableCount = 7

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 350;
g_Lua_NearValue4 = 550;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;
g_Lua_NearValue7 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
  State2 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Walk_Back", 2 },
      { "Attack2_Upper", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near7 = { 
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Slash", rate = 3, loop = 1 },
}
