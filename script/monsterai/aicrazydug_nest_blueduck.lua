g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200 --몬스터가 돌격시에 타겟에게 다가가는 최소거리
g_Lua_AssualtTime = 1000 --Assault 명령을 받고 돌격 할 때 타겟을 쫓아가는 시간. 


g_Lua_CustomAction = {
  CustomAction1 = { 
      { "Walk_Left", 0 },
      { "Attack1_Bite", 0 },
  },
  
  CustomAction2 = {
      { "Walk_Right", 0 },
      { "Attack1_Bite", 0 },
  },
 
 }


 g_Lua_Near1 = { --뒤로 후퇴 
   { action_name = "Move_Back", rate = 1, loop = 1  },
}
 
g_Lua_Near2 = { --옆으로 이동하고 물거나 그냥 물거나
   { action_name = "CustomAction1", rate = 1, loop = 0  },
   { action_name = "CustomAction2", rate = 1, loop = 0  },
   { action_name = "Attack1_Bite", rate = 2, loop = 0  },
}

g_Lua_Near3 = { --돌격
   { action_name = "Assault", rate = 1, loop = 0  },
}

g_Lua_Near4 = { --너무 멀면 대기
   { action_name = "Stand", rate = 1, loop = 0  },
   { action_name = "Walk_Front", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 1, loop = 0  },
   { action_name = "Walk_Left", rate = 1, loop = 0  },
}


g_Lua_Assault = { 
   { action_name = "Attack1_Bite", rate = 1, loop = 0  },
}