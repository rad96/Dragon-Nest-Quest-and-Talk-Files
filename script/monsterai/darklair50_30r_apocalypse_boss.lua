
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 600.0;
g_Lua_NearValue2 = 1000.0;
g_Lua_NearValue3 = 1500.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 500;
g_Lua_AssualtTime = 5000;

g_Lua_SkillProcessor = {
     { skill_index = 30207,  changetarget = "3000,1"  },
}

g_Lua_GlobalCoolTime1 = 30000
g_Lua_GlobalCoolTime2 = 15000

g_Lua_Near1 = 
{ 
     { action_name = "Stand_1", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Stand_2", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Stand_3", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Walk_Left_DarkLair50", rate = 2, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right_DarkLair50", rate = 2, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Attack001_ClawMelee1", rate = 7,	loop = 1, td = "FL,FR" },
     { action_name = "Attack020_Jump_DarkLair50", rate = 20, loop = 3, td = "FL,FR", cooltime = 30000, globalcooltime = 1 },
}

g_Lua_Near2 = 
{ 
     { action_name = "Stand_1", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Stand_2", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Stand_3", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Move_Front", rate = 8, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Walk_Left_DarkLair50", rate = 10, loop = 2, td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right_DarkLair50", rate = 10, loop = 2, td = "FL,FR,LF,RF" },
     { action_name = "Attack002_ClawRange1", rate = 7, loop = 1, td = "FL,FR" },
     { action_name = "Attack020_Jump_DarkLair50", rate = 15, loop = 3, td = "FL,FR", cooltime = 30000, globalcooltime = 1 },
}

g_Lua_Near3 = 
{ 
     { action_name = "Stand_1", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Stand_2", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Stand_3", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Move_Front", rate = 20, loop = 1, td = "FL,FR,LF,RF" },
}

g_Lua_Near4 = 
{ 
     { action_name = "Stand_1", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Stand_2", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Stand_3", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Move_Front", rate = 10, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Attack003_Assault", rate = 20, loop = 1, td = "FL,FR,LF,RF", limitcount = 1 },
}

g_Lua_Near5 = 
{ 
     { action_name = "Stand_1", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Stand_2", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Stand_3", rate = 4, loop = 1, td = "FL,RF", globalcooltime = 2 },
     { action_name = "Move_Front", rate = 10, loop = 1, td = "FL,FR,LF,RF" },
}

g_Lua_Skill = { 
-- 샤우트
     { skill_index = 30205, cooltime = 50000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercent = 75, next_lua_skill_index = 2, limitcount = 1 },
-- 샤우트
     { skill_index = 30205, cooltime = 50000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercent = 25, next_lua_skill_index = 2, limitcount = 1 },
-- 훨윈드
     { skill_index = 30207,  cooltime = 10000, 	rate = -1, rangemin = 0, rangemax = 4000, target = 3 },
-- 스트레치 아웃
     { skill_index = 30206, cooltime = 13000, rate = 70, rangemin = 0, rangemax = 2500, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- 레이저1_다크레어전용
     { skill_index = 32414,  cooltime = 20000, 	rate = 100, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 50, limitcount = 1 },
-- 이베포레이션
     { skill_index = 30459, cooltime = 15000, rate = 75, rangemin = 0, rangemax = 4000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = 1, enocountertime = 10000 },
}