--DarkLairDefence_Lv7_SkeletonWhite.lua
--스켈레톤 그레이보다 다가오는 속도가 느림
--스킬 사용, 순간 근접 하거나 연속 찌르기

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Attack1_Chop", rate = 18, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Back", rate = 4, loop = 2  },
   { action_name = "Move_Back", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_Chop", rate = 25, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
   { action_name = "Assault", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Move_Front", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20046,  cooltime = 8000, rate = 80, rangemin = 000, rangemax = 250, target = 3, selfhppercent = 100 },
   { skill_index = 20045,  cooltime = 12000, rate = 50, rangemin = 400, rangemax = 800, target = 3, selfhppercent = 100 },
}