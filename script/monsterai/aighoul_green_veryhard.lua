-- Ghoul Green Master AI

g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 250.0;
g_Lua_NearValue3 = 400.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 1000.0;
g_Lua_NearValue6 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Move_Back",	rate = 10,		loop = 2 },
	{ action_name = "Attack1_ArmAttack",	rate = 4,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1 },
	{ action_name = "Move_Back",	rate = 20,		loop = 2 },
	{ action_name = "Attack1_ArmAttack",	rate = 4,		loop = 1 },
        { action_name = "Attack2_ArmSlash",	rate = 2,		loop = 1, max_missradian = 40 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Move_Back",	rate = 15,		loop = 2 },
        { action_name = "Attack2_ArmSlash",	rate = 6,		loop = 1, max_missradian = 20 },
}

g_Lua_Near4 = 
{ 
        { action_name = "Stand_1",	rate = 5,		loop = 2 },
        { action_name = "Walk_Front",	rate = 5,		loop = 3 },
        { action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Assault",	rate = 5,		loop = 1, max_missradian = 10 },
}

g_Lua_Near5 = 
{ 
        { action_name = "Stand_1",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
        { action_name = "Move_Front",	rate = 20,		loop = 2 },
	{ action_name = "Assault",	rate = 6,		loop = 1, max_missradian = 10 },
}

g_Lua_Near6 = 
{ 
        { action_name = "Stand_1",	rate = 1,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 1,		loop = 2 },
        { action_name = "Move_Front",	rate = 5,		loop = 3 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_ArmAttack",	rate = 2,	loop = 1, cancellook = 1, approach = 150.0 },
        { action_name = "Attack2_ArmSlash",	rate = 4,	loop = 1, cancellook = 1, approach = 250.0 },
}

g_Lua_Skill=
{
	{ skill_index = 20020, SP = 0, cooltime =15000, rangemin = 0, rangemax = 600, target = 3, selfhppercent = 80, max_missradian = 20 },
}          