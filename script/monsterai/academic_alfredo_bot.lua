-- Academic_Alfredo

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 600;


g_Lua_NoAggroOwnerFollow=1;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 20000
g_Lua_GlobalCoolTime3 = 30000
g_Lua_GlobalCoolTime4 = 24000

g_Lua_Near1 = 
{
	{ action_name = "Attack1",		rate = 15,		loop = 1 }, 
	{ action_name = "Attack2",		rate = 10,		loop = 1 }, 
	{ action_name = "Attack3",		rate = 10,		loop = 1 }, 
	{ action_name = "Walk_Right",		rate = 2,		loop = 1 },
	{ action_name = "Walk_Left",		rate = 2,		loop = 1 },
}
g_Lua_Near2 = 
{
	{ action_name = "Move_Front",		rate = 15,		loop = 1 }, 
}
g_Lua_Near3 = 
{
	{ action_name = "Assault",		rate = 15,		loop = 1 }, 
	{ action_name = "Move_Front",		rate = 15,		loop = 1 }, 
}
g_Lua_Assault =				
{ 				
	{ action_name = "Attack1",	rate = 20,		loop = 1, approach = 150.0 },
	{ action_name = "Attack2",	rate = 30,		loop = 1, approach = 150.0 },
	{ action_name = "Attack3",	rate = 30,		loop = 1, approach = 150.0 },
}				
				

g_Lua_Skill = { 

    {skill_index=84061,cooltime=15000,rate=100,rangemin=0,rangemax=400,target=3,selfhppercent=100, globalcooltime = 1},--알프레도스톰프

    {skill_index=84062,cooltime=20000,rate=100,rangemin=0,rangemax=1000,target=1,selfhppercent=100, globalcooltime = 2},--알프레도버서커
 
    {skill_index=84063,cooltime=35000,rate=100,rangemin=0,rangemax=400,target=3,selfhppercent=100, globalcooltime = 3},--알프레도휠윈드

    --{skill_index=260701,cooltime=35000,rate=100,rangemin=0,rangemax=400,target=3,selfhppercent=100,checkpassiveskill="4303,-1", globalcooltime = 4},--알프레도빔
 
    {skill_index=84064,cooltime=48000,rate=100,rangemin=0,rangemax=1000,target=2,selfhppercent=100, priority=1},--데미지트렌지션


}
