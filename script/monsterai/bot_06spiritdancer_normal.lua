
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" },
 State2 = {"Stay|Move|Attack", "!Down","!Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Down|!Move","Air"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 신 브레이커
  CustomAction6 = {
     { "useskill", lua_skill_index = 30, rate = 100 },
  },
  -- 
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 31, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12800

g_Lua_Near1 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Right", rate = 5, loop = 1, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 12000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 24000, target_condition = "State4" },
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 20, loop = 1 },
     { action_name = "Move_Right", rate = 20, loop = 1 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 12000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 24000, target_condition = "State4" },
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}

g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
     --{ action_name = "SKill_Dash", rate = -1, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Tumble_Back", rate = 20, loop = 1, globalcooltime = 1 },	 
     { action_name = "CustomAction2", rate = 10, loop = 1  },
     { action_name = "CustomAction3", rate = 10, loop = 1 },
}

g_Lua_NonDownRangeDamage = {

	 { action_name = "useskill", lua_skill_index = 37, rate = 30 },
	 { action_name = "useskill", lua_skill_index = 41, rate = 50 },
}

g_Lua_Skill = { 

-- 0 소울윈드
     { skill_index = 84501, cooltime = 4000, rate = 10, rangemin = 300, rangemax = 1200, target = 3, combo1 = "1,40,1", combo2 =" 3,50,1" },-- 액션프레임끊기 nextcustomaction 시그널
-- 1 스팅 브리즈
     { skill_index = 84502, cooltime = 8000, rate = 30, rangemin = 0, rangemax = 300, target = 3, combo1 = "4,40,1", combo2 = "2,100,0" , target_condition = "State3"},
-- 2 스피릿 블로우
     { skill_index = 84503, cooltime = 12000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, combo1 = " 1,60,1", combo2 = "16,100,0" },-- 케릭터 어그로유지 LocktargetLook
-- 3 디스페어 니들
     { skill_index = 84504, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 ="2,100,1" , target_condition = "State3"},

-- 4 슬라이트 턴
     { skill_index = 84509, cooltime = 0, rate = -1, rangemin = 0, rangemax = 200, target = 3 , combo1 ="3,70,1", combo2 ="16,100,0"},
-- 5 소울 키스
     { skill_index = 84510, cooltime = 2400, rate = 20, rangemin = 0, rangemax = 150, target = 3 ,combo1 = "2,100,1" , target_condition = "State1"},
-- 6 리턴 스핀
     { skill_index = 84512, cooltime = 7200, rate = -1, rangemin = 0, rangemax = 200, target = 3, blowcheck = "23"  },
-- 7고스트 킥
     { skill_index = 84513, cooltime = 4000, rate = -1, rangemin = 0, rangemax = 400, target = 3 },
-- 8 슬레이브 핸드
     { skill_index = 84514, cooltime = 9600, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 = "15,100,1" },
  
-- 9 스위트 서클
     { skill_index = 84523, cooltime = 8000, rate = 20, rangemin = 0, rangemax = 300, target = 3, combo1 = "11,60,1" , combo2 = "14,100,1" , resetcombo =1 },
-- 10 트윙클 스핀
     { skill_index = 84524, cooltime = 12000, rate = -1, rangemin = 0, rangemax = 1200, target = 3 ,combo1 = "7,100,0" },
-- 11 엘레강스 스톰
     { skill_index = 84526, cooltime = 25600, rate = 20, rangemin = 0, rangemax = 300, target = 3, combo1 = "14,50,1", combo2 = "10,100,1" },
-- 12 일루전 댄스
     { skill_index = 84528, cooltime = 12000, rate = 30, rangemin = 300, rangemax = 1200, target = 3, combo1 = "15,100,0" },
-- 13 일루전 게이즈
     { skill_index = 84529, cooltime = 20000, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "10,50,1" , resetcombo =1},	

-- 14 더스크헌터 EX
     { skill_index = 84551, cooltime = 8800, rate = 20, rangemin = 0, rangemax = 700, target = 3, cancellook = 1},

-- 15 시니아 턴
     { skill_index = 84540, cooltime = 12000, rate = 20, rangemin = 0, rangemax = 700, target = 3, combo1 = "3,60,1", combo2 = "1,100,1" ,combo4 ="2,100,0"},	
	 
-- 16 평타1
     { skill_index = 84556, cooltime = 100, rate = 50, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,100,1", combo2 = "1,100,0", target_condition = "State3"},
-- 17 평타2
     { skill_index = 84557, cooltime = 100, rate = -1, rangemin = 0, rangemax = 150, target = 3, combo1 = "18,100,1", combo2 = "9,100,0" },
-- 18 평타3
     { skill_index = 84558, cooltime = 100, rate = -1, rangemin = 0, rangemax = 800, target = 3, combo1 = "1,60,1", combo2 = "14,70,1", combo3 =  "10,100,0" },	

	 }