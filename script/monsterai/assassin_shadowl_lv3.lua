
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_NoAggroOwnerFollow=1
g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|Air", "Down" },
 State2 = {"Move|Attack", "Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Hit","Down","Stiff"},
}


g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 10000

g_Lua_Near1 = 
{ 
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
      { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}

g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Move_Front", rate = 20, loop = 2 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Move_Front", rate = 20, loop = 2 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Move_Front", rate = 20, loop = 3 },
}

g_Lua_Assault = { 
     --{ action_name = "SKill_Dash", rate = -1, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Move_Back", rate = 10, loop = 1 },
	 { action_name = "Skill_EvasionSlash", rate = 30, loop = 1, cooltime = 15000,},
     { action_name = "CustomAction2", rate = 10, loop = 1  },
     { action_name = "CustomAction3", rate = 10, loop = 1 },
}

g_Lua_NonDownRangeDamage = {

	 { action_name = "Skill_DeepStraight_EX_Dash", rate = 30, loop = 1, cooltime = 14400 },
	 { action_name = "useskill", lua_skill_index = 9, rate = 50 },
}

g_Lua_Skill = { 
-- 0아웃 브레이크
     { skill_index = 262431, cooltime = 3000, rate = 40, rangemin = 0, rangemax = 600, target = 3, combo1 = "5,100,0", priority= 3},
-- 1아웃 브레이크 추가타
	 { skill_index = 262432, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo = "7,100,0"},
-- 2아웃 브레이크 EX
     { skill_index = 262433, cooltime = 7000, rate = -1, rangemin = 0, rangemax = 500, target = 3, combo1 = "3,50,1", combo2 = "4,100,1"}, 
-- 3아웃 브레이크 EX 추가타
     { skill_index = 262434, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,60,1"},
-- 4아웃 브레이크 EX 추가타 공중
     { skill_index = 262435, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,60,1"},
-- 5링 스트라이크 
     { skill_index = 262462, cooltime = 3000, rate = 40, rangemin = 0, rangemax = 200, target = 3, priority= 2, combo1="0,100,0"},
-- 6링 스트라이크 EX
     { skill_index = 262431, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3},
-- 7플라즈마 버스터
     { skill_index = 262492, cooltime = 3000, rate = 40, rangemin = 0, rangemax = 500, target = 3 , priority= 2, combo1 = "10,100,0"},
-- 8플라즈마 버스터EX
     { skill_index = 262494, cooltime = 9000, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "9,100,1" },
-- 9플라즈마 버스터EX 추가타
     { skill_index = 262495, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,60,1", combo2 = "9,30,1", combo3 = "1,100,1", combo4 = "28,100,0" },
-- 10라인오브 다크니스
     { skill_index = 262522, cooltime = 3000, rate = 40, rangemin = 0, rangemax = 600, target = 3,  priority= 3, combo1 = "11,100,0" },
-- 11라인 오브 다크니스 추가타
     { skill_index = 262523, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "7,100,0"  },	
-- 12라인오브 다크니스EX
     { skill_index = 262431, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,60,1", combo2 = "9,30,1", combo3 = "1,100,1", combo4 = "28,100,0" },
-- 13라인오브 다크니스EX 추가타
     { skill_index = 262431, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,60,1", combo2 = "9,30,1", combo3 = "1,100,1", combo4 = "28,100,0" },
-- 14라인오브 다크니스EX 막타
     { skill_index = 262431, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,60,1", combo2 = "9,30,1", combo3 = "1,100,1", combo4 = "28,100,0" },	
-- 15평타
     { skill_index = 262582, cooltime = 0, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "16,100,1"},	
-- 16평타2
     { skill_index = 262583, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,100,0" },
-- 17평타3
     { skill_index = 262584, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "18,100,0" },
-- 18평타4
     { skill_index = 262585, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 600, target = 3 , combo1 = "1,100,1"},

-- 19덤블링F
     { skill_index = 262588, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,60,1", combo2 = "9,30,1", combo3 = "1,100,1", combo4 = "28,100,0" },
-- 20덤블링B
     { skill_index = 262589, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,60,1", combo2 = "9,30,1", combo3 = "1,100,1", combo4 = "28,100,0" },
-- 21덤블링R
     { skill_index = 262587, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,60,1", combo2 = "9,30,1", combo3 = "1,100,1", combo4 = "28,100,0" },
-- 22덤블링L
     { skill_index = 262586, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "17,60,1", combo2 = "9,30,1", combo3 = "1,100,1", combo4 = "28,100,0" },	 
	 }