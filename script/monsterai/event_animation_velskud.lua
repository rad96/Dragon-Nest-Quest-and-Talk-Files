
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 950;
g_Lua_NearValue5 = 1250;
g_Lua_NearValue6 = 1550;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air|Stiff","!Move"}, 
}

g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Stand_1", rate = 2, loop = 1 },
     { action_name = "Walk_Left", rate = 4, loop = 2 },
     { action_name = "Walk_Right", rate = 4, loop = 2 },
     { action_name = "Move_Back", rate = 2, loop = 1 },
	 
}

g_Lua_Near2 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Stand_1", rate = 2, loop = 1 },
     { action_name = "Move_Front", rate = 6, loop = 1 },
     { action_name = "Move_Left", rate = 4, loop = 1 },
     { action_name = "Move_Right", rate = 4, loop = 1 },
}

g_Lua_Near3 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 8, loop = 2 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
}

g_Lua_Near4 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 8, loop = 4 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
     { action_name = "Assault", rate = 4, loop = 1 },
}

g_Lua_Near5 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 3 },
     { action_name = "Move_Left", rate = 3, loop = 2 },
     { action_name = "Move_Right", rate = 3, loop = 2 },
     { action_name = "Assault", rate = 4, loop = 1 },
}

g_Lua_Near6 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 5 },
     { action_name = "Assault", rate = 4, loop = 1 },
}

g_Lua_Assault = { 
   { action_name = "Skill_Nanmu", rate = 16, approach = 300 },
}



g_Lua_Skill =
{
--브레이크
     { skill_index = 96181, cooltime = 15000, rangemin = 0, rangemax = 500, target = 3, rate = 60 },
--이스케이프 스워드
     { skill_index = 96184, cooltime = 15000, rangemin = 0, rangemax = 200, target = 3, rate = 70 },
--난무
     { skill_index = 96185, cooltime = 9000, rangemin = 0, rangemax = 300, target = 3, rate = 50 },
--스톰프앤슬래쉬
     { skill_index = 96191, cooltime = 27000, rangemin = 0, rangemax = 400, target = 3, rate = 80 },
--어퍼
     { skill_index = 96192, cooltime = 15000, rangemin = 0, rangemax = 300, target = 3, rate = 70 },
} 
  