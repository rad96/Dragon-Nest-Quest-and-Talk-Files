--DarkLairDefence_Lv15_MinotaurosRed.lua
--스톰프,도끼던지기,광분,버프는 사용하지 않는다.

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 3500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_SkillProcessor = {
   { skill_index = 20332, changetarget = "2000,0" },
}

g_Lua_BeHitSkill = {
   { lua_skill_index = 1, rate = 100, skill_index = 20334 },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 3 },
   { action_name = "Walk_Right", rate = 8, loop = 3 },
   { action_name = "Walk_Back", rate = 4, loop = 2 },
   { action_name = "Attack2_Slash_Red", rate = 8, loop = 1, cooltime = 12000 },
   { action_name = "Attack1_bash_Red", rate = 8, loop = 1, cooltime = 18000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 3 },
   { action_name = "Walk_Right", rate = 8, loop = 3 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Attack2_Slash_Red", rate = 8, loop = 1, cooltime = 12000 },
   { action_name = "Attack1_bash_Red", rate = 8, loop = 1, cooltime = 18000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 2 },
   { action_name = "Walk_Right", rate = 5, loop = 2 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Walk_Back", rate = 3, loop = 1 },
   { action_name = "Move_Left", rate = 10, loop = 1 },
   { action_name = "Move_Right", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 35, loop = 1 },
   { action_name = "Assault", rate = 20, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 35, loop = 1 },
   { action_name = "Assault", rate = 20, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 7, loop = 1 },
   { action_name = "Move_Right", rate = 7, loop = 1 },
   { action_name = "Move_Front", rate = 25, loop = 1 },
   { action_name = "Assault", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
   { action_name = "Attack4_Pushed_Red", rate = 8, loop = 1, approach = 250.0 },
}

g_Lua_Skill = { 
-- 도끼 던지기
   { skill_index = 20332, cooltime = 30000, rate = 30, rangemin = 400, rangemax = 1300, target = 3 },
-- 도끼 던지기(원거리시 확률 증가와 쿨타임 감소)
   { skill_index = 20332, cooltime = 15000, rate = 70, rangemin = 800, rangemax = 2000, target = 3 },
-- 돌진
   { skill_index = 20335, cooltime = 45000, rate = 50, rangemin = 100, rangemax = 1300, target = 3, cancellook = 1 },
-- 포효
   { skill_index = 20334, cooltime = 30000, rate = 50, rangemin = 600, rangemax = 3500, target = 3 },
}