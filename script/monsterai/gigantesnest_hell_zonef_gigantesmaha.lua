--GreenDragonNest_Gate1_Golem_Blue.lua


g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 25000
g_Lua_GlobalCoolTime2 = 30000
g_Lua_GlobalCoolTime3 = 20000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Back", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 15, loop = 1, cooltime = 15000, globalcoomtime = 3 },
   { action_name = "Walk_Right", rate = 15, loop = 1, cooltime = 15000, globalcoomtime = 3 },
   { action_name = "Attack01_Punch", rate = 10, loop = 1, cooltime = 10000 },
   { action_name = "Attack02_CannonPunch", rate = 10, loop = 1, cooltime = 10000 },
   { action_name = "Attack06_RightAttack", rate = 10, loop = 1, cooltime = 12000, td = "FR,RF,RB,BR" },
   { action_name = "Attack07_LeftAttack", rate = 10, loop = 1, cooltime = 12000, td = "FL,LF,BL,LB" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 1, cooltime = 15000, globalcoomtime = 3 },
   { action_name = "Walk_Right", rate = 10, loop = 1, cooltime = 15000, globalcoomtime = 3 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 1 },
   { action_name = "Assault", rate = 30, loop = 1, cooltime = 27000, selfhppercent = 95 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack03_PunchHammer", rate = 30, loop = 1, approach = 250.0  },
}

g_Lua_Skill = {
   -- 돌진
   { skill_index = 31838,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
   -- 흡수
   { skill_index = 31843,  cooltime = 40000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "80,100", next_lua_skill_index = 0, encountertime = 10000 },
   -- 양발구르기
   { skill_index = 31839,  cooltime = 60000, rate = 50, rangemin = 0, rangemax = 3500, target = 3, multipletarget = "1,4", encountertime = 5000 },
   -- 내리치기
   --{ skill_index = 31834,  cooltime = 25000, rate = 50, rangemin = 200, rangemax = 600, target = 3, selfhppercentrange = "80,100" },
   -- 양손내리치기
   { skill_index = 31836,  cooltime = 35000, rate = 50, rangemin = 400, rangemax = 1200, target = 3, selfhppercentrange = "0, 39" },
   -- 포탄 포격
   { skill_index = 31837,  cooltime = 35000, rate = 50, rangemin = 700, rangemax = 1500, target = 3, selfhppercentrange = "80,100" },
   -- 자폭장치
   { skill_index = 33073,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80, next_lua_skill_index = 6, limitcount = 1 },
   { skill_index = 33068,  cooltime = 15000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 33073,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 40, next_lua_skill_index = 8, limitcount = 1 },
   { skill_index = 33068,  cooltime = 15000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   -- 클러스터폭탄
   { skill_index = 31841,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 1700, target = 3, selfhppercentrange = "40,79" },
   -- 미사일난사
   { skill_index = 31844,  cooltime = 35000, rate = 50, rangemin = 0, rangemax = 2500, target = 3, globalcoomtime = 2, selfhppercentrange = "80,100" },
   -- 미사일난사(전방향)
   { skill_index = 31845,  cooltime = 35000, rate = 50, rangemin = 0, rangemax = 2500, target = 3, globalcoomtime = 2, selfhppercentrange = "0,39" },
   -- 판넬 사출
   { skill_index = 33063,  cooltime = 30000, rate = 50, rangemin = 300, rangemax = 3000, target = 3, encountertime = 10000 },
   -- 레이저샤워
   { skill_index = 33064,  cooltime = 30000, rate = 50, rangemin = 300, rangemax = 900, target = 3, selfhppercent = 39 },
   -- 빔포
   { skill_index = 31842,  cooltime = 30000, rate = 50, rangemin = 300, rangemax = 600, target = 3, selfhppercentrange = "80,100" },

}