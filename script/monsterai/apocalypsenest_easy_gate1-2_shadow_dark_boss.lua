
--AiShadow_Dark_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 450;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 2  },
   { action_name = "Attack1_Claw", rate = 14, loop = 1  },
   { action_name = "Attack10_BossAction_Apocalypse", rate = 100, loop = 1, td = "FL,FR,RB,BR,BL,LB", selfhppercentrange = "15,20", cooltime = 90000  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 30, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Back", rate = 30, loop = 1  },
   { action_name = "Attack1_Claw", rate = 17, loop = 1  },
   { action_name = "Attack2_Curve", rate = 17, loop = 1  },
   { action_name = "Attack10_BossAction_Apocalypse", rate = 100, loop = 1, td = "FL,FR,RB,BR,BL,LB", selfhppercentrange = "15,20", cooltime = 90000  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack1_Claw", rate = 14, loop = 1  },
   { action_name = "Attack4_Pierce", rate = 21, loop = 1  },
   { action_name = "Attack10_BossAction_Apocalypse", rate = 100, loop = 1, td = "FL,FR,RB,BR,BL,LB", selfhppercentrange = "15,20", cooltime = 90000  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Attack10_BossAction_Apocalypse", rate = 100, loop = 1, td = "FL,FR,RB,BR,BL,LB", selfhppercentrange = "15,20", cooltime = 90000  },
}
g_Lua_NearNonDownMeleeDamage = { 
   { action_name = "Attack1_Claw", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 20, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack4_Pierce", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 15, loop = 2  },
   { action_name = "Move_Right", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20240,  cooltime = 60000, rate = 50, rangemin = 0, rangemax = 1200, target = 3, selfhppercent = 100 },
   { skill_index = 20241,  cooltime = 60000, rate = 50, rangemin = 500, rangemax = 800, target = 3, selfhppercent = 100  },
}
