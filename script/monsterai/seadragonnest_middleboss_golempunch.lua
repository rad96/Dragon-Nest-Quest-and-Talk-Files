--SeaDragonNest_MiddleBoss_GolemPunch.lua
--/genmon 235756
--Stand1 : 지하
--Stand2 : 지상
--30516 : 주먹공격 및 DIESKILL
--30517 : 체력이 0 이 되면 요 스킬 사용
--30518 : 시간이 10초가 지나면 땅 속으로 들어간다

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 10, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 10, loop = 1 },
}