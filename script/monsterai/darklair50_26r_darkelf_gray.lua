--AiDarkElf_Gray_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Attack1_Claw", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
   { action_name = "Attack4_SickleKick", rate = 7, loop = 1, max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 2  },
   { action_name = "Walk_Right", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 7, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 7, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_2", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 7, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 5, loop = 1, approach = 150.0  },
}
