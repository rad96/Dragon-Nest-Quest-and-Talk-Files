--AiBoarWarrior_Red_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop=1 },
   { action_name = "Walk_Back", rate = 7, loop=1 },
   { action_name = "Walk_Left", rate = 5, loop=1 },
   { action_name = "Walk_Right", rate = 5, loop=1 },
   { action_name = "Attack02_Chop", rate = 9, loop=1,max_missradian = 30 },
   { action_name = "Attack03_TripleSwing", rate = 6, loop=1,max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 6, loop=1 },
   { action_name = "Walk_Front", rate = 7, loop=2 },
   { action_name = "Walk_Back", rate = 4, loop=1 },
   { action_name = "Walk_Left", rate = 5, loop=2 },
   { action_name = "Walk_Right", rate = 5, loop=2 },
   { action_name = "Attack06_Combo", rate = 9, loop=1,max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop=1 },
   { action_name = "Walk_Front", rate = 8, loop=2 },
   { action_name = "Walk_Left", rate = 4, loop=2 },
   { action_name = "Walk_Right", rate = 4, loop=2 },
   { action_name = "Move_Front", rate = 9, loop=1 },
   { action_name = "Assault", rate = 9, loop=1 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 15, loop=2 },
}
g_Lua_Assault = { 
   { action_name = "Attack02_Chop", rate = 4, loop = 1, approach = 250, max_missradian = 30 },
   { action_name = "Attack03_TripleSwing", rate = 4, loop = 1, approach = 250, max_missradian = 30 },
   { action_name = "Walk_Back", rate = 6, loop = 1, approach = 250 },
   { action_name = "Stand", rate = 6, loop = 1, approach = 250 },
}
