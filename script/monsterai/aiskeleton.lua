-- Skeleton AI

g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 0.0;
g_Lua_NearValue2 = 150.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 700.0;
g_Lua_NearValue5 = 900.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 500;
g_Lua_AssualtTime = 5000;

g_Lua_CustomAction =
{
	CustomAction1 = 
	{
		{ "Attack4_FlyAttack", 1},
		{ "Attack1", 1},
		{ "Attack2", 1},
	},

  CustomAction2 = 
	{
		{ "Walk_Right", 1},
		{ "Walk_Left", 1},
		{ "Walk_Right", 1},
		{ "Attack1", 1},
	}
}



g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Back",	rate = 5,		loop = 2 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 3,		loop = 2 },
	{ action_name = "Attack1_Chop",	rate = 10,		loop = 1 },
	{ action_name = "Attack2_Cut",	rate = 10,		loop = 1 },
	{ action_name = "Attack4_FlyAttack",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction1",	rate = 0,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 3,		loop = 2 },
            { action_name = "Attack6_Scream",	rate = 10,		loop = 1 },
	{ action_name = "Attack8_Whirlwind",	rate = 10,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 1,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Front",	rate = 5,		loop = 3 },
	{ action_name = "Move_Back",	rate = 1,		loop = 3 },
	{ action_name = "Attack5_JumpAttack",	rate = 30,		loop = 1, cancellook = 1 },
	{ action_name = "Assault",	rate = 30,		loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 30,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Front",	rate = 30,		loop = 3 },
	{ action_name = "Assault",	rate = 50,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 30,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Front",	rate = 30,		loop = 3 },
	{ action_name = "Assault",	rate = 30,		loop = 1 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Chop",	rate = 5,	loop = 1, cancellook = 0, approach = 200.0 },
	{ action_name = "Attack1_Chop",	rate = 5,	loop = 1, cancellook = 1, approach = 200.0 },
}