--DarkLairDefence_Lv21_OrcGray.luav
--오크4마리가 나오는 커맨더 모드, 각 오크별로 특징을 준다
--기본 AI는 오크그린

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Attack2_bash", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Attack2_bash", rate = 4, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 12, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Assault", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 7, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 2  },
   { action_name = "Move_Right", rate = 2, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Stand2", rate = 4, loop = 2 },
   { action_name = "Walk_Back", rate = 1, loop = 2 },
   { action_name = "Walk_Front", rate = 15, loop = 2 },
   { action_name = "Move_Front", rate = 15, loop = 2 },
   { action_name = "Walk_Left", rate = 4, loop = 2 },
   { action_name = "Walk_Right", rate = 4, loop = 2 },
   { action_name = "Move_Left", rate = 1, loop = 2 },
   { action_name = "Move_Right", rate = 1, loop = 2 },
}
g_Lua_MeleeDefense = { 
   { action_name = "Attack1_Pushed", rate = -1, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
}
g_Lua_RangeDefense = { 
   { action_name = "Assault", rate = 70, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 70, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_bash", rate = 30, loop = 2, cancellook = 0, approach = 250.0  },
}