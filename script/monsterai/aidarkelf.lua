-- DarkElf AI

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 0.0;
g_Lua_NearValue2 = 150.0;
g_Lua_NearValue3 = 250.0;
g_Lua_NearValue4 = 700.0;
g_Lua_NearValue5 = 1200.0;


g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_CustomAction =
{
	CustomAction1 = 
	{
	 	{ "Attack3", 2},
                { "Move_Back", 1},
	 	{ "Attack1", 1},
	},

        CustomAction2 = 
	{
	 	{ "Walk_Right", 1},
	 	{ "Walk_Left", 1},
                { "Walk_Right", 1},
                { "Attack1", 1},
	}
}



g_Lua_Near1 = 
{ 
            { action_name = "Walk_Back",	rate = 10,		loop = 2 },
            { action_name = "Move_Back",	rate = 10,		loop = 2 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	{ action_name = "Move_Back",	rate = 15,		loop = 2 },
            { action_name = "Attack1_Claw",	rate = 30,		loop = 1 },
	{ action_name = "Attack3_AerialCombo",	rate = 30,		loop = 1, cancellook = 1 },
	{ action_name = "Attack2_Somersault",	rate = 30,		loop = 1 },
}

g_Lua_Near3 = 
{ 
        { action_name = "Stand",	rate = 1,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 10,		loop = 3 },
	{ action_name = "Move_Right",	rate = 10,		loop = 3 },
	{ action_name = "Move_Front",	rate = 10,		loop = 3 },
	{ action_name = "Move_Back",	rate = 20,		loop = 3 },
	{ action_name = "Attack4_SickleKick",	rate = 40,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 3 },
	{ action_name = "Move_Left",	rate = 10,		loop = 3 },
	{ action_name = "Move_Right",	rate = 10,		loop = 3 },
	{ action_name = "Move_Front",	rate = 20,		loop = 3 },
	{ action_name = "Move_Back",	rate = 15,		loop = 2 },
	{ action_name = "Assault",	rate = 40,		loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 3 },
	{ action_name = "Move_Left",	rate = 15,		loop = 3 },
	{ action_name = "Move_Right",	rate = 15,		loop = 3 },
	{ action_name = "Move_Front",	rate = 30,		loop = 3 },
	{ action_name = "Move_Back",	rate = 15,		loop = 2 },
	{ action_name = "Assault",	rate = 10,		loop = 1 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Claw",	rate = 5,	loop = 1, cancellook = 0, approach = 200.0 },
	{ action_name = "Attack3_AerialCombo",	rate = 5,	loop = 1, cancellook = 1, approach = 200.0 },
	{ action_name = "Attack2_Somersault",	     rate = 5,	loop = 1, cancellook = 0, approach = 200.0 },
}
