--AiHound_General_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Attack1_BiteCombo", rate = 10, loop = 1  },
   { action_name = "Attack2_BigBite", rate = 10, loop = 1  },
   { action_name = "Provoke", rate = 30, loop = 1, target_condition = "State1"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 6, loop = 1  },
   { action_name = "Attack6_JumpBite", rate = 6, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },
}

g_Lua_Assault = { 
   { action_name = "Attack2_BigBite", rate = 5, loop = 1, cancellook = 1, approach = 250.0  },
   { action_name = "Attack1_BiteCombo", rate = 3, loop = 1, cancellook = 1, approach = 100.0  },
}
