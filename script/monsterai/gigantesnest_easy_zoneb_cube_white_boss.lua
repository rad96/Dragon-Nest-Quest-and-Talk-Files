--AiCube_White_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 18000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   --{ action_name = "Attack2_Bit_Named", rate = 20, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 12, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2  },
   { action_name = "Move_Front", rate = 12, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 12, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 1  },
}
g_Lua_Skill = { 
   -- ȭ�̾�
   { skill_index = 20924, cooltime = 50000, rate = 100, rangemin = 0, rangemax = 1500, target = 3 },
   -- ����
   { skill_index = 20925, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 400, target = 3 },
   -- Ʈ��
   { skill_index = 31737, cooltime = 27000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "50,100", randomtarget=1.1, globalcoomtime = 1 },
   -- ȥ��
   { skill_index = 31735, cooltime = 30000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,49", randomtarget=1.1, globalcoomtime = 1 },
   -- ���⼺ ��ź(����)
   { skill_index = 31732, cooltime = 38000, rate = 100, rangemin = 0, rangemax = 800, target = 3 },
   -- �ӻ���(���ο�)
   { skill_index = 31734, cooltime = 33000, rate = 100, rangemin = 200, rangemax = 1400, target = 3 },
   -- ������ ��ź(����)
   { skill_index = 31731, cooltime = 33000, rate = 100, rangemin = 200, rangemax = 1500, target = 3, selfhppercentrange = "0,75" },
   -- ����(����)
   { skill_index = 31733, cooltime = 41000, rate = 100, rangemin = 400, rangemax = 1500, target = 3, selfhppercentrange = "0,75" },
   
}