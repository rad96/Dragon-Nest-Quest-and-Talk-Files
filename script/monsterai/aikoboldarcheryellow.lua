-- KoboldArcher AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 400.0;
g_Lua_NearValue2 = 900.0;
g_Lua_NearValue3 = 1200.0;
g_Lua_NearValue4 = 1500.0;

g_Lua_LookTargetNearState = 4;

g_Lua_WanderingDistance = 1500.0;

g_Lua_PatrolBaseTime = 5000;

g_Lua_PatrolRandTime = 3000;


g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 20,		loop = 2 },
	{ action_name = "Move_Left",	rate = 10,		loop = 2 },
	{ action_name = "Move_Right",	rate = 10,		loop = 2 },
	{ action_name = "Move_Back",	rate = 30,		loop = 2 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 15,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 15,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 10,		loop = 3 },
	{ action_name = "Move_Right",	rate = 10,		loop = 3 },
	{ action_name = "Move_Back",	rate = 5,		loop = 2 },
	{ action_name = "Attack1",	rate = 5,		loop = 2 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 3 },
	{ action_name = "Move_Left",	rate = 10,		loop = 3 },
	{ action_name = "Move_Right",	rate = 10,		loop = 3 },
	{ action_name = "Move_Front",	rate = 10,		loop = 3 },
	{ action_name = "Move_Back",	rate = 5,		loop = 3 },
	{ action_name = "Attack1",	rate = 15,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 10,		loop = 2 },
	{ action_name = "Move_Right",	rate = 10,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 2 },
}
