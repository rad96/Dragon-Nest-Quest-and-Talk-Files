--AiHarpy_White_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 25, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 20, loop = 3  },
   { action_name = "Attack1_Throw", rate = 10, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 20, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 15, loop = 3  },
   { action_name = "Walk_Right", rate = 15, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 3  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 3  },
   { action_name = "Attack1_Throw", rate = 20, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 15, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 15, loop = 3  },
   { action_name = "Walk_Right", rate = 15, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
   { action_name = "Attack1_Throw", rate = 30, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 3  },
   { action_name = "Move_Right", rate = 10, loop = 3  },
   { action_name = "Attack1_Throw", rate = 20, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20211,  cooltime = 20000, rate = 100,rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 80 },
}
