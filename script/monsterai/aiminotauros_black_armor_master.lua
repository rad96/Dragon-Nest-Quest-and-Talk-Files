--AiMinotauros_Black_Armor_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 350;
g_Lua_NearValue3 = 500;
g_Lua_NearValue4 = 750;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 30, loop = 2  },
   { action_name = "Attack1_bash", rate = 9, loop = 1  },
   { action_name = "Attack2_Slash", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 6, loop = 2  },
   { action_name = "Walk_Right", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Attack1_bash", rate = 6, loop = 1  },
   { action_name = "Attack3_DashAttack", rate = 2, loop = 1  },
   { action_name = "Attack2_Slash", rate = 7, loop = 1  },
   { action_name = "Attack9_BigBash", rate = 6, loop = 1, selfhppercent = 50 },
   { action_name = "Attack9_BigBash", rate = 5, loop = 1, selfhppercent = 30 },
   { action_name = "Attack2_Slash", rate = -1, loop = 1,target_condition = "State1"   },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 23, loop = 1  },
   { action_name = "Assault", rate = 12, loop = 1  },
   { action_name = "Attack3_DashAttack", rate = 6, loop = 1  },
   { action_name = "Attack9_BigBash", rate = 15, loop = 1, selfhppercent = 50 },
   { action_name = "Attack9_BigBash", rate = 15, loop = 1, selfhppercent = 30 },
   { action_name = "Attack2_Slash", rate = -1, loop = 1,target_condition = "State1"   },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 35, loop = 2  },
   { action_name = "Assault", rate = 9, loop = 1  },
   { action_name = "Attack9_BigBash", rate = 15, loop = 1, selfhppercent = 50 },
   { action_name = "Attack9_BigBash", rate = 15, loop = 1, selfhppercent = 30 },
   { action_name = "Attack3_DashAttack", rate = 9, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 7, loop = 2  },
   { action_name = "Move_Right", rate = 7, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
   { action_name = "Attack3_DashAttack", rate = 7, loop = 1  },
   { action_name = "Assault", rate = 12, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = -1, loop = 1, approach = 300.0  },
   { action_name = "Attack4_Pushed", rate = 5, loop = 1, approach = 250.0  },
}
