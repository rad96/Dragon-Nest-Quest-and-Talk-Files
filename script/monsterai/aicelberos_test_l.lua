-- Celberos Normal TestL
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 400.0;
g_Lua_NearValue2 = 800.0;
g_Lua_NearValue3 = 1300.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 320;
g_Lua_AssualtTime = 3000;


g_Lua_Near1 = 
{ 
	{ action_name = "Stand_2",	rate = 5,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 5,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 5,		loop = -1, td = "RF,RB,BR" },
	{ action_name = "Attack_Back_Left",	rate = 15,		loop = 1, td = "LF,LB,BL" },
	{ action_name = "Attack_Back_Right",	rate = 15,		loop = 1, td = "RF,RB,BR" },
	{ action_name = "Avoid",	rate = 10,		loop = 1, td = "LF,FL,FR,RF", usedskill = 30103 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_2",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Front",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_2",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Front",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_2",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Move_Front",	rate = 10,		loop = 2, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand_2",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Move_Front",	rate = 30,	        loop = 2, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 5,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 5,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Skill=
{
-- 각 속성 버프
	{ skill_index = 30102, SP = 0, cooltime = 10000, rangemin = 000, rangemax = 3000, target = 3, existparts="6", notusedskill = "30101,30102,30103" },

	
-- 화염버프
	-- 화염돌격
	{ skill_index = 30109, SP = 0, cooltime = 7000, rangemin = 600, rangemax = 3000, target = 3, rate = 80, usedskill = 30102, td = "FL,RF", existparts="6", selfhppercent = 100 },
	-- 파이어월
	{ skill_index = 30107, SP = 0, cooltime = 15000, rangemin = 100, rangemax = 600, target = 3, rate = 50, usedskill = 30102, td = "LF,FL,FR,RF", existparts="6" },
	-- 화염구 난무
	{ skill_index = 30108, SP = 0, cooltime = 10000, rangemin = 300, rangemax = 1500, target = 3, rate = 50, usedskill = 30102, td = "FL,FR", existparts="6" },
}

