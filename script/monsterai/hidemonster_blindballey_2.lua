
g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 0.0;
g_Lua_NearValue2 = 100.0;
g_Lua_NearValue3 = 200.0;
g_Lua_NearValue4 = 450.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 120;
g_Lua_AssualtTime = 3000;


g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1 },
}

g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1 },
}

g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1 },
}

g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 20976,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 1, limitcount = 1 },
}