-- Kali_SummonPuppet.lua

g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 500.0;
g_Lua_NearValue2 = 2000.0;

g_Lua_LookTargetNearState = 2;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;

g_Lua_Near1 = 
{
	{ action_name = "Stand",		rate = 5,		loop = 1 }, 
}


g_Lua_Near2 = 
{ 
	{ action_name = "Stand",		rate = 10,		loop = 1 },
}

g_Lua_Skill=
{
	{skill_index = 30833, SP = 0, cooltime = 2000, rangemin = 0, rangemax = 400, target = 3, rate = 100 }, --자해 및 광역디버프
	--{skill_index = 30835, SP = 0, cooltime = 0, rangemin = 0, rangemax = 5000, target = 3, rate = 100, waitorder=5403, priority= 3 }
	--{skill_index= 30835,cooltime=0,rate=100,rangemin=0,rangemax=1200,target=2,selfhppercent=100,waitorder=5413, priority= 3}, --자힐
}