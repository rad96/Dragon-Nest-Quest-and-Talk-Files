--AiGoblin_Blue_Abyss.lua

g_Lua_NearTableCount = 7

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 350;
g_Lua_NearValue4 = 550;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;
g_Lua_NearValue7 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Attack1_Slash", rate = 9, loop = 2 },
}
g_Lua_Near2 = { 
   { action_name = "stand_1", rate = 3, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Attack1_Slash", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "stand_1", rate = 3, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack2_Upper", rate = 10, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "stand_1", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 1, loop = 1 },
   { action_name = "Move_Right", rate = 1, loop = 1 },
   { action_name = "Attack8_ThrowStone", rate = 10, loop = 1, max_missradian = 30, cooltime = 5000 },
}
g_Lua_Near5 = { 
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Attack8_ThrowStone", rate = 30, loop = 1, max_missradian = 30, cooltime = 5000 },
}
g_Lua_Near6 = { 
   { action_name = "Walk_Front", rate = 3, loop = 2 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
}
g_Lua_Near7 = { 
   { action_name = "Walk_Front", rate = 3, loop = 2 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Slash", rate = 5, loop = 1, cancellook = 0, approach = 250.0 },
   { action_name = "Attack2_Upper", rate = 5, loop = 1, cancellook = 1, approach = 250.0 },
}
g_Lua_Skill = { 
   { skill_index = 20003,  cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, max_missradian = 30 },
}
