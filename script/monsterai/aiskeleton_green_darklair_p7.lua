--AiSkeleton_Green_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1200;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Attack11_Separate_DarkLair", rate = 9, loop = 1, max_missradian = 270  },
}
g_Lua_Near2 = { 
   { action_name = "Attack11_Separate_DarkLair", rate = 9, loop = 1, max_missradian = 270  },
}
g_Lua_Near3 = { 
   { action_name = "Attack11_Separate_DarkLair", rate = 10, loop = 1, max_missradian = 270  },
}
g_Lua_Near4 = { 
   { action_name = "Attack11_Separate_DarkLair", rate = 10, loop = 1, max_missradian = 270  },
}
g_Lua_Near5 = { 
   { action_name = "Attack11_Separate_DarkLair", rate = 10, loop = 1, max_missradian = 270  },
}
g_Lua_Near6 = { 
   { action_name = "Attack11_Separate_DarkLair", rate = 10, loop = 1, max_missradian = 270  },
}
