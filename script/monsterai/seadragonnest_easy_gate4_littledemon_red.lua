--AiLittleDemon_Red_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 10000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 3  },
   { action_name = "Move_Back", rate = 10, loop = 3  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Assault", rate = 25, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 1  },
   { action_name = "Move_Right", rate = 10, loop = 1  },
   { action_name = "Assault", rate = 25, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 1, loop = 3  },
   { action_name = "Assault", rate = 25, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 5, loop = 1, cancellook = 0, approach = 50.0  },
}
g_Lua_Skill = { 
   { skill_index = 20313,  cooltime = 30000, rate = 100,rangemin = 100, rangemax = 300, target = 1, selfhppercent = 50 },
   { skill_index = 20312,  cooltime = 30000, rate = 100, rangemin = 300, rangemax = 500, target = 1, selfhppercent = 50 },
   { skill_index = 20311,  cooltime = 30000, rate = 100, rangemin = 500, rangemax = 800, target = 1, selfhppercent = 50 },
}
