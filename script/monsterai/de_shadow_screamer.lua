
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1100;
g_Lua_NearValue4 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;


g_Lua_CustomAction = {
-- 기본 공격 2연타 145프레임
  CustomAction1 = {
     { "Attack1_Fan" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction2 = {
     { "Attack1_Fan" },
     { "Attack2_Fan" },
  },
-- 기본 공격 4연타 313프레임
  CustomAction3 = {
     { "Attack1_Fan" },
     { "Attack2_Fan" },
     { "Attack3_Fan" },
  },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 6000

g_Lua_Near1 = 
{
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 6, loop = 1 },
     { action_name = "Move_Right", rate = 6, loop = 1 },
     { action_name = "Move_Back", rate = 2, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Back", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 3 },
     { action_name = "Move_Left", rate = 1, loop = 1 },
     { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 4 },
}

g_Lua_Skill = { 
-- 소울 윈드 Skill_SoulWind
   { skill_index = 31181, cooltime = 8000, rate = 70, rangemin = 300, rangemax = 1000, target = 3, globalcooltime = 2 },
-- 스팅 브리즈 Skill_StingBreeze
   { skill_index = 31182,  cooltime = 10000, rate = 20, rangemin = 0, rangemax = 200, target = 3, globalcooltime = 2 },
-- 디스페어 니들 Skill_DespairNeedle
   { skill_index = 31184,  cooltime = 15000, rate = 40, rangemin = 100, rangemax = 300, target = 3, globalcooltime = 2 },

-- 팬텀 클로 Skill_PhantomClaw_1
   -- { skill_index = 31185, cooltime = 15000, rate = 60, rangemin = 0, rangemax = 450, target = 3, globalcooltime = 2 },
-- 램피지 클로 Skill_RampageClaw
   -- { skill_index = 31186, cooltime = 14000, rate = 40, rangemin = 0, rangemax = 400, target = 3, globalcooltime = 2 },
-- 리벤지 핸드 Skill_RevengeHand
   -- { skill_index = 31187, cooltime = 14000, rate = 40, rangemin = 300, rangemax = 600, target = 3, selfhppercent = 25, globalcooltime = 2 },
-- 스피릿페이퍼 Skill_SpiritPaper
   -- { skill_index = 31188, cooltime = 31000, rate = 60, rangemin = 0, rangemax = 1000, target = 3, globalcooltime = 2 },
-- 비스트 스피릿 Skill_BeastSpirit
   -- { skill_index = 31189, cooltime = 27000, rate = 60, rangemin = 600, rangemax = 800, target = 3, selfhppercent = 50, globalcooltime = 2 },
}