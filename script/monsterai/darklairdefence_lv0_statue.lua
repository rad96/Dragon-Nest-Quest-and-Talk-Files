--DarkLairDefence_Lv0_Statue.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
   CustomAction1 = {
      { "useskill", lua_skill_index = 0 },
      { "useskill", lua_skill_index = 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
   { action_name = "CustomAction1", rate = 90, loop = 1, cooltime = 25000, encountertime = 5000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 50009, cooltime = 1, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
   { skill_index = 50006, cooltime = 1, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
}