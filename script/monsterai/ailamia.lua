-- Lamia AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 200.0;
g_Lua_NearValue2 = 400.0;
g_Lua_NearValue3 = 800.0;
g_Lua_NearValue4 = 1300.0;
g_Lua_NearValue5 = 1500.0;

g_Lua_LookTargetNearState = 5;

g_Lua_WanderingDistance = 1800.0;

g_Lua_PatrolBaseTime = 5000;

g_Lua_PatrolRandTime = 3000;


g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	{ action_name = "Move_Back",	rate = 5,		loop = 2 },
	{ action_name = "Attack1",	rate = 10,		loop = 1 },
            { action_name = "Attack2",	rate = 20,		loop = 1 },
--	{ action_name = "Attack5_ChargeCut",	rate = 10,		loop = 1 },
            { action_name = "Attack6_Combo",	rate = 20,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3 },
            { action_name = "Walk_Front",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 3 },
	{ action_name = "Move_Left",	rate = 10,		loop = 2 },
	{ action_name = "Move_Right",	rate = 10,		loop = 2 },
            { action_name = "Move_Front",	rate = 5,		loop = 2 },
	{ action_name = "Move_Back",	rate = 5,		loop = 2 },
	{ action_name = "Attack2",	rate = 20,		loop = 1 },
	{ action_name = "Attack6_Combo",	rate = 20,		loop = 1 },
}


g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 10,		loop = 3 },
	{ action_name = "Move_Right",	rate = 10,		loop = 3 },
	{ action_name = "Move_Front",	rate = 20,		loop = 2 },
	{ action_name = "Move_Back",	rate = 5,		loop = 2 },
	{ action_name = "Assault",	rate = 20,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 1,		loop = 2 },
}

g_Lua_MeleeDefense =
{ 
	{ action_name = "Walk_Front",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Back",	rate = 1,		loop = 1 },
	{ action_name = "Move_Front",	rate = 1,		loop = 1 },
	{ action_name = "Move_Back",	rate = 1,		loop = 1 },
	{ action_name = "Attack1",	rate = 3,		loop = 1 },
	{ action_name = "Attack3_BackCut",	rate = 3,		loop = 1 },
	{ action_name = "Attack4_RollBite",	rate = 3,		loop = 1 },
}
