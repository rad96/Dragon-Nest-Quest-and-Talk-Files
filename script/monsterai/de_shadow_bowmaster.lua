
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
 State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_CustomAction = {
-- 3��Ÿ ����
  CustomAction1 = {
      { "Shoot_Stand_BigBow" },
      { "Shoot_Stand_BigBow" },
      { "Shoot_Finish_BigBow" },
  },
-- ���� ���̵� ����
  CustomAction2 = {
      { "Move_Right" },
      { "Shoot_Stand_BigBow" },
  },
-- ���� ���̵� ����
  CustomAction3 = {
      { "Move_Left" },
      { "Shoot_Stand_BigBow" },
  },
-- ��������������
  CustomAction4 = {
     { "Tumble_Left" },
     { "Shoot_Stand_BigBow" },
  },
-- ��������������
  CustomAction5 = {
     { "Tumble_Right" },
     { "Shoot_Stand_BigBow" },
  },
-- �����������
  CustomAction5 = {
     { "Tumble_Back" },
     { "Shoot_Stand_BigBow" },
  },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 6000

g_Lua_Near1 = 
{
     { action_name = "Attack_Down", rate = 30, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Stand", rate = 2, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 10, loop = 2 },
     { action_name = "Tumble_Back", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, cooltime = 7000, globalcooltime = 1 },
     { action_name = "Attack_SideKick", rate = 12, loop = 1, cooltime = 30000, globalcooltime = 1 },
     { action_name = "CustomAction5", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 1, loop = 1 },
     { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, cooltime = 7000, globalcooltime = 1 },
--3��Ÿ ����
     { action_name = "CustomAction1", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
--�����̵� �� 3Ÿ
     { action_name = "CustomAction2", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Front", rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 2 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 4 },
}

g_Lua_NonDownMeleeDamage = {
      { action_name = "CustomAction4", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "CustomAction5", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "Move_Left", rate = 10, loop = 1 },
      { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_NonDownRangeDamage = {
      { action_name = "Stand", rate = 2, loop = 1 },
      { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, globalcooltime = 1 },
      { action_name = "CustomAction1", rate = 10, loop = 1, globalcooltime = 1 },
}

g_Lua_Skill = { 
-- Ʈ���� 4500 (600+80+200)
   { skill_index = 31031,  cooltime = 30000, rate = 30, rangemin = 0, rangemax = 800, target = 3, globalcooltime = 2 },
-- ���ο콺��ű 12000
   { skill_index = 31034,  cooltime = 20000, rate = 20, rangemin = 0, rangemax = 300, target = 3, globalcooltime = 2 },
-- ��Ƽ�� 28000(�����Ÿ� ����X)
   { skill_index = 31032,  cooltime = 35000, rate = 30, rangemin = 0, rangemax = 500, target = 3, globalcooltime = 2 },
-- �����ַο� 13000 (600+500+0)
   { skill_index = 31033,  cooltime = 15000, rate = 40, rangemin = 0, rangemax = 1100, target = 3, globalcooltime = 2 },

-- ���ǵ� ��
   -- { skill_index = 31047,  cooltime = 15000, rate = 80, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR", selfhppercent = 70, globalcooltime = 2 },
-- �ͽ��ټǿ��ο�
   -- { skill_index = 31046,  cooltime = 17000, rate = 40, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR", globalcooltime = 2 },
-- ������ 15000 (600+0+300)
   -- { skill_index = 31041,  cooltime = 15000, rate = 90, rangemin = 300, rangemax = 800, target = 3, globalcooltime = 2 },
-- ������Ʈ�� 18000
   -- { skill_index = 31045,  cooltime = 27000, rate = 80, rangemin = 0, rangemax = 650, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", globalcooltime = 2 },
-- �ַο���� 35000
   -- { skill_index = 31044,  cooltime = 35000, rate = 60, rangemin = 500, rangemax = 1500, target = 3, selfhppercent = 20, globalcooltime = 2 },
}