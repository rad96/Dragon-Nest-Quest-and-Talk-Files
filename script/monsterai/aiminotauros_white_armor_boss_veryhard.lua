-- Minotauros White Armor Boss Master AI

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 200.0;
g_Lua_NearValue2 = 350.0;
g_Lua_NearValue3 = 500.0;
g_Lua_NearValue4 = 750.0;
g_Lua_NearValue5 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 300;
g_Lua_AssualtTime = 3000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Stun" },
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand_1",	rate = 1,		loop = 1 },
        { action_name = "Walk_Left",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Back",	rate = 30,		loop = 1 },
        { action_name = "Attack1_Bash",	rate = 27,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
        { action_name = "Walk_Left",	rate = 6,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 6,		loop = 1 },
	{ action_name = "Walk_Back",	rate = 20,		loop = 1 },
        { action_name = "Attack1_Bash",	rate = 18,		loop = 1 },
        { action_name = "Attack3_DashAttack",	rate = 3,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 1 },
	{ action_name = "Move_Left",	rate = 10,		loop = 1 },
	{ action_name = "Move_Right",	rate = 10,		loop = 1 },
	{ action_name = "Move_Front",	rate = 23,		loop = 1 },
	{ action_name = "Attack3_DashAttack",	rate = 30,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 1 },
	{ action_name = "Move_Front",	rate = 35,		loop = 1 },
	{ action_name = "Assault",	rate = 15,		loop = 1 },
	{ action_name = "Attack3_DashAttack",	rate = 45,		loop = 1, cancellook = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand_1",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 7,		loop = 1 },
	{ action_name = "Move_Right",	rate = 7,		loop = 1 },
	{ action_name = "Move_Front",	rate = 25,		loop = 1 },
	{ action_name = "Attack3_DashAttack",	rate = 15,		loop = 1, },
        { action_name = "Assault",	rate = 15,		loop = 1 },
}


g_Lua_Assault =
{ 
	{ action_name = "Attack1_Bash",	rate = 8,	loop = 1, approach = 300.0 },
}

g_Lua_Skill=
{
	{ skill_index = 20202, SP = 0, cooltime = 12000, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100, rate = 100 },
	{ skill_index = 20203, SP = 0, cooltime = 12000, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 60, rate = 100 },
	{ skill_index = 20201, SP = 0, cooltime = 25000, rangemin = 0, rangemax = 800, target = 2, selfhppercent = 30, rate = 100 },
}    