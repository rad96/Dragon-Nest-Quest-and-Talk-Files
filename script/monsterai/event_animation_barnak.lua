
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1400;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|Air", "Down" },
 State2 = {"Move|Attack", "Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Hit","Down","Stiff"},
}

g_Lua_Near1 = 
{ 
     { action_name = "Attack_Down", rate = 15, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Stand_1", rate = 2, loop = 1 },
     { action_name = "Walk_Left", rate = 4, loop = 2 },
     { action_name = "Walk_Right", rate = 4, loop = 2 },
     { action_name = "Walk_Back1", rate = 2, loop = 1 },	 
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Stand_1", rate = 2, loop = 1 },
     { action_name = "Move_Front", rate = 6, loop = 2 },
     { action_name = "Move_Left", rate = 4, loop = 1 },
     { action_name = "Move_Right", rate = 4, loop = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 8, loop = 4 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
}
g_Lua_Near4 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 8, loop = 8 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
     { action_name = "Assault", rate = 4, loop = 1 },
}

g_Lua_Near5 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 10 },
     { action_name = "Move_Left", rate = 3, loop = 2 },
     { action_name = "Move_Right", rate = 3, loop = 2 },
     { action_name = "Assault", rate = 4, loop = 1 },
}

g_Lua_Assault = { 
   { action_name = "Skill_HeavySlash", rate = 16, approach = 200 },
}

g_Lua_GlobalCoolTime1 = 15000

g_Lua_Skill = { 
-- 0평타1
     { skill_index = 82049, cooltime = 9000, rate = 70, rangemin = 0, rangemax = 300, target = 3, td = "FL,FR", combo1 = "1,100,2" },
-- 1평타2
     { skill_index = 82050, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", combo1 = "2,100,2" },
-- 2평타3
     { skill_index = 82051, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", combo1 = "3,100,2" },
-- 3평타4
     { skill_index = 82052, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },

-- 4/ 21해킹 스탠스 EX
     { skill_index = 82045, cooltime = 40000, globalcooltime = 1, rate = 80, rangemin = 0, rangemax = 700, target = 3, combo1 = "8,100,0"},
-- 5/ 42해킹 스탠스 EX_L
     { skill_index = 82059, cooltime = 100, rate = -1, rangemin = 0, rangemax = 1500, target = 3, combo1 = "6,100,2" , combo2 = "8,100,0",  usedskill = "82045" },
-- 6/ 43해킹 스탠스 EX_L2
     { skill_index = 82062, cooltime = 100, rate = -1, rangemin = 0, rangemax = 1500, target = 3, combo1 = "5,100,2" , combo2 = "8,100,0",  usedskill = "82045" },	 	 
-- 7/ 44해킹 스탠스 EX_R
     { skill_index = 82060, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 700, target = 3, combo1 = "0,100,0" },	 
-- 8/ 45해킹 스탠스 EX_Dash
     { skill_index = 82061, cooltime = 100, rate = -1, rangemin = 0, rangemax = 1500, target = 3, combo1 = "6,100,0" , approach = 10,  usedskill = "82045" },

-- 사이클론 슬래쉬
     { skill_index = 82016, cooltime = 12000, rate = 80, rangemin = 500, rangemax = 1200, target = 3 },
-- 이클립스
     { skill_index = 82023, cooltime = 16000, rate = 70, rangemin = 0, rangemax = 400, target = 3 },
-- 서클브레이크
     { skill_index = 82003, cooltime = 15000, rate = 60, rangemin = 0, rangemax = 400, target = 3 },
-- 58 휠 윈드EX
     { skill_index = 96037, cooltime = 32000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 500, target = 3 },	 	 	 
}