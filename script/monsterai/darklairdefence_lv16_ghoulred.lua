--DarkLairDefence_Lv16_GhoulRed.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 2  },
   { action_name = "Attack1_ArmAttack", rate = 6, loop = 1  },
   { action_name = "Attack1_ArmAttack", rate = 9, loop = 1, encountertime = 40000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Attack1_ArmAttack", rate = 4, loop = 1  },
   { action_name = "Attack1_ArmAttack", rate = 9, loop = 1, encountertime = 40000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Front", rate = 2, loop = 2  },
   { action_name = "Move_HitFront", rate = 2, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 2, loop = 2  },
   { action_name = "Move_HitFront", rate = 2, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 2, loop = 2  },
   { action_name = "Move_HitFront", rate = 2, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 50011,  cooltime = 5000, rate = 100, rangemin = 100, rangemax = 1500, target = 3, encountertime = 20000 },
}
