-- Ogre AI

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 200.0;
g_Lua_NearValue2 = 300.0;
g_Lua_NearValue3 = 600.0;
g_Lua_NearValue4 = 1000.0;
g_Lua_NearValue5 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 3000;


g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Attack1_Bash",	rate = 25,		loop = 2 },
	{ action_name = "Attack2_Berserk",	rate = 5,		loop = 1 },
	{ action_name = "Attack6_HornAttack",	rate = 15,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
            { action_name = "Walk_Back",	rate = 1,		loop = 2 },
            { action_name = "Attack1_Bash",	rate = 20,		loop = 2 },
	{ action_name = "Attack2_Berserk",	rate = 10,		loop = 1 },
	{ action_name = "Attack6_HornAttack",	rate = 10,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 0,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Front",	rate = 30,		loop = 2 },
	{ action_name = "Attack2_Berserk",	rate = 10,		loop = 1 },
	{ action_name = "Attack7_JumpAttack",	rate = 20,		loop = 1 },
            { action_name = "Assault",	rate = 40,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 0,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 20,		loop = 2 },
	{ action_name = "Move_Front",	rate = 50,		loop = 1 },
            { action_name = "Assault",	rate = 50,		loop = 1 },

}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 0,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 1,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 1,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Front",	rate = 30,		loop = 1 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Bash",	rate = 5,	loop = 1, cancellook = 0, approach = 250.0 },
	{ action_name = "Attack7_JumpAttack",	rate = 5,	loop = 1, cancellook = 0, approach = 600.0 },
}
