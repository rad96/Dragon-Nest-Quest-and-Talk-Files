--AiManticore_Yellow_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1800;
g_Lua_NearValue5 = 3500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,RF", existparts="1" },
   { action_name = "Walk_Left", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Right", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Back", rate = 8, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Attack11_LHook", rate = 13, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1" },
   { action_name = "Attack11_LHook", rate = -1, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1", target_condition = "State2" },
   { action_name = "Attack11_RHook", rate = 13, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1" },
   { action_name = "Attack11_RHook", rate = -1, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1", target_condition = "State2" },
   { action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
   { action_name = "Attack4_Tail_L", rate = 9, loop = 1, custom_state1 = "custom_ground", td = "LF,LB,BL,BR", existparts="1" },
   { action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
   { action_name = "Attack5_Tail_R", rate = 9, loop = 1, custom_state1 = "custom_ground", td = "RF,RB,BL,BR", existparts="1" },
   { action_name = "Attack10_Bite", rate = 35, loop = 1, custom_state1 = "custom_ground", existparts="1", cooltime = 15000, td = "LF,FL,FR,RF" },
   { action_name = "Fly", rate = 10, loop = 1, custom_state1 = "custom_fly", existparts="1" },
   { action_name = "Attack7_Swoop", rate = 10, loop = 1, custom_state1 = "custom_fly", existparts="1", randomtarget=1.1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
   { action_name = "Walk_Left", rate = 5, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Right", rate = 5, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Front", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Back", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Attack11_LHook", rate = 22, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1" },
   { action_name = "Attack11_LHook", rate = -1, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1", target_condition = "State2" },
   { action_name = "Attack11_RHook", rate = 22, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1" },
   { action_name = "Attack11_RHook", rate = -1, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1", target_condition = "State2" },
   { action_name = "Turn_Left", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
   { action_name = "Turn_Right", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
   { action_name = "Attack10_Bite", rate = 33, loop = 1, custom_state1 = "custom_ground", existparts="1", cooltime = 15000, td = "LF,FL,FR,RF" },
   { action_name = "Fly", rate = 10, loop = 1, custom_state1 = "custom_fly", existparts="1" },
   { action_name = "Attack7_Swoop", rate = 10, loop = 1, custom_state1 = "custom_fly", existparts="1", randomtarget=1.1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = -1, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
   { action_name = "Walk_Left", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Right", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Front", rate = 30, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Back", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Turn_Left", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
   { action_name = "Turn_Right", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
   { action_name = "Attack9_Jump", rate = 60, loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", existparts="1", randomtarget=1.1 },
   { action_name = "Fly", rate = 10, loop = 1, custom_state1 = "custom_fly", existparts="1" },
   { action_name = "Attack7_Swoop", rate = 10, loop = 1, custom_state1 = "custom_fly", existparts="1", randomtarget=1.1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = -1, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
   { action_name = "Walk_Left", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Right", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Front", rate = 20, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Back", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Turn_Left", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
   { action_name = "Turn_Right", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
   { action_name = "Attack9_Jump", rate = 45, loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", existparts="1", randomtarget=1.1 },
   { action_name = "Fly", rate = 10, loop = 1, custom_state1 = "custom_fly", existparts="1" },
   { action_name = "Attack7_Swoop", rate = 10, loop = 1, custom_state1 = "custom_fly", existparts="1", randomtarget=1.1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
   { action_name = "Walk_Left", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Right", rate = 10, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Walk_Front", rate = 20, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { action_name = "Turn_Left", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
   { action_name = "Turn_Right", rate = 20, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
   { action_name = "Fly", rate = 10, loop = 1, custom_state1 = "custom_fly", existparts="1" },
   { action_name = "Attack7_Swoop", rate = 10, loop = 1, custom_state1 = "custom_fly", existparts="1", randomtarget=1.1 },
}
g_Lua_Skill = { 
   { skill_index = 30001,  cooltime = 15000, rate = -1,rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 80,  custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { skill_index = 30003,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 50,  custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { skill_index = 30004,  cooltime = 10000, rate = -1, rangemin = 0, rangemax = 700, target = 3, selfhppercent = 100,  custom_state1 = "custom_ground", existparts="1" },
   { skill_index = 30002,  cooltime = 90000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, selfhppercent = 10,  custom_state1 = "custom_ground", existparts="1" },
   { skill_index = 30005,  cooltime = 10000, rate = -1, rangemin = 500, rangemax = 1800, target = 3, selfhppercent = 100, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
   { skill_index = 30005,  cooltime = 5000, rate = 50, rangemin = 500, rangemax = 1800, target = 3, selfhppercent = 80, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
}
