--AiOgre_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_bash", rate = 3, loop = 1  },
   { action_name = "Attack3_Upper", rate = 3, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 2, loop = 2  },
   { action_name = "Move_Front", rate = 4, loop = 1  },
   { action_name = "Attack13_Dash", rate = 6, loop = 1, cooltime = 25000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 4, loop = 1  },
   { action_name = "Attack7_JumpAttack", rate = 4, loop = 1  },
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 9, loop = 1 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
   { action_name = "Attack13_Dash", rate = 6, loop = 1 },
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Front", rate = 30, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Assault", rate = 50, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 3, loop = 1, approach = 250.0  },
   { action_name = "Attack3_Upper", rate = 5, loop = 1, approach = 300.0  },
}
