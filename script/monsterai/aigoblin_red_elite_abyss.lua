--AiGoblin_Red_Elite_Abyss.lua

g_Lua_NearTableCount = 7

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 300;
g_Lua_NearValue4 = 500;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;
g_Lua_NearValue7 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 30, loop = 2  },
   { action_name = "Move_Back", rate = 30, loop = 2  },
   { action_name = "Attack1_Slash", rate = 22, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Back", rate = 21, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 2  },
   { action_name = "Attack1_Slash", rate = 23, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "stand_1", rate = 9, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 2  },
   { action_name = "Walk_Right", rate = 1, loop = 2  },
   { action_name = "Walk_Back", rate = 9, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Back", rate = 15, loop = 1  },
   { action_name = "Attack2_Upper", rate = 17, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 21, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 2  },
   { action_name = "Walk_Right", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = 6, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 21, loop = 2  },
   { action_name = "Assault", rate = 35, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand2", rate = 15, loop = 2  },
   { action_name = "Walk_Left", rate = 1, loop = 2  },
   { action_name = "Walk_Right", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
   { action_name = "Assault", rate = 32, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "stand_1", rate = 9, loop = 2  },
   { action_name = "Walk_Left", rate = 1, loop = 3  },
   { action_name = "Walk_Right", rate = 1, loop = 3  },
   { action_name = "Walk_Front", rate = 15, loop = 3  },
   { action_name = "Move_Left", rate = 4, loop = 3  },
   { action_name = "Move_Right", rate = 4, loop = 3  },
   { action_name = "Move_Front", rate = 45, loop = 3  },
   { action_name = "Assault", rate = 34, loop = 1  },
}
g_Lua_Near7 = { 
   { action_name = "stand_1", rate = 9, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 3  },
   { action_name = "Walk_Right", rate = 2, loop = 3  },
   { action_name = "Walk_Front", rate = 15, loop = 3  },
   { action_name = "Move_Left", rate = 5, loop = 3  },
   { action_name = "Move_Right", rate = 5, loop = 3  },
   { action_name = "Move_Front", rate = 45, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Upper", rate = 5, loop = 1 },
   { action_name = "Attack1_Slash", rate = 3, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 20000,  cooltime = 5000, rate = 56,rangemin = 0, rangemax = 800, target = 3, selfhppercent = 100, target_condition = "State1" },
   { skill_index = 20002,  cooltime = 10000, rate = 35, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 50, target_condition = "State1" },
}
