-- Manticore_Lord Hard A.I
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 500.0;
g_Lua_NearValue2 = 1000.0;
g_Lua_NearValue3 = 1500.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 500;
g_Lua_AssualtTime = 3000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}


g_Lua_CustomAction =
{
     CustomAction1 = 
     {
          { "Attack11_LHook_Lord", 0},
          { "Walk_Back", 0},
          { "Attack06_GravityThorn", 0}, 		
     },
     CustomAction2 = 
     {
          { "Attack11_RHook_Lord", 0},
          { "Walk_Back", 0},
          { "Attack06_GravityThorn", 0}, 
     },
     CustomAction3 = 
     {
          { "Fly", 0},
          { "Fly", 0},
          { "Attack07_HeavySwoop_DarkLair50", 0},
     },
}

g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,RF" },
     { action_name = "Walk_Left", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Back", rate = 3, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "CustomAction1", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", selfhppercentrange = "60,100" },
     { action_name = "CustomAction2", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", selfhppercentrange = "60,100" },
     { action_name = "Attack11_LHook_Lord", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR", selfhppercentrange = "5,60" },
     { action_name = "Attack11_RHook_Lord", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,RF", selfhppercentrange = "5,60" },
     { action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
     { action_name = "Attack04_Tail_L_Lord", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "LF,LB,BL,BR", selfhppercentrange = "5,100", cooltime = 20000 },
     { action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
     { action_name = "Attack05_Tail_R_Lord", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "RF,RB,BL,BR", selfhppercentrange = "5,100", cooltime = 20000 },
     { action_name = "CustomAction3", rate = 30, loop = 1, custom_state1 = "custom_fly", selfhppercent = 50 },
}

g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR" },
     { action_name = "Walk_Left", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Front", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Back", rate = 3, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "CustomAction1", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR", selfhppercentrange = "60,100" },
     { action_name = "CustomAction2", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,RF", selfhppercentrange = "60,100" },
     { action_name = "Attack11_LHook_Lord", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR", selfhppercentrange = "5,60" },
     { action_name = "Attack11_RHook_Lord", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,RF", selfhppercentrange = "5,60" },
     { action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
     { action_name = "Attack04_Tail_L_Lord", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "LF,LB,BL,BR", selfhppercentrange = "5,100", cooltime = 20000 },
     { action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
     { action_name = "Attack05_Tail_R_Lord", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "RF,RB,BL,BR", selfhppercentrange = "5,100", cooltime = 20000 },
     { action_name = "Attack06_GravityThorn",rate = 20, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", selfhppercentrange = "5,60" },
     { action_name = "CustomAction3", rate = 30, loop = 1, custom_state1 = "custom_fly", selfhppercent = 50 },
}

g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_ground", td = "FL,FR" },
     { action_name = "Walk_Left", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Front", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Back", rate = 3, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
     { action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
     { action_name = "Attack06_GravityThorn",rate = 20, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", selfhppercentrange = "5,40" },
     { action_name = "Attack09_HeavyJump", rate = 60, loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", randomtarget=1.1, cooltime = 30000 },
     { action_name = "CustomAction3", rate = 30, loop = 1, custom_state1 = "custom_fly", selfhppercent = 50 },
}

g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1, custom_state1 = "custom_ground", td = "FL,FR" },
     { action_name = "Walk_Left", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right", rate = 2, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Front", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Back", rate = 3, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
     { action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
     { action_name = "Attack09_HeavyJump", rate = 70, loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", randomtarget=1.1, cooltime = 30000 },
     { action_name = "CustomAction3", rate = 30, loop = 1, custom_state1 = "custom_fly", selfhppercent = 50 },
}

g_Lua_Near5 = 
{ 
     { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_ground", td = "FL,FR" },
     { action_name = "Walk_Left", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right", rate = 5, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Walk_Front", rate = 10, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
     { action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
     { action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
     { action_name = "CustomAction3", rate = 30, loop = 1, custom_state1 = "custom_fly", selfhppercent = 50 },
}

g_Lua_Skill = { 
-- 빅 그라비티볼
   { skill_index = 30012, cooltime = 25000, rate = 70, rangemin = 600, rangemax = 2000, target = 3, custom_state1 = "custom_ground", td = "FL,FR" },
-- 지상 회오리
   { skill_index = 30017, cooltime = 40000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, custom_state1 = "custom_ground", selfhppercent = 75 },
-- 비상
   { skill_index = 30018, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, custom_state1 = "custom_ground", selfhppercent = 50, limitcount = 1 },
-- 충격파
   { skill_index = 30011, cooltime = 40000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, custom_state1 = "custom_ground", selfhppercent = 25, limitcount = 1 },

}
