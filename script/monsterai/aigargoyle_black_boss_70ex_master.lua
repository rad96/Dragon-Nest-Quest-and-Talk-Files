--AiGargoyle_Black_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 2, loop = 1 },
   { action_name = "Attack1_DashAttack_N", rate = 3, loop = 1, globalcooltime = 3 },
   { action_name = "Attack3_Combo_N", rate = 10, loop = 1, globalcooltime = 4 },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 2, loop = 21 },
   { action_name = "Move_Left", rate = 4, loop = 1  },
   { action_name = "Move_Right", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 14, loop = 1  },
   { action_name = "Attack1_DashAttack", rate = 17, loop = 1, globalcooltime = 3 },
   { action_name = "Attack3_Combo", rate = 10, loop = 1, globalcooltime = 4 },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 25, loop = 2 },
}

g_Lua_GlobalCoolTime1 = 38000
g_Lua_GlobalCoolTime2 = 33000
g_Lua_GlobalCoolTime3 = 22000
g_Lua_GlobalCoolTime4 = 22000
g_Lua_GlobalCoolTime5 = 25000

g_Lua_Skill = { 
   { skill_index = 35105,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 1500, target = 3 },
   { skill_index = 35110,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 2 },
   { skill_index = 35106,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 3 },
   { skill_index = 35107,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 35115,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,75", next_lua_skill_index = 1, globalcooltime = 5, limitcount = 1 },
   { skill_index = 35115,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,50", next_lua_skill_index = 1, globalcooltime = 5, limitcount = 1 },
   { skill_index = 35115,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,25", next_lua_skill_index = 1, globalcooltime = 5, limitcount = 1 },
   { skill_index = 35101,  cooltime = 50000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, td = "FR,FL", randomtarget = "1.6,0,1" },
   { skill_index = 35102,  cooltime = 43000, rate = 50, rangemin = 500, rangemax = 1500, target = 3, globalcooltime = 1 },
   { skill_index = 35103,  cooltime = 43000, rate = 50, rangemin = 0, rangemax = 1000, target = 3, globalcooltime = 1 },
   { skill_index = 35104,  cooltime = 37000, rate = 70, rangemin = 0, rangemax = 500, target = 3, next_lua_skill_index = 0 },
   { skill_index = 35109,  cooltime = 75000, rate = 30, rangemin = 200, rangemax = 800, target = 3, selfhppercentrange = "0,75", td = "FR,FL,LF,RF" },
   { skill_index = 35113,  cooltime = 37000, rate = 50, rangemin = 0, rangemax = 5000, target = 1, globalcooltime = 2 },
   { skill_index = 35114,  cooltime = 37000, rate = 60, rangemin = 0, rangemax = 5000, target = 1, globalcooltime = 2 },
}
