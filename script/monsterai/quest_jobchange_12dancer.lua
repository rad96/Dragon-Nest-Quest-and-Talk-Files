
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1100;
g_Lua_NearValue4 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;


g_Lua_CustomAction = {
-- 기본 공격 2연타 145프레임
  CustomAction1 = {
     { "Attack1_Chakram" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction2 = {
     { "Attack1_Chakram" },
     { "Attack2_Chakram" },
  },
-- 기본 공격 4연타 313프레임
  CustomAction3 = {
     { "Attack1_Chakram" },
     { "Attack2_Chakram" },
     { "Attack3_Chakram" },
  },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 6000
g_Lua_GlobalCoolTime3 = 25000

g_Lua_Near1 = 
{
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 6, loop = 1 },
     { action_name = "Move_Right", rate = 6, loop = 1 },
     { action_name = "Move_Back", rate = 2, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Back", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 3 },
     { action_name = "Move_Left", rate = 1, loop = 1 },
     { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 4 },
}

g_Lua_Skill = { 
-- 소울 윈드 Skill_SoulWind
   { skill_index = 31181, cooltime = 8000, rate = 70, rangemin = 300, rangemax = 1000, target = 3, globalcooltime = 2 },
-- 스팅 브리즈 Skill_StingBreeze
   { skill_index = 31182,  cooltime = 10000, rate = 20, rangemin = 0, rangemax = 200, target = 3, globalcooltime = 2 },
-- 디스페어 니들 Skill_DespairNeedle
   { skill_index = 31184,  cooltime = 15000, rate = 40, rangemin = 100, rangemax = 300, target = 3, globalcooltime = 2 , encountertime = 5000 },
-- 팬시 턴   Skill_FancyTurn
   { skill_index = 31183,  cooltime = 15000, rate = 40, rangemin = 0, rangemax = 300, target = 3, globalcooltime = 2 },

-- 스위트 서클  Skill_SweetCircle
   -- { skill_index = 31190, cooltime = 15000, rate = 60, rangemin = 0, rangemax = 300, target = 3, globalcooltime = 2 },
-- 더스크 헌터 Skill_DuskHunter
   { skill_index = 31191, cooltime = 14000, rate = 40, rangemin = 0, rangemax = 450, target = 3, globalcooltime = 2, encountertime = 5000  },
-- 제네럴 던블레이드 Skill_GeneralDawnBlade
   { skill_index = 31194, cooltime = 14000, rate = 40, rangemin = 0, rangemax = 600, target = 3, selfhppercent = 50, globalcooltime = 2 },
-- 라프레싱 스크류  Skill_RefreshingScrew
   { skill_index = 31192, cooltime = 31000, rate = 60, rangemin = 0, rangemax = 800, target = 3, globalcooltime = 2 , encountertime = 5000  },
-- 엘레강스 스톰 Skill_EleganceStorm
   { skill_index = 31193, cooltime = 27000, rate = 60, rangemin = 0, rangemax = 450, target = 3, selfhppercent = 50, globalcooltime = 2 },
-- 엑스테틱 댄스 Skill_EcstaticDance
    { skill_index = 31195, next_lua_skill_index = 9, cooltime = 30000, rate = 60, rangemin = 300, rangemax = 600, target = 3, globalcooltime = 2, limitcount = 2 },
--엑스테틱 댄스 소환 Skill_EcstaticDanceP1
    { skill_index = 31196, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1600, target = 3 },
	}
