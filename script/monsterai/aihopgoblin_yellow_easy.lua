--AiHopGoblin_Yellow_Easy.lua

g_Lua_NearTableCount = 7

g_Lua_NearValue1 = 0;
g_Lua_NearValue2 = 150;
g_Lua_NearValue3 = 300;
g_Lua_NearValue4 = 550;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;
g_Lua_NearValue7 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Walk_Back", 0 },
      { "Attack7_GoblinNanmu", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Stand_1", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Stand2", rate = -1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Bite", rate = -1, loop = 1  },
   { action_name = "Attack2_Nanmu", rate = 3, loop = 1  },
   { action_name = "Attack7_GoblinNanmu", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Stand2", rate = -1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack1_Bite", rate = -1, loop = 1  },
   { action_name = "Attack2_Nanmu", rate = 2, loop = 1  },
   { action_name = "Attack7_GoblinNanmu", rate = 2, loop = 1  },
   { action_name = "CustomAction1", rate = -1, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Stand2", rate = -1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
   { action_name = "Attack7_GoblinNanmu", rate = 3, loop = 1  },
   { action_name = "Attack8_GoblinThrowSton", rate = 3, loop = 1  },
   { action_name = "CustomAction1", rate = -1, loop = 1  },
   { action_name = "Assault", rate = 1, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Stand2", rate = -1, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Attack8_GoblinThrowSton", rate = 4, loop = 1  },
   { action_name = "Assault", rate = 2, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near7 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 3, loop = 1  },
   { action_name = "Attack8_GoblinThrowSton", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Bite", rate = -1, loop = 1, approach = 100.0  },
   { action_name = "Attack7_GoblinNanmu", rate = 3, loop = 1, approach = 400.0  },
   { action_name = "Attack2_Nanmu", rate = 8, loop = 1, approach = 150.0  },
}
