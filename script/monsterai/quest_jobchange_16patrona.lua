
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1100;
g_Lua_NearValue4 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;


g_Lua_CustomAction = {
-- 기본 공격 2연타 145프레임
  CustomAction1 = {
     { "Attack1_Knucklegear" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction2 = {
     { "Attack1_Knucklegear" },
     { "Attack3_Knucklegear" },
  },
-- 기본 공격 4연타 313프레임
  CustomAction3 = {
     { "Attack1_Knucklegear" },
     { "Attack3_Knucklegear" },
     { "Attack4_Knucklegear" },
  },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 6000
g_Lua_GlobalCoolTime3 = 25000

g_Lua_Near1 = 
{
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 6, loop = 1 },
     { action_name = "Move_Right", rate = 6, loop = 1 },
     { action_name = "Move_Back", rate = 2, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Back", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 3 },
     { action_name = "Move_Left", rate = 1, loop = 1 },
     { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 4 },
}

g_Lua_Skill = { 
-- 0로우 릴 훅  Skill_HalfCutting
   { skill_index = 31241, cooltime = 8000, rate = 60, rangemin = 0, rangemax = 600, target = 3  },
-- 1피봇 건 샷 Skill_FlashLift
   { skill_index = 31242,  cooltime = 10000, rate = 60, rangemin = 0, rangemax = 200, target = 3 },
-- 2바운딩 리액션 Skill_DeepPierce
   { skill_index = 31243,  cooltime = 15000, rate = 60, rangemin = 0, rangemax = 300, target = 3 },
-- 3엘보 어택 Skill_CrossCutter
   { skill_index = 31244, cooltime = 15000, rate = 60, rangemin = 0, rangemax = 300, target = 3 },
-- 4풋 스톰핑 Skill_ShutterBounce
   { skill_index = 31251, cooltime = 10000, rate = 60, rangemin = 0, rangemax = 400, target = 3, combo1 = "5,100,1"  },
-- 5플로 업 Skill_HoveringBlade
   { skill_index = 31252, cooltime = 10000, rate = 30, rangemin = 0, rangemax = 400, target = 3 },
-- 6스텝 업 Skill_StabScrew
   { skill_index = 31253, cooltime = 10000, rate = 60, rangemin = 0, rangemax = 500, target = 3, combo1 = "7,100,1"},
-- 7리빙 오버 Skill_FlingFling
   { skill_index = 31254, cooltime = 19000, rate = 30, rangemin = 0, rangemax = 800, target = 3 },
-- 8록 어웨이 Skill_FlingFling
   { skill_index = 31255, cooltime = 10000, rate = 60, rangemin = 0, rangemax = 400, target = 3 , combo1 = "9,100,1" },
-- 9플라잉 니킥 Skill_FlingFling
   { skill_index = 31256, cooltime = 15000, rate = 30, rangemin = 0, rangemax = 800, target = 3 },   
}