--AiHoundArmor_General_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_ReturnUnitArea = 2000
g_Lua_ReturnTime = 6000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 25, loop = 1  },
   { action_name = "Provoke", rate = 5, loop = 1  },
   { action_name = "Attack2_BigBite", rate = 6, loop = 1  },
   { action_name = "Provoke", rate = 30, loop = 1, target_condition = "State1"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 35, loop = 1  },
   { action_name = "Provoke", rate = 10, loop = 1  },
   { action_name = "Attack4_Headbutt", rate = 10, loop = 1, max_missradian = 30 },
   { action_name = "Attack2_BigBite", rate = 2, loop = 1  },
   { action_name = "Attack6_JumpBite", rate = 6, loop = 1  },
   { action_name = "Provoke", rate = 50, loop = 1, target_condition = "State1"  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Attack1_BiteCombo", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Assault", rate = 4, loop = 1  },
   { action_name = "Attack6_JumpBite", rate = 2, loop = 1  },
   { action_name = "Attack4_Headbutt", rate = 4, loop = 1, max_missradian = 30 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Attack1_BiteCombo", rate = 15, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
   { action_name = "Assault", rate = 9, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Attack1_BiteCombo", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_BigBite", rate = 5, loop = 1, cancellook = 1, approach = 250.0  },
   { action_name = "Attack4_Headbutt", rate = 5, loop = 1, cancellook = 1, approach = 300.0, max_missradian = 30 },
}
g_Lua_Skill = { 
	{ skill_index = 400003, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, returneffect =1 },--소
	-- { skill_index = 400004, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--중
	-- { skill_index = 400005, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--대
	-- { skill_index = 400006, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--특대
}