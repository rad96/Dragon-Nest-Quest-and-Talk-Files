--AiHopGoblin_Black_Abyss.lua
--/genmon 901412
g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 0;
g_Lua_NearValue2 = 150;
g_Lua_NearValue3 = 300;
g_Lua_NearValue4 = 550;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_ReturnUnitArea = 4000
g_Lua_ReturnTime = 10000

g_Lua_DestinationCount = 3
g_Lua_Destination1 = 4372
g_Lua_Destination2 = 4373
g_Lua_Destination3 = 4374

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 20000
g_Lua_GlobalCoolTime3 = 40000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Stand_1", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Bite", rate = 9, loop = 1  },
   { action_name = "Attack2_Nanmu_Black", rate = 15, loop = 1, globalcooltime=1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Stand2", rate = -1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 6, loop = 1  },
   { action_name = "Move_Right", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 6, loop = 1  },
   { action_name = "Attack1_Bite", rate = 7, loop = 1  },
   { action_name = "Attack2_Nanmu_Black", rate = 13, loop = 1, globalcooltime=1   },
   { action_name = "Attack3_Upper_3", rate = 13, loop = 1, globalcooltime=3  },
   { action_name = "CustomAction1", rate = 8, loop = 1,globalcooltime=4  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
   { action_name = "Stand2", rate = -1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
   { action_name = "Attack3_Upper_3", rate = 15, loop = 1 , globalcooltime=3  },
   { action_name = "CustomAction1", rate = 10, loop = 1 ,globalcooltime=3 },
   { action_name = "Assault", rate = 5, loop = 1 ,globalcooltime=2 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Attack3_Upper_3", rate = 15, loop = 1 , globalcooltime=3  },
   { action_name = "CustomAction1", rate = 15, loop = 1,globalcooltime=3  },
   { action_name = "Assault", rate = 4, loop = 1 ,globalcooltime=2 },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
   { action_name = "Assault", rate = 10, loop = 1 ,globalcooltime=2 },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 9, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Bite", rate = 5, loop = 1, approach = 100  },
   { action_name = "Attack2_Nanmu_Black", rate = 10, loop = 1, approach = 400  },
}
g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack3_Upper_3", 0 },
      { "Attack3_Upper_4", 0 },
  },
}
g_Lua_Skill = { 
   { skill_index = 20230,  cooltime = 60000, rate = 100,rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 30 },
	-- { skill_index = 400003, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--소
	-- { skill_index = 400004, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--중
	{ skill_index = 400006, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, returneffect =1 },--대
	-- { skill_index = 400006, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--특대
}
