--AiKoboldWarrior_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 50;
g_Lua_NearValue2 = 150;
g_Lua_NearValue3 = 200;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_ReturnUnitArea = 2000
g_Lua_ReturnTime = 6000


g_Lua_CustomAction = {
  CustomAction1 = {
      { "Move_Left", 2 },
      { "Attack2_Sever", 1 },
  },
  CustomAction2 = {
      { "Move_Right", 2 },
      { "Attack2_Sever", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 2  },
   { action_name = "CustomAction1", rate = 3, loop = 1,  },
   { action_name = "CustomAction2", rate = 3, loop = 1,  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 8, loop = 2  },
   { action_name = "Move_Back", rate = 8, loop = 1  },
   { action_name = "Move_Left", rate = 8, loop = 1  },
   { action_name = "Move_Right", rate = 8, loop = 1  },
   { action_name = "Attack1_Chop", rate = 11, loop = 1  },
   { action_name = "Attack2_Sever", rate = 9, loop = 1  },
   { action_name = "CustomAction1", rate = 50, loop = 1,  },
   { action_name = "CustomAction2", rate = 50, loop = 1,  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack2_Sever", rate = 9, loop = 1  },
   { action_name = "CustomAction1", rate = 35, loop = 1,  },
   { action_name = "CustomAction2", rate = 35, loop = 1,  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 12, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack7_ThrowBomb", rate = 4, loop = 1,  approach = 800  },
}
g_Lua_Skill = { 
   -- { skill_index = 20033,  cooltime = 5000, rate = 100,rangemin = 100, rangemax = 1000, target = 3, },
	{ skill_index = 400003, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, returneffect =1 },--소
	-- { skill_index = 400004, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--중
	-- { skill_index = 400005, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--대
	-- { skill_index = 400006, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--특대
}
