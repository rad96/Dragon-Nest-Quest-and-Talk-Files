--AiHarpy_Red_Normal.lua
--/genmon 901413
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_ReturnUnitArea = 4000
g_Lua_ReturnTime = 10000

g_Lua_DestinationCount = 3
g_Lua_Destination1 = 4369
g_Lua_Destination2 = 4370
g_Lua_Destination3 = 4371

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 15, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 8, loop = 1  }, 
   { action_name = "Attack1_Throw_R", rate = 6, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 15, loop = 1, globalcooltime=1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 12, loop = 3  },
   { action_name = "Walk_Right", rate = 12, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 3  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack1_Throw_R", rate = 12, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 8, loop = 1, globalcooltime=1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 15, loop = 3  },
   { action_name = "Walk_Right", rate = 15, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
   { action_name = "Attack1_Throw_R", rate = 24, loop = 1,  },
}
g_Lua_Near4= { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Walk_Left", rate = 10, loop = 3  },
   { action_name = "Walk_Right", rate = 10, loop = 3  },
   { action_name = "Attack1_Throw_R", rate = 20, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 400026,  cooltime = 25000, rate = 80,rangemin = 300, rangemax = 1000, target = 3 },
	-- { skill_index = 400003, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--소
	-- { skill_index = 400004, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--중
	{ skill_index = 400006, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, returneffect =1 },--대
	-- { skill_index = 400006, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--특대
}
