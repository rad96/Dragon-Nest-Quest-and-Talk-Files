-- Goblin Grey Normal AI

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 100.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 600.0;
g_Lua_NearValue4 = 1000.0;
g_Lua_NearValue5 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 120;
g_Lua_AssualtTime = 5000;

g_Lua_ReturnUnitArea = 2000
g_Lua_ReturnTime = 6000

g_Lua_GlobalCoolTime1 = 10000

g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Back",	rate = 15,		loop = 2 },
	{ action_name = "Move_Back",	rate = 15,		loop = 2 },
	{ action_name = "Attack1_Slash",	rate = 4,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand2",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 7,		loop = 2 },
	{ action_name = "Move_Back",	rate = 5,		loop = 2 },
	{ action_name = "Attack1_Slash",	rate = 3,		loop = 1 },
	{ action_name = "Attack2_Boom_Range",	rate = 5,		loop = 1, globalcooltime = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand2",	rate = 7,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 7,		loop = 2 },
	{ action_name = "Move_Left",	rate = 1,		loop = 1 },
	{ action_name = "Move_Right",	rate = 1,		loop = 1 },
    { action_name = "Move_Front",	rate = 3,		loop = 1 },
	{ action_name = "Attack2_Boom_Range",	rate = 5,		loop = 1, globalcooltime = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand2",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 7,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 7,		loop = 2 },
	{ action_name = "Move_Left",	rate = 1,		loop = 2 },
	{ action_name = "Move_Right",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 3,		loop = 2 },
	{ action_name = "Move_Back",	rate = 1,		loop = 2 },
	{ action_name = "Attack2_Boom_Range",	rate = 5,		loop = 1, globalcooltime = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand2",	rate = 12,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 12,   loop = 2 },
	{ action_name = "Move_Left",	rate = 1,		loop = 2 },
	{ action_name = "Move_Right",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 5,		loop = 2 },
	{ action_name = "Attack2_Boom_Range",	rate = 5,		loop = 1, globalcooltime = 1 },
}

g_Lua_Assault =
{         { action_name = "Attack1_Slash",	rate = 3,	loop = 1, cancellook = 1, approach = 100.0 },}

g_Lua_Skill = { 
	{ skill_index = 400003, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, returneffect =1 },--소
	-- { skill_index = 400004, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--중
	-- { skill_index = 400005, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--대
	-- { skill_index = 400006, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--특대
}
       