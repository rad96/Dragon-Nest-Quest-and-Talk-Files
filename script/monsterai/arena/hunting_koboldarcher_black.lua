--AiKoboldArcher_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_ReturnUnitArea = 2000
g_Lua_ReturnTime = 6000

g_Lua_GlobalCoolTime1 = 15000

g_Lua_Near1 = { 
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 25, loop = 2  },
   { action_name = "Attack1", rate = 5, loop = 1  },
   { action_name = "Attack1_Miss1", rate = 5, loop = 1  },
   { action_name = "Attack2_MultiShot", rate = 13, loop = 1 ,globalcooltime =1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 2  },
   { action_name = "Move_Right", rate = 8, loop = 2  },
   { action_name = "Move_Back", rate = 20, loop = 2  },
   { action_name = "Attack1", rate = 7, loop = 1  },
   { action_name = "Attack1_Miss1", rate = 7, loop = 1  },
   { action_name = "Attack2_MultiShot", rate = 10, loop = 1  },
   { action_name = "Attack3_ArrowShower", rate = 10, loop = 1 ,globalcooltime =1},
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 30, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 11, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack1", rate = 5, loop = 1  },
   { action_name = "Attack1_Miss2", rate = 5, loop = 1  },
   { action_name = "Attack3_ArrowShower", rate = 22, loop = 1 ,globalcooltime =1},
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 30, loop = 1  },
   { action_name = "Attack3_ArrowShower", rate = 10, loop = 1 ,globalcooltime =1 },
}
g_Lua_Skill = { 
	{ skill_index = 400003, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, returneffect =1 },--소
	-- { skill_index = 400004, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--중
	-- { skill_index = 400005, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--대
	-- { skill_index = 400006, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--특대
}
