--AiWraith_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_ReturnUnitArea = 2000
g_Lua_ReturnTime = 6000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 8, loop = 1  },
   { action_name = "Attack1_Cut", rate = 9, loop = 1  },
   { action_name = "Attack4_BackAttack_Mini", rate = 7, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 12, loop = 2  },
   { action_name = "Walk_Right", rate = 12, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 8, loop = 1  },
   { action_name = "Move_Right", rate = 8, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 1, loop = 1  },
   { action_name = "Attack5_DashAttack", rate = 13, loop = 1  },
   { action_name = "Attack3_ThrowScythe", rate = 9, loop = 1  },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Attack3_ThrowScythe", rate = 18, loop = 1  },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 22, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Cut", rate = 1, loop = 1, approach = 200.0  },
   { action_name = "Attack4_BackAttack_Mini", rate = 10, loop = 1, approach = 200.0  },
}
g_Lua_Skill = { 
	-- { skill_index = 400003, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--소
	-- { skill_index = 400004, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--중
	{ skill_index = 400005, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, returneffect =1 },--대
	-- { skill_index = 400006, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, returneffect =1 },--특대
}
