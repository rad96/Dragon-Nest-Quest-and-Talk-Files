--/genmon 901725
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 20, loop = 1 },
	{ action_name = "Walk_Left", rate = 5, loop = 1 },
	{ action_name = "Walk_Right", rate = 5, loop = 1 },
	{ action_name = "Walk_Back", rate = 5, loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 23, loop = 1 },	
	{ action_name = "Walk_Front", rate = 8, loop = 1 },
	{ action_name = "Walk_Left", rate = 6, loop = 1 },
	{ action_name = "Walk_Right", rate = 6, loop = 1 },	
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 30, loop = 1 },	
   	{ action_name = "Walk_Front", rate = 13, loop = 1 },
    { action_name = "Walk_Left", rate = 5, loop = 1 },
	{ action_name = "Walk_Right", rate = 5, loop = 1 },
	{ action_name = "Assault", rate = 10, loop = 1, cooltime = 10000},	
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 8, loop = 1 },	
	{ action_name = "Move_Front", rate = 15, loop = 1, },
	{ action_name = "Assault", rate = 40, loop = 1, },
}

g_Lua_RandomSkill =
{{
	{{ skill_index = 36816, rate = 50, next_lua_skill_index = 3 },{ skill_index = 36817, rate = 50, next_lua_skill_index = 2 },},
	{{ skill_index = 36809, rate = 50, },{ skill_index = 36818, rate = 50, },},
}}

g_Lua_GlobalCoolTime1 = 20000
g_Lua_GlobalCoolTime2 = 30000 --예비

g_Lua_Assault = { { lua_skill_index = 3, rate = 10, approach = 200},} --Attack4_Teleport (0 부터 시작)
--globalcooltime=1, priority = 1, 
g_Lua_Skill = { 
	 { skill_index = 36809, cooltime = 50000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, },	--Attack2_EarthQuake_Ground 전기
	 --{ skill_index = 36810, cooltime = 3500, rate = 50, rangemin = 0, rangemax = 2000, target = 3, },	--Attack2_EarthQuake_Range 빙결
	 --{ skill_index = 36811, cooltime = 3000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 2000, target = 3, },	--Attack2_EarthQuake_Shot 화염
     --{ skill_index = 36812, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, },	--Attack3_Buff 파이어볼
	 --{ skill_index = 36813, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, },	--Attack4_Teleport
	 --{ skill_index = 36815, cooltime = 10000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 2000, target = 3, },   --Attack6_CrazyShot
	 { skill_index = 36816, cooltime = 500, rate = 100, rangemin = 0, rangemax = 350, target = 3, skill_random_index = 2},   --Attack1_Punchi1
	 { skill_index = 36817, cooltime = 500, rate = 100, rangemin = 0, rangemax = 350, target = 3, skill_random_index = 1},   --Attack1_Punchi2
	 { skill_index = 36818, cooltime = 500, rate = 100, rangemin = 0, rangemax = 350, target = 3, skill_random_index = 1},   --Attack1_Punchi3
}
