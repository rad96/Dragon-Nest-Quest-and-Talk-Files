--AiBoarGunner_Red_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 10000

g_Lua_GlobalCoolTime1 = 30000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop=1 },
   { action_name = "Walk_Back", rate = 7, loop=1 },
   { action_name = "Move_Back", rate = 7, loop=1 },
   { action_name = "Attack06_ShieldAttack", rate = 13, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 7, loop=1 },
   { action_name = "Walk_Back", rate = 10, loop=2 },
   { action_name = "Walk_Left", rate = 7, loop=2 },
   { action_name = "Walk_Right", rate = 7, loop=2 },
   { action_name = "Attack01_Shot", rate = 28, loop=1,max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 15, loop=1 },
   { action_name = "Walk_Front", rate = 9, loop=2 },
   { action_name = "Walk_Left", rate = 7, loop=2 },
   { action_name = "Walk_Right", rate = 7, loop=2 },
   { action_name = "Move_Front", rate = 13, loop=1 },
   { action_name = "Attack02_DoubleShot", rate = 42, loop=1,max_missradian = 30 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 7, loop=1 },
   { action_name = "Walk_Front", rate = 9, loop=3 },
   { action_name = "Move_Front", rate = 17, loop=2 },
}
g_Lua_Skill = { 
   { skill_index = 33685,  cooltime = 43000, rate = 40,rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,50",next_lua_skill_index=1,globalcooltime=1 },
   { skill_index = 33681,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1300, target = 3 },
   { skill_index = 33684,  cooltime = 25000, rate = 50, rangemin = 300, rangemax = 800, target = 3,checkweapon = "2", },
}
