
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1100;
g_Lua_NearValue4 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;


g_Lua_CustomAction = {
-- 기본 공격 2연타 145프레임
  CustomAction1 = {
     { "Attack1_Spear" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction2 = {
     { "Attack1_Spear" },
     { "Attack2_Spear" },
  },
-- 기본 공격 4연타 313프레임
  CustomAction3 = {
     { "Attack1_Spear" },
     { "Attack2_Spear" },
     { "Attack3_Spear" },
  },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 6000
g_Lua_GlobalCoolTime3 = 25000

g_Lua_Near1 = 
{
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 6, loop = 1 },
     { action_name = "Move_Right", rate = 6, loop = 1 },
     { action_name = "Move_Back", rate = 2, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Back", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 3 },
     { action_name = "Move_Left", rate = 1, loop = 1 },
     { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 4 },
}

g_Lua_Skill = { 
-- 하프 컷팅 Skill_HalfCutting
   { skill_index = 31221, cooltime = 8000, rate = 70, rangemin = 300, rangemax = 1000, target = 3, globalcooltime = 2 },
-- 플래시 리프트 Skill_FlashLift
   { skill_index = 31222,  cooltime = 10000, rate = 20, rangemin = 0, rangemax = 200, target = 3, globalcooltime = 2 },
-- 딥 피어스 Skill_DeepPierce
   { skill_index = 31223,  cooltime = 15000, rate = 40, rangemin = 100, rangemax = 300, target = 3, globalcooltime = 2 },
-- 크로스 커터 Skill_CrossCutter
   { skill_index = 31224, cooltime = 15000, rate = 60, rangemin = 0, rangemax = 450, target = 3, globalcooltime = 2 },
-- 셔터 바운스 Skill_ShutterBounce
   { skill_index = 31225, cooltime = 14000, rate = 40, rangemin = 0, rangemax = 400, target = 3, globalcooltime = 2 },
-- 호버링 블레이드 Skill_HoveringBlade
   { skill_index = 31226, cooltime = 14000, rate = 40, rangemin = 300, rangemax = 600, target = 3, globalcooltime = 2 },
-- 스탭 스크류 Skill_StabScrew
   { skill_index = 31227, cooltime = 21000, rate = 60, rangemin = 0, rangemax = 1000, target = 3, globalcooltime = 2 },
-- 플링플링 Skill_FlingFling
   { skill_index = 31228, cooltime = 19000, rate = 60, rangemin = 600, rangemax = 800, target = 3, globalcooltime = 2 },	  
}