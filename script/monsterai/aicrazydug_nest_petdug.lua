g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 500.0;
g_Lua_NearValue2 = 2000.0;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 1500; --몬스터가 돌격시에 타겟에게 다가가는 최소거리
g_Lua_AssaultTime = 3000; --Assault 명령을 받고 돌격 할 때 타겟을 쫓아가는 시간. 

g_Lua_Near1 = { 
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Attack1_Bite", rate = 80, loop = 1  }, 
   { action_name = "Assault", rate = 80, loop = 1  }, 
}

g_Lua_Near2 = { 
   { action_name = "Assault", rate = 80, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
}

g_Lua_Assault = { { lua_skill_index = 0, rate = 10, approach = 1500},} --Attack01_RedDNest_Bash

g_Lua_Skill = --머리 커지게 하는 스킬
{ 
   { skill_index = 36800,  cooltime = 4000, rate = 1, rangemin = 0, rangemax = 1500, target = 3 }, 
}