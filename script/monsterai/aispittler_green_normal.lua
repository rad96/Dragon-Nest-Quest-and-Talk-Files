--AiSpittler_Green_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 20, loop = 1, custom_state1 = "custom_ground",  },
   { action_name = "Attack2_Bite", rate = 5, loop = 1, custom_state1 = "custom_ground",  },
   { action_name = "Stand_On", rate = 10, loop = 1, custom_state1 = "custom_underground",  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, custom_state1 = "custom_ground",  },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground",  },
   { action_name = "Stand_On", rate = -1, loop = 1, custom_state1 = "custom_underground",  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, custom_state1 = "custom_ground",  },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground",  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground",  },
}
g_Lua_Skill = { 
   { skill_index = 20251,  cooltime = 15000, rate = -1,rangemin = 300, rangemax = 2000, target = 3, custom_state1 = "custom_ground" },
   { skill_index = 20250,  cooltime = 15000, rate = -1, rangemin = 200, rangemax = 1500, target = 3, custom_state1 = "custom_ground" },
   { skill_index = 20250,  cooltime = 30000, rate = 50, rangemin = 200, rangemax = 1500, target = 3, custom_state1 = "custom_ground" },
   --{ skill_index = 20253,  cooltime = 10000, rate = 100, rangemin = 1000, rangemax = 2000, target = 3, custom_state1 = "custom_ground" },
   --{ skill_index = 20254,  cooltime = 5000, rate = 100, rangemin = 0, rangemax = 15000, target = 3, custom_state1 = "custom_underground" },
}
