--AiTroll_Black_Elite_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Claw", rate = 9, loop = 1  },
   { action_name = "Attack6_Bite", rate = 6, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
   { action_name = "Assault", rate = 8, loop = 1  },
   
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 7, loop = 3  },
   { action_name = "Move_Right", rate = 7, loop = 3  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
   { action_name = "Move_Back", rate = 3, loop = 3  },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = -1, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 3  },
   { action_name = "Move_Right", rate = 5, loop = 3  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
   { action_name = "Move_Back", rate = 1, loop = 3  },
}
g_Lua_NonDownRangeDamage = { 
   { lua_skill_index = 0, rate = 40   },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 30, loop = 1, approach = 200.0  },
   { action_name = "Attack6_Bite", rate = 20, loop = 1, approach = 200.0  },
}
g_Lua_BeHitSkill =
	{
		{ lua_skill_index = 1, rate = 100, skill_index = 33341 },
	}
g_Lua_Skill = { 
   { skill_index = 33341, cooltime = 15000, rate = 150, rangemin = 500, rangemax = 1500, target = 3, selfhppercent = 80},
     {skill_index = 33342, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3 },
}