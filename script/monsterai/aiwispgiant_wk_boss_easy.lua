--AiWispGiant_WK_Boss_Easy.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 1 },
   { action_name = "Attack3_SummonWisp", rate = 4, loop = 1, cooltime = 60000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 2  },
   { action_name = "Move_Right", rate = 20, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 1 },
   { action_name = "Attack3_SummonWisp", rate = 40, loop = 1, cooltime = 60000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 20, loop = 2  },
   { action_name = "Walk_Right", rate = 20, loop = 2  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 2  },
   { action_name = "Move_Right", rate = 20, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
   { action_name = "Attack3_SummonWisp", rate = -1, loop = 1, cooltime = 60000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 3  },
   { action_name = "Move_Right", rate = 20, loop = 3  },
   { action_name = "Move_Front", rate = 50, loop = 3  },
   { action_name = "Attack3_SummonWisp", rate = -1, loop = 1, cooltime = 60000 },
}
g_Lua_Skill = { 
   { skill_index = 20283,  cooltime = 30000, rate = 30,rangemin = 000, rangemax = 2000, target = 1, notusedskill = "20283,20284" },
   { skill_index = 20284,  cooltime = 30000, rate = 100, rangemin = 000, rangemax = 2000, target = 1, notusedskill = "20283,20284" },
   { skill_index = 20290,  cooltime = 25000, rate = 80, rangemin = 000, rangemax = 1500, target = 3, usedskill = 20284 },
   { skill_index = 20291,  cooltime = 25000, rate = 80, rangemin = 000, rangemax = 500, target = 3, usedskill = 20284 },
   { skill_index = 20292,  cooltime = 25000, rate = 80, rangemin = 000, rangemax = 1500, target = 3, usedskill = 20283 },
   { skill_index = 20293,  cooltime = 25000, rate = 80, rangemin = 000, rangemax = 1000, target = 3, usedskill = 20283 },
}
