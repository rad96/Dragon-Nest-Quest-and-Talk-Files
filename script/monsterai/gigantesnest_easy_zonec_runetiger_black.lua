--RuneTiger_Black_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 800;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Back", rate = 12, loop = 1, td = "FL,FR" },
   { action_name = "Attack1_Claw", rate = 15, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Back", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 18, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 3, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 3, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 20, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 8, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Back", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 18, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 3, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 3, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 20, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 15, loop = 2, td = "FL,FR" },
   { action_name = "Move_Left", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Move_Right", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Move_Front", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 26, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 10, loop = 2, td = "FL,FR" },
   { action_name = "Move_Left", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Move_Right", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Move_Front", rate = 20, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 10, loop = 1, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 20721,  cooltime = 38000, rate = 100,rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 50, td = "FL,FR" },
   { skill_index = 20722,  cooltime = 12000, rate = 100, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100, td = "FL,FR" },
   { skill_index = 20723,  cooltime = 75000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 80, td = "FL,FR" },
}
