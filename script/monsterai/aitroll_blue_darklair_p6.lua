--AiTroll_Blue_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Attack3_RollingAttackS_N", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Attack3_RollingAttack_N", rate = 3, loop = 1 },
   { action_name = "Attack3_RollingAttackS_N", rate = 12, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Attack3_RollingAttack_N", rate = 10, loop = 1 },
   { action_name = "Attack3_RollingAttackS_N", rate = 10, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Attack3_RollingAttack", rate = 13, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack3_RollingAttack", rate = 9, loop = 1  },
}
