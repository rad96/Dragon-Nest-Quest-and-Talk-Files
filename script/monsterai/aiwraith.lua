-- Wraith AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 1500.0;
g_Lua_NearValue4 = 2000.0;

g_Lua_LookTargetNearState = 4;

g_Lua_WanderingDistance = 2000.0;

g_Lua_PatrolBaseTime = 5000;

g_Lua_PatrolRandTime = 3000;



g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 10,		loop = 1 },
	{ action_name = "Move_Right",	rate = 10,		loop = 1 },
	{ action_name = "Move_Back",	rate = 10,		loop = 1 },
	{ action_name = "Attack1",	rate = 30,		loop = 1 },
	{ action_name = "Attack4",	rate = 30,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 15,		loop = 1 },
	{ action_name = "Move_Right",	rate = 15,		loop = 1 },
	{ action_name = "Move_Front",	rate = 15,		loop = 1 },
	{ action_name = "Move_Back",	rate = 15,		loop = 1 },
	{ action_name = "Attack5_DashAttack",	rate = 20,		loop = 1 },
	{ action_name = "Assault",	rate = 5,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 10,		loop = 1 },
	{ action_name = "Move_Right",	rate = 10,		loop = 1 },
	{ action_name = "Move_Front",	rate = 10,		loop = 1 },
	{ action_name = "Move_Back",	rate = 10,		loop = 1 },
	{ action_name = "Attack3_ThrowScythe",	rate = 20,		loop = 1 },
	{ action_name = "Assault",	rate = 2,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Walk_Front",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 1,		loop = 2 },
}
