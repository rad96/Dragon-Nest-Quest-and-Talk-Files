--GreenDragonNest_FinalBoss_GreenDragon_2Phase.lua

g_Lua_NearTableCount = 2


g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 5000;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Chase_Stand", rate = 10, loop = 1, selfhppercent = 100  },
   { action_name = "Chase_Shout", rate = 10, loop = 1, selfhppercent = 100  },
--   { action_name = "Chase_Attack", rate = 10, loop = 1, selfhppercent = 100  },
   { action_name = "Chase_Run", rate = 100, loop = 1, selfhppercentrange = "0,100",  cooltime = 30000}
}

g_Lua_Near2 = { 
   { action_name = "Chase_Stand", rate = 10, loop = 1, selfhppercent = 100  },
   { action_name = "Chase_Shout", rate = 10, loop = 1, selfhppercent = 100  },
--   { action_name = "Chase_Attack", rate = 10, loop = 1, selfhppercent = 100  },
}

g_Lua_Skill = { 
-- 브레스로 다 죽이기
   { skill_index = 31531,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3},
}