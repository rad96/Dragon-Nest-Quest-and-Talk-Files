--AiMinotauros_Black_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 350;
g_Lua_NearValue3 = 500;
g_Lua_NearValue4 = 750;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
   { action_name = "Attack2_Slash_N", rate = 3, loop = 1  },
   { action_name = "Attack1_bash", rate = 6, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_bash", rate = 3, loop = 1  },
   { action_name = "Attack2_Slash_N", rate = 6, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 8, loop = 1  },
   { action_name = "Move_Front_NoShake", rate = 8, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front_NoShake", rate = 8, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front_NoShake", rate = 8, loop = 3  },
}
