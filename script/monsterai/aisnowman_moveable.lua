-- Goblin Blue Elite Normal AI
g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 500.0;
g_Lua_NearValue3 = 700.0;
g_Lua_NearValue4 = 1000.0;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 300;

g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 10,		loop = 5 },	
}
g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 10,		loop = 1 },	
	{ action_name = "Stand",	rate = 10,		loop = 4 },	
}
g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 10,		loop = 1 },	
}
g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 10,		loop = 2 },	
}
