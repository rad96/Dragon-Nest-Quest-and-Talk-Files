-- Slime AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 250.0;
g_Lua_NearValue3 = 350.0;
g_Lua_NearValue4 = 600.0;


g_Lua_NoAggroOwnerFollow=1
g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;



g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
        { action_name = "Attack_Punch_PvP", rate = 30,              loop = 1 },
        { action_name = "Attack_Punch_B_PvP", rate = 30,              loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
        { action_name = "Attack_Punch_PvP", rate = 30,              loop = 1 },
        { action_name = "Attack_Punch_B_PvP", rate = 30,              loop = 1 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
}



g_Lua_Skill = { 
	{ skill_index = 30810, cooltime = 11000, rangemin = 0, rangemax = 150, target = 3, rate = 100 },

    {skill_index=260801,cooltime=0,rate=100,rangemin=0,rangemax=1000,target=2,selfhppercent=100,waitorder=260801, priority=2 },--세크리파이스Lv1

}
