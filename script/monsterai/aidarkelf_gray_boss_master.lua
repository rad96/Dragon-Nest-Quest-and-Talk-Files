--AiDarkElf_Gray_Boss_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 25, loop = 1 },
   { action_name = "Attack1_Claw", rate = 14, loop = 1  },
   { action_name = "Attack2_Somersault", rate = 14, loop = 1  },
   { action_name = "Attack3_AerialCombo", rate = 14, loop = 1, cooltime = 10000 },
   { action_name = "Attack3_AerialCombo", rate = 300, loop = 1, target_condition = "State1", cooltime = 10000 },
   { action_name = "Attack4_SickleKick", rate = 300, loop = 1, target_condition = "State2", max_missradian = 30 },
   { action_name = "Attack2_Somersault", rate = 300, loop = 1, target_condition = "State3"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 13, loop = 2  },
   { action_name = "Walk_Right", rate = 13, loop = 2  },
   { action_name = "Walk_Back", rate = 25, loop = 2  },
   { action_name = "Move_Left", rate = -1, loop = 1  },
   { action_name = "Move_Right", rate = -1, loop = 1  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Attack4_SickleKick", rate = 20, loop = 1, max_missradian = 30 },
   { action_name = "Attack3_AerialCombo", rate = 12, loop = 1, cooltime = 10000 },
   { action_name = "Attack3_AerialCombo", rate = 700, loop = 1, target_condition = "State1", cooltime = 10000 },
   { action_name = "Attack4_SickleKick", rate = 700, loop = 1, target_condition = "State2", max_missradian = 30 },
   { action_name = "Attack2_Somersault", rate = 700, loop = 1, target_condition = "State3"  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 12, loop = 2  },
   { action_name = "Walk_Back", rate = 15, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 7, loop = 1  },
   { action_name = "Move_Back", rate = 7, loop = 1  },
   { action_name = "Assault", rate = 40, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 14, loop = 1  },
   { action_name = "Move_Right", rate = 14, loop = 1  },
   { action_name = "Move_Front", rate = 24, loop = 1  },
   { action_name = "Move_Back", rate = 20, loop = 1  },
   { action_name = "Assault", rate = 50, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 1  },
   { action_name = "Move_Right", rate = 20, loop = 1  },
   { action_name = "Move_Front", rate = 40, loop = 1  },
   { action_name = "Move_Back", rate = 7, loop = 1  },
   { action_name = "Assault", rate = 70, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 5, loop = 1, approach = 150.0  },
   { action_name = "Attack2_Somersault", rate = 5, loop = 1, approach = 200.0  },
   { action_name = "Attack4_SickleKick", rate = 5, loop = 1, approach = 150.0, max_missradian = 30 },
}
