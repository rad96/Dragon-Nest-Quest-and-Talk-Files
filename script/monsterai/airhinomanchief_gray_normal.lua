--AiMinotauros_White_Armor_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 350;
g_Lua_NearValue3 = 500;
g_Lua_NearValue4 = 750;
g_Lua_NearValue5 = 1600;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 30, loop = 2  },
   { action_name = "Attack1_Bash", rate = 8, loop = 1  },
   { action_name = "Attack12_SupportCannonShot", rate = 8, loop = 1,cooltime = 40000  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 6, loop = 2  },
   { action_name = "Walk_Right", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Attack1_Bash", rate = 7, loop = 1  },
   { action_name = "Attack10_Shot", rate = 2, loop = 1  },
   { action_name = "Attack12_SupportCannonShot", rate = 8, loop = 1,cooltime = 40000  },
   { action_name = "Attack13_FrontBreakShot", rate = 4, loop = 1, cooltime = 30000  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 18, loop = 1  },
   { action_name = "Attack10_Shot", rate = 15, loop = 1  },
   { action_name = "Attack11_WideAreaShot", rate = 4, loop = 1, cooltime = 30000  },
   { action_name = "Attack12_SupportCannonShot", rate = 8, loop = 1,cooltime = 40000  },
   { action_name = "Attack13_FrontBreakShot", rate = 4, loop = 1, cooltime = 30000  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
   { action_name = "Attack10_Shot", rate = 15, loop = 1  },
   { action_name = "Attack11_WideAreaShot", rate = 4, loop = 1, cooltime = 30000  },
   { action_name = "Attack12_SupportCannonShot", rate = 8, loop = 1,cooltime = 40000  },
   { action_name = "Attack13_FrontBreakShot", rate = 4, loop = 1, cooltime = 30000  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
   { action_name = "Attack10_Shot", rate = 15, loop = 1  },
   { action_name = "Attack11_WideAreaShot", rate = 4, loop = 1, cooltime = 30000  },
   { action_name = "Attack12_SupportCannonShot", rate = 8, loop = 1,cooltime = 40000  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Bash", rate = 8, loop = 1, approach = 300.0  },
}

