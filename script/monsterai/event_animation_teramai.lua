
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|!Air", "Down" },
 State2 = {"!Stay|!Move|!Attack", "!Down" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Air"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 신 브레이커
  CustomAction6 = {
     { "useskill", lua_skill_index = 23, rate = 100 },
  },
  -- 
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 15, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Walk_Left", rate = 4, loop = 2 },
     { action_name = "Walk_Right", rate = 4, loop = 2 },
     { action_name = "Walk_Back1", rate = 2, loop = 1 },	 
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 6, loop = 2 },
     { action_name = "Move_Left", rate = 4, loop = 1 },
     { action_name = "Move_Right", rate = 4, loop = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 8, loop = 4 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
}
g_Lua_Near4 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 8, loop = 8 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
     { action_name = "Assault", rate = 4, loop = 1 },
}

g_Lua_Near5 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 10 },
     { action_name = "Move_Left", rate = 3, loop = 2 },
     { action_name = "Move_Right", rate = 3, loop = 2 },
     { action_name = "Assault", rate = 4, loop = 1 },
}

g_Lua_Assault = { 
     { action_name = "Skill_LightningUpper", rate = 16, loop = 1, approach = 150 },
}



g_Lua_Skill = { 
-- 0 평타1
     { skill_index = 96121, cooltime = 9000, rate = 60, rangemin = 0, rangemax = 200, target = 3 , combo1 = "1,100,2" },
-- 1 평타2
     { skill_index = 83556, cooltime = 0, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "2,100,2" },	
-- 2 평타3
     { skill_index = 83557, cooltime = 0, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1= " 3,100,2" },
-- 3 평타4
     { skill_index = 83558, cooltime = 0, rate = -1, rangemin = 0, rangemax = 300, target = 3 },

-- 블록
     { skill_index = 83509, cooltime = 36000, rate = 30, rangemin = 0, rangemax = 1200, target = 1 },
-- 라이트닝 어퍼
     { skill_index = 83501, cooltime = 11000, rate = 70, rangemin = 0, rangemax = 180, target = 3 },
-- 쉴드 차지
     { skill_index = 83522, cooltime = 18000, rate = 60, rangemin = 0, rangemax = 1500, target = 3 },

-- 렐릭 힐EX
     { skill_index = 96131, cooltime = 20000, rate = 70, rangemin = 0, rangemax = 1200, target = 2 },

-- 홀리 크로스EX 83542
     { skill_index = 83538, cooltime = 56000, rate = 80, rangemin = 0, rangemax = 900, target = 3 },
-- 홀리 버스트
     { skill_index = 96134, cooltime = 48000, rate = 70, rangemin = 0, rangemax = 600, target = 3 },


}