--AiTroll_Blue_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000



g_Lua_Near1 = { 
   { action_name = "Attack1_SelfBomb", rate = 10, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Move", rate = 20, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Move", rate = 20, loop = 3  },
}
g_Lua_Near4 = { 
   { action_name = "Move", rate = 30, loop = 3  },
}