--AiMinotauros_Black_Armor_Patrol.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 1800;
g_Lua_NearValue2 = 3000;

g_Lua_PatrolBaseTime = 10000
g_Lua_PatrolRandTime = 6000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Attack7_Event", rate = 5, loop = 1, cooltime = 1000 },
   { action_name = "Attack7_Event", rate = 5, loop = 1, cooltime = 1000 },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Front_Decal", rate = 5, loop = 1 },
   { action_name = "Walk_Front_Decal", rate = 5, loop = 2 },
}