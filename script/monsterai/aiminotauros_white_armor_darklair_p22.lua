--AiMinotauros_White_Armor_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 350;
g_Lua_NearValue3 = 500;
g_Lua_NearValue4 = 750;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Move_Left", rate = 5, loop = 2, max_missradian = 360  },
   { action_name = "Move_Right", rate = 5, loop = 2, max_missradian = 360  },
   { action_name = "Move_Back", rate = 30, loop = 2, max_missradian = 360  },
   { action_name = "Attack6_Howl_NotTarget", rate = 50, loop = 1, max_missradian = 360  },
}
g_Lua_Near2 = { 
   { action_name = "Move_Left", rate = 6, loop = 2, max_missradian = 360  },
   { action_name = "Move_Right", rate = 6, loop = 2, max_missradian = 360  },
   { action_name = "Move_Back", rate = 20, loop = 2, max_missradian = 360  },
   { action_name = "Attack6_Howl_NotTarget", rate = 50, loop = 1, max_missradian = 360  },
}
g_Lua_Near3 = { 
   { action_name = "Move_Left", rate = 10, loop = 2, max_missradian = 360  },
   { action_name = "Move_Right", rate = 10, loop = 2, max_missradian = 360  },
   { action_name = "Move_Front", rate = 23, loop = 1, max_missradian = 360  },
   { action_name = "Attack6_Howl_NotTarget", rate = 50, loop = 1, max_missradian = 360  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 35, loop = 2, max_missradian = 360  },
   { action_name = "Attack6_Howl_NotTarget", rate = 50, loop = 1, max_missradian = 360  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Left", rate = 7, loop = 2, max_missradian = 360  },
   { action_name = "Move_Right", rate = 7, loop = 2, max_missradian = 360  },
   { action_name = "Move_Front", rate = 25, loop = 2, max_missradian = 360  },
   { action_name = "Attack6_Howl_NotTarget", rate = 50, loop = 1, max_missradian = 360  },
}
g_Lua_Skill = { 
   { skill_index = 20202,  cooltime = 20000, rate = 80,rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100, max_missradian = 360 },
}
