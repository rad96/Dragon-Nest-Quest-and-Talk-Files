-- Troll Blue Normal AI (Event)

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 200.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200.0;

g_Lua_LookTargetNearState = 3;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 3000;



g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 2,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
        { action_name = "Move_Leftk",	rate = 4,		loop = 2 },
        { action_name = "Move_Right",	rate = 4,		loop = 2 },
        { action_name = "Move_Back",	rate = 10,		loop = 2 },
        { action_name = "Attack1_Claw", rate = 4,                   loop = 1 },
        { action_name = "Attack3_RollingAttackS_N", rate = 1,          loop = 1 },   
        { action_name = "Attack6_Bite", rate = 2,                   loop = 1 },
            
}

g_Lua_Near2 = 
{
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 3,		loop = 2 },
        { action_name = "Move_Leftk",	rate = 5,		loop = 2 },
        { action_name = "Move_Right",	rate = 5,		loop = 2 },
        { action_name = "Move_Back",	rate = 3,		loop = 2 },
        { action_name = "Attack3_RollingAttack_N", rate = 1,                   loop = 1 },
        { action_name = "Attack3_RollingAttackS_N", rate = 5,                   loop = 1 },   
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 7,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 7,		loop = 2 },
        { action_name = "Walk_Front",	rate = 5,		loop = 2 },
        { action_name = "Walk_Back",	rate = 3,		loop = 2 },
        { action_name = "Move_Leftk",	rate = 7,		loop = 3 },
        { action_name = "Move_Right",	rate = 7,		loop = 3 },
        { action_name = "Move_Front",	rate = 5,		loop = 3 },
        { action_name = "Move_Back",	rate = 3,		loop = 3 },
        { action_name = "Attack3_RollingAttack_N", rate = 4,                   loop = 1 },
        { action_name = "Attack3_RollingAttackS_N", rate = 4,                   loop = 1 },   
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
        { action_name = "Walk_Front",	rate = 5,		loop = 2 },
        { action_name = "Walk_Back",	rate = 0,		loop = 2 },
        { action_name = "Move_Leftk",	rate = 5,		loop = 3 },
        { action_name = "Move_Right",	rate = 5,		loop = 3 },
        { action_name = "Move_Front",	rate = 5,		loop = 3 },
        { action_name = "Move_Back",	rate = 1,		loop = 3 },
        { action_name = "Attack3_RollingAttack_N", rate = 6,                  loop = 1 },
}

g_Lua_NonDownRangeDamage = 				
{ 				
	{ action_name = "Attack3_RollingAttack_N",	rate = 10,		loop = 1 },
	{ action_name = "Move_Left",	rate = 1,		loop = 2 },
	{ action_name = "Move_Right",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 2 },
}