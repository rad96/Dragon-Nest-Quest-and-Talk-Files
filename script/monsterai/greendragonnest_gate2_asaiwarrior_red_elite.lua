--AiAsaiWorrior_Assault_Blue_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
}

g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Move_Front",	rate = 10,		loop = 1 },
   { action_name = "Move_Left",	rate = 3,		loop = 1 },
   { action_name = "Move_Right",	rate = 3,		loop = 1 },
   { action_name = "Attack1_Pierce", rate = 5, loop = 2 },
}

g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Move_Front",	rate = 10,		loop = 1 },
   { action_name = "Move_Left",	rate = 1,		loop = 1 },
   { action_name = "Move_Right",	rate = 1,		loop = 1 },
}

g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 3, loop = 3 },
}

g_Lua_Skill = { 
   { skill_index = 20470,  cooltime = 4500, rate = 100,rangemin = 0, rangemax = 600, target = 3,usedskill="20471" }, --"����"
   { skill_index = 20471,  cooltime = 15000, rate = 100,limitcount = 5, rangemin = 0, rangemax = 1500, target = 2, selfhppercent = 60 },-- "�ݳ�"
}