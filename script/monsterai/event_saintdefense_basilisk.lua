--AiGoblin_Green_Normal.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 1300;
g_Lua_NearValue2 = 1700;
g_Lua_NearValue3 = 9000;



g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 1300
g_Lua_AssaultTime = 5000

g_Lua_CustomAction =
{
   CustomAction1 = 
   {
	{ "Turn_Left", 1 },
 	{ "useskill", lua_skill_index = 0, rate = 100 },
   },
   CustomAction2 = 
   {
 	{ "Turn_Right", 1 },
 	{ "useskill", lua_skill_index = 0, rate = 100 },
   },
}


g_Lua_Near1 = { 
   { action_name = "Stand_2", globalcooltime = 7, rate = 12, loop = 1 },
   { action_name = "Stand_1", globalcooltime = 7, rate = 6, loop = 1 },
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Turn_Left", rate = 16, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 16, loop = -1, td = "RF,RB,BR" },
   { action_name = "CustomAction1", rate = 50, loop = -1, globalcooltime = 2, priority = 45, td = "LF,LB,BL" },
   { action_name = "CustomAction2", rate = 50, loop = -1, globalcooltime = 2, priority = 45, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", globalcooltime = 7, rate = 6, loop = 1 },
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 32, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 16, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 16, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 32, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 16, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 16, loop = -1, td = "RF,RB,BR" },
}

g_Lua_Assault = { 
   { action_name = "Stand_2", rate = 12, loop = 1, approach = 1300 },
   { lua_skill_index = 0, rate = 6, approach = 1300 },
}

g_Lua_GlobalCoolTime1 = 25000 -- FootAttack,TailAttack
g_Lua_GlobalCoolTime2 = 30000 -- ustomAction
g_Lua_GlobalCoolTime3 = 50000 -- Attack12_Rollin
g_Lua_GlobalCoolTime4 = 8000 -- Attack14_Shout
g_Lua_GlobalCoolTime5 = 60000 -- Attack07_Stomp
g_Lua_GlobalCoolTime6 = 70000 -- SpuerBreath
g_Lua_GlobalCoolTime7 = 10000 -- Stand_2
g_Lua_GlobalCoolTime8 = 60000 -- Rush

g_Lua_Skill = { 
-- 34528 : Attack01_ChinAttack / 34520 : Attack09_Rampage_Start / 34518 : Attack08_Dig
   { skill_index = 34528, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3  },
   { skill_index = 34520, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3  },
   { skill_index = 34518, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3  },
-- 34531 : Attack14_Prison2 / 34532 : Attack06_Prison4_Start
   { skill_index = 34531, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 19000, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 4 },
   { skill_index = 34532, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 19000, target = 3 },
-- 34523 : Attack12_Rollin_Left / 34524 : Attack13_Rollin_Right
   { skill_index = 34523, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3 },
   { skill_index = 34524, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3 },
-- 34534 : Attack07_Stomp
   { skill_index = 34534, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3 },
-- 34534 : SuperBreath
   { skill_index = 34526, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3 },
   { skill_index = 34533, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3 },
-- Attack08_Prison1_Start
   { skill_index = 34530, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, next_lua_skill_index = 3 },

-- Attack09_Rampage_Burrow
   { skill_index = 34519, cooltime = 1000, rate = 100, priority = 100, rangemin = 0, rangemax = 19000, target = 3, selfhppercent = 75, limitcount = 1 },
   { skill_index = 34537, cooltime = 1000, rate = 100, priority = 100, rangemin = 0, rangemax = 19000, target = 3, selfhppercent = 50, limitcount = 1 },
   { skill_index = 34538, cooltime = 1000, rate = 100, priority = 100, rangemin = 0, rangemax = 19000, target = 3, selfhppercent = 25, limitcount = 1 },

-- Attack01_ChinAttack
   { skill_index = 34511, cooltime = 25000, rate = 60, rangemin = 0, rangemax = 1300, target = 3, td = "FR,FL" },
-- Attack02_FootAttack_Right, Attack03_FootAttack_Left, Attack04_TailAttack
   { skill_index = 34512, cooltime = 1000, globalcooltime = 1, rate = 60, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", td = "RF,RB" },
   { skill_index = 34513, cooltime = 1000, globalcooltime = 1, rate = 60, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", td = "LF,LB" },
   { skill_index = 34514, cooltime = 1000, globalcooltime = 1, rate = 60, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", td = "BL,BR" },
-- Attack05_Breath
   { skill_index = 34515, cooltime = 37000, rate = 50, rangemin = 0, rangemax = 2000, target = 3, td = "FR,FL", encountertime = 20000 },
-- Attack14_Shout
   { skill_index = 34525, cooltime = 50000, globalcooltime = 4, rate = 60, rangemin = 0, rangemax = 8000, target = 3, randomtarget = "1.6,0,1", encountertime = 20000 },
-- Attack10_Shake
   { skill_index = 34521, cooltime = 50000, rate = 70, rangemin = 0, rangemax = 8000, target = 3 },
-- Attack06_Rush_Start
   { skill_index = 34516, cooltime = 1000, globalcooltime = 8, rate = 55, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", td = "FR,FL,RF,LF", next_lua_skill_index = 2, encountertime = 20000, selfhppercentrange = "50,100" },
   { skill_index = 34516, cooltime = 1000, globalcooltime = 8, rate = 55, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 10, selfhppercentrange = "0,49" },

-- 34523 : Attack12_Rollin_Left / 34524 : Attack13_Rollin_Right
   { skill_index = 34523, cooltime = 1000, globalcooltime = 3, rate = 65, rangemin = 0, rangemax = 6000, target = 3, td = "LF,LB", randomtarget = "1.6,0,1", next_lua_skill_index = 6, selfhppercent = 74 },
   { skill_index = 34524, cooltime = 1000, globalcooltime = 3, rate = 65, rangemin = 0, rangemax = 6000, target = 3, td = "RF,RB", randomtarget = "1.6,0,1", next_lua_skill_index = 5, selfhppercent = 74 },
-- Attack07_Stomp
   { skill_index = 34517, cooltime = 1000, globalcooltime = 5, rate = 53, rangemin = 0, rangemax = 6000, target = 3, selfhppercentrange = "25,63" },
   { skill_index = 34517, cooltime = 1000, globalcooltime = 5, rate = 55, rangemin = 0, rangemax = 6000, target = 3, selfhppercentrange = "0,24", next_lua_skill_index = 7 },

-- Attack11_Kung
   { skill_index = 34522, cooltime = 70000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "1.6,0,1", selfhppercent = 38 },

-- 34526 : Attack15_SuperBreath_Right / 34533 : Attack15_SuperBreath_Left_Start
   { skill_index = 34535, cooltime = 1000, globalcooltime = 6, rate = 73, rangemin = 0, rangemax = 9000, target = 3, selfhppercent = 24, next_lua_skill_index = 8 },
   { skill_index = 34535, cooltime = 1000, globalcooltime = 6, rate = 75, rangemin = 0, rangemax = 9000, target = 3, selfhppercent = 24, next_lua_skill_index = 9 },
}