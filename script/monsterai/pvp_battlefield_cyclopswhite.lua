--AiCyclops_Green_Boss_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 450;
g_Lua_NearValue3 = 850;
g_Lua_NearValue4 = 1250;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 250
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop = 1},
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
   { action_name = "Attack001_Bash", rate = 12, loop = 1 },
   { action_name = "Attack004_TurningSlash", rate = 8, loop = 1, selfhppercent = 75, cooltime = 18000 },
   { action_name = "Attack012_SummonRelic_Buff2", rate = 4, randomtarget = "1.6,0,1", cooltime = 25000, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1},
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack003_JumpAttack", rate = 12, loop = 1, randomtarget = "1.6,0,1" },
   { action_name = "Assault", rate = 20, loop = 1  },
   { action_name = "Attack008_SummonRelic_Charger", rate = 4, cooltime = 25000, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 3 },
   { action_name = "Assault", rate = 13, loop = 1  },
}

g_Lua_Assault = { 
   { action_name = "Attack001_Bash", rate = 8, loop = 1, approach = 250 },
}

g_Lua_BeHitSkill = { 
   { lua_skill_index = 0, rate = 100, skill_index = 30315  },
}

g_Lua_Skill = {
-- 데스허그2
   { skill_index = 30316,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500,  target = 3 },
-- 데스허그 Attack013_DeathHug1_Start
   { skill_index = 30315,  cooltime = 40000, rate = 60, rangemin = 0, rangemax = 800,  target = 3, td = "FL,FR,RB,BR", randomtarget = "1.6,0,1", selfhppercent = 75 },
-- 스턴샤우트 Attack005_StunShout
   { skill_index = 30310,  cooltime = 45000, rate = 40, rangemin = 0, rangemax = 1000, target = 3, encountertime = 10000 },
-- 헤비대쉬 Attack007_HeavyDash
   { skill_index = 30312,  cooltime = 30000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, td = "FL,FR", randomtarget = "1.6,0,1" },
-- 아이오브일렉트릭 Attack006_EyeofElectric
   { skill_index = 30311,  cooltime = 15000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, td = "FL,FR" },

-- 라이트닝스톰 Attack011_LightningStorm_Start
--   { skill_index = 30314,  cooltime = 40000, rate = 90, rangemin = 0, 	rangemax = 1500, target = 3, td = "FL,FR,RB,BR,BL,LB",	selfhppercentrange = "25,50" },
}
