g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300 --몬스터가 돌격시에 타겟에게 다가가는 최소거리
g_Lua_AssualtTime = 1000 --Assault 명령을 받고 돌격 할 때 타겟을 쫓아가는 시간. 

g_Lua_Near1 = { 
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Attack1_Bite", rate = 80, loop = 1  }, --돌격 하거나   
}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 50, loop = 1  },
   { action_name = "Assault", rate = 150, loop = 1  }, --돌격 하거나
}

g_Lua_Near3 = { --너무 멀면 대기
   { action_name = "Stand", rate = 80, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
}

g_Lua_Assault = { { lua_skill_index = 0, rate = 10, approach = 1500},} 

g_Lua_Skill = 
{ 
   { skill_index = 36806,  cooltime = 2000, rate = 1, rangemin = 0, rangemax = 3000, target = 3 }, 
}