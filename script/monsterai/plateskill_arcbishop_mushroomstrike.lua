g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 2000;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 =
{
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Attack1_SelfBomb_PlateSkill", rate = 1, loop = 1 },

}
g_Lua_Near2 =
{
   { action_name = "Move_Front", rate = 5, loop = 1 },
}
g_Lua_Near3 =
{
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Stand_1", rate = 2, loop = 1  },
}
g_Lua_Near4 =
{
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Stand_1", rate = 2, loop = 1  },
}