--AiGhoul_Red_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 600;
g_Lua_NearValue5 = 1000;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}

g_Lua_Skill = { 
   { skill_index = 20022, cooltime = 5000, rate = 100,rangemin = 100, rangemax = 800, target = 3 },
}
