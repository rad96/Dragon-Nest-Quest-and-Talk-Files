--AiAsaiShaman_White_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 4000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Back", rate = 15, loop = 1 },
   { action_name = "Move_Back", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 7, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 1, loop = 1 },
   { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 2 },
}
g_Lua_Skill = { 
   { skill_index = 20522,  cooltime = 11000, rate = 80,rangemin = 0, rangemax = 600, target = 3 },
   { skill_index = 20528,  cooltime = 30000, rate = 80, rangemin = 0, rangemax = 800, target = 2, selfhppercent = 50 },
   { skill_index = 20525,  cooltime = 23000, rate = 80, rangemin = 400, rangemax = 800, target = 3, encountertime = 7000 },
}
