--AIApocalypse_Tentacle_nest.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 30000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 3000

g_Lua_State = {
  State1 = { "Stay|Move|Attack|Air|Stun", "!Down|!Stiff|!Hit" },
  State2 = { "Down", "!Stay|!Move|!Attack|!Air|!Stun|!Stiff|!Hit" },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 12, loop = 2  },
   { action_name = "Walk_Right", rate = 12, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 1  },
   { action_name = "Move_Right", rate = 8, loop = 1  },
   { action_name = "Move_Back", rate = 8, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 7, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 8, loop = 1  },
   { action_name = "Move_Right", rate = 8, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 10, loop = 1  },

}
g_Lua_BeHitSkill = { 
   { lua_skill_index = 1, rate = 100, skill_index = 30218  },
}
g_Lua_Skill = { 
-- ����
   { skill_index = 30218,  cooltime = 10000, rate = 50, rangemin = 0, 	rangemax = 500,  target = 3, target_condition = "State1"  },
-- ����
   { skill_index = 30217,  cooltime = 1000, rate = -1, rangemin = 0, 	rangemax = 600,  target = 3 },
}