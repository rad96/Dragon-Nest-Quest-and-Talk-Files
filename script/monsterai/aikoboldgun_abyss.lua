--AiKoboldGun_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 2000;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Turn_Left", rate = 15, loop = 2, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = 1, td = "RF,RB,BR" },
   { action_name = "Attack1", rate = 18, loop = 1, td = "FL,FR", max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Turn_Left", rate = 20, loop = 2, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 20, loop = 2, td = "RF,RB,BR" },
   { action_name = "Attack1", rate = 24, loop = 1, td = "FL,FR", max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Turn_Left", rate = 20, loop = 2, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 20, loop = 2, td = "RF,RB,BR" },
   { action_name = "Attack1", rate = 22, loop = 1, td = "FL,FR", max_missradian = 30 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Turn_Left", rate = 20, loop = 3, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 20, loop = 3, td = "RF,RB,BR" },
   { action_name = "Attack1", rate = -1, loop = 3, td = "FL,FR", max_missradian = 30 },
}
