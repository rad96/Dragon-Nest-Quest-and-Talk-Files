
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1400;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 3000;

g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Walk_Left", rate = 4, loop = 2 },
     { action_name = "Walk_Right", rate = 4, loop = 2 },
     { action_name = "Walk_Back1", rate = 12, loop = 1 },	
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 6, loop = 1 },
     { action_name = "Move_Left", rate = 4, loop = 1 },
     { action_name = "Move_Right", rate = 4, loop = 1 },
}

g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 2 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 4 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 6 },
     { action_name = "Move_Left", rate = 3, loop = 2 },
     { action_name = "Move_Right", rate = 3, loop = 2 },
}

g_Lua_Skill = { 
-- 블링크_B
    { skill_index = 83002, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 600, target = 3 },
-- 평타
     { skill_index = 83055, cooltime = 5000, rate = 50, rangemin = 0, rangemax = 700, target = 3, cancellook = 1 },
-- 쇼크 웨이브
     { skill_index = 83019, cooltime = 11000, rate = 80, rangemin = 0, rangemax = 200, target = 3 },
-- 타임 리스트릭션
     { skill_index = 96111, cooltime = 30000, rate = 90, rangemin = 0, rangemax = 500, target = 3, next_lua_skill_index = 0 },
-- 스펙트럼 샤워EX
     { skill_index = 96105, cooltime = 35000, rate = 70, rangemin = 0, rangemax = 600, target = 3 },
-- 스위치 그라비티
     { skill_index = 96107, cooltime = 42000, rate = 70, rangemin = 0, rangemax = 600, target = 3 },
}