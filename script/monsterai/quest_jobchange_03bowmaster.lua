

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 120;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}


g_Lua_Near1 = 
{ 
-- 0 ~ 120 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Attack_SideKick",	rate = 12,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Back",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Shoot_Stand_BigBow",	rate = 15,		loop = 1,	selfhppercentrange = "0,100", cooltime = 5000 },
	{ action_name = "Move_Left",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },

}
g_Lua_Near2 = 
{ 
-- 120 ~ 500 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Shoot_Stand_BigBow",	rate = 15,		loop = 1,	selfhppercentrange = "0,100", cooltime = 5000 },
	{ action_name = "Move_Back",		rate = 30,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near3 = 
{ 
-- 500 ~ 800 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Back",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Shoot_Stand_BigBow",	rate = 15,		loop = 1,	selfhppercentrange = "0,100", cooltime = 5000 },
}

g_Lua_Near4 = 
{ 
-- 800 ~ 1200 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Shoot_Stand_BigBow",	rate = 15,		loop = 1,	selfhppercentrange = "0,100", cooltime = 5000 },
	{ action_name = "Move_Left",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Back",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near5 = 
{ 
-- 1200 ~ 10000 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Back",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
}


g_Lua_Skill = { 
-- ������ ����
   { skill_index = 31121,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "RB,BR,BL,LB"  },
-- ������ ����
   { skill_index = 31122,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FL,FR,LF,RF"  },
-- ������ ������
   { skill_index = 31123,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FL,LF,RB,BR,BL"  },
-- ������ ����
   { skill_index = 31124,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FR,RF,RB,BR,BL"  },
-- Ʈ����
   { skill_index = 31031,  cooltime = 4500, rate = 100, rangemin = 0, rangemax = 1000,target = 3  },
-- ��Ƽ��
   { skill_index = 31032,  cooltime = 28000, rate = 100, rangemin = 0, rangemax = 800,target = 3 },
-- �����ַο�
   { skill_index = 31033,  cooltime = 13000, rate = 100, rangemin = 0, rangemax = 800,target = 3  },
-- ���ο콺��ű
   { skill_index = 31034,  cooltime = 12000, rate = 100, rangemin = 0, rangemax = 300,target = 3  },

-- ������
   { skill_index = 31041,  cooltime = 15000, rate = 100, rangemin = 500, rangemax = 1200,target = 3},
-- ���̸���ü�μ�
   { skill_index = 31042,  cooltime = 17000, rate = 100, rangemin = 0, rangemax = 500,target = 3, td = "FL,FR" },
-- ���̵�弦
   { skill_index = 31043,  cooltime = 30000, rate = 100, rangemin = 300, rangemax = 800,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,50" },
-- �ַο����
   { skill_index = 31044,  cooltime = 35000, rate = 100, rangemin = 500, rangemax = 1500,target = 3  },
-- ������Ʈ��
   { skill_index = 31045,  cooltime = 18000, rate = 100, rangemin = 0, rangemax = 500,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
}