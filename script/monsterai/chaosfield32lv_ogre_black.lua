--AiOgre_Black_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Attack1_bash", rate = 13, loop = 1  },
   { action_name = "Attack3_Upper", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 6, loop = 1  },
   { action_name = "Move_Front", rate = 4, loop = 1  },
   { action_name = "Attack1_bash", rate = 9, loop = 1  },
   { action_name = "Attack3_Upper", rate = 18, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
   { action_name = "Attack13_Dash", rate = 12, loop = 1, cooltime = 15000 },
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 2  },
   { action_name = "Attack7_JumpAttack", rate = 12, loop = 1  },
   { action_name = "Attack13_Dash", rate = 9, loop = 1, cooltime = 15000 },
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 30, loop = 2  },
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Front", rate = 30, loop = 1  },
   { action_name = "Assault", rate = 22, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 3, loop = 1, approach = 250.0  },
   { action_name = "Attack3_Upper", rate = 5, loop = 1, approach = 300.0  },
}
