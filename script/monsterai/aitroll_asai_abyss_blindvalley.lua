--AiTroll_Blue_Assault_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 6, loop = 1  },
   { action_name = "Walk_Right", rate = 6, loop = 1  },
   { action_name = "Attack1_Claw", rate = 13, loop = 1 },
   { action_name = "Attack6_Bite", rate = 9, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1  },
   { action_name = "Attack8_Wave", rate = 6, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 2  },
   { action_name = "Attack8_Wave", rate = 9, loop = 1 },
   { action_name = "Assault", rate = 6, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
   { action_name = "Assault", rate = 6, loop = 1 },
}

g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 10, loop = 1, approach = 250.0  },
}

g_Lua_Skill = {
   { skill_index = 20971,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 3000, target = 1, limitcount = 1 }, 
}