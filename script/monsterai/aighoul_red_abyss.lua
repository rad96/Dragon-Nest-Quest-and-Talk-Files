--AiGhoul_Red_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 600;
g_Lua_NearValue5 = 1000;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_ArmAttack", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Move_Back", rate = 20, loop = 2  },
   { action_name = "Attack1_ArmAttack", rate = 10, loop = 1  },
   { action_name = "Attack2_ArmSlash", rate = 8, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Attack2_ArmSlash", rate = 13, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 1, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_ArmAttack", rate = 2, loop = 1, cancellook = 1, approach = 150.0  },
   { action_name = "Attack2_ArmSlash", rate = 4, loop = 1, cancellook = 1, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 20022,  cooltime = 5000, rate = 100,rangemin = 100, rangemax = 800, target = 3, selfhppercent = 30 },
}
