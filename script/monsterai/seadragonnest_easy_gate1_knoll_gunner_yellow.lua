--AiKnoll_Gunner_Yellow_Elite_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 50000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Attack1_Fire_ApocalypseNest", rate = 15, loop = 1, max_missradian = 20  },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Fire_ApocalypseNest", rate = 20, loop = 1, max_missradian = 20  },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
   { action_name = "Attack1_Fire_ApocalypseNest", rate = 25, loop = 1 , max_missradian = 30 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 20, loop = 6 },
}
g_Lua_Skill = { 
   { skill_index = 30404,  cooltime = 40000, rate = 80, rangemin = 300, rangemax = 10000, target = 3, selfhppercent = 100  },
}
