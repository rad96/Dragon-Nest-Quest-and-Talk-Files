--90lv_Queen_spider_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 1200.0;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Walk_Back", rate = 6, loop = 1, cooltime = 5000 },
	{ action_name = "Attack1_Slash", rate = 5, loop = 1, cooltime = 6000, selfhppercentrange = "61,100" },
	{ action_name = "Attack1_Slash_Lv2_Boss", rate = 8, loop = 1, cooltime = 6000, selfhppercentrange = "0,60" },
	{ action_name = "Attack2_Blow", rate = 8, loop = 1, cooltime = 8000 },

}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 6, loop = 1 },
	{ action_name = "Walk_Front", rate = 4, loop = 1 },
	{ action_name = "Attack3_JumpAttack", rate = 5, loop = 1, cooltime = 20000, selfhppercentrange = "61,100"  },
	{ action_name = "Attack3_JumpAttack_Lv2", rate = 5, loop = 1, cooltime = 18000, selfhppercentrange = "0,60" },
	{ action_name = "Attack2_Blow", rate = 12, loop = 1, cooltime = 8000 },

}

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Front", rate = 7, loop = 1 },
	{ action_name = "Attack3_JumpAttack", rate = 5, loop = 1, cooltime = 24000, selfhppercentrange = "61,100" },
	{ action_name = "Attack3_JumpAttack_Lv2", rate = 5, loop = 1, cooltime = 20000, selfhppercentrange = "0,60" },
}

g_Lua_GlobalCoolTime1 = 28000 --_Attack8_RollingAttack_Boss

g_Lua_Skill = { 
-- next_lua_skill_index
	-- 0: Attack09_SelfExplosion
	{ skill_index = 500280, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3 },
	-- 1: Attack8_RollingAttack_Boss
	{ skill_index = 500279, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3 },
	-- 2 : Attack6_Pull
	{ skill_index = 500278, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3 },

-- AI
	-- Attack4_Wep_Nest
	{ skill_index = 500276, cooltime = 28000, rate = 30, rangemin = 0, rangemax = 1200, target = 3, multipletarget = "1,2,0,1", selfhppercentrange = "61,100", next_lua_skill_index = 2 },
	{ skill_index = 500276, cooltime = 24000, globalcooltime = 1, rate = 30, rangemin = 0, rangemax = 1200, target = 3, encountertime = 15000, multipletarget = "1,2,0,1", selfhppercentrange = "0,60", next_lua_skill_index = 1 },
	-- Attack5_Prickle
	{ skill_index = 500277, cooltime = 36000, rate = 40, rangemin = 0, rangemax = 500, target = 3, encountertime = 3000, randomtarget= "1.6,0,1", selfhppercentrange = "41,95" },
	-- Attack6_Pull
	{ skill_index = 500278, cooltime = 24000, rate = 50, rangemin = 0, rangemax = 800, target = 3, encountertime = 10000, selfhppercentrange = "41,60" },
	{ skill_index = 500278, cooltime = 48000, rate = 50, rangemin = 0, rangemax = 800, target = 3, encountertime = 10000, selfhppercentrange = "0,40", next_lua_skill_index = 0 },
	-- Attack8_RollingAttack_Boss
	{ skill_index = 500279, cooltime = 32000, globalcooltime = 1, rate = 30, rangemin = 200, rangemax = 800, target = 3, selfhppercentrange = "0,95" },

}
