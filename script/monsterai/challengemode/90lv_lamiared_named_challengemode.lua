-- 90lv_LamiaRed_Named_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
	CustomAction1 = {
		{ "Attack01_TailAttack", 0 },
		{ "Walk_Back", 2 },
	},
}

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Left", rate = 4, loop = 1 },
	{ action_name = "Walk_Right", rate = 4, loop = 1 },
	{ action_name = "Walk_Back", rate = 4, loop = 1 },
	{ action_name = "Attack4_RollBite", rate = 18, loop = 1, cooltime = 12000 },
	{ action_name = "Attack3_BackCut", rate = 18, loop = 1, cooltime = 6000 },

}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 4, loop = 1 },
	{ action_name = "Walk_Front", rate = 10, loop = 1 },
	{ action_name = "CustomAction1", rate = 12, loop = 1, cooltime = 8000 },
	{ action_name = "Attack6_Combo", rate = 18, loop = 1, cooltime = 12000 },

}
g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 4, loop = 1 },
	{ action_name = "Move_Front", rate = 6, loop = 2 },

}

g_Lua_Skill = { 
-- Ai
	-- Attack05_LavaWave_start
	{ skill_index = 500166,  cooltime = 48000, rate = 160, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,70" },
	-- Attack03_FireBall2_Start
	{ skill_index = 500167,  cooltime = 24000, rate = 50, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,90" },

}
