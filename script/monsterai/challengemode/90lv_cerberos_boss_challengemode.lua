--90lv_Cerberos_Boss_ChallengeMode

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 200.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 800.0;

--_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_GlobalCoolTime1 = 12000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 12, loop = 1 },
	{ action_name = "Walk_Back", rate = 6, loop = 1 },
	{ action_name = "Walk_Left", rate = 3, loop = 1 },
	{ action_name = "Walk_Right", rate = 3, loop = 1 },
	{ action_name = "Attack_Punch", rate = 20, loop = 1, td = "FL,FR,LF,RF",cooltime = 8000, globalcooltime = 1 },
	{ action_name = "Attack_Bite", rate = 26, loop = 1, cooltime = 8000 },
	{ action_name = "Attack_Back_Left", rate = 25, loop = 1, td = "BL,LB", cooltime = 8000 },
	{ action_name = "Attack_Back_Right", rate = 25, loop = 1, td = "BR,RB", cooltime = 8000 },

}

g_Lua_Near2 = {
	{ action_name = "Stand", rate = 6, loop = 1 },
	{ action_name = "Walk_Front", rate = 4, loop = 1 },
	{ action_name = "Walk_Left", rate = 3, loop = 1 },
	{ action_name = "Walk_Right", rate = 3, loop = 1 },
	{ action_name = "Attack_Punch", rate = 15, loop = 1, td = "FL,FR,LF,RF", cooltime = 8000, globalcooltime = 1 },
	{ action_name = "Attack_Down_Right", rate = 15, loop = 1, td = "FR,FL", cooltime = 8000 },

}

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 6, loop = 1 },
	{ action_name = "Move_Front", rate = 5, loop = 1 },
	{ action_name = "Attack002_ClawRange1", rate = 20, loop = 1, cooltime = 6000 },
	{ action_name = "Attack020_Stamp", rate = 20, loop = 1, cooltime = 14000 },
	{ action_name = "Assault", rate = 25, loop = 1, cooltime = 18000 },
}

g_Lua_Assault = { 
	{ action_name = "Attack020_Stamp", rate = 4, loop = 1, approach = 400 },
	{ action_name = "Attack018_Bite", rate = 6, loop = 1, approach = 200 },
}

g_Lua_GlobalCoolTime2 = 23000 	-- _G1: Attack2_LightningField

g_Lua_Skill = { 

-- Next_lua_skill_index
	-- 0: Attack3_FireBreath	
	{ skill_index = 500221, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 800, target = 3 },
	-- 1: Stun
	{ skill_index = 500224, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 800, target = 3 },
	-- 2: Attack2_DarkLightning
	{ skill_index = 500218, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 800, target = 3 },
	
-- AI
	-- Attack1_EyeOfIce
	{ skill_index = 500216, cooltime = 16000, rate = 50, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "81,100" },
	-- Attack1_IceBreath
	{ skill_index = 500217, cooltime = 23000, rate = 50, rangemin = 200, rangemax = 800, target = 3, selfhppercentrange = "0,80" },
	-- Attack3_FireBreath	
	{ skill_index = 500221, cooltime = 16000, rate = 50, rangemin = 200, rangemax = 800, target = 3, selfhppercentrange = "81,100" },
	-- Attack3_FireBall
	{ skill_index = 500220, cooltime = 23000, rate = 50, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,80" },
	-- Attack2_LightningField
	{ skill_index = 500219, cooltime = 30000, globalcooltime = 2, rate = 100, rangemin = 0, rangemax = 800, target = 3, encountertime = 30000, selfhppercentrange = "61,100" },
	{ skill_index = 500219, cooltime = 30000, globalcooltime = 2, rate = 100, rangemin = 0, rangemax = 800, target = 3, encountertime = 30000, selfhppercentrange = "0,60", next_lua_skill_index = 0 },
	-- Avoid
	{ skill_index = 500222, cooltime = 40000, rate = 150, rangemin = 0, rangemax = 300, target = 3, selfhppercentrange = "0,80", next_lua_skill_index = 2 },
	-- Attack3_FireWall
	{ skill_index = 500223, cooltime = 52000, rate = 300, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,70", next_lua_skill_index = 1 },

}