--AiMecharoid_vII_Normal.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 7, loop = 1 },
   { action_name = "Walk_Right", rate = 7, loop = 1 },
   { action_name = "Walk_Back", rate = 7, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Front", rate = 18, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
}

g_Lua_Skill = { 
   { skill_index = 500183,  cooltime = 8000, rate = 80,rangemin = 0, rangemax = 600, target = 3 },
   { skill_index = 500184,  cooltime = 19000, rate = 80, rangemin = 0, rangemax = 400, target = 3 },
}
