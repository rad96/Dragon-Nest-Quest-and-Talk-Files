--90lv_Ignacio_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 100
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Back", rate = 5, loop = 1, cooltime = 5000 },
	{ action_name = "Attack1_ChargeVolt_Guardian_Challenge", rate = 12, loop = 1, td = "FR,FL,RF,LF", cooltime = 6000 },

}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 3, loop = 1 },
	{ action_name = "Walk_Front", rate = 5, loop = 1 },
	{ action_name = "Attack13_Dash_Guardian", rate = 8, loop = 1, cooltime = 18000 },

}

g_Lua_GlobalCoolTime1 = 15000

g_Lua_Skill = { 
-- next_lua_skill_index
 	-- 0: Attack19_HeavensJudgment_Guardian_Start
	{ skill_index = 500347, cooltime = 18000, rate = -1, rangemin = 0, rangemax = 1200, target = 3 },	

--Ai
	-- Attack1_ChargeVolt_Guardian
	{ skill_index = 500346, cooltime = 10000, rate = 30, rangemin = 0, rangemax = 400, target = 3, encountertime = 12000, selfhppercentrange = "0,85" },	
	-- Attack12_LightningVolt_Guardian
	{ skill_index = 500351, cooltime = 16000, rate = 30, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,90" },	
	-- Attack7_JumpChargeVolt_Guardian
	{ skill_index = 500348, cooltime = 24000, rate = 50, rangemin = 200, rangemax = 800, target = 3, selfhppercentrange = "0,85" },	
	-- Attack17_Gravity_Emergence
	{ skill_index = 500349, cooltime = 46000, rate = 200, rangemin = 0, rangemax = 1200, target = 3, encountertime = 2000, selfhppercentrange = "0,60", next_lua_skill_index = 0 },	
	--  Attack17_HollyBurst_Guardian
	{ skill_index = 500350, cooltime = 36000, rate = 150, rangemin = 0, rangemax = 800, target = 3, encountertime = 3000, selfhppercentrange = "0,70" },	
	-- Attack14_Combo_Guardian
	{ skill_index = 500352, cooltime = 28000, rate = 40, rangemin = 0, rangemax = 500, target = 3, encountertime = 20000, selfhppercentrange = "0,80" },	

}
