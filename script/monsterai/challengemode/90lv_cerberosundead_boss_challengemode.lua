--90lv_CerberosUndead_Boss_ChallengeMode

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 800.0;

--_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;

g_Lua_CustomAction =
{
	CustomAction1 = {
		{ "Attack3", 0 },
		{ "Attack_Punch", 0 },
	},
}

g_Lua_GlobalCoolTime1 = 8000
g_Lua_GlobalCoolTime2 = 8000


g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 15, loop = 1 },
	{ action_name = "Walk_Back", rate = 3, loop = 1 },
	{ action_name = "Walk_Left", rate = 5, loop = 1 },
	{ action_name = "Walk_Right", rate = 5, loop = 1 },
	{ action_name = "Attack_Back_Left", rate = 20, loop = 1, td = "LB,BL", cooltime = 6000 },
	{ action_name = "Attack_Back_Right", rate = 20, loop = 1, td = "RB,BR", cooltime = 6000 },
	{ action_name = "Attack_Down_Left", rate = 20, loop = 1, td = "FL,LF", cooltime = 6000, globalcooltime = 1 },
	{ action_name = "Attack_Down_Right", rate = 20, loop = 1, td = "FR,RF", cooltime = 6000, globalcooltime = 2 },

}

g_Lua_Near2 = {
	{ action_name = "Stand", rate = 12, loop = 1, td = "FL,RF" },
	{ action_name = "Walk_Front", rate = 3, loop = 1, td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Left", rate = 8, loop = 1 },
	{ action_name = "Walk_Right", rate = 8, loop = 1 },
	{ action_name = "CustomAction1", rate = 25, loop = 1, td = "FR,FL,RF,LF", cooltime = 10000 },
	{ action_name = "Attack_Down_Left", rate = 20, loop = 1, td = "FL,LF", cooltime = 6000, globalcooltime = 1 },
	{ action_name = "Attack_Down_Right", rate = 20, loop = 1, td = "FR,RF", cooltime = 6000, globalcooltime = 2 },

}

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Front", rate = 20, loop = 1, td = "FL,FR,LF,RF"},

}


g_Lua_GlobalCoolTime3 = 23000 	--_G1: Jump CoolTime
g_Lua_GlobalCoolTime4 = 45000 	--_G2: ttack_Curse_SeaDragon_3

g_Lua_Skill = {
-- next_lua_skill_index
	--Attack_Down_Left_SeaDragon
	{ skill_index = 500226, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 800, target = 3, next_lua_skill_index = 1 },
	-- Attack_Down_Right_SeaDragon
	{ skill_index = 500227, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 800, target = 3 },
-- AI
	-- Attack_Cry
	{ skill_index = 500228, cooltime = 48000, rate = 100, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR,LF,RF", encountertime = 30000, selfhppercentrange = "0,70", next_lua_skill_index = 0 },
	-- Attack_Curse_SeaDragon_3
	{ skill_index = 500229, cooltime = 36000, globalcooltime = 4, rate = 100, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR,LF,RF", encountertime = 10000, selfhppercentrange = "71,80", limitecount = 1 },
	{ skill_index = 500229, cooltime = 36000, globalcooltime = 4, rate = 100, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR,LF,RF", encountertime = 10000, selfhppercentrange = "51,60", limitecount = 1 },
	{ skill_index = 500229, cooltime = 36000, globalcooltime = 4, rate = 100, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR,LF,RF", encountertime = 10000, selfhppercentrange = "0,40" },
	-- Attack_Jump2
	{ skill_index = 500230, cooltime = 23000, globalcooltime = 3, rate = 100, rangemin = 300, rangemax = 800, target = 3, encountertime = 30000, selfhppercentrange = "61,100" },
	{ skill_index = 500230, cooltime = 18000, globalcooltime = 3, rate = 100, rangemin = 300, rangemax = 800, target = 3,  encountertime = 30000, selfhppercentrange = "0,60", next_lua_skill_index = 0 },

}