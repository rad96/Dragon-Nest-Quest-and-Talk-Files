--AiCyclops_White_Normal.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
	CustomAction1 = {
		{ "Attack003_JumpAttack", 0 },
		{ "Attack004_TurningSlash", 0 },
	},
}

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Attack001_Bash", rate = 20, loop = 1 },
}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "CustomAction1", rate = 25, loop = 1, cooltime = 12000 },
   { action_name = "Assault", rate = 25, loop = 1, cooltime = 12000 },
}

g_Lua_Assault = { 
   { action_name = "Attack003_JumpAttack", rate = 10, loop = 1, approach = 250 },
   { action_name = "Attack001_Bash", rate = 10, loop = 1, approach = 250 },
}

g_Lua_Skill = { 
--next_lua_skill_index
	-- Attack006_EyeofElectric
	{ skill_index = 500147,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 800, target = 3, },

--Ai
	-- Attack005_StunShout
	{ skill_index = 500146,  cooltime = 32000, rate = 60,rangemin = 0, rangemax = 800, target = 3, encountertime = 9000, selfhppercentrange = "61,90" },
	{ skill_index = 500146,  cooltime = 32000, rate = 60,rangemin = 0, rangemax = 800, target = 3, encountertime = 30000, selfhppercentrange = "0,60", next_lua_skill_index = 0 },
	-- Attack007_HeavyDash
	{ skill_index = 500148,  cooltime = 18000, rate = 120, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,60"  },
}
