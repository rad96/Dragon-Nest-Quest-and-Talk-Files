--AiTank_Red_Normal.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
	{ action_name = "Stand_1", rate = 16, loop = 1 },
	{ action_name = "Walk_Left", rate = 8, loop = 3 },
	{ action_name = "Walk_Right", rate = 8, loop = 3 },
	{ action_name = "Walk_Back", rate = 8, loop = 1 },
	{ action_name = "Attack5_Swing", rate = 8, loop = 1, cooltime = 3000 },

}

g_Lua_Near2 = { 
	{ action_name = "Stand_1", rate = 5, loop = 1 },
	{ action_name = "Walk_Front", rate = 10, loop = 1 },
	{ action_name = "Walk_Left", rate = 5, loop = 3 },
	{ action_name = "Walk_Right", rate = 5, loop = 3 },

}

g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 500383,  cooltime = 8000, rate = 30, rangemin = 0, rangemax = 800, target = 3 },

}
