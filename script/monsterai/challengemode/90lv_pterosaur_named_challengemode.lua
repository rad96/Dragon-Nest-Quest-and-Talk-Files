-- 90lv_Pterosaur_Named_ChallengeMode.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Turn_Left", rate = 1, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 1, loop = -1, td = "RF,RB,BR" },
   { action_name = "Attack03_TailAttack", rate = 15, loop = 1, td = "BL,BR" },

}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 4, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 4, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },

}

g_Lua_Assault = { 
   { action_name = "Attack01_Slash", rate = 8, loop = 1, approach = 400 },
   { action_name = "Stand", rate = 2, loop = 1, approach = 400 },

}

g_Lua_GlobalCoolTime1 = 15000

g_Lua_Skill = { 
-- Ai 
	-- Attack01_Slash
	{ skill_index = 500161, cooltime = 6000, rate = 50, rangemin = 0, rangemax = 400, target = 3, td = "FR,FL" },
	-- Attack02_Combo
	{ skill_index = 500162, cooltime = 12000, rate = 60, rangemin = 0, rangemax = 600, target = 3, td = "FR,FL" },
	-- Attack05_Breath
	{ skill_index = 500163, cooltime = 26000, rate = 100, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,70" },
	-- Attack07_Shout_Start
	{ skill_index = 500164,  cooltime = 38000, rate = 200, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,85" },

}
