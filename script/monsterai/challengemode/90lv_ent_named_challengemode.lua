-- 90lv_Ent_Named_ChallengeMode.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 350;
g_Lua_NearValue2 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 6000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 2, loop= 1 },
	{ action_name = "Walk_Left", rate = 1, loop= 1 },
	{ action_name = "Walk_Right", rate = 1, loop= 1 },
	{ action_name = "Attack01_RightHand_MistNest", rate = 19, loop = 1, cooltime = 6000 },

}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 1, loop = 1 },
	{ action_name = "Walk_Front", rate = 5, loop = 1 },
	{ action_name = "Assault", rate = 22, loop = 1, cooltime = 6000 },

}

g_Lua_Assault = { 
   { action_name = "Attack01_RightHand_MistNest", rate = 13, approach = 350 },
}

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Skill = { 
-- Ai
	-- Attack02_StainedFootStep_MistNest
	{ skill_index = 500172,  cooltime = 24000, rate = 200, rangemin = 0, rangemax = 300, target = 3, encountertime = 15000, selfhppercentrange = "0,80" },
	-- Attack08_LineAttack_Start
	{ skill_index = 500173,  cooltime = 36000, rate = 400, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,70" },
 	-- Attack04_ClodThrow_MistNest
	{ skill_index = 500171,  cooltime = 18000, rate = 200, rangemin = 150, rangemax = 800, target = 3, encountertime = 30000 },

}
