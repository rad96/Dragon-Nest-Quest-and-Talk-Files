--AiMinotauros_Red_Boss_Nest.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 20000 -- ����&�������.

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 }, 
   { action_name = "Attack001_Bash", rate = 18, loop = 1 },
   { action_name = "Attack003_Chop", rate = 20, loop = 1 },

}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 35, loop = 1 }, 

}

g_Lua_Skill = { 
-- Ai
	-- Attack002_BigBash
	{ skill_index = 500121, cooltime = 18000, rate = 100, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,90" },
	-- Attack008_Shout
	{ skill_index = 500122, cooltime = 48000, rate = 150, rangemin = 200, rangemax = 800, target = 3, selfhppercentrange = "0,80" },
	-- Attack011_Nandong_Start
	{ skill_index = 500123, cooltime = 24000, rate = 100, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,70" },

}