--90lv_MuscleDuckKing_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Back", rate = 3, loop = 1, cooltime = 8000 },
	{ action_name = "Attack1_Punch", rate = 6, loop = 1, td = "FL,LF", cooltime = 8000 },
	{ action_name = "Attack17_RightHand", rate = 15, loop = 1, td = "FR,FL,RF", cooltime = 8000 },
	{ action_name = "Attack5_HipAttack", rate = 20, loop = 1, td = "BL,BR,RB,LB", cooltime = 10000 },

}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Front", rate = 3, loop = 1 },
	{ action_name = "Attack17_RightHand", rate = 12, loop = 1, td = "FR,FL,RF", cooltime = 10000 },
	{ action_name = "Attack1_Punch", rate = 12, loop = 1, td = "FL,LF", cooltime = 8000 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 3, loop = 1 },
	{ action_name = "Walk_Front", rate = 5, loop = 1 },
	{ action_name = "Move_Front", rate = 12, loop = 2 },
}

g_Lua_GlobalCoolTime1 = 30000

g_Lua_Skill = { 

-- next_lua_skill_index
	-- 0: Attack2_Wave
	{ skill_index = 500302, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3 },	
 
--Ai
	-- Attack22_Combo
	{ skill_index = 500301, cooltime = 22000, rate = 50, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "61,95" },	
	{ skill_index = 500301, cooltime = 26000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 600, target = 3, encountertime = 18000, selfhppercentrange = "0,60", next_lua_skill_index = 0 },
	-- Attack2_Wave
	{ skill_index = 500302, cooltime = 30000, rate = 50, rangemin = 200, rangemax = 1000, target = 3, selfhppercentrange = "61,98" },
	{ skill_index = 500302, cooltime = 30000, globalcooltime = 1, rate = 50, rangemin = 200, rangemax = 1000, target = 3, encountertime = 18000, selfhppercentrange = "0,60" },
	-- Attack14_BigJump_LotusMarsh
	{ skill_index = 500303, cooltime = 42000, rate = 50, rangemin = 0, rangemax = 1000, target = 3, selfhppercentrange = "0,80" },
	-- Attack13_BigEarthQuake_LotusMarsh
	{ skill_index = 500304, cooltime = 54000, rate = 150, rangemin = 0, rangemax = 1000, target = 3, selfhppercentrange = "0,75" },	
	-- Attack6_SpinAttack_SeaDragon
	{ skill_index = 500305, cooltime = 32000, rate = 50, rangemin = 0, rangemax = 800, target = 3, encountertime = 2000, selfhppercentrange = "51,90" },	
	{ skill_index = 500305, cooltime = 30000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 800, target = 3, encountertime = 32000, selfhppercentrange = "0,50", next_lua_skill_index = 0 },	
	
}
