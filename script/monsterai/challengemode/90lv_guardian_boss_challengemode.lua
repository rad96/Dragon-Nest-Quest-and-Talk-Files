--90lv_guardian_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 200.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 1200.0;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 3, loop = 1 },
	{ action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 5000 },	
	{ action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 5000 },
	{ action_name = "Walk_Back", rate = 3, loop = 1, cooltime = 5000 },
	{ action_name = "Walk_Front", rate = 2, loop = 1 },
	{ action_name = "Attack01_Bash", rate = 6, loop = 1, cooltime = 8000 },
	{ action_name = "Attack12_Judgment_Shoot", rate = 10, loop = 1, td = "FL,FR,LF,RF", cooltime = 24000 },
	{ action_name = "Attack12_Judgment_Back", rate = 8, loop = 1, td = "BR,BL", cooltime = 20000 },
	{ action_name = "Attack03_Tail", rate = 5, loop = 1, td = "BR,BL", cooltime = 8000 },

}	

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Walk_Front", rate = 3, loop = 1 },
	{ action_name = "Walk_Left", rate = 2, loop = 1 },	
	{ action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 5000 },
	{ action_name = "Attack01_Bash", rate = 4, loop = 1, cooltime = 50000 },
	{ action_name = "Attack12_Judgment_Front", rate = 6, loop = 1, cooltime = 20000, td = "FL,FR", selfhppercentrange = "0,80" },
	{ action_name = "Attack12_Judgment_Left", rate = 6, loop = 1, cooltime = 20000, td = "LF,LB", selfhppercentrange = "0,80" },
	{ action_name = "Attack12_Judgment_Right", rate = 6, loop = 1, cooltime = 20000, td = "RF,RB", selfhppercentrange = "0,80" },

}	

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Move_Front", rate = 5, loop = 1 },
	{ action_name = "Walk_Left", rate = 1, loop = 1 },	
	{ action_name = "Walk_Right", rate = 1, loop = 1 },
	{ action_name = "Attack12_Judgment_Shoot", rate = 5, loop = 1, td = "FL,FR,LF,RF", cooltime = 12000 },

}	

g_Lua_Skill = { 
-- next_lua_skill_index
	-- 0: Attack04_Combo_Step2
	{ skill_index = 500284, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3 },
	-- 1: Attack04_Combo_Step1
	{ skill_index = 500283, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3 },

-- AI
	-- Attack14_DoubleBomb
	{ skill_index = 500281, cooltime = 32000, rate = 30, rangemin = 0, rangemax = 800, target = 3, encountertime = 15000, selfhppercentrange = "61,100", multipletarget = "1,4,0,1" },
	{ skill_index = 500281, cooltime = 38000, rate = 30, rangemin = 0, rangemax = 800, target = 3, encountertime = 18000, selfhppercentrange = "0,60", next_lua_skill_index = 0 },
	-- Attack05_Spear
	{ skill_index = 500282, cooltime = 28000, rate = 50, rangemin = 300, rangemax = 1200, target = 3, encountertime = 20000, selfhppercentrange = "61,100" },
	{ skill_index = 500282, cooltime = 32000, rate = 50, rangemin = 300, rangemax = 1200, target = 3, encountertime = 20000, selfhppercentrange = "0,60", next_lua_skill_index = 1 },
	-- Attack04_Combo_Step1
	{ skill_index = 500283, cooltime = 24000, rate = 50, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,90" },
	-- Attack04_Combo_Step2
	{ skill_index = 500284, cooltime = 24000, rate = 50, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,95" },
	-- Attack13_ForestPrison_Start_Challenge
	{ skill_index = 500286, cooltime = 64000, rate = 50, rangemin = 0, rangemax = 1000, target = 3, selfhppercentrange = "0,60" },
}
