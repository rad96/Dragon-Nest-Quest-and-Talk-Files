--Ogre_Challenge Mode.lua

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 800;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;

g_Lua_ApproachValue = 250;
g_Lua_AssualtTime = 3000;


g_Lua_Near1 = {
	{ action_name = "Stand", rate = 5, loop = 2 },
	{ action_name = "Walk_Front", rate = 8, loop = 1 },
	{ action_name = "Walk_Left", rate = 3, loop = 2 },
	{ action_name = "Walk_Right", rate = 3, loop = 2 },
	{ action_name = "Attack1_bash", rate = 5, loop = 1 },
	{ action_name = "Attack3_Upper", rate = 5, loop = 1 },
	
}

g_Lua_Near2 = {
	{ action_name = "Stand", rate = 3, loop = 3 },
	{ action_name = "Move_Front", rate = 4, loop = 6 },
	{ action_name = "Walk_Left", rate = 2, loop = 3 },
	{ action_name = "Walk_Right", rate = 2, loop = 3 },
}

g_Lua_Near3 = {
	{ action_name = "Stand", rate = 3, loop = 3 },
	{ action_name = "Move_Front", rate = 6, loop = 3 },
	{ action_name = "Attack13_Dash", rate = 4, loop = 1 },	
}
