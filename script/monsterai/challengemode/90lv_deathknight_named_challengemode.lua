--90lv_DeathKnight_Named_ChallengeMode.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 400;
g_Lua_NearValue3 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand_1", rate = 10, loop = 1 },
	{ action_name = "Walk_Left", rate = 8, loop = 1 },
	{ action_name = "Walk_Right", rate = 8, loop = 1 },
	{ action_name = "Walk_Back", rate = 4, loop = 1 },
	{ action_name = "Attack1_Combo", rate = 16, loop = 1, cooltime = 8000 },
}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 4, loop = 1 },
	{ action_name = "Walk_Front", rate = 12, loop = 1 },
	{ action_name = "Move_Front", rate = 12, loop = 1 },
	{ action_name = "Assault", rate = 16, loop = 1  },
	{ action_name = "Attack1_Combo", rate = 16, loop = 1, cooltime = 8000 },
}

g_Lua_Assault = { 
	{ action_name = "Stand_1", rate = 8, loop = 1, approach = 300 },
	{ action_name = "Walk_Left", rate = 4, loop = 1, approach = 300 },
	{ action_name = "Walk_Right", rate = 4, loop = 1, approach = 300 },
	{ action_name = "Attack1_Combo", rate = 16, loop = 1, approach = 300 },
	{ action_name = "Attack3_LineDrive", rate = 18, loop = 1, approach = 300, selfhppercentrange = "0,90" },
}

g_Lua_Skill = { 
-- Ai
	-- Attack2_CycloneSlash
	{ skill_index = 500111, cooltime = 24000, rate = 100, rangemin = 0, rangemax = 800, target = 3, randomtarget = "1.6,0,1" , selfhppercentrange = "0,85" },
	-- Attack3_LineDrive
	{ skill_index = 500112, cooltime = 36000, rate = 150, rangemin = 200, rangemax = 800, target = 3, selfhppercentrange = "0,90" },
	-- Attack4_Sword
	{ skill_index = 500113, cooltime = 45000, rate = 300, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,70" },

}
