--AiDragonSycophantMagician_Blue_Elite_Normal.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1200;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 4000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Walk_Left", rate = 3, loop = 2 },
	{ action_name = "Walk_Right", rate = 3, loop = 2 },
	{ action_name = "Move_Back", rate = 2, loop = 2 },

}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 7, loop = 1 },
	{ action_name = "Walk_Left", rate = 4, loop = 2 },
	{ action_name = "Walk_Right", rate = 4, loop = 2 },
	{ action_name = "Move_Front", rate = 5, loop = 1 },
}

g_Lua_Skill = { 
	{ skill_index = 500392,  cooltime = 12000, rate = 80, rangemin = 0, rangemax = 600, target = 3 },
	{ skill_index = 500393,  cooltime = 22000, rate = 80, rangemin = 0, rangemax = 800, target = 3 },

}
