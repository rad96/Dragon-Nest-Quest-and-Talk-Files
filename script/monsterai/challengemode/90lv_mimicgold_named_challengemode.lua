-- 90lv_Mimicgold_Named_ChallengeMode.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack01_RushBite", rate = 22, loop = 1, cooltime = 6000 },
   { action_name = "Attack02_DownBite", rate = 32, loop = 1 },

}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Attack01_RushBite", rate = 30, loop = 1, max_missradian = 15, cooltime = 6000 },
   { action_name = "Attack04_SpectrumShower", rate = 100, loop = 1, cooltime = 24000, selfhppercentrange = "0,80" },

}

g_Lua_Skill = { 
-- Ai
	-- Attack05_GoldCannon_Start
	{ skill_index = 500127,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 800, target = 3, encountertime = 10000 },
	--Attack06_GoldRain_Start
	{ skill_index = 500126,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,60" },

}
