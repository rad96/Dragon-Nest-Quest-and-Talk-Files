-- 90lv_HalfGolem_Normal_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1300;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 20, loop = 1 },
	{ action_name = "Walk_Left", rate = 5, loop = 1, },
	{ action_name = "Walk_Right", rate = 5, loop = 1, },
	{ action_name = "Walk_Back", rate = 5, loop = 1, },
	{ action_name = "Attack01_LinearPunch_MTnest", rate = 15, loop = 1, },
	{ action_name = "Attack02_Shortblow_MTnest", rate = 25, loop = 1, },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 15, loop = 1 },	
	{ action_name = "Walk_Front", rate = 8, loop = 1, },
	{ action_name = "Walk_Left", rate = 6, loop = 1, },
	{ action_name = "Walk_Right", rate = 6, loop = 1, },
	{ action_name = "Attack01_LinearPunch_MTnest", rate = 15, loop = 1, },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 8, loop = 1 },	
	{ action_name = "Move_Front", rate = 15, loop = 1, },

}
