--90lv_ProfessorKRed_Named_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1200;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand_1", rate = 8, loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Move_Back", rate = 5, loop = 1, custom_state1 = "custom_ground", cooltime = 6000 },
	{ action_name = "Walk_Left", rate = 3, loop = 1, custom_state1 = "custom_ground", cooltime = 5000 },
	{ action_name = "Walk_Right", rate = 3, loop = 1, custom_state1 = "custom_ground", cooltime = 3000 },
	{ action_name = "Attack01_ChainSaw", rate = 19, custom_state1 = "custom_ground", cooltime = 5000 },

}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 8, loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Walk_Left", rate = 3, loop = 1, custom_state1 = "custom_ground", cooltime = 3000 },
	{ action_name = "Walk_Right", rate = 3, loop = 1, custom_state1 = "custom_ground", cooltime = 5000 },
   { action_name = "Attack01_ChainSaw", rate = 19, custom_state1 = "custom_ground" ,  cooltime = 5000 },

}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 7, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Front", rate = 10, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Assault", rate = 19, loop = 1, custom_state1 = "custom_ground" },

}

g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 8, loop = 1, approach = 200, custom_state1 = "custom_ground" },
   { action_name = "Attack01_ChainSaw", rate = 19, loop = 1, approach = 200, custom_state1 = "custom_ground" },
}

g_Lua_Skill = { 
-- Ai
	-- Attack02_ChainSawRush
	{ skill_index = 500106,  cooltime = 18000, rate = 50, rangemin = 100, rangemax = 800, target = 3, randomtarget = 1.1, custom_state1 = "custom_ground", selfhppercentrange = "0,90" },
	-- Attack03_OpenCover_ElectricBall
	{ skill_index = 500107,  cooltime = 28000, rate = 100, rangemin = 100, rangemax = 800, target = 3, custom_state1 = "custom_ground", encountertime = 10000, selfhppercentrange = "0,80" },
	-- Attack05_Mortar
	{ skill_index = 500108,  cooltime = 48000, rate = 150, rangemin = 0, rangemax = 800, target = 3, eencustom_state1 = "custom_ground", selfhppercentrange = "0,70" },
	-- Attack03_OpenCover_Poison
	{ skill_index = 500109,  cooltime = 36000, rate = 100, rangemin = 100, rangemax = 800, target = 3, custom_state1 = "custom_ground", encountertime = 10000, selfhppercentrange = "0,60" },

}
