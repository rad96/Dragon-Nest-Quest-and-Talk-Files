--90lv_Karahan_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Move_Left", rate = 6, loop = 1, td = "RF,RB", cooltime = 6000 },
	{ action_name = "Move_Right", rate = 6, loop = 1, td = "LF,LB", cooltime = 6000 },
	{ action_name = "Move_Back", rate = 6, loop = 1, td = "FR,FL", cooltime = 5000 },
}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Move_Left", rate = 6, loop = 1, td = "RF,RB", cooltime = 6000 },
	{ action_name = "Move_Right", rate = 6, loop = 1, td = "LF,LB", cooltime = 6000 },
	{ action_name = "Walk_Front", rate = 5, loop = 1 },

}

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Move_Left", rate = 5, loop = 1, cooltime = 8000 },
	{ action_name = "Move_Right", rate = 5, loop = 1, cooltime = 8000 },
	{ action_name = "Move_Front", rate = 3, loop = 1 },

}

g_Lua_GlobalCoolTime1 = 48000 --_G1 : fly Skill 

g_Lua_Skill = { 
-- next_lua_skill_index
	-- 0: Attack11_FireBall_Start
	{ skill_index = 500341, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3 },	
	-- 1: Attack12_FreezingSpike_Start
	{ skill_index = 500342, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, randomtarget = "1.6,0,1", target = 3 },	
 	
--Ai
	--Attack01_Push
	{ skill_index = 500331, cooltime = 12000, rate = 30, rangemin = 0, rangemax = 200, target = 3, selfhppercentrange = "71,100" },	
	--Attack01_Push_Challenge
	{ skill_index = 500332, cooltime = 16000, rate = 30, rangemin = 0, rangemax = 300, target = 3, encountertime = 12000, selfhppercentrange = "0,70" },	
	--Attack02_MagicMissile
	{ skill_index = 500333, cooltime = 20000, rate = 30, rangemin = 0, rangemax = 400, target = 3, encountertime = 12000 },	
	--Attack03_FlameWorm
	{ skill_index = 500334, cooltime = 26000, rate = 30, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,85" },	
	--Attack02_IceNeedle
	{ skill_index = 500335, cooltime = 20000, rate = 30, rangemin = 300, rangemax = 800, target = 3, selfhppercentrange = "0,95" },	
	--Attack04_RollingLava
	{ skill_index = 500336, cooltime = 38000, rate = 50, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,70" },	
	--Attack04_IceWave
	{ skill_index = 500337, cooltime = 52000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 1200, target = 3, encountertime = 2000, selfhppercentrange = "51,80", next_lua_skill_index = 0, limitcount = 1 },	
	{ skill_index = 500337, cooltime = 52000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 1200, target = 3, encountertime = 2000, selfhppercentrange = "31,40", next_lua_skill_index = 0, limitcount = 1 },
	{ skill_index = 500337, cooltime = 52000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 1200, target = 3, encountertime = 2000, selfhppercentrange = "11,20", next_lua_skill_index = 0, limitcount = 1 },
	{ skill_index = 500337, cooltime = 52000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 1200, target = 3, encountertime = 2000, selfhppercentrange = "0,10", next_lua_skill_index = 0 },

	--Attack06_GlacialWave
	{ skill_index = 500338, cooltime = 52000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 800, target = 3, encountertime = 5000, selfhppercentrange = "61,90", next_lua_skill_index = 1, limitcount = 1 },	
	{ skill_index = 500338, cooltime = 52000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 1200, target = 3, encountertime = 2000, selfhppercentrange = "41,50", next_lua_skill_index = 1, limitcount = 1 },
	{ skill_index = 500338, cooltime = 52000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 1200, target = 3, encountertime = 2000, selfhppercentrange = "21,30", next_lua_skill_index = 1, limitcount = 1 },
	{ skill_index = 500338, cooltime = 52000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 1200, target = 3, encountertime = 2000, selfhppercentrange = "0,10", next_lua_skill_index = 1 },

	--Attack05_IceSword_Start
	{ skill_index = 500339, cooltime = 42000, rate = 80, rangemin = 500, rangemax = 1200, target = 3, encountertime = 3000, selfhppercentrange = "0,75" },	
	--Attack13_BlackHole_Start
	{ skill_index = 500340, cooltime = 78000, rate = 50, rangemin = 300, rangemax = 800, target = 3, selfhppercentrange = "0,85" },	
	
}
