--90lv_TypoonKim_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 350;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 100
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Back", rate = 3, loop = 1, cooltime = 5000 },
	{ action_name = "Attack001_ArmedPush", rate = 10, loop = 1, cooltime = 10000 },
	{ action_name = "Attack010_Armed3combo", rate = 12, loop = 1, cooltime = 13000 },
	{ action_name = "Attack007_Stomp", rate = 8, loop = 1, cooltime = 12000 },

}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Left", rate = 3, loop = 1 },
	{ action_name = "Walk_Right", rate = 3, loop = 1 },
	{ action_name = "Move_Front", rate = 5, loop = 2, selfhppercentrange = "0,60" },
	{ action_name = "Attack012_Chop", rate = 10, loop = 1, cooltime = 8000 },
	{ action_name = "Attack003_FullSwing", rate = 12, loop = 1, cooltime = 8000, },


}

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Left", rate = 6, loop = 1 },
	{ action_name = "Walk_Right", rate = 6, loop = 1 },
	{ action_name = "Move_Front", rate = 12, loop = 2 },
	{ action_name = "Attack013_JumpChop", rate = 8, loop = 1, cooltime = 18000, selfhppercentrange = "61,100" },
	{ action_name = "Attack013_JumpChop_Lightning", rate = 15, loop = 1, cooltime = 18000, selfhppercentrange = "0,60" },
	{ action_name = "Attack005_GreatBomb_Lightning", rate = 10, loop = 1, cooltime = 14000, selfhppercentrange = "0,60" },
}

g_Lua_GlobalCoolTime1 = 26000
g_Lua_GlobalCoolTime2 = 48000

g_Lua_Skill = { 
-- next_lua_skill_index
	-- 0: Attack007_Stomp_Lightning
	{ skill_index = 500322, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1200, target = 3 },	
	-- 1: Attack007_Stomp_Lightning
	{ skill_index = 500322, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1200, target = 3, next_lua_skill_index = 2 },	
	-- 2: Attack012_Chop_Lightning
	{ skill_index = 500323, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1200, target = 3 },	
	-- 3: Attack007_Stomp_Lightning
	{ skill_index = 500322, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1200, target = 3, next_lua_skill_index = 4 },	
	-- 4: Attack012_Chop_Lightning
	{ skill_index = 500323, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1200, target = 3, next_lua_skill_index = 5 },	
	-- 5: Attack013_JumpChop_Lightning
	{ skill_index = 500324, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1200, target = 3 },	

--Ai
	-- Attack016_Stare
	{ skill_index = 500321, cooltime = 26000, rate = 30, rangemin = 100, rangemax = 600, target = 3, encountertime = 28000, selfhppercentrange = "71,95", next_lua_skill_index = 0 },	
	{ skill_index = 500321, cooltime = 24000, globalcooltime = 1, rate = 30, rangemin = 100, rangemax = 600, target = 3, encountertime = 26000, selfhppercentrange = "46,71", next_lua_skill_index = 1 },	
	{ skill_index = 500321, cooltime = 20000, globalcooltime = 1,rate = 30, rangemin = 100, rangemax = 600, target = 3, encountertime = 24000, selfhppercentrange = "0,45", next_lua_skill_index = 3 },	
	-- Attack002_Combo_Lightning
	{ skill_index = 500325, cooltime = 26000, globalcooltime = 1, rate = 30, rangemin = 0, rangemax = 300, target = 3, selfhppercentrange = "0,51", randomtarget = "1.6,0,1" },	
	-- Attack008_ShockBreak
	{ skill_index = 500326, cooltime = 42000, rate = 150, rangemin = 0, rangemax = 1200, target = 3, selfhppercentrange = "70,80", limitcount = 1 },
	{ skill_index = 500326, cooltime = 42000, rate = 150, rangemin = 0, rangemax = 1200, target = 3, selfhppercentrange = "40,55", limitcount = 1 },	
	{ skill_index = 500326, cooltime = 42000, rate = 150, rangemin = 0, rangemax = 1200, target = 3, selfhppercentrange = "0,20", limitcount = 1 },	
	-- Attack004_StraightArm_Lightning
	{ skill_index = 500327, cooltime = 36000, rate = 50, rangemin = 100, rangemax = 800, target = 3, encountertime = 30000, selfhppercentrange = "0,85" },	
	-- Attack014_WhirlWindLight_Start
	{ skill_index = 500328, cooltime = 48000, rate = 50, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,70", randomtarget = "1.6,0,1" },	
	-- Attack018_SlideCharge_Start
	{ skill_index = 500328, cooltime = 52000, rate = 50, rangemin = 150, rangemax = 1200, target = 3, selfhppercentrange = "0,80" },	

}