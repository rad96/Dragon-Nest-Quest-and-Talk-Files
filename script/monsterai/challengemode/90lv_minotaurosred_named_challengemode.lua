--90lv_MinotaurosRed_Named_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 3, cooltime = 5000 },
   { action_name = "Walk_Right", rate = 2, loop = 3, cooltime = 5000 },
   { action_name = "Walk_Back", rate = 1, loop = 2 },
   { action_name = "Attack4_Pushed_Red", rate = 12, loop = 1, cooltime = 12000 },
	{ action_name = "Attack1_bash_Red", rate = 12, loop = 1, cooltime = 6000  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 3, cooltime = 5000 },
   { action_name = "Walk_Right", rate = 3, loop = 3, cooltime = 5000 },
   { action_name = "Attack2_Slash_Red", rate = 16, loop = 1, cooltime = 12000 },
   { action_name = "Attack1_bash_Red", rate = 16, loop = 1, cooltime = 6000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 35, loop = 1 },
   { action_name = "Assault", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
   { action_name = "Attack4_Pushed_Red", rate = 8, loop = 1, approach = 250.0 },
}

g_Lua_Skill = { 
-- Ai
	-- Attack8_Stamp_Red
	{ skill_index = 500101, cooltime = 24000, rate = 50, rangemin = 100, rangemax = 500, target = 3, selfhppercentrange = "0,90" },
	-- Attack6_Howl_Red
	{ skill_index = 500102, cooltime = 48000, rate = 120, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,80" },
	-- Attack3_DashAttack_Red1
	{ skill_index = 500103, cooltime = 36000, rate = 100, rangemin = 200, rangemax = 600, target = 3, encountertime = 15000 },
	--Attack9_BigBash_Red
	{ skill_index = 500104, cooltime = 16000, rate = 300, rangemin = 0, rangemax = 400, target = 3, encountertime = 24000, selfhppercentrange = "0,70" },

}