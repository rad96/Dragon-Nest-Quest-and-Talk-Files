--90lv_ManticoreBlack_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 700.0;
g_Lua_NearValue3 = 1200.0;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;


g_Lua_Near1 = { 
-- HP : 61~100
	{ action_name = "Stand", rate = 12, loop = 1 },
	{ action_name = "Walk_Back", rate = 3, loop = 1, td = "FL,FR,LF,RF", cooltime = 8000 },
	{ action_name = "Attack11_LHook_Lord_Normal", rate = 8, loop = 1, td = "FL,LF", cooltime = 6000, selfhppercentrange = "61,100"  },
	{ action_name = "Attack11_RHook_Lord_Normal", rate = 8, loop = 1, td = "FR,RF", cooltime = 6000, selfhppercentrange = "61,100"  },
	{ action_name = "Attack11_LHook_Lord", rate = 8, loop = 1, td = "FL,LF", cooltime = 8000, selfhppercentrange = "0,60"  },
	{ action_name = "Attack11_RHook_Lord", rate = 8, loop = 1, td = "FR,RF", cooltime = 8000, selfhppercentrange = "0,60"  },
	{ action_name = "Attack04_Tail_L_Lord", rate = 12, loop = 1, td = "BL,LB", cooltime = 12000, randomtarget = "1.6,0,1" },
	{ action_name = "Attack05_Tail_R_Lord", rate = 12, loop = 1, td = "BR,RB", cooltime = 12000, randomtarget = "1.6,0,1" },

}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 12, loop = 1 },
	{ action_name = "Turn_Left", rate = 2, loop = 1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 2, loop = 1, td = "RF,RB,BR" },
	{ action_name = "Walk_Front", rate = 5, loop = 1, td = "FL,FR,LF,RF", cooltime = 5000 },
	{ action_name = "Attack11_LHook_Lord_Normal", rate = 10, loop = 1, td = "FL,LF", cooltime = 8000, selfhppercentrange = "61,100" },
	{ action_name = "Attack11_RHook_Lord_Normal", rate = 10, loop = 1, td = "FR,RF", cooltime = 8000, selfhppercentrange = "61,100" },
	{ action_name = "Attack11_LHook_Lord", rate = 10, loop = 1, td = "FL,LF", cooltime = 8000, selfhppercentrange = "0,60" },
	{ action_name = "Attack11_RHook_Lord", rate = 10, loop = 1, td = "FR,RF", cooltime = 8000, selfhppercentrange = "0,60" },

}

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 12, loop = 1 },
	{ action_name = "Move_Front", rate = 8, loop = 1, td = "FL,FR,LF,RF", cooltime = 5000 },

}

g_Lua_GlobalCoolTime1 = 40000 	-- _G1: RaiseGravity_NestBossRush cooltime
g_Lua_GlobalCoolTime2 = 60000 	-- _G2: Spectrum cooltime

g_Lua_Skill = { 
-- next_lua_skill_index
	-- 0: Attack01_BigGravityBall
	{ skill_index = 500231, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	-- 1: Attack06_GravityThorn
	{ skill_index = 500236, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	-- 2: Attack13_Spectrum
	{ skill_index = 500237, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },

-- AI
	-- Attack01_BigGravityBall
	{ skill_index = 500231, cooltime = 26000, rate = 30, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR,LF,RF", encountertime = 7000, selfhppercentrange = "61,90" },
	{ skill_index = 500231, cooltime = 24000, rate = 30, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR,LF,RF", encountertime = 20000, selfhppercentrange = "0,60" },

	-- Attack03_HeavyStamp
	{ skill_index = 500232, cooltime = 32000, rate = 30, rangemin = 0, rangemax = 800, target = 3, encountertime = 7000, selfhppercentrange = "61,95" },
	{ skill_index = 500232, cooltime = 30000, rate = 30, rangemin = 0, rangemax = 800, target = 3, encountertime = 15000, selfhppercentrange = "0,60", next_lua_skill_index = 2 },

	-- Attack14_RaiseGravity_NestBossRush1
	{ skill_index = 500233, cooltime = 40000, globalcooltime = 1, rate = 250, rangemin = 0, rangemax = 800, target = 3, td = "BL,BR,LB,RB", selfhppercentrange = "0,70" },
	-- Attack14_RaiseGravity_NestBossRush2
	{ skill_index = 500234, cooltime = 40000, globalcooltime = 1, rate = 250, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "0,70"},
	-- Attack02_BlackHole
	{ skill_index = 500235, cooltime = 48000, rate = 30, rangemin = 600, rangemax = 1000, target = 3, td = "FL,FR", encountertime = 12000 },
	-- Attack13_Spectrum	
	{ skill_index = 500237, cooltime = 56000, globalcooltime = 2, rate = 30, rangemin = 300, rangemax = 800, target = 3, randomtarget = "1.6,0,1",selfhppercentrange = "61,100" },

}