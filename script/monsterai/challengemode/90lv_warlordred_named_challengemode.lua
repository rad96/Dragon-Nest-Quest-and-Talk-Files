-- 90lv_WarlordRed_Named_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 50;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
   { action_name = "Attack01_Punch", rate = 12, loop = 1, cooltime = 6000 },
   { action_name = "Attack02_Combo", rate = 6, loop = 1, cooltime = 12000 },
}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack01_Punch", rate = 8, loop = 1, cooltime = 6000 },
   { action_name = "Attack02_Combo", rate = 12, loop = 1, cooltime = 12000 },

}

g_Lua_Near3 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Attack04_Stomp_Long", rate = 60, loop = 1, cooltime = 8000 },
   { action_name = "Attack06_ShotBullet", rate = 73, loop = 1, cooltime = 12000 },

}


g_Lua_Skill = { 
-- Ai	
	-- Attack08_RollingBomb_Start
	{ skill_index = 500141,  cooltime = 28000, rate = 100, rangemin = 100, rangemax = 800, target = 3, selfhppercentrange = "0,70" }, 
	-- Attack08_FlameShower_Start
	{ skill_index = 500142,  cooltime = 32000, rate = 150, rangemin = 0, rangemax = 800, target = 3, encountertime = 25000 },
	-- Attack06_ShotNapalm
	{ skill_index = 500143,  cooltime = 16000, rate = 180, rangemin = 0, rangemax = 800, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "0,90" },

}
