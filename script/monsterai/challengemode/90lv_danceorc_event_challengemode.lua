--90lv_DanceOrc_Event_ChallengeMode.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 8000;
g_Lua_NearValue2 = 10000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_Skill = { 
-- ai
	--Attack1_DanceBuff_90lv_Challenge_Start
   { skill_index = 500401, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 30000, target = 3 },
}
