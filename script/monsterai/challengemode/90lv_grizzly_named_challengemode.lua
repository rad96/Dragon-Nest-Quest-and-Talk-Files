-- 90lv_Grizzly_Named_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 350;
g_Lua_NearValue2 = 650;
g_Lua_NearValue3 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 350
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 8000

g_Lua_CustomAction = {
	CustomAction1 = {
		{ "Attack03_Kick", 0 },
		{ "Attack01_RightHand", 0 },
	},
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Back", rate = 6, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
   { action_name = "Attack01_Bash", rate = 22, loop = 1, td = "FR,FL", cooltime = 6000 },
   { action_name = "Attack02_Crawling", rate = 25, loop = 1, td = "FR,FL", cooltime = 8000 },
   { action_name = "Attack09_Combo", rate =30, loop = 1, td = "FR,FL", cooltime = 10000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 3, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
   { action_name = "Attack01_Bash", rate = 22, loop = 1, td = "FR,FL", cooltime = 10000 },
   { action_name = "Attack02_Crawling", rate = 30, loop = 1, td = "FR,FL", cooltime = 10000 },
   { action_name = "Attack05_SweepBite", rate = 35, loop = 1, randomtarget = "1.6,0,1", globalcooltime = 1, td = "FR,FL" },
   { action_name = "Attack09_Combo", rate = 30, loop = 1, td = "FR,FL", cooltime = 12000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1, td = "FR,FL" },
   { action_name = "Move_Front", rate = 7, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 13, loop = 1, randomtarget = "1.6,0,1", globalcooltime = 1, td = "FR,FL" },
}

g_Lua_Assault = { 
   { action_name = "Attack01_Bash", rate = 13, approach = 400.0,td = "FR,FL" },

}

g_Lua_Skill = { 
	-- Attack03_BeastHowl_Start
	{ skill_index = 500156,  cooltime = 38000, rate = 120, rangemin = 100, rangemax = 800, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "0,60" },
	-- Attack06_BattleDash_Start
	{ skill_index = 500157,  cooltime = 30000, rate = 80, rangemin = 200, rangemax = 800, target = 3, encountertime = 10000 },
	-- Attack04_QuakeAttack_Start
	{ skill_index = 500158,  cooltime = 24000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, randomtarget = "1.5,0,1", encountertime = 15000, selfhppercentrange = "0,70" },
}
