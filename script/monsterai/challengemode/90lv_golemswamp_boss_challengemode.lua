--90lv_MuscleDuckKing_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Back", rate = 3, loop = 1, cooltime = 8000 },

}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Front", rate = 5, loop = 1 },

}

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },	
	{ action_name = "Walk_Left", rate = 6, loop = 1 },
	{ action_name = "Walk_Right", rate = 6, loop = 1 },
	{ action_name = "Move_Front", rate = 15, loop = 2 },

}

g_Lua_GlobalCoolTime1 = 28000

g_Lua_Skill = { 

--Ai	
	-- Attack1_Punch_Blue_Lv2
	{ skill_index = 500311, cooltime = 6000, rate = 30, rangemin = 0, rangemax = 600, target = 3, td = "FL,LF" },	
	-- Attack17_RightHand_Lv2
	{ skill_index = 500312, cooltime = 8000, rate = 30, rangemin = 0, rangemax = 500, target = 3, td = "RF,FL,LR" },	
	-- Attack5_HipAttack_Lv2
	{ skill_index = 500316, cooltime = 10000, rate = 50, rangemin = 0, rangemax = 600, target = 3, td = "BR,BL", selfhppercentrange = "0,95" },	
	-- Attack3_Breath_GreenDragon
	{ skill_index = 500315, cooltime = 46000, rate = 50, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "0,90" },	
	-- Attack6_SpinAttack_GreenDragon_Lv2
	{ skill_index = 500317, cooltime = 38000, rate = 50, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,70" },	
	-- Attack13_BigEarthQuake_SeaDragon
	{ skill_index = 500313, cooltime = 24000, globalcooltime = 1, rate = 50, rangemin = 100, rangemax = 800, target = 3, selfhppercentrange = "0,95" },	
	-- Attack8_EarthQuake_GreenDragon_Lv1
	{ skill_index = 500318, cooltime = 32000, globalcooltime = 1, rate = 50, rangemin = 100, rangemax = 600, target = 3, selfhppercentrange = "0,90" },	
	
}