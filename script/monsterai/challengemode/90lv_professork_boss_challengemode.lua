--90lv_ProfessorK_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 200.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 1200.0;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;


g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Walk_Back", rate = 2, loop = 1, cooltime = 8000 },
	{ action_name = "Attack01_RightHook", rate = 10, loop = 1, td = "FR,RF", cooltime = 6000 },
	{ action_name = "Attack02_LeftUpper", rate = 10, loop = 1, td = "FL,LF", cooltime = 8000 },
}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 6, loop = 1 },
	{ action_name = "Walk_Front", rate = 3, loop = 1 },
	{ action_name = "Attack01_RightHook", rate = 10, loop = 1, td = "FR,RF", cooltime = 8000 },
	{ action_name = "Attack02_LeftUpper", rate = 10, loop = 1, td = "FL,RF", cooltime = 6000 },
	{ action_name = "Attack03_Clap", rate = 15, loop = 1, cooltime = 18000 },
}

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Move_Front", rate = 8, loop = 2 },
}

g_Lua_GlobalCoolTime1 = 32000; --_WaveAttack Cooltime
g_Lua_GlobalCoolTime2 = 48000; --_RiderKick,HandStomp Cooltime

g_Lua_Skill = { 
-- next_lua_skill_index
	--Atttack05_RiderKick_Start
	{ skill_index = 500257, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	--Attack09_Electric_Start
	{ skill_index = 500261, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,2,0,1" },
-- AI
	--Attack10_Poison
	{ skill_index = 500256, cooltime = 18000, rate = 300, rangemin = 200, rangemax = 800, target = 3, encountertime = 15000 },
	--Attack11_WaveAttack_Start
	{ skill_index = 500259, cooltime = 42000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "51,85" },
	{ skill_index = 500259, cooltime = 42000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 800, target = 3, encountertime = 24000, selfhppercentrange = "0,50", next_lua_skill_index = 1 },
	--Atttack05_RiderKick_Start
	{ skill_index = 500257, cooltime = 28000, rate = 150, rangemin = 200, rangemax = 800, target = 3, randomtarget = "1.5,1", selfhppercentrange = "41,70" },
	--Attack07_HandStomp_Out_Start
	{ skill_index = 500260, cooltime = 36000, rate = 350, rangemin = 0, rangemax = 600, target = 3, encountertime = 24000, selfhppercentrange = "0,40", next_lua_skill_index = 0 },

}
