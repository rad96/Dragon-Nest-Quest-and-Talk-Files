--90lv_ProfessorK_Red_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 400.0;
g_Lua_NearValue3 = 800.0;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Walk_Back", rate = 3, loop = 1, cooltime = 5000 },
	{ action_name = "Attack04_EarthQuake_VolNest", rate = 12, loop = 1, cooltime = 15000, td = "FR,RF" },

}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 4, loop = 1 },
	{ action_name = "Walk_Front", rate = 5, loop = 1, cooltime = 5000 },
	{ action_name = "Attack02_LeftUpper_VolNest", rate = 12, loop = 1, cooltime = 6000, td = "FL,LF" },	
	{ action_name = "Attack01_RightHook_VolNest", rate = 12, loop = 1, cooltime = 6000, td = "FR,RF" },

}

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Move_Front", rate = 6, loop = 1 },

}

g_Lua_GlobalCoolTime1 = 32000 --_DampseyRoll_JumpAttack
g_Lua_GlobalCoolTime2 = 30000 --_Stomp_WhirlAttak

g_Lua_Skill = { 
-- next_lua_skill_index
	-- 0: Attack14_JumpAttack_VolNest
	{ skill_index = 500270, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3 },
	-- 1: Attack07_HandStompMo_VolNest_Start
	{ skill_index = 500268, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1200, target = 3 },
	-- 2: Attack04_Stomp
	{ skill_index = 500266, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },

-- AI
	-- Attack04_Stomp
	{ skill_index = 500266, cooltime = 42000, rate = 50, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "61,100" },
	{ skill_index = 500266, cooltime = 42000, globalcooltime = 2, rate = 30, rangemin = 0, rangemax = 800, target = 3, encountertime = 32000, selfhppercentrange = "0,60" },
	-- Attack06_DampseyRoll_VolNest_Start
	{ skill_index = 500267, cooltime = 24000, globalcooltime = 1, rate = 40, rangemin = 0, rangemax = 500, target = 3, encountertime = 3000, randomtarget= "0.6,1", selfhppercentrange = "41,95" },
	{ skill_index = 500267, cooltime = 24000, globalcooltime = 1, rate = 40, rangemin = 0, rangemax = 500, target = 3, encountertime = 28000, randomtarget= "0.6,1", selfhppercentrange = "0,40", next_lua_skill_index = 0 },
	-- Attack15_Howl_VolNest
	{ skill_index = 500269, cooltime = 50000, rate = 150, rangemin = 0, rangemax = 600, target = 3, encountertime = 20000, selfhppercentrange = "0,70", next_lua_skill_index = 1 },
	-- Attack14_JumpAttack_VolNest
	{ skill_index = 500270, cooltime = 35000, rate = 50, rangemin = 200, rangemax = 500, target = 3, selfhppercentrange = "41,95" },
	-- Attack13_WhirlAttack_VolNest
	{ skill_index = 500271, cooltime = 22000, rate = 30, rangemin = 100, rangemax = 600, target = 3, encountertime = 10000, selfhppercentrange = "61,100" },
	{ skill_index = 500271, cooltime = 24000, globalcooltime = 2, rate = 100, rangemin = 100, rangemax = 600, target = 3, encountertime = 18000, selfhppercentrange = "0,60", next_lua_skill_index = 2 },

}
