-- 90lv_HalfGolemIron_Named_ChallengeMode.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 10, loop = 1  },
	{ action_name = "Walk_Front", rate = 4, loop = 1  },
	{ action_name = "Walk_Left", rate = 2, loop = 1  },
	{ action_name = "Walk_Right", rate = 2, loop = 1  },
	{ action_name = "Walk_Back", rate = 4, loop = 1  },
	{ action_name = "Attack1_LightningPunch", rate = 16, loop = 1, cooltime = 5000 },
	{ action_name = "Attack2_LightningBlow", rate = 16, loop = 1, cooltime = 8000 },

}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 10, loop = 1  },
	{ action_name = "Walk_Front", rate = 3, loop = 1  },
	{ action_name = "Walk_Left", rate = 2, loop = 1  },
	{ action_name = "Walk_Right", rate = 2, loop = 1  },
	{ action_name = "Attack11_ChargeBeam", rate = 25, loop = 1, cooltime = 16000, encountertime = 15000 },
	{ action_name = "Assault", rate = 22, loop = 1  },

}

g_Lua_Assault = { 
   { action_name = "Attack1_LightningPunch", rate = 5, loop = 1, approach = 350.0  },
   { action_name = "Attack2_LightningBlow", rate = 10, loop = 1, approach = 200.0  },
}

g_Lua_Skill = { 
-- ai
	-- Attack1_LightningBall
	{ skill_index = 500131,  cooltime = 18000, rate = 50, rangemin = 0, rangemax = 600, target = 3, encountertime = 15000, selfhppercentrange = "0,99" },
	-- Attack10_FingerChainlightning_Start
	{ skill_index = 500132,  cooltime = 28000, rate = 100, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,80" },
	-- Attack8_Lightningburst
	{ skill_index = 500133,  cooltime = 38000, rate = 300, rangemin = 100, rangemax = 600, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "0,60" },

}
