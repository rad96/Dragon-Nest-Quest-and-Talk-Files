--90lv_Apocalypse_Boss_ChallengeMode
g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1200;

--g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 5000;

g_Lua_Near1 = { 
	{ action_name = "Stand_1", rate = 12, loop = 1 },
	{ action_name = "Walk_Right", rate = 3, loop = 1, cooltime = 5000 },
	{ action_name = "Walk_Left", rate = 3, loop = 1, cooltime = 5000 },
	{ action_name = "Walk_Back", rate = 5, loop = 1, cooltime = 5000 },
	{ action_name = "Attack001_NormalAttack", rate = 20, loop = 1, td = "FR,FL", cooltime = 6000 },
	{ action_name = "Attack018_Bite", rate = 16, loop = 1, td = "FR,FL,LF,RF", cooltime = 12000 },

}

g_Lua_Near2 = { 
	{ action_name = "Stand_1", rate = 12, loop = 1 },
	{ action_name = "Walk_Right", rate = 3, loop = 1, cooltime = 5000 },
	{ action_name = "Walk_Left", rate = 3, loop = 1, cooltime = 5000 },
	{ action_name = "Attack001_NormalAttack", rate = 20, loop = 1, td = "FR,FL", cooltime = 6000 },
	{ action_name = "Attack020_Stamp", rate = 25, loop = 1, td = "FR,FL", cooltime = 16000 },

}

g_Lua_Near3 = {
	{ action_name = "Stand_1", rate = 3, loop = 1 },
	{ action_name = "Walk_Front", rate = 6, loop = 1 },
	{ action_name = "Attack002_ClawRange1", rate = 20, loop = 1, td = "FR,FL,LF,LR", cooltime = 8000 },
	{ action_name = "Assault", rate = 8, loop = 1, td = "FR,FL", cooltime = 18000 },

}

g_Lua_Assault = { 
   { action_name = "Attack020_Stamp", rate = 4, loop = 1, approach = 400 },
   { action_name = "Attack018_Bite", rate = 6, loop = 1, approach = 200 },
}

-- GlobalCoolTime 
g_Lua_GlobalCoolTime1 = 30000 	-- _G1: ������ ��Ÿ��

g_Lua_Skill = { 
-- Next_lua_skill_index
	-- Attack014_Star_NestBossRush
	{ skill_index = 500207, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 800, target = 3 },
	--Attack004_StretchOut
	{ skill_index = 500204, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 800, target = 3 },
-- AI
	--Attack003_Assault
	{ skill_index = 500203, cooltime = 12000, rate = 10, rangemin = 0, rangemax = 800, target = 3, td = "FR,FL", selfhppercentrange = "0,99" },
	--Attack004_StretchOut
	{ skill_index = 500204, cooltime = 24000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 800, target = 3, td = "FR,FL", encountertime = 30000, selfhppercentrange = "61,95" },
	{ skill_index = 500204, cooltime = 24000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 800, target = 3, td = "FR,FL", encountertime = 30000, selfhppercentrange = "0,60", next_lua_skill_index = 1 },
	--Attack005_Shout
	{ skill_index = 500205, cooltime = 46000, rate = 100, rangemin = 200, rangemax = 800, target = 3, td = "FR,FL", selfhppercentrange = "0,75", next_lua_skill_index = 0 },
	--Attack009_Whirlwind_Start
	{ skill_index = 500206, cooltime = 24000, rate = 10, rangemin = 100, rangemax = 800, target = 3, selfhppercentrange = "0,80" },
	--Attack012_Apocalypse_NestBossRush_Start
	{ skill_index = 500210, cooltime = 36000, rate = 30, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,85", multipletarget = "1,2,0,1" },
	--Attack021_Spin
	{ skill_index = 500211, cooltime = 44000, rate = 10, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,95" },

}