--Alfredo_Challenge Mode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 25000
g_Lua_GlobalCoolTime2 = 20000
g_Lua_GlobalCoolTime3 = 10000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Attack1", rate = 8, loop = 1 },
   { action_name = "Attack2", rate = 6, loop = 1, td = "FL,LF,LB" , globalcooltime = 3 },
   { action_name = "Attack3", rate = 6, loop = 1, td = "FR,RF,RB" , globalcooltime = 3 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
}

g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 4, loop = 1, approach = 200 },
   { action_name = "Attack1", rate = 20, loop = 1, approach = 200 },
}
