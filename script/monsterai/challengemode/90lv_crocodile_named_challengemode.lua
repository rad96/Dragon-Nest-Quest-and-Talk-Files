-- 90lv_Crocodile_Named_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 700;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Walk_Back", 0 },
      { "Attack01_Swing", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 1, loop = 1 },
   { action_name = "Walk_Right", rate = 1, loop = 1 },
   { action_name = "Attack03_ShieldAttack", rate = 12, loop = 1, cooltime = 6000 },
   { action_name = "CustomAction1", rate = 8, loop = 1, cooltime = 8000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Attack01_Swing", rate = 12, loop = 1, cooltime = 6000 },
   { action_name = "Attack02_TailAttack", rate = 12, loop = 1, cooltime = 12000 },

}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Assault", rate = 4, loop = 1 },
}

g_Lua_Assault = { 
   { action_name = "Attack02_TailAttack", rate = 10, loop = 1, approach = 300 },
   { action_name = "Walk_Back", rate = 2, loop = 1, approach = 200 },
}
g_Lua_Skill = { 
-- Ai 
	-- Attack04_Charge_Start
	{ skill_index = 500151,  cooltime = 23000, rate = 80, rangemin = 200, rangemax = 800, target = 3, randomtarget = "1.6,0,1", encountertima = 15000 },
	-- Attack05_Combo_Start
	{ skill_index = 500152,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,85" },
	-- Attack13_Circle_VolNest_Star
	{ skill_index = 500153,  cooltime = 38000, rate = 100, rangemin = 0, rangemax = 500, target = 3, selfhppercentrange = "0,40" },
}
