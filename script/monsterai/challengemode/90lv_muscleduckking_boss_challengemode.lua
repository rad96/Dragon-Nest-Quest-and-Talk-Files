--90lv_MuscleDuckKing_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 100
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 10, loop = 1 },
	{ action_name = "Walk_Back", rate = 12, loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 10, loop = 1 },	
	{ action_name = "Walk_Right", rate = 12, loop = 1 },	
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 8, loop = 1 },	
	{ action_name = "Move_Front", rate = 15, loop = 1, },
}

g_Lua_GlobalCoolTime1 = 24000
g_Lua_GlobalCoolTime2 = 24000 
g_Lua_GlobalCoolTime3 = 24000 

g_Lua_Skill = { 
-- next_lua_skill_index
	-- 0: Attack1_Punchi1	
	{ skill_index = 500296, cooltime = 500, rate = -1, rangemin = 0, rangemax = 1000, target = 3, next_lua_skill_index = 1 },  
	-- 1: Attack1_Punchi3
	{ skill_index = 500298, cooltime = 500, rate = -1, rangemin = 0, rangemax = 100, target = 3 },   
	-- 2: Attack2_EarthQuake_Ground 전기
	{ skill_index = 500291, cooltime = 500, rate = -1, rangemin = 0, rangemax = 1000, target = 3 },   
	-- 3: Attack2_EarthQuake_Shot 화염
	{ skill_index = 500293, cooltime = 500, rate = -1, rangemin = 0, rangemax = 1000, target = 3 },   
 	
--Ai
	--Attack1_Punch1	
	{ skill_index = 500296, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 450, target = 3 },  
	--Attack1_Punch2
	{ skill_index = 500297, cooltime = 8000, rate = 100, rangemin = 0, rangemax = 350, target = 3, next_lua_skill_index = 0, selfhppercentrange = "0,80" },   
	-- Attack1_Punch3
	{ skill_index = 500298, cooltime = 8000, rate = 100, rangemin = 0, rangemax = 350, target = 3, selfhppercentrange = "71,100" },   
	{ skill_index = 500298, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 350, target = 3, encountertime = 500, next_lua_skill_index = 0, selfhppercentrange = "0,70" },   

	-- Attack2_EarthQuake_Ground 전기
	{ skill_index = 500291, cooltime = 24000, globalcooltime = 3, rate = 50, rangemin = 400, rangemax = 700, target = 3, selfhppercentrange = "81,100" },	
	{ skill_index = 500291, cooltime = 24000, globalcooltime = 2, rate = 50, rangemin = 400, rangemax = 700, target = 3, encountertime = 12000, selfhppercentrange = "0,80" },	
	-- Attack2_EarthQuake_Range 빙결
	{ skill_index = 500292, cooltime = 24000, globalcooltime = 3, rate = 50, rangemin = 300, rangemax = 700, target = 3, selfhppercentrange = "51,90" },	
	{ skill_index = 500292, cooltime = 24000, globalcooltime = 1, rate = 50, rangemin = 300, rangemax = 700, target = 3, encountertime = 15000, selfhppercentrange = "0,50", next_lua_skill_index = 2 },	
	{ skill_index = 500292, cooltime = 24000, globalcooltime = 1, rate = 50, rangemin = 300, rangemax = 700, target = 3, encountertime = 15000, selfhppercentrange = "0,50", next_lua_skill_index = 3 },	
	--Attack2_EarthQuake_Shot 화염
	{ skill_index = 500293, cooltime = 24000, globalcooltime = 3, rate = 50, rangemin = 500, rangemax = 700, target = 3, selfhppercentrange = "81,100" },	
	{ skill_index = 500293, cooltime = 24000, globalcooltime = 2, rate = 50, rangemin = 500, rangemax = 700, target = 3, encountertime = 12000, selfhppercentrange = "0,80" },	
	--Attack3_fire_Start
	{ skill_index = 500294, cooltime = 34000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "0,80", multipletarget = "2,2,0,1"  },
	--Attack6_CrazyShot
	{ skill_index = 500295, cooltime = 42000, rate = 100, rangemin = 300, rangemax = 700, target = 3, selfhppercentrange = "0,60" },   

}
