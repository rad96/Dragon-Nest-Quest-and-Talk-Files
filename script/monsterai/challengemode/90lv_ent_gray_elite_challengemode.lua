-- 90lv_Ent_Gray_Elite_ChallengeMode.lua 

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 800;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 10000

g_Lua_GlobalCoolTime1 = 6000
g_Lua_GlobalCoolTime2 = 4000
g_Lua_GlobalCoolTime3 = 35000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack03_Kick", 0 },
      { "Attack01_RightHand", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop=1 },
   { action_name = "Walk_Front", rate = 1, loop=1 },
   { action_name = "Walk_Back", rate = 1, loop=1 },
   { action_name = "Walk_Left", rate = 3, loop=1 },
   { action_name = "Walk_Right", rate = 3, loop=1 },
   { action_name = "Attack01_RightHand", rate = 9, loop=1 },
   { action_name = "Attack03_Kick", rate = 6, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop=1 },
   { action_name = "Walk_Front", rate = 3, loop=2 },
   { action_name = "Walk_Back", rate = 3, loop=1 },
   { action_name = "Walk_Left", rate = 3, loop=1 },
   { action_name = "Walk_Right", rate = 3, loop=1 },
   { action_name = "Assault", rate = 10, loop=1 },
}

g_Lua_Assault = { 
   { action_name = "CustomAction1", rate = 13, approach = 250.0 },
}
g_Lua_Skill = { 
   { skill_index = 500396,  cooltime = 22000, rate = 60,rangemin = 0, rangemax = 400, target = 3, td = "LF,FL,FR,RF,RB,BR,BL,LB", encountertime=5000, },
}
