-- 90lv_OgreRed_Named_ChallengeMode.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 600;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 10, loop = 1 },
	{ action_name = "Move_Front", rate = 8, loop = 1 },
	{ action_name = "Walk_Left", rate = 8, loop = 1 },
	{ action_name = "Walk_Right", rate = 8, loop = 1 },
	{ action_name = "Attack1_bash", rate = 12, loop = 1, cooltime = 6000 },
	{ action_name = "Attack6_HornAttack", rate = 15, loop = 1, cooltime = 8000 },
}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 1 },
   { action_name = "Move_Right", rate = 3, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Attack1_bash", rate = 15, loop = 1, cooltime = 6000 },
   { action_name = "Attack2_Berserk", rate = 30, loop = 1, cooltime = 18000, selfhppercentrange = "0,70" },

}

g_Lua_Near3 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
   { action_name = "Assault", rate = 18, loop = 1, cooltime = 12000 },
}


g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 5, loop = 1, approach = 250.0  },
   { action_name = "Attack6_HornAttack", rate = 3, loop = 1, approach = 300.0  },
}
g_Lua_Skill = { 
-- Ai
	-- Attack12_BigFireBall
	{ skill_index = 500136, cooltime = 18000, rate = 100, rangemin = 0, rangemax = 800, target = 3, multipletarget = "1,2,0,1", selfhppercentrange = "0,90" },
	-- Attack11_FireWave
	{ skill_index = 500137, cooltime = 28000, rate = 150, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,75" },

}