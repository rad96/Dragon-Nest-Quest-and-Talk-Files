--AiRhinoMan_Gray_Boss_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Walk_Back", rate = 8, loop = 1  },
   { action_name = "Attack1_bash", rate = 15, loop = 1  },
   { action_name = "Attack14_TripleSwing", rate = 22.5, loop = 1, cooltime = 6000 },

}

g_Lua_Near2= { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Attack14_TripleSwing", rate = 22.5, loop = 1, cooltime = 6000 },
   { action_name = "Attack10_Shot", rate = 45, loop = 1, cooltime = 8000  },

}

g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 35, loop = 1  },
   { action_name = "Attack10_Shot", rate = 33.75, loop = 1  },
   { action_name = "Assault", rate = 27, loop = 1  },
}

g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 8, loop = 1, approach = 250.0  },
}
g_Lua_Skill = { 
-- Ai
	-- Attack11_WideAreaShot
	{ skill_index = 500116,  cooltime = 16000, rate = 90,rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,80" },
	-- Attack12_SupportCannonShot
	{ skill_index = 500117,  cooltime = 46000, rate = 90, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,70" },
	-- Attack13_FrontBreakShot
	{ skill_index = 500118,  cooltime = 32000, rate = 90, rangemin = 200, rangemax = 800, target = 3, selfhppercentrange = "0,60" },
}
