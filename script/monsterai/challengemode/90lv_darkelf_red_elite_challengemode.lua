--AiDarkElf_Red_Boss_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Move_Back", rate = 20, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 15, loop = 1  },
   { action_name = "Attack4_SickleKick", rate = 25, loop = 1, max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 7, loop = 1  },
   { action_name = "Move_Back", rate = 7, loop = 1  },
   { action_name = "Attack4_SickleKick", rate = 20, loop = 1  },
   { action_name = "Assault", rate = 12, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 12, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 1  },
   { action_name = "Move_Right", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Move_Back", rate = 15, loop = 1  },
   { action_name = "Attack7_ThrowingKnife", rate = -1, loop = 1  },
   { action_name = "Assault", rate = 10, loop = 1  },
}

g_Lua_Assault = { 
   { action_name = "Attack4_SickleKick", rate = 5, loop = 1, approach = 150.0, max_missradian = 30 },
}
g_Lua_Skill = { 
   { skill_index = 500380,  cooltime = 12000, rate = 80, rangemin = 0, rangemax = 300, target = 3, selfhppercent = 100 },
}
