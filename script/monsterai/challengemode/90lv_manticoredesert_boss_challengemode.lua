--90lv_ManticoreDesert_Boss_ChallengeMode.lua

g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 800.0;


g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;

g_Lua_GlobalCoolTime1 = 10000 ---G1: Attack07_LHook_EX_Challenge, Attack07_LHook_EX
g_Lua_GlobalCoolTime2 = 10000 ---G1: Attack07_LHook_EX_Challenge, Attack07_LHook_EX


g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Walk_Back", rate = 2, loop = 1, cooltime = 5000 },
	{ action_name = "Turn_Left", rate = 3, loop = 1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 3, loop = 1, td = "RF,RB,BR" },
	{ action_name = "Attack07_LHook_EX_Challenge", rate = 12, loop = 1, td = "FL,LF", cooltime = 8000, selfhppercentrange = "0,60" },
	{ action_name = "Attack08_RHook_EX_Challenge", rate = 12, loop = 1, td = "FR,RF", cooltime = 8000, selfhppercentrange = "0,60" },

}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Move_Front", rate = 8, loop = 2, cooltime = 5000 },

}

g_Lua_GlobalCoolTime3 = 8000 --_G2: Tail -> turn -> Hook
g_Lua_GlobalCoolTime4 = 25000 --_G3: Tail -> turn -> Hook

g_Lua_Skill = { 
-- next_lua_skill_index
	-- 0: Turn_Left
	{ skill_index = 500252, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	-- 1: Turn_Right
	{ skill_index = 500231, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	-- 2: Turn_Left
	{ skill_index = 500252, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, next_lua_skill_index = 4 },
	-- 3: Turn_Right
	{ skill_index = 500231, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, next_lua_skill_index = 5 },
	-- 4: Attack07_LHook_EX
	{ skill_index = 500243, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	-- 5: Attack08_RHook_EX
	{ skill_index = 500244, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },

-- AI
	-- Attack04_Tail_L_EX
	{ skill_index = 500241, cooltime = 6000, rate = 20, rangemin = 0, rangemax = 500, target = 3, td = "BL,LB", selfhppercentrange = "41,95", next_lua_skill_index = 0 },
	{ skill_index = 500241, cooltime = 6000, rate = 20, rangemin = 0, rangemax = 500, target = 3, td = "BL,LB", selfhppercentrange = "0,40", next_lua_skill_index = 2 },
	-- Attack05_Tail_R_EX
	{ skill_index = 500242, cooltime = 6000, rate = 20, rangemin = 0, rangemax = 500, target = 3, td = "BR,RB", selfhppercentrange = "41,95", next_lua_skill_index = 1 },
	{ skill_index = 500242, cooltime = 6000, rate = 20, rangemin = 0, rangemax = 500, target = 3, td = "BR,RB", selfhppercentrange = "0,40", next_lua_skill_index = 3 },
	-- Attack07_LHook_EX
	{ skill_index = 500243, cooltime = 6000, rate = 30, rangemin = 0, rangemax = 400, target = 3, td = "FL,LF", selfhppercentrange = "61,100" },
	-- Attack08_RHook_EX
	{ skill_index = 500244, cooltime = 6000, rate = 30, rangemin = 0, rangemax = 400, target = 3, td = "FR,RF", selfhppercentrange = "61,100" },
	-- Attack16_SlidingBash
	{ skill_index = 500245, cooltime = 18000, rate = 50, rangemin = 200, rangemax = 1000, target = 3, td = "FL,FR,LF,RF", encountertime = 40000 },
	-- Attack01_BigGravityBallEX_MT
	{ skill_index = 500246, cooltime = 24000, rate = 30, rangemin = 0, rangemax = 800, target = 3, multipletarget = "1,4,0,1", selfhppercentrange = "0,90" },
	-- Attack03_HeavyStampEX
	{ skill_index = 500248, cooltime = 36000, rate = 30, rangemin = 0, rangemax = 800, target = 3, encountertime = 15000, selfhppercentrange = "0,85" },
	-- Attack06_HeavyJumpEX
	{ skill_index = 500249, cooltime = 28000, rate = 25, rangemin = 3, rangemax = 800, target = 3, selfhppercentrange = "0,80" },
	-- Attack11_RaiseGravity1_EX
	{ skill_index = 500251, cooltime = 28000, rate = 30, rangemin = 0, rangemax = 800, target = 3, encountertime = 24000, selfhppercentrange = "0,70" },
	-- Attack10_SpectrumEX
	{ skill_index = 500250, cooltime = 48000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, selfhppercentrange = "0,70" },
	
}
