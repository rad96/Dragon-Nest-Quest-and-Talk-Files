--AiMinotauros_Black_Armor_Patrol.lua

g_Lua_NearTableCount = 7

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 550;
g_Lua_NearValue4 = 850;
g_Lua_NearValue5 = 1150;
g_Lua_NearValue6 = 1450;
g_Lua_NearValue7 = 1750;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 50
g_Lua_AssualtTime = 5000

-- 이벤트 시그널 9876


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_NontargetFront", rate = 30, loop = 2 },
}
g_Lua_Near2 = { 
   { action_name = "Move_Left", rate = 35, loop = 1 },
   { action_name = "Move_Right", rate = 35, loop = 1 },
   { action_name = "Walk_Left", rate = 35, loop = 1 },
   { action_name = "Walk_Right", rate = 35, loop = 1 },
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_NontargetFront", rate = 30, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Move_Left", rate = 35, loop = 1 },
   { action_name = "Move_Right", rate = 35, loop = 1 },
   { action_name = "Walk_Left", rate = 35, loop = 1 },
   { action_name = "Walk_Right", rate = 35, loop = 1 },
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_NontargetFront", rate = 30, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Left", rate = 35, loop = 1 },
   { action_name = "Move_Right", rate = 35, loop = 1 },
   { action_name = "Walk_Left", rate = 35, loop = 1 },
   { action_name = "Walk_Right", rate = 35, loop = 1 },
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_NontargetFront", rate = 30, loop = 2 },
}
g_Lua_Near5 = { 
   { action_name = "Move_Left", rate = 35, loop = 1 },
   { action_name = "Move_Right", rate = 35, loop = 1 },
   { action_name = "Walk_Left", rate = 35, loop = 1 },
   { action_name = "Walk_Right", rate = 35, loop = 1 },
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_NontargetFront", rate = 30, loop = 2 },
}
g_Lua_Near6 = { 
   { action_name = "Move_Left", rate = 15, loop = 1 },
   { action_name = "Move_Right", rate = 25, loop = 1 },
   { action_name = "Walk_Left", rate = 25, loop = 1 },
   { action_name = "Walk_Right", rate = 25, loop = 1 },
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_NontargetFront", rate = 30, loop = 2 },
}
g_Lua_Near7 = { 
   { action_name = "Move_Left", rate = 25, loop = 1 },
   { action_name = "Move_Right", rate = 25, loop = 1 },
   { action_name = "Walk_Left", rate = 25, loop = 1 },
   { action_name = "Walk_Right", rate = 25, loop = 1 },
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_NontargetFront", rate = 30, loop = 2 },
}

g_Lua_Skill = { 
   { skill_index = 21248, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 100, target = 3, encountertime = 4000 },
   { skill_index = 21258, cooltime = 5000, rate = 40, rangemin = 0, rangemax = 1000, target = 3, encountertime = 4000, randomtarget = "1.6,0,1" },
   { skill_index = 21259, cooltime = 5000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6,0,1" },
}