--AiLizardman_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack11_ShieldChim", rate = 20, loop = 1, checkweapon=2  },
   { action_name = "Attack1_Piercing", rate = 28, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Attack11_ShieldChim", rate = 45, loop = 1, checkweapon=2  },
   { action_name = "Attack1_Piercing", rate = 13, loop = 1  },
   { action_name = "Attack12_ShieldCharge", rate = 32, loop = 1 },
   { action_name = "Attack13_BigSwing_SeaDragon", rate = 22, loop = 1, selfhppercent = 50  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Attack13_BigSwing_SeaDragon", rate = 65, loop = 2, selfhppercent = 75  },
   { action_name = "Attack12_ShieldCharge", rate = 60, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Assault", rate = 45, loop = 1  },
   { action_name = "Attack12_ShieldCharge", rate = 90, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
}
g_Lua_RangeDefense = { 
   { action_name = "Assault", rate = 45, loop = 1  },
   { action_name = "Attack12_ShieldCharge", rate = 45, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 3 },
   { action_name = "Walk_Right", rate = 10, loop = 3  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 45, loop = 1  },
   { action_name = "Attack12_ShieldCharge", rate = 45, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 3  },
   { action_name = "Walk_Right", rate = 10, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack11_ShieldChim", rate = 30, loop = 2, cancellook = 0, approach = 250.0, checkweapon=2   },
   { action_name = "Attack1_Piercing", rate = 10, loop = 2, cancellook = 0, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 30559,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3, announce = "100,10000" },
}
