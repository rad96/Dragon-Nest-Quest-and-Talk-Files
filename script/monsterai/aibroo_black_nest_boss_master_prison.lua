--AiBroo_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Attack1_GravityBall_Lv2", rate = 50, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Attack1_GravityBall_Lv2", rate = 55, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 20156,  cooltime = 20000, rate = 50,rangemin = 300, rangemax = 1000, target = 3, selfhppercent = 100 },
   { skill_index = 20157,  cooltime = 20000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 100 },
}
