--AiHarpy_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 6, loop = 3  },
   { action_name = "Attack3_FallDown", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Attack1_Throw_B", rate = 6, loop = 1  },
   { action_name = "Attack1_Throw_B_T", rate = 6, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 6, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 4, loop = 1  },
   { action_name = "Attack1_Throw_B", rate = 4, loop = 1  },
   { action_name = "Attack1_Throw_B_T", rate = 6, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 15, loop = 3  },
   { action_name = "Move_Right", rate = 15, loop = 3  },
   { action_name = "Attack1_Throw_B_T", rate = 50, loop = 1  },
}
g_Lua_Skill = { 
-- 유혹
   { skill_index = 20213,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, encountertime = 10000 },
-- 수면가스
--   { skill_index = 20214,  cooltime = 15000, rate = -1, rangemin = 0, rangemax = 500, target = 3 },
}
