--AiGoblin_White_Assault.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Attack1_Slash", rate = 1, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Assault", rate = 3, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Slash", rate = 1, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20001,  cooltime = 100, rate = 100, rangemin = 100, rangemax = 300, target = 3 },
}