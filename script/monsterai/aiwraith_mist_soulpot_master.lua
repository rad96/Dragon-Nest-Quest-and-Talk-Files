--Wraith_Mist_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1700;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssaultTime = 6000

g_Lua_SkillProcessor = {
   { skill_index = 21114, changetarget = "1700,0" },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Walk_Back", rate = 2, loop = 1, cooltime = 6000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Assault", rate = 12, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Assault", rate = 11, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "lua_skill_index = 2", rate = 16, approach = 400 },
}
g_Lua_Skill = { 
   { skill_index = 21112,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 5000, target = 3, combo1 = "1,100,0" },
   { skill_index = 21113,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 21116,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "0,100,0" },
   { skill_index = 21111,  cooltime = 16000, rate = 70, rangemin = 0, rangemax = 400, target = 3, combo1 = "0,100,0" },
   { skill_index = 21114,  cooltime = 25000, rate = 70, rangemin = 0, rangemax = 3000, target = 3 },
   { skill_index = 21115,  cooltime = 31000, rate = 60, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "1.6,0,1" },
}
