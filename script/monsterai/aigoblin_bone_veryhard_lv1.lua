-- Goblin Bone Master Lv1 AI

g_Lua_NearTableCount = 7;

g_Lua_NearValue1 = 100.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;
g_Lua_NearValue7 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 120;
g_Lua_AssualtTime = 5000;

g_Lua_CustomAction =
{
	CustomAction1 = 
	{
      			{ "Move_Back", 1},
			{ "Attack7_Nanmu", 1},	 		
	},
}

g_Lua_State = 	
{	
	State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" },
	State2 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" },
}	



g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Back",	rate = 15,		loop = 2 },
	{ action_name = "Move_Back",	rate = 15,		loop = 2 },
	{ action_name = "Attack1_Slash",	rate = 6,		loop = 1, max_missradian = 60 },
	{ action_name = "CustomAction1",	rate = -1,		loop = 1, target_condition = "State2" },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand2",	rate = 6,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 8,		loop = 2 },
	{ action_name = "Move_Back",	rate = 5,		loop = 2 },
	{ action_name = "Attack1_Slash",	rate = 7,		loop = 1 },
	{ action_name = "Attack7_Nanmu",	rate = -1,		loop = 1, max_missradian = 30 },
	{ action_name = "CustomAction1",	rate = -1,		loop = 1, target_condition = "State2" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand2",	rate = 8,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 8,		loop = 2 },
	{ action_name = "Move_Left",	rate = 1,		loop = 1 },
	{ action_name = "Move_Right",	rate = 1,		loop = 1 },
        { action_name = "Move_Front",	rate = 3,		loop = 1 },
        { action_name = "Attack2_Upper",	rate = 7,		loop = 1 },
	{ action_name = "Attack7_Nanmu",	rate = -1,		loop = 1, max_missradian = 10 },
	{ action_name = "CustomAction1",	rate = -1,		loop = 1, target_condition = "State2" },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand2",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 8,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 1,		loop = 2 },
	{ action_name = "Move_Right",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 3,		loop = 2 },
	{ action_name = "Move_Back",	rate = 1,		loop = 2 },
        	{ action_name = "Assault",	rate = 10,		loop = 1 },
	{ action_name = "Attack7_Nanmu",	rate = -1,		loop = 1, max_missradian = 10 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand2",	rate = 12,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 12,		loop = 2 },
	{ action_name = "Move_Left",	rate = 1,		loop = 2 },
	{ action_name = "Move_Right",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 5,		loop = 2 },
	{ action_name = "Assault",	rate = 8,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Stand2",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 3 },
	{ action_name = "Move_Right",	rate = 3,		loop = 3 },
	{ action_name = "Move_Front",	rate = 10,		loop = 3 },
        { action_name = "Assault",	rate = 5,		loop = 1 },
}

g_Lua_Near7 = 
{ 
	{ action_name = "Stand2",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 2,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 2,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack2_Upper",	rate = 5,	loop = 1, cancellook = 1, approach = 250.0 },
        { action_name = "Attack1_Slash",	rate = 3,	loop = 1, cancellook = 1, approach = 100.0 },
}
       