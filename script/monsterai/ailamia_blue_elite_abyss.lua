--AiLamia_Blue_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1300;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack2_TailAttack", 1 },
      { "Attack1_Smash_Blue", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 1  },
   { action_name = "Walk_Right", rate = 8, loop = 1  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
   { action_name = "Attack1_Smash_Blue", rate = 9, loop = 1  },
   { action_name = "Attack2_TailAttack", rate = -1, loop = 1  },
   { action_name = "CustomAction1", rate = 18, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack1_Smash_Blue", rate = 9, loop = 1  },
   { action_name = "Attack2_TailAttack", rate = -1, loop = 1  },
   { action_name = "CustomAction1", rate = 18, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack5_ChargeCut_Blue", rate = 20, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 2, loop = 1  },
   { action_name = "Assault", rate = 27, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 3  },
   { action_name = "Move_Right", rate = 10, loop = 3  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
}
g_Lua_RangeDefense = { 
   { action_name = "Attack5_ChargeCut_Blue", rate = 8, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack5_ChargeCut_Blue", rate = 8, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Smash", rate = 10, loop = 1, approach = 300.0 },
}
g_Lua_Skill = { 
   { skill_index = 20226,  cooltime = 15000, rate = 80,rangemin = 0, rangemax = 800, target = 3, selfhppercent = 100 },
}
