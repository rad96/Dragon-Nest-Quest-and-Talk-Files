
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1400;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|!Air", "Down" },
 State2 = {"!Air", "!Down" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"!Down|Hit","Air|Down"}, 
 State5 = {"!Move","Air"},
}

g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Walk_Back1", rate = 12, loop = 1 },	
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Walk_Back1", rate = 12, loop = 1 },	
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 8, loop = 1 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 3 },
     { action_name = "Move_Left", rate = 3, loop = 2 },
     { action_name = "Move_Right", rate = 3, loop = 2 },
}

g_Lua_GlobalCoolTime1 = 10000

g_Lua_Skill = { 
-- 1����_��
     { skill_index = 82502, cooltime = 15000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 500, target = 3 },
-- 7������_��
     { skill_index = 82508, cooltime = 26000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 500, target = 3 },

-- ��Ƽ �� 
     { skill_index = 82505, cooltime = 12000, rate = 80, rangemin = 0, rangemax = 400, target = 3 },
-- Ʈ�� ��
     { skill_index = 82504, cooltime = 8000, rate = 60, rangemin = 200, rangemax = 1000, target = 3 }, 
-- 11�Ǿ�� ��
     { skill_index = 82516, cooltime = 12000, rate = 70, rangemin = 400, rangemax = 1000, target = 3 },
-- 13���� ��
     { skill_index = 96087, cooltime = 20000, rate = 80, rangemin = 500, rangemax = 1000, target = 3 },	 	 

-- 15���̵�� �� EX
     { skill_index = 82543, cooltime = 32000, rate = 80, rangemin = 500, rangemax = 1500, target = 3 },	 
-- 18�ַο� ����
     { skill_index = 82526, cooltime = 45000, rate = 80, rangemin = 700, rangemax = 1000, target = 3, cancellook = 1 },
}