
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
 State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashUpper" },
  },
-- 라이징슬래쉬
  CustomAction2 = {
     { "Skill_RisingSlash_LV2" },
     { "Skill_RisingSlash_Part2_Lv2" },
     { "Skill_RisingSlash_Part3" },
  },
-- 앞 구르기 후 스톰프
  CustomAction3 = {
     { "Tumble_Front" },
     { "Skill_Stomp" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Axe" },
     { "Attack2_Axe" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Axe" },
     { "Attack2_Axe" },
     { "Attack3_Axe" },
  },
-- 기본 공격 4연타 313프레임
  CustomAction6 = {
     { "Attack1_Axe" },
     { "Attack2_Axe" },
     { "Attack3_Axe" },
     { "Attack4_Axe" },
  },
}

g_Lua_GlobalCoolTime1 = 18000
g_Lua_GlobalCoolTime2 = 18000

g_Lua_Near1 = 
{ 
     { action_name = "Attack_Down", rate = 30, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Stand_1", rate = 1, loop = 1 },
     { action_name = "Move_Left", rate = 3, loop = 1 },
     { action_name = "Move_Right", rate = 3, loop = 1 },
     { action_name = "Move_Back", rate = 2, loop = 1 },
--1타
     { action_name = "Attack1_Axe", rate = 10, loop = 1, globalcooltime = 1 },
--2타
     { action_name = "CustomAction4", rate = 7, loop = 1, globalcooltime = 1 },
--3타
     { action_name = "CustomAction5", rate = 5, loop = 1, cooltime = 7000, globalcooltime = 1 },
--4타
     { action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 10000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand_1", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 2 },
     { action_name = "Move_Back", rate = 5, loop = 1 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
     { action_name = "Tumble_Front", rate = 5, loop = 1, globalcooltime = 3 },
     { action_name = "Tumble_Left", rate = 1, loop = 1, globalcooltime = 3 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand_1", rate = 3, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 4 },
     { action_name = "Move_Left", rate = 3, loop = 1 },
     { action_name = "Move_Right", rate = 3, loop = 1 },
     { action_name = "Assault",  rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand_1", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 6 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Assault",  rate = 20,  loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 10, loop = 1 },
}


g_Lua_Assault = { 
   { action_name = "Skill_DashUpper", rate = 30, loop = 1, approach = 300   },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "CustomAction2", rate = 3, loop = 1, globalcooltime = 3 },
     { action_name = "CustomAction3", rate = 3, loop = 1, globalcooltime = 3 },
}

g_Lua_NonDownRangeDamage = {
     { action_name = "Move_Front", rate = 5, loop = 1 },
     { action_name = "Skill_PunishingSwing", rate = 3, loop = 1, cooltime = 9000 },
}

g_Lua_Skill = {
-- 디스인챈팅하울
   { skill_index = 31027, cooltime = 30000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 30, globalcooltime = 2 },
-- 휠윈드
   { skill_index = 31023, cooltime = 40000, rate = 100, rangemin = 0, rangemax = 500,target = 3, selfhppercent = 30, limitcount = 2, globalcooltime = 2 },
-- 스톰프
   { skill_index = 31021, cooltime = 20000, rate = 60, rangemin = 0, rangemax = 800,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercent = 70, globalcooltime = 2 },
-- 퍼니싱스윙
   { skill_index = 31024, cooltime = 14000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 70, globalcooltime = 2 },
-- 플라잉스윙
   { skill_index = 31022, cooltime = 34000, rate = 80, rangemin = 0, rangemax = 500,target = 3, globalcooltime = 2 },
-- 데몰리션 피스트
   { skill_index = 31026, cooltime = 14000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, globalcooltime = 2 },
 
-- 임팩트펀치
     { skill_index = 31001, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 200, target = 3, globalcooltime = 2 },
-- 헤비슬래쉬
     { skill_index = 31002, cooltime = 10000, rate = 40, rangemin = 200, rangemax = 600, target = 3, globalcooltime = 2 },
-- 서클브레이크
     { skill_index = 31004, cooltime = 15000, rate = 50, rangemin = 0, rangemax = 400, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", globalcooltime = 2 },
-- 임팩트웨이브
     { skill_index = 31003, cooltime = 14000, rate = 40, rangemin = 0, rangemax = 600, target = 3, globalcooltime = 2 },
 -- 버프삭제패턴(타운팅하울활용)
	{ skill_index = 32670, cooltime = 40000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, },
}