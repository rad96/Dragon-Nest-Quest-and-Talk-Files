
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
 State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
     { "Skill_DashCombo" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
     { "Skill_DashCombo" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 기본 공격 4연타 313프레임
  CustomAction6 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
     { "Attack4_Sword" },
  },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 15000

g_Lua_Near1 = 
{ 
     { action_name = "Attack_Down", rate = 30, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Stand", rate = 2, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Right", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Back", rate = 3, loop = 1 },
     { action_name = "Attack_SideKick", rate = 12, loop = 1, cooltime = 30000, globalcooltime = 1 },
--1타
     { action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     { action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     { action_name = "CustomAction5", rate = 5, loop = 1, cooltime = 13000, globalcooltime = 1 },
--4타
     { action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 16000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 20, loop = 1 },
     { action_name = "Move_Right", rate = 20, loop = 1 },
     { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 4 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Assault",  rate = 10, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 6 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
     { action_name = "Assault",  rate = 10,  loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
     { action_name = "Skill_DashSlash", rate = 30, loop = 1, approach = 300 },
     { action_name = "Attack1_Sword", rate = 10, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Stand", rate = 15, loop = 1 },
     { action_name = "Move_Back", rate = 10, loop = 1 },
     { action_name = "CustomAction2", rate = 3, loop = 1, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 3, loop = 1, globalcooltime = 1 },
}

g_Lua_NonDownRangeDamage = {
     { action_name = "Assault", rate = 15, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 1 },
}

g_Lua_Skill = { 
-- 라인드라이브
     { skill_index = 31014, cooltime = 45000, rate = 80, rangemin = 0, rangemax = 900, target = 3, td = "FL,FR", selfhppercent = 30, globalcooltime = 2 },
-- 하프문슬래쉬
     { skill_index = 31015, cooltime = 45000, rate = 80, rangemin = 0, rangemax = 600, target = 3, td = "FL,FR,LF,RF", selfhppercent = 30, globalcooltime = 2  },
-- 크레센트클리브
     { skill_index = 31011, cooltime = 24000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, td = "FL,FR,LF,RF", selfhppercent = 70, globalcooltime = 2 },
-- 이클립스
     { skill_index = 31012, cooltime = 30000, rate = 100, rangemin = 0, rangemax = 400, target = 3, selfhppercent = 70, globalcooltime = 2 },
-- 딥스트레이트콤보
     { skill_index = 31013, cooltime = 17000, rate = 50, rangemin = 100, rangemax = 1000, target = 3, td = "FL,FR", globalcooltime = 2 },
-- 문라이트 스프리터
     { skill_index = 31017, cooltime = 15000, rate = 50, rangemin = 100, rangemax = 700, target = 3, td = "FL,FR", globalcooltime = 2 },


-- 임팩트펀치
     { skill_index = 31001, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 200, target = 3, globalcooltime = 2 },
-- 헤비슬래쉬
     { skill_index = 31002, cooltime = 10000, rate = 40, rangemin = 200, rangemax = 600, target = 3, globalcooltime = 2 },
-- 서클브레이크
     { skill_index = 31004, cooltime = 15000, rate = 50, rangemin = 0, rangemax = 400, target = 3, globalcooltime = 2 },
-- 임팩트웨이브
     { skill_index = 31003, cooltime = 14000, rate = 40, rangemin = 0, rangemax = 600, target = 3, globalcooltime = 2 },
}