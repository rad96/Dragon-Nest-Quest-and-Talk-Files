--AiQueenSpider_ANU_Elite_Easy.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 450;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 350
g_Lua_AssaultTime = 10000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 1, loop=1 },
   { action_name = "Walk_Left", rate = 3, loop=1 },
   { action_name = "Walk_Right", rate = 3, loop=1 },
   { action_name = "Attack1_Slash_Lv2", rate = 6, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 3, loop=1 },
   { action_name = "Walk_Left", rate = 5, loop=1 },
   { action_name = "Walk_Right", rate = 5, loop=1 },
   { action_name = "Walk_Front", rate = 5, loop=1 },
   { action_name = "Attack1_Slash_Lv2", rate = 8, loop=1 },
   { action_name = "Attack3_JumpAttack", rate = 5, loop=1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop=1 },
   { action_name = "Walk_Left", rate = 5, loop=1 },
   { action_name = "Walk_Right", rate = 5, loop=1 },
   { action_name = "Walk_Front", rate = 10, loop=1 },
   { action_name = "Attack3_JumpAttack", rate = 5, loop=1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop=1 },
   { action_name = "Walk_Left", rate = 5, loop=1 },
   { action_name = "Walk_Right", rate = 5, loop=1 },
   { action_name = "Walk_Front", rate = 25, loop=1 },
   { action_name = "Attack3_JumpAttack", rate = 5, loop=1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 5, loop=1 },
   { action_name = "Walk_Front", rate = 10, loop=1 },
}
g_Lua_Skill = { 
   { skill_index = 31412,  cooltime = 1000, rate = 100,rangemin = 200, rangemax = 5000, target = 1, limitcount=1, selfhppercent = 50 },
   { skill_index = 31414,  cooltime = 48000, rate = 50, rangemin = 300, rangemax = 700, target = 3 },
   { skill_index = 31415,  cooltime = 38000, rate = 80, rangemin = 0, rangemax = 1500, target = 3 },
}
