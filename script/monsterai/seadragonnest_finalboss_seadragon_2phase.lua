--AiHalfGolem_Stone_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 30000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}


-- 거리별 액션은 모두 동일하게 잡는다.
g_Lua_Near1 = { 
   { action_name = "Die", rate = 100, loop = 1, selfhppercentrange = "0,75"  },
   { action_name = "Stand_2Phase", rate = 10, loop = 2, selfhppercentrange = "0,100"  },
   { action_name = "Walk_Left", rate = 4, loop = 1, selfhppercentrange = "75,85", cooltime = 50000  },
   { action_name = "Walk_Right", rate = 4, loop = 1, selfhppercentrange = "75,85", cooltime = 50000 },
   { action_name = "Walk_Left", rate = 4, loop = 1, selfhppercentrange = "72,85", cooltime = 50000  },
   { action_name = "Walk_Right", rate = 4, loop = 1, selfhppercentrange = "71,85", cooltime = 50000 },
}
g_Lua_Near2 = { 
   { action_name = "Die", rate = 100, loop = 1, selfhppercentrange = "0,75"  },
   { action_name = "Stand_2Phase", rate = 10, loop = 2, selfhppercentrange = "0,100"  },
   { action_name = "Walk_Left", rate = 4, loop = 1, selfhppercentrange = "75,85", cooltime = 50000  },
   { action_name = "Walk_Right", rate = 4, loop = 1, selfhppercentrange = "75,85", cooltime = 50000 },
   { action_name = "Walk_Left", rate = 4, loop = 1, selfhppercentrange = "72,85", cooltime = 50000  },
   { action_name = "Walk_Right", rate = 4, loop = 1, selfhppercentrange = "71,85", cooltime = 50000 },
}
g_Lua_Near3 = { 
   { action_name = "Die", rate = 100, loop = 1, selfhppercentrange = "0,75"  },
   { action_name = "Stand_2Phase", rate = 10, loop = 2, selfhppercentrange = "0,100"  },
   { action_name = "Walk_Left", rate = 4, loop = 1, selfhppercentrange = "75,85", cooltime = 50000  },
   { action_name = "Walk_Right", rate = 4, loop = 1, selfhppercentrange = "75,85", cooltime = 50000 },
   { action_name = "Walk_Left", rate = 4, loop = 1, selfhppercentrange = "72,85", cooltime = 50000  },
   { action_name = "Walk_Right", rate = 4, loop = 1, selfhppercentrange = "71,85", cooltime = 50000 },
}
g_Lua_Near4 = { 
   { action_name = "Die", rate = 100, loop = 1, selfhppercentrange = "0,75"  },
   { action_name = "Stand_2Phase", rate = 10, loop = 2, selfhppercentrange = "0,100"  },
   { action_name = "Walk_Left", rate = 4, loop = 1, selfhppercentrange = "75,85", cooltime = 50000  },
   { action_name = "Walk_Right", rate = 4, loop = 1, selfhppercentrange = "75,85", cooltime = 50000 },
   { action_name = "Walk_Left", rate = 4, loop = 1, selfhppercentrange = "72,85", cooltime = 50000  },
   { action_name = "Walk_Right", rate = 4, loop = 1, selfhppercentrange = "71,85", cooltime = 50000 },
}
g_Lua_Near5 = { 
   { action_name = "Die", rate = 100, loop = 1, selfhppercentrange = "0,75"  },
   { action_name = "Stand_2Phase", rate = 10, loop = 2, selfhppercentrange = "0,100"  },
   { action_name = "Walk_Left", rate = 4, loop = 1, selfhppercentrange = "75,85", cooltime = 50000  },
   { action_name = "Walk_Right", rate = 4, loop = 1, selfhppercentrange = "75,85", cooltime = 50000 },
   { action_name = "Walk_Left", rate = 4, loop = 1, selfhppercentrange = "72,85", cooltime = 50000  },
   { action_name = "Walk_Right", rate = 4, loop = 1, selfhppercentrange = "71,85", cooltime = 50000 },
}

g_Lua_Skill = { 
-- ___________조건스킬__________
-- 빙결 폭탄
   { skill_index = 30597,  cooltime = 1000, 	rate = -1, 	rangemin = 0, rangemax = 40000,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", blowcheck = "100", multipletarget = 1, custom_state1 = "custom_Fly"},
-- ___________페이즈2__________
-- 타겟 록온(Stand)
   { skill_index = 30596,  cooltime = 20000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "70,100", td = "FL,FR,LF,RF,RB,BR,BL,LB", next_lua_skill_index = 0, custom_state1 = "custom_Fly", multipletarget = "1,3" },
-- 몬스터 소환3
   { skill_index = 30585,  cooltime = 40000, 	rate = 90, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "75,80", td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = "1,2,exceptcannon", custom_state1 = "custom_Fly"},
-- 몬스터 소환2
   { skill_index = 30584,  cooltime = 40000, 	rate = 90, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "75,85", td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = "1,3,exceptcannon", custom_state1 = "custom_Fly"},
-- 몬스터 소환1
   { skill_index = 30583,  cooltime = 40000,	rate = 90, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "80,90", td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = "1,5,exceptcannon", custom_state1 = "custom_Fly"},
-- 빙산 소환
   { skill_index = 30578,  cooltime = 90000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "75,100", td = "FL,FR,LF,RF,RB,BR,BL,LB", custom_state1 = "custom_Fly", encountertime = 60000  },
-- 드래곤브레스5-1step
   { skill_index = 30577,  cooltime = 1000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "70,90", td = "FL,FR,LF,RF,RB,BR,BL,LB", custom_state1 = "custom_Fly", limitcount = 1  },
-- 드래곤브레스5-1step
--   { skill_index = 30577,  cooltime = 1000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "70,85", td = "FL,FR,LF,RF,RB,BR,BL,LB", custom_state1 = "custom_Fly", limitcount = 1  },
-- 드래곤브레스5-1step
--   { skill_index = 30577,  cooltime = 1000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "70,80", td = "FL,FR,LF,RF,RB,BR,BL,LB", custom_state1 = "custom_Fly", limitcount = 1  },
-- ___________페이즈3__________
-- 타겟 록온(Stand)
   { skill_index = 30596,  cooltime = 20000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "85,90", td = "FL,FR,LF,RF,RB,BR,BL,LB", mulitpletarget = "1,2", next_lua_skill_index = 0, custom_state1 = "custom_Fly"},
-- 타겟 록온(Stand)
   { skill_index = 30596,  cooltime = 20000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "80,85", td = "FL,FR,LF,RF,RB,BR,BL,LB", mulitpletarget = "1,3", next_lua_skill_index = 0, custom_state1 = "custom_Fly"},
-- 타겟 록온(Stand)
   { skill_index = 30596,  cooltime = 20000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "75,80", td = "FL,FR,LF,RF,RB,BR,BL,LB", mulitpletarget = "1,4", next_lua_skill_index = 0, custom_state1 = "custom_Fly"},
}