g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 12, loop = 1  },
   { action_name = "Walk_Right", rate = 12, loop = 1  },
   { action_name = "Attack1_bash", rate = 14, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 30, loop = 1  },
   { action_name = "Attack1_bash", rate = 10, loop = 1  },
   { action_name = "Attack9_Punch", rate = 7, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
   { action_name = "Attack9_Punch", rate = 10, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Attack2_Berserk", rate = 7, loop = 1  },
   { action_name = "Attack13_Dash_Named", rate = 35, loop = 1, cooltime = 10000 },
   { action_name = "Assault", rate = 21, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 1  },
   { action_name = "Attack13_Dash_Named", rate = 42, loop = 1, cooltime = 10000 },
   { action_name = "Assault", rate = 14, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Front", rate = 30, loop = 3, approach = 400  },
   { action_name = "Assault", rate = 50, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 3, loop = 1, approach = 250.0  },
   { action_name = "Attack6_HornAttack", rate = 5, loop = 1, approach = 300.0  },
   { action_name = "Attack9_Punch", rate = 15, loop = 1, approach = 400  },
   { action_name = "Stand_1", rate = 10, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 30411,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 2000, target = 4, selfhppercent = 100  },
}
