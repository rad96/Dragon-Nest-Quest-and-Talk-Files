--AiOgre_Black_Boss_Nest_Cerberos.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {

  CustomAction1 = {
      { "Attack7_JumpAttack"  },
      { "Attack9_Punch"  },
  },
  CustomAction2 = {
      { "Attack1_bash"  },
      { "Attack6_HornAttack"  },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1, selfhppercent = 100  },
   { action_name = "Walk_Left", rate = 10, loop = 1, selfhppercent = 99  },
   { action_name = "Walk_Right", rate = 10, loop = 1, selfhppercent = 99  },
   { action_name = "Walk_Back", rate = 8, loop = 1, selfhppercent = 99  },
   { action_name = "CustomAction2", rate = 5, loop = 1, selfhppercent = 99  },
   { action_name = "Attack6_HornAttack_Apocalypse", rate = 5, loop = 1, selfhppercent = 99  },
   { action_name = "Attack1_bash_Apocalypse", rate = 5, loop = 1, selfhppercent = 99  },
   { action_name = "Attack3_Upper", rate = 3, loop = 1, selfhppercent = 100  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1, selfhppercent = 99  },
   { action_name = "Walk_Left", rate = 8, loop = 1, selfhppercent = 99  },
   { action_name = "Walk_Right", rate = 8, loop = 1, selfhppercent = 99  },
   { action_name = "Move_Front", rate = 15, loop = 1, selfhppercentrange = "99,100"  },
   { action_name = "Walk_Back", rate = 8, loop = 1, selfhppercent = 99  },
   { action_name = "Attack9_Punch", rate = 10, loop = 1, selfhppercent = 99  },
   { action_name = "CustomAction1", rate = 10, loop = 1, selfhppercent = 99  },
   { action_name = "Attack13_Dash_Apocalypse", rate = 25, loop = 1, cooltime = 10000, selfhppercentrange = "99,100" },
   { action_name = "Attack13_Dash", rate = 15, loop = 1, cooltime = 10000, selfhppercent = 99  },
   { action_name = "Attack6_HornAttack_Apocalypse", rate = 5, loop = 1, selfhppercent = 99  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, selfhppercent = 99  },
   { action_name = "Walk_Left", rate = 5, loop = 1, selfhppercent = 99  },
   { action_name = "Walk_Right", rate = 5, loop = 1, selfhppercent = 99  },
   { action_name = "Walk_Front", rate = 10, loop = 2, selfhppercent = 99  },
   { action_name = "Move_Front", rate = 10, loop = 2, selfhppercent = 99  },
   { action_name = "Move_Front", rate = 50, loop = 1, selfhppercentrange = "99,100"  },
   { action_name = "Attack9_Punch", rate = 10, loop = 1, selfhppercent = 99  },
   { action_name = "Attack7_JumpAttack", rate = 10, loop = 1, selfhppercent = 99  },
   { action_name = "Attack13_Dash_Apocalypse", rate = 25, loop = 1, cooltime = 10000, selfhppercentrange = "99,100"  },
   { action_name = "Attack13_Dash", rate = 15, loop = 1, cooltime = 10000, selfhppercent = 99  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1, selfhppercent = 99  },
   { action_name = "Walk_Left", rate = 5, loop = 1, selfhppercent = 99  },
   { action_name = "Walk_Right", rate = 5, loop = 1, selfhppercent = 99  },
   { action_name = "Walk_Front", rate = 10, loop = 2, selfhppercent = 99  },
   { action_name = "Move_Front", rate = 40, loop = 1, selfhppercent = 99  },
   { action_name = "Move_Front", rate = 50, loop = 1, selfhppercentrange = "99,100"  },
   { action_name = "Attack7_JumpAttack", rate = 15, loop = 1, selfhppercent = 99  },
   { action_name = "Assault", rate = 10, loop = 1  },
   { action_name = "Attack13_Dash_Apocalypse", rate = 25, loop = 1, cooltime = 10000, selfhppercentrange = "99,100"  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, selfhppercent = 99  },
   { action_name = "Walk_Left", rate = 5, loop = 2, selfhppercent = 99  },
   { action_name = "Walk_Right", rate = 5, loop = 2, selfhppercent = 99  },
   { action_name = "Walk_Front", rate = 10, loop = 2, selfhppercent = 99  },
   { action_name = "Move_Front", rate = 30, loop = 1, selfhppercentrange = "99,100"  },
   { action_name = "Move_Front", rate = 30, loop = 1, selfhppercent = 99  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Front", rate = 30, loop = 1, selfhppercent = 99  },
   { action_name = "Move_Left", rate = 3, loop = 3, selfhppercent = 99  },
   { action_name = "Move_Right", rate = 3, loop = 3, selfhppercent = 99  },
   { action_name = "Assault", rate = 25, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "CustomAction1", rate = 5, loop = 1, approach = 250.0, selfhppercent = 99  },
   { action_name = "CustomAction2", rate = 5, loop = 1, approach = 300.0, selfhppercent = 99  },
}
g_Lua_Skill = { 
   { skill_index = 20133,  cooltime = 120000, rate = 100, rangemin = 0, rangemax = 2000, target = 1, selfhppercent = 99 },
   { skill_index = 30410,  cooltime = 40000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, selfhppercent = 99, announce = "100, 2000" },
}
