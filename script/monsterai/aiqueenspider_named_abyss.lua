--AiQueenSpider_Elite_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 450;
g_Lua_NearValue4 = 700;
g_Lua_NearValue5 = 1000;
g_Lua_NearValue6 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction =
{
	CustomAction1 = 
	{
		{ "Attack3_JumpAttack", 2},
	},
}
g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Slash", rate = 40, loop = 1  },
   { action_name = "Attack2_Blow", rate = 27, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Attack1_Slash", rate = 18, loop = 1  },
   { action_name = "Attack2_Blow", rate = 27, loop = 1  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Attack2_Blow", rate = 27, loop = 1  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Stand_2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "CustomAction1", rate = 60, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Stand_2", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "CustomAction1", rate = 40, loop = 1  },
   { action_name = "Assault", rate = 3, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Stand", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Slash", rate = 5, loop = 1, cancellook = 1, approach = 150.0  },
   { action_name = "Attack2_Blow", rate = 10, loop = 1, cancellook = 0, approach = 150.0  },
}
g_Lua_Skill = { 
   { skill_index = 30203,  cooltime = 8000, rate = 100,rangemin = 200, rangemax = 800, target = 3, selfhppercent = 80 },
}
