--AiPlayerMonster_Archer_Elite_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssaultTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" }, 
}

g_Lua_CustomAction = {
-- 3연타 공격
  CustomAction1 = {
      { "Shoot_Stand_BigBow" },
      { "Shoot_Stand_BigBow" },
      { "Shoot_Finish_BigBow" },
  },
-- 공격 우이동 공격
  CustomAction2 = {
      { "Move_Right" },
      { "Shoot_Stand_BigBow" },
  },
-- 공격 좌이동 공격
  CustomAction3 = {
      { "Move_Left" },
      { "Shoot_Stand_BigBow" },
  },
-- 좌측덤블링어택
  CustomAction4 = {
     { "Tumble_Left" },
     { "Shoot_Stand_BigBow" },
  },
-- 우측덤블링어택
  CustomAction5 = {
     { "Tumble_Right" },
     { "Shoot_Stand_BigBow" },
  },
-- 백덤블링어택
  CustomAction6 = {
     { "Tumble_Back" },
     { "Shoot_Stand_BigBow" },
  },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 6000

g_Lua_Near1 = { 
   { action_name = "Attack_Down", rate = 30, loop = 1, cooltime = 23000, target_condition = "State1" },
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "Move_Back", rate = -1, loop = 2 },
   { action_name = "Tumble_Back", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
   { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
   { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
   { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, cooltime = 7000, globalcooltime = 1 },
   { action_name = "Attack_SideKick", rate = 8, loop = 1, cooltime = 30000, globalcooltime = 1 },
   { action_name = "CustomAction6", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "Move_Back", rate = 1, loop = 1 },
   { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, cooltime = 7000, globalcooltime = 1 },
   { action_name = "CustomAction1", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
   { action_name = "CustomAction2", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
   { action_name = "CustomAction3", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 2 },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 20, loop = 4 },
}

g_Lua_NonDownMeleeDamage = {
      { action_name = "CustomAction4", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "CustomAction5", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "Move_Left", rate = 10, loop = 1 },
      { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_NonDownRangeDamage = {
      { action_name = "Stand", rate = 2, loop = 1 },
      { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, globalcooltime = 1 },
      { action_name = "CustomAction1", rate = 10, loop = 1, globalcooltime = 1 },
}

g_Lua_Skill = { 
   { skill_index = 31031,  cooltime = 10000, rate = 80,rangemin = 0, rangemax = 800, target = 3 },
}
