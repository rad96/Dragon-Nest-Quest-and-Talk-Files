--AiDragonSycophantCleric_White_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack|!Air", "Down" }, 
}

g_Lua_CustomAction = {
-- 쉴드차지 이후 밟기 공격
  CustomAction1 = {
      { "Attack6_ShieldCharge_ArcBishop" },
      { "Attack3_Down" },
  },
}

g_Lua_Near1 = { 
   { action_name = "Attack3_Down", rate = 15, loop = 1, target_condition = "State1" },
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
   { action_name = "Attack4_Kick", rate = 13, loop = 1, max_missradian = 20 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Attack5_MaceCombo", rate = 9, loop = 1, max_missradian = 20 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Attack7_JudgmentHammer_ArcBishopHell", rate = 17, loop = 1, max_missradian = 20 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "CustomAction1", rate = 27, loop = 1  },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 2 },
   { action_name = "Assault", rate = 9, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
}
g_Lua_Assault = { 
   { action_name = "Attack4_Kick", rate = 6, loop = 1, approach = 150 },
   { action_name = "Walk_Left", rate = 3, loop = 1, approach = 150 },
   { action_name = "Walk_Right", rate = 3, loop = 1, approach = 150 },
   { action_name = "Stand_1", rate = 2, loop = 1, approach = 150 },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "CustomAction1", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 3, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 1, loop = 1 },
}
g_Lua_Skill = { 
--디바인 아바타
   { skill_index = 30991, cooltime = 80000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, selfhppercent = 25 },
--토르 해머
   { skill_index = 30996, cooltime = 40000, rate = 100, rangemin = 0, rangemax = 1500, target = 3 },
}
