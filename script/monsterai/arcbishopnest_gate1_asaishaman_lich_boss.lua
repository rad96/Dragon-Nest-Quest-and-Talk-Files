--AiAsaiShaman_Lich_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 4000

g_Lua_GlobalCoolTime1 = 8000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 20532,  cooltime = 20000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, globalcooltime = 1 },
   { skill_index = 20522,  cooltime = 13000, rate = 70, rangemin = 0, rangemax = 1500, target = 3, globalcooltime = 1 },
   { skill_index = 20521,  cooltime = 13000, rate = 80,rangemin = 0, rangemax = 1500, target = 3, globalcooltime = 1 },
}
