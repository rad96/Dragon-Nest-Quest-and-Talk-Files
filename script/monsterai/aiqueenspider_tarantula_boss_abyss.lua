--AiQueenSpider_Tarantula_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1400;
g_Lua_NearValue4 = 1900;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 21000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Attack1_Slash_Lv2_Boss", rate = 9, loop = 1  },
   { action_name = "Attack2_Blow_Boss", rate = 9, loop = 1  },
   { action_name = "Attack3_JumpAttack_Lv2_Boss", rate = 4, loop = 1, randomtarget = "1.1" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack3_JumpAttack_Lv2_Boss", rate = 9, loop = 1, randomtarget = "1.1" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack3_JumpAttack_Lv2_Boss", rate = 13, loop = 1, randomtarget = "1.1" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Attack3_JumpAttack_Lv2_Boss", rate = 13, loop = 1, randomtarget = "1.1" },
}

g_Lua_Skill = {
-- ������ 75%
   { skill_index = 30196,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, limitcount = 1, next_lua_skill_index = 1 },
   { skill_index = 30202,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- ������ 25%
   { skill_index = 30196,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 25, limitcount = 1, next_lua_skill_index = 3 },
   { skill_index = 30202,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- �������
   { skill_index = 30200,  cooltime = 16000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, next_lua_skill_index = 5, randomtarget = "1.1", globalcooltime = 1 },
   { skill_index = 30198,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- �Ź̾�
   { skill_index = 30197,  cooltime = 21000, rate = 100, rangemin = 300, rangemax = 1500, target = 3 },
-- ��
   { skill_index = 30201,  cooltime = 21000, rate = 50, rangemin = 0, rangemax = 800, target = 3 },
-- �Ѹ�����
   { skill_index = 30199,  cooltime = 21000, rate = 70, rangemin = 0, rangemax = 1500, target = 3, globalcooltime = 1 },
-- �����Ź̺θ���
   { skill_index = 30195,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5500, target = 3, selfhppercent = 90, limitcount = 1 },
   { skill_index = 30195,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5500, target = 3, selfhppercent = 65, limitcount = 1 },
   { skill_index = 30195,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5500, target = 3, selfhppercent = 40, limitcount = 1 },
}