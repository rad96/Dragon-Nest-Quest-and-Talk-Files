-- Slime AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 250.0;
g_Lua_NearValue3 = 350.0;
g_Lua_NearValue4 = 600.0;


g_Lua_NoAggroOwnerFollow=1
g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;



g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
        --{ action_name = "Attack_Punch", rate = 30,              loop = 1 },
        --{ action_name = "Attack_Punch_B", rate = 30,              loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
        --{ action_name = "Attack_Punch", rate = 30,              loop = 1 },
       -- { action_name = "Attack_Punch_B", rate = 30,              loop = 1 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
}



g_Lua_Skill = { 
	{ skill_index = 30810, cooltime = 8000, rangemin = 0, rangemax = 300, target = 3, rate = 100 , combo1 = "3,100,2", combo2 = "4,100,0",priority=1 },
	{ skill_index = 30812, cooltime = 10000, rangemin = 0, rangemax = 200, target = 3, rate = 100 , combo1 = "3,100,2", combo2 = "4,100,0",priority=2 },
	{ skill_index = 30811, cooltime = 13000, rangemin = 0, rangemax = 200, target = 3, rate = 100 , combo1 = "3,100,2", combo2 = "4,100,0",priority=3 },
	{ skill_index = 2366, cooltime = 4000, rangemin = 0, rangemax = 300, target = 3, rate = 70, combo1 = "4,100,2", combo2 = "0,100,0" },
	{ skill_index = 2367, cooltime = 4000, rangemin = 0, rangemax = 300, target = 3, rate = 70, combo1 = "0,50,2", combo2 = "1,70,2", combo3 = "2,70,2" },

    {skill_index=260801,cooltime=0,rate=100,rangemin=0,rangemax=1000,target=2,selfhppercent=100,waitorder=260801, priority=3 },--세크리파이스Lv1

}
