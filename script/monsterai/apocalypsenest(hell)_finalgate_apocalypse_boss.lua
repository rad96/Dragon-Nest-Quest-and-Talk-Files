-- Apocalypse Normal A.I
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 600.0;
g_Lua_NearValue2 = 1000.0;
g_Lua_NearValue3 = 1500.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 500;
g_Lua_AssualtTime = 5000;

g_Lua_CustomAction =
{
	CustomAction1 = 
	{
      			{ "Attack001_ClawMelee1"  },
			{ "Attack003_Assault"  },	 
			{ "Attack021_Spin" },
	},
	CustomAction2 = 
	{
      			{ "Attack002_ClawRange1"  },
      			{ "Attack001_ClawMelee1_1" },
	},
	CustomAction3 = 
	{
      			{ "Attack001_ClawMelee1" },
      			{ "Move_Front", 0 },
			{ "Attack003_Assault", 0 },	 		
	},
	CustomAction4 = 
	{
      			{ "Move_Front" },
			{ "Attack003_Assault" },
			{ "Attack021_Spin" },
	 
	},
	CustomAction5 = 
	{
      			{ "Attack001_ClawMelee1" },
	},
}
g_Lua_RebirthParts =
{
	{ tableID="143,144,145,146,147", tick = 5000 },
}

g_Lua_Near1 = 
{ 
-- 0 ~ 600
-- 페이즈1 : 붙은 상태에서 계속 공격
	{ action_name = "Stand",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100", exitparts = "143" },
	{ action_name = "_Stand3",		rate = 30,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100", exitparts = "143" },
	{ action_name = "Move_Back",		rate = 15,		loop = 2, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143" },
	{ action_name = "Walk_Left",		rate = 20,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Walk_Right",		rate = 20,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "CustomAction5",	rate = 20,		loop = 1, td = "FL,FR", 	selfhppercentrange = "80,100", exitparts = "143" },
	{ action_name = "Attack018_Bite",	rate = 5,		loop = 1, td = "FL,FR", 	selfhppercentrange = "80,100", exitparts = "143" },
	{ action_name = "CustomAction4", 	rate = 5,		loop = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB", 	selfhppercentrange = "80,100", exitparts = "143" },
-- 페이즈2 : Walk 대신 Move로 이동한다.
	{ action_name = "Stand",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercentrange = "60,80", exitparts = "143" },
	{ action_name = "_Stand3",		rate = 20,		loop = 1, td = "FL,RF", 	selfhppercentrange = "60,80", exitparts = "143" },
	{ action_name = "Move_Back",		rate = 15,		loop = 2, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143" },
	{ action_name = "Attack018_Bite",	rate = 10,		loop = 1, td = "FL,FR", 	selfhppercentrange = "60,80", exitparts = "143" },
	{ action_name = "CustomAction5",	rate = 12,		loop = 1, td = "FL,FR", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Walk_Left",		rate = 10,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Walk_Right",		rate = 10,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "CustomAction4",	rate = 5,		loop = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB", 	selfhppercentrange = "60,80", exitparts = "143" },
-- 페이즈3~4 : 적으로부터 떨어지려 한다.
	{ action_name = "Stand",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercentrange = "20,60", exitparts = "143" },
	{ action_name = "_Stand4",		rate = 20,		loop = 1, td = "FL,RF", 	selfhppercentrange = "20,60", exitparts = "143" },
	{ action_name = "Attack018_Bite",	rate = 10,		loop = 1, td = "FL,FR", 	selfhppercentrange = "20,60", exitparts = "143" },
	{ action_name = "Move_Back",		rate = 10,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "20,60", exitparts = "143" },
	{ action_name = "Attack020_Jump",	rate = 20,		loop = 3, td = "FL,FR",		selfhppercentrange = "20,60", cooltime = 15000, exitparts = "143"  },
	{ action_name = "CustomAction5",	rate = 10,		loop = 1, td = "FL,FR", 	selfhppercentrange = "20,60", exitparts = "143" },
	{ action_name = "CustomAction4",	rate = 5,		loop = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB", 	selfhppercentrange = "20,60", exitparts = "143" },
-- 페이즈5 : 스킬 빈도를 높이기 위해 일반행동 빈도를 줄인다.
	{ action_name = "Stand",		rate = 5,		loop = 1, td = "FL,RF",		selfhppercent = 20, exitparts = "143" },
	{ action_name = "Move_Back",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercent = 20, exitparts = "143" },
	{ action_name = "CustomAction5",	rate = 10, 		loop = 1, td = "FL,FR", 	selfhppercent = 20, exitparts = "143", cooltime = 20000 },
}
g_Lua_Near2 = 
{ 
-- 600 ~ 1000
-- 페이즈1 : 좌/우로 이동하면서 원거리 공격을 취한다.
	{ action_name = "Stand",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100", exitparts = "143" },
	{ action_name = "_Stand3",		rate = 20,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100", exitparts = "143" },
	{ action_name = "Move_Front",		rate = 8,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143" },
	{ action_name = "Walk_Left",		rate = 10,		loop = 2, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Walk_Right",		rate = 10,		loop = 2, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Attack002_ClawRange1",	rate = 15,		loop = 1, td = "FL,FR", 	selfhppercentrange = "80,100", exitparts = "143" },
	{ action_name = "CustomAction2",	rate = 15,		loop = 1, td = "FL,FR", 	selfhppercentrange = "80,100", exitparts = "143" },
	{ action_name = "CustomAction4",	rate = 5,		loop = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB", 	selfhppercentrange = "80,100", exitparts = "143" },
-- 페이즈2 : Walk 대신 Move로 이동한다.
	{ action_name = "Stand",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "_Stand3",		rate = 25,		loop = 1, td = "FL,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Walk_Left",		rate = 5,		loop = 2, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Walk_Right",		rate = 5,		loop = 2, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 10,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Attack003_Assault",	rate = 8, 		loop = 1, td = "FL,FR", 	selfhppercentrange = "60,80", exitparts = "143", cooltime=30000 },
	{ action_name = "CustomAction2",	rate = 15,		loop = 1, td = "FL,FR", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "CustomAction4",	rate = 5,		loop = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB", 	selfhppercentrange = "60,80", exitparts = "143" },
-- 페이즈3~4 : 적으로부터 떨어지려 한다.
	{ action_name = "Stand",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercentrange = "20,60", exitparts = "143" },
	{ action_name = "_Stand4",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "20,60", exitparts = "143" },
	{ action_name = "Move_Back",		rate = 10,		loop = 1, td = "FL,FR,LF,RF",	selfhppercentrange = "20,60", exitparts = "143" },
	{ action_name = "Attack002_ClawRange1",	rate = 5,		loop = 1, td = "FL,FR", 	selfhppercentrange = "20,60", exitparts = "143" },
	{ action_name = "Attack003_Assault",	rate = 5, 		loop = 1, td = "FL,FR", 	selfhppercentrange = "20,60", exitparts = "143", cooltime=30000 },
	{ action_name = "Attack020_Jump",	rate = 15,		loop = 3, td = "FL,FR",		selfhppercentrange = "20,60", cooltime = 15000, exitparts = "143"  },
-- 페이즈5 : 스킬 빈도를 높이기 위해 일반행동 빈도를 줄인다.
	{ action_name = "Stand",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercent = 20, exitparts = "143"  },
	{ action_name = "Move_Back",		rate = 5,		loop = 1, td = "FL,FR,LF,RF",	selfhppercent = 20, exitparts = "143"  },
	{ action_name = "Attack002_ClawRange1",	rate = 10,		loop = 1, td = "FL,FR", 	selfhppercent = 20, exitparts = "143" },
}

g_Lua_Near3 = 
{ 
-- 1000 ~ 1500
-- 페이즈1 : 타겟을 향해 돌진한다. 타겟과 멀이지지 않으려 한다.
	{ action_name = "Stand",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "_Stand3",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 20,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Walk_Left",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Walk_Right",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Attack002_ClawRange1",	rate = 15,		loop = 1, td = "FL,FR", 	selfhppercentrange = "80,100", exitparts = "143"  },
-- 페이즈2 : Walk 대신 Move로 이동한다.
	{ action_name = "Stand",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "_Stand3",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 15,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "CustomAction4",	rate = 10,		loop = 1, td = "FL,FR", 	selfhppercentrange = "60,80", cooltime=30000, exitparts = "143"  },
	{ action_name = "Walk_Left",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Walk_Right",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "CustomAction2",	rate = 15,		loop = 1, td = "FL,FR", 	selfhppercentrange = "60,80", exitparts = "143"  },
-- 페이즈3~4 : 적으로부터 거리를 유지한다.
	{ action_name = "Stand",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "20,60", exitparts = "143"  },
	{ action_name = "_Stand4",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "20,60", exitparts = "143"  },
	{ action_name = "Move_Back",		rate = 2,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "20,60", exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 10,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "20,60", exitparts = "143"  },
	{ action_name = "Attack002_ClawRange1",	rate = 10,		loop = 1, td = "FL,FR", 	selfhppercentrange = "20,60", exitparts = "143"  },
-- 페이즈5 : 스킬 빈도를 높이기 위해 일반행동 빈도를 줄인다.
	{ action_name = "Stand",		rate = 15,		loop = 1, td = "FL,RF", 	selfhppercent = 20, exitparts = "143"  },
	{ action_name = "Move_Back",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercent = 20, exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 20,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercent = 20, exitparts = "143"  },
	{ action_name = "Attack002_ClawRange1",	rate = 10,		loop = 1, td = "FL,FR", 	selfhppercent = 20, exitparts = "143"  },
}

g_Lua_Near4 = 
{ 
-- 1500 ~ 2000
-- 페이즈1 : 타겟을 향해 돌진한다. 타겟과 멀이지지 않으려 한다.
	{ action_name = "Stand",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "_Stand3",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 30,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Walk_Left",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Walk_Right",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
-- 페이즈2 : Walk 대신 Move로 이동한다.
	{ action_name = "Stand",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "_Stand3",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 10,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Attack003_Assault",	rate = 15,		loop = 1, td = "FL,FR", 	selfhppercentrange = "60,80", cooltime=30000, exitparts = "143"  },
	{ action_name = "Walk_Left",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Walk_Right",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
-- 페이즈3~4 : 적으로부터 거리를 유지한다.
	{ action_name = "Stand",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "20,60", exitparts = "143"  },
	{ action_name = "_Stand4",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercentrange = "20,60", exitparts = "143"  },
	{ action_name = "Move_Back",		rate = 2,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "20,60", exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 10,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "20,60", exitparts = "143"  },
-- 페이즈5 : 스킬 빈도를 높이기 위해 일반행동 빈도를 줄인다.
	{ action_name = "Stand",		rate = 10,		loop = 1, td = "FL,RF", 	selfhppercent = 20, exitparts = "143"  },
	{ action_name = "Move_Back",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercent = 20, exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 10,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercent = 20, exitparts = "143"  },
}

g_Lua_Near5 = 
{ 
-- 2000 ~ 3500
-- 페이즈1 : 타겟을 향해 돌진한다. 타겟과 멀이지지 않으려 한다.
	{ action_name = "Stand",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "_Stand3",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 20,		loop = 3, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Walk_Left",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
	{ action_name = "Walk_Right",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "80,100", exitparts = "143"  },
-- 페이즈2 : Walk 대신 Move로 이동한다.
	{ action_name = "Stand",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "_Stand3",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 20,		loop = 3, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Attack003_Assault",	rate = 10, 		loop = 1, td = "FL,FR", 	selfhppercentrange = "60,80", cooltime=30000, exitparts = "143"  },
	{ action_name = "Walk_Left",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
	{ action_name = "Walk_Right",		rate = 5,		loop = 1, td = "FL,FR,LF,RF", 	selfhppercentrange = "60,80", exitparts = "143"  },
-- 페이즈3~4 : 적에게 다가간다.
	{ action_name = "Stand",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercentrange = "20,60", exitparts = "143"  },
	{ action_name = "_Stand4",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercentrange = "20,60", exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 10,		loop = 2, td = "FL,FR,LF,RF", 	selfhppercentrange = "20,60", exitparts = "143"  },
	{ action_name = "Attack003_Assault",	rate = 3, 		loop = 1, td = "FL,FR", 	selfhppercentrange = "20,60",cooltime=30000, exitparts = "143"  },
-- 페이즈5 : 스킬 빈도를 높이기 위해 일반행동 빈도를 줄인다.
	{ action_name = "Stand",		rate = 5,		loop = 1, td = "FL,RF", 	selfhppercent = 20, exitparts = "143"  },
	{ action_name = "Move_Front",		rate = 10,		loop = 2, td = "FL,FR,LF,RF", 	selfhppercent = 20, exitparts = "143"  },
}

g_Lua_SkillProcessor = {
   { skill_index = 30207,  changetarget = "3000,1"  },
}

g_Lua_Skill = { 
-- ___________페이즈1__________
-- 샤우트
--   { skill_index = 30205,  cooltime = 50000, 	rate = 20, 	rangemin = 400, rangemax = 1000,target = 3, selfhppercentrange = "80,100", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- 스트레치 아웃
   { skill_index = 30206,  cooltime = 18000, 	rate = 70, 	rangemin = 00, rangemax = 1200, target = 3, selfhppercentrange = "80,100", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- 이베포레이션1
   { skill_index = 30457,  cooltime = 9000, 	rate = 100, 	rangemin = 0, rangemax = 4000, 	target = 3, selfhppercentrange = "80,100", td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = 1, exitparts = "143" },
-- ___________페이즈2__________
-- 이베포레이션2
   { skill_index = 30458,  cooltime = 20000,	rate = 90, 	rangemin = 0, rangemax = 4000, 	target = 3, selfhppercentrange = "60,80", td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = 1, exitparts = "143" },
-- 레이저1
   { skill_index = 30208,  cooltime = 20000, 	rate = 90, 	rangemin = 0, rangemax = 2000, 	target = 3, selfhppercentrange = "60,80", td = "FL,RF", exitparts = "143" },
   { skill_index = 30208,  cooltime = 1000000, 	rate = 100, 	rangemin = 0, rangemax = 2000, 	target = 3, selfhppercentrange = "60,80", td = "FL,RF", exitparts = "143" },
-- 레이저2
   { skill_index = 30209,  cooltime = 20000, 	rate = 90, 	rangemin = 0, rangemax = 2000, 	target = 3, selfhppercentrange = "60,80", td = "FL,RF", exitparts = "143" },
   { skill_index = 30209,  cooltime = 1000000, 	rate = 100, 	rangemin = 0, rangemax = 2000, 	target = 3, selfhppercentrange = "60,80", td = "FL,RF", exitparts = "143" },
-- 스트레치 아웃
   { skill_index = 30206,  cooltime = 20000, 	rate = 50, 	rangemin = 0, rangemax = 800, 	target = 3, selfhppercentrange = "60,80", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- 샤우트
--   { skill_index = 30205,  cooltime = 30000, 	rate = 70, 	rangemin = 400, rangemax = 1000,target = 3, selfhppercentrange = "60,80", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- ___________페이즈3__________
-- 이베포레이션3
   { skill_index = 30459,  cooltime = 30000,	rate = 90, 	rangemin = 0, rangemax = 2000, 	target = 3, selfhppercentrange = "40,60", td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = 1, exitparts = "143" },
-- 훨윈드
   { skill_index = 30207,  cooltime = 80000, 	rate = 70, 	rangemin = 400, rangemax = 1000,target = 3, selfhppercentrange = "40,60", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- 레이저1
   { skill_index = 30208,  cooltime = 40000, 	rate = 50, 	rangemin = 0, rangemax = 2000, 	target = 3, selfhppercentrange = "40,60", td = "FL,RF", exitparts = "143" },
-- 레이저2
   { skill_index = 30209,  cooltime = 40000, 	rate = 50, 	rangemin = 0, rangemax = 2000, 	target = 3, selfhppercentrange = "40,60", td = "FL,RF", exitparts = "143" },
-- 스트레치 아웃
   { skill_index = 30206,  cooltime = 20000, 	rate = 50,	rangemin = 0, rangemax = 800, 	target = 3, selfhppercentrange = "40,60", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143"  },
-- 샤우트
--   { skill_index = 30205,  cooltime = 50000, 	rate = 40, 	rangemin = 400, rangemax = 1000,target = 3, selfhppercentrange = "40,60", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- ___________페이즈4__________
-- 오망성
   { skill_index = 30211,  cooltime = 100000,	rate = 100, 	rangemin = 0, rangemax = 3500, 	target = 3, selfhppercentrange = "20,40", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
   { skill_index = 30211,  cooltime = 30000, 	rate = 50, 	rangemin = 0, rangemax = 3500, 	target = 3, selfhppercentrange = "20,40", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- 크로우세퍼릿
   { skill_index = 30456,  cooltime = 20000, 	rate = 90, 	rangemin = 0, rangemax = 1000, 	target = 3, selfhppercentrange = "20,40", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- 훨윈드
   { skill_index = 30455,  cooltime = 70000, 	rate = 30, 	rangemin = 400,rangemax = 1000,	target = 3, selfhppercentrange = "20,40", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- 스트레치 아웃
   { skill_index = 30206,  cooltime = 20000, 	rate = 70, 	rangemin = 0, rangemax = 800, 	target = 3, selfhppercentrange = "20,40", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- 샤우트
--   { skill_index = 30205,  cooltime = 50000, 	rate = 40, 	rangemin = 400, rangemax = 1000,target = 3, selfhppercentrange = "20,40", td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- ___________페이즈5__________
-- 아포칼립스
   { skill_index = 30460,  cooltime = 25000, 	rate = 90,	rangemin = 0, rangemax = 4000, 	target = 3, selfhppercent = 20, td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = 1, exitparts = "143" },
-- 크로우세퍼릿
   { skill_index = 30456,  cooltime = 30000, 	rate = 70, 	rangemin = 0, rangemax = 1000, 	target = 3, selfhppercent = 20, td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- 오망성
   { skill_index = 30211,  cooltime = 30000, 	rate = 50, 	rangemin = 0, rangemax = 3500, 	target = 3, selfhppercent = 20, td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- 스트레치 아웃
   { skill_index = 30206,  cooltime = 20000, 	rate = 60, 	rangemin = 0, rangemax = 800, 	target = 3, selfhppercent = 20, td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- 샤우트
--   { skill_index = 30205,  cooltime = 50000, 	rate = 40, 	rangemin = 400, rangemax = 1000,target = 3, selfhppercent = 20, td = "FL,FR,LF,RF,RB,BR,BL,LB", exitparts = "143" },
-- ___________시간제한 11분__________
-- 플래어
   { skill_index = 30216,  cooltime = 75000, 	rate = 100, 	rangemin = 0, rangemax = 3500, 	target = 3, selfhppercent = 100, td = "FL,FR,LF,RF,RB,BR,BL,LB", encountertime = 660000, exitparts = "143" },
}