g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2200;
g_Lua_NearValue5 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack2_Slash_GreenDragon", 0 },
      { "Attack3_JumpChopping", 0 },
  },
}
g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack2_Slash_GreenDragon", rate = 15, loop = 1  },
   { action_name = "CustomAction1", rate = 30, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack2_Slash_GreenDragon", rate = 45, loop = 1  },
   { action_name = "Assault", rate = 15, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Assault", rate = 60, loop = 1  },
}
g_Lua_Near4 = { 
   -- { action_name = "Stand_1", rate = 15, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 1  },
   -- { action_name = "Walk_Right", rate = 5, loop = 1  },
   -- { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   -- { action_name = "Assault", rate = 96, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 20, loop = 1  },
}


g_Lua_Assault = { 
   { action_name = "CustomAction1", rate = 5, loop = 1, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 20380,  cooltime = 35000, rate = 30, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 70 },--�������
   { skill_index = 20381,  cooltime = 30000, rate = 40, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 40 },--������
   -- { skill_index = 20382,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 70 },--��������
   -- { skill_index = 20383,  cooltime = 50000, rate = 100, rangemin = 0, rangemax = 2000, target = 1, selfhppercent = 50 },--����ȭ
   -- { skill_index = 20384,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100 },--�͵�����
   -- { skill_index = 20385,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 70 },--ȭ������
   -- { skill_index = 20386,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 70 },--������
   { skill_index = 31325,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 1400, target = 3, selfhppercent = 100, limitcount = 1},--�������
}
