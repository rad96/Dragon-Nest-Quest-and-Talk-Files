--AiBoarManGunner_Gray_Elite_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 10000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Attack01_Shot", rate = 40.5, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Attack01_Shot", rate = 30, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Move_Front", rate = 20, loop = 6  },
}
g_Lua_Skill = { 
   { skill_index = 20342,  cooltime = 18000, rate = 100,rangemin = 0, rangemax = 500, target = 3 },
   { skill_index = 20341,  cooltime = 6000, rate = 100, rangemin = 500, rangemax = 10000, target = 3 },
}
