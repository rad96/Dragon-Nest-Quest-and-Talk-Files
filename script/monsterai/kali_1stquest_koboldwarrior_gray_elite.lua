--AiKoboldWarrior_Gray_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 120;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1400;


g_Lua_PatrolBaseTime = 3000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000


g_Lua_CustomAction = {
	CustomAction1 = {
	 { "Move_Back", 0},
	 { "Attack6_RollingThrust" , 0},
 },
	CustomAction2 = {
	 { "Move_Left" , 1},
	 { "Attack12_Sever_Strong" , 0},
 },
	CustomAction3 = {
	 { "Move_Right" , 1},
	 { "Attack12_Sever_Strong" , 0},
 },
    CustomAction4 = {
	{"Move_Front" , 5},
	{"Attack12_Sever_Strong" , 0},
}
}


g_Lua_Near1 = { 
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Attack_Chop", rate = 5, loop = 0  },
}

g_Lua_Near2 = { 
   { action_name = "Move_Front", rate = 5, loop = 3  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
   { action_name = "CustomAction1", rate = 3, loop = 1},
   { action_name = "CustomAction2", rate = 3, loop = 1  },
   { action_name = "CustomAction3", rate = 3, loop = 1  },
   { action_name = "Attack10_Nanmu", rate = 3, loop = 0},
   { action_name = "Attack12_Sever_Strong", rate = 5, loop = 0  },
   { action_name = "Attack6_RollingThrust", rate = 5, loop = 0  },
}


g_Lua_Near3 = { 
   { action_name = "Move_Front", rate = 5, loop = 5  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Attack5_JumpAttack", rate = 3, loop = 0 },
   { action_name = "CustomAction4", rate = 3, loop = 0 },
}

g_Lua_Near4 = {
   { action_name = "Stand", rate = 5, loop = 3 },
   { action_name = "Move_Front", rate = 10, loop = 8 },
   { action_name = "Walk_Left", rate = 5, loop = 4 },
   { action_name = "Walk_Right", rate = 5, loop = 4 },
}

g_Lua_Skill = { 
   { skill_index = 20031,  cooltime = 20000, rate = 100 , rangemin = 0, rangemax = 500, target = 1, selfhppercent = 20 },
}
