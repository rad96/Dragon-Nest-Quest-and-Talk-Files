--GuildWar_Ballista_Fire.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 1000;
g_Lua_NearValue2 = 2500;
g_Lua_NearValue3 = 2700;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Turn_Left", rate = 5, loop = 1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 5, loop = 1, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Turn_Left", rate = 5, loop = 1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 5, loop = 1, td = "RF,RB,BR" },
   { action_name = "Attack1", rate = 15, loop = 1, td = "FL,FR", cooltime = 23000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Turn_Left", rate = 5, loop = 1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 5, loop = 1, td = "RF,RB,BR" },
}