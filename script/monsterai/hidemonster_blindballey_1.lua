
g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1500;
g_Lua_NearValue3 = 2000;
g_Lua_NearValue4 = 4500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 120;
g_Lua_AssualtTime = 3000;


g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1 },
}

g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1 },
}

g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1 },
     { action_name = "Attack_flock", rate = 25, loop = 1, cooltime = 5000 },
}

g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 20976,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 1, limitcount = 1 },
}