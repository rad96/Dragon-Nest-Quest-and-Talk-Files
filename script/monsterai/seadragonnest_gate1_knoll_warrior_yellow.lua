--AiKnoll_Warrior_Yellow_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 50;
g_Lua_NearValue2 = 150;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 50000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Move_Back", 1 },
      { "Attack2_HeavyAttack", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 15, loop = 2  },
   { action_name = "Attack1_ShotAttack", rate = 19.5, loop = 1 },
   { action_name = "CustomAction1", rate = 19.5, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 8, loop = 2  },
   { action_name = "Attack1_ShotAttack", rate = 27, loop = 1  },
   { action_name = "CustomAction1", rate = 13.5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 4, loop = 1  },
   { action_name = "CustomAction1", rate = 13.5, loop = 1 },
   { action_name = "Attack2_HeavyAttack", rate = 13.5, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 8, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 5, loop = 7  },
}
g_Lua_Skill = { 
   { skill_index = 20331,  cooltime = 15000, rate = 80,rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 90 },
}
