--AiDragonSycophantBishop_White.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   -- { action_name = "Move_Left", rate = 7, loop = 1  },
   -- { action_name = "Move_Right", rate = 7, loop = 1  },
   -- { action_name = "Move_Back", rate = 15, loop = 2  },
   { action_name = "Attack1_Push", rate = 80, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   -- { action_name = "Move_Left", rate = 5, loop = 1  },
   -- { action_name = "Move_Right", rate = 5, loop = 1  },
   -- { action_name = "Move_Back", rate = 8, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
}
g_Lua_Skill = { 
-- 라이트닝 볼트
   { skill_index = 20426,  cooltime = 5000, rate = 30, rangemin = 150, rangemax = 1000, target = 3, },
-- 불량문장_전기
   { skill_index = 20428,  cooltime = 15000, rate = 30, rangemin = 0, rangemax = 1200, target = 3, },
-- 라이트닝 필드
   { skill_index = 20427,  cooltime = 10000, rate = 30, rangemin = 0, rangemax = 1000, target = 3 },
}

