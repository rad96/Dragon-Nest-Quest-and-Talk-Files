--AiAsaiShaman_Blue_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 4000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 2 },
   { action_name = "Walk_Right", rate = 10, loop = 2 },
   { action_name = "Walk_Back", rate = 15, loop = 2 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "Move_Back", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 2 },
   { action_name = "Walk_Right", rate = 10, loop = 2 },
   { action_name = "Move_Left", rate = 3, loop = 1 },
   { action_name = "Move_Right", rate = 3, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Walk_Left", rate = 10, loop = 2 },
   { action_name = "Walk_Right", rate = 3, loop = 2 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 3 },
   { action_name = "Walk_Left", rate = 2, loop = 2 },
   { action_name = "Walk_Right", rate = 2, loop = 2 },
   { action_name = "Move_Front", rate = 5, loop = 2 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
}
g_Lua_Skill = { 
   { skill_index = 30961,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 1500, target = 4, selfhppercentrange = "99,100" },
   { skill_index = 30962,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 1500, target = 4, selfhppercentrange = "75,98" },
   { skill_index = 30963,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 1500, target = 4, selfhppercentrange = "50,74" },
}
