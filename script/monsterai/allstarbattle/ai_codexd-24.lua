
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" },
 State2 = {"Stay|Move|Attack", "!Down","!Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Down|!Move","Air"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 신 브레이커
  CustomAction6 = {
     { "useskill", lua_skill_index = 23, rate = 100 },
  },
  -- 
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 15, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Right", rate = 5, loop = 1, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 7200, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 17600, target_condition = "State4" },
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 20, loop = 1 },
     { action_name = "Move_Right", rate = 20, loop = 1 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 7200, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 17600, globalcooltime = 2, target_condition = "State4" },
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}

g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
     --{ action_name = "SKill_Dash", rate = -1, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Tumble_Back", rate = 20, loop = 1, globalcooltime = 1 },
	 { action_name = "Skill_StandOfFaith_Guard", rate = 30, loop = 1, cooltime = 15000 },
     { action_name = "CustomAction2", rate = 10, loop = 1  },
     { action_name = "CustomAction3", rate = 10, loop = 1 },
}

g_Lua_NonDownRangeDamage = {

	 { action_name = "useskill", lua_skill_index = 26, rate = 30 },
	 { action_name = "useskill", lua_skill_index = 18, rate = 50 },
}

g_Lua_Skill = { 
-- 0 퀵샷
     { skill_index = 84001, cooltime = 4500, rate = 20, rangemin = 0, rangemax = 1200, target = 3, combo1 = "23,60,1" },-- 액션프레임끊기 nextcustomaction 시그널
-- 1 에어 샷
     { skill_index = 84002, cooltime = 6400, rate = 30, rangemin = 0, rangemax = 200, target = 3, combo1 = "10,40,1", combo2 = "2,100,0", target_condition = "State3" },
-- 2 에어 샷2
     { skill_index = 84003, cooltime =1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "0,50,1", combo2 = "50,100,0"  },-- 케릭터 어그로유지 LocktargetLook
-- 3 스패너 스매싱
     { skill_index = 84004, cooltime = 0, rate = 50, rangemin = 0, rangemax = 150, target = 3, combo1 ="50,100,1" },
-- 4 왁스
	 { skill_index = 84005, cooltime = 24000, rate = 50, rangemin = 0, rangemax = 1200, target = 3, priority= 3 },-- 인풋으로 발동하는 건 액션마다 테이블 다 등록
-- 5 메그너피어 어택
     { skill_index = 84006, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 100, target = 3, combo1 = "1,50,1", combo2 = "10,100,1", target_condition = "State1"},
-- 6 서클 밤
     { skill_index = 84007, cooltime = 12800, rate = -1, rangemin = 0, rangemax = 200, target = 3 },
	 
-- 7 버블버블
     { skill_index = 84008, cooltime = 24000, rate = 30, rangemin = 0, rangemax = 400, target = 3, combo1 = " 35,50,0", combo2 = "23,60,0", combo3 = "25,100,0" },
-- 8 에어 붐
     { skill_index = 84009, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 = "50,100,0"  },
-- 9 메가 해머
     { skill_index = 84010, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 300, target = 3, blowcheck = "23" },
-- 10 포스 아웃
     { skill_index = 84011, cooltime = 8000, rate = 40, rangemin = 0, rangemax = 170, target = 3,priority= 3, combo1= "23,50,1", combo2 = "24,100,1", target_condition = "State3"  },

	 
-- 11페이크 밤F
     { skill_index = 84012, cooltime = 4800, rate = 20, rangemin = 0, rangemax = 400, target = 3, globalcooltime = 1, combo1 = "10,40,1" },
-- 12 페이크 밤B
     { skill_index = 84013, cooltime = 4800, rate = 20, rangemin = 0, rangemax = 400, target = 3, globalcooltime = 1, combo1 = "37,100,1" },
-- 13 페이크 밤L
     { skill_index = 84025, cooltime = 4800, rate = 20, rangemin = 0, rangemax = 400, target = 3, globalcooltime = 1, combo1 = "25,100,1" },	  
-- 14 페이크 밤R
     { skill_index = 84031, cooltime = 4800, rate = 20, rangemin = 0, rangemax = 400, target = 3, globalcooltime = 1, combo1 = "17,40,0" , combo2 = "11,50,1" },
-- 15 스턴 그레네이드
     { skill_index = 84014, cooltime = 8000, rate = 30, rangemin = 0, rangemax = 900, target = 3 , combo1= "25,50,0", combo2 = "32,50,0" },

-- 16 서먼 알프레도
     { skill_index = 84015, cooltime = 90000, rate = 100, rangemin = 400, rangemax = 900, target = 3, selfhppercent=100, priority= 3,selfhppercent=70 },

-- 17 에리얼 이베이전
     { skill_index = 84016, cooltime = 11520, rate = -1, rangemin = 0, rangemax = 500, target = 3, combo1 = "37,60,1", combo2 = "26,100,1" },

-- 18 로켓 점프F
     { skill_index = 84017, cooltime = 8000, rate = 30, rangemin = 0, rangemax = 700, target = 3, combo1 = "50,100,1" },
-- 19 로켓 점프B
     { skill_index = 84018, cooltime = 8000, rate = -1, rangemin = 400, rangemax = 900, target = 3, cancellook = 1, selfhppercent=50 },	
-- 20 로켓 점프L
     { skill_index = 84019, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 700, target = 3 },
-- 21 로켓 점프R
     { skill_index = 84031, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1 },
	 
-- 22 오토멧
     { skill_index = 84020, cooltime = 200000, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "26,80,1", combo2 = "25,20,1", combo3 = "28,30,0" },
-- 23 핑퐁 밤
     { skill_index = 84021, cooltime = 9600, rate = 50, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "50,40,1", combo2 = "34,50,1", combo3 = "25,100,0" },
-- 24 마인쓰로어
     { skill_index = 84022, cooltime = 22400, rate = 20, rangemin = 400, rangemax = 900, target = 3, cancellook = 1, combo1 = "23,100,0" },
-- 25 그라비티 그레네이드
     { skill_index = 84023, cooltime = 11200, rate = 30, rangemin = 50, rangemax = 900, target = 3, cancellook = 1, combo1 = "7,100,0" },
-- 26 캐미컬 미사일
     { skill_index = 84024, cooltime = 36000, rate = 30, rangemin = 0, rangemax = 900, target = 3, combo1 = "34,50,0", combo2 = "35,100,0" },

-- 27 개틀링 타워
     { skill_index = 84027, cooltime = 16000, rate = 20, rangemin = 0, rangemax = 900, target = 3 , priority= 2,selfhppercent=60},
-- 28 아이스 펌프 타워
     { skill_index = 84028, cooltime = 28000, rate = 30, rangemin = 0, rangemax = 900, target = 3, combo1 = "29,100,0" , priority= 3 ,selfhppercent=80},
-- 29 캐논 타워
     { skill_index = 84029, cooltime = 20000, rate = 30, rangemin = 0, rangemax = 900, target = 3, combo1 = "28,100,0" , priority= 2,selfhppercent=60},	
	 
-- 30 데미지 트렌지션
     { skill_index = 84035, cooltime = 48000, rate = 80, rangemin = 0, rangemax = 900, target = 3, selfhppercent=100, usedskill = "84015"},	
-- 31플래쉬 그레네이드
     { skill_index = 84041, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 =" 46,40,0", combo2 = "47,50,0" },--몬스터는 점프시그널이 안뎀 그래서 벨로시티시그널 써야뎀, 몬스터는 크로스헤어로 타겟잡으면 안뎀
-- 32 네이팜
     { skill_index = 84042, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "28,100,0" },
-- 33 리콜 알프레도
     { skill_index = 84043, cooltime = 36000, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "35,30,0", combo2 = "10,30,0", combo3= "46,30,0", combo4= "47,30,0", combo5 = "28,40,0", combo6 = "11,100,0" },
-- 34 메카덕EX
     { skill_index = 84045, cooltime = 15000, rate = 30, rangemin = 0, rangemax = 800, target = 3, combo1 = "15,100,0" , priority= 3},	
-- 35 빅 메카 붐버
     { skill_index = 84030, cooltime = 29000, rate = 20, rangemin = 0, rangemax = 700, target = 3, combo1 ="36,20,0" },
-- 36 빅 메카 붐버1
     { skill_index = 84046, cooltime =1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "38,100,0", combo2 = "8,100,0" },	 
-- 37 빅 메카 붐버2
     { skill_index = 84050, cooltime =1000, rate = -1, rangemin = 0, rangemax = 700, target = 3 },	
-- 38 메카 쇼크EX
     { skill_index = 84047, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 900, target = 3 },	
-- 39 메카 사이렌 EX
     { skill_index = 84048, cooltime = 18000, rate = -1, rangemin = 0, rangemax = 900, target = 3 },--몬스터는 점프시그널이 안뎀 그래서 벨로시티시그널 써야뎀, 몬스터는 크로스헤어로 타겟잡으면 안뎀
-- 40 메카 붐버 EX
     { skill_index = 84049, cooltime = 25000, rate = 20, rangemin = 0, rangemax = 900, target = 3 },
-- 41 점프
     { skill_index = 84051, cooltime = 10000, rate = 30, rangemin = 0, rangemax = 300, target = 3, combo1 = "52,100,0" },

-- 42 닷지F
     { skill_index = 84053, cooltime =2800, rate = -1, rangemin = 0, rangemax = 300, target = 3 , combo1 = "2,100,1" },	 
-- 43 닷지B
     { skill_index = 84054, cooltime =2800, rate = -1, rangemin = 0, rangemax = 300, target = 3 , combo1 = "2,100,1" },	 
-- 44 닷지L
     { skill_index = 84055, cooltime =2800, rate = -1, rangemin = 0, rangemax = 300, target = 3 , combo1 = "2,100,1" },	 
-- 45 닷지R
     { skill_index = 84056, cooltime =2800, rate = -1, rangemin = 0, rangemax = 300, target = 3 , combo1 = "2,100,1" },	 

-- 46 다운구르기F
     { skill_index = 84057, cooltime =0, rate = -1, rangemin = 0, rangemax = 300, target = 3 },
-- 47 다운구르기B
     { skill_index = 84058, cooltime =0, rate = -1, rangemin = 0, rangemax = 300, target = 3 },
-- 48 다운구르기L
     { skill_index = 84059, cooltime =0, rate = -1, rangemin = 0, rangemax = 300, target = 3},
-- 49 다운구르기R
     { skill_index = 84060, cooltime =0, rate = -1, rangemin = 0, rangemax = 300, target = 3 },	 

-- 50 평타
     { skill_index = 84052, cooltime =100, rate = -1, rangemin = 0, rangemax = 900, target = 3 , combo1 = "1,50,0", combo2 = "23,50,0", combo3 = "34,100,0" , target_condition = "State3"},	 
-- 51 체인소우타워
     { skill_index = 84044, cooltime =27000, rate = 30, rangemin = 0, rangemax = 900, target = 3, priority= 3 , selfhppercent=30},	 
-- 52 점프 평타
     { skill_index = 84040, cooltime =100, rate = -1, rangemin = 0, rangemax = 300, target = 3 , combo1 = "8,100,0" },	 

	 }