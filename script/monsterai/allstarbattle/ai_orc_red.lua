--AiOrc_Green_Boss_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 300;
g_Lua_NearValue4 = 600;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Down|Air","!Move"},  -- 대상이 뜨거나 다운중
}

g_Lua_CustomAction = {
  -- 근거리 방패 > 띄우기
  CustomAction1 = 
   {
	   { "Attack1_Pushed" },
	   { "Attack3_Air" },
   },
   -- 근거리 방패 > 난무
   CustomAction2 = 
   {
	   { "Attack1_Pushed" },
	   { "Attack7_Nanmu" },
   },
   -- 근거리 내리치기 > 돌진
   CustomAction3 = 
   {
	   { "Attack2_bash" },
	   { "Attack9_DashAttack" },
   },
   -- 중거리 강타 > 점프내려치기
   CustomAction4 = 
   {
	   { "Attack6_HeavyAttack" },
	   { "Attack10_AssaultStrike" },
   },
}

g_Lua_GlobalCoolTime1 = 8000

g_Lua_Near1 = { 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Walk_Back",	rate = 5,		loop = 1},
   { action_name = "CustomAction1", rate = 17, loop = 1  },
   { action_name = "Attack3_Air", rate = 20, loop = 1  },
}
g_Lua_Near2 = { 
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
	{ action_name = "Walk_Back",	rate = 5,		loop = 1},
   { action_name = "CustomAction1", rate = 11, loop = 1  },
   { action_name = "Attack3_Air", rate = 20, loop = 1  },
}
g_Lua_Near3 = { 
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
   { action_name = "Attack7_Nanmu", rate = 20, loop = 1  },
}
g_Lua_Near4 = { 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
   { action_name = "Attack8_ThrowWeapon", rate = 15, loop = 1, globalcooltime = 1  },
}
g_Lua_Near5 = { 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
   { action_name = "Attack8_ThrowWeapon", rate = 20, loop = 1, globalcooltime = 1 },
}
g_Lua_Near6 = { 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
}
g_Lua_MeleeDefense = { 
   { action_name = "Move_Back", rate = 12, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
}
g_Lua_RangeDefense = { 
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 2, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 2, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 20072,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 500, target = 2, randomtarget=1.1 },-- 버프
}