--GreenDragonNest_Gate1_Golem_Blue.lua


g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssaultTime = 5000

g_Lua_GlobalCoolTime1 = 20000
g_Lua_GlobalCoolTime2 = 25000
g_Lua_GlobalCoolTime3 = 15000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Attack01_Punch", rate = 20, loop = 1, cooltime = 5000 },
   { action_name = "Attack02_CannonPunch", rate = 20, loop = 1, cooltime = 5000 },
   { action_name = "Attack06_RightAttack", rate = 20, loop = 1, cooltime = 5000, td = "RF,RB,BR" },
   { action_name = "Attack07_LeftAttack", rate = 20, loop = 1, cooltime = 5000, td = "LF,BL,LB" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 20, loop = 1, cooltime = 22000, selfhppercent = 95 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack03_PunchHammer", rate = 30, loop = 1, approach = 250.0  },
}

g_Lua_Skill = {
   -- 돌진
   { skill_index = 31838,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, randomtarget=1.1 },
   -- 양발구르기 
   { skill_index = 31839,  cooltime = 27000, rate = 100, rangemin = 0, rangemax = 3500, target = 3, selfhppercentrange = "0,40", multipletarget = "1,2,0,1" },
   -- 내리치기
   { skill_index = 31834,  cooltime = 13000, rate = 100, rangemin = 0, rangemax = 800, target = 3 },
   -- 양손내리치기
   { skill_index = 31836,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 1200, target = 3 },
   -- 포탄 포격
   { skill_index = 31837,  cooltime = 15000, rate = 100, rangemin = 300, rangemax = 1500, target = 3, selfhppercentrange = "60,100" },
   -- 클러스터폭탄
   { skill_index = 31841,  cooltime = 24000, rate = 100, rangemin = 0, rangemax = 1700, target = 3, selfhppercentrange = "0,60" },
   -- 빔포
   { skill_index = 31842,  cooltime = 15000, rate = 100, rangemin = 300, rangemax = 600, target = 3, selfhppercentrange = "20,80" },
   -- 흡수
   { skill_index = 31843,  cooltime = 22000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "20,80", next_lua_skill_index = 0, randomtarget=1.1 },
   -- 미사일난사
   { skill_index = 31844,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 2500, target = 3, selfhppercentrange = "0,80" , globalcoomtime = 2, randomtarget=1.1 },
   -- 미사일난사(전방향)
   -- { skill_index = 31845,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 2500, target = 3, selfhppercentrange = "0,39" , globalcoomtime = 2, randomtarget=1.1 },
}