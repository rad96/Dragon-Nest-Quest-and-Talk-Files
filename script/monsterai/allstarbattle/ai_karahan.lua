
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 3,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0 쀅
	 {skill_index=96461,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1},
--1블랙홀
	 {skill_index=96462,  cooltime = 20000, rangemin = 0, rangemax = 700, target = 3, rate = 20, combo1 = " 14,60,0", combo2 = "15,70,0", combo3 = "11,100,0" },
--2 블리자드
	 {skill_index=96463, cooltime = 30000, rangemin = 0, rangemax = 1200, target = 3, rate = 50, combo1 = " 16,60,2", combo2 = "5,70,2", combo3 = "17,100,2", selfhppercent= 60  },
--3 캣치앤 쓰로우
	 {skill_index=96464, cooltime = 20000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, combo1 = "12,60,2", combo2 = " 6,70,2", combo3 = "11,100,2", combo4 = "14,100,0"},
--4 파이어볼
	 {skill_index=96465, cooltime = 27000, rangemin = 0, rangemax = 1200, target = 3, rate = 50, combo1 = "3,60,2", combo2 = "17,100,2", selfhppercent= 60 },	
--5 파이어 버드
	 {skill_index=96466, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 =" 3,60,0", combo2 = "8,70,0", combo3 = "2,100,0"},	
--6 플레임 로드
	 {skill_index=96467, cooltime = 30000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = "11,60,2" ,combo2 = "14,70,2", combo3 = "16,100,0" },
--7 플레임 웜
	 {skill_index=96468, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 20, combo1 = "13,60,2", combo2 = " 4,70,2", combo3 = "18,100,2", combo4 = " 14,100,0"},
--8 프리징 스파이크
	 {skill_index=96469, cooltime = 30000, rangemin = 0, rangemax = 900, target = 3, rate = 20, combo1 = "15,60,0", combo2 = " 3,100,2"},
--9 글레셜 웨이브
	 {skill_index=96470, cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = 40, combo1 = "4,60,2", combo2 = " 14,70,2", combo3 = "17,100,2", combo4 = "1,100,0"},
--10 아이스 베리어
	 {skill_index=96471, cooltime = 60000, rangemin = 0, rangemax = 1200, target = 3, rate = 60 },	
--11 아이스 니들
	 {skill_index=96472, cooltime = 15000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = "13,70,0", combo2 =" 15,100,0" },
--12 아이스 소드
	 {skill_index=96473, cooltime = 25000, rangemin = 0, rangemax = 900, target = 3, rate = 20, combo1 =" 17,70,0", combo2 = " 2,100,0"},		
--13 아이스 웨이브
	 {skill_index=96474, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = "3,70,2", combo2 = "17,100,2", combo3 = "5,100,0"},
--14 매직 미사일
	 {skill_index=96475, cooltime = 15000, rangemin = 0, rangemax = 1200, target = 3, rate = 20, combo1 =" 5,60,0", combo2 = " 16,100,0"},
--15 푸쉬
	 {skill_index=96476, cooltime = 15000, rangemin = 0, rangemax = 400, target = 3, rate = 40, combo1 = " 7,60,0", combo2 = "14,100,0"},
--16 레이지 그라비티
	 {skill_index=96477, cooltime = 15000, rangemin = 0, rangemax = 1200, target = 3, rate = 20 },
--17 롤링 라바
	 {skill_index=96478, cooltime = 25000, rangemin = 0, rangemax = 700, target = 3,rate = 20, combo1 = "13,60,0", combo2 =" 3,100,0"},	 
--18 플레임 볼텍스
	 {skill_index=96479, cooltime = 45000, rangemin = 0, rangemax = 1200, target = 3,rate = 60, combo1 = "1,60,0", combo2 = "11,100,0", selfhppercent= 20},

	 
	 } 
  