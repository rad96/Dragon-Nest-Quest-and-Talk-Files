
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 1,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
}


g_Lua_Skill =
{
--0광폭화
	 {skill_index=96371,  cooltime = 60000, rangemin = 0, rangemax = 900, target = 3, rate = 50, selfhppercent= 60},
--1대지진
	 {skill_index=96372,  cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = " 11,60,2", combo2 = "12,70,2",combo3 = "6,100,2", combo4 = "10,100,0"},
--2빅점프후 돌진
	 {skill_index=96373, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 20, combo1 = "11,60,2", combo2 = "8,100,2", combo3 = "15,100,0"},
--3빅점프
	 {skill_index=96374, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 30 , combo1 = "12,60,2",combo2 = "6,70,2", combo3 = " 7,100,2", combo4 = "10,100,0"},
--4파고들기
	 {skill_index=96375, cooltime = 45000, rangemin = 0, rangemax = 900, target = 3, rate = -1, combo1 = "14,60,2" },	
--5브뤠스
	 {skill_index=96376, cooltime = 20000, rangemin = 0, rangemax = 700, target = 3, rate = 20, combo1 = "7,60,2", combo2 = " 3,100,2"	 },	
--6콤보
	 {skill_index=96377, cooltime = 5000, rangemin = 0, rangemax = 800, target = 3, rate = 40, combo1 = " 15,60,2",combo2 = "10,100,2"  },
--7돌진
	 {skill_index=96378, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 20, combo1 = "11,60,2" , combo2 = "8,100,2" },
--8왼쪽손
	 {skill_index=96379, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = "9,100,2", combo2 = "15,100,0"	 },
--9오른쪽손
	 {skill_index=96380, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = "2,60,2", combo2 = " 5,70,2", combo3 = "10,100,2"},
--10라이트붐버
	 {skill_index=96381, cooltime = 5000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = "15,60,2", combo2 = " 6,100,2", },	
--11돌려차기
	 {skill_index=96382, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = " 6,60,2", combo2 = " 1,100,2"	 },
--12스포어
	 {skill_index=96383, cooltime = 40000, rangemin = 0, rangemax = 1200, target = 3, rate = 20},	
--13가시
	 {skill_index=96384, cooltime = 5000, rangemin = 0, rangemax = 900, target = 3, rate = -1},
--14 3단 지진
	 {skill_index=96385, cooltime = 25000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = " 5,60,2", combo2 = " 7,100,2"},
--15힙 어택
	 {skill_index=96386, cooltime = 8000, rangemin = 0, rangemax = 300, target = 3,rate = -1, combo1 = " 14,50,0", combo2 = "5,70,0", combo3 ="10,100,0"},
--16힙 어택P
	 {skill_index=96387, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3,rate = -1},
--16펀치
	 {skill_index=96388, cooltime = 45000, rangemin = 0, rangemax = 900, target = 3,rate = 50 , selfhppercent= 30},
--17돌려차기P
	 {skill_index=96389, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = -1 },

} 
  