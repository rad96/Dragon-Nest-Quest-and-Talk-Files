
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 1,		loop = 1 },
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0박수
	 {skill_index=97012,  cooltime = 8000, rangemin = 0, rangemax = 500, target = 3, rate = 30, resetcombo =1, combo1 = "3,70,2", combo2 = "7,100,2" },
--1가위바위보
	 {skill_index=97013,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 30, resetcombo =1, combo1 = " 3,60,2", combo2 = "5,70,2", combo3 = "0,100,2"},
--2눈의 축복	 {skill_index=97014,  cooltime = 30000, rangemin = 200, rangemax = 1200, target = 3, rate = 30 , resetcombo =1, selfhppercent= 80 },
--3블로킹
	 {skill_index=97015,  cooltime = 10000, rangemin = 100, rangemax = 500, target = 3, rate = 30 , combo1 = "1,70,2", combo2 = "5,100,2", resetcombo =1},
--4닷지
	 {skill_index=97016,  cooltime = 5000, rangemin = 0, rangemax = 1200, target = 3, rate = -1, combo1 = " 6,100,0", resetcombo =1},
--5더치 럽
	 {skill_index=97017,  cooltime = 10000, rangemin = 100, rangemax = 600, target = 3, rate = 30, combo1 ="3, 60,2", combo2 ="1,70,2" , combo3 = "0,80,2", combo4 = "7,100,2", resetcombo =1},
--6닷지 P
	 {skill_index=97018,  cooltime = 5000, rangemin = 0, rangemax = 1200, target = 3, rate = -1, combo1 = "9,60,2", combo2 =" 5,100,0" , resetcombo =1},
--7롤링 스노우 볼
	 {skill_index=97019,  cooltime = 15000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1 =" 3,60,2", combo2 ="1,70,2" , combo3 = "5,100,2", resetcombo =1},
--8프로즌 프레이Y	 {skill_index=97010,  cooltime = 40000, rangemin = 0, rangemax = 1500, target = 3, rate = 30, combo1 =" 1,80,0", resetcombo =1, selfhppercent= 50, limitcount = 3},
--9아이싱 브레스Y
	 {skill_index=97011,  cooltime = 30000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1 =" 3,60,2", combo2 ="1,70,2" , combo3 = "5,100,2", resetcombo =1, priority=3, selfhppercent= 60},
--10스노우볼
	 {skill_index=97020,  cooltime = 15000, rangemin = 0, rangemax = 700, target = 3, rate = 30, resetcombo =1, priority=3},
--11프로즌크리스탈
	 {skill_index=97021,  cooltime = 30000, rangemin = 0, rangemax = 700, target = 3, rate = 30, resetcombo =1, priority=3},
--12랜덤공용스킬
     {skill_index=97050, cooltime = 10000, rangemin = 0, rangemax = 1200, target = 3,rate = -1, priority=3},	 

} 
  