
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air|Stiff","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 1,		loop = 1 },
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0브레이크
	 {skill_index=96181,  cooltime = 5000, rangemin = 0, rangemax = 500, target = 3, rate = 30, resetcombo =1, combo1 = "9,100,2", combo2 =" 4,100,4", resetcombo =1},
--1다크터치
	 {skill_index=96182,  cooltime = 10000, rangemin = 0, rangemax = 300, target = 3, rate = 20, resetcombo =1, combo1 = "11,60,2", combo2 = " 10, 100,2", resetcombo =1},
--2디펜스
	 {skill_index=96183,  cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = -1 , resetcombo =1},
--3이스케이프 스워드
	 {skill_index=96184,  cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = 20, combo1 =" 12,60,2", combo2 = "5,100,0", resetcombo =1 },
--4난무
	 {skill_index=96185,  cooltime = 3000, rangemin = 0, rangemax = 300, target = 3, rate = 50, combo1 = " 11,60,2", combo2 = " 1, 70,2", combo3 = "9, 100,0", target_condition = "State3" , resetcombo =1},
--5일섬
	 {skill_index=96186,  cooltime = 10000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = " 6,100,0", resetcombo =1},
--6일섬 샷(롱)
	 {skill_index=96187,  cooltime = 100, rangemin = 0, rangemax = 1500, target = 3, rate = -1, combo1 = " 7,100,0" , usedskill = "96186", resetcombo =1},
--7일섬 샷(하프문)
	 {skill_index=96188,  cooltime = 100, rangemin = 0, rangemax = 600, target = 3, rate = -1, combo1 = " 8, 100,1", combo2 =" 6,100,0", usedskill = "96186", resetcombo =1},
--8일섬 샷(평타)
	 {skill_index=96189,  cooltime = 100, rangemin = 0, rangemax = 600, target = 3, rate = -1,  combo1 = " 7,100,1", combo2 = "6, 100,0",usedskill = "96186", resetcombo =1},
--9슬래쉬
	 {skill_index=96190,  cooltime = 5000, rangemin = 0, rangemax = 250, target = 3, rate = 50, combo1 = " 4, 70 ,2", combo2 = "3, 70,0", combo3 = "0,100,0", resetcombo =1 },
--10스톰프앤슬래쉬
	 {skill_index=96191,  cooltime = 15000, rangemin = 0, rangemax = 400, target = 3, rate = 20, combo1 =" 11,60,2", combo2 = " 12,70,2", combo3 = " 0,100,0"	, resetcombo =1 },
--11어퍼
	 {skill_index=96192,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 20, combo1 =" 4, 70,2", combo2 ="5, 100,0" , resetcombo =1},
--12월
	 {skill_index=96193,  cooltime = 15000, rangemin = 0, rangemax = 700, target = 3, rate = 30 , resetcombo =1, combo1 =" 4,70,2", combo2 =" 10,100,1" , resetcombo =1},
--13이스케이프 스워드_P
	 {skill_index=96174,  cooltime = 5000, rangemin = 0, rangemax = 100, target = 3, rate = -1 , resetcombo =1},
--14일섬 샷(평타A)
	 {skill_index=96175,  cooltime = 100, rangemin = 0, rangemax = 1200, target = 3, rate = -1,  combo1 = "8,100,2", combo2 = " 7,70,0", combo3 = "6,100,0", usedskill = "96186" , resetcombo =1},
--15숏 슬래쉬
	 {skill_index=96176,  cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = -1 },
--16랜덤공용스킬
     {skill_index=97050, cooltime = 10000, rangemin = 0, rangemax = 1200, target = 3,rate = -1, priority=3},
--17아카데믹대응스킬
     {skill_index=97045, cooltime = 10000, rangemin = 0, rangemax = 1200, target = 3,rate = -1, priority=3},	 
} 
  