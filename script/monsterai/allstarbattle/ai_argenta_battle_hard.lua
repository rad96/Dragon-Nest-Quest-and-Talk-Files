
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air|Stiff","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 1,		loop = 1 },
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 2,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate =3,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0난무
	 {skill_index=96201,  cooltime = 3000, rangemin = 0, rangemax = 200, target = 3, rate = 30, combo1 = " 12,60,2", combo2 = " 8,70,2", combo3 = " 17,100,2"},
--1푸쉬윈드 차징
	 {skill_index=96202,  cooltime = 100, rangemin = 0, rangemax = 1500, target = 3, rate = -1, resetcombo =1, combo1 =" 15,100,2", combo2 =" 14,100,0" , usedskill = "96206" },
--2푸쉬 윈드 슛
	 {skill_index=96203,  cooltime = 100, rangemin = 0, rangemax = 1200, target = 3, rate = -1 , resetcombo =1, combo1 ="3,100,2", combo2 ="7,100,0" , usedskill = "96206" },
--3푸쉬 윈드 슛 힛
	 {skill_index=96204,  cooltime = 100, rangemin = 0, rangemax = 1200, target = 3, rate = -1 , resetcombo =1, combo1 ="2,100,2", combo2 ="7,100,0", usedskill = "96206"   },
--4푸쉬 윈드 슛 노힛
	 {skill_index=96205,  cooltime = 7000, rangemin = 0, rangemax = 200, target = 3, rate = -1, combo1 =" 4,100,2" , combo2 = "7,100,0", resetcombo =1, usedskill = "96206" },
--5푸쉬 윈드
	 {skill_index=96206,  cooltime = 30000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = "1,100,2", combo2= "7,100,0" , resetcombo =1},
--6슬래쉬
	 {skill_index=96207,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 =" 10,60,2", combo2 = " 0,100,2"},
--7덤블링
	 {skill_index=96208,  cooltime = 3000, rangemin = 0, rangemax = 1200, target = 3, rate = -1},
--8어퍼
	 {skill_index=96209,  cooltime = 8000, rangemin = 0, rangemax = 600, target = 3, rate = 10, combo1 =" 0,100,2"},
--9윈드
	 {skill_index=96210,  cooltime = 15000, rangemin = 0, rangemax = 250, target = 3, rate = 30, combo1 = "4,50,2", combo2 = " 8,70,2", combo3 = " 0,100,2", combo4 = "7,100,0" },
--10윈드 슬래쉬
	 {skill_index=96211,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 =" 17,60,2", combo2 ="11,100,2", combo3 = "7,100,0" },
--11윈드 슬래쉬 A
	 {skill_index=96212,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 =" 17,60,2", combo2 =" 8,100,2", combo3 ="9,100,0"},
--12엑스 슬래쉬
	 {skill_index=96213,  cooltime = 15000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = " 0,100,2"},
--13엑스 슬래쉬 P
	 {skill_index=96214,  cooltime = 5000, rangemin = 0, rangemax = 100, target = 3, rate = -1 , resetcombo =1},
--14푸쉬 윈드 차징A
	 {skill_index=96215,  cooltime = 100, rangemin = 0, rangemax = 1500, target = 3, rate = -1, resetcombo =1, combo1 =" 15,100,2", combo2 =" 1,100,0" , usedskill = "96206"  },
--15푸쉬 윈드 차징S
	 {skill_index=96216,  cooltime = 10000, rangemin = 0, rangemax = 1200, target = 3, rate = -1, resetcombo =1, combo1 ="2,100,2", combo2 ="7,100,0"},-- , combo1 ="1,100,0" },	 
--16부스트
	 {skill_index=96217,  cooltime = 45000, rangemin = 0, rangemax = 1200, target = 2, rate = 30, resetcombo =1},
--17시저스
	 {skill_index=97040,  cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = 30, resetcombo =1, combo1 =" 8,60,2", combo2 = " 0,100,2"},
--18윈드블로우
	 {skill_index=97041,  cooltime = 30000, rangemin = 0, rangemax = 600, target = 3, rate = 30, resetcombo =1, combo1 =" 10,60,2", combo2 = " 0,100,2", priority=3, selfhppercent= 80},
--19퀘이크
	 {skill_index=97042,  cooltime = 10000, rangemin = 0, rangemax = 800, target = 3, rate = 30, resetcombo =1},	
--20랜덤공용스킬
     {skill_index=97050, cooltime = 10000, rangemin = 0, rangemax = 1200, target = 3,rate = -1, priority=3},	 
} 
  