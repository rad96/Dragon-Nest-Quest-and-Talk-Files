
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 300;
g_Lua_NearValue4 = 600;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }


g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left", rate = 2, loop = 1 },
	 { action_name = "Move_Right", rate = 2, loop = 1 },
	 { action_name = "Move_Front", rate = 2, loop = 1 },
         { action_name = "Skill_Tumble_Front", rate = 10, loop = 1},
	 { action_name = "Skill_Tumble_Back", rate = 10, loop = 1},
	 { action_name = "Skill_Tumble_Left", rate = 10, loop = 1 },
	 { action_name = "Skill_Tumble_Right", rate = 10, loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front", rate = 5, loop = 1 },
	{ action_name = "Move_Left", rate = 5, loop = 1 },
	{ action_name = "Move_Right", rate = 5, loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front", rate = 15, loop = 3 },
	{ action_name = "Move_Left", rate = 3, loop = 2 },
	{ action_name = "Move_Right", rate = 3, loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front", rate = 15, loop = 3 },
	{ action_name = "Move_Left", rate = 5, loop = 2 },
	{ action_name = "Move_Right", rate = 5, loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front", rate = 15, loop = 3 },
	{ action_name = "Move_Left", rate = 3, loop = 2 },
	{ action_name = "Move_Right", rate = 3, loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front", rate = 15, loop = 5 },
}


-- 0 : 히트되지 않아도 사용, 1 : 유저에게 히트된 경우에만, 2: 유저 또는 몬스터에게 히트되면
g_Lua_Skill =
{
--0 KEY 에어리얼 콤보 Skill_AerialCombo(90f)
	 { skill_index = 96141, cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = 10, combo1 = "9,50,2", combo2 = "1,100,1"},
--1에어리얼 콤보1 Skill_AerialCombo1(150f)
	 { skill_index = 96142, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = -1, combo1 = "6,60,2", combo2 = "8,100,0" },

--2 KEY 체인 라이트닝 Skill_Chain(110f)  
	 { skill_index = 96143, cooltime = 12000, rangemin = 0, rangemax = 600, target = 3, rate = 20, combo1 = "9,100,0" },
--3클로 Skill_Claw(55f)
	 { skill_index = 96144, cooltime = 6000, rangemin = 0, rangemax = 250, target = 3, rate = -1, combo1 = "2,60,2", combo2 = "8,100,0" },

--4 blowcheck 데토 Skill_Deto(100f) 
	 { skill_index = 96145, cooltime = 15000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, blowcheck = "43", combo1 = "14,100,2" }, 
--5 KEY 라이트닝 볼 Skill_LightningBall_Lv3(100f) 
	 { skill_index = 96146, cooltime = 24000, rangemin = 100, rangemax = 600, target = 3, rate = 10, combo1 = "11,100,0" }, 
--6 KEY 라이트닝 볼트 Skill_LightningVolt_Lv2(110f)
	 { skill_index = 96147, cooltime = 8000, rangemin = 0, rangemax = 600, target = 3, rate = 20, combo1 = "9,100,0" },

--7마인드 Skill_Mind(74f) 
	 { skill_index = 96148, cooltime = 11000, rangemin = 0, rangemax = 1200, target = 3, rate = -1, combo1 = "6,100,0"  },
--8시클킥 Skill_SickleKick(48f)
	 { skill_index = 96149, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = -1 , combo1 = "9,50,2", combo2 = "11,70,2", combo3 = "2,100,0"},
--9섬머솔트 Skill_Somersault(60f)  , cancellook = 0
	 { skill_index=96150, cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = -1, combo1 = "0,100,2", combo2 = "11,100,0" },
--10서몬쉐도우 Skill_Summonshadow(240f) , cancellook = 0
	 { skill_index=96151, cooltime = 36000, rangemin = 0, rangemax = 900, target = 3, rate = -1,  priority= 3, combo1 = "8,30,2", combo2 = "3,100,0" , selfhppercent = 80 }, 

--11 KEY 쓰로잉 나이프 Skill_ThrowingKnife(170f), cancellook = 0
	 {skill_index=96152, cooltime = 15000, rangemin = 0, rangemax = 900, target = 3, rate = 10, combo1 = "7,50,2", combo2 = "2,100,0" },
--12 KEY 트랜스 Skilll_Trans_Start, cancellook = 0
	 {skill_index=96153, cooltime = 24000, rangemin = 0, rangemax = 900, target = 3, rate = 100, combo1 = " 1,100,0", selfhppercent = 40 },

--13웨이브 Skilll_wave, cancellook = 0
	 {skill_index=96154, cooltime = 6000, rangemin = 0, rangemax = 200, target = 3, rate = -1 },
--14시클킥 Sub Skill_SickleKick_Sub, cancellook = 0
	 {skill_index=96155, cooltime = 3600, rangemin = 0, rangemax = 400, target = 3, rate = -1, combo1 = "3,50,2", combo2 = "9,60,2", combo3 = "8,100,0" },
--15덤블링B Skill_Somersault_Sub, cancellook = 0
	 {skill_index=96156, cooltime = 18000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 = "0,100,0" },
--16덤블링L Skill_AerialCombo_Sub, cancellook = 0
	 {skill_index=96157, cooltime = 18000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 = "0,100,0" },
--17덤블링R Skill_Tumble_Right, cancellook = 0
	 {skill_index=96158, cooltime = 18000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 = "0,100,0" },  
} 
  