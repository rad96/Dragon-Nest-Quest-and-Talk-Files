--AiGreatBeatle_Green_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 5000

g_Lua_GlobalCoolTime1 = 30000
g_Lua_GlobalCoolTime2 = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Attack01_RightHook", rate = 22, loop = 1, globalcooltime = 2},
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
   { action_name = "Attack01_RightHook", rate = 12, loop = 1, globalcooltime = 2},
   { action_name = "Attack02_LeftUpper", rate = 19, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Assault", rate = 19, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Assault", rate = 19, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack01_RightHook", rate = 12, loop = 1, approach = 300 },
}
g_Lua_Skill = {
   { skill_index = 33179,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3 , randomtarget=1.1}, -- ������-�ٴ�ģ �� ���
   { skill_index = 33173,  cooltime = 28000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, randomtarget=1.1 }, -- ���̴�ű
   { skill_index = 33176,  cooltime = 36000, rate = 100,rangemin = 0, rangemax = 4000, globalcooltime = 1, randomtarget=1.1, target = 3, selfhppercent = 40, next_lua_skill_index = 0 }, -- �Ÿ������� > ������
   { skill_index = 33177,  cooltime = 32000, rate = 100, rangemin = 0, rangemax = 4000, globalcooltime = 1, multipletarget = "1,8", target = 3 }, -- ����
   { skill_index = 33171,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, randomtarget=1.1 }, -- �ջ�ġ��
   { skill_index = 33172,  cooltime = 20000, rate = 60, rangemin = 0, rangemax = 1000, target = 3, randomtarget=1.1 }, -- �߱����� 
   { skill_index = 33174,  cooltime = 34000, rate = 70,rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 40 , randomtarget=1.1}, -- �����÷�
   { skill_index = 33178,  cooltime = 20000, rate = 80, rangemin = 900, rangemax = 2000, target = 3, randomtarget=1.1 }, -- ���׹��
   
}
