
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0에어리얼 콤보
	 {skill_index=96141,  cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = 10, combo1 = " 9,50, 2", combo2 = " 1,100,1"},
--1에어리얼 콤보1
	 {skill_index=96142,  cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = -1,combo1 = " 6,60,2" , combo2 = " 8,100,0" },
--2체인 라이트닝
	 {skill_index=96143, cooltime = 12000, rangemin = 0, rangemax = 600, target = 3, rate = 20, combo1 = "9,100,0" },
--3클로
	 {skill_index=96144, cooltime = 5000, rangemin = 0, rangemax = 250, target = 3, rate = -1, combo1 = "2,60,2", combo2 = "8,100,0" },
--4데토
	 {skill_index=96145, cooltime = 15000, rangemin = 0, rangemax = 1200, target = 3, rate = 60, blowcheck = "43 ", combo1 = " 14,100,2"},	
--5라이트닝 볼
	 {skill_index=96146, cooltime = 20000, rangemin = 100, rangemax = 600, target = 3, rate = 20,combo1 = " 11,100,0" },	
--6라이트닝 볼트
	 {skill_index=96147, cooltime = 8000, rangemin = 0, rangemax = 600, target = 3, rate = 20,combo1 = " 9,100,0" },
--7마인드
	 {skill_index=96148, cooltime = 9000, rangemin = 0, rangemax = 1200, target = 3, rate = -1,  combo1 = " 6,100,0"  },
--8시클킥
	 {skill_index=96149, cooltime = 3000, rangemin = 0, rangemax = 400, target = 3, rate = -1 , combo1 = " 9,50,2", combo2 = " 11,70,2", combo3 = "2, 100,0"},
--9섬머솔트
	 {skill_index=96150, cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = -1, cancellook = 0, combo1 = "0,100,2", combo2 = " 11,100,0" },
--10서몬쉐도우
	 {skill_index=96151, cooltime = 30000, rangemin = 0, rangemax = 900, target = 3, rate = -1, cancellook = 0,  priority= 3, combo1 = " 8,60,2", combo2 = " 3,100,0" , selfhppercent=80 },	
--11쓰로잉 나이프
	 {skill_index=96152, cooltime = 15000, rangemin = 0, rangemax = 900, target = 3, rate = 10, cancellook = 0, combo1 = " 7, 50,2", combo2 = " 2, 100,0" },
--12트랜스
	 {skill_index=96153, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 100, cancellook = 0, combo1 = " 1,100,0"  , selfhppercent=40},	
--13웨이브
	 {skill_index=96154, cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = -1, cancellook = 0 },
--14시클킥 Sub
	 {skill_index=96155, cooltime = 3000, rangemin = 0, rangemax = 400, target = 3, rate = -1, cancellook = 0, combo1 = "3, 50,2", combo2 = " 9, 60,2", combo3 = " 8,100,0" },
--15덤블링B
	 {skill_index=96156, cooltime = 15000, rangemin = 0, rangemax = 300, target = 3, rate = -1, cancellook = 0, combo1 = " 0,60,0" },
--16덤블링L
	 {skill_index=96157, cooltime = 15000, rangemin = 0, rangemax = 300, target = 3, rate = -1, cancellook = 0, combo1 = " 0,60,0" },
--17덤블링R
	 {skill_index=96158, cooltime = 15000, rangemin = 0, rangemax = 300, target = 3, rate = -1, cancellook = 0, combo1 = " 0,60,0" },	 
} 
  