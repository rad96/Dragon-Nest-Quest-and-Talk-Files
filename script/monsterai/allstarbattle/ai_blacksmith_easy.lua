
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0�Ѹ�
	 {skill_index=96241,  cooltime = 12000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1 = "8,70,2" , combo2 = "15,100,2"},
--1�转
	 {skill_index=96242,  cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = 20, combo1 = " 4, 100,2"},
--2��
	 {skill_index=96243, cooltime = 15000, rangemin = 0, rangemax = 1200, target = 3, rate = 20, combo1 = " 6,80,2"},
--3�� ����
	 {skill_index=96244, cooltime = 10000, rangemin = 0, rangemax = 900, target = 3, rate = -1,},
--4���
	 {skill_index=96245, cooltime = 30000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, resetcombo =1},	
--5��� ����
	 {skill_index=96246, cooltime = 16000, rangemin = 0, rangemax = 1200, target = 3, rate = -1, resetcombo =1},	
--6����
	 {skill_index=96247, cooltime = 15000, rangemin = 0, rangemax = 700, target = 3, rate = 20, combo1 = " 8,100,2"},
--7��
	 {skill_index=96248, cooltime = 10000, rangemin = 0, rangemax = 900, target = 3, rate = 20, },
--8��������
	 {skill_index=96249, cooltime = 10000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = "10,100,2", combo2 = "6,100,0" },
--9��������P
	 {skill_index=96250, cooltime = 5000, rangemin = 0, rangemax = 600, target = 3, rate = -1 },
--10����1
	 {skill_index=96251, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1,combo1 = "11,100,2" },	
--11����2
	 {skill_index=96252, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1,combo1 = "12,100,2" },
--12����3
	 {skill_index=96253, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 = "13,60,2", combo2 = "1,100,2", combo3 = " 15,100,0" },	
--13�� ����, cancellook = 0
	 {skill_index=96254, cooltime = 20000, rangemin = 0, rangemax = 500, target = 3, rate = 30, combo1 = " 11,60,2", combo2 = "12,100,2", resetcombo =1},
--14�� Ÿ��Ǭ, cancellook = 0
	 {skill_index=96255, cooltime = 45000, rangemin = 0, rangemax = 500, target = 3, rate = 50, selfhppercent=60 , combo1 = " 15,100,2" , resetcombo =1},
--15��Ŭ ����, cancellook = 0
	 {skill_index=96256, cooltime = 15000, rangemin = 0, rangemax = 500, target = 3, rate = -1, combo1 = " 10,60,2", combo2 = "11,70,2", combo3 = "12,100,2", comb4 = " 0,100,0"},
--16������ A, cancellook = 0
	 {skill_index=96257, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 = "17,100,0", resetcombo =1 },
--17������ B, cancellook = 0
	 {skill_index=96258, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, resetcombo =1},
--18Ÿ��ǬA, cancellook = 0
	 {skill_index=96259, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, usedskill = "96255" , combo1 = "19,100,0", resetcombo =1},
--19Ÿ��ǬB, cancellook = 0
	 {skill_index=96260, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, usedskill = "96255"  , combo1 = "18,100,0", resetcombo =1 },	 	 
} 
  