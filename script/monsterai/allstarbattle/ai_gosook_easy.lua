
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0브랜디쉬 나이프
	 {skill_index=96005,  cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = " 6,100, 1"},
--1파이어 대쉬
	 {skill_index=96007,  cooltime = 8000, rangemin = 0, rangemax = 800, target = 3, rate = 20,combo1 = " 9,100,0"  },
--2마엘스톰
	 {skill_index=96009, cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = "9,100,0" },
--3난무
	 {skill_index=96010, cooltime = 3000, rangemin = 0, rangemax = 250, target = 3, rate = 30, combo1 = "2,60,2", combo2 = "8,100,0" },
--4파워
	 {skill_index=96011, cooltime = 20000, rangemin = 200, rangemax = 1200, target = 3, rate = 50, selfhppercent= 60},	
--5사이렌
	 {skill_index=96012, cooltime = 15000, rangemin = 0, rangemax = 200, target = 3, rate = 30,combo1 = " 13,100,0" },	
--6점프어택 숏
	 {skill_index=96013, cooltime = 10000, rangemin = 200, rangemax = 600, target = 3, rate = 50,combo1 = " 9,100,0" },
--7점프어택 롱
	 {skill_index=96014, cooltime = 15000, rangemin = 400, rangemax = 1200, target = 3, rate =50, combo1 = " 9,100,0" },
--8푸쉬업
	 {skill_index=96015, cooltime = 20000, rangemin = 0, rangemax = 400, target = 3, rate = 20, combo1 = " 3,100,0" },
--9슬래쉬 , cancellook = 0
	 {skill_index=96016, cooltime = -1, rangemin = 0, rangemax = 200, target = 3, rate = 50, combo1 = "13,60,2", combo2 = " 3,100,0" },
--10스톰프 , cancellook = 0
	 {skill_index=96017, cooltime = 12000, rangemin = 0, rangemax = 500, target = 3, rate = 30, combo1 = " 8,60,2", combo2 = " 3,100,0" },	
--11큰돌던지기 , cancellook = 0
	 {skill_index=96018, cooltime = 15000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = " 1,100,0" },
--12작은돌던지기 , cancellook = 0
	 {skill_index=96019, cooltime = 9000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = " 1,100,0" },	
--13어퍼, cancellook = 0
	 {skill_index=96020, cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = 30, target_condition = "State3", combo1 = " 9,100,0" },
--5서먼코멧, cancellook = 0
	 {skill_index=96021, cooltime = 15000, rangemin = 0, rangemax = 300, target = 3, rate = 20, combo1 = " 0,60,0" },	 
} 
  