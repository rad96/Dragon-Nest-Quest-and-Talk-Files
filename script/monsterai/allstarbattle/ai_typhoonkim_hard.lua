
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0 에임 숏
	 {skill_index=96431,  cooltime = 15000, rangemin = 100, rangemax = 900, target = 3, rate = 30, combo1 = "9,60,0" , combo2 = "10,70,0", combo3  = "11,100,0"},
--1암3단 콤보
	 {skill_index=96432,  cooltime = 8000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = "15,60,2", combo2 = " 4,70,2", combo3 = "9,100,2", combo4 = "7,100,0"},
--2 암 푸쉬
	 {skill_index=96433, cooltime = 5000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 =" 1,60,2", combo2 = "6,100,2", combo3 =" 18,100,0" },
--3 암 푸쉬P
	 {skill_index=96434, cooltime = 5000, rangemin = 0, rangemax = 500, target = 3, rate = -1},
--4 춉
	 {skill_index=96435, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = "2,50,2", combo2 = " 17,60,2", combo3 =" 14,70,2", combo4 = "20,100,2"},	
--5 춉 라이트닝
	 {skill_index=96436, cooltime = 10000, rangemin = 0, rangemax = 500, target = 3, rate = 20, combo1 = "2,50,2", combo2 = " 17,60,2", combo3 =" 14,70,2", combo4 = "20,100,2"},	
--6 콤보
	 {skill_index=96437, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = "1,50,2", combo2 = "15,60,2", combo3 = " 4,70,2", combo4 = "2,100,2", combo5 = "7,100,0" },
--7 피어
	 {skill_index=96438, cooltime = 20000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1 =" 0,60,2"},
--8 풀 스윙
	 {skill_index=96439, cooltime = 8000, rangemin = 0, rangemax = 350, target = 3, rate = 30, combo1 = "1,50,2", combo2 = "6,60,2", combo3 = "4,70,2", combo4 = "17,100,2"},
--9 그레이트 밤
	 {skill_index=96440, cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = "10,50,2", combo2 = "11,60,2", combo3 = "16,100,2", combo4 = "0,100,0"},
--10 점프 춉
	 {skill_index=96441, cooltime = 20000, rangemin = 0, rangemax = 800, target = 3, rate = 30, combo1 = "20,60,2", combo2 = " 2,70,2", combo3 = "18,100,2", combo4 = "0,100,0"},	
--11 점프 춉 라이트닝
	 {skill_index=96442, cooltime = 20000, rangemin = 0, rangemax = 800, target = 3, rate = 30 , combo1 = "20,60,2", combo2 = " 2,70,2", combo3 = "15,100,2", combo4 = "0,100,0"},
--12 점프 춉 P
	 {skill_index=96443, cooltime = 5000, rangemin = 0, rangemax = 700, target = 3, rate = -1},		
--13 쇼크 브레이크
	 {skill_index=96444, cooltime = 30000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = "0,100,0"},
--14 샤우트
	 {skill_index=96445, cooltime = 20000, rangemin = 0, rangemax = 700, target = 3, rate = 20},
--15 슬라이드 차지
	 {skill_index=96446, cooltime = 20000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, combo1 = "6,60,2", combo2 = "9,100,2"},
--16 스테어
	 {skill_index=96447, cooltime = 45000, rangemin = 0, rangemax = 900, target = 3, rate = 50, selfhppercent= 90, priority=3	 },
--17 스톰프
	 {skill_index=96448, cooltime = 20000, rangemin = 0, rangemax = 400, target = 3,rate =30, combo1 = "16,60,2", combo2 = "1,70,2", combo3 = "8,100,2" },	 
--18 스트레이트 암
	 {skill_index=96449, cooltime = 20000, rangemin = 0, rangemax = 600, target = 3,rate = 30, combo1 = "6,60,2", combo2 = " 14,100,2"},
--19 퓨리
	 {skill_index=96450, cooltime = 60000, rangemin = 0, rangemax = 900, target = 3,rate = 60, selfhppercent= 50, priority=3},
--20 휠 윈드
	 {skill_index=96451, cooltime = 30000, rangemin = 0, rangemax = 400, target = 3,rate = 30, combo1 = "9,60,2", combo2 = "8,70,2", combo3 = "6,100,2"},
--21 휠 타이푼
	 {skill_index=96452, cooltime = 60000, rangemin = 0, rangemax = 500, target = 3,rate = 60, selfhppercent= 50, priority=3 },		 
--22랜덤공용스킬
     {skill_index=97050, cooltime = 10000, rangemin = 0, rangemax = 1200, target = 3,rate = -1, priority=3},	 
	 } 
  