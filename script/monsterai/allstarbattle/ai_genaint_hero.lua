
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 1000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_GlobalCoolTime1 = 0
g_Lua_GlobalCoolTime2 = 0

g_Lua_Near1 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 15,		loop = 2 },
	{ action_name = "Move_Right",	rate = 15,		loop = 2 },
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 15,		loop = 2 },
	{ action_name = "Move_Right",	rate = 15,		loop = 2 },
}
g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 15,		loop = 2 },
	{ action_name = "Move_Right",	rate = 15,		loop = 2 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 15,		loop = 2 },
	{ action_name = "Move_Right",	rate = 15,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 15,		loop = 2 },
	{ action_name = "Move_Right",	rate = 15,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 15,		loop = 2 },
	{ action_name = "Move_Right",	rate = 15,		loop = 2 },
}


g_Lua_Skill =
{
--0스킬 Skill_01
	 {skill_index=98140,  cooltime = 0, rangemin = 0, rangemax = 5000, target = 3, rate = 100, resetcombo =1,ignoreaggro = 1, combo1 = " 1,100,2"},
--0스킬 Skill_02
	 {skill_index=98141,  cooltime = 0, rangemin = 0, rangemax = 5000, target = 3, rate = 0, resetcombo =1,ignoreaggro = 1, combo1 = " 2,100,2"},
--0스킬 Skill_03
	 {skill_index=98142,  cooltime = 0, rangemin = 0, rangemax = 5000, target = 3, rate = 0,ignoreaggro = 1},	 
} 
  