
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0�转
	 {skill_index=96321,  cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 ="9,50,2", combo2 ="15,60,2", combo3 = "3,100,2", combo4 = " 12,100,0" },
--1����ũ
	 {skill_index=96322,  cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = 20, combo1 =" 11,100,0"},
--2í����Ʈ
	 {skill_index=96323, cooltime = 20000, rangemin = 0, rangemax = 400, target = 3, rate = 20, combo1 = "12,100,0" },
--3�޺�
	 {skill_index=96324, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 40, combo1 = "15,60,2", combo2 = "9,70,2", combo3 =" 4,100,2", },
--4�뽬
	 {skill_index=96325, cooltime = 20000, rangemin = 0, rangemax = 1200, target = 3, rate = 40, combo1 = " 17,60,2", combo2 = " 6,70,2", combo3 = " 3, 100,2"},	
--5���ڽ�ŸƮ ��Ÿ��
	 {skill_index=96326, cooltime = 30000, rangemin = 0, rangemax = 400, target = 3, rate = -1, combo1 ="19,100,2"},	
--6����� ������Ʈ
	 {skill_index=96327, cooltime = 45000, rangemin = 0, rangemax = 600, target = 3, rate = 45, selfhppercent= 60  },
--7Ȧ�� ����Ʈ
	 {skill_index=96328, cooltime = 20000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = " 9,100,0" },
--8Ȧ�� ũ�ν�
	 {skill_index=96329, cooltime = 15000, rangemin = 0, rangemax = 900, target = 3, rate = 30 },
--9���� ����
	 {skill_index=96330, cooltime = 10000, rangemin = 100, rangemax = 600, target = 3, rate = 20, combo1 =" 15,60,2", combo2 = "4,70,2", combo3 = "11,100,2" , combo4 = "15,100,0"},
--10���� �нú�
	 {skill_index=96331, cooltime = 5000, rangemin = 0, rangemax = 900, target = 3, rate = -1  },	
--11����Ʈ�� ��Ʈ
	 {skill_index=96332, cooltime = 20000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 ="4,60,2", combo2 ="3,100,2"	 },
--12��ġ
	 {skill_index=96333, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = " 18,60,2", combo2 = " 9,70,2", combo3 = "7,100,2", combo4 = " 8,100,0" },	
--13��ġ �нú�, cancellook = 0
	 {skill_index=96334, cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = -1 },
--14Ȧ�巼��
	 {skill_index=96335, cooltime = 60000, rangemin = 0, rangemax = 1200, target = 3, rate = 10 },
--15����Ʈ�� ����
	 {skill_index=96336, cooltime = 20000, rangemin = 0, rangemax = 1200, target = 3, rate = 10},
--16���� ����, cancellook = 0
	 {skill_index=96337, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1 },
--17ȥ ����, cancellook = 0
	 {skill_index=96338, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = " 5,60,1", combo2 = "3,70,2", combo3 = "12,100,2" , combo4 = "2,100,0"},
--18�� ũ�ν�, cancellook = 0
	 {skill_index=96339, cooltime = 20000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, combo2 = " 4,100,0" },
--19���ڷ���, cancellook = 0
	 {skill_index=96340, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 = "12,70,2", combo2 = " 3,100,2"},	 
} 
  