
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0 ���� ��
	 {skill_index=96431,  cooltime = 15000, rangemin = 100, rangemax = 900, target = 3, rate = 30, combo1 = "9,60,0" , combo2 = "10,70,0", combo3  = "11,100,0"},
--1��3�� �޺�
	 {skill_index=96432,  cooltime = 8000, rangemin = 0, rangemax = 300, target = 3, rate = 40, combo1 = "15,60,2", combo2 = " 4,70,2", combo3 = "9,100,2", combo4 = "7,100,0"},
--2 �� Ǫ��
	 {skill_index=96433, cooltime = 5000, rangemin = 0, rangemax = 900, target = 3, rate = 20, combo1 =" 1,60,2", combo2 = "6,100,2", combo3 =" 18,100,0" },
--3 �� Ǫ��P
	 {skill_index=96434, cooltime = 5000, rangemin = 0, rangemax = 500, target = 3, rate = -1},
--4 �h
	 {skill_index=96435, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 10, combo1 = "2,50,2", combo2 = " 17,60,2", combo3 =" 14,70,2", combo4 = "20,100,2"},	
--5 �h ����Ʈ��
	 {skill_index=96436, cooltime = 10000, rangemin = 0, rangemax = 500, target = 3, rate = 20, combo1 = "2,50,2", combo2 = " 17,60,2", combo3 =" 14,70,2", combo4 = "20,100,2"},	
--6 �޺�
	 {skill_index=96437, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = 40, combo1 = "1,50,2", combo2 = "15,60,2", combo3 = " 4,70,2", combo4 = "2,100,2", combo5 = "7,100,0" },
--7 �Ǿ�
	 {skill_index=96438, cooltime = 20000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1 =" 0,60,2"},
--8 Ǯ ����
	 {skill_index=96439, cooltime = 8000, rangemin = 0, rangemax = 350, target = 3, rate = 30, combo1 = "1,50,2", combo2 = "6,60,2", combo3 = "4,70,2", combo4 = "17,100,2", combo5 = " 14,100,0"},
--9 �׷���Ʈ ��
	 {skill_index=96440, cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = 20, combo1 = "10,50,2", combo2 = "11,60,2", combo3 = "16,100,2", combo4 = "0,100,0"},
--10 ���� �h
	 {skill_index=96441, cooltime = 20000, rangemin = 0, rangemax = 800, target = 3, rate = 30, combo1 = "20,60,2", combo2 = " 2,70,2", combo3 = "18,100,2", combo4 = "0,100,0"},	
--11 ���� �h ����Ʈ��
	 {skill_index=96442, cooltime = 20000, rangemin = 0, rangemax = 800, target = 3, rate = 20 , combo1 = "20,60,2", combo2 = " 2,70,2", combo3 = "15,100,2", combo4 = "0,100,0"},
--12 ���� �h P
	 {skill_index=96443, cooltime = 5000, rangemin = 0, rangemax = 700, target = 3, rate = -1},		
--13 ��ũ �극��ũ
	 {skill_index=96444, cooltime = 30000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = "0,100,0"},
--14 ����Ʈ
	 {skill_index=96445, cooltime = 35000, rangemin = 0, rangemax = 700, target = 3, rate = 30},
--15 �����̵� ����
	 {skill_index=96446, cooltime = 30000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, combo1 = "6,60,2", combo2 = "9,100,2"},
--16 ���׾�
	 {skill_index=96447, cooltime = 45000, rangemin = 0, rangemax = 900, target = 3, rate = 50, selfhppercent= 50 },
--17 ������
	 {skill_index=96448, cooltime = 20000, rangemin = 0, rangemax = 400, target = 3,rate =30, combo1 = "16,60,2", combo2 = "1,70,2", combo3 = "8,100,2" },	 
--18 ��Ʈ����Ʈ ��
	 {skill_index=96449, cooltime = 30000, rangemin = 0, rangemax = 600, target = 3,rate = 30, combo1 = "6,60,2", combo2 = " 14,100,2"},
--19 ǻ��
	 {skill_index=96450, cooltime = 60000, rangemin = 0, rangemax = 900, target = 3,rate = 50, selfhppercent= 30},
--20 �� ����
	 {skill_index=96451, cooltime = 35000, rangemin = 0, rangemax = 400, target = 3,rate = 30, combo1 = "9,60,2", combo2 = "8,70,2", combo3 = "6,100,2"},
--21 �� Ÿ��Ǭ
	 {skill_index=96452, cooltime = 60000, rangemin = 0, rangemax = 500, target = 3,rate = 50, selfhppercent= 40 },		 
	 
	 } 
  