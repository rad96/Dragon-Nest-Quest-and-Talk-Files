
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 1,		loop = 1 },
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0베리어(대)
	 {skill_index=96161,  cooltime = 20000, rangemin = 100, rangemax = 600, target = 3, rate = 30, resetcombo =1},
--1베리어(소)
	 {skill_index=96162,  cooltime = 20000, rangemin = 0, rangemax = 300, target = 3, rate = 30, resetcombo =1 },
--2디펜스
	 {skill_index=96163,  cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = -1 , resetcombo =1},
--3임펙트 어택
	 {skill_index=96164,  cooltime = 10000, rangemin = 0, rangemax = 200, target = 3, rate = 20 , combo1 = "10,60,2", combo2 = "6,100,0", resetcombo =1},
--4레이저
	 {skill_index=96165,  cooltime = 10000, rangemin = 0, rangemax = 700, target = 3, rate = 20, combo1 = " 6,100,0", resetcombo =1},
--5라인
	 {skill_index=96166,  cooltime = 2000, rangemin = 0, rangemax = 600, target = 3, rate = -1, combo1 ="8, 60,2", combo2 ="7,70,2" , combo3 = "7,80,2", combo4 = "10,100,0", resetcombo =1},
--6라인 레디
	 {skill_index=96167,  cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = 10, combo1 = "9,60,2", combo2 =" 5,100,0" , resetcombo =1},
--7난무 스타트
	 {skill_index=96168,  cooltime = 3000, rangemin = 0, rangemax = 300, target = 3, rate = 50, combo1 =" 5,50,2", combo2 ="9,100,2" , combo3 = "11,100,0", resetcombo =1},
--8난무 앤드
	 {skill_index=96169,  cooltime = 5000, rangemin = 0, rangemax = 700, target = 3, rate = -1 , combo1 = " 5, 60,2", combo2 = "10,70,0", combo3 = "10,100,0", resetcombo =1},
--9숏 슬래쉬
	 {skill_index=96170,  cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = 10 , combo1 = "7,100,2",resetcombo =1},
--10슬래쉬
	 {skill_index=96171,  cooltime = 5000, rangemin = 0, rangemax = 600, target = 3, rate = 10 , combo1 = "6,100,2", resetcombo =1},
--11어퍼
	 {skill_index=96172,  cooltime = 10000, rangemin = 0, rangemax = 250, target = 3, rate = 10 ,combo1 = "7,100,2", resetcombo =1,  target_condition = "State3" },
--12 서먼 베리어
	 {skill_index=96173,  cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = -1 , resetcombo =1},
--13 덤블링B
	 {skill_index=96174,  cooltime = 10000, rangemin = 0, rangemax = 100, target = 3, rate = -1 , resetcombo =1},
--9숏 슬래쉬
	 {skill_index=96175,  cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = -1 },
--9숏 슬래쉬
	 {skill_index=96176,  cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = -1 },	 
} 
  