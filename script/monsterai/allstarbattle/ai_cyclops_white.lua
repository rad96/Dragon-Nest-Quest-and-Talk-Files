
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0 백프론트 쵸핑
	 {skill_index=96401,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = "14,60,2", combo2 = " 3,70,2", combo3 = "4,100,2", combo4 ="17,100,0"},
--1배쉬
	 {skill_index=96402,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 20, combo1 = " 8,60,2", combo2 = " 5, 70,2", combo3 = "10,100,2"},
--2 버서크 라인
	 {skill_index=96403, cooltime = 10000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = "10,60,2" , combo2 = "18,100,2", combo3 = "7,100,0"},
--3 빅 점프
	 {skill_index=96404, cooltime = 15000, rangemin = 0, rangemax = 500, target = 3, rate = 30, combo1 = " 14,60,2", combo2 = " 4,100,0" },
--4 캐드 어택
	 {skill_index=96405, cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = " 0,60,2", combo2 = " 12,100,2", combo3 = "22,100,0" },	
--5 데스 허그
	 {skill_index=96406, cooltime = 30000, rangemin = 0, rangemax = 900, target = 3, rate = 40, combo1 = "20,100,2" },	
--6 데스 허그p
	 {skill_index=96407, cooltime = 5000, rangemin = 0, rangemax = 1200, target = 3, rate = -1 },
--7 아이오브 일렉트릭
	 {skill_index=96408, cooltime = 15000, rangemin = 0, rangemax = 900, target = 3, rate = 20, combo1 = "17,100,0" },
--8 헤비대쉬
	 {skill_index=96409, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 20, combo1 = "14,100,0" },
--9 혼 어택
	 {skill_index=96410, cooltime = 5000, rangemin = 0, rangemax = 600, target = 3, rate = 30 , combo1 = " 4,60,2", combo2 = " 2,80,2", combo3 = "10,100,2",combo4 = "13,100,0"},
--10 점프 어택
	 {skill_index=96411, cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = " 9,60,2", combo2 = " 3,100,2", combo3 = "16,100,0" },	
--11 점프 어택P
	 {skill_index=96412, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1 },
--12 라이트닝 스톰
	 {skill_index=96413, cooltime = 30000, rangemin = 0, rangemax = 700, target = 3, rate = 50, combo1 = " 21,100,0", selfhppercent= 60},		
--13 라이트닝 차져
	 {skill_index=96414, cooltime = 10000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = " 7,60,2", combo2 = "17,100,2"},
--14 펀치
	 {skill_index=96415, cooltime = 10000, rangemin = 0, rangemax = 500, target = 3, rate = 20, combo1 =" 9,60,2", combo2 = " 1,100,2", combo3 =" 3, 100,0"},
--15 리바이브
	 {skill_index=96416, cooltime = 60000, rangemin = 0, rangemax = 1200, target = 3, rate = 50, selfhppercent= 30, limitcount = 3},
--16 스턴 샤우트
	 {skill_index=96417, cooltime = 30000, rangemin = 0, rangemax = 900, target = 3, rate = 40 },
--17 렐릭 블루
	 {skill_index=96418, cooltime = 15000, rangemin = 0, rangemax = 600, target = 3,rate = 30, combo1 = " 18,20,0"},	 
--18 렐릭 레드
	 {skill_index=96419, cooltime = 15000, rangemin = 0, rangemax = 600, target = 3,rate = 30, combo2 ="17,20,0"},
--19 터닝 슬래쉬
	 {skill_index=96420, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3,rate = -1},
--20 데스허그 피니쉬
	 {skill_index=96421, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3,rate = -1, combo1 = "9,60,2"},
--21 렐릭 난무
	 {skill_index=96422, cooltime = 30000, rangemin = 0, rangemax = 500, target = 3,rate = 30, combo1 = " 22, 100,0" },	
--22 렐릭 발동!
	 {skill_index=96423, cooltime = 20000, rangemin = 0, rangemax = 600, target = 3,rate = 30},	 
	 
	 } 
  