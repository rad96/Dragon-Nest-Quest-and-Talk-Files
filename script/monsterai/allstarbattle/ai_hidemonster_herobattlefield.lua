
g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 5000;
g_Lua_NearValue2 = 9000;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;

g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_Skill =
{
     { skill_index = 34997,  cooltime = 7000, rangemin = 0, rangemax = 9000, target = 3, rate = -1 },
} 
  