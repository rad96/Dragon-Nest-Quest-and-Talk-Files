
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0콤보
	 {skill_index=96346,  cooltime = 3000, rangemin = 0, rangemax = 400, target = 3, rate = 50, combo1 = "15,60,2", combo2 = " 2,100,2", combo3 = " 6,100,0", target_condition = "State3"},
--1콤보패시브
	 {skill_index=96347,  cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = -1},
--2사이클론슬래쉬
	 {skill_index=96348, cooltime = 15000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 =" 8,60,2", combo2 = "13,100,2"},
--3대쉬난무L
	 {skill_index=96349, cooltime = 3000, rangemin = 0, rangemax = 400, target = 3, rate = -1 },
--4대쉬난무S
	 {skill_index=96350, cooltime = 20000, rangemin = 0, rangemax = 500, target = 3, rate = 30, combo1 = " 3,100,0"},	
--5대쉬난무E
	 {skill_index=96351, cooltime = 3000, rangemin = 0, rangemax = 400, target = 3, rate = -1},	
--6하울
	 {skill_index=96352, cooltime = 25000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, combo1 =" 8,60,2", combo2 = "7,100,2", combo3 = "10,100,0"},
--7어스퀘이크
	 {skill_index=96353, cooltime = 5000, rangemin = 0, rangemax = 900, target = 3, rate = -1 },
--8라인드라이브
	 {skill_index=96354, cooltime = 15000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1= "0,60,2", combo2 = "4,70,2", combo3 = "2,80,2" , combo4 = " 6,100,0" },
--9피스트
	 {skill_index=96355, cooltime = 5000, rangemin = 0, rangemax = 600, target = 3, rate = -1},
--10스워드
	 {skill_index=96356, cooltime = 20000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1= " 0,60,2", combo2 = "2,100,0" },	
--11어퍼p
	 {skill_index=96357, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1 },
--12휠 타이푼
	 {skill_index=96358, cooltime = 45000, rangemin = 0, rangemax = 600, target = 3, rate = 50, selfhppercent=40},	
--13어스퀘이크 쿨타임 체크용
	 {skill_index=96359, cooltime = 25000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = "7,100,2"},
--14피스트 패시브
	 {skill_index=96360, cooltime = 5000, rangemin = 0, rangemax = 1200, target = 3, rate = -1},
--15어퍼
	 {skill_index=96361, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3,rate = 30, combo1 =" 0,70,2", combo2 = " 4,100,2", target_condition = "State3"},
--16덤블링
	 {skill_index=96362, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1 },

} 
  