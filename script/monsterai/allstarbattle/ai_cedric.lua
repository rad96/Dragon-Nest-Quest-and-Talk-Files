
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0볼트
	 {skill_index=96281,  cooltime = 20000, rangemin = 0, rangemax = 600, target = 3,rate = 30, combo1 =" 6,100,2", combo2 = "11,100,0" },
--1대쉬
	 {skill_index=96282,  cooltime = 10000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = "0,70,2", combo2 = " 6,100,2"},
--2프레임
	 {skill_index=96283., cooltime = 15000, rangemin = 0, rangemax = 900, target = 3,rate = 50, blowcheck = "325"},
--3힐
	 {skill_index=96284, cooltime = 45000, rangemin = 0, rangemax = 600, target = 3, rate = 40, combo1 = "11,100,0"},
--4라이트닝
	 {skill_index=96285, cooltime = 8000, rangemin = 0, rangemax = 600, target = 3, rate = 20 , limitcount = 4},	
--5서몬
	 {skill_index=96286, cooltime = 450000, rangemin = 0, rangemax = 700, target = 3,rate = 80, selfhppercent=90},	
--6어퍼
	 {skill_index=96287, cooltime = 8000, rangemin = 0, rangemax = 300, target = 3,rate = 20, combo1 = "7,70,2", combo2 = "0,100,2"},
--7완드평타1
	 {skill_index=96288, cooltime = 3000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 = "8,100,0", resetcombo =1},
--8완드평타2
	 {skill_index=96289, cooltime = 1000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 = " 6, 40,2", combo2 = "9,100,0", resetcombo =1},
--9완드평타3
	 {skill_index=96290, cooltime = 1000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 = "10,100,0", resetcombo =1},
--10완드평타4
	 {skill_index=96291, cooltime = 1000, rangemin = 0, rangemax = 400, target = 3, rate = -1, combo1 = "6,60,2", combo2 = "4,100,2" },	
--11텔레포트B
	 {skill_index=96292, cooltime = 5000, rangemin = 0, rangemax = 900, target = 3, rate = 30, resetcombo =1},

} 
  