--AiMinotauros_Red_Boss_Nest.lua

g_Lua_NearTableCount = 5
g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1100;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2100;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 400;
g_Lua_AssaultTime = 5000;

g_Lua_CustomAction =
{
   CustomAction1 = 
   {{ "Attack005_Upper", 0 },{ "Attack004_SwingFire_Enchant", 0 },},
   CustomAction2 =
   {{ "Attack003_JumpAttack", 0 },{ "Attack017_Chop", 0 },},
}

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Attack001_bash", rate = 10, loop = 1 },
   { action_name = "CustomAction1", rate = 10, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Attack003_JumpAttack", rate = 13, loop = 1 },
   { action_name = "CustomAction2", rate = 16, loop = 1 },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 3, loop = 1 },
   { action_name = "Attack003_JumpAttack", rate = 15, loop = 1 },
   { action_name = "CustomAction2", rate = 10, loop = 1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
}
g_Lua_Near5 = 
{ 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 40, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 33258, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 1300, target = 3 ,next_lua_skill_index=1},--0 무브프론트
   { skill_index = 33309, cooltime = 3000 ,rate = -1, rangemin = 100, rangemax = 1300, target = 3 },--1 비열한공격
   { skill_index = 33260, cooltime = 32000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, next_lua_skill_index=0,usedskill=32453,priority=1},--2 대점프
   { skill_index = 33251, cooltime = 28000, rate = 40, rangemin = 0, rangemax = 1500, target = 3, encountertime=10000, notusedskill=33263 ,},   --3 플레임대쉬
   { skill_index = 33252, cooltime = 28000, rate = 40, rangemin = 0, rangemax = 1500, target = 3, usedskill=33263},--4 플레임대쉬강화
   { skill_index = 33253, cooltime = 10000, rate = 55, rangemin = 0, rangemax = 1000, target = 3, next_lua_skill_index=6 },--5 파이어브레스
   { skill_index = 33255, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, },   --6 찍기강화
   { skill_index = 33264, cooltime = 12000 ,rate = 40, rangemin = 0, rangemax = 400, target = 3,next_lua_skill_index=8 },--7 어퍼
   { skill_index = 33265, cooltime = 1000 ,rate = -1, rangemin = 0, rangemax = 1300, target = 3 },--8 스윙파이어인챈트
   { skill_index = 33261, cooltime = 16000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, notusedskill=33263, multipletarget = "1,2",},   --9 볼케이노
   { skill_index = 33263, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 1,selfhppercentrange = "0,50",notusedskill=33263, limitcount=1 },      --11 이프리트
   { skill_index = 33257, cooltime = 40000, rate = 90, rangemin = 0, rangemax = 1500, target = 3,usedskill=33263, encountertime=10000 },      --12 이그니션
   }