
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0어썰트스트라이크
	 {skill_index=96261,  cooltime = 10000, rangemin = 200, rangemax = 600, target = 3, rate = 30, combo1 ="6,60,2", combo2 ="10,100,2", combo3 = "12,100,0" },
--1배쉬
	 {skill_index=96264,  cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = 20, combo1 = "6,60,2", combo2 = "10,100,2", combo3 = " 4,100,0"},
--2댄스
	 {skill_index=96265, cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = -1,  target_condition = "State3", combo1 ="10,100,0"},
--3대쉬어택
	 {skill_index=96266, cooltime = 15000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1= "13,100,2", combo2 ="9,100,0" },
--4해비어택
	 {skill_index=96267, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1= " 10,60,2", combo2 = "8,100,0" },	
--5하울링
	 {skill_index=96268, cooltime = 10000, rangemin = 0, rangemax = 700, target = 3, rate = -1, combo1= " 12,100,2", resetcombo =1},	
--6난무
	 {skill_index=96269, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = 40, combo1 = " 10,60,2", combo2 = " 0, 70,2", combo3 = "9,100,2", combo4 = "13,100,0" },
--7포즈
	 {skill_index=96270, cooltime = 15000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = " 14,100,2", resetcombo =1},
--8푸쉬드
	 {skill_index=96271, cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = 40, combo1 = " 6,70,2", combo2 = "1,100,1"},
--9쓰로잉 웨폰
	 {skill_index=96272, cooltime = 5000, rangemin = 200, rangemax = 1200, target = 3, rate = 20},
--10어퍼
	 {skill_index=96273, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, combo1 ="6,60,2", combo2 = " 15,70,2", combo3 = "7,80,2", combo4 = " 16,100,2" },	
--11원기옥
	 {skill_index=96274, cooltime = 5000, rangemin = 0, rangemax = 900, target = 3, rate = -1, resetcombo =1},
--12에네르기파
	 {skill_index=96275, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = "0,100,2"},	
--13슈퍼스톰프
	 {skill_index=96276, cooltime = 20000, rangemin = 0, rangemax = 600, target = 3, rate = 30, cancellook = 0 , combo1 = " 0,60,2", combo2 = "12,100,2"},
--14포즈A
	 {skill_index=96277, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = -1, combo1 = " 10,60,2", combo2 = "6,100,2", combo3 = "3,100,0", resetcombo =1},
--15하울링 스타트
	 {skill_index=96278, cooltime = 20000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = "5,100,2", resetcombo =1},
--16원기옥 스타트
	 {skill_index=96279, cooltime = 40000, rangemin = 0, rangemax = 900, target = 3, rate = 30, cancellook = 0, combo1 = "11,100,0" , resetcombo =1, selfhppercent= 60},
--17덤블링R
	 {skill_index=96280, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, cancellook = 0 },	 
} 
  