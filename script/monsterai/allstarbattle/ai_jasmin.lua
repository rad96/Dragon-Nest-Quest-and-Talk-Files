
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0빅메카
	 {skill_index=96221,  cooltime = 15000, rangemin = 0, rangemax = 500, target = 3, rate = 20, combo1 = "3,60,2", combo2 = " 2,70,0", combo3 = " 9,100,0"},
--1버프
	 {skill_index=96222,  cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = -1},
--2크로스
	 {skill_index=96223, cooltime = 20000, rangemin = 0, rangemax = 600, target = 3, rate = 10, combo1 = " 15,60,2", combo2 = " 9,70,2", combo3 =" 4,100,0"},
--3마그마펀치
	 {skill_index=96224, cooltime = 8000, rangemin = 0, rangemax = 600, target = 3, rate = 20, combo1 = " 9,60,2", combo2 =" 15,100,2", combo3 = "5,100,0"},
--4마그마스플래쉬
	 {skill_index=96225, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = -1, combo1 = " 9,60,2", combo2 =" 15,100,2"},	
--5마그마스플래쉬A
	 {skill_index=96226, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = -1, combo1 = " 9,60,2", combo2 =" 15,100,2"},	
--6메카
	 {skill_index=96227, cooltime = 15000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, combo1= "13,100,0" },
--7난무
	 {skill_index=96228, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 20, combo1 = " 9, 60,2", combo2 ="11,70,2", combo3 = "2,100,2", combo4 ="3,100,0"},
--8포이즌 풀
	 {skill_index=96229, cooltime = 25000, rangemin = 0, rangemax = 500, target = 3, rate = 30, combo1 = "4,60,2" , selfhppercent= 40},
--9스패너 점프
	 {skill_index=96230, cooltime = 10000, rangemin = 0, rangemax = 200, target = 3, rate = 30 , combo1 ="15,60,2", combo2 ="2,70,2"},
--10스패너 점프P
	 {skill_index=96231, cooltime = 8000, rangemin = 0, rangemax = 900, target = 3, rate = -1 },	
--11스패너 스윙
	 {skill_index=96232, cooltime = 5000, rangemin = 0, rangemax = 900, target = 3, rate = -1, combo1 = " 15,60,2", combo2 = "4,100,2" },
--12스패너 콤보
	 {skill_index=96233, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 30, combo1 = " 14,50,1", combo2 ="15,100,0" },	
--13덤블링
	 {skill_index=96234, cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = -1, cancellook = 0 },
--14스패너 콤보A
	 {skill_index=96235, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3, rate = -1, cancellook = 0, combo1 ="15,50,1", combo2 = "0, 100,0"},
--15난무A
	 {skill_index=96236, cooltime = 8000, rangemin = 0, rangemax = 300, target = 3, rate = 30, cancellook = 0, combo1 = " 9, 60,2", combo2 ="11,70,2", combo3 = "2,100,2", combo4 ="3,100,0"},
--16덤블링L
	 {skill_index=96237, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, cancellook = 0 },
--17덤블링R
	 {skill_index=96238, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = -1, cancellook = 0 },	 
} 
  