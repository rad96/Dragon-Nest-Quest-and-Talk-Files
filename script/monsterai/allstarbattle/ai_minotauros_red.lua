
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{
--0 에임
	 {skill_index=96491,  cooltime = 35000, rangemin = 0, rangemax = 900, target = 3, rate = 40, combo1 = " 14,60,2", combo2 = " 5,100,2"},
--1 배쉬
	 {skill_index=96492,  cooltime = 8000, rangemin = 0, rangemax = 300, target = 3, rate = 30 , combo1 = " 5,60,2" ,combo2 = "4,70,2", combo3 ="10,100,2", combo4 = " 8,100,0"},
--2 버서커
	 {skill_index=96493, cooltime = 180000, rangemin = 0, rangemax = 1200, target = 3, rate = 20, selfhppercent= 30},
--3 빅 배쉬
	 {skill_index=96494, cooltime = 25000, rangemin = 0, rangemax = 700, target = 3, rate = 20, combo1 = "14,60,2", combo2 = "9,100,2"},
--4 춉
	 {skill_index=96495, cooltime = 8000, rangemin = 0, rangemax = 500, target = 3, rate = 20, combo1 = " 5,60,2", combo2 = " 7,100,2"},	
--5 콤보
	 {skill_index=96496, cooltime = 8000, rangemin = 0, rangemax = 400, target = 3, rate = 40, combo1 = " 10,60,2", combo2 =" 7,70,2", combo3 = "14,100,2", combo4 = "3,100,0" },	
--6 대쉬
	 {skill_index=96497, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = "7,60,2", combo2 = " 8,100,2", combo3 = "9,100,0"},
--7 대쉬 어택
	 {skill_index=96498, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = " 5,60,2" , combo2 = " 1,100,2"},
--8 피어 샤우트
	 {skill_index=96499, cooltime = 30000, rangemin = 0, rangemax = 600, target = 3, rate = 20, combo1 = " 14,60,2", combo2 = " 10,100,2"},
--9 하울
	 {skill_index=96500, cooltime = 30000, rangemin = 0, rangemax = 600, target = 3, rate = 30},
--10 난동
	 {skill_index=96501, cooltime = 15000, rangemin = 0, rangemax = 800, target = 3, rate = 30, combo1 = "12,60,2", combo2 = " 6,70,2", combo3 = " 4,100,2"},	
--11 푸쉬
	 {skill_index=96502, cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 40, combo1 = " 13,60,2", combo2 = "5,100,2"},
--12 슬래쉬
	 {skill_index=96503, cooltime = 15000, rangemin = 0, rangemax = 500, target = 3, rate = 20, combo1 ="7,60,2" ,combo2 = "3,100,2"},		
--13 스톰프
	 {skill_index=96504, cooltime = 8000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = " 5,100,2"},
--14 쓰로우 엑스
	 {skill_index=96505, cooltime = 20000, rangemin = 0, rangemax = 700, target = 3, rate = 30, combo1 = "8,70,0", combo2 = "15,100,0"},
--15 웨폰 파워
	 {skill_index=96506, cooltime = 40000, rangemin = 0, rangemax = 1200, target = 3, rate = 50, selfhppercent= 50, combo1 = " 5,60,2", combo2 = " 7,100,2"},
--16 웨폰 파워air
	 {skill_index=96507, cooltime = 5000, rangemin = 0, rangemax = 900, target = 3, rate =-1 },
--17 웨폰 파워P
	 {skill_index=96508, cooltime = 5000, rangemin = 0, rangemax = 400, target = 3,rate =-1 },	 

	 
	 } 
  