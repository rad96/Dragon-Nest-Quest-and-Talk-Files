--AiGreatBeatle_Green_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 55000
g_Lua_GlobalCoolTime2 = 25000
g_Lua_GlobalCoolTime3 = 30000

g_Lua_CustomAction =
{
	 CustomAction1 = 
     {
          { "Fly", 0},
          { "Attack07_ThrowChainSaw", 0},
		  { "Fly", 0},
	      { "Attack08_Landing", 0},
     },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 1, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Left", rate = 3, loop = 1 , custom_state1 = "custom_ground"},
   { action_name = "Walk_Right", rate = 3, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "CustomAction1", rate = 30, loop = 1, custom_state1 = "custom_fly" },
   { action_name = "Attack01_ChainSaw", rate = 30, loop = 1, custom_state1 = "custom_ground"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 1, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Left", rate = 3, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Right", rate = 3, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Front", rate = 8, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "CustomAction1", rate = 30, loop = 1, custom_state1 = "custom_fly" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Front", rate = 12, loop = 2, custom_state1 = "custom_ground" },
   { action_name = "Assault", rate = 19, loop = 1, custom_state1 = "custom_ground"  },
   { action_name = "CustomAction1", rate = 30, loop = 1, custom_state1 = "custom_fly" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Front", rate = 12, loop = 2, custom_state1 = "custom_ground" },
   { action_name = "CustomAction1", rate = 30, loop = 1, custom_state1 = "custom_fly" },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 8, loop = 1, approach = 200, custom_state1 = "custom_ground" },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 200, custom_state1 = "custom_ground" },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 200, custom_state1 = "custom_ground" },
   { action_name = "Attack01_ChainSaw", rate = 12, loop = 1, approach = 200 , custom_state1 = "custom_ground" },
}
g_Lua_Skill = {
   -- 방전
   { skill_index = 33188,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3 , randomtarget=1.1, custom_state1 = "custom_ground"  },
   -- 돌진
   { skill_index = 33185,  cooltime = 8000, rate = 100, rangemin = 150, rangemax = 1000, target = 3, randomtarget=1.1, custom_state1 = "custom_ground" },
   -- 피뢰침
   { skill_index = 33186,  cooltime = 10000, rate = 100, rangemin = 200, rangemax = 700, target = 3, randomtarget=1.1, custom_state1 = "custom_ground"  },
   -- 독액발사
   { skill_index = 33187,  cooltime = 18000, rate = 100, rangemin = 0, rangemax = 700, target = 3 , randomtarget=1.1, custom_state1 = "custom_ground" },
   -- 밀어내기
   { skill_index = 33189,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 4000, target = 3 , randomtarget=1.1, encountertime = 15000, next_lua_skill_index = 0 , selfhppercentrange = "0,80", custom_state1 = "custom_ground" },
   -- 상승 : 1페이즈
   { skill_index = 33193,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3 , randomtarget=1.1, encountertime = 10000, selfhppercentrange = "0,80", custom_state1 = "custom_ground", limitcount = 1},
   -- 상승 : 3페이즈
   -- { skill_index = 33193,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3 , randomtarget=1.1, encountertime = 25000, selfhppercentrange = "0,40", custom_state1 = "custom_ground", limitcount = 1},
}
