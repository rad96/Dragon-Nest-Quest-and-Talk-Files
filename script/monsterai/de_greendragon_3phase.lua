--GreenDragonNest_FinalBoss_GreenDragon_3Phase.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 2000;
g_Lua_NearValue2 = 3000;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 20000
g_Lua_GlobalCoolTime2 = 30000
g_Lua_GlobalCoolTime3 = 90000
g_Lua_GlobalCoolTime4 = 30000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_Ground"  },
   { action_name = "Attack11_Bite&Attack", rate = 20, loop = 1, td = "FL,FR,LF",  cooltime = 10000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack12_Right&LeftAttack", rate = 20, loop = 1, td = "FL,FR,RF",  cooltime = 10000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack55_LeftSideAttack", rate = 40, loop = 1, td = "FL,LF,LB",  cooltime = 10000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack56_RightSideAttack", rate = 40, loop = 1, td = "FR,RF,RB",  cooltime = 10000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack14_TailAttack", rate = 50, loop = 1, td = "RB,BR,BL,LB",  cooltime = 20000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack14_TailAttack", rate = 30, loop = 1, td = "FL,FR,LF,RF",  cooltime = 30000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack13_Stomp&Tail", rate = 40, loop = 1, cooltime = 30000, custom_state1 = "custom_Ground" },
   { action_name = "Attack57_PushingWind", rate = 40, loop = 1, td = "RF,LF,RB,BR,BL,LB",  cooltime = 30000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack33_CliffAttack", rate = 20, loop = 1,cooltime = 50000, custom_state1 = "custom_Fly2"  },
   { action_name = "Attack54_FlyLanding", rate = 30, loop = 1, cooltime = 50000, custom_state1 = "custom_Fly"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 100, loop = 1, custom_state1 = "custom_Ground"  },
   { action_name = "Attack11_Bite&Attack", rate = 20, loop = 1, td = "FL,FR,LF",  cooltime = 10000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack12_Right&LeftAttack", rate = 20, loop = 1, td = "FL,FR,LF,RF",  cooltime = 10000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack14_TailAttack", rate = 50, loop = 1, td = "RB,BR,BL,LB",  cooltime = 20000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack13_Stomp&Tail", rate = 40, loop = 1, cooltime = 30000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack57_PushingWind", rate = 40, loop = 1, td = "RF,LF,RB,BR,BL,LB",  cooltime = 30000, custom_state1 = "custom_Ground"  },
   { action_name = "Attack33_CliffAttack", rate = 20, loop = 1, cooltime = 50000, custom_state1 = "custom_Fly2"  },
   { action_name = "Attack54_FlyLanding", rate = 30, loop = 1, cooltime = 50000, custom_state1 = "custom_Fly"  },
}


g_Lua_Skill = {

-- 비상 볼케이노
   { skill_index = 31592,  cooltime = 50000, rate = -1, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "0,40", custom_state1 = "custom_Fly" },
-- 비상 글레이서
   { skill_index = 31593,  cooltime = 50000, rate = -1, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "0,40", custom_state1 = "custom_Fly", multipletarget = "1,1" },


-- 절벽 브레스_1
   { skill_index = 31581,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "35,70", custom_state1 = "custom_Ground", limitcount = 1 },
-- 절벽 브레스_2
   { skill_index = 31581,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "0,40", custom_state1 = "custom_Ground", limitcount = 1 },
-- 절벽 브레스_3
   { skill_index = 31581,  cooltime = 60000, rate = -1, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "16,40", custom_state1 = "custom_Ground", limitcount = 1 },
-- 절벽 브레스_4
   { skill_index = 31581,  cooltime = 60000, rate = -1, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "15,25", custom_state1 = "custom_Ground", limitcount = 1 },

-- 포이즌 미스트
   { skill_index = 31560,  cooltime = 50000, rate = 100, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "0,20", custom_state1 = "custom_Ground", multipletarget = "1,1", next_lua_skill_index = 17 },
-- 나무 소환1
   { skill_index = 31559,  cooltime = 40000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "0,25", custom_state1 = "custom_Ground", globalcooltime = 4},
-- 나무 소환2
   { skill_index = 31563,  cooltime = 40000, rate = -1, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "0,25", custom_state1 = "custom_Ground", globalcooltime = 4},


-- 비상 몬스터소환_볼케이노
   { skill_index = 31591,  cooltime = 120000, rate = 50, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "0,40", custom_state1 = "custom_Ground", next_lua_skill_index = 0, globalcooltime = 3},
-- 비상 몬스터소환_글레이서
   { skill_index = 31591,  cooltime = 120000, rate = 50, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "0,40", custom_state1 = "custom_Ground", next_lua_skill_index = 1, globalcooltime = 3},


-- 절벽 이동
   { skill_index = 31582,  cooltime = 150000, rate = 80, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "20,70", custom_state1 = "custom_Ground"},
-- 절벽 파이어볼
   { skill_index = 31583,  cooltime = 90000, rate = 80, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "0,75", custom_state1 = "custom_Fly2"},
-- 절벽 아이스월
   { skill_index = 31584,  cooltime = 90000, rate = 80, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "0,75", custom_state1 = "custom_Fly2", multipletarget = "1,6"},
-- 절벽 날갯짓
   { skill_index = 31585,  cooltime = 90000, rate = 80, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "0,40", custom_state1 = "custom_Fly2"},
 
-- 넝쿨 소환
   { skill_index = 31555,  cooltime = 30000, rate = -1, rangemin = 800, rangemax = 40000, target = 3,  selfhppercentrange = "0,75", custom_state1 = "custom_Ground", randomtarget=1.1},
-- 정면 포이즌 브레스
   { skill_index = 31556,  cooltime = 20000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "0,75", custom_state1 = "custom_Ground", globalcooltime = 2 },

-- 포이즌 샤워
   { skill_index = 31561,  cooltime = 30000, rate = 80, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "0,75", custom_state1 = "custom_Ground", multipletarget = "1,4"},
-- 절벽 리턴 브레스
   { skill_index = 31586,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "0,100", custom_state1 = "custom_Fly2"},

-- 플레임 도넛
   { skill_index = 31557,  cooltime = 50000, rate = 60, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "25,70", custom_state1 = "custom_Ground", globalcooltime = 2, multipletarget = "1,1", randomtarget=1.1},
-- 블리자드 (?)
   { skill_index = 31558,  cooltime = 50000, rate = 60, rangemin = 0, rangemax = 40000, target = 3,  selfhppercentrange = "25,70", custom_state1 = "custom_Ground", globalcooltime = 2, randomtarget=1.1},

-- 라이트브레스
   { skill_index = 31553,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,RF", selfhppercentrange = "35,70", custom_state1 = "custom_Ground", globalcooltime = 1},
-- 레프트브레스
   { skill_index = 31554,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF", selfhppercentrange = "35,70", custom_state1 = "custom_Ground", globalcooltime = 1},
}