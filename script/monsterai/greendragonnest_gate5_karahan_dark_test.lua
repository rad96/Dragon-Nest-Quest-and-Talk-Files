-- GreenDragonNest_Gate5_Karahan_Dark

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 800.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3000.0;



g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;


g_Lua_GlobalCoolTime1 = 70000


g_Lua_Near1 = 
{ 
	{ action_name = "Stand_4",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Back",	rate = 15,		loop = 1 },
	{ action_name = "Move_Back",	rate = 5,		loop = 1 },
--	{ action_name = "Attack01_Push",	rate = 20,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_4",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 12,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 1 },
--	{ action_name = "Attack02_MagicMissile",	rate = 25,		loop = 1 },
}


g_Lua_Near3 = 
{ 
	{ action_name = "Stand_4",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 20,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Back",	rate = 5,		loop = 1 },
--	{ action_name = "Attack02_MagicMissile",	rate = 30,		loop = 1 },
}


g_Lua_Near4 = 
{ 
	{ action_name = "Stand_4",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 1,		loop = 1},
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
	{ action_name = "Move_Front",	rate = 25,		loop = 2 },
	{ action_name = "Move_Back",	rate = 1,		loop = 1},
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand_4",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
	{ action_name = "Move_Front",	rate = 30,		loop = 2 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Upper",	rate = 15,		loop = 1, cancellook = 0, approach = 150.0 },
	{ action_name = "Attack5_ImpactAttack",	rate = 10,		loop = 1, cancellook = 0, approach = 200.0 },
}


g_Lua_SkillProcessor = {
   { skill_index = 31353,  changetarget = "1500,1"  },
}


g_Lua_Skill=
{
-- 파이어볼
	{ skill_index = 31358, cooltime = 30000, rangemin = 0, rangemax = 3000, target = 3, rate = 100, selfhppercentrange = "40,100" },
-- 블리자드
--	{ skill_index = 31359, cooltime = 30000, rangemin = 0, rangemax = 3000, target = 3, rate = 80, selfhppercentrange = "40,60" },
-- 마나 폭발
--	{ skill_index = 31365, cooltime = 30000, rangemin = 0, rangemax = 3000, target = 3, rate = 80, selfhppercentrange = "20,40" },
-- 레이즈 그라비티
--	{ skill_index = 31363, cooltime = 30000, rangemin = 0, rangemax = 3000, target = 3, rate = 80, selfhppercentrange = "20,40", multipletarget = "1"},


-- 글레이셜 웨이브 - 마나 폭발
--	{ skill_index = 31357, cooltime = 90000, rangemin = 0, rangemax = 3000, target = 3, rate = 80, selfhppercentrange = "40,60" },


-- 플레임 볼텍스
--	{ skill_index = 31366, cooltime = 40000, rangemin = 0, rangemax = 3000, target = 3, rate = 50, selfhppercentrange = "0,20" },
-- 프리징 스파이크
--	{ skill_index = 31367, cooltime = 40000, rangemin = 0, rangemax = 3000, target = 3, rate = 60, selfhppercentrange = "0,20", multipletarget = "1,5" },
-- 블랙홀
	{ skill_index = 31368, cooltime = 60000, rangemin = 0, rangemax = 3000, target = 3, rate = 80, selfhppercentrange = "0,20"},


-- 암흑버프
--	{ skill_index = 31362, cooltime = 30000, rangemin = 0, rangemax = 3000, target = 3, rate = 100, selfhppercentrange = "20,40" },
-- 잡아던지기
--	{ skill_index = 31364, cooltime = 30000, rangemin = 0, rangemax = 3000, target = 3, rate = 50, selfhppercentrange = "20,40" },



-- 롤링라바
--	{ skill_index = 31360, cooltime = 45000, rangemin = 0, rangemax = 3000, target = 3, rate = 50, selfhppercentrange = "40,60" },
-- 아이스 배리어
--	{ skill_index = 31361, cooltime = 90000, rangemin = 0, rangemax = 1500, target = 1, rate = 80, selfhppercentrange = "40,60" },



-- 플레임 로드
--	{ skill_index = 31353, cooltime = 30000, rangemin = 200, rangemax = 2000, target = 3, rate = 30, selfhppercentrange = "60,80" },
-- 파이어 버드
--	{ skill_index = 31354, cooltime = 30000, rangemin = 200, rangemax = 2000, target = 3, rate = 30, selfhppercentrange = "60,80" },
-- 아이스 니들
--	{ skill_index = 31355, cooltime = 30000, rangemin = 500, rangemax = 2000, target = 3, rate = 30, selfhppercentrange = "60,80", multipletarget = "1,2" },
-- 아이스 소드
--	{ skill_index = 31356, cooltime = 30000, rangemin = 0, rangemax = 2000, target = 3, rate = 30, selfhppercentrange = "60,80", multipletarget = "1" },


-- 플레임웜
--	{ skill_index = 31351, cooltime = 15000, rangemin = 0, rangemax = 1500, target = 3, rate = 50, selfhppercentrange = "80,100"},
-- 아이스 웨이브
--	{ skill_index = 31352, cooltime = 15000, rangemin = 0, rangemax = 1500, target = 3, rate = 50, selfhppercentrange = "80,100" },
}