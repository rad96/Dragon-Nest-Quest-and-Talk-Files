--AiHalfGolem_Stone_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 700;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 2000;
g_Lua_NearValue4 = 4000;
g_Lua_NearValue5 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack03_StompRight", 0 },
      { "Attack01_BashRight", 0 },
  },
  CustomAction2 = {
      { "Attack04_StompLeft", 0 },
      { "Attack02_BashLeft", 0 },
  },
}

g_Lua_GlobalCoolTime1 = 30000


g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000 },
   { action_name = "Attack05_PunchBreak", rate = 8, loop = 1, cooltime = 15000, td = "FR,FL", selfhppercent = 100 },
   { action_name = "CustomAction1", rate = 4, loop = 1, cooltime = 15000, td = "FR,RF,RB,BR", selfhppercentrange = "40,100" },
   { action_name = "CustomAction2", rate = 4, loop = 1, cooltime = 15000, td = "FL,LF,LB,BL", selfhppercentrange = "40,100" },
}

g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000 },
   { action_name = "Attack10_EyeLaser_Front", rate = 5, loop = 1, cooltime = 20000, td = "LF,FL,FR,RF", selfhppercentrange = "40,100" },
   { action_name = "Attack10_EyeLaser_Left", rate = 5, loop = 1, cooltime = 20000, td = "LF,FL,LB", selfhppercentrange = "40,100" },
   { action_name = "Attack10_EyeLaser_Right", rate = 5, loop = 1, cooltime = 20000, td = "FR,RF,RB", selfhppercentrange = "40,100" },
   { action_name = "CustomAction1", rate = 4, loop = 1, cooltime = 15000, td = "FR,RF,RB,BR", selfhppercentrange = "40,100" },
   { action_name = "CustomAction2", rate = 4, loop = 1, cooltime = 15000, td = "FL,LF,LB,BL", selfhppercentrange = "40,100" },
}

g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000 },
   { action_name = "Attack10_EyeLaser_Front", rate = 10, loop = 1, cooltime = 20000, td = "LF,FL,FR,RF", selfhppercentrange = "40,100" },
   { action_name = "Attack10_EyeLaser_Left", rate = 10, loop = 1, cooltime = 20000, td = "LF,FL,LB", selfhppercentrange = "40,100" },
   { action_name = "Attack10_EyeLaser_Right", rate = 10, loop = 1, cooltime = 20000, td = "FR,RF,RB", selfhppercentrange = "40,100" },
}

g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000 },
   { action_name = "Attack10_EyeLaser_Front", rate = 10, loop = 1, cooltime = 20000, td = "LF,FL,FR,RF", selfhppercentrange = "40,100" },
   { action_name = "Attack10_EyeLaser_Left", rate = 10, loop = 1, cooltime = 20000, td = "LF,FL,LB", selfhppercentrange = "40,100" },
   { action_name = "Attack10_EyeLaser_Right", rate = 10, loop = 1, cooltime = 20000, td = "FR,RF,RB", selfhppercentrange = "40,100" },
}

g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, cooltime = 15000 },
}

g_Lua_Skill = { 
-- 어스퀘이크
   { skill_index = 30983,  cooltime = 30000, rate = 70, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 25 },
-- 센터레이저
   { skill_index = 30979,  cooltime = 40000, rate = 70, rangemin = 500, rangemax = 4000, target = 3, selfhppercent = 50, globalcooltime = 1 },
-- 서먼 볼케이노
   { skill_index = 30984,  cooltime = 50000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 50 },
-- 외치기
   { skill_index = 30973,  cooltime = 50000, rate = 50, rangemin = 300, rangemax = 4000, target = 3, selfhppercent = 75, globalcooltime = 1 },
-- 서먼 락
   { skill_index = 30982,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 75 },
-- 포이즌 브레스
   { skill_index = 30972,  cooltime = 30000, rate = 50, rangemin = 400, rangemax = 4000, target = 3, selfhppercent = 75 },
}
