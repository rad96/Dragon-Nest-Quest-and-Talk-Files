--AiCube_White_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 35000
g_Lua_GlobalCoolTime2 = 13000
g_Lua_GlobalCoolTime3 = 35000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Front", rate = 6, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
}

g_Lua_Skill = { 
   -- 돌진
   { skill_index = 33053, next_lua_skill_index = 1, cooltime = 35000, rate = 40, rangemin = 0, rangemax = 1500, target = 3 },
   { skill_index = 33054, rate = -1, rangemin = 0, rangemax = 5500, target = 3, randomtarget=1.0 },
   -- 트랩
   { skill_index = 31737, cooltime = 40000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "50,100", globalcooltime = 1, encountertime = 15000 },
   -- 혼란
   { skill_index = 33074, cooltime = 35000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 49, globalcooltime = 1 },
   -- 지향성 산탄(블루),  이속감소 효과
   { skill_index = 31732, cooltime = 30000, rate = 60, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "50,100", globalcooltime = 2 },
   -- 속사포(옐로우), 데미지 존나 쎔, 전기
   { skill_index = 31734, cooltime = 48000, rate = 40, rangemin = 200, rangemax = 1400, target = 3, globalcooltime = 2  },
   -- 전방향 산탄(블랙), 혼란효과
   { skill_index = 31731, cooltime = 35000, rate = 60, rangemin = 200, rangemax = 1500, target = 3, selfhppercentrange = "50,100", globalcooltime = 2 },
   -- 포격(레드) 화속
   { skill_index = 31733, cooltime = 35000, rate = 60, rangemin = 400, rangemax = 1500, target = 3, selfhppercent = 50 , globalcooltime = 2 },
   -- 진동
   { skill_index = 33059, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 50 },   
}