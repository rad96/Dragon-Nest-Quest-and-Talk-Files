--AiGargoyle_Gray_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue3 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 1  },
   { action_name = "Move_Right", rate = 4, loop = 1  },
   { action_name = "Move_Back", rate = 4, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 1  },
   { action_name = "Move_Right", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 14, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
}

g_Lua_GlobalCoolTime1 = 38000
g_Lua_GlobalCoolTime2 = 25000
g_Lua_GlobalCoolTime3 = 12000
g_Lua_GlobalCoolTime4 = 10000

g_Lua_Skill = { 
  
   { skill_index = 35101,  cooltime = 40000, rate = 60, rangemin = 200, rangemax = 3000, target = 3, td = "FR,FL", encountertime = 5000 }, -- 직선레이저

}
