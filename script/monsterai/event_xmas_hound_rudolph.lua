--AiHound_General_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 22, loop = 1 },
   { action_name = "Provoke", rate = 3, loop = 1 },
   { action_name = "Attack1_BiteCombo", rate = 1, loop = 1 },
   { action_name = "Attack2_BigBite", rate = 3, loop = 1 },
   { action_name = "Provoke", rate = 30, loop = 1, target_condition = "State1" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Attack4_Headbutt_XmasEvent", rate = 12, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
}
