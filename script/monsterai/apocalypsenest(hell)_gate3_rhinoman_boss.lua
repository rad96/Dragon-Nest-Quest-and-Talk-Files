--AiRhinoMan_Gray_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {

  CustomAction1 = {
      { "Attack4_Pushed" },
      { "Attack10_Shot" },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_bash", rate = 15, loop = 1  },
   { action_name = "CustomAction1", rate = 20, loop = 1  },
   { action_name = "Attack14_TripleSwing_Nest(Hell)", rate = 40, loop = 1, selfhppercent = 50  },
   { action_name = "Attack8_Stamp", rate = 25, loop = 1  },

}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 1  },
   { action_name = "Walk_Right", rate = 8, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_bash", rate = 15, loop = 1  },
   { action_name = "Attack14_TripleSwing_Nest(Hell)", rate = 40, loop = 1, selfhppercent = 75  },
   { action_name = "Attack10_Shot", rate = 30, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 18, loop = 1  },
   { action_name = "Attack10_Shot", rate = 100, loop = 1  },
   { action_name = "Attack14_TripleSwing_Nest(Hell)", rate = 30, loop = 1, selfhppercent = 75  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 25, loop = 1  },
   { action_name = "Attack10_Shot", rate = 90, loop = 1  },
   { action_name = "Assault", rate = 50, loop = 1  },
   { action_name = "Attack14_TripleSwing_Nest(Hell)", rate = 30, loop = 1, selfhppercent = 75  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Attack10_Shot", rate = 100, loop = 1  },
   { action_name = "Assault", rate = 50, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 10, loop = 1  },
   { action_name = "Attack10_Shot", rate = 10, loop = 1  },
   { lua_skill_index = 2, rate = 40, selfhppercent = 80  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 8, loop = 1, approach = 250.0  },
   { action_name = "Attack14_TripleSwing_Nest(Hell)", rate = 6, loop = 1, approach = 300.0  },
}
g_Lua_Skill = { 
-- 원형난무
   { skill_index = 30301,  cooltime = 12000, rate = 80,rangemin = 500, rangemax = 1500, td = "LF,FL,FR,RF", target = 3, selfhppercent = 75 },
-- 콜캐논
   { skill_index = 30330,  cooltime = 24000, rate = 80, rangemin = 0, rangemax = 1500, td = "LF,FL,FR,RF,RB,BR,BL,LB", multipletarget = 1, target = 3, selfhppercent = 50 },
-- 정면난무
   { skill_index = 30303,  cooltime = 12000, rate = 90, rangemin = 500, rangemax = 1500, target = 3, selfhppercent = 75 },
-- 헤이스트
   { skill_index = 30451,  cooltime = 20000, rate = 100, rangemin = 000, rangemax = 1500, target = 4, selfhppercent = 50 },
   { skill_index = 30451,  cooltime = 10000, rate = 100, rangemin = 000, rangemax = 1500, target = 4, selfhppercent = 25 },
}
