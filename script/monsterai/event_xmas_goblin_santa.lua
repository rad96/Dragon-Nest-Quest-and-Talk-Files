--AiGoblin_Blue_Easy.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 4000

g_Lua_Near1 = { 
   { action_name = "Stand3", rate = 6, loop = 1, globalcooltime = 1 },
   { action_name = "Stand_1", rate = 4, loop = 1, globalcooltime = 1 },
   { action_name = "Stand2", rate = 4, loop = 1, globalcooltime = 1 },
   { action_name = "Walk_Left", rate = 1, loop = 1 },
   { action_name = "Walk_Right", rate = 1, loop = 1 },
   { action_name = "Attack1_Slash", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand3", rate = 6, loop = 1, globalcooltime = 1 },
   { action_name = "Stand_1", rate = 4, loop = 1, globalcooltime = 1 },
   { action_name = "Stand2", rate = 4, loop = 1, globalcooltime = 1 },
   { action_name = "Attack6_ThrowSnowBall", rate = 10, loop = 1, cooltime = 7000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand3", rate = 6, loop = 1, globalcooltime = 1 },
   { action_name = "Stand_1", rate = 4, loop = 1, globalcooltime = 1 },
   { action_name = "Stand2", rate = 4, loop = 1, globalcooltime = 1 },
   { action_name = "Attack6_ThrowSnowBall", rate = 10, loop = 1, cooltime = 7000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand3", rate = 6, loop = 1, globalcooltime = 1 },
   { action_name = "Stand_1", rate = 4, loop = 1, globalcooltime = 1 },
   { action_name = "Stand2", rate = 4, loop = 1, globalcooltime = 1 },
   { action_name = "Attack6_ThrowSnowBall", rate = 10, loop = 1, cooltime = 7000 },
}