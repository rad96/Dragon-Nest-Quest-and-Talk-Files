--DarkLairDefence_Lv27_GargoyleGray.lua
--타임아웃 모드, 석상화는 하지 않는다
--대쉬공격과 기본공격만 한다
--타임아웃 되면 돌 소환도 사용

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 6, loop = 2  },
   { action_name = "Walk_Right", rate = 6, loop = 2  },
   { action_name = "Attack3_Combo_N", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Back", rate = 4, loop = 2  },
   { action_name = "Move_Back", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_DashAttack", rate = 11, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
}