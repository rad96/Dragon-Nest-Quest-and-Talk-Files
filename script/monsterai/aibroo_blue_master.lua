--AiBroo_Blue_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = 7, loop = 1  },
   { action_name = "Move_Right", rate = 7, loop = 1  },
   { action_name = "Move_Back", rate = 15, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 1  },
   { action_name = "Move_Right", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
   { action_name = "Attack3_IceSword", rate = 20, loop = 2  },
   { action_name = "Attack3_IceSword", rate = -1, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 3  },
   { action_name = "Walk_Right", rate = 10, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 10, loop = 1  },
   { action_name = "Move_Right", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
   { action_name = "Attack3_IceSword", rate = 20, loop = 1  },
   { action_name = "Attack3_IceSword", rate = -1, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Left", rate = 15, loop = 2  },
   { action_name = "Walk_Right", rate = 15, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 3, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20153,  cooltime = 12000, rate = 80,rangemin = 0, rangemax = 400, target = 3, selfhppercent = 100 },
   { skill_index = 20154,  cooltime = 10000, rate = 80, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 100 },
   { skill_index = 20154,  cooltime = 15000, rate = -1, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 100 },
   { skill_index = 20155,  cooltime = 17000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 50 },
}
