
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 30000
g_Lua_GlobalCoolTime2 = 10000


g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Attack1_Chopping", rate = 5, loop = 1  },
   { action_name = "Attack2_Slash", rate = 5, loop = 1  },
   { action_name = "Attack3_JumpChopping", rate = 2, loop = 1, randomtarget = 1.1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 4, loop = 2  },
   { action_name = "Attack1_Chopping", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 6, loop = 1  },
   { action_name = "Attack3_JumpChopping", rate = 10, loop = 1 },
   { action_name = "Assault", rate = 4, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 6, loop = 2  },
   { action_name = "Walk_Right", rate = 6, loop = 2  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 8, loop = 3  },
   { action_name = "Assault", rate = 4, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Chopping", rate = 5, loop = 1, approach = 600.0 },
   { action_name = "Move_Left", rate = 6, loop = 3  },
   { action_name = "Move_Right", rate = 6, loop = 3  },
}
g_Lua_Skill = { 
--�罽���� ���� ���� ���� ���
   { skill_index = 30905, next_lua_skill_index = 1, cooltime = 60000, rate = 50, rangemin = 0, rangemax = 3200, target = 3, selfhppercent = 75, multipletarget = 1, globalcooltime = 2 },
   { skill_index = 30906, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1200, target = 3 },
--������� (1������~)
   { skill_index = 30904, cooltime = 23000, rate = 60, rangemin = 300, rangemax = 1200, target = 3, next_lua_skill_index = 3, randomtarget = 1.1 },
   { skill_index = 30913, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3 },
--���� ����(�ٴ� �����)
   { skill_index = 30908, cooltime = 53000, rate = 60, rangemin = 100, rangemax = 1000, target = 3, multipletarget = "1,3" },
--����� ��ǳ(���� ��ų)
   { skill_index = 30912, cooltime = 75000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, globalcooltime = 2, selfhppercent = 50 },
--����� ��ǳ
   { skill_index = 30907, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3 },
--������
   { skill_index = 30923, cooltime = 53000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, randomtarget = 1.0, selfhppercent = 50 },
--����ȭ, 1�ܰ� �߻�--
   { skill_index = 30914, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 1, encountertime = 30000, globalcooltime = 1, limitcount = 1 },
   { skill_index = 30902, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 1, usedskill = "30914", notusedskill = "30902", globalcooltime = 1 },
--����ȭ, 2�ܰ�--
   { skill_index = 30901, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 1, usedskill = "30909", notusedskill = "30901", globalcooltime = 1 },
   { skill_index = 30902, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 1, usedskill = "30901", notusedskill = "30902", globalcooltime = 1 },
   { skill_index = 30903, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 1, usedskill = "30902", notusedskill = "30903", globalcooltime = 1 },
}