--AiCyclops_Green_Boss_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 30000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {

  CustomAction1 = {
      { "Stand_5" },
      { "Attack001_Bash" },
  },
  CustomAction2 = {
      { "Stand_5" },
      { "Attack002_BackNFrontChopping" },
  },
  CustomAction3 = {
      { "Stand_2" },
      { "Attack002_BackNFrontChopping" },
  },
  CustomAction4 = {
      { "Attack003_JumpAttack" },
      { "Attack004_TurningSlash" },
  },
  CustomAction5 = {
      { "Attack003_JumpAttack", 1 },
  },
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand_5",			rate = 5,	loop = 1, td = "FL,FR,RB,BR,BL,LB", 	selfhppercentrange = "75,100" },
	{ action_name = "Walk_Left",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Walk_Right",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Move_Back",			rate = 4,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Attack001_Bash",		rate = 14,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Attack002_BackNFrontChopping",	rate = 14,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Stand_2",			rate = 5,	loop = 1, td = "FL,FR,RB,BR,BL,LB",	selfhppercentrange = "50,75" },
	{ action_name = "Walk_Left",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Walk_Right",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Move_Back",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Attack001_Bash",		rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Attack002_BackNFrontChopping",	rate = 14,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "CustomAction3",		rate = 14,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "CustomAction4",		rate = 14,	loop = 1, td = "FL,FR", 		selfhppercentrange = "50,75" },
	{ action_name = "Stand_1",			rate = 5,	loop = 1, td = "FL,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Walk_Left",			rate = 8,	loop = 2, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Walk_Right",			rate = 8,	loop = 2, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Move_Back",			rate = 8,	loop = 2, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Attack001_Bash",		rate = 5,	loop = 1, td = "FL,FR", 		selfhppercentrange = "25,50" },
	{ action_name = "Attack002_BackNFrontChopping",	rate = 21,	loop = 1, td = "FL,FR", 		selfhppercentrange = "25,50" },
	{ action_name = "CustomAction4",		rate = 42,	loop = 1, td = "FL,FR", 		selfhppercentrange = "25,50" },
	{ action_name = "Attack012_SummonRelic_Buff2",	rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50", cooltime = 25000, randomtarget=1.1 },
	{ action_name = "Stand_3",			rate = 3,	loop = 1, td = "FL,RF",			selfhppercent = 25 },
	{ action_name = "Walk_Left",			rate = 5,	loop = 1, td = "FL,FR,LF,RF",		selfhppercent = 25 },
	{ action_name = "Walk_Right",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
	{ action_name = "Move_Back",			rate = 10,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
	{ action_name = "Attack002_BackNFrontChopping",	rate = 14,	loop = 1, td = "FL,FR", 		selfhppercent = 25 },
	{ action_name = "CustomAction4",		rate = 21,	loop = 1, td = "FL,FR", 		selfhppercent = 25 },
	{ action_name = "Attack012_SummonRelic_Buff2",	rate = 42,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25, cooltime = 25000, randomtarget=1.1 },
}
g_Lua_Near2 = { 
	{ action_name = "Stand_5",			rate = 5,	loop = 1, td = "FL,FR,RB,BR,BL,LB", 	selfhppercentrange = "75,100" },
	{ action_name = "Walk_Left",			rate = 2,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Walk_Right",			rate = 2,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Move_Back",			rate = 4,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Move_Front",			rate = 10,	loop = 2, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Stand_2",			rate = 10,	loop = 1, td = "FL,FR,RB,BR,BL,LB",	selfhppercentrange = "50,75" },
	{ action_name = "Walk_Left",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Walk_Right",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Move_Back",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Move_Front",			rate = 15,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Attack008_SummonRelic_Charger",rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75", cooltime = 12000, randomtarget=1.1 },
	{ action_name = "CustomAction4",		rate = 17,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Attack009_SummonRelic_Buff",	rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75", cooltime = 15000, randomtarget=1.1 },
	{ action_name = "Stand_1",			rate = 5,	loop = 1, td = "FL,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Walk_Left",			rate = 13,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Walk_Right",			rate = 13,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Move_Back",			rate = 10,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Move_Front",			rate = 15,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "CustomAction4",		rate = 42,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Attack012_SummonRelic_Buff2",	rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50", cooltime = 25000, randomtarget=1.1 },
	{ action_name = "Stand_3",			rate = 3,	loop = 1, td = "FL,RF",			selfhppercent = 25 },
	{ action_name = "Walk_Left",			rate = 5,	loop = 1, td = "FL,FR,LF,RF",		selfhppercent = 25 },
	{ action_name = "Walk_Right",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
	{ action_name = "Move_Back",			rate = 15,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
	{ action_name = "CustomAction4",		rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
	{ action_name = "Attack012_SummonRelic_Buff2",	rate = 42,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25, cooltime = 25000, randomtarget=1.1 },
}
g_Lua_Near3 = { 
	{ action_name = "Stand_5",			rate = 2,	loop = 1, td = "FL,FR,RB,BR,BL,LB", 	selfhppercentrange = "75,100" },
	{ action_name = "Move_Front",			rate = 10,	loop = 3, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Stand_2",			rate = 5,	loop = 1, td = "FL,FR,RB,BR,BL,LB",	selfhppercentrange = "50,75" },
	{ action_name = "Walk_Left",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Walk_Right",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Move_Front",			rate = 30,	loop = 3, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Attack009_SummonRelic_Buff",	rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75", cooltime = 5000, randomtarget=1.1 },
	{ action_name = "Attack008_SummonRelic_Charger",rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75", cooltime = 5000, randomtarget=1.1 },
	{ action_name = "Stand_1",			rate = 5,	loop = 1, td = "FL,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Walk_Left",			rate = 10,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Walk_Right",			rate = 10,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Move_Back",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Move_Front",			rate = 10,	loop = 2, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Attack012_SummonRelic_Buff2",	rate = 42,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50", cooltime = 5000, randomtarget=1.1 },
	{ action_name = "Stand_3",			rate = 3,	loop = 1, td = "FL,RF",			selfhppercent = 25 },
	{ action_name = "Walk_Left",			rate = 5,	loop = 1, td = "FL,FR,LF,RF",		selfhppercent = 25 },
	{ action_name = "Walk_Right",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
	{ action_name = "Move_Front",			rate = 15,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
	{ action_name = "Attack012_SummonRelic_Buff2",	rate = 42,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25, cooltime = 5000, randomtarget=1.1 },
}
g_Lua_Near4 = { 
	{ action_name = "Stand_5",			rate = 5,	loop = 1, td = "FL,FR,RB,BR,BL,LB", 	selfhppercentrange = "75,100" },
	{ action_name = "Move_Front",			rate = 20,	loop = 3, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Stand_2",			rate = 5,	loop = 1, td = "FL,FR,RB,BR,BL,LB",	selfhppercentrange = "50,75" },
	{ action_name = "Walk_Left",			rate = 5,	loop = 2, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Walk_Right",			rate = 5,	loop = 2, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Move_Front",			rate = 20,	loop = 3, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Attack009_SummonRelic_Buff",	rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75", cooltime = 5000, randomtarget=1.1 },
	{ action_name = "Attack008_SummonRelic_Charger",rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75", cooltime = 5000, randomtarget=1.1 },
	{ action_name = "Stand_1",			rate = 5,	loop = 1, td = "FL,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Walk_Left",			rate = 5,	loop = 2, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Walk_Right",			rate = 5,	loop = 2, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Move_Front",			rate = 15,	loop = 2, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Attack012_SummonRelic_Buff2",	rate = 35,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50", cooltime = 5000, randomtarget=1.1 },
	{ action_name = "Stand_3",			rate = 3,	loop = 1, td = "FL,RF",			selfhppercent = 25 },
	{ action_name = "Walk_Left",			rate = 5,	loop = 1, td = "FL,FR,LF,RF",		selfhppercent = 25 },
	{ action_name = "Walk_Right",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
	{ action_name = "Move_Front",			rate = 15,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
	{ action_name = "Attack012_SummonRelic_Buff2",	rate = 35,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25, cooltime = 5000, randomtarget=1.1 },
}
g_Lua_Near5 = { 
	{ action_name = "Move_Front",			rate = 20,	loop = 3, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Assault",			rate = 24,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "75,100" },
	{ action_name = "Move_Front",			rate = 15,	loop = 3, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Assault",			rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "50,75" },
	{ action_name = "Stand_1",			rate = 5,	loop = 1, td = "FL,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Assault",			rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercentrange = "25,50" },
	{ action_name = "Stand_3",			rate = 3,	loop = 1, td = "FL,RF",			selfhppercent = 25 },
	{ action_name = "Walk_Left",			rate = 5,	loop = 1, td = "FL,FR,LF,RF",		selfhppercent = 25 },
	{ action_name = "Walk_Right",			rate = 5,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
	{ action_name = "Move_Back",			rate = 15,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
	{ action_name = "Assault",			rate = 21,	loop = 1, td = "FL,FR,LF,RF", 		selfhppercent = 25 },
}

g_Lua_Assault = { 
	{ action_name = "CustomAction4",		rate = 10,	loop = 1, td = "FL,FR", 		approach = 700 },
	{ action_name = "Attack001_Bash",		rate = 10,	loop = 1, td = "FL,FR,LF,RF", 		approach = 500 },
	{ action_name = "Attack002_BackNFrontChopping",	rate = 10,	loop = 1, td = "FL,FR,LF,RF", 		approach = 500 },
}

g_Lua_NonDownRangeDamage = { 
   { action_name = "assualt", rate = 10, loop = 1, selfhppercentrange = "50,100"  },
   { action_name = "Move_Left", rate = 5, loop = 3  },
   { action_name = "Move_Right", rate = 5, loop = 3  },
   { lua_skill_index = 18, rate = 30, loop =  1, selfhppercent = 50 },
   { lua_skill_index = 19, rate = 30, loop =  1, selfhppercent = 50 },
   { action_name = "Attack009_SummonRelic_Buff", rate = 30, loop = 1, td = "FL,FR,LF,RF", selfhppercentrange = "50,75", cooltime = 15000, randomtarget=1.1 },
   { action_name = "Attack008_SummonRelic_Charger", rate = 30, loop = 1, td = "FL,FR,LF,RF", selfhppercentrange = "50,75", cooltime = 15000, randomtarget=1.1 },
}

g_Lua_BeHitSkill = { 
   { lua_skill_index = 12, rate = 100, skill_index = 30315  },
}

g_Lua_Skill = { 
   { skill_index = 30310,  cooltime = 30000, rate = 80, rangemin = 0, 	rangemax = 600,  target = 3, td = "FL,FR,RB,BR,BL,LB",	selfhppercentrange = "75,100" },
   { skill_index = 30312,  cooltime = 45000, rate = 60, rangemin = 0, 	rangemax = 1500, target = 3, td = "FL,FR,RB,BR",	selfhppercentrange = "75,100" },
   { skill_index = 30311,  cooltime = 22000, rate = 80, rangemin = 000, rangemax = 1000, target = 3, td = "FL,FR",		selfhppercentrange = "50,75" },
   { skill_index = 30312,  cooltime = 45000, rate = 60, rangemin = 0, 	rangemax = 1500, target = 3, td = "FL,FR,RB,BR",	selfhppercentrange = "50,75" },
   { skill_index = 30310,  cooltime = 60000, rate = 40, rangemin = 0, 	rangemax = 600,  target = 3, td = "FL,FR,RB,BR,BL,LB",	selfhppercentrange = "50,75" },
   { skill_index = 30314,  cooltime = 60000, rate = 90, rangemin = 0, 	rangemax = 1500, target = 3, td = "FL,FR,RB,BR,BL,LB",	selfhppercentrange = "25,50" },
   { skill_index = 30311,  cooltime = 30000, rate = 80, rangemin = 000, rangemax = 1000, target = 3, td = "FL,FR",		selfhppercentrange = "25,50", randomtarget=1.1 },
   { skill_index = 30310,  cooltime = 75000, rate = 50, rangemin = 0, 	rangemax = 600,  target = 3, td = "FL,FR,RB,BR,BL,LB",	selfhppercentrange = "25,50" },
   { skill_index = 30312,  cooltime = 60000, rate = 60, rangemin = 0, 	rangemax = 1500, target = 3, td = "FL,FR,RB,BR",	selfhppercentrange = "25,50", randomtarget=1.1 },
   { skill_index = 30315,  cooltime = 19000, rate = 100, rangemin = 0, 	rangemax = 800,  target = 3, td = "FL,FR,RB,BR",	selfhppercent = 25, randomtarget=1.1  },
   { skill_index = 30311,  cooltime = 75000, rate = 60, rangemin = 000, rangemax = 1000, target = 3, td = "FL,FR",		selfhppercent = 25  },
   { skill_index = 30314,  cooltime = 60000, rate = 65, rangemin = 0, 	rangemax = 1500, target = 3, td = "FL,FR,RB,BR,BL,LB",	selfhppercent = 25 },
   { skill_index = 30316,  cooltime = 1000, rate = -1, rangemin = 0, 	rangemax = 800,  target = 3, td = "FL,FR,RB,BR",	selfhppercent = 50 },
   { skill_index = 30311,  cooltime = 1000, rate = -1, rangemin = 000,  rangemax = 1000, target = 3, td = "FL,FR",		selfhppercent = 100 },
   { skill_index = 30312,  cooltime = 1000, rate = -1, rangemin = 0,	rangemax = 1500, target = 3, td = "FL,FR,RB,BR",	selfhppercent = 100 },
}
