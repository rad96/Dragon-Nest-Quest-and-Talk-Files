--DarkLairDefence_Lv26_WraithRedMIni.lua
--커맨더 모드의 부하 몬스터, 근접을 많이 하고 근접 공격을 많이 하게
--어려운 공격은 제외, 이동도 너무 자주 하지 않도록

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Attack1_Cut", rate = 6, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 7, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 2  },
   { action_name = "Move_Right", rate = 2, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 4, loop = 2 },
   { action_name = "Walk_Back", rate = 1, loop = 2 },
   { action_name = "Walk_Front", rate = 15, loop = 2 },
   { action_name = "Move_Front", rate = 15, loop = 2 },
   { action_name = "Walk_Left", rate = 4, loop = 2 },
   { action_name = "Walk_Right", rate = 4, loop = 2 },
   { action_name = "Move_Left", rate = 1, loop = 2 },
   { action_name = "Move_Right", rate = 1, loop = 2 },
}