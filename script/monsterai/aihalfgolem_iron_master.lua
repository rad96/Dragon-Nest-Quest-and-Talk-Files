--HalfGolem_Iron_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 20, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 15, loop = 1  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Attack11_ChargeBeam", rate = -1, loop = 1  },
   { action_name = "Attack17_ChargeBeam_Spin", rate = -1, loop = 1  },
   { action_name = "Attack2_Blow", rate = 22, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 20, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 8, loop = 1  },
   { action_name = "Walk_Back", rate = 18, loop = 1  },
   { action_name = "Move_Left", rate = -1, loop = 1  },
   { action_name = "Move_Right", rate = -1, loop = 1  },
   { action_name = "Move_Front", rate = -1, loop = 1  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Attack11_ChargeBeam", rate = 9, loop = 1, cooltime = 15000, encountertime = 10000 },
   { action_name = "Attack17_ChargeBeam_Spin", rate = -1, loop = 1, cooltime = 15000, encountertime = 10000 },
   { action_name = "Attack12_EyeBeam", rate = 18, loop = 1, cooltime = 7000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 20, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = -1, loop = 1  },
   { action_name = "Move_Right", rate = -1, loop = 1  },
   { action_name = "Move_Front", rate = -1, loop = 1  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Attack11_ChargeBeam", rate = 9, loop = 1, cooltime = 15000, encountertime = 10000 },
   { action_name = "Attack17_ChargeBeam_Spin", rate = 9, loop = 1, cooltime = 15000, encountertime = 10000 },
   { action_name = "Attack12_EyeBeam", rate = 18, loop = 1, cooltime = 7000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = -1, loop = 1  },
   { action_name = "Move_Right", rate = -1, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 1  },
   { action_name = "Attack11_ChargeBeam", rate = 22, loop = 1, cooltime = 15000, encountertime = 10000 },
   { action_name = "Attack17_ChargeBeam_Spin", rate = 22, loop = 1, cooltime = 15000, encountertime = 10000 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = -1, loop = 1  },
   { action_name = "Move_Right", rate = -1, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Attack11_ChargeBeam", rate = -1, loop = 1, cooltime = 15000, encountertime = 10000 },
   { action_name = "Attack17_ChargeBeam_Spin", rate = -1, loop = 1, cooltime = 15000, encountertime = 10000 },
}
g_Lua_Skill = { 
   { skill_index = 20753,  cooltime = 30000, rate = 100,rangemin = 0, rangemax = 3000, target = 3, encountertime = 30000 },
   { skill_index = 20754,  cooltime = 30000, rate = -1, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 80 },
   { skill_index = 20752,  cooltime = 30000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 75 },
   { skill_index = 20751,  cooltime = 30000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 50 },
}
