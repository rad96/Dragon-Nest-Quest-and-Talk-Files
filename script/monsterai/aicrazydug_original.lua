-- Goblin Blue Elite Normal AI

g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;


g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Walk_Back", 2 },
      { "Look_Back", 0 },
      { "Walk_Back", 0 },
      { "Turn_Left", 0 },

  },
  CustomAction2 = {
      { "Walk_Left", 2 },
      { "Turn_Right", 0 },
  },
  CustomAction3 = {
      { "Walk_Right", 2 },
      { "Turn_Left", 0 },
  },
  CustomAction4 = {
      { "Walk_Back", 2 },
      { "Turn_Right", 0 },
  },

  CustomAction5 = {
      { "Move_Back", 3 },
      { "Look_Back", 0 },
      { "Move_Back", 0 },
      { "Turn_Left", 0 },

  },
  CustomAction6 = {
      { "Move_Left", 3 },
      { "Turn_Right", 0 },
  },
  CustomAction7 = {
      { "Move_Right", 3 },
      { "Turn_Left", 0 },
  },
  CustomAction8 = {
      { "Move_Back", 3 },
      { "Turn_Right", 0 },
  },
  CustomAction9 = {
      { "Attack1_Bite", 0 },
      { "Move_Back", 4 },
      { "Look_Back", 0 },
      { "Turn_Left", 0 },
  },



}


g_Lua_Near1 = 
{ 
	{ action_name = "CustomAction1",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction4",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction5",	rate = 30,		loop = 1 },
	{ action_name = "CustomAction8",	rate = 30,		loop = 1 },
	{ action_name = "CustomAction9",	rate = 40,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "CustomAction3",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction1",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction4",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction7",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction6",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction5",	rate = 40,		loop = 1 },
	{ action_name = "CustomAction8",	rate = 40,		loop = 1 },
	{ action_name = "CustomAction9",	rate = 40,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction3",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction1",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction4",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction7",	rate = 8,		loop = 1 },
	{ action_name = "CustomAction6",	rate = 8,		loop = 1 },
	{ action_name = "CustomAction5",	rate = 40,		loop = 1 },
	{ action_name = "CustomAction8",	rate = 40,		loop = 1 },
   --     { action_name = "Attack3_Saliva",	rate = 13,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "CustomAction3",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
        { action_name = "CustomAction1",	rate = 10,		loop = 1 },
        { action_name = "CustomAction4",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction5",	rate = 30,		loop = 1 },
	{ action_name = "CustomAction8",	rate = 30,		loop = 1 },
  --    { action_name = "Attack3_Saliva",	rate = 13,		loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "CustomAction2",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction3",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Moe_Front",	rate = 15,		loop = 2 },
	{ action_name = "CustomAction5",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction8",	rate = 20,		loop = 1 },
    --  { action_name = "Attack3_Saliva",	rate = 13,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "CustomAction3",	rate = 3,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Moe_Front",	rate = 15,		loop = 3 },
}


