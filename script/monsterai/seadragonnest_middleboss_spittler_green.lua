--SeaDragonNest_MiddleBoss_Spittler_Green.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 1500;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_On", rate = 35, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Stand_1", rate = 1, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Attack2_Bite_SeaDragon", rate = 25, loop = 1, cooltime = 7000, custom_state1 = "custom_ground" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_On", rate = 35, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Stand_1", rate = 10, loop = 1, custom_state1 = "custom_ground", },
}
g_Lua_Near3 = { 
   { action_name = "Stand_On", rate = 35, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Stand_1", rate = 10, loop = 1, custom_state1 = "custom_ground", },
}
g_Lua_Skill = { 
--��׷� �ʱ�ȭ
   { skill_index = 30548, cooltime = 12000, rate = 50, rangemin = 0, rangemax = 3500, target = 3, custom_state1 = "custom_ground" },
--ħ
   { skill_index = 30549, cooltime = 9000, rate = 80, rangemin = 400, rangemax = 1500, target = 3, custom_state1 = "custom_ground" },
--����
   { skill_index = 30554, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 4500, target = 3, custom_state1 = "custom_ground", announce = "100,7000" },
}
