--AiOrc_Green_Easy.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 450;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Attack3_Air_T", rate = 100, loop = 1, cooltime = 3000,  },
}
g_Lua_Near2 = { 
   
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
g_Lua_Near3 = { 
  
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
g_Lua_Near4 = { 
   
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
g_Lua_Near5 = { 
     
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
g_Lua_MeleeDefense = { 
   
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
g_Lua_RangeDefense = { 
   
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
g_Lua_Assault = { 
   
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
