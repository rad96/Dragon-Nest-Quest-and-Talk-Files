
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
 State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_CustomAction = {
-- ��������������
  CustomAction2 = {
     { "Tumble_Left" },
     { "Shoot_Stand" },
  },
-- ��������������
  CustomAction3 = {
     { "Tumble_Right" },
     { "Shoot_Stand" },
  },
  CustomAction3 = {
     { "Shoot_Stand" },
     { "Shoot_Stand" },
  },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 13000
g_Lua_GlobalCoolTime3 = 23000

g_Lua_Near1 = 
{
     { action_name = "Stand", rate = 3, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 10, loop = 2 },
     { action_name = "Attack_Down", rate = 30, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Shoot_Stand", rate = 7, loop = 1, globalcooltime = 1 },
     { action_name = "Attack_SideKick_Book", rate = 12, loop = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 3, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 15, loop = 1 },
     { action_name = "Shoot_Stand", rate = 15, loop = 1, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 3 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 3 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 3 },
     { action_name = "Shoot_Stand", rate = 15, loop = 1, globalcooltime = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 2 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 4 },
}
g_Lua_Skill = {
-- �۷��̼ȿ��̺�
   { skill_index = 31076, cooltime = 50000, rate = 50, rangemin = 0, rangemax = 400, target = 3,  },
-- ĥ���̽�Ʈ 45��
   { skill_index = 31074, cooltime = 60000, rate = 10, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 30, limitcount = 1, td = "FL,FR,LF,RF",  }, -- ��Ÿ������, Ȯ������, HP�ۼ�Ʈ, Ƚ������
-- ����¡�ҵ� 15��
   { skill_index = 31073, cooltime = 30000, rate = 40, rangemin = 0, rangemax = 500,target = 3 }, --����¡�ҵ� ��Ž 30���� ����
-- �۷��̼Ƚ�����ũ 6.5�� (500+500)
   { skill_index = 31062, cooltime = 8000, rate = 80, rangemin = 500, rangemax = 1000, target = 3,},
   { skill_index = 33331, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1 }
}