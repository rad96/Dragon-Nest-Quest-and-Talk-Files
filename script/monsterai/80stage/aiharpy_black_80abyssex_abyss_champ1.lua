--AiHarpy_Black_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 25, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 20, loop = 3  },
   { action_name = "Attack1_Throw_B", rate = 2, loop = 1  },
   { action_name = "Attack1_Throw_B_T", rate = 7, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 11, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 15, loop = 3  },
   { action_name = "Walk_Right", rate = 15, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 3  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 3  },
   { action_name = "Attack1_Throw_B", rate = 5, loop = 1  },
   { action_name = "Attack1_Throw_B_T", rate = 11, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 15, loop = 3  },
   { action_name = "Walk_Right", rate = 15, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 7, loop = 2  },
   { action_name = "Move_Right", rate = 7, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
   { action_name = "Attack1_Throw_B", rate = 7, loop = 3  },
   { action_name = "Attack1_Throw_B_T", rate = 19, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Walk_Back", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 3  },
   { action_name = "Walk_Right", rate = 10, loop = 3  },
   { action_name = "Attack1_Throw_B_T", rate =11, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20215,  cooltime = 60000, rate = 80,rangemin = 0, rangemax = 1000, target = 3 },-- 수면
   { skill_index = 20214,  cooltime = 60000, rate = 80, rangemin = 0, rangemax = 500, target = 3 },-- 크리티컬업
}
