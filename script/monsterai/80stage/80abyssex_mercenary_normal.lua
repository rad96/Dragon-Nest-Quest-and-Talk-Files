
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
 State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_CustomAction = {
  CustomAction6 = {
     { "Attack1_Axe" },
     { "Attack2_Axe" },
     { "Attack3_Axe" },
     { "Attack4_Axe" },
  },
}

g_Lua_Near1 = 
{ 
     { action_name = "Stand_1", rate = 2, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 3, loop = 1 },
     { action_name = "Attack1_Axe", rate = 10, loop = 1,},
	 { action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 5000,},
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand_1", rate = 8, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Back", rate = 10, loop = 1 },
     { action_name = "Move_Left", rate = 20, loop = 1 },
     { action_name = "Move_Right", rate = 20, loop = 1 },
     { action_name = "Tumble_Front", rate = 10, loop = 1,},
     { action_name = "Tumble_Left", rate = 2, loop = 1, },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand_1", rate = 5, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 4 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Assault",  rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand_1", rate = 8, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 6 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
     { action_name = "Assault",  rate = 10,  loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Skill_DashUpper", rate = 20, loop = 1, approach = 300   },
}
g_Lua_Skill = {
   { skill_index = 32449, cooltime = 30000, rate = 100, rangemin = 0, rangemax = 3000,target = 3, next_lua_skill_index=1,priority=1,encountertime=6000},-- 휠윈드
   { skill_index = 32452, cooltime = 100, rate = -1, rangemin = 0, rangemax = 3000,target = 1},-- 어그로 초기화
   { skill_index = 31026, cooltime = 28000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, },-- 데몰리션 피스트
   { skill_index = 31004, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 300, target = 3, td = "FL,FR", },-- 서클브레이크
}