--80AbyssEX_Ogre_Gsell_Boss.lua
-- /genmon 703202

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 16, loop = 1  },
   { action_name = "Attack1_bash", rate = 8, loop = 1  },
   { action_name = "Attack3_Upper", rate = 7, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_bash", rate = 7, loop = 1  },
   { action_name = "Attack3_Upper", rate = 15, loop = 1  },
   { action_name = "Attack2_Berserk", rate = 25, loop = 1, selfhppercent = 50, cooltime = 50000},
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
   { action_name = "Attack13_Dash", rate = 15, loop = 1, cooltime = 30000 },
   { action_name = "Assault", rate = 7, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Move_Front", rate = 50, loop = 1  },
   { action_name = "Attack7_JumpAttack", rate = 15, loop = 1  },
   { action_name = "Attack13_Dash", rate = 15, loop = 1, cooltime = 40000 },
   { action_name = "Assault", rate = 7, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 1  },
   { action_name = "Attack13_Dash", rate = 20, loop = 1, cooltime = 40000 },
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Front", rate = 30, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Assault", rate = 15, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 3, loop = 1, approach = 250.0  },
   { action_name = "Attack3_Upper", rate = 5, loop = 1, approach = 300.0  },
}

g_Lua_GlobalCoolTime1 = 15000

g_Lua_Skill = { 
   { skill_index = 20130,  cooltime = 50000, rate = 30, rangemin = 0, rangemax = 400, target = 3, td = "FL,FR"}, -- Attack8_Spew 독뱉기
   { skill_index = 20131,  cooltime = 40000, rate = 70, rangemin = 0, rangemax = 1500, target = 3, randometarget = "1.5,0,1" }, -- Attack12_BigFireBall  화상
   { skill_index = 20132,  cooltime = 50000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, td = "FL,FR" }, -- Attack11_FireWave 화상
   { skill_index = 20133,  cooltime = 80000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 80, }, -- Attack10_BerserkHowl  공방버프
   --신규스킬
   { skill_index = 36429,  cooltime = 60000, rate = 80, rangemin = 0, rangemax = 800, target = 3, }, -- Attack9_Punch 바닥 여러 번 치는 범위공격 이속저하  디버프  
   { skill_index = 36421,  cooltime = 50000, rate = 80, rangemin = 0, rangemax = 700, target = 3, selfhppercent = 100}, --Attack20_SpearCombo_80AbyssEX 스피어콤보
   { skill_index = 36422,  cooltime = 70000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 75, limitcount = 1}, --Attack21_SpearStance_80AbyssEX 스피어 스탠스 원래 75
   { skill_index = 36422,  cooltime = 70000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 50, imitcount = 1}, --Attack21_SpearStance_80AbyssEX 스피어 스탠스 원래 
   { skill_index = 36422,  cooltime = 70000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 25, imitcount = 1}, --Attack21_SpearStance_80AbyssEX 스피어 스탠스 원래 75
   { skill_index = 36422,  cooltime = 70000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 24, }, --Attack21_SpearStance_80AbyssEX 스피어 스탠스 원래 75
   { skill_index = 36423,  cooltime = 140000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 50, multipletarget = "2,4,0,1", }, --Attack19_HeavensJudgment_80AbyssEX_Start 프로텍트 버프원래50
   --테스트용
   -- { skill_index = 36423,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 800, target = 3, },
}
