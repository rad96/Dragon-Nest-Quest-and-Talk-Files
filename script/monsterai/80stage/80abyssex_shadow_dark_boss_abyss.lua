--80AbyssEX_Shadow_Dark_Boss 
-- /genmon 703182

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 350;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_Claw", rate = 30, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 11, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Walk_Left", rate = 15, loop = 2  },
   { action_name = "Walk_Right", rate = 15, loop = 2  },
   { action_name = "Walk_Back", rate = 26, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 13, loop = 1  },
   { action_name = "Attack_Stand_Clone", rate = 80, loop = 1  },
   { action_name = "Attack1_Claw", rate = 25, loop = 1  },
   { action_name = "Attack2_Curve", rate = 30, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 20, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack4_Pierce", rate = 30, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 13, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = -1, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = -1, loop = 2  },
}
g_Lua_NearNonDownMeleeDamage = { 
   { action_name = "Attack1_Claw", rate = 27, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 20, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack4_Pierce", rate = 30, loop = 1  },
   { action_name = "Move_Left", rate = 15, loop = 2  },
   { action_name = "Move_Right", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
}

g_Lua_GlobalCoolTime1 = 15000

g_Lua_Skill = { 
   { skill_index = 20240,  cooltime = 30000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 100 }, --Attack3_Breath
   { skill_index = 20241,  cooltime = 50000, rate = 100, rangemin = 0, rangemax = 350, target = 3, selfhppercent = 100 }, --Attack5_Twist_Start
   { skill_index = 36424,  cooltime = 80000, rate = 100, rangemin = 50, rangemax = 1100, target = 3, selfhppercent = 80, globalcooltime = 1}, --Attack8_BoltNanmu_80AbyssEX_Start 원래 80퍼에 시작
   { skill_index = 36426,  cooltime = 30000, rate = 100, rangemin = 50, rangemax = 2000, target = 3, }, --Attack9_BoltBoom  20243 > 36426 
   { skill_index = 36425,  cooltime = 40000, rate = 80, rangemin = 0, rangemax = 450, target = 3, }, --Attack4_BoltWall_80AbyssEX
    --신규스킬
   { skill_index = 36418,  cooltime = 65000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, randometarget = "1.5,0,1", }, -- 점프 공격
   { skill_index = 36419,  cooltime = 45000, rate = 80, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 80, }, -- 바닥쓸기
   { skill_index = 36420,  cooltime = 1000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 75, limitcount = 1 }, -- 바닥쓸기 EX
   { skill_index = 36420,  cooltime = 1000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 50, limitcount = 1 }, -- 바닥쓸기 EX
   { skill_index = 36420,  cooltime = 1000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 25, limitcount = 1 }, -- 바닥쓸기 EX
   { skill_index = 36427,  cooltime = 80000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "2,4,0,1", selfhppercent = 40, globalcooltime = 1}, --Attack8_Marking_80AbyssEX_Start 원래 50에 시작
   --테스트용
   -- { skill_index = 36427,  cooltime = 15000, rate = 100, rangemin = 50, rangemax = 1100, target = 3, priority = 100},
}
