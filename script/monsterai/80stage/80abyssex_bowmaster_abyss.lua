
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;


g_Lua_CustomAction = {
-- 3연타 공격
  CustomAction1 = {
      { "Shoot_Stand_BigBow" },
      { "Shoot_Stand_BigBow" },
      { "Shoot_Finish_BigBow" },
  },
}



g_Lua_Near1 = 
{
     { action_name = "Stand", rate = 2, loop = 1 },
     { action_name = "Move_Left", rate = 3, loop = 1 },
     { action_name = "Move_Right", rate = 3, loop = 1 },
     { action_name = "Move_Back", rate = 20, loop = 2 },
     { action_name = "Tumble_Back", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, cooltime = 7000 },
	 { action_name = "Attack_SideKick", rate = 10, loop = 1, cooltime = 7000 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 3, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 10, loop = 1 },
     { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, cooltime = 7000 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Front", rate = 5, loop = 1 },
	 { action_name = "Move_back", rate = 15, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "CustomAction1", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
     { action_name = "CustomAction4", rate = 8, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 4 },
}

g_Lua_NonDownMeleeDamage = {
      { action_name = "CustomAction4", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "CustomAction5", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "Move_Left", rate = 10, loop = 1 },
      { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_NonDownRangeDamage = {
      { action_name = "Stand", rate = 2, loop = 1 },
      { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, globalcooltime = 1 },
      { action_name = "CustomAction1", rate = 10, loop = 1, globalcooltime = 1 },
}

g_Lua_Skill = {
   -- { skill_index = 32444,  cooltime = 30000, rate = 70, rangemin = 500, rangemax = 1500, target = 3,encountertitme=15000,next_lua_skill_index=1},-- 애로우샤워 35000 쿨증가, 확률저하, 인카운트 타임증가
   { skill_index = 32452, cooltime = 100, rate = -1, rangemin = 0, rangemax = 3000,target = 1},-- 어그로 초기화
   { skill_index = 31048,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 3000, target = 2, selfhppercent = 100, priority=1 },-- 아울즈레이지
   { skill_index = 31034,  cooltime = 8000, rate = 100, rangemin = 0, rangemax = 350, target = 3, },-- 윌로우스핀킥 12000
   { skill_index = 31041,  cooltime = 7000, rate = 100, rangemin = 300, rangemax = 3000, target = 3, },-- 차지샷 15000 (600+0+300)
}