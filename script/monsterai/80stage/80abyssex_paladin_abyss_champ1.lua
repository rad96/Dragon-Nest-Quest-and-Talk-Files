
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_GlobalCoolTime1 = 17000
g_Lua_GlobalCoolTime2 = 17000
g_Lua_GlobalCoolTime3 = 33000

g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 2, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 3, loop = 1 },
--1타
     { action_name = "Attack1_Mace", rate = 5, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     { action_name = "CustomAction3", rate = 4, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     { action_name = "CustomAction4", rate = 3, loop = 1, cooltime = 13000, globalcooltime = 1 },
--4타
     { action_name = "CustomAction5", rate = 1, loop = 1, cooltime = 16000, globalcooltime = 1 },
}

g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Tumble_Front", rate = 5, cooltime = 20000, loop = 1, globalcooltime = 3 },
     { action_name = "Tumble_Left", rate = 1, cooltime = 20000, loop = 1, globalcooltime = 3 },
     { action_name = "Tumble_Right", rate = 1, cooltime = 20000, loop = 1, globalcooltime = 3 },
}

g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 4 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Assault",  rate = 3, loop = 1 },
}

g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 6 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
     { action_name = "Assault",  rate = 10,  loop = 1 },
}

g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
     { action_name = "Attack1_Mace", rate = 15, loop = 1, approach = 150 },
     { action_name = "Skill_ShieldBlow", rate = 5, loop = 1, approach = 150, globalcooltime = 2 },
}

g_Lua_Skill = { 
-- 스마이트 30초
   { skill_index = 31102,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 800, target = 3,  }, -- 쿨탐 5증가
-- 아머브레이크 8초
   { skill_index = 31103,  cooltime = 24000, rate = 100, rangemin = 0, rangemax = 500,target = 3, },
-- 블록 45초
   -- { skill_index = 31091,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 1000, target = 1,  },
-- 홀리크로스 20초
   -- { skill_index = 31101,  cooltime = 20000, rate = 100, rangemin = 400, rangemax = 800, target = 3, td = "FL,FR,LF,RF",  }, -- 홀리크로스 삭제
-- 디바인콤보 15초
   { skill_index = 31093,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 300,target = 3, },
}