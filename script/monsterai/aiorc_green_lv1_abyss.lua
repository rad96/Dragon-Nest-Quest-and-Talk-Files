--AiOrc_Green_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 450;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1200;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 21, loop = 1  },
   { action_name = "Move_Back", rate = 39, loop = 1  },
   { action_name = "Attack2_bash", rate = 12, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Walk_Front", rate = 51, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 36, loop = 2  },
   { action_name = "Attack1_Pushed", rate = -1, loop = 1  },
   { action_name = "Attack2_bash", rate = 13, loop = 1  },
   { action_name = "Attack7_Nanmu", rate = 9, loop = 1  },
   { action_name = "Attack6_HeavyAttack", rate = 80, loop = 1, target_condition = "State1" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Walk_Front", rate = 51, loop = 2  },
   { action_name = "Walk_Back", rate = 36, loop = 2  },
   { action_name = "Attack2_bash", rate = 1, loop = 1  },
   { action_name = "Attack6_HeavyAttack", rate = 4, loop = 1  },
   { action_name = "Attack7_Nanmu", rate = 4, loop = 1  },
   { action_name = "Attack6_HeavyAttack", rate = 69, loop = 1, target_condition = "State1" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 2  },
   { action_name = "Walk_Right", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 24, loop = 2  },
   { action_name = "Walk_Back", rate = 9, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 60, loop = 2  },
   { action_name = "Move_Back", rate = 21, loop = 2  },
   { action_name = "Assault", rate = 5, loop = 1  },
   { action_name = "Attack6_HeavyAttack", rate = 5, loop = 1  },
   { action_name = "Attack7_Nanmu", rate = 5, loop = 1  },
   { action_name = "Attack6_HeavyAttack", rate = 80, loop = 1, target_condition = "State1" },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Front", rate = 30, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 75, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
   { action_name = "Assault", rate = 24, loop = 2  },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 3, loop = 3  },
   { action_name = "Walk_Left", rate = 2, loop = 3  },
   { action_name = "Walk_Right", rate = 2, loop = 3  },
   { action_name = "Walk_Front", rate = 30, loop = 3  },
   { action_name = "Move_Left", rate = 6, loop = 3  },
   { action_name = "Move_Right", rate = 6, loop = 3  },
   { action_name = "Move_Front", rate = 90, loop = 3  },
}
g_Lua_MeleeDefense = { 
   { action_name = "Attack1_Pushed", rate = -1, loop = 1  },
   { action_name = "Move_Left", rate = 6, loop = 2  },
   { action_name = "Move_Right", rate = 6, loop = 2  },
}
g_Lua_RangeDefense = { 
   { action_name = "Assault", rate = 47, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 47, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_bash", rate = 30, loop = 2, cancellook = 0, approach = 250.0  },
   { action_name = "Attack7_Nanmu", rate = 15, loop = 2, cancellook = 0, approach = 250.0  },
}
