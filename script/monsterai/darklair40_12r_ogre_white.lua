--AiOgre_White_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 1  },
   { action_name = "Attack1_bash", rate = 3, loop = 1  },
   { action_name = "Attack6_HornAttack", rate = 3, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_bash", rate = 3, loop = 1  },
   { action_name = "Attack6_HornAttack", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 2, loop = 2  },
   { action_name = "Move_Front_Mini", rate = 2, loop = 1  },
   { action_name = "Attack6_HornAttack_Mini", rate = 4, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 2  },
   { action_name = "Move_Front_Mini", rate = 4, loop = 1  },
   { action_name = "Attack7_JumpAttack", rate = 1, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
   { action_name = "Move_Front_Mini", rate = 8, loop = 2 },
}