--DragonSycophantBishop_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

-- g_Lua_GlobalCoolTime1 = 3000 

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
    { action_name = "Walk_Front", rate = 7, loop = 1  },
   -- { action_name = "Walk_Back", rate = 15, loop = 1  },
   -- { action_name = "Attack1_Push", rate = 40, loop = 1  },
   { action_name = "Attack2_Spell1_Laser", rate = 15, loop = 1  },
   -- { action_name = "Attack3_Spell2_EraserBall_GreenDragon", rate = 22, loop = 1,cooltime=5000  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   -- { action_name = "Walk_Back", rate = 10, loop = 1  },
   -- { action_name = "Move_Left", rate = 3, loop = 1  },
   -- { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   -- { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack2_Spell1_Laser", rate = 15, loop = 1  },
   { action_name = "Attack3_Spell2_EraserBall_GreenDragon", rate = 22, loop = 1,cooltime=4000   },
}
g_Lua_Near3 = { 
   -- { action_name = "Stand_1", rate = 10, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 1  },
   -- { action_name = "Walk_Right", rate = 5, loop = 1  },
   -- { action_name = "Walk_Front", rate = 10, loop = 2  },
   -- { action_name = "Walk_Back", rate = 5, loop = 1  },
   -- { action_name = "Move_Left", rate = 5, loop = 1  },
   -- { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   -- { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack3_Spell2_EraserBall_GreenDragon", rate = 15, loop = 1,cooltime=4000   },
}
g_Lua_Near4 = { 
   -- { action_name = "Stand_1", rate = 7, loop = 1  },
   -- { action_name = "Stand_2", rate = 7, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 1  },
   -- { action_name = "Walk_Right", rate = 5, loop = 1  },
   -- { action_name = "Walk_Front", rate = 20, loop = 2  },
   -- { action_name = "Move_Left", rate = 5, loop = 1  },
   -- { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Attack3_Spell2_EraserBall_GreenDragon", rate = 15, loop = 1,cooltime=4000   },
}
g_Lua_Near5 = { 
   -- { action_name = "Stand_1", rate = 5, loop = 1  },
   -- { action_name = "Stand_2", rate = 10, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 2  },
   -- { action_name = "Walk_Right", rate = 5, loop = 2  },
   -- { action_name = "Walk_Front", rate = 10, loop = 2  },
   -- { action_name = "Move_Left", rate = 5, loop = 1  },
   -- { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 1  },
}
g_Lua_Skill = { 
	  { skill_index = 31335,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 500, target =2, selfhppercent = 100,limitcount=1},--Aura : 광폭화+HP회복+데미지감소 (비숍골드)
	  { skill_index = 31331,  cooltime = 35000, rate = 100, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 100,encountertime = 15000},--타임리스트릭션
	  { skill_index = 30931,  cooltime = 10000,  rate = 100,rangemin = 0, rangemax = 300, target = 3, selfhppercent = 100 },-- 포이즌 블로우 
	  { skill_index = 31336,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 600, target = 3, selfhppercent = 100,encountertime = 10000 },--리버스그라비티.
	  
}
