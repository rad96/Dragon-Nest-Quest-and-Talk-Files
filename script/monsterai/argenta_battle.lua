-- NPC Geraint Normal AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

 g_Lua_GlobalCoolTime1 = 4000
 g_Lua_GlobalCoolTime2 = 20000
 g_Lua_GlobalCoolTime3 = 30000
 g_Lua_GlobalCoolTime4 = 20000

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Attack01_Slash", rate = 10, loop = 1, globalcooltime = 1 },
   { action_name = "Attack02_XSlash", rate = 5, loop = 1, globalcooltime = 2 },
   { action_name = "Attack03_EarthQuake", rate = 5, loop = 1, globalcooltime = 3 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
   { action_name = "Move_Left", rate = 5,loop = 1 },
   { action_name = "Move_Right", rate = 5,loop = 1 },
   { action_name = "Attack02_XSlash", rate = 10, loop = 1, globalcooltime = 2 },
   { action_name = "Attack03_EarthQuake", rate = 10, loop = 1, globalcooltime = 3 },
   { action_name = "Attack04_WindSlash", rate = 10, loop = 1, globalcooltime = 4 },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
   { action_name = "Attack03_EarthQuake", rate = 10, loop = 1, globalcooltime = 3 },
   { action_name = "Attack04_WindSlash", rate = 8, loop = 1, globalcooltime = 4 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Move_Front", rate = 25, loop = 1 },
   { action_name = "Attack04_WindSlash", rate = 8, loop = 1, globalcooltime = 4 },
}
g_Lua_Skill = { 
   -- { skill_index = 35351,  cooltime = 3000, rate = 100,rangemin = 300, rangemax = 2000, target = 3 }, -- 디펜스
   { skill_index = 35352,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, td = "LF,FL,FR,RF"  },
}