-- /genmon 236010
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 800.0;
g_Lua_NearValue2 = 1200.0;
g_Lua_NearValue3 = 1700.0;
g_Lua_NearValue4 = 2500.0;
g_Lua_NearValue5 = 5000.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 800;
g_Lua_AssualtTime = 5000;

g_Lua_GlobalCoolTime1 = 50000 
g_Lua_GlobalCoolTime2 = 25000
g_Lua_GlobalCoolTime3 = 18000
g_Lua_GlobalCoolTime4 = 60000

g_Lua_CustomAction = 
{
	CustomAction1 = 
	{
		{"Attack01_Bite_Open",0},
		{"Attack05_Spin_Open",0},
	},
}
g_Lua_Near1 = 
	{
		{ action_name = "Stand",rate = 10, loop = 1 }, 
		{ action_name = "Attack01_Bite_Open",rate = 15, loop = 1,td="FL,FR",selfhppercentrange="0,40" }, 
		{ action_name = "Attack_BasicLeft",rate = 25,td = "FR,RF,RB", loop = 1,cooltime = 6000 }, 
		{ action_name = "Attack_BasicRight",rate = 25,td = "LF,FL,LB", loop = 1,cooltime = 6000 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2 }, 
		{ action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1, globalcooltime=2 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three_Fury",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="0,40",usedskill="31402" }, 
	}
g_Lua_Near2 = 
	{ 
		{ action_name = "Stand",rate = 10, loop = 1 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2 }, 
		{ action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1, globalcooltime=2 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three_Fury",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="0,50",usedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Four",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="0,40" ,usedskill="31402"},
	}
g_Lua_Near3 = 
	{
		{ action_name = "Stand",rate = 10, loop = 1 },
		{ action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2 }, 
		{ action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1, globalcooltime=2 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three_Fury",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="0,50",usedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Four",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="0,40" ,usedskill="31402"},
	}
g_Lua_Near4 = 
	{
		{ action_name = "Stand",rate = 10, loop = 1 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2 }, 
		{ action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1, globalcooltime=2 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three_Fury",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="0,50",usedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Four",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="0,40" ,usedskill="31402"},
	}
g_Lua_Near5 = 
	{ 
		{ action_name = "Stand",rate = 10, loop = 1 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2 }, 
		{ action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1, globalcooltime=2 }, 
		{ action_name = "Attack16_EyeLaser_Line_Three_Fury",rate = 10, loop = 1,globalcooltime=2,selfhppercentrange="0,50",usedskill="31402" }, 
		{ action_name = "Attack16_EyeLaser_Line_Four",rate = 10, loop = 1, globalcooltime=2,selfhppercentrange="0,40" ,usedskill="31402"},
	}
g_Lua_Skill = {
   { skill_index = 31397, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2500, target = 3 },  -- "점프" #4
   { skill_index = 31396, cooltime = 30000, rate = 100, rangemin = 0 ,rangemax= 5500,target = 3,next_lua_skill_index = 0,multipletarget = "1,4",encountertime=30000,selfhppercentrange="0,75" }, --"호밍빔" : 넥스트액션으로 점프를 실행 #5
   { skill_index = 31391, cooltime = 70000, rate = 20, rangemin = 0, rangemax = 5500, target = 3 ,encountertime=20000 }, -- "외쳐밀쳐내기" #7
   { skill_index = 31402, cooltime = 50000, rate = 100, rangemin = 0, rangemax = 2500, target = 1,selfhppercentrange="0,40",notusedskill="31402"},  --"격노" : 체력 40프로 이하부터 격노상태로 변환. #8   
   
}
   