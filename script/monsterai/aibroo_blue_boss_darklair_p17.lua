--AiBroo_Blue_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Move_Left_NotTarget", rate = 8, loop = 1 , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 8, loop = 1 , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 15, loop = 2 , max_missradian = 360 },
}
g_Lua_Near2 = { 
   { action_name = "Move_Left_NotTarget", rate = 12, loop = 1 , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 12, loop = 1 , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 3, loop = 2 , max_missradian = 360 },
}
g_Lua_Near3 = { 
   { action_name = "Move_Left_NotTarget", rate = 12, loop = 1 , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 12, loop = 1 , max_missradian = 360 },
   { action_name = "Move_Front_NotTarget", rate = 2, loop = 1 , max_missradian = 360 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Left_NotTarget", rate = 7, loop = 1 , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 7, loop = 1 , max_missradian = 360 },
   { action_name = "Move_Front_NotTarget", rate = 3, loop = 1 , max_missradian = 360 },
}
g_Lua_Skill = { 
   { skill_index = 20153,  cooltime = 7000, rate = 50,rangemin = 0, rangemax = 400, target = 3, selfhppercent = 100 },
   { skill_index = 20165,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 800, max_missradian = 360 , target = 3, selfhppercent = 100 },
   { skill_index = 20165,  cooltime = 3000, rate = 50, rangemin = 0, rangemax = 800, max_missradian = 360 , target = 3, selfhppercent = 100 },
   { skill_index = 20155,  cooltime = 10000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 70 },
}
