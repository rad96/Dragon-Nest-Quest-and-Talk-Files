--AiCharty_Shadow_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack05_Swoop", 1 },
      { "Attack06_Thrusting", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack06_Thrusting", rate = 6, loop = 1, target_condition = "State1" },
   { action_name = "Attack03_Indiscriminate_Gigantes", rate = 6, loop = 1, cooltime = 17000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = -1, loop = 2  },
   { action_name = "Move_Back", rate = 2, loop = 2  },
   { action_name = "CustomAction1", rate = 6, loop = 1  },
   { action_name = "Attack03_Indiscriminate_Gigantes", rate = 6, loop = 1, cooltime = 17000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 9, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 2  },
   { action_name = "Move_Left", rate = 6, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 2, loop = 2  },
   { action_name = "CustomAction1", rate = 6, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = -1, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = -1, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Move_Left", rate = 7, loop = 2  },
   { action_name = "Move_Right", rate = 7, loop = 2  },
   { action_name = "Move_Front", rate = 16, loop = 2  },
}
g_Lua_Skill = { 
   -- 두번할퀴기
   { skill_index = 20941,  cooltime = 60000, rate = 60, rangemin = 50, rangemax = 300, target = 3, selfhppercentrange = "50,100" },
   -- 바닥가시강화
   { skill_index = 33061,  cooltime = 60000, rate = 60, rangemin = 0, rangemax = 400, target = 3, selfhppercentrange = "50,100" },
   -- 검은가시 포효
   { skill_index = 31751,  cooltime = 60000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "0,49" },
   -- 밀어내기 포효
   { skill_index = 31752,  cooltime = 60000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,49" },
   -- 숨기(트리거)
   --{ skill_index = 31753,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "60,80" },
   --{ skill_index = 31753,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "40,60" },
   --{ skill_index = 31753,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "20,40" },
   --{ skill_index = 31753,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "0,20" },
   -- 검은 구슬 발사
   { skill_index = 31754,  cooltime = 70000, rate = 100, rangemin = 300, rangemax = 2000, target = 3, selfhppercentrange = "50,100" },
   -- 직전 가시
   { skill_index = 33060,  cooltime = 70000, rate = 100, rangemin = 300, rangemax = 2000, target = 3, selfhppercentrange = "0,49" },
}