--AiSpider_Black_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 80;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 350;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack3_DashBite", 0 },
      { "Walk_Back", 1 },
  },
  CustomAction2 = {
      { "Attack5_JumpAttack", 0 },
      { "Move_Back", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 18, loop = 1  },
   { action_name = "Walk_Back", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 6, loop = 1  },
   { action_name = "Attack2_Bite", rate = 6, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 15, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 1  },
   { action_name = "Attack2_Bite", rate = 5, loop = 1  },
   { action_name = "Attack3_DashBite", rate = 3, loop = 1, max_missradian = 20  },
   { action_name = "CustomAction1", rate = 4, loop = 1, max_missradian = 20  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Attack3_DashBite", rate = 2, loop = 1, max_missradian = 20  },
   { action_name = "Attack5_JumpAttack", rate = 2, loop = 1, max_missradian = 10  },
   { action_name = "CustomAction1", rate = 4, loop = 1, max_missradian = 20  },
   { action_name = "CustomAction2", rate = 3, loop = 1, max_missradian = 10  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 25, loop = 1  },
   { action_name = "Attack5_JumpAttack", rate = 2, loop = 1, max_missradian = 10  },
   { action_name = "CustomAction2", rate = 4, loop = 1, max_missradian = 10  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 30, loop = 1  },
   { action_name = "Move_Left", rate = 18, loop = 1  },
   { action_name = "Move_Right", rate = 18, loop = 1  },
   { action_name = "Move_Back", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Front", rate = 30, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 3  },
   { action_name = "Move_Right", rate = 10, loop = 3  },
   { action_name = "Attack5_JumpAttack", rate = 24, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20095,  cooltime = 40000, rate = 40, rangemin = 100, rangemax = 800, target = 3, selfhppercent = 100 },
}
