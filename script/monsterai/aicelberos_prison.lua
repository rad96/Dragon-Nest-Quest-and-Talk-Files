-- Celberos Normal Prison
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 400.0;
g_Lua_NearValue2 = 800.0;
g_Lua_NearValue3 = 1300.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 320;
g_Lua_AssualtTime = 3000;


g_Lua_Near1 = 
{ 
	{ action_name = "Stand_2",	rate = 5,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 5,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 5,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 5,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_2",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_3",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_3",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand_3",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Skill=
{
	-- ����¡ ��
	{ skill_index = 30104, SP = 0, cooltime = 15000, rangemin = 000, rangemax = 3000, target = 3, rate = 50, td = "LF,FL,FR,RF", rate = 50 },	
	-- ȭ���� ����
	{ skill_index = 30108, SP = 0, cooltime = 15000, rangemin = 000, rangemax = 3000, target = 3, rate = 50, td = "LF,FL,FR,RF", rate = 50 },
	-- ��ũ����Ʈ��
	{ skill_index = 30110, SP = 0, cooltime = 15000, rangemin = 100, rangemax = 2000, target = 3, rate = 50, td = "FL,FR", rate = 50 },
}

