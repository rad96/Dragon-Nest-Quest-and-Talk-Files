
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" },
 State2 = {"Stay|Move|Attack", "!Down","!Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Down|!Move","Air"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 신 브레이커
  CustomAction6 = {
     { "useskill", lua_skill_index = 30, rate = 100 },
  },
  -- 
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 31, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12800

g_Lua_Near1 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Right", rate = 5, loop = 1, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 12000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 24000, target_condition = "State4" },
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 20, loop = 1 },
     { action_name = "Move_Right", rate = 20, loop = 1 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 12000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 24000, target_condition = "State4" },
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}

g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
     --{ action_name = "SKill_Dash", rate = -1, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Tumble_Back", rate = 20, loop = 1, globalcooltime = 1 },	 
     { action_name = "CustomAction2", rate = 10, loop = 1  },
     { action_name = "CustomAction3", rate = 10, loop = 1 },
}

g_Lua_NonDownRangeDamage = {

	 { action_name = "useskill", lua_skill_index = 37, rate = 30 },
	 { action_name = "useskill", lua_skill_index = 41, rate = 50 },
}

g_Lua_Skill = { 
-- 0 소울윈드
     { skill_index = 84501, cooltime = 4000, rate = 10, rangemin = 300, rangemax = 1200, target = 3, combo1 = "20,30,1", combo2 = "1,40,1", combo3 =" 3,50,1" },-- 액션프레임끊기 nextcustomaction 시그널
-- 1 스팅 브리즈
     { skill_index = 84502, cooltime = 8000, rate = 30, rangemin = 0, rangemax = 300, target = 3, combo1 = "8,40,1", combo2 = "2,100,0" , target_condition = "State3"},
-- 2 스피릿 블로우
     { skill_index = 84503, cooltime = 12000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, combo1 = " 1,60,1", combo2 = "32,100,0" },-- 케릭터 어그로유지 LocktargetLook
-- 3 디스페어 니들
     { skill_index = 84504, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 ="2,100,1" , target_condition = "State3"},
-- 4 네거티브 고스트
	 { skill_index = 84505, cooltime = 16000, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "0,100,1" },-- 인풋으로 발동하는 건 액션마다 테이블 다 등록
-- 5 팬시 턴
     { skill_index = 84506, cooltime = 9600, rate = -1, rangemin = 0, rangemax = 1000, target = 3 },
-- 6 팬텀 가드
     { skill_index = 84507, cooltime = 24000, rate = 20, rangemin = 300, rangemax = 1200, target = 3, selfhppercent= 100 },
-- 7 스피릿 오브 지니
     { skill_index = 84508, cooltime = 48000, rate = 20, rangemin = 300, rangemax = 1200, target = 3 , selfhppercent= 60},
-- 8 슬라이트 턴
     { skill_index = 84509, cooltime = 0, rate = -1, rangemin = 0, rangemax = 200, target = 3 , combo1 ="3,70,1", combo2 ="13,100,0"},
-- 9 소울 키스
     { skill_index = 84510, cooltime = 2400, rate = 20, rangemin = 0, rangemax = 150, target = 3 ,combo1 = "22,100,1" , target_condition = "State1"},
-- 10 리턴 스핀
     { skill_index = 84512, cooltime = 7200, rate = -1, rangemin = 0, rangemax = 200, target = 3, blowcheck = "23"  },
-- 11고스트 킥
     { skill_index = 84513, cooltime = 4000, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 = "36,40,1" , combo2 = " 47,100,1" },
-- 12 슬레이브 핸드
     { skill_index = 84514, cooltime = 9600, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 = "37,100,1" },

-- 13 그레이즈 댄스
     { skill_index = 84522, cooltime = 10400, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 = "50,30,0" , combo2 = "22,100,1" , resetcombo =1},	  
-- 14 스위트 서클
     { skill_index = 84523, cooltime = 8000, rate = 20, rangemin = 0, rangemax = 300, target = 3, combo1 = "50,30,0", combo2 = "17,40,1" , combo3 = "21,50,1" , combo4 = "23,100,1", resetcombo =1 },
-- 15 트윙클 스핀
     { skill_index = 84524, cooltime = 12000, rate = -1, rangemin = 0, rangemax = 1200, target = 3 , combo1= "36,50,1", combo2 ="47,60,0", combo3 = "11,100,0" },
-- 16 리프레싱 스크류
     { skill_index = 84525, cooltime = 12800, rate = 20, rangemin = 0, rangemax = 900, target = 3, combo1 = "50,30,0", combo2 = "46,100,0",  globalcooltime = 2, resetcombo =1},
-- 17 엘레강스 스톰
     { skill_index = 84526, cooltime = 25600, rate = 20, rangemin = 0, rangemax = 300, target = 3, combo1 = "15,60,1", combo2 = "30,100,1" },
-- 18 일루전 댄스
     { skill_index = 84528, cooltime = 12000, rate = 30, rangemin = 300, rangemax = 1200, target = 3, combo1 = "49,100,0" },
-- 19 일루전 게이즈
     { skill_index = 84529, cooltime = 20000, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "15,50,1", combo2 = "22,100,1" , resetcombo =1},	

-- 20 스토커 EX
     { skill_index = 84550, cooltime = 16000, rate = 20, rangemin = 200, rangemax = 1200, target = 3, combo1 = "49,100,0" },
-- 21 더스크헌터 EX
     { skill_index = 84551, cooltime = 8800, rate = 20, rangemin = 0, rangemax = 700, target = 3, cancellook = 1,combo1 = "38,50,0", combo2 = "19,60,0", combo3 = "22,100,0"  },
-- 22 어볼리셔 EX
     { skill_index = 84552, cooltime = 9600, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "32,60,1", combo2 = "23,70,1", combo3 = "30,70,0", combo4 = " 24,100,0" },
-- 23 수피 댄서
     { skill_index = 84533, cooltime = 13600, rate = -1, rangemin = 0, rangemax = 200, target = 3, cancellook = 1, combo1 = "17,40,1", combo2 = "31,60,1", combo3 = "28,70,1", combo4 =" 18,100,0" ,target_condition = "State3"},
-- 24 제너럴 던블레이드 EX
     { skill_index = 84554, cooltime = 17600, rate = 20, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = " 29,50,1", combo2 = "22,100,1" },

-- 25 엑스태틱 댄스 힐
     { skill_index = 84536, cooltime = 72000, rate = -1, rangemin = 0, rangemax = 1200, target = 3, selfhppercent= 60 },
-- 26 엑스태틱 댄스 피니쉬
     { skill_index = 84537, cooltime = 72000, rate = -1, rangemin = 0, rangemax = 900, target = 3 },
-- 27 엑스태틱 댄스 피뻥
     { skill_index = 84538, cooltime = 72000, rate = -1, rangemin = 0, rangemax = 900, target = 3 },
-- 28 엑스태틱 댄스 준비
     { skill_index = 84539, cooltime = 72000, rate = 10, rangemin = 0, rangemax = 900, target = 3, combo1 = "25,70,0", selfhppercent= 80},

-- 29 시니아 턴
     { skill_index = 84540, cooltime = 12000, rate = 20, rangemin = 0, rangemax = 700, target = 3, combo1 = "3,60,1", comb2 = "13,70,1", combo3 = "1,100,1" ,combo4 ="2,100,0"},	
	 
-- 30 와이드 스팅어
     { skill_index = 84549, cooltime = 12000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = " 17,50,1", combo2 = "23,60,0", combo3 = "24,100,0" },	
-- 31프라이 토르
     { skill_index = 84553, cooltime = 24000, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 =" 22,40,1", combo2 = "2,50,1", combo3 = "23,100,1" },--몬스터는 점프시그널이 안뎀 그래서 벨로시티시그널 써야뎀, 몬스터는 크로스헤어로 타겟잡으면 안뎀

-- 32 평타1
     { skill_index = 84556, cooltime = 100, rate = 50, rangemin = 0, rangemax = 200, target = 3, combo1 = "33,100,1", combo2 = "1,50,0", combo3 = "22,100,0" , target_condition = "State3"},
-- 33 평타2
     { skill_index = 84557, cooltime = 100, rate = -1, rangemin = 0, rangemax = 150, target = 3, combo1 = "34,100,1", combo2 = "22,100,0" },
-- 34 평타3
     { skill_index = 84558, cooltime = 100, rate = -1, rangemin = 0, rangemax = 800, target = 3, combo1 = "23,40,1", combo2 = "1,60,1", combo3 = "21,70,1", combo4 = "22,80,0", combo5 = "15,100,0" },	

-- 35 점프
     { skill_index = 84559, cooltime = 15000, rate = 20, rangemin = 0, rangemax = 700, target = 3,combo1 = "35,50,0",combo1 = "47,60,0", combo3 ="11,100,0" },
-- 36 점프평타
     { skill_index = 84560, cooltime =1000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "11,60,0" , combo2 ="47,100,0"},	 

-- 37 터닝 스텝F
     { skill_index = 84511, cooltime =1000, rate = -1, rangemin = 0, rangemax = 700, target = 3 },	
-- 38 터닝 스텝B
     { skill_index = 84561, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 900, target = 3 },	
-- 39 터닝 스텝L
     { skill_index = 84562, cooltime = 18000, rate = -1, rangemin = 0, rangemax = 900, target = 3 },--몬스터는 점프시그널이 안뎀 그래서 벨로시티시그널 써야뎀, 몬스터는 크로스헤어로 타겟잡으면 안뎀

-- 40 더미고스트F
     { skill_index = 84515, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 900, target = 3 },
-- 41 더미고스트B
     { skill_index = 84568, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 300, target = 3 },
-- 42 더미고스트L
     { skill_index = 84569, cooltime =2800, rate = -1, rangemin = 0, rangemax = 300, target = 3 },	 
-- 43 더미고스트R
     { skill_index = 84570, cooltime =2800, rate = -1, rangemin = 0, rangemax = 300, target = 3 },	 

-- 44 미스트 스탭 1
     { skill_index = 84571, cooltime =2800, rate = -1, rangemin = 0, rangemax = 300, target = 3 },	 
-- 45 미스트 스탭 2
     { skill_index = 84572, cooltime =2800, rate = -1, rangemin = 0, rangemax = 300, target = 3 , combo1 = "11,50,0" , combo2 = "36.60.1", combo3 ="47,100,0" },	 

-- 46 리프레싱 스크류1
     { skill_index = 84573, cooltime =0, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "50,100,0" , resetcombo =1},
-- 47 리프레싱 스크류 공중
     { skill_index = 84574, cooltime =0, rate = -1, rangemin = 0, rangemax = 300, target = 3 , combo1 = "50,100,0" ,globalcooltime = 2, resetcombo =1},

-- 48 에스테틱 본크
     { skill_index = 84575, cooltime = 20000, rate = -1, rangemin = 0, rangemax = 300, target = 3},
-- 49 미스트스텝
     { skill_index = 84541, cooltime = 14400, rate = 20, rangemin = 0, rangemax = 700, target = 3 , combo1 = "44,20,0", combo2 = "45,100,0" , resetcombo =1},	
-- 50 브리즈 콜
     { skill_index = 84521, cooltime = 20000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "22,50,1", combo2 = "23,60,1", combo3 = "23,100,0" , resetcombo =1},	 

	 }