
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1100;
g_Lua_NearValue4 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;


g_Lua_CustomAction = {
-- 기본 공격 2연타 145프레임
  CustomAction1 = {
     { "Attack1_Scimitar" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction2 = {
     { "Attack1_Scimitar" },
     { "Attack2_Scimitar" },
  },
-- 기본 공격 4연타 313프레임
  CustomAction3 = {
     { "Attack1_Scimitar" },
     { "Attack2_Scimitar" },
     { "Attack3_Scimitar" },
  },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 6000
g_Lua_GlobalCoolTime3 = 25000

g_Lua_Near1 = 
{
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 6, loop = 1 },
     { action_name = "Move_Right", rate = 6, loop = 1 },
     { action_name = "Move_Back", rate = 2, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Back", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Left", rate = 2, loop = 1 },
     { action_name = "Move_Right", rate = 2, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 1 },
     { action_name = "CustomAction1", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 6, loop = 1, cooltime = 16000, globalcooltime = 1 },
     { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 3 },
     { action_name = "Move_Left", rate = 1, loop = 1 },
     { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 15, loop = 4 },
}

g_Lua_Skill = { 
-- 스피디컷 Skill_SpeedyCut
   { skill_index = 31201, cooltime = 8000, rate = 80, rangemin = 0, rangemax = 300, target = 3, globalcooltime = 2 },
-- 더티트릭 Skill_DurtyTrick
   { skill_index = 31202,  cooltime = 10000, rate = 50, rangemin = 0, rangemax = 400, target = 3, globalcooltime = 2 },
-- 피어싱 스타 Skill_PiercingStar
   { skill_index = 31203,  cooltime = 15000, rate = 80, rangemin = 0, rangemax = 700, target = 3, globalcooltime = 2 },
-- 쉐도우 핸드   Skill_ShadowHand
   { skill_index = 31204,  cooltime = 15000, rate = 40, rangemin = 0, rangemax = 1000, target = 3, globalcooltime = 2 },

-- 어플러즈  Skill_Applause
   -- { skill_index = 31205, cooltime = 15000, rate = 80, rangemin = 0, rangemax = 700, target = 3, globalcooltime = 2 },
-- 레이크 Skill_Rake
   { skill_index = 31206, cooltime = 14000, rate = 40, rangemin = 0, rangemax = 700, target = 3, globalcooltime = 2  },
-- 쉬프트블로우 Skill_Shiftblow
   { skill_index = 31207, cooltime = 14000, rate = 80, rangemin = 0, rangemax = 300, target = 3, globalcooltime = 2 },
-- 모탈블로우  Skill_Motalblow
   { skill_index = 31208, cooltime = 31000, rate = 20, rangemin = 0, rangemax = 500, target = 3, globalcooltime = 2  },
-- 버닝 콜 Skill_Xenoside
   { skill_index = 31209, cooltime = 27000, rate = 80, rangemin = 0, rangemax = 300, target = 3, globalcooltime = 2 },
}