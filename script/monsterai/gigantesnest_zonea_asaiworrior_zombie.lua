--AiAsaiWorrior_Zombie_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 900;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 15000

g_Lua_Near1 = { 
   { action_name = "Attack2_SelfBomb_OnlyEnemy", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Move_Front", rate = 2, loop = 3 },
}
g_Lua_Near3 = { 
   { action_name = "Move_Front", rate = 2, loop = 4 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 2, loop = 5 },
}

g_Lua_Assault = { 
   { action_name = "Attack2_SelfBomb_OnlyEnemy", rate = 5, loop = 1, approach = 400 },
}