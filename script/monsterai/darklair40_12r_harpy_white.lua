--AiHarpy_White_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 6, loop = 3  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Attack1_Throw", rate = 6, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 4, loop = 1  },
   { action_name = "Attack1_Throw", rate = 4, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 1, loop = 1  },
}

g_Lua_Skill = { 
   { skill_index = 20211,  cooltime = 20000, rate = 20, rangemin = 500, rangemax = 1000, target = 3 },
}
