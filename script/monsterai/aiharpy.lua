-- Harpy AI

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 700.0;
g_Lua_NearValue3 = 1500.0;

g_Lua_LookTargetNearState = 3;

g_Lua_WanderingDistance = 1500.0;

g_Lua_PatrolBaseTime = 5000;

g_Lua_PatrolRandTime = 3000;


g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 20,		loop = 3 },
	{ action_name = "Move_Left",	rate = 2,		loop = 2 },
	{ action_name = "Move_Right",	rate = 2,		loop = 2 },
	{ action_name = "Move_Back",	rate = 30,		loop = 3 },
--	{ action_name = "Attack2",	rate = 20,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 3 },
	{ action_name = "Move_Left",	rate = 30,		loop = 3 },
	{ action_name = "Move_Right",	rate = 30,		loop = 3 },
	{ action_name = "Move_Front",	rate = 5,		loop = 2 },
	{ action_name = "Move_Back",	rate = 10,		loop = 3 },
	{ action_name = "Attack1",	rate = 50,		loop = 1 },
	{ action_name = "Attack3",	rate = 60,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 30,		loop = 3 },
	{ action_name = "Move_Right",	rate = 30,		loop = 3 },
	{ action_name = "Move_Front",	rate = 30,		loop = 3 },
            { action_name = "Attack1",	rate = 50,		loop = 1 },
--	{ action_name = "Assault",	rate = 40,		loop = 1 },
}
