--AiHarpy_Red_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 6, loop = 3  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Attack1_Throw_R", rate = 6, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 4, loop = 1  },
   { action_name = "Attack1_Throw_R", rate = 4, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 1, loop = 1  },
}

g_Lua_Skill = { 
-- 프레임(버프)
   { skill_index = 20212,  cooltime = 18000, rate = 60,rangemin = 0, rangemax = 1000, target = 2, selfhppercent = 80 },
-- 크리티컬 업(버프)
   { skill_index = 20215,  cooltime = 22000, rate = 50, rangemin = 0, rangemax = 1000, target = 2, selfhppercent = 30 },
}
