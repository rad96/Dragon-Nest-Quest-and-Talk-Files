g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue1 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
}



g_Lua_Skill = {
	-- 스폰스킬
	{ skill_index = 37001,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 8000, target = 3, limitcount = 1 }, -- Attack05_Shout
	-- 줄패턴류
	{ skill_index = 37003,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 8000, target = 3, limitcount = 1, selfhppercent = 75 }, -- Attack07_FlyBomb
	{ skill_index = 37005,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 8000, target = 3, limitcount = 1, selfhppercent = 25 }, -- Attack08_Breath_Start
	-- 일반 공격	
	{ skill_index = 37006,  encounter = 20000, cooltime = 30000, rate = 2, rangemin = 0, rangemax = 8000, target = 3 }, -- Attack01_BlackDragon_SuperBreath - 12s
	{ skill_index = 37007,  encounter = 20000, cooltime = 30000, rate = 3, rangemin = 0, rangemax = 8000, target = 3 }, -- Attack02_SeaDragon_Bash - 9s
	{ skill_index = 37008,  encounter = 20000, cooltime = 30000, rate = 2, rangemin = 0, rangemax = 8000, target = 3 }, -- Attack03_GreenDragon_IceWall - 9s
	{ skill_index = 37009,  encounter = 20000, cooltime = 30000, rate = 2, rangemin = 0, rangemax = 8000, target = 3 }, -- Attack04_DesertDragon_Stomp - 9s (공격불가패턴)
	{ skill_index = 37010,  encounter = 20000, cooltime = 30000, rate = 2, rangemin = 0, rangemax = 8000, target = 3 }, -- Attack06_BlackDragon_BlackHole - 9s
}