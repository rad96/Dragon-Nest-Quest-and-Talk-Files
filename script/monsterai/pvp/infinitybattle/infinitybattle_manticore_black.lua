--/genmon 901753
--InfinityBattle_Manticore_Black.lua
g_Lua_NearTableCount = 4
g_Lua_NearValue1 = 500
g_Lua_NearValue2 = 1000
g_Lua_NearValue3 = 3000
g_Lua_NearValue4 = 10000

g_Lua_PatrolBaseTime = 2000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 3000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop=1,td = "FR,FL",},
   { action_name = "Walk_Left", rate = 4, loop = 1,td = "FL,FR,LF,RF", },
   { action_name = "Walk_Right", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop=1,td = "FR,FL", },
   { action_name = "Walk_Front", rate = 8, loop=1,td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Walk_Right", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 10, loop = 1,td = "FR,FL",},
   { action_name = "Move_Front", rate = 13, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 10, loop=1,td = "FR,FL", },
   { action_name = "Move_Front", rate = 15, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_GlobalCoolTime1 = 12000 
g_Lua_GlobalCoolTime2 = 30000 
-- g_Lua_Assault =  {    { lua_skill_index = 3, rate = 20, approach = 300 },}
g_Lua_Skill = {
   { skill_index = 400121,  cooltime = 8000, rate =40, rangemin = 0, rangemax = 400, target = 3, td = "FL,LF", },--Attack001_LHook_IB -- 좌베기
   { skill_index = 400122,  cooltime = 8000, rate =40, rangemin = 0, rangemax = 400, target = 3, td = "FR,RF", },--Attack002_RHook_IB -- 우베기
   { skill_index = 400123,  cooltime = 12000, rate = 50, rangemin = 0, rangemax = 400, target = 3, td = "BL", randomtarget = "0.6,0,1", globalcooltime = 1 },--Attack004_RB_IB --꼬리휘두르기
   { skill_index = 400124,  cooltime = 12000, rate = 50, rangemin = 0, rangemax = 400, target = 3, td = "BR", randomtarget = "0.6,0,1", globalcooltime = 1 },--Attack005_LB_IB -- 꼬리 휘두르기
   { skill_index = 400125,  cooltime = 30000, rate = 60, rangemin = 600, rangemax = 1500, target = 3, td = "FR,FL", encountertime = 15000 },--Attack006_Thorn_IB -꼬리 미사일
   { skill_index = 400126,  cooltime = 45000, rate = 90, rangemin = 0, rangemax = 5000, target = 3, td = "FR,FL,LF,RF", },--Attack010_BlackCloud_IB 
   { skill_index = 400127,  cooltime = 90000, rate = 85, rangemin = 300, rangemax = 3000, target = 3, td = "FR,FL,LF,RF", encountertime = 120000 ,globalcooltime = 2 },--Attack011_PhantomHowl_IB@@@@
   { skill_index = 400129,  cooltime = 45000, rate = 85, rangemin = 300, rangemax = 1500, target = 3, randomtarget = "0.6,0,1", td = "FR,FL", },--Attack021_DashAttack_IB
   { skill_index = 400130,  cooltime = 90000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, td = "LF,FL,FR,RF,RB,BR,BL,LB" , selfhppercentrange = "0,50", globalcooltime = 2 },--Attack022_BlackBreath_IB_Start@@@@
   { skill_index = 400128,  cooltime = 5000, rate = 100, rangemin = 0, rangemax = 5000, target = 1, priority = 100, encountertime = 90000, limitcount = 1 },--Attack014_ByeBye_IB
}