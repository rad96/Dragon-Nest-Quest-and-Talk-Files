--/genmon 901754
--InfinityBattle_Crocodile_Red.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 6000;

g_Lua_PatrolBaseTime = 6000
g_Lua_PatrolRandTime = 6000
g_Lua_ApproachValue = 500
g_Lua_AssaultTime = 8000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 2, loop = 1 },
	{ action_name = "Walk_Left", rate = 13, loop = 1 },
	{ action_name = "Walk_Right", rate = 13, loop = 1 },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 3, loop = 1 },	
    { action_name = "Walk_Front", rate = 13, loop = 1 },
}
g_Lua_Near3 = 
{ 
    { action_name = "Walk_Left", rate = 6, loop = 1 },
	{ action_name = "Walk_Right", rate = 6, loop = 1 },
   	{ action_name = "Move_Front", rate = 15, loop = 1 },
	{ action_name = "Assault", rate = 13, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Walk_Left", rate = 3, loop = 1 },
	{ action_name = "Walk_Right", rate = 3, loop = 1 },
	{ action_name = "Move_Front", rate = 15, loop = 1 },
	{ action_name = "Assault", rate = 13, loop = 1 },
}

g_Lua_MeleeDefense = { { lua_skill_index = 1, rate = 100  },  }
g_Lua_RangeDefense = { { lua_skill_index = 1, rate = 100  }, }
g_Lua_Assault = { { lua_skill_index = 0, rate = 5, approach = 500},}

g_Lua_Skill = { 
    { skill_index = 400132,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 6000, target = 3,},--Attack03_ShieldAttack_IB
	{ skill_index = 400134,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 6000, target = 3,},--Attack06_Counter_IB
    { skill_index = 400131,  cooltime = 9000, rate = 50, rangemin = 0, rangemax = 800, target = 3,},--Attack01_Swing_IB
	{ skill_index = 400133,  cooltime = 30000, rate = 70, rangemin = 300, rangemax = 1500, target = 3,},--Attack04_Charge_IB_Start
	{ skill_index = 400136,  cooltime = 45000, rate = 60, rangemin = 0, rangemax = 6000, target = 1, randomtarget = "0.6,0,1" ,encountertime = 30000  },--Attack10_ExplosionCri_IB_Start
	{ skill_index = 400135,  cooltime = 90000, rate = 90, rangemin = 0, rangemax = 6000, target = 3, selfhppercentrange = "0,50", },--Attack06_Counter_IB_Start
	{ skill_index = 400137,  cooltime = 5000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, priority = 100, encountertime = 90000, limitcount = 1 },--Attack12_ByeBye_IB
}