--/genmon 901751
--InfinityBattle_Golem_Ice.lua
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 20, loop = 1 },
	{ action_name = "Walk_Left", rate = 5, loop = 1, },
	{ action_name = "Walk_Right", rate = 5, loop = 1, },
	{ action_name = "Walk_Back", rate = 5, loop = 1, },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 15, loop = 1 },	
	{ action_name = "Walk_Front", rate = 8, loop = 1, },
	{ action_name = "Walk_Left", rate = 6, loop = 1, },
	{ action_name = "Walk_Right", rate = 6, loop = 1, },
}
g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 15, loop = 1 },	
   	{ action_name = "Walk_Front", rate = 13, loop = 1, },
    { action_name = "Walk_Left", rate = 3, loop = 1, },
	{ action_name = "Walk_Right", rate = 3, loop = 1, },
}
g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 8, loop = 1 },	
	{ action_name = "Move_Front", rate = 15, loop = 1, },
	{ action_name = "Assault", rate = 10, loop = 1, cooltime = 15000, },
}

g_Lua_Assault = { { lua_skill_index = 0, rate = 10, approach = 300},} --Attack01_RedDNest_Bash
g_Lua_Skill = {
   { skill_index = 400102, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 3000, target = 3,  },--Attack02_Shortblow_IB
   { skill_index = 400101, cooltime = 8000, rate = 60, rangemin = 0, rangemax = 450, target = 3,  },--Attack01_LinearPunch_IB
   { skill_index = 400103, cooltime = 50000, rate = 50, rangemin = 0, rangemax = 4000, target = 3, randomtarget = "1.6,0,1", },--Attack04_RollingAttack_IB_Start
   { skill_index = 400105, cooltime = 45000, rate = 70, rangemin = 200, rangemax = 1500, target = 3, randomtarget = "1.6,0,1", encountertime = 10000 },--Attack19_8MJump_IB
   { skill_index = 400104, cooltime = 40000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "0,50", },--Attack06_IceTrail_IB
   { skill_index = 400106, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 5000, target = 1, priority = 100, encountertime = 90000, limitcount = 1 },--Attack20_ByeBye_IB_Start
}