--InfinityBattle_Cyclops_White.lua
--/genmon 901752

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 6000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000


g_Lua_Near1 = 
{ 
   { action_name = "Stand_5", rate = 5, loop = 1, },
   { action_name = "Walk_Left", rate = 5, loop = 1, },
   { action_name = "Walk_Right", rate = 5, loop = 1, },
   { action_name = "Attack001_Bash", rate = 20, loop = 1, },
}

g_Lua_Near2 = { 
   { action_name = "Stand_5", rate = 5, loop = 1, },
   { action_name = "Walk_Left", rate = 2, loop = 1, },
   { action_name = "Walk_Right", rate = 2, loop = 1, },
   { action_name = "Move_Front", rate = 10, loop = 1,},
}

g_Lua_Near3 = { 
   { action_name = "Stand_5", rate = 2, loop = 1,},
   { action_name = "Move_Front", rate = 10, loop = 3,},
}

g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 20, loop = 1, },
   { action_name = "Assault", rate = 30, loop = 1, },
}
g_Lua_Assault = { 
   { action_name = "Attack001_Bash", rate = 10, loop = 1, },
}
g_Lua_BeHitSkill = { { lua_skill_index = 0, rate = 100, skill_index = 400117  },}
g_Lua_Skill = { 
   { skill_index = 400118, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, },--Attack26_DeathHugFin_IB
   { skill_index = 400111, cooltime = 20000, rate = 30, rangemin = 0, rangemax = 2000, target = 3,  },--Attack01_Bash_IB
   { skill_index = 400112, cooltime = 20000, rate = 30, rangemin = 0, rangemax = 2000, target = 3,  },--Attack07_ElectricDash_IB
   { skill_index = 400113, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 5000, target = 1, priority = 100, encountertime = 90000, limitcount = 1  },--Attack15_ByeBye_IB
   { skill_index = 400114, cooltime = 20000, rate = 30, rangemin = 0, rangemax = 2000, target = 3,  },--Attack20_LinearLightning_IB
   { skill_index = 400115, cooltime = 20000, rate = 30, rangemin = 0, rangemax = 2000, target = 3,  },--Attack24_SummonRelicBuff_IB
   { skill_index = 400116, cooltime = 20000, rate = 30, rangemin = 0, rangemax = 2000, target = 3,  },--Attack24_SummonRelicCharger_IB
   { skill_index = 400117, cooltime = 30000, rate = 30, rangemin = 0, rangemax = 1500, target = 3, },--Attack25_DeathHug_IB_Start
}
