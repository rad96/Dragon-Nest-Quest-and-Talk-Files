-- /genmon 901756
-- InfinityBattle_QueenSpider_Green.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 8000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 18, loop = 1 },
	{ action_name = "Walk_Left", rate = 13, loop = 1 },
	{ action_name = "Walk_Right", rate = 13, loop = 1 },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1 },	
	{ action_name = "Walk_Front", rate = 13, loop = 1 },
    { action_name = "Walk_Left", rate = 13, loop = 1 },
	{ action_name = "Walk_Right", rate = 13, loop = 1 },
}
g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1 },	
   	{ action_name = "Walk_Front", rate = 13, loop = 1 },
	{ action_name = "Move_Front", rate = 4, loop = 1 },
}
g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1 },	
	{ action_name = "Move_Front", rate = 15, loop = 1 },
}

g_Lua_GlobalCoolTime1 = 50000 
g_Lua_GlobalCoolTime2 = 40000 

g_Lua_Skill = {
	{ skill_index = 400146, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, },--Attack13_WhirlAttack_IB
    --// Next_Skill // --
    { skill_index = 400141, cooltime = 12000, rate = 60, rangemin = 0, rangemax = 500, target = 3, },--Attack01_Slash_IB
    { skill_index = 400142, cooltime = 25000, rate = 55, rangemin = 300, rangemax = 1200, target = 3, },--Attack03_JumpAttack_IB
    { skill_index = 400143, cooltime = 40000, rate = 70, rangemin = 0, rangemax = 1500, target = 3, },--Attack04_Wep_IB
	{ skill_index = 400144, cooltime = 60000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, encountertime = 15000 },--Attack05_Spawn_IB
	{ skill_index = 400145, cooltime = 5000, rate = 45, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,30", },--Attack07_Fury_IB
	{ skill_index = 400148, cooltime = 30000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, encountertime = 30000  },--Attack08_RollingAttack_IB
	{ skill_index = 400147, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 5000, target = 1, priority = 100, encountertime = 90000, limitcount = 1  },--Attack09_ByeBye_IB
}


