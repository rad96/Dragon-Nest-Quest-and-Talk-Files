--AiTroll_Blue_Assault_Easy.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 10000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_Claw", rate = 4, loop = 1, target_condition = "State1" },
   { action_name = "Attack3_RollingAttackS_N", rate = 1, loop = 1, target_condition = "State1", globalcoomtime=1 },
   { action_name = "Attack6_Bite", rate = -1, loop = 1, target_condition = "State1" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Attack3_RollingAttack_N", rate = 1, loop = 1, target_condition = "State1", globalcooltime=1 },
   { action_name = "Attack3_RollingAttackS_N", rate = 3, loop = 1, target_condition = "State1", globalcooltime=1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
   { action_name = "Attack3_RollingAttack_N", rate = 2, loop = 1, target_condition = "State1", globalcooltime=1 },
   { action_name = "Attack3_RollingAttackS_N", rate = 3, loop = 1, target_condition = "State1", globalcooltime=1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
   { action_name = "Attack3_RollingAttack", rate = 4, loop = 1, globalcooltime=1 },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack3_RollingAttack", rate = 3, loop = 1, globalcooltime=1 },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 20107,  cooltime = 39000, rate = 20,loop = 1, rangemin = 900, rangemax = 1200, target = 3 },
   { skill_index = 20108,  cooltime = 39000, rate = 50, loop = 1, rangemin = 600, rangemax = 900, target = 3, target_condition = "State1" },
   { skill_index = 20109,  cooltime = 26000, rate = 50, loop = 1, rangemin = 200, rangemax = 600, target = 3, target_condition = "State1" },
}
