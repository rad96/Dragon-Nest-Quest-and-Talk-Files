--AiKoboldWarrior_Gray_Elite_Normal.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 50;
g_Lua_NearValue2 = 150;
g_Lua_NearValue3 = 200;
g_Lua_NearValue4 = 300;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Move_Left", 2 },
      { "Attack2_Sever", 1 },
  },
  CustomAction2 = {
      { "Move_Right", 2 },
      { "Attack2_Sever", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 2  },
   { action_name = "CustomAction1", rate = 5, loop = 1, target_condition = "State1"  },
   { action_name = "CustomAction2", rate = 5, loop = 1, target_condition = "State1"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 8, loop = 2  },
   { action_name = "Move_Back", rate = 8, loop = 1  },
   { action_name = "Move_Left", rate = 8, loop = 1  },
   { action_name = "Move_Right", rate = 8, loop = 1  },
   { action_name = "Attack1_Chop", rate = 9, loop = 1  },
   { action_name = "Attack2_Sever", rate = 6, loop = 1  },
   { action_name = "CustomAction1", rate = 15, loop = 1, target_condition = "State1"  },
   { action_name = "CustomAction2", rate = 15, loop = 1, target_condition = "State1"  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack2_Sever", rate = 6, loop = 1  },
   { action_name = "CustomAction1", rate = 9, loop = 1, target_condition = "State1"  },
   { action_name = "CustomAction2", rate = 9, loop = 1, target_condition = "State1"  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 1, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Sever", rate = 4, loop = 1, cancellook = 1, approach = 150.0  },
   { action_name = "Attack1_Chop", rate = 5, loop = 1, cancellook = 1, approach = 100.0  },
}
g_Lua_Skill = { 
   { skill_index = 20031,  cooltime = 15000, rate = 100,rangemin = 0, rangemax = 500, target = 1, selfhppercent = 100 },
}
