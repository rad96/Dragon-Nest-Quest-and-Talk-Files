--SeaDragonNest_MiddleBoss_HideMonster.lua
--/genmon 235742
--skill : 30523
--초원골렘의 Announce를 받아 스킬을 실행하도록 한다.

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 3000
g_Lua_PatrolRandTime = 1000
g_Lua_AssualtTime = 5000
g_Lua_ApproachValue = 150

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 30, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 30, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 30, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 30, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 30523, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, announce = "100,20000" },
}