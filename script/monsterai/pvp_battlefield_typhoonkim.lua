--AiMinotauros_Red_Boss_Nest.lua

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 1200.0;
g_Lua_NearValue4 = 1600.0;
g_Lua_NearValue5 = 2000.0;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 250
g_Lua_AssualtTime = 8000

g_Lua_GlobalCoolTime1 = 50000 -- 트리거
g_Lua_GlobalCoolTime2 = 10000 -- 스트레이트.
g_Lua_GlobalCoolTime3 = 41000 -- Shout
g_Lua_GlobalCoolTime4 = 35000 --휠윈드
g_Lua_GlobalCoolTime5 = 25000 -- 그레이트 봄버!
g_Lua_GlobalCoolTime6 = 15000 --

g_Lua_Near1 = 
	{ 
		{ action_name = "Stand", rate = 4, loop = 1 },
		{ action_name = "Walk_Left", rate = 8, loop = 1 },
		{ action_name = "Walk_Right", rate = 8, loop = 1 },
		{ action_name = "Attack002_Combo", rate = 12, loop = 1 ,cooltime = 25000  },
		{ action_name = "Attack003_FullSwing", rate = 12, loop = 1 },
	}
g_Lua_Near2 = 
	{ 
		{ action_name = "Stand", rate = 4, loop = 1 },	
		{ action_name = "Walk_Left", rate = 8, loop = 1 },
		{ action_name = "Walk_Right", rate = 8, loop = 1 },		
		{ action_name = "Attack003_FullSwing", rate = 12, loop = 1 },
	}
g_Lua_Near3 = 
	{ 
		{ action_name = "Stand", rate = 4, loop = 1 },
		{ action_name = "Attack013_JumpChop", rate = 12, loop = 1, randomtarget = "1.6,0,1" },
		{ action_name = "Move_Front", rate = 12, loop = 2 },
		{ action_name = "Move_Left", rate = 4, loop = 1 },
		{ action_name = "Move_Right", rate = 4, loop = 1 },		
	}
g_Lua_Near4 = 
	{ 
		{ action_name = "Stand", rate = 2, loop = 1 },
		{ action_name = "Attack005_GreatBomb", rate = 28, loop = 1, cooltime=25000 },
		{ action_name = "Move_Front", rate = 12, loop = 3 },
		{ action_name = "Assault", rate = 15, loop = 1 },
	}
g_Lua_Near5 = 
	{ 
		{ action_name = "Stand", rate = 2, loop = 1 },
		{ action_name = "Move_Front", rate = 12, loop = 4 },
		{ action_name = "Assault", rate = 15, loop = 1 },
	}

g_Lua_Assault = { 
   { action_name = "Attack002_Combo", rate = 8, loop = 1, approach = 250 },
}

g_Lua_Skill = 
{ 
	{skill_index=33298, cooltime = 55000, rate = 70,   rangemin=0,  rangemax=500,   target=3, next_lua_skill_index=1, selfhppercent = 50 },--Attack007_Stomp_Lightning
	{skill_index=33299, cooltime = 1000, rate=-1,   rangemin=0,  rangemax=5000,   target=3, },--Attack014_WhirlWindLight_Start
-- Attack017_Fear
	{skill_index=33292,  cooltime = 45000, rate = 40, rangemin = 0, rangemax = 1000, target = 3, encountertime = 10000 },
-- Attack004_StraightArm
	{skill_index=33286,  cooltime=25000, rate=80,   rangemin=0,  rangemax=1200,   target=3 },
-- Attack018_SlideCharge_Start
	{skill_index=33302,  cooltime=20000,  cooltime = 30000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, td = "FL,FR", randomtarget = "1.6,0,1" },
}