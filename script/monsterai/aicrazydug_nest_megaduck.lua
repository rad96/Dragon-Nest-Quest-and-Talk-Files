--/genmon 901710
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 20, loop = 1 },
	{ action_name = "Walk_Left", rate = 5, loop = 1 },
	{ action_name = "Walk_Right", rate = 5, loop = 1 },
	{ action_name = "Walk_Back", rate = 5, loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 23, loop = 1 },	
	{ action_name = "Walk_Front", rate = 8, loop = 1 },
	{ action_name = "Walk_Left", rate = 6, loop = 1 },
	{ action_name = "Walk_Right", rate = 6, loop = 1 },	
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 30, loop = 1 },	
   	{ action_name = "Walk_Front", rate = 13, loop = 1 },
    { action_name = "Walk_Left", rate = 5, loop = 1 },
	{ action_name = "Walk_Right", rate = 5, loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 8, loop = 1 },	
	{ action_name = "Move_Front", rate = 15, loop = 1, },
	{ action_name = "Assault", rate = 40, loop = 1, },
}

g_Lua_RandomSkill =
{{
	{{ skill_index = 36822, rate = 50, next_lua_skill_index = 2 },},
	{{ skill_index = 36821, rate = 50, next_lua_skill_index = 4 },},
}}

g_Lua_Assault = { { lua_skill_index = 2, rate = 10, approach = 2000},} --(0 부터 시작)
-- globalcooltime=1, priority = 1, 
g_Lua_Skill = { 
	 { skill_index = 36819, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, },	--Attack1_foot 밟기
	 { skill_index = 36820, cooltime = 40000, rate = 50, rangemin = 0, rangemax = 2000, target = 3, },	--Attack2_Jump_Start 날아올라찍기
	 { skill_index = 36821, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, },	--Attack3_Dash_Start 달려가물어뜯기
     { skill_index = 36822, cooltime = 20000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, },	--Attack4_Scream_Start 레이져발사
	 { skill_index = 36819, cooltime = 10000, rate = 120, rangemin = 0, rangemax = 2000, target = 3, skill_random_index = 1}, 
	 { skill_index = 36821, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 350, target = 3, skill_random_index = 2},   
	 { skill_index = 36822, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 350, target = 3, skill_random_index = 2},   
}