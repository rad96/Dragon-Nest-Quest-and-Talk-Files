--AiDarkElf_Gray_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 10000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Down|!Attack", "Air" }, 
  State3 = {"!Stay|!Move|!Stiff|!Attack", "Stun" }, 
}

g_Lua_Near1 = { 
   { action_name = "Attack1_Claw", rate = 9, loop = 1  },
   { action_name = "Attack2_Somersault", rate = 9, loop = 1  },
   { action_name = "Attack3_AerialCombo", rate = 9, loop = 1  },
   { action_name = "Attack3_AerialCombo", rate = 200, loop = 1, target_condition = "State1"  },
   { action_name = "Attack4_SickleKick", rate = 200, loop = 1, target_condition = "State2"  },
   { action_name = "Attack2_Somersault", rate = 200, loop = 1, target_condition = "State3"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 4, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 6, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 4, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_2", rate = 4, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 5, loop = 1, approach = 150.0  },
   { action_name = "Attack2_Somersault", rate = 3, loop = 1, approach = 200.0  },
   { action_name = "Attack4_SickleKick", rate = 5, loop = 1, approach = 150.0  },
}
