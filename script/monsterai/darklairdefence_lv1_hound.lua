--DarkLairDefence_Lv1_Hound.lua
--1라운드
--좌우이동이 어느정도 유지되는 대신 뒤로 물러나는 것은 빈도를 낮춰야 함
--전체적인 난이도는 Abyss기준

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 5000;

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 16, loop = 1 },
	{ action_name = "Provoke", rate = 1, loop = 1 },
        { action_name = "Attack1_BiteCombo", rate = 3, loop = 1, cooltime = 12000 },
	{ action_name = "Atttack6_JumpBite", rate = 2, loop = 1, cooltime = 12000 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 16, loop = 1 },
        { action_name = "Attack1_BiteCombo", rate = 3, loop = 1, cooltime = 12000 },
	{ action_name = "Atttack6_JumpBite", rate = 2, loop = 1, cooltime = 12000 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 10, loop = 1 },
        { action_name = "Move_Front", rate = 15, loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 10, loop = 1 },
        { action_name = "Move_Front", rate = 10, loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand", rate = 10, loop = 1 },
	{ action_name = "Move_Front", rate = 10, loop = 1 },
}     