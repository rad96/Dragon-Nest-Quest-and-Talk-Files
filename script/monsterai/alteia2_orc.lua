--AiGoblin_Green_Normal.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 350;
g_Lua_NearValue3 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 15, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 15, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 21, loop = 1  },
}