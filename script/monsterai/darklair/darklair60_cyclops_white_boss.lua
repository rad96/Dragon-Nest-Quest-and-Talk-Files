--AiCyclops_Green_Boss_Normal.lua
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 30000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_globalcooltime1=10000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Stand_5" },
      { "Attack001_Bash" },
  },
  CustomAction2 = {
      { "Stand_5" },
      { "Attack002_BackNFrontChopping" },
  },
  CustomAction3 = {
      { "Attack003_JumpAttack" },
      { "Attack004_TurningSlash" },
  },
  CustomAction4 = {
      { "Attack003_JumpAttack", 1 },
  },
}
g_Lua_Near1 = 
{ 
	{ action_name = "Stand_5",			       rate = 5,	loop = 1 },
	{ action_name = "Walk_Left",			   rate = 5,	loop = 1, },
	{ action_name = "Walk_Right",			   rate = 5,	loop = 1, },
	{ action_name = "Attack001_Bash",		rate = 20,	loop = 1 },
	{ action_name = "Attack002_BackNFrontChopping",	rate = 20,	loop = 1,},
	{ action_name = "CustomAction3",		rate = 20,	loop = 1, },
}
g_Lua_Near2 = { 
	{ action_name = "Stand_5",			    rate = 5,	loop = 1, },
	{ action_name = "Walk_Left",			rate = 2,	loop = 1, },
	{ action_name = "Walk_Right",			rate = 2,	loop = 1, },
	{ action_name = "Move_Front",			rate = 10,	loop = 2, },
	{ action_name = "Attack008_SummonRelic_Charger",rate = 30,	loop = 1,  globalcooltime=1},
	{ action_name = "CustomAction3",		rate = 25,	loop = 1, },
}
g_Lua_Near3 = { 
	{ action_name = "Stand_5",			    rate = 2,	loop = 1, },
	{ action_name = "Move_Front",			rate = 10,	loop = 3, },
	{ action_name = "Walk_Left",			rate = 5,	loop = 1, },
	{ action_name = "Walk_Right",			rate = 5,	loop = 1, },
	{ action_name = "Move_Front",			rate = 30,	loop = 3, },
	{ action_name = "Attack008_SummonRelic_Charger",rate = 30,	loop = 1, globalcooltime=1 },
}
g_Lua_Near4 = { 
	{ action_name = "Stand_5",			    rate = 5,	loop = 1, },
	{ action_name = "Move_Front",			rate = 20,	loop = 3, },
	{ action_name = "Walk_Left",			rate = 5,	loop = 2, },
	{ action_name = "Walk_Right",			rate = 5,	loop = 2, },
	{ action_name = "Move_Front",			rate = 20,	loop = 3, },
	{ action_name = "Attack008_SummonRelic_Charger",rate = 30,	loop = 1, globalcooltime=1 },
}
g_Lua_Near5 = { 
	{ action_name = "Move_Front",			rate = 20,	loop = 3, },
	{ action_name = "Walk_Left",			rate = 5,	loop = 1, },
	{ action_name = "Walk_Right",			rate = 5,	loop = 1, },
	{ action_name = "Move_Back",			rate = 15,	loop = 1, },
	{ action_name = "Assault",			rate = 30,	loop = 1, },
}
g_Lua_Assault = { 
	{ action_name = "CustomAction4",		rate = 10,	loop = 1,  },
	{ action_name = "Attack001_Bash",		rate = 10,	loop = 1, },
	{ action_name = "Attack002_BackNFrontChopping",	rate = 10,	loop = 1,},
}
g_Lua_BeHitSkill = { 
   { lua_skill_index = 4,   rate = 100,   skill_index = 30315  },
}
g_Lua_Skill = { 
   { skill_index = 30312,  cooltime = 30000, rate = 60,   rangemin = 0, 	rangemax = 1500,  target = 3, encountertime = 9000  },-- 헤비대쉬
   { skill_index = 30311,  cooltime = 10000, rate = 80,   rangemin = 0,     rangemax = 1000,  target = 3, encountertime = 5000 },-- 아이오브일렉트릭   
   { skill_index = 30310,  cooltime = 60000, rate = 40,   rangemin = 0, 	rangemax = 600,    target = 3, td = "FL,FR,RB,BR,BL,LB", },-- 스턴샤우트
   { skill_index = 30315,  cooltime = 40000, rate = 100, rangemin = 0, 	rangemax = 800,    target = 3, td = "FL,FR,RB,BR",	usedskill = 32453, randomtarget=1.1},-- 데스허그
   { skill_index = 30316,  cooltime = 1000,   rate = -1,   rangemin = 0, 	rangemax = 800,    target = 3, td = "FL,FR,RB,BR", },-- 데스허그2
   { skill_index = 30314,  cooltime = 40000, rate = 65, rangemin = 0, 	rangemax = 1500, target = 3, td = "FL,FR,RB,BR,BL,LB", selfhppercent=40 },
}
