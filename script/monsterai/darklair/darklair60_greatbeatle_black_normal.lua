--/genmon 400101


g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Back", rate =2, loop = 1 },
   { action_name = "Attack01_Slash", rate = 8, loop = 1, max_missradian = 15 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2 },
   { action_name = "Move_Front", rate = 16, loop = 1 },
   { action_name = "Assault", rate = 5, loop = 1  },
   { action_name = "Attack07_Swoop", rate = 13, loop = 1, randomtarget = 1.1  },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 10, loop = 1, approach = 200 },
   { action_name = "Attack01_Slash", rate = 8, loop = 1, max_missradian = 15, approach = 200 },
   { action_name = "Attack04_Combo", rate = 8, loop = 1, max_missradian = 15, cooltime=7000 },
}
g_Lua_Skill = { 
   { skill_index = 20851,  cooltime = 22000, rate = 40, rangemin = 0, rangemax = 600, target = 3, max_missradian = 15, selfhppercent = 80 },
   -- { skill_index = 20854,  cooltime = 31000, rate = 40,rangemin = 400, rangemax = 1000, target = 3, max_missradian = 15 },
}
