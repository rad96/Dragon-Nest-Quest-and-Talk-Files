
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = 
{
     { action_name = "Stand", rate = 3, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 10, loop = 2 },
     { action_name = "Attack_SideKick", rate = 12, loop = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 3, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 15, loop = 1 },
     { action_name = "Shoot_Stand", rate = 15, loop = 1, globalcooltime = 1 },
      { action_name = "Skill_ForceExplosion", rate = 7, loop = 1, globalcooltime = 2 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Tumble_Front", rate = 10, loop = 1, globalcooltime = 3 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, globalcooltime = 3 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, globalcooltime = 3 },
     { action_name = "Shoot_Stand", rate = 15, loop = 1, globalcooltime = 1 },
      { action_name = "Skill_ForceExplosion", rate = 7, loop = 1, globalcooltime = 2 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 2 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 4 },
}

g_Lua_Skill = {
-- 타임 리스트릭션
   { skill_index = 31086,  cooltime = 50000, rate = 100, rangemin =  0, rangemax = 500,target = 3,}, 
-- 레이즈그라비티 35초
   { skill_index = 31083,  cooltime = 30000, rate = 40, rangemin =  0, rangemax = 1000,target = 3, td = "FL,FR,LF,RF",   },
-- 스펙트럼샤워 35초
   { skill_index = 31084,  cooltime = 30000, rate = 60, rangemin =  0, rangemax = 800,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", },
-- 써먼블랙홀 45000 (700)
   { skill_index = 31082,  cooltime = 30000, rate = 60, rangemin = 0, rangemax = 1000,target = 3, td = "FL,FR,LF,RF",  },
-- 써먼코멧 28초
   { skill_index = 31085,  cooltime = 20000, rate = 80, rangemin = 500, rangemax = 1000, target = 3, td = "FL,FR",  },
-- 리니어레이 15000
   { skill_index = 31081,  cooltime = 17000, rate = 40, rangemin = 0, rangemax = 800, target = 3, },
}