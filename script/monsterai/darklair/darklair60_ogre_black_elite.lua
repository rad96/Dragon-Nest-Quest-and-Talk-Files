--AiOgre_Black_Boss_Nest_Cerberos.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 23000
g_Lua_GlobalCoolTime2 = 10000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff", "!Down|!Air" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
   { action_name = "Attack1_bash_DarkLair", rate = 33, loop = 1  },
   { action_name = "Attack3_Upper_DarkLair", rate = 22, loop = 1 ,target_condition = "State1" ,globalcooltime=2, },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 7, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 1  },
   { action_name = "Attack1_bash_DarkLair", rate = 15, loop = 1  },
   { action_name = "Attack3_Upper_DarkLair", rate = 17, loop = 1,target_condition = "State1" ,globalcooltime=2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 3, loop = 1  },
   { action_name = "Attack13_Dash", rate = 25, loop = 1,target_condition = "State1",globalcooltime=1 },
   { action_name = "Assault", rate = 20, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 1  },
   { action_name = "Attack7_JumpAttack_DarkLair", rate = 25, loop = 1  },
   { action_name = "Attack13_Dash", rate = 25, loop = 1,globalcooltime=1 },
   { action_name = "Assault", rate = 30, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 1  },
   { action_name = "Attack13_Dash", rate = 60, loop = 1, globalcooltime=1 },
   { action_name = "Assault", rate = 20, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Front", rate = 30, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Assault", rate = 50, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash_DarkLair", rate = 3, loop = 1, approach = 250.0  },
}
