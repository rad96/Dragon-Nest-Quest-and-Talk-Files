--AiCalamaryMini_Red_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1600;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   -- { action_name = "Attack1_Slash", rate = 3, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 16, loop = 1 },
   -- { action_name = "Assault", rate = 3, loop = 1, max_missradian = 30 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 16, loop = 2 },
   -- { action_name = "Assault", rate = 3, loop = 1, max_missradian = 30 },
}
g_Lua_Skill = { 
   { skill_index = 32445,  cooltime = 20000, rate = 100,rangemin = 0, rangemax = 3000, target = 3 ,encountertime=5000,randomtarget = 1.1},
   -- { skill_index =32446, rate=100, cooltime=1,rangemin=0,range=5000,target=1,limitcount=1},
}
