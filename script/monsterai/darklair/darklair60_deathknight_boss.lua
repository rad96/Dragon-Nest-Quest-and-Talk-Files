--AiDeathKnight_Red_Boss_Meteorite__Purple_Boss_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Attack1_Combo", rate = 9, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 8, loop = 1, approach = 300 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 300 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 300 },
   { action_name = "Attack1_Combo", rate = 16, loop = 1, approach = 300 },
}
g_Lua_Skill = { 
   { skill_index = 30121,  cooltime = 112000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, },
   -- { skill_index = 30125,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1 },
   { skill_index = 30122,  cooltime = 31000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, randomtarget = 1 },
   { skill_index = 30123,  cooltime = 31000, rate = 60, rangemin = 0, rangemax = 1000, target = 3 },
   { skill_index = 30124,  cooltime = 22000, rate = 60, rangemin = 400, rangemax = 1200, target = 3, encountertime = 15000 },
   { skill_index = 32477,  cooltime = 100, rate = 100, rangemin = 0, rangemax = 3000, target = 3, priority=1,limitcount=1},
}
