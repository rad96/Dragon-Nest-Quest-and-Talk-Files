--AiMeteorFairyQueen_Blue_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 20000
g_Lua_GlobalCoolTime2 = 60000
g_Lua_GlobalCoolTime3 = 25000
g_Lua_GlobalCoolTime4 = 30000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 3, loop = 2 },
   { action_name = "Walk_Left", rate = 6, loop = 2 },
   { action_name = "Walk_Right", rate = 6, loop = 2 },
   { action_name = "Walk_Back", rate = 10, loop = 2 },
   { action_name = "Attack04_IceSwordAttack1", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 6, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Walk_Left", rate = 6, loop = 2 },
   { action_name = "Walk_Right", rate = 6, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 2 },
   { action_name = "Attack04_IceSwordAttack2", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 3 },
   { action_name = "Move_Front", rate = 15, loop = 3 },
   { action_name = "Assault", rate = 25, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 16, loop = 3 },
   { action_name = "Move_Left", rate = 6, loop = 2 },
   { action_name = "Move_Right", rate = 6, loop = 2 },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 8, loop = 1, approach = 300 },
   { action_name = "Walk_Left", rate = 16, loop = 1, approach = 300 },
   { action_name = "Walk_Right", rate = 16, loop = 1, approach = 300 },
   { action_name = "Attack01_Wind", rate = 25, loop = 1, approach = 300 },
}
g_Lua_Skill = {
   --암흑 탄막
   { skill_index = 33126, cooltime = 25000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, encountertime = 10000,  globalcooltime = 4},
   --레인 오브 다크니스
   { skill_index = 33113, cooltime = 25000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, encounrtertime=5000,     globalcooltime = 4},
   --암흑 장막
   --{ skill_index = 33131, cooltime = 35000, rate = 100, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 50, encountertime = 20000 },
   --텔레포트 공격4m
   { skill_index = 33121, cooltime = 20000, rate = 100, rangemin = 380, rangemax = 420, target = 3, globalcooltime = 1, randomtarget=1.1 },
   --텔레포트 공격6m
   { skill_index = 33148, cooltime = 20000, rate = 100, rangemin = 580, rangemax = 620, target = 3, globalcooltime = 1, randomtarget=1.1 },
   --텔레포트 공격8m
   { skill_index = 33149, cooltime = 20000, rate = 100, rangemin = 780, rangemax = 820, target = 3, globalcooltime = 1, randomtarget=1.1 },
   --암흑공격3
   { skill_index = 33116, cooltime = 30000, rate = 50, rangemin = 450, rangemax = 550, target = 3 },
   --암흑공격5
   { skill_index = 33118, cooltime = 30000, rate = 50, rangemin = 450, rangemax = 550, target = 3, selfhppercentrange = "51,100" },
   --암흑공격6
   { skill_index = 33119, cooltime = 30000, rate = 50, rangemin = 300, rangemax = 700, target = 3, selfhppercent = 50 },
   --근접 공격 
   { skill_index = 33150,  cooltime = 20000, rate = 50, rangemin = 0, rangemax = 200, target = 3 },
   --돌진
   { skill_index = 33151,  cooltime = 40000, rate = 100, rangemin = 300, rangemax = 600, target = 3, globalcooltime = 1, randomtarget=1.1 },	
   --다크 볼
   --{ skill_index = 33115,  cooltime = 25000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "51,100" },
   --게이트 오브 다크
   --{ skill_index = 33130,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "51,100" },
}

