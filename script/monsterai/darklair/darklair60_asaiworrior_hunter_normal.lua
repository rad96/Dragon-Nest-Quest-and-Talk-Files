--AiAsaiWorrior_Green_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 9999000
g_Lua_GlobalCoolTime2 = 9999000
g_Lua_GlobalCoolTime3 = 9999000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 14, loop = 1 },
   { action_name = "Walk_Left", rate = 12, loop = 1 },
   { action_name = "Walk_Right", rate = 12, loop = 1 },
   { action_name = "Attack2_Slash_Boss", rate = 13, loop = 1 },
   { action_name = "Attack1_Pierce_Boss", rate = 13, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 12, loop = 1 },
   { action_name = "Walk_Front", rate = 14, loop = 1 },
   { action_name = "Walk_Left", rate = 12, loop = 1 },
   { action_name = "Walk_Right", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 9, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate =14, loop = 1 },
   { action_name = "Attack8_Double_Boomerang", rate = 13, loop = 1 },
   { action_name = "Walk_Front", rate = 14, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 12, loop = 1 },
   { action_name = "Walk_Front", rate = 16, loop = 3 },
   { action_name = "Move_Front", rate = 18, loop = 3 },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Slash_Boss", rate = 10, loop = 1, approach = 150 },
   { action_name = "Attack1_Pierce_Boss", rate = 10, loop = 1, approach = 400 },
}

g_Lua_Skill = { 
   { skill_index = 20492,  cooltime = 29000, rate = 80, rangemin = 400, rangemax = 1200, target = 3, next_lua_skill_index = 1, randomtarget = 1.1 },-- �Ű浶 Attack3_Poison_Boss
   { skill_index = 20495,  cooltime = 19000, rate = 50, rangemin = 0, rangemax = 600, target = 3 },-- ����
   { skill_index = 20487,  cooltime = 13000, rate = 60, rangemin = 900, rangemax = 1500, target = 3, randomtarget = 1.1 },-- Attack6_JumpAttack_Wave
   { skill_index = 20484,  cooltime = 40000, rate = 100, rangemin = 0, rangemax = 3500, target = 3, encountertime = 25000 },-- �����м� �θ޶� Attack10_Boomerang
   { skill_index = 32465,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 3500, target = 3, Priority=1 ,limitcount=3},-- ��ȯ -- Attack3_Summon
   -- { skill_index = 20489,  cooltime = 60000, rate = 50, rangemin = 0, rangemax = 3500, target = 3, next_lua_skill_index = 1, selfhppercent = 100 },-- ȣ�� �θ޶�(�����Ͽ� ����-> �����Ͽ� ��-> ȣ�� �θ޶�)
   -- { skill_index = 20490,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3500, target = 3, next_lua_skill_index = 2 },
   -- { skill_index = 20486,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3500, target = 3, }, -- 
}