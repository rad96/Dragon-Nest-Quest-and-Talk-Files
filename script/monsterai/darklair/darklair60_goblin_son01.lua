--/genmon 400102

g_Lua_NearTableCount = 7

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 350;
g_Lua_NearValue4 = 550;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;
g_Lua_NearValue7 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop = 2  },
   { action_name = "Stand_2", rate = 10, loop = 2  },   
   -- { action_name = "Walk_Left", rate = 3, loop = 2  },
   -- { action_name = "Walk_Right", rate = 3, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Stand_2", rate = 10, loop = 2  },  
   -- { action_name = "Walk_Left", rate = 3, loop = 2  },
   -- { action_name = "Walk_Right", rate = 3, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Stand_2", rate = 10, loop = 2  },  
   -- { action_name = "Walk_Left", rate = 3, loop = 2  },
   -- { action_name = "Walk_Right", rate = 3, loop = 2  },  
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 2  },
   { action_name = "Stand_2", rate = 10, loop = 2  },  
   -- { action_name = "Walk_Left", rate = 3, loop = 2  },
   -- { action_name = "Walk_Right", rate = 3, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 5, loop = 2  },
   { action_name = "Stand_2", rate = 10, loop = 2  },  
   -- { action_name = "Walk_Left", rate = 3, loop = 2  },
   -- { action_name = "Walk_Right", rate = 3, loop = 2  },
}
g_Lua_Near6 = { 
   { action_name = "Stand", rate = 5, loop = 2  },
   { action_name = "Stand_2", rate = 10, loop = 2  },  
   -- { action_name = "Walk_Left", rate = 3, loop = 2  },
   -- { action_name = "Walk_Right", rate = 3, loop = 2  },  
}
g_Lua_Near7 = { 
   { action_name = "Stand", rate = 5, loop = 2  }, 
   { action_name = "Stand_2", rate = 10, loop = 2  },  
   -- { action_name = "Walk_Left", rate = 3, loop = 2  },
   -- { action_name = "Walk_Right", rate = 3, loop = 2  },   
}

g_Lua_Skill = { 
   { skill_index = 32461,  cooltime = 10000, rate = 100,rangemin = 0, rangemax = 4000, target = 3,encountertime=5000 },
}
