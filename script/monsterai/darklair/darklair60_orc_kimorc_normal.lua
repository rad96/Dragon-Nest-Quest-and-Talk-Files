--AiOrc_Black2_Normal.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 450;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1200;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 5, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
}
g_Lua_Skill = {
   { skill_index = 32457, cooltime = 15000, rate = 100, rangemin = 0, rangemax = 3000, target =3, encountertime=10000, globalcooltime=1,},
   { skill_index = 32458, cooltime = 15000, rate = 100, rangemin = 0, rangemax = 3000, target =3, encountertime=10000, globalcooltime=1,},
   -- { skill_index = 32456, cooltime = 15000, rate = 100, rangemin = 0, rangemax = 3000, target =3, },
}
