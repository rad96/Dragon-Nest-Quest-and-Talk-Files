--GreenDragonNest_Gate1_Golem_Blue.lua


g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 25000
g_Lua_GlobalCoolTime2 = 30000
g_Lua_GlobalCoolTime3 = 20000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   -- { action_name = "Walk_Back", rate = 15, loop = 1 },
   -- { action_name = "Walk_Left", rate = 15, loop = 1, cooltime = 15000, globalcoomtime = 3 },
   -- { action_name = "Walk_Right", rate = 15, loop = 1, cooltime = 15000, globalcoomtime = 3 },
   -- { action_name = "Attack01_Punch", rate = 10, loop = 1, cooltime = 10000 },
   -- { action_name = "Attack02_CannonPunch", rate = 10, loop = 1, cooltime = 10000 },
   -- { action_name = "Attack06_RightAttack", rate = 10, loop = 1, cooltime = 12000, td = "FR,RF,RB,BR" },
   -- { action_name = "Attack07_LeftAttack", rate = 10, loop = 1, cooltime = 12000, td = "FL,LF,BL,LB" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   -- { action_name = "Walk_Front", rate = 15, loop = 1 },
   -- { action_name = "Walk_Left", rate = 10, loop = 1, cooltime = 15000, globalcoomtime = 3 },
   -- { action_name = "Walk_Right", rate = 10, loop = 1, cooltime = 15000, globalcoomtime = 3 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   -- { action_name = "Walk_Front", rate = 20, loop = 1 },
   -- { action_name = "Assault", rate = 30, loop = 1, cooltime = 27000, selfhppercent = 95 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   -- { action_name = "Move_Front", rate = 20, loop = 1 },
}
-- g_Lua_Assault = { 
   -- { action_name = "Attack03_PunchHammer", rate = 30, loop = 1, approach = 250.0  },
-- }

g_Lua_Skill = {
   { skill_index = 33073,  cooltime = 3000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 1, },
   { skill_index = 33068,  cooltime = 15000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
}