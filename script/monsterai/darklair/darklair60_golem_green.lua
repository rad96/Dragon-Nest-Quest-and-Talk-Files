g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 5000

g_Lua_SkillProcessor = {{ skill_index = 30515, changetarget = "2000,0" },}
g_Lua_CustomAction ={ CustomAction1 = {{ "Attack8_EarthQuake_GreenDragon_Lv1", 0},},}
g_Lua_GlobalCoolTime1= 10000

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 5, loop = 1, },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Attack1_Punch_Blue_Lv1", rate = 20, loop = 1, cooltime = 3000, notusedskill="31319",globalcooltime=1},
   { action_name = "Attack17_RightHand", rate = 20, loop = 1, cooltime = 7000, notusedskill="31319",globalcooltime=1 },
   { action_name = "Attack5_HipAttack_GreenDragon", rate = 50, loop = 1, cooltime = 10000, notusedskill="31319", td = "RB,BR,BL,LB" },
   { action_name = "CustomAction1", rate = 50, loop = 1, cooltime = 20000, notusedskill="31319" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 5, loop = 1,},
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Attack17_RightHand", rate = 20, loop = 1, cooltime = 7000, notusedskill="31319" ,globalcooltime=1},
   { action_name = "Attack5_HipAttack_GreenDragon", rate = 50, loop = 1, cooltime = 10000, notusedskill="31319", td = "RB,BR,BL,LB" },
   { action_name = "CustomAction1", rate = 50, loop = 1, cooltime = 20000, notusedskill="31319" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 5, loop = 1, },
   { action_name = "Move_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Assault", rate = 20, loop = 1, cooltime = 27000, notusedskill="31319" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 7000 },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 7000 },
}
g_Lua_Assault = { 
   { action_name = "Attack17_RightHand", rate = 30, loop = 1, approach = 250.0  },
}

g_Lua_Skill = {
   { skill_index = 31314, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, usedskill ="31319" },-- ���ɾ��� Lv2 - Attack6_SpinAttack_GreenDragon_Lv2
   { skill_index = 31319, cooltime = 110000, rate = 100, rangemin = 0, rangemax = 4500, target = 1, selfhppercentrange = "0,50", notusedskill = "31319" ,next_lua_skill_index = 0},-- "�ݳ�"-Attack15_Berserke_Start_GreenDragon
   { skill_index = 31320, cooltime = 45000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, },-- ������ -Attack19_BigJumpLv3
   { skill_index = 31316, cooltime = 55000, rate = 20, rangemin = 0, rangemax = 3000, target = 3, notusedskill="31319",encountertime=20000},-- 3���� ��ġ�� -Attack18_TripleEarthQuake
   { skill_index = 31311, cooltime = 20000, rate = 30, rangemin = 0, rangemax = 1000, target = 3, td = "FL,FR,LF,RF", usedskill ="31319" ,globalcooltime=1},-- ���ָ�ġ�� Lv2 Attack1_Punch_Blue_Lv2
   { skill_index = 31312, cooltime = 20000, rate = 30, rangemin = 0, rangemax = 1500, target = 3, td = "FL,FR,LF,RF", usedskill ="31319" ,globalcooltime=2},-- �����ָ�ġ�� Lv2Attack17_RightHand_Lv2
   { skill_index = 31313, cooltime = 20000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, usedskill ="31319" },-- ������ ġ�� Lv2 Attack5_HipAttack_Lv2
   { skill_index = 31315, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,25", usedskill ="31319"  },-- ��ġ�� Lv2 -- Attack8_EarthQuake_GreenDragon_Lv2
   { skill_index = 32471, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, notusedskill="31319",encountertime=5000},-- ����� ��� LV1 Attack7_Bubble_GreenDragon_DarkLair
   { skill_index = 32472, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, usedskill="31319" }, -- ����� ��� LV2 Attack7_Bubble_GreenDragon_Lv2_DarkLair
   { skill_index = 31318, cooltime = 30000, rate = 40, rangemin = 0, rangemax = 1000, target = 3, td = "FL,FR,LF,RF",usedskill ="31319" },-- �극�� Attack3_Breath_GreenDragon
}