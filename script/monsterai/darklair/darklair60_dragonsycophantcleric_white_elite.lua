--/genmon 400110
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
   { action_name = "Attack9_WandCombo_GreenDragon", rate = 100, loop = 1, },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Attack6_ShieldChargeMB_GreenDragon", rate = 20, loop = 1, cooltime=10000},
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 2 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
}

g_Lua_Skill = {
   { skill_index = 32463, cooltime = 35000, rate = 100, rangemin = 0, rangemax = 1500, target = 3,selfhppercent=40,encountertime=20000 },--����������
}