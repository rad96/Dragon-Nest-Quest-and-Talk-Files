--/genmon 400114

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 550;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 10, loop = 1  },
   { action_name = "Attack2_bash", rate = 4, loop = 1,max_missradian=30  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack2_bash", rate = 10.5, loop = 1 ,max_missradian=30  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack2_bash", rate = 9, loop = 1  ,max_missradian=30 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Assault", rate = 12, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_bash", rate = 10, loop = 2, approach = 250.0  },
   { action_name = "Attack1_Pushed", rate = -1, loop = 1, approach = 150.0 },
}
g_Lua_Skill = { 
   { skill_index = 20071,  cooltime = 40000, rate = 30, rangemin = 0, rangemax = 500, target = 2,encountertitme=5000,selfhppercent=20},
}
