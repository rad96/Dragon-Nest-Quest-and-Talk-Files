--AiGoblin_Blue_Abyss.lua 400142

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction =
{
   CustomAction1 = 
   {{ "Attack1_Slash", 0 },{ "Walk_Back", 0 },},
}


g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Attack1_Slash", rate = 13, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 13, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "CustomAction1", rate = 8, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 8, loop = 1  },
   { action_name = "Move_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 20001,  cooltime = 15000, rate = 50, rangemin = 0, rangemax = 600, target = 3,encountertime=10000 },
   -- { skill_index = 32451,  cooltime = 15000, rate = 100, rangemin = 300, rangemax = 3000, target = 3 },
}
