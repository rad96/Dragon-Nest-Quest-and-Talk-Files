--AiGoblin_Red_Abyss.lua 400145

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 5500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 250
g_Lua_AssualtTime = 6000

g_Lua_CustomAction =
{
   CustomAction1 = 
   {{ "Attack7_Nanmu", 0 },{ "Walk_Back", 0 },},
}
g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 6, loop = 2  },
   { action_name = "Walk_Right", rate = 6, loop = 2  },
   { action_name = "Attack1_Slash", rate = 22, loop = 1,cooltime=3000  },
   -- { action_name = "CustomAction1", rate = 23, loop = 1,cooltime=5000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Front", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   -- { action_name = "Attack2_Upper", rate = 23, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 7, loop = 1  },
   { action_name = "Walk_Front", rate = 2, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   -- { action_name = "Assault", rate = 32, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 32, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand2", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 3  },
   { action_name = "Move_Front", rate = 15, loop = 3  },
   { action_name = "Assault", rate = 32, loop = 1  },
}

g_Lua_Assault = { 
   { action_name = "Stand2", rate = 5, loop = 1 },
   { action_name = "Attack1_Slash", rate = 3, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 20002,  cooltime = 10000, rate = 70, rangemin = 0, rangemax = 1200, target = 3, max_missradian = 15  },
}
