--AiMinotauros_Black_Boss_Abyss.lua
--미노 기본형, 걷는 구간이 많고, 이동할 때는 전진 달리기만 사용한다.(대쉬어택은 스킬)

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 12, loop = 1  },
   { action_name = "Walk_Right", rate = 12, loop = 1  },
   { action_name = "Walk_Back", rate = 15, loop = 1  },
   { action_name = "Attack2_Slash", rate = 20, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 12, loop = 1  },
   { action_name = "Walk_Right", rate = 12, loop = 1  },
   { action_name = "Walk_Back", rate = 7, loop = 1  },
   { action_name = "Attack1_bash", rate = 20, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 7, loop = 1  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
   { action_name = "Assault", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 25, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 25, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 3, loop = 1, approach = 250.0  },
   { action_name = "Attack3_DashAttack", rate = 8, loop = 1, approach = 500.0  },
}
