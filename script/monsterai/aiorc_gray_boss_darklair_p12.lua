--AiOrc_Gray_Boss_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 450;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1200;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Move_Back", rate = 13, loop = 1 , max_missradian = 360 },
   { action_name = "Move_Left_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Front_NotTarget", rate = 20, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Attack2_ThrowBomb", rate = 50, loop = 1 , max_missradian = 360 },
   { action_name = "Attack4_CallRat", rate = 20, loop = 1, max_missradian = 360  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  , max_missradian = 360},
   { action_name = "Move_Left_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Front_NotTarget", rate = 20, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Attack2_ThrowBomb", rate = 50, loop = 1 , max_missradian = 360 },
   { action_name = "Attack4_CallRat", rate = 20, loop = 1, max_missradian = 360  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  , max_missradian = 360},
   { action_name = "Move_Left_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Front_NotTarget", rate = 20, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Attack2_ThrowBomb_DarkLair", rate = 50, loop = 1 , max_missradian = 360 },
   { action_name = "Attack4_CallRat", rate = 20, loop = 1, max_missradian = 360  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  , max_missradian = 360},
   { action_name = "Move_Left_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Front_NotTarget", rate = 20, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 7, loop = 2  , max_missradian = 360},
   { action_name = "Attack2_ThrowBomb_DarkLair", rate = 50, loop = 1 , max_missradian = 360 },
   { action_name = "Attack4_CallRat", rate = 20, loop = 1 , max_missradian = 360 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, max_missradian = 360 },
   { action_name = "Move_Left_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Front_NotTarget", rate = 20, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Attack2_ThrowBomb_DarkLair", rate = 50, loop = 1 , max_missradian = 360 },
   { action_name = "Attack4_CallRat", rate = 20, loop = 1, max_missradian = 360  },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 1, loop = 3, max_missradian = 360 },
   { action_name = "Move_Left_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Front_NotTarget", rate = 30, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 7, loop = 2 , max_missradian = 360 },
}

