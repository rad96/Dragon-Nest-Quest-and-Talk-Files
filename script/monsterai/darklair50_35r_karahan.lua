
g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;


g_Lua_GlobalCoolTime1 = 5000
g_Lua_GlobalCoolTime2 = 4000


g_Lua_Near1 = 
{ 
     { action_name = "Stand_3", rate = 2, loop = 1 },
     { action_name = "Stand_4", rate = 8, loop = 1 },
     { action_name = "Move_Left_DarkLair50", rate = 2, loop = 1, globalcooltime = 2 },
     { action_name = "Move_Right_DarkLair50", rate = 2, loop = 1, globalcooltime = 2 },
     { action_name = "Move_Back_DarkLair50", rate = 4, loop = 1, globalcooltime = 2 },
     { action_name = "Attack01_Push_DarkLair50", rate = 12, loop = 1, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand_3", rate = 2, loop = 1 },
     { action_name = "Stand_4", rate = 8, loop = 1 },
     { action_name = "Move_Left_DarkLair50", rate = 2, loop = 1, globalcooltime = 2 },
     { action_name = "Move_Right_DarkLair50", rate = 2, loop = 1, globalcooltime = 2 },
     { action_name = "Attack02_MagicMissile_DarkLair50", rate = 24, loop = 1, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand_3", rate = 2, loop = 1 },
     { action_name = "Stand_4", rate = 4, loop = 1 },
     { action_name = "Move_Front_DarkLair50", rate = 16, loop = 2, globalcooltime = 2 },
     { action_name = "Attack02_MagicMissile_DarkLair50", rate = 8, loop = 1, globalcooltime = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand_3", rate = 2, loop = 1 },
     { action_name = "Stand_4", rate = 4, loop = 1 },
     { action_name = "Move_Front_DarkLair50", rate = 16, loop = 2, globalcooltime = 2 },
}

g_Lua_Skill=
{
-- 롤링라바 Attack04_RollingLava 
     { skill_index = 32428, cooltime = 20000, rangemin = 0, rangemax = 3000, target = 3, rate = 50 },
-- 아이스 소드 Attack05_IceSword_Start 
     { skill_index = 32431, cooltime = 12000, rangemin = 0, rangemax = 2000, target = 3, rate = 30, multipletarget = "1" },
-- 블랙홀 Attack13_BlackHole_Start 
     { skill_index = 32430, cooltime = 28000, rangemin = 0, rangemax = 3000, target = 3, rate = 80, selfhppercent = 75 },
-- 레이즈 그라비티 Attack06_RaiseGravity 40000 (켈베 그림자 함정)
     { skill_index = 32427, cooltime = 30000, rangemin = 0, rangemax = 3000, target = 3, rate = 20, multipletarget = "1", selfhppercent = 50 },
-- 아이스 배리어 Attack08_IceBarrier 
     { skill_index = 32429, cooltime = 90000, rangemin = 0, rangemax = 1500, target = 1, rate = 100, selfhppercent = 25 },
}