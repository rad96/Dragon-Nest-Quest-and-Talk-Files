-- Trap AI

g_Lua_NearTableCount = 2;
g_Lua_NearValue1 = 100.0;
g_Lua_NearValue2 = 500.0;

g_Lua_GlobalCoolTime1 = 1000
g_Lua_GlobalCoolTime2 = 1000
g_Lua_GlobalCoolTime3 = 1000

g_Lua_LookTargetNearState = 2;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;



g_Lua_Near1 = 
{ 
	{ action_name = "Stand",		rate = 2,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",		rate = 2,		loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 30635, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 100, target = 3, globalcooltime = 1},
   { skill_index = 30636, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 100, target = 3, globalcooltime = 2},
   { skill_index = 30637, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 100, target = 3, globalcooltime = 3},   
   { skill_index = 30638, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 100, target = 3, checkpassiveskill = "1710,-1", priority=1, globalcooltime = 1},
   { skill_index = 30639, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 100, target = 3, checkpassiveskill = "1710,-1", priority=1, globalcooltime = 2},
   { skill_index = 30640, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 100, target = 3, checkpassiveskill = "1710,-1", priority=1, globalcooltime = 3},
}
    