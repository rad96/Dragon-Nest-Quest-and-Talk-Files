--AiBroo_Lich_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 12, loop=1 },
   { action_name = "Walk_Back", rate = 20, loop=2 },
   { action_name = "Walk_Left", rate = 8, loop=2 },
   { action_name = "Walk_Right", rate = 8, loop=2 },
   { action_name = "Move_Back", rate = 15, loop=2 },
   { action_name = "Move_Left", rate = 7, loop=1 },
   { action_name = "Move_Right", rate = 7, loop=1 },
   { action_name = "Attack1_Lich_Soul", rate = 20, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop=1 },
   { action_name = "Walk_Back", rate = 5, loop=2 },
   { action_name = "Walk_Left", rate = 10, loop=1 },
   { action_name = "Walk_Right", rate = 10, loop=1 },
   { action_name = "Move_Back", rate = 3, loop=1 },
   { action_name = "Move_Left", rate = 10, loop=1 },
   { action_name = "Move_Right", rate = 10, loop=1 },
   { action_name = "Attack3_SoulCatcher", rate = 36, loop=1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop=1 },
   { action_name = "Move_Front", rate = 10, loop=2 },
   { action_name = "Move_Left", rate = 6, loop=1 },
   { action_name = "Move_Right", rate = 6, loop=1 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 3, loop=2 },
}
g_Lua_Skill = { 
   -- { skill_index = 33741,  cooltime = 35000, rate = -1,rangemin = 0, rangemax = 1000, target = 3,selfhppercentrange = "0,50" },
   { skill_index = 33742,  cooltime = 14000, rate = 60, rangemin = 0, rangemax = 1000, target = 3, },
   { skill_index = 33744,  cooltime = 11000, rate = 100, rangemin = 0, rangemax = 1200, target = 3 },
}
