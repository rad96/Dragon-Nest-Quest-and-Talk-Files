-- Reboot_40Lv_SeaDragon.lua 

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 700;
g_Lua_NearValue2 = 1300;
g_Lua_NearValue3 = 1800;
g_Lua_NearValue4 = 2500;
g_Lua_NearValue5 = 10000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 1000
g_Lua_AssualtTime = 5000
g_Lua_NoAggroStand = 1

g_Lua_State = {	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 2, loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 12, loop = 1, td = "LF,FL", cooltime = 10000, custom_state1 = "custom_ground" },
	{ action_name = "Turn_Left", rate = 10, loop = 2, td = "LF,LB,BL", custom_state1 = "custom_ground" },
	{ action_name = "Turn_Right", rate = 10, loop = 2, td = "RF,RB,BR", custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 10, loop = 1, td = "FR,RF", cooltime = 10000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 10, loop = 1, td = "LF,FL", cooltime = 10000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 8, loop = 1, td = "BL,LB,LF", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 8, loop = 1, td = "RF,RB,BR", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack029_GraptoThrow_Right", rate = 12, loop = 1, td = "FR,RF", cooltime = 13000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack028_GraptoThrow_Left", rate = 12, loop = 1, td = "LF,FL", cooltime = 13000, custom_state1 = "custom_ground" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12, loop = 1, td = "LB,BL,BR,RB", cooltime = 8000, custom_state1 = "custom_ground" },
}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 2, loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Turn_Left", rate = 10, loop = 2, td = "LF,LB,BL", custom_state1 = "custom_ground" },
	{ action_name = "Turn_Right", rate = 10, loop = 2, td = "RF,RB,BR", custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 10, loop = 1, td = "FR,RF", cooltime = 10000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 10, loop = 1, td = "LF,FL", cooltime = 10000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 12, loop = 1, td = "FR,FL", cooltime = 10000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8, loop = 1, td = "LF", cooltime = 13000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8, loop = 1, td = "RF", cooltime = 13000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 5, loop = 1, td = "BL,LB,LF", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 5, loop = 1, td = "RF,RB,BR", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12, loop = 1,	td = "LB,BL,BR,RB", cooltime = 8000, custom_state1 = "custom_ground" },
}

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 2, loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Turn_Left", rate = 10, loop = 2, td = "LF,LB,BL", custom_state1 = "custom_ground" },
	{ action_name = "Turn_Right", rate = 10, loop = 2, td = "RF,RB,BR", custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 15, loop = 1, td = "BL,LB,LF", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 15, loop = 1, td = "RF,RB,BR", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 8, loop = 1, td = "FR,RF", cooltime = 10000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 8, loop = 1, td = "LF,FL", cooltime = 10000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 12, loop = 1, td = "FR,FL", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8, loop = 1, td = "LF", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8, loop = 1, td = "RF", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12, loop = 1, td = "LB,BL,BR,RB", cooltime = 10000, custom_state1 = "custom_ground" },
}

g_Lua_Near4 = { 
	{ action_name = "Stand", rate = 2, loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Turn_Left", rate = 30, loop = 2, td = "LF,LB,BL", custom_state1 = "custom_ground" },
	{ action_name = "Turn_Right", rate = 30, loop = 2, td = "RF,RB,BR", custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 8, loop = 1, td = "BL,LB,LF", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 8, loop = 1, td = "RF,RB,BR", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 15, loop = 1, td = "FR,FL", cooltime = 10000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8, loop = 1, td = "FL,LF", cooltime = 13000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8, loop = 1, td = "FR,RF", cooltime = 13000, custom_state1 = "custom_ground" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 20, loop = 1, td = "LB,BL,BR,RB", cooltime = 8000, custom_state1 = "custom_ground" },
}

g_Lua_Near5 = { 
	{ action_name = "Stand", rate = 2, loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Turn_Left", rate = 30, loop = 2, td = "RF,RB,BR", custom_state1 = "custom_ground" },
	{ action_name = "Turn_Right", rate = 30, loop = 2, td = "LF,LB,BL", custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 15, loop = 1, td = "FR,FL", cooltime = 15000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8, loop = 1, td = "FL,LF", cooltime = 10000, custom_state1 = "custom_ground" },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8, loop = 1,	td = "FR,RF", cooltime = 10000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 12, loop = 1, td = "BL,LB,LF", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 12, loop = 1, td = "RF,RB,BR", cooltime = 8000, custom_state1 = "custom_ground" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12, loop = 1, td = "LB,BL,BR,RB", cooltime = 8000, custom_state1 = "custom_ground" },
}


g_Lua_BeHitSkill = {
	-- 4 : 3Phase_Attack017_DeathCoil_ChangeAxis
   { lua_skill_index = 1, rate = 100, skill_index = 301753 },
}

-- Global_CoolTime
g_Lua_GlobalCoolTime1 = 60000 -- 공중 스킬, 꼬리 잡기  

g_Lua_Skill = { 
	
-- next_lua_skill_index
	-- 0 : 5Phase_Attack029_DragonBreath4
	{ skill_index = 301760, cooltime = 1000, rate = -1, custom_state1 = "custom_fly", rangemin = 0, rangemax = 10000, target = 3 },
	-- 1 :3Phase_Attack018_DeathCoil_Loop
	{ skill_index = 301754, cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 4000, target = 3 },
	-- 2 : 4Phase_Attack023_DragonBreath2
	{ skill_index = 301756, cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 3000, target = 3 },
	-- 3 : 3Phase_Attack017_DeathCoil_ChangeAxis
	{ skill_index = 301753, cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 2000, target = 3 },
	-- 4 : Fly ( 5Phase_Attack029_DragonBreath4 )
	{ skill_index = 301766, cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 10000, target = 3, next_lua_skill_index = 0 },

-- Ai 
	-- 5Phase_Attack032_SummonStorm
	{ skill_index = 301764, cooltime = 35000, rate = 60, custom_state1 = "custom_ground", rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "10,90", td = "FL,FR", next_lua_skill_index = 3 },
	-- 5Phase_Attack030_SideBreath_Left
	{ skill_index = 301762, cooltime = 20000, rate = 60, custom_state1 = "custom_ground", rangemin = 0, rangemax = 1600, target = 3, selfhppercentrange = "5,85", td = "LF,BL,LB" },
	-- 5Phase_Attack031_SideBreath_Right
	{ skill_index = 301763, cooltime = 20000, rate = 60, custom_state1 = "custom_ground", rangemin = 0, rangemax = 1600, target = 3, selfhppercentrange = "5,85", td = "RF,RB,BR" },
	-- 3Phase_Attack019_WingsAttack
	{ skill_index = 301755, cooltime = 60000, globalcooltime = 1, rate = 120, custom_state1 = "custom_ground", rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "45,85", next_lua_skill_index = 4, limitcount = 3 },
	-- 4Phase_Attack022_Howl_Start
	{ skill_index = 301751, cooltime = 60000, globalcooltime = 1, rate = 120, custom_state1 = "custom_ground", rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "5,40", next_lua_skill_index = 2, limitcount = 2 },


}
