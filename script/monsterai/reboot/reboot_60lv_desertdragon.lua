-- ReBoot_60Lv_DesertDragon.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 3500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction =
{
	CustomAction1 = 
	{
		{ "Turn_Left", 1 },
		{ "useskill", lua_skill_index = 8, rate = 100 },
	},
	CustomAction2 = 
	{
		{ "Turn_Right", 1 },
		{ "useskill", lua_skill_index = 7, rate = 100 },
	},
}

-- GlobalCoolTime 
g_Lua_GlobalCoolTime1 = 4000 	-- _G1: 전방 공격
g_Lua_GlobalCoolTime2 = 5000 	-- _G2: 후방 공격
g_Lua_GlobalCoolTime3 = 70000 	-- _G3: 활강 공격
g_Lua_GlobalCoolTime4 = 80000	-- _G4: 블랙홀
g_Lua_GlobalCoolTime5 = 30000	-- 연속 속박 수정


g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 6, loop = 1 },
	{ action_name = "CustomAction1", rate = 50, loop = -1, cooltime = 5000, priority = 45, td = "LF,LB,BL" },
	{ action_name = "CustomAction2", rate = 50, loop = -1, cooltime = 5000, priority = 45, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 2, loop = 1 },

}

g_Lua_Skill = { 

--Next_lua_Skill_Index
	-- 0 Attack09_Rage_End_86343
	{ skill_index = 301361, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	-- 1 Attack09_Jump  	
	{ skill_index = 301369, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, next_lua_skill_index = 2 },
	-- 2 Attack03_Combo_Start 
	{ skill_index = 301351, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	-- 3 Attack20_Fly_Front
	{ skill_index = 301364, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	-- 4 Attack08_Thorn_Start
	{ skill_index = 301374, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	-- 5 Attack04_BreathFront_Hell_Start
	{ skill_index = 301365, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },

-- 1 Phase	
	-- Attack01_Slash /_GlobalCooltime1
	{ skill_index = 301366, cooltime = 4000, globalcooltime = 1, rate = 40, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange  = "31,100", priority = 10, td = "FL,FR" },
	-- Attack02_StampRight /_GlobalCooltime1
	{ skill_index = 301367, cooltime = 8000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 3000, target = 3, td = "FR,RF" },
	-- Attack02_StampLeft /_GlobalCooltime1
	{ skill_index = 301368, cooltime = 8000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange  = "81,100", priority = 10, td = "FL,LF" },
	-- Attack19_TailSmash /_GlobalCooltime2
	{ skill_index = 301373, cooltime = 8000, globalcooltime = 2, rate = 100, rangemin = 0, rangemax = 10000, target = 3, td = "BL,BR", randomtarget = "0.6,1" },
	-- Attack18_BackAttack /_GlobalCooltime2
	{ skill_index = 301378, cooltime = 8000, globalcooltime = 2, rate = 100, rangemin = 0, rangemax = 10000, target = 3, td = "BL,BR", randomtarget = "0.6,1" },
	-- Attack20_Fly_Front /_GlobalCooltime3
	{ skill_index = 301364, cooltime = 40000, globalcooltime = 3, rate = 40, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "81,90", encountertime = 25000, td = "FL,FR" },
	-- Attack06_WaveFront
	{ skill_index = 301359, cooltime = 20000, rate = 80, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "50,95", encountertime = 8000, randomtarget = 0.6, td = "LF,RF" },
	-- Attack07_WaveBack 
	{ skill_index = 301360, cooltime = 20000, rate = 80, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "50,95", encountertime = 8000, randomtarget = 0.6, td = "LB,RB" },
	-- Attack08_Thorn_Start 
	{ skill_index = 301374, cooltime = 60000, rate = 60, rangemin = 0, rangemax = 10000, target = 3, encountertime = 15000, multipletarget = "1,8,0,1" },

-- 1 Phase ▶ 2 Phase 변환
	-- Attack09_Rage_End_86343
	{ skill_index = 301362, cooltime = 72000, globalcooltime = 5, priority = 150, rate = 100, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "71,85", next_lua_skill_index = 0, limitcount = 1 },
	

-- 2 Phase 
	-- Attack13_Fly2_Start /_GlobalCooltime3
	{ skill_index = 301376, cooltime = 70000, globalcooltime = 3, rate = 130, rangemin = 0, rangemax = 10000, target = 3, multipletarget = "1,8,0,1", selfhppercentrange = "50,74" },
	-- Attack10_Trap_Front /_GlobalCooltime4
	{ skill_index = 301355, cooltime = 80000, globalcooltime = 4, rate = 130, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "0,75", td = "FL,FR", randomtarget= 0.6, next_lua_skill_index = 3 },
	-- Attack10_Trap_Right /_GlobalCooltime4
	{ skill_index = 301357, cooltime = 80000, globalcooltime = 4, rate = 130, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "0,75", td = "RF,RB", randomtarget= 0.6, next_lua_skill_index = 0 },
	-- Attack10_Trap_Left /_GlobalCooltime4
	{ skill_index = 301354, cooltime = 80000, globalcooltime = 4, rate = 130, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "0,75", td = "LF,LB", randomtarget= 0.6, next_lua_skill_index = 4 },
  -- Attack11_Sand_Start 
	{ skill_index = 301371, cooltime = 60000, globalcooltime = 5, rate = 90, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "0, 80", encountertime = 10000, randomtarget = "0.6,0,1", td = "FL,FR,LF,RF" },
	-- Attack05_TailAttack 
	{ skill_index = 301372, cooltime = 20000, rate = 90, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "0,90", td = "BL,BF,LB,RB", randomtarget = 0.6 },
	
-- 2 Phase ▶ 3 Phase 변환
	-- Attack12_Sun 
	{ skill_index = 301353, cooltime = 40000, rate = 150, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "0,50", limitcount = 1, next_lua_skill_index = 1 },

-- 3 Phase 
	-- Attack01_Slash /_GlobalCooltime1
	{ skill_index = 301366, cooltime = 10000, globalcooltime = 1, rate = 40, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "0,30", priority = 30, td = "FL,FR", next_lua_skill_index = 5 },
	-- Attack04_BreathFront_Hell_Start 
	{ skill_index = 301365, cooltime = 15000, rate = 70, rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "31,50", td = "FL,FR" },
	-- Attack13_Fly3_Start  /_GlobalCooltime3
	{ skill_index = 301377, cooltime = 70000, globalcooltime = 3, rate = 65, rangemin = 0, rangemax = 10000, target = 3, multipletarget = "1,8,0,1", selfhppercentrange = "10,49" },
	-- Attack14_BreathRound_Start_DE
	{ skill_index = 301370, cooltime = 240000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority = 100, selfhppercentrange = "0,20", limitcount = 3 },

}
