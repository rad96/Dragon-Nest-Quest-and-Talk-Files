--AiBroo_Blue_Normal.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 5000;
g_Lua_NearValue2 = 6000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_Skill = { 
-- 2,4페이즈 회오리 브레스
   { skill_index = 32161, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 3페이즈 회오리 죽이기
   { skill_index = 32173, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 3페이즈 광역 공격
   { skill_index = 32174, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 2,4페이즈 회오리 브레스 (원정대용)
   { skill_index = 301380, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
}
