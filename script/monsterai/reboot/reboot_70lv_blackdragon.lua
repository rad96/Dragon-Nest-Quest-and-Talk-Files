-- Reboot_70Lv_BlackDragon.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 9000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = 
{
	CustomAction1 = 
	{
		{ "Turn_Left", 1 },
		{ "useskill", lua_skill_index = 0, rate = 100 },
	},
	CustomAction2 = 
	{
		{ "Turn_Right", 1 },
		{ "useskill", lua_skill_index = 1, rate = 100 },
	},
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 6, loop = 1 },
	{ action_name = "CustomAction1", rate = 50, loop = 1, cooltime = 8000, priority = 45, td = "LF,LB,BL" },
	{ action_name = "CustomAction2", rate = 50, loop = 1, cooltime = 8000, priority = 45, td = "RF,RB,BR" },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_RandomSkill =
{
	{
		 -- 1.Fly_Dash
		{
			--Attack13_Fly_Dash_Start
			{ skill_index = 301385, rate = 50 }, 
			--Attack13_Fly_Dash_Start
			{ skill_index = 301386, rate = 50 },
		},
		-- 2. FlyTurnBreath
		{
			--Attack10_FlyTurnBreath_In_Start
		   { skill_index = 301382, rate = 25 }, 
			--Attack11_FlyTurnBreath_Out_Start
		   { skill_index = 301383, rate = 25 },
			--Attack10_FlyTurnBreath_In_Start
		   { skill_index = 301391, rate = 25 },
			--Attack11_FlyTurnBreath_Out_Start
		   { skill_index = 301392, rate = 25 },
		},
	}
}

-- GolbalCoolTime
g_Lua_GlobalCoolTime1 = 40000 -- HP에 따른 Stomp 공격
g_Lua_GlobalCoolTime2 = 30000 -- 블랙홀 계열
g_Lua_GlobalCoolTime3 = 45000 -- Fly 계열
g_Lua_GlobalCoolTime4 = 10000 -- 후방
g_Lua_GlobalCoolTime5 = 11000 -- 후방


g_Lua_Skill = 
{

-- Next_Lua_Skill
	--0. Attack02_Bash_Left
	{ skill_index = 301389, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
	--1. Attack03_Bash_Right
	{ skill_index = 301390, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
	--2. Attack05_ShortBreath_Left2
	{ skill_index = 301393, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
	--3. Attack05_ShortBreath_Right2
	{ skill_index = 301394, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
	--4. Attack05_ShortBreath_Front(3.8sec)
	{ skill_index = 301345, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
	--5. Attack13_Fly_Dash_Start
	{ skill_index = 301385, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
	--6. Attack001_Bite(3sec)
	{ skill_index = 301341, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
	--7. Attack04_TailBash (4sec)
	{ skill_index = 301344, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },

-- 기본 공격
	--Attack002_Bash_Left(2.4sec)
	{ skill_index = 301342, cooltime = 8000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "FL,LF", selfhppercentrange = "51,100" },
	--Attack003_Bash_Right(2.4sec)	
	{ skill_index = 301343, cooltime = 8000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "FR,RF", selfhppercentrange = "51,100" },
	--Attack001_Bite(3sec) 
	{ skill_index = 301341, cooltime = 12000, rate = 30, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", selfhppercentrange = "71,100" },
	--Attack002_Bash_Left(2.4sec)
	{ skill_index = 301342, cooltime = 8000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "FL,LF", selfhppercentrange = "0,50", next_lua_skill_index = 4 },
	--Attack003_Bash_Right(2.4sec)	
	{ skill_index = 301343, cooltime = 8000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "FR,RF", selfhppercentrange = "0,50", next_lua_skill_index = 4 },
	--Attack001_Bite(3sec) 
	{ skill_index = 301341, cooltime = 10000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "FR,RF", selfhppercentrange = "0,70", next_lua_skill_index = 4 },

-- 브레스	
	--Attack05_ShortBreath_Front(3.8sec)
	{ skill_index = 301345, cooltime = 15000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR,LF,RF", encountertime = 30000 },
	--Attack05_ShortBreath_Left2(3.8sec)
	{ skill_index = 301393, cooltime = 13000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "LF,LB", selfhppercentrange = "0,85", encountertime = 30000 },
	--Attack05_ShortBreath_Right2(3.8sec)
	{ skill_index = 301394, cooltime = 13000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "RF,RB", selfhppercentrange = "0,85", encountertime = 30000 },

-- 후방 공격
	--Attack04_TailBash (4sec)
	{ skill_index = 301344, cooltime = 10000, globalcooltime = 4, rate = 100, priority = 20, rangemin = 0, rangemax = 3000, target = 3, td = "BL,BR,RL", encountertime = 7000, next_lua_skill_index = 2  },
	{ skill_index = 301344, cooltime = 10000, globalcooltime = 4, rate = 100, priority = 20, rangemin = 0, rangemax = 3000, target = 3, td = "BL,BR,RB", encountertime = 7000, next_lua_skill_index = 3  },

-- 범위 공격
	--Attack06_Meteor_Start(9sec)
	{ skill_index = 301348, cooltime = 45000, rate = 70, rangemin = 0, rangemax = 6000, target = 3, selfhppercentrange = "0,80", encountertime = 7000, multipletarget = "1,8,0,1" },
	--Attack09_Fly_Stomp 34878(10.75sec)
	{ skill_index = 301381, cooltime = 80000, globalcooltime = 3, rate = 50, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,90", encountertime = 30000, limitcount = 1 },
	--Attack07_Claw_Start_DE
	{ skill_index = 301349, cooltime = 35000, rate = 130, rangemin = 0, rangemax = 9000, target = 3, selfhppercentrange = "0,90", encountertime = 30000, randomtarget = 0.6, limitcount = 8 },

--Attack13_Fly_Dash_Start (Land - 1)(10.5 / 16.5sec)
	{ skill_index = 301385, cooltime = 60000, rate = 70, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "83,93", encountertime = 7000, randomtarget = "1.6,0,1" },

-- 블랙홀
	--Attack14_BlackHole_Start(11sec)
	{ skill_index = 301387, cooltime = 30000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,80", td = "FR,FL",next_lua_skill_index = 6 },	
	{ skill_index = 301387, cooltime = 30000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,80", td = "RF,FB",next_lua_skill_index = 3 },	
	{ skill_index = 301387, cooltime = 30000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,80", td = "LF,LB",next_lua_skill_index = 2 },	
	{ skill_index = 301387, cooltime = 30000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,80", td = "BL,BR",next_lua_skill_index = 7 },	
	--Attack14_BlackHole_Start_Cross
	{ skill_index = 301388, cooltime = 30000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,80", td = "FR,FL", next_lua_skill_index = 4 },
	{ skill_index = 301388, cooltime = 30000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,80", td = "RF,FB", next_lua_skill_index = 3 },
	{ skill_index = 301388, cooltime = 30000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,80", td = "LF,LB", next_lua_skill_index = 2 },
	{ skill_index = 301388, cooltime = 30000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,80", td = "BL,BR", next_lua_skill_index = 7 },	


-- 트리거와 함께하는 공격
	--Attack10_FlyTurnBreath_Out_Start(11sec)
	{ skill_index = 301391, cooltime = 45000, globalcooltime = 3, rate = 150, rangemin = 0, rangemax = 30000, target = 3, selfhppercentrange = "31,70", limitcount = 4 },
	--Attack10_FlyTurnBreath_In_Start(11sec)
	{ skill_index = 301382, cooltime = 45000, globalcooltime = 3, rate = 150, rangemin = 0, rangemax = 30000, target = 3, selfhppercentrange = "31,70", limitcount = 2 },
	--Attack12_Fear_Start (Land)(9.6sec)
	{ skill_index = 301384, cooltime = 50000, rate = 150, rangemin = 0, rangemax = 30000, target = 3, selfhppercentrange = "51,88", encountertime = 7000, limitcount = 3 },
	--Attack08_Trap_Start(35.75sec)
	{ skill_index = 301350, cooltime = 80000, rate = 150, rangemin = 0, rangemax = 30000, target = 3, selfhppercentrange = "0,30", limitcount = 2 },

}

