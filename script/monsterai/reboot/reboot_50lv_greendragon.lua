-- Reboot_50Lv_GreenDragon.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 2000;
g_Lua_NearValue2 = 3000;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_Ground" },
	{ action_name = "Attack11_Bite&Attack", rate = 20, loop = 1, td = "FL,FR,LF",  cooltime = 12000, custom_state1 = "custom_Ground" },
	{ action_name = "Attack12_Right&LeftAttack", rate = 20, loop = 1, td = "FL,FR,RF",  cooltime = 12000, custom_state1 = "custom_Ground" },
	{ action_name = "Attack55_LeftSideAttack", rate = 40, loop = 1, td = "FL,LF,LB",  cooltime = 12000, custom_state1 = "custom_Ground" },
	{ action_name = "Attack56_RightSideAttack", rate = 40, loop = 1, td = "FR,RF,RB",  cooltime = 12000, custom_state1 = "custom_Ground" },
	{ action_name = "Attack14_TailAttack", rate = 50, loop = 1, td = "RB,BR,BL,LB",  cooltime = 8000, custom_state1 = "custom_Ground" },
	{ action_name = "Attack55_LeftSideAttack", rate = 60, loop = 1, td = "LB,LF",  cooltime = 8000, custom_state1 = "custom_Ground" },
	{ action_name = "Attack56_RightSideAttack", rate = 60, loop = 1, td = "RF,RB",  cooltime = 8000, custom_state1 = "custom_Ground" },
	{ action_name = "Attack13_Stomp&Tail", rate = 40, loop = 1, cooltime = 30000, encountertime = 8000, custom_state1 = "custom_Ground" },
	{ action_name = "Attack57_PushingWind", rate = 40, loop = 1, td = "RF,LF,RB,BR,BL,LB",  cooltime = 20000, encountertime = 30000, custom_state1 = "custom_Ground"  },
	{ action_name = "Attack54_FlyLanding", rate = 30, loop = 1, cooltime = 50000,  encountertime = 20000, custom_state1 = "custom_Fly"  },

}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 100, loop = 1, custom_state1 = "custom_Ground"  },
	{ action_name = "Attack11_Bite&Attack", rate = 20, loop = 1, td = "FL,FR,LF",  cooltime = 12000, custom_state1 = "custom_Ground"  },
	{ action_name = "Attack12_Right&LeftAttack", rate = 20, loop = 1, td = "FL,FR,LF,RF",  cooltime = 12000, custom_state1 = "custom_Ground"  },
	{ action_name = "Attack14_TailAttack", rate = 50, loop = 1, td = "RB,BR,BL,LB", cooltime = 8000, custom_state1 = "custom_Ground"  },
	{ action_name = "Attack13_Stomp&Tail", rate = 40, loop = 1, cooltime = 30000, encountertime = 8000, custom_state1 = "custom_Ground"  },
	{ action_name = "Attack55_LeftSideAttack", rate = 60, loop = 1, td = "LB,LF",  cooltime = 8000, custom_state1 = "custom_Ground" },
	{ action_name = "Attack56_RightSideAttack", rate = 60, loop = 1, td = "RF,RB",  cooltime = 8000, custom_state1 = "custom_Ground" },
	{ action_name = "Attack57_PushingWind", rate = 40, loop = 1, td = "RF,LF,RB,BR,BL,LB", cooltime = 20000, encountertime = 30000, custom_state1 = "custom_Ground"  },
	{ action_name = "Attack33_CliffAttack", rate = 10, loop = 1, cooltime = 50000, encountertime = 15000, custom_state1 = "custom_Fly2"  },
	{ action_name = "Attack54_FlyLanding", rate = 30, loop = 1, cooltime = 50000, encountertime = 20000, custom_state1 = "custom_Fly"  },

}

-- GlobalCollTime
g_Lua_GlobalCoolTime1 = 30000	-- 브레스 
g_Lua_GlobalCoolTime2 = 90000	-- 미노 소환
g_Lua_GlobalCoolTime3 = 40000	-- 절벽 쿨타임
g_Lua_GlobalCoolTime4 = 30000	-- 밀고 브레스

g_Lua_Skill = {

-- next_lua_skill_index
	-- 0 : Attack52_FlyVolcano_Start
	{ skill_index = 301328,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 40000, target = 3, custom_state1 = "custom_Fly" },
	-- 1 : Attack52_FlyGlacier_Start 
	{ skill_index = 301329,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 40000, target = 3, custom_state1 = "custom_Fly" },
	-- 2 : Attack15_RightBreath
	{ skill_index = 301310,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 40000, target = 3, custom_state1 = "custom_Ground" },
	-- 3 : Attack16_LeftBreath
	{ skill_index = 301311,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 40000, target = 3, custom_state1 = "custom_Ground" },
	-- 4 : Attack34_CliffReturn
	{ skill_index = 301326,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, custom_state1 = "custom_Fly2"},

-- 미노 스켈레톤 소환 패턴
	-- Attack50_FlySummon ▶ Attack52_FlyVolcano_Start / 볼케이노 소환
	{ skill_index = 301327,  cooltime = 90000, globalcooltime = 2, rate = 100, rangemin = 0, rangemax = 40000, target = 3, selfhppercentrange = "0,30", custom_state1 = "custom_Ground", next_lua_skill_index = 0, limitcount = 3 },
	-- Attack50_FlySummon ▶ Attack52_FlyGlacier_Start / 글레이서 소환
	{ skill_index = 301327,  cooltime = 900000, globalcooltime = 2, rate = 100, rangemin = 0, rangemax = 40000, target = 3, selfhppercentrange = "0,30", custom_state1 = "custom_Ground", next_lua_skill_index = 1, limitcount = 3 },

-- 나무 소환 패턴
	-- Attack20_SummonTree_1
	{ skill_index = 301316,  cooltime = 35000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, selfhppercentrange = "0,80", td = "FL,FR,LF,RF", custom_state1 = "custom_Ground" },

-- 절벽 패턴
	-- Attack31_CliffMove
	{ skill_index = 301322,  cooltime = 100000, rate = 80, rangemin = 0, rangemax = 40000, target = 3, selfhppercentrange = "40,80", custom_state1 = "custom_Ground" },
	-- Attack35_CliffFireBall
	{ skill_index = 301323,  cooltime = 40000, rate = 80, globalcooltime = 2 ,rate = 80, rangemin = 0, rangemax = 40000, target = 3, selfhppercentrange = "36,80", custom_state1 = "custom_Fly2", next_lua_skill_index = 4 },
	-- Attack35_CliffIceWall
	{ skill_index = 301324,  cooltime = 40000, rate = 80, globalcooltime = 2, rate = 80, rangemin = 0, rangemax = 40000, target = 3, selfhppercentrange = "36,80", custom_state1 = "custom_Fly2", next_lua_skill_index = 4 },
	-- Attack30_CliffBreath_Start
--	{ skill_index = 301321,  cooltime = 120000, rate = 150, rangemin = 0, rangemax = 40000, target = 3, selfhppercentrange = "30,50", custom_state1 = "custom_Ground", limitcount = 1 },
	-- Attack36_CliffFlap
	{ skill_index = 301325,  cooltime = 60000, rate = 60, rangemin = 0, rangemax = 10000, target = 3, encountertime = 5000, selfhppercentrange = "40,80", custom_state1 = "custom_Fly2", next_lua_skill_index = 4, limitcount = 1 },


-- 브레스 공격
	-- Attack18_PoisonBreath
	{ skill_index = 301313, cooltime = 20000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 40000, target = 3, selfhppercentrange = "0,80", td = "FL,FR", custom_state1 = "custom_Ground" },
	-- Attack15_RightBreath
	{ skill_index = 301310,  cooltime = 30000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 40000, target = 3, selfhppercentrange = "71,90", td = "FR,RF", custom_state1 = "custom_Ground" },
	-- Attack16_LeftBreath
	{ skill_index = 301311,  cooltime = 30000, globalcooltime = 2, rate = 50, rangemin = 0, rangemax = 40000, target = 3, selfhppercentrange = "71,90", td = "FL,LF", custom_state1 = "custom_Ground" },

-- 밀기 공격 후 브레스
	-- Attack57_PushingWind
	{ skill_index = 301337,  cooltime = 30000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 40000, target = 3, td = "RF,RB,BR", selfhppercentrange = "5,70", custom_state1 = "custom_Ground", next_lua_skill_index = 2 },
	{ skill_index = 301337,  cooltime = 30000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 40000, target = 3, td = "LF,BL,LB", selfhppercentrange = "5,70", custom_state1 = "custom_Ground", next_lua_skill_index = 3 },

}