--AiAsaiShaman_Lich_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 250
g_Lua_AssualtTime = 4000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 7, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 32413,  cooltime = 11000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, randomtarget = 1.1, max_missradian = 30, encountertime = 5000 },
}
