-- Wraith Red Master AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 1500.0;

g_Lua_LookTargetNearState = 4;
g_Lua_WanderingDistance = 2000.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;



g_Lua_Near1 = 
{ 
	{ action_name = "Stand_1",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	{ action_name = "Move_Back",	rate = 8,		loop = 1 },
	{ action_name = "Attack1_Cut",	rate = 11, 		loop = 1 },
	{ action_name = "Attack4_BackAttack",	rate = -1,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Walk_Left",	rate = 12,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 12,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 12,		loop = 1 },
	{ action_name = "Move_Right",	rate = 12,		loop = 1 },
	{ action_name = "Move_Front",	rate = 10,		loop = 1 },
	{ action_name = "Move_Back",	rate = 8,		loop = 1 },
	{ action_name = "Attack5_DashAttack",	rate = -1,		loop = 1 },
	{ action_name = "Attack3_ThrowScythe",	rate = 9,		loop = 1 },
	{ action_name = "Assault",	rate = 7,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 7,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 8,		loop = 1 },
	{ action_name = "Move_Right",	rate = 8,		loop = 1 },
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Back",	rate = 3,		loop = 1 },
	{ action_name = "Attack3_ThrowScythe",	rate = 9,		loop = 1 },
	{ action_name = "Assault",	rate = 5,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 10,		loop = 1 },
	{ action_name = "Move_Right",	rate = 10,		loop = 1 },
	{ action_name = "Assault",	rate = 8,		loop = 1 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Cut",	rate = 5,		loop = 1, approach = 200.0 },
	{ action_name = "Attack4_BackAttack",	rate = -1,		loop = 1, approach = 150.0 },
}