-- Orc Grey Normal AI

g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 250.0;
g_Lua_NearValue3 = 450.0;
g_Lua_NearValue4 = 900.0;
g_Lua_NearValue5 = 1200.0;
g_Lua_NearValue6 = 1500.0;


g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;


g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" },
}	

g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Back",	rate = 10,		loop = 1 },
	{ action_name = "Move_Back",	rate = 10,		loop = 1 },
	{ action_name = "Attack1_Upper",	rate = 10,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 15,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Attack1_Upper",	rate = 15,		loop = 1 },
	{ action_name = "Attack3_NanMu2",		rate = 6,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 15,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
        { action_name = "Attack1_Upper",	rate = 5,		loop = 1 },
	{ action_name = "Attack3_NanMu2",	rate = 15,		loop = 1 },
	{ action_name = "Attack4_CallRat",	rate = 10,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
	{ action_name = "Move_Back",	rate = 5,		loop = 2 },
	{ action_name = "Assault",	rate = 8,		loop = 1 },
	{ action_name = "Attack2_ThrowBomb",	rate = 20,		loop = 1 },
	{ action_name = "Attack4_CallRat",	rate = 20,		loop = 1 },
}


g_Lua_Near5 = 
{ 
	{ action_name = "Stand_1",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 1,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
	{ action_name = "Move_Front",	rate = 25,		loop = 2 },
	{ action_name = "Move_Back",	rate = 1,		loop = 2 },
	{ action_name = "Attack2_ThrowBomb",	rate = 20,		loop = 1 },
	{ action_name = "Attack4_CallRat",	rate = 20,		loop = 1 },
	{ action_name = "Assault",	rate = 5,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Stand_1",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 3 },
	{ action_name = "Move_Right",	rate = 3,		loop = 3 },
	{ action_name = "Move_Front",	rate = 30,		loop = 3 },
}

g_Lua_MeleeDefense =
{ 
	{ action_name = "Attack1_Upper",	rate = -1,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
}

g_Lua_RangeDefense =
{      
        { action_name = "Assault",	rate = 30,		loop = 1 },
	{ action_name = "Attack2_ThrowBomb",	rate = 20,		loop = 1 },
	{ action_name = "Move_Left",	rate = 1,		loop = 2 },
	{ action_name = "Move_Right",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 2 },

}

g_Lua_NonDownRangeDamage = 				
{ 				
	{ action_name = "Assault",	rate = 30,		loop = 1 },
	{ action_name = "Attack2_ThrowBomb",	rate = 20,		loop = 1 },
	{ action_name = "Move_Left",	rate = 1,		loop = 2 },
	{ action_name = "Move_Right",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 2 },
}				

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Upper",	rate = 30,		loop = 1, approach = 150.0 },
	{ action_name = "Attack3_NanMu2",	rate = 30,		loop = 1, approach = 250.0 },
}

