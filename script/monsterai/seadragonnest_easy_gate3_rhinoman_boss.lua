--AiRhinoMan_Gray_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {

  CustomAction1 = {
      { "Attack4_Pushed" },
      { "Attack10_Shot" },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_bash", rate = 10, loop = 1  },
   { action_name = "CustomAction1", rate = 15, loop = 1  },
   { action_name = "Attack14_TripleSwing_Nest", rate = 15, loop = 1  },

}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 1  },
   { action_name = "Walk_Right", rate = 8, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_bash", rate = 7, loop = 1  },
   { action_name = "Attack14_TripleSwing_Nest", rate = 20, loop = 2, selfhppercent = 50  },
   { action_name = "Attack14_TripleSwing_Nest", rate = 15, loop = 1  },
   { action_name = "Attack10_Shot", rate = 25, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 18, loop = 1  },
   { action_name = "Attack10_Shot", rate = 50, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 25, loop = 1  },
   { action_name = "Attack10_Shot", rate = 45, loop = 1  },
   { action_name = "Assault", rate = 25, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Attack10_Shot", rate = 50, loop = 1  },
   { action_name = "Assault", rate = 25, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 10, loop = 1  },
   { action_name = "Attack10_Shot", rate = 10, loop = 1  },
   { lua_skill_index = 2, rate = 40, selfhppercent = 80  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 8, loop = 1, approach = 250.0  },
   { action_name = "Attack14_TripleSwing_Nest", rate = 6, loop = 1, approach = 300.0  },
}
g_Lua_Skill = { 
-- 원형난무
   { skill_index = 30301,  cooltime = 25000, rate = 80,rangemin = 500, rangemax = 1500, td = "LF,FL,FR,RF", target = 3, selfhppercent = 25 },
-- 콜캐논
   { skill_index = 30330,  cooltime = 48000, rate = 80, rangemin = 0, rangemax = 1500, td = "LF,FL,FR,RF,RB,BR,BL,LB", multipletarget = 1, target = 3, selfhppercent = 60 },
   { skill_index = 30330,  cooltime = 25000, rate = 80, rangemin = 0, rangemax = 1500, td = "LF,FL,FR,RF,RB,BR,BL,LB", multipletarget = 1, target = 3, selfhppercent = 25 },
-- 정면난무
   { skill_index = 30303,  cooltime = 25000, rate = 90, rangemin = 500, rangemax = 1500, target = 3, selfhppercent = 80 },
}
