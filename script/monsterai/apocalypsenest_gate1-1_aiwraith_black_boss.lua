--AiWraith_Black_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack1_Cut", rate = 30, loop = 1  },
   { action_name = "Attack4_BackAttack", rate = 30, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 8, loop = 1  },
   { action_name = "Assault", rate = 20, loop = 1  },
   { action_name = "Attack2_Pain", rate = 25, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Assault", rate = 15, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 10, loop = 3  },
   { action_name = "Assault", rate = 14, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Cut", rate = 3, loop = 1, approach = 200.0  },
   { action_name = "Attack4_BackAttack", rate = 10, loop = 1, approach = 200.0  },
}
g_Lua_NonDownRangeDamage = { 
   { lua_skill_index = 1, rate = 50  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_NonDownMeleeDamage = { 
   { action_name = "Attack1_Cut", rate = 7, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },

}
g_Lua_Skill = { 
-- 페인
   { skill_index = 20180,  cooltime = 30000, rate = 80, rangemin = 300, rangemax = 1000, target = 3 },
-- 무력화
   { skill_index = 20181,  cooltime = 25000, rate = 80, rangemin = 0, rangemax = 1000, target = 3 },
}
