

g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 500.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"!Stay|!Move|!Attack", "Air", "Hit" }, 
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = {
       { "useskill", lua_skill_index = 0, rate = 100 },
	   { "Move_Back_Clone" },
  },
 }

g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
    { action_name = "Move_Front",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	--{ action_name = "Attack_AjanaDrop", rate = 100, loop = 1, target_condition = "State3", cooltime = 5000, cancellook = 0 },
        --{ action_name = "Attack1", rate = 0,             loop = 1 },
	--{ action_name = "Attack_AjanaDrop", rate = 100, loop = 1, target_condition = "State3", cooltime = 5000, cancellook = 0 },
        --{ action_name = "Attack1", rate = 0,             loop = 1 },
	--{ action_name = "Attack_AjanaDrop", rate = 100, loop = 1, target_condition = "State3", cooltime = 5000, cancellook = 0 },
        --{ action_name = "Attack1", rate = 0,             loop = 1 },		
     { action_name = "CustomAction1", rate = 50,     loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
    { action_name = "Move_Back",	rate = 5,		loop = 4 },
}

	
g_Lua_Skill =
{
	    {skill_index=262201, cooltime = 5000, rangemin = 0, rangemax = 1000, target = 2, rate = 10,  priority=1, ignoreaggro = 1},--아즈나힐링Lv1		
} 