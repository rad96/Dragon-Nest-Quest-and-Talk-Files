

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 900;
g_Lua_NearValue2 = 1300;
g_Lua_NearValue3 = 1800;
g_Lua_NearValue4 = 2500;
g_Lua_NearValue5 = 10000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 1000
g_Lua_AssualtTime = 5000
g_Lua_NoAggroStand = 1
g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_Near1 = 
{ 
-- 0 ~ 1000
	{ action_name = "Stand",		rate = 2,		custom_state1 = "custom_ground",	loop = 1 },
	{ action_name = "Turn_Left",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL" },
	{ action_name = "Turn_Right",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR" },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", cooltime = 20000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", cooltime = 20000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", cooltime = 10000 },
	{ action_name = "1Phase_Attack029_GraptoThrow_Right", rate = 12,custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", cooltime = 10000 },
	{ action_name = "1Phase_Attack028_GraptoThrow_Left", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", cooltime = 10000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	 selfhppercentrange = "0,70", cooltime = 90000 },
	{ action_name = "Fly_Stand",		rate = 30,		custom_state1 = "custom_Fly",		loop = 1,	selfhppercentrange = "0,70" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "0,40", cooltime = 10000 },
}

g_Lua_Near2 = 
{ 
-- 1000 ~ 1300
	{ action_name = "Stand",		rate = 2,		custom_state1 = "custom_ground",	loop = 1 },
	{ action_name = "Turn_Left",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL" },
	{ action_name = "Turn_Right",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR" },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", cooltime = 20000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", cooltime = 20000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 5,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 5,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", cooltime = 10000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	 selfhppercentrange = "0,70", cooltime = 90000 },
	{ action_name = "Fly_Stand",		rate = 30,		custom_state1 = "custom_Fly",		loop = 1,	selfhppercentrange = "0,70" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "0,40", cooltime = 10000 },
}

g_Lua_Near3 = 
{ 
-- 1400 ~ 1800
	{ action_name = "Stand",		rate = 2,		custom_state1 = "custom_ground",	loop = 1 },
	{ action_name = "Turn_Left",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL" },
	{ action_name = "Turn_Right",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR" },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", cooltime = 10000 },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", cooltime = 20000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8,		custom_state1 = "custom_ground",	loop = 1,	td = "LF", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF", cooltime = 20000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	 selfhppercentrange = "0,70", cooltime = 90000 },
	{ action_name = "Fly_Stand",		rate = 30,		custom_state1 = "custom_Fly",		loop = 1,	selfhppercentrange = "0,70" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "0,40", cooltime = 10000 },
}

g_Lua_Near4 = 
{ 
-- 1800 ~ 2500
	{ action_name = "Stand",		rate = 2,		custom_state1 = "custom_ground",	loop = 1 },
	{ action_name = "Turn_Left",		rate = 30,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL" },
	{ action_name = "Turn_Right",		rate = 30,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR" },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", cooltime = 10000 },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,LF", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", cooltime = 20000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	 selfhppercentrange = "0,70", cooltime = 90000 },
	{ action_name = "Fly_Stand",		rate = 30,		custom_state1 = "custom_Fly",		loop = 1,	selfhppercentrange = "0,70" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 20,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "0,40", cooltime = 10000 },
}

g_Lua_Near5 = 
{ 
-- 2500 ~ 3000
-- 페이즈3 : 거의 움직이지 않은 상태에서 가끔식 물리 공격
	{ action_name = "Stand",		rate = 2,		custom_state1 = "custom_ground",	loop = 1 },
	{ action_name = "Turn_Left",		rate = 30,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR" },
	{ action_name = "Turn_Right",		rate = 30,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,LF", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", cooltime = 20000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "40,100", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "40,100", cooltime = 10000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	 selfhppercentrange = "0,70", cooltime = 90000 },
	{ action_name = "Fly_Stand",		rate = 30,		custom_state1 = "custom_Fly",		loop = 1,	selfhppercentrange = "0,70" },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate =12,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "0,40", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "0,40", cooltime = 10000 },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "0,40", cooltime = 10000 },
}


g_Lua_BeHitSkill = { 
   { lua_skill_index = 1, rate = 100, skill_index = 30587  },
}

g_Lua_Skill = { 
-- _________조건스킬모음________
-- 드래곤브레스4
   { skill_index = 30601,  cooltime = 1000, rate = -1, custom_state1 = "custom_fly",	rangemin = 0, rangemax = 10000,target = 3 },
-- 꼬리공격_Loop
   { skill_index = 30588,  cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 4000, target = 3 },
-- 드래곤브레스1
   { skill_index = 30592,  cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 3000, target = 3 },
-- 착륙
   { skill_index = 30602,  cooltime = 1000, rate = -1, custom_state1 = "custom_fly",	rangemin = 0, rangemax = 3000, target = 3 },
-- 꼬리공격_Start
   { skill_index = 30587,  cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 2000, target = 3 },
-- 얼음감옥
   { skill_index = 30607,  cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 3000, target = 3 },
-- 드래곤브레스3-->8
   { skill_index = 30600,  cooltime = 1000, rate = -1, custom_state1 = "custom_fly",	rangemin = 0, rangemax = 10000,target = 3,  multipletarget = 1, next_lua_skill_index = 0, blowcheck = "100" },

-- 공중 스킬
-- 비늘 공격
   { skill_index = 30593,  cooltime = 1000, rate = 90,	custom_state1 = "custom_fly",	 rangemin = 0, rangemax = 10000,target = 3, selfhppercentrange = "0,45",  multipletarget = 1, next_lua_skill_index = 3 },
-- 드래곤브레스4
   { skill_index = 30601,  cooltime = 1000, rate = 70, custom_state1 = "custom_fly",	 rangemin = 0, rangemax = 10000,target = 3, selfhppercentrange = "30,100" },
-- 드래곤브레스3록온
   { skill_index = 30586,  cooltime = 1000, rate = 70,	custom_state1 = "custom_fly",	 rangemin = 0, rangemax = 10000,target = 3, selfhppercentrange = "0,30",  multipletarget = "1,4", next_lua_skill_index = 6 },

-- 지상 스킬 
-- 아이스볼트2
   { skill_index = 30598,  cooltime = 25000, rate = 70, custom_state1 = "custom_ground", rangemin = 0, rangemax = 4000, target = 3, multipletarget = "1,5" },
-- 좌측 브레스
   { skill_index = 30604,  cooltime = 36000, rate = 30, custom_state1 = "custom_ground", rangemin = 0, rangemax = 1300,	target = 3, selfhppercentrange = "0,30", td = "LF,BL,LB" },
-- 우측 브레스
   { skill_index = 30605,  cooltime = 36000, rate = 30, custom_state1 = "custom_ground", rangemin = 0, rangemax = 1300,	target = 3, selfhppercentrange = "0,30", td = "RF,RB,BR" },
-- 날개공격
   { skill_index = 30589,  cooltime = 100000, rate = 60, custom_state1 = "custom_ground", rangemin = 0, rangemax = 4000,	target = 3, selfhppercentrange = "0,80",  next_lua_skill_index = 5 },
-- 포효
   { skill_index = 30576,  cooltime = 36000, rate = 70,	custom_state1 = "custom_ground", rangemin = 0, rangemax = 3000,	target = 3, selfhppercentrange = "0,85",  next_lua_skill_index = 2, limitcount = 5  },
-- 소용돌이 소환 
   { skill_index = 30606,  cooltime = 45000, rate = 90, custom_state1 = "custom_ground", rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "40,80", td = "FL,FR,LF,RF", next_lua_skill_index = 4  },
}