-- Goblin Blue Elite Normal AI

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1500;

g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;

g_Lua_CustomAction = {
-- 근거리 도망가기
  CustomAction1 = {
      { "Walk_Back", 2 },
      { "Look_Back", 0 },
      { "Turn_Left", 0 },

  },
-- 왼쪽가다 오른쪽 감
  CustomAction2 = {
      { "Walk_Left", 2 },
      { "Turn_Right", 0 },
  },
-- 오른쪽가다 왼쪽으로 돌림
  CustomAction3 = {
      { "Walk_Right", 2 },
      { "Turn_Left", 0 },
  },
-- 근거리 도망가기
  CustomAction4 = {
      { "Walk_Back", 2 },
      { "Look_Back", 0 },
      { "Turn_Right", 0 },
  },
-- 원거리 빠르게 옆으로 가다 오른쪽
  CustomAction5 = {
      { "Move_Left", 3 },
      { "Turn_Right", 0 },
  },
  CustomAction6 = {
      { "Move_Right", 3 },
      { "Turn_Left", 0 },
  },

  CustomAction7 = {
      { "Attack1_Bite", 0 },
      { "Walk_Back", 2 },
      { "Look_Back", 0 },
      { "Turn_Left", 0 },
  },



}


g_Lua_Near1 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction1",	rate = 2,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction3",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction4",	rate = 2,		loop = 1 },
	{ action_name = "CustomAction7",	rate = 5,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "CustomAction3",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 5,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Move_Front",	rate = 5,		loop = 2 },
	{ action_name = "CustomAction3",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction6",	rate = 8,		loop = 1 },
	{ action_name = "CustomAction5",	rate = 8,		loop = 1 },
}