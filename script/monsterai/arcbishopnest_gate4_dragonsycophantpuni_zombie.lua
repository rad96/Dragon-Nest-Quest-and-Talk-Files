g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
}

g_Lua_Skill = { 
   { skill_index = 55009, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1000, target = 1, slefhppercent = "99,100" },
   { skill_index = 55004, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1000, target = 1, slefhppercent = "80,98" },
   { skill_index = 55007, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1000, target = 1, slefhppercent = "70,79" },
   { skill_index = 55008, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1000, target = 1, slefhppercent = "60,69" },
}