--AiMinotauros_Red_Boss_Nest.lua

g_Lua_NearTableCount = 5
g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1100;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2100;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 400;
g_Lua_AssualtTime = 5000;

g_Lua_GlobalCoolTime1 = 33000

g_Lua_CustomAction =
{
   CustomAction1 = 
   {{ "Attack005_Upper", 0 },{ "Attack004_SwingFire_Enchant", 0 },},
   CustomAction2 =
   {{ "Attack003_JumpAttack", 0 },{ "Attack017_Chop", 0 },},
}

g_Lua_Near1 = 
{ 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 9, loop = 1 },
   { action_name = "Walk_Right", rate = 9, loop = 1 },
   { action_name = "Attack1_bash", rate = 10, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
   { action_name = "Attack003_JumpAttack", rate = 13, loop = 1 },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Attack003_JumpAttack", rate = 15, loop = 1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
}
g_Lua_Near5 = 
{ 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 40, loop = 1 },
}



g_Lua_Skill = { 
-- Move_Front_Skill
   { skill_index = 33258, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 1300, target = 3 ,next_lua_skill_index=1},--0 무브프론트
-- Attack018_CadAttack
   { skill_index = 33259, cooltime = 3000 ,rate = -1, rangemin = 100, rangemax = 1300, target = 3 },--1 비열한공격
-- Attack019_BigJump
   { skill_index = 33260, cooltime = 50000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, next_lua_skill_index=0, globalcooltime=1,usedskill=33263},--2 대점프
-- Attack007_FlameDash
   { skill_index = 33251, cooltime = 45000, rate = 40, rangemin = 0, rangemax = 1500, target = 3, encountertime=10000, notusedskill=33263 ,},   --3 플레임대쉬
-- Attack022_FlameDash_Start
   { skill_index = 33252, cooltime = 45000, rate = 40, rangemin = 0, rangemax = 1500, target = 3, usedskill=33263},--4 플레임대쉬강화
-- Attack008_FireBreath
   { skill_index = 33253, cooltime = 17000, rate = 55, rangemin = 0, rangemax = 1000, target = 3,next_lua_skill_index=6 },--5 파이어브레스
-- Attack017_Chop_Finish
   { skill_index = 33255, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, },   --6 찍기강화
-- Attack005_Upper
   { skill_index = 33264, cooltime = 20000 ,rate = 40, rangemin = 0, rangemax = 400, target = 3,next_lua_skill_index=8 },--7 어퍼
-- Attack004_SwingFire_Enchant
   { skill_index = 33265, cooltime = 1000 ,rate = -1, rangemin = 0, rangemax = 1300, target = 3 },--8 스윙파이어인챈트
-- Attack009_EarthQuake_Volcano
   { skill_index = 33261, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, usedskill=33263, multipletarget = "1,2",},   --9 볼케이노
-- Attack016_Ifrit
   { skill_index = 33263, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 1,selfhppercentrange = "0,50",notsedskill=33263, limitcount=1 },      --11 이프리트
-- Attack014_Ignition
   { skill_index = 33257, cooltime = 60000, rate = 90, rangemin = 0, rangemax = 1500, target = 3,usedskill=33263,encountertime=10000 },      --12 이그니션
   }