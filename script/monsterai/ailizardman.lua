-- Lizardman AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 250.0;
g_Lua_NearValue2 = 500.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 1500.0;

g_Lua_LookTargetNearState = 3;

g_Lua_WanderingDistance = 1800.0;

g_Lua_PatrolBaseTime = 5000;

g_Lua_PatrolRandTime = 3000;


g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	{ action_name = "Move_Back",	rate = 15,		loop = 2 },
	{ action_name = "Attack2",	rate = 40,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 3,		loop = 2 },
	{ action_name = "Move_Left",	rate = 15,		loop = 3 },
	{ action_name = "Move_Right",	rate = 15,		loop = 3 },
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Back",	rate = 15,		loop = 2 },
	{ action_name = "Attack1",	rate = 50,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 30,		loop = 3 },
	{ action_name = "Move_Left",	rate = 20,		loop = 3 },
	{ action_name = "Move_Right",	rate = 20,		loop = 3 },
	{ action_name = "Move_Front",	rate = 30,		loop = 3 },
            { action_name = "Assault",	rate = 5,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 40,		loop = 2 },
	{ action_name = "Move_Left",	rate = 20,		loop = 3 },
	{ action_name = "Move_Right",	rate = 20,		loop = 3 },
	{ action_name = "Move_Front",	rate = 50,		loop = 3 },
            { action_name = "Assault",	rate = 5,		loop = 1 },
}

g_Lua_Defence =
{ 
	{ action_name = "Walk_Back",	rate = 1,		loop = 2 },
	{ action_name = "Move_Back",	rate = 1,		loop = 2 },
	{ action_name = "Attack2",	rate = 8,		loop = 1 },
}
