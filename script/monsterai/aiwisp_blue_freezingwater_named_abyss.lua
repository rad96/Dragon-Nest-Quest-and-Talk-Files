--AiWisp_Blue_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 1 },
   { action_name = "Attack3_BodyAttack", rate = 9, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 2  },
   { action_name = "Move_Right", rate = 20, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 25, loop = 1 },
   { action_name = "Attack3_BodyAttack", rate = 34, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 20, loop = 2  },
   { action_name = "Walk_Right", rate = 20, loop = 2  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 2  },
   { action_name = "Move_Right", rate = 20, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Assault", rate = 34, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 3  },
   { action_name = "Move_Right", rate = 20, loop = 3  },
   { action_name = "Move_Front", rate = 50, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack3_BodyAttack", rate = 5, loop = 1, cancellook = 0, approach = 200.0  },
}
g_Lua_Skill = { 
   { skill_index = 20280,  cooltime = 5000, rate = 100,rangemin = 000, rangemax = 2000, target = 1 },
   { skill_index = 20271,  cooltime = 22000, rate = 80, rangemin = 000, rangemax = 500, target = 4, hppercent = 50, },
   { skill_index = 20275,  cooltime = 12000, rate = 50, rangemin = 200, rangemax = 800, target = 3 },
}
