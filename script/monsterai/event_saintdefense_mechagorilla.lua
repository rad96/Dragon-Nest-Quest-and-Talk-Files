
g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 1300;
g_Lua_NearValue2 = 1700;
g_Lua_NearValue3 = 9000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 1300
g_Lua_AssaultTime = 5000

g_Lua_CustomAction =
{
   CustomAction1 = 
   {
	{ "Turn_Left", 1 },
 	{ "useskill", lua_skill_index = 0, rate = 100 },
   },
   CustomAction2 = 
   {
 	{ "Turn_Right", 1 },
	{ "useskill", lua_skill_index = 0, rate = 100 },
   },
}


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Turn_Left", rate = 16, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 16, loop = -1, td = "RF,RB,BR" },
   { action_name = "CustomAction1", rate = 50, loop = -1, priority = 45, td = "LF,LB,BL" },
   { action_name = "CustomAction2", rate = 50, loop = -1, priority = 45, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 32, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 16, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 16, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 32, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 16, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 16, loop = -1, td = "RF,RB,BR" },
}

g_Lua_Assault = { 
   { action_name = "Stand_2", rate = 12, loop = 1, approach = 1300 },
   { lua_skill_index = 0, rate = 6, approach = 1300 },
}

g_Lua_RandomSkill ={
{
{    { skill_index = 36616, rate = 34 },   { skill_index = 36628, rate = 33 },  { skill_index = 36629, rate = 33 },   },
{    { skill_index = 36605, rate = 34 },   { skill_index = 36626, rate = 33 },  { skill_index = 36627, rate = 33 },    },
}
}

g_Lua_GlobalCoolTime1 = 30000 --Attack03_HandAttack_Right && Attack03_HandAttack_Left && Attack03_HandAttack_Front
g_Lua_GlobalCoolTime2 = 60000 -- Attack06_UnderGround && Attack10_IronMan
g_Lua_GlobalCoolTime3 = 60000 -- Attack03_HandAttack_Rush && Attack07_HeadSpin_Rush && Attack11_Rush
g_Lua_GlobalCoolTime4 = 80000
g_Lua_GlobalCoolTime5 = 20000

g_Lua_Skill = { 
-- Attack01_Splash (HP 100 % ~ 발동)
   { skill_index = 36600, cooltime = 7000, rate = 100, rangemin = 0, rangemax = 1700, target = 3, td = "FR,FL,LF,RF", encountertime = 10000, combo1 = "20,100,0" }, --0 
-- Attack03_HandAttack_Right (HP 100 % ~ 발동)
   { skill_index = 36601, cooltime = 35000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 19000, target = 3, td = "RF,RB", randomtarget = "1.6,0,1", encountertime = 20000, combo1 = "22,100,0" }, --1
-- Attack03_HandAttack_Left (HP 100 % ~ 발동)
   { skill_index = 36610, cooltime = 35000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 19000, target = 3, td = "LF,LB", randomtarget = "1.6,0,1", encountertime = 20000, combo1 = "22,100,0" }, --2   
-- Attack03_HandAttack_Front (HP 100 % ~ 발동)
   { skill_index = 36611, cooltime = 35000, globalcooltime = 1, globalcooltime = 5, rate = 100, rangemin = 0, rangemax = 19000, target = 3, td = "FL,FR", randomtarget = "1.6,0,1", encountertime = 20000, combo1 = "22,100,0" }, --3 
-- Attack03_HandAttack_Rush (HP 70 % ~ 발동) -- EX 스킬
   { skill_index = 36612, cooltime = 60000, globalcooltime = 5, rate = 100, rangemin = 0, rangemax = 19000, target = 3, randomtarget = "1.6,0,1", encountertime = 20000, combo1 = "23,100,0" }, --4 
 
 
-- Attack05_BreastLaser (HP 90 % ~ 발동)
   { skill_index = 36602, cooltime = 85000, rate = 100,  rangemin = 0, rangemax = 19000, target = 3, td = "FR,FL,LF,RF", randomtarget = "1.6,0,1", encountertime = 40000, selfhppercentrange = "0,90", combo1 = "23,100,0" }, --5
-- Attack06_UnderGround (HP 70 % ~ 발동)
   { skill_index = 36603, cooltime = 70000, globalcooltime = 2, rate = 100, rangemin = 0, rangemax = 19000, target = 3, randomtarget = "1.6,0,1", encountertime = 60000, selfhppercentrange = "0,70", combo1 = "22,100,0" }, --6 
-- Stand_5
   { skill_index = 36622, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3 }, --7

-- Attack06_UnderGround_Teleport (HP 40 % ~ 발동) -- EX 스킬
   --{ skill_index = 36603, cooltime = 70000, globalcooltime = 3, rate = 100, rangemin = 0, rangemax = 19000, target = 3, randomtarget = "1.6,0,1", encountertime = 60000, selfhppercentrange = "0,39" },
   --{ skill_index = 36615, cooltime = 70000, rate = 100, rangemin = 0, rangemax = 19000, target = 3, randomtarget = "1.6,0,1", encountertime = 60000, selfhppercentrange = "0,39" }, --7    
   
-- Attack07_HeadSpin (HP 70 % ~ 발동)
   { skill_index = 36604, cooltime = 100000, globalcooltime = 2, rate = 100, rangemin = 0, rangemax = 19000, target = 3, randomtarget = "1.6,0,1", encountertime = 60000, selfhppercentrange = "40,70", combo1 = "20,100,0" }, --8 
-- Attack07_HeadSpin_Rush (맵 중앙 강제 이동)
   { skill_index = 36613, cooltime = 100000, globalcooltime = 4, rate = 100, rangemin = 0, rangemax = 19000, target = 3, randomtarget = "1.6,0,1", encountertime = 60000, selfhppercentrange = "0,39", combo1 = "20,100,0" }, --9 
-- Attack08_Shout
   { skill_index = 36605, cooltime = 150000, skill_random_index = 2, rate = 100, rangemin = 0, rangemax = 19000, target = 3, encountertime = 60000, selfhppercentrange = "0,90", combo1 = "20,100,0" }, --10
-- Attack09_Supporting_Fire (HP 80 % ~ 발동)
   { skill_index = 36606, cooltime = 100000, rate = 100, rangemin = 0, rangemax = 19000, target = 3, encountertime = 60000, selfhppercentrange = "0,80", combo1 = "22,100,0" }, --11 
-- Attack10_IronMan (HP 60 % ~ 발동)
   { skill_index = 36607 , cooltime = 90000, rate = 100, rangemin = 0, rangemax = 19000, target = 3, randomtarget = "1.6,0,1", encountertime = 60000, selfhppercentrange = "0,60", combo1 = "22,100,0" }, --12
-- Attack11_Rush (HP 80 % ~ 발동)
   { skill_index = 36608, cooltime = 150000, globalcooltime = 4, rate = 100, rangemin = 0, rangemax = 19000, target = 3, randomtarget = "1.6,0,1", encountertime = 60000, selfhppercentrange = "0,80", combo1 = "22,100,0" }, --13
-- Attack13_Move - 줄 패턴 콤보 액션 (1)
   { skill_index = 36614, cooltime = 1000, rate = 100, priority = 95, rangemin = 0, rangemax = 19000, target = 3, encountertime = 60000, limitcount = 1, selfhppercentrange = "0,75", combo1 = "18,100,0" }, --14
   { skill_index = 36614, cooltime = 1000, rate = 100, priority = 95, rangemin = 0, rangemax = 19000, target = 3, encountertime = 60000, limitcount = 1, selfhppercentrange = "0,50", combo1 = "18,100,0" }, --15
   { skill_index = 36614, cooltime = 1000, rate = 100, priority = 95, rangemin = 0, rangemax = 19000, target = 3, encountertime = 60000, limitcount = 1, selfhppercentrange = "0,25", combo1 = "18,100,0"  }, --16
   { skill_index = 36614, cooltime = 1000, rate = 100, priority = 95, rangemin = 0, rangemax = 19000, target = 3, encountertime = 60000, limitcount = 1, selfhppercentrange = "0,12", combo1 = "18,100,0"  }, --17
-- Attack08_Shout - 줄 패턴 콤보 액션 (2)
   { skill_index = 36616, cooltime = 1000, skill_random_index = 1, rate = -1, rangemin = 0, rangemax = 19000, target = 3, combo1 = "19,100,0"  }, --18
-- Attack12_Fountain - 줄 패턴 콤보 액션 (3)
   { skill_index = 36609, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3, combo1 = "20,100,0" }, --19
-- Stand_1 -- [2초]
   { skill_index = 36617, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3 }, --20
-- Stand_2 -- [4초] 
   { skill_index = 36618, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3, combo1 = "22,100,0" }, --21
-- Stand_3
   { skill_index = 36619, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3 }, --22
-- Stand_4 -- [6초]
   { skill_index = 36620, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3, combo1 = "24,100,0" }, --23
-- Stand_5
   { skill_index = 36621, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 19000, target = 3, combo1 = "7,100,0" }, --24
}