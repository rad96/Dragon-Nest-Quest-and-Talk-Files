--AiTroll_Blue_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Attack1_Claw", rate = 12, loop = 1, target_condition = "State1" },
   { action_name = "Attack3_RollingAttackS_N", rate = 10, loop = 1, globalcoomtime=1 },
   { action_name = "Attack6_Bite", rate = 20, loop = 1, target_condition = "State1" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Attack3_RollingAttack_N", rate = 6, loop = 1, globalcoomtime=1 },
   { action_name = "Attack3_RollingAttackS_N", rate = 12, loop = 1, globalcoomtime=1 },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Front", rate = 2, loop = 2 },
   { action_name = "Move_Front", rate = 5, loop = 3 },
   { action_name = "Attack3_RollingAttack_N", rate = 10, loop = 1, globalcoomtime=1 },
   { action_name = "Attack3_RollingAttackS_N", rate = 10, loop = 1, globalcoomtime=1 },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 2, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 3  },
   { action_name = "Attack3_RollingAttack", rate = 7, loop = 1, globalcoomtime=1 },
}