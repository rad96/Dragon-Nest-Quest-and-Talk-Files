--AiSkeleton_Gold_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 10000

g_Lua_GlobalCoolTime1 = 6000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop=2 },
   { action_name = "Walk_Back", rate = 10, loop=2 },
   { action_name = "Attack1_Chop_Dark", rate = 19, loop=1 },
   { action_name = "Attack4_Upper_Dark", rate = 13, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 10, loop=1 },
   { action_name = "Walk_Right", rate = 10, loop=1 },
   { action_name = "Walk_Back", rate = 10, loop=1 },
   { action_name = "Attack5_JumpAttack_Dark", rate = 19, loop=1 },
   { action_name = "Attack1_Chop_Dark", rate = 13, loop=1 },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Front", rate = 20, loop=1 },
   { action_name = "Move_Front", rate = 10, loop=2 },
   { action_name = "Move_Left", rate = 10, loop=1 },
   { action_name = "Move_Right", rate = 10, loop=1 },
   { action_name = "Assault", rate = 13, loop=1 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 30, loop=2 },
   { action_name = "Move_Left", rate = 10, loop=1 },
   { action_name = "Move_Right", rate = 10, loop=1 },
   { action_name = "Assault", rate = 13, loop=1 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Chop_Dark", rate = 6, loop=1,cancellook = 0,approach = 250 },
   { action_name = "Attack4_Upper_Dark", rate = 10, loop=1,cancellook = 0,approach = 250 },
}
g_Lua_Skill = { 
   { skill_index = 33763,  cooltime = 60000, rate = 100,rangemin = 0, rangemax = 4000, target = 3, encountertime=10000, next_lua_skill_index=1 }, -- 스켈 소환
   { skill_index = 33761,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 1, encountertime=3000 }, -- 스켈 버프
   { skill_index = 33762,  cooltime = 30000, rate = 60, rangemin = 0, rangemax = 800, target = 3, encountertime=1500,randomtarget=1.5 }, -- 얼굴돌진
}
