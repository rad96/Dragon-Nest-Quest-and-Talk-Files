--AiDragonSycophantMagician_Black_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 1  },
   { action_name = "Move_Right", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
   { action_name = "Attack4", rate = 4, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20260,  cooltime = 8000, rate = 100, rangemin = 0, rangemax = 1000, target = 3 },
   { skill_index = 20261,  cooltime = 15000, rate = 50, rangemin = 0, rangemax = 800, target = 4, hppercent = 50 },
   { skill_index = 20262,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, encountertime = 20000 },
}
