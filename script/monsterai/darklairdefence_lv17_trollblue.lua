--DarkLairDefence_Lv17_TrollBlue.lua
--원거리에서 스킬로 근접한 후, 다시 이탈하는 것 반복(기본은 회피모드)
--공격->이탈은 짜증나니 완전 근접시에 스탠드, 좌우이동을 조금 높혀줄 것

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Move_Back", rate = 4, loop = 4  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Attack1_Claw", rate = 4, loop = 1 },
   { action_name = "Attack6_Bite", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 4  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Move_Back", rate = 1, loop = 3  },
   { action_name = "Walk_Front", rate = 2, loop = 2  },
   { action_name = "Move_Front", rate = 2, loop = 3  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Move_Left", rate = 7, loop = 3  },
   { action_name = "Move_Right", rate = 7, loop = 3  },
   { action_name = "Attack3_RollingAttackS", rate = 10, loop = 1, encountertime = 7000, target_condition = "State1"},
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 3  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 3  },
   { action_name = "Move_Right", rate = 5, loop = 3  },
   { action_name = "Attack3_RollingAttack", rate = 20, loop = 1, encountertime = 7000, target_condition = "State1"},
}