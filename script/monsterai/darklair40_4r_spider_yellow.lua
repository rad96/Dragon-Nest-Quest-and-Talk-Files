--AiSpider_Yellow_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 1  },
   { action_name = "Attack2_Bite", rate = 6, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 2  },
   { action_name = "Move_Right", rate = 2, loop = 2  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 6, loop = 6  },
}
g_Lua_Skill = { 
   { skill_index = 20090,  cooltime = 21000, rate = 100,rangemin = 100, rangemax = 600, target = 3, max_missradian = 30, encountertime = 10000 },
}
