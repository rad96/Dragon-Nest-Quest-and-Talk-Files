--AiTroll_Black_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Back", rate = 2, loop = 2  },
   { action_name = "Attack1_Claw", rate = 6, loop = 1  },
   { action_name = "Attack6_Bite", rate = 4, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
   { action_name = "Attack5_Rolling_Air", rate = 3, loop = 1  },
   { action_name = "Assault", rate = 5, loop = 1  },
   { action_name = "Attack5_Rolling_Air", rate = -1, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 7, loop = 3  },
   { action_name = "Move_Right", rate = 7, loop = 3  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
   { action_name = "Move_Back", rate = 3, loop = 3  },
   { action_name = "Attack5_Rolling_Air", rate = 8, loop = 1  },
   { action_name = "Assault", rate = 3, loop = 1  },
   { action_name = "Attack5_Rolling_Air", rate = -1, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = -1, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 3  },
   { action_name = "Move_Right", rate = 5, loop = 3  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
   { action_name = "Move_Back", rate = 1, loop = 3  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack5_Rolling_Air", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 10, loop = 1, approach = 200.0  },
   { action_name = "Attack6_Bite", rate = 5, loop = 1, approach = 200.0  },
}
