--AiDarkElf_Gray_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Down|!Attack", "Air" }, 
  State3 = {"!Stay|!Move|!Stiff|!Attack", "Stun" }, 
}

g_Lua_Near1 = { 
   { action_name = "Move_Back", rate = 25, loop = 2  },
   { action_name = "Attack1_Claw", rate = 9, loop = 1  },
   { action_name = "Attack2_Somersault", rate = 9, loop = 1  },
   { action_name = "Attack3_AerialCombo", rate = 9, loop = 1  },
   { action_name = "Attack3_AerialCombo", rate = 200, loop = 1, target_condition = "State1"  },
   { action_name = "Attack4_SickleKick", rate = 200, loop = 1, target_condition = "State2"  },
   { action_name = "Attack2_Somersault", rate = 200, loop = 1, target_condition = "State3"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 13, loop = 2  },
   { action_name = "Move_Right", rate = 13, loop = 2  },
   { action_name = "Move_Back", rate = 25, loop = 2  },
   { action_name = "Attack4_SickleKick", rate = 10, loop = 1  },
   { action_name = "Attack3_AerialCombo", rate = 10, loop = 1  },
   { action_name = "Attack3_AerialCombo", rate = 200, loop = 1, target_condition = "State1"  },
   { action_name = "Attack4_SickleKick", rate = 200, loop = 1, target_condition = "State2"  },
   { action_name = "Attack2_Somersault", rate = 200, loop = 1, target_condition = "State3"  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 6, loop = 2  },
   { action_name = "Move_Left", rate = 11, loop = 3  },
   { action_name = "Move_Right", rate = 11, loop = 3  },
   { action_name = "Move_Front", rate = 18, loop = 3  },
   { action_name = "Move_Back", rate = 21, loop = 3  },
   { action_name = "Assault", rate = 25, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 17, loop = 3  },
   { action_name = "Move_Right", rate = 17, loop = 3  },
   { action_name = "Move_Front", rate = 27, loop = 3  },
   { action_name = "Move_Back", rate = 23, loop = 2  },
   { action_name = "Assault", rate = 25, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Move_Left", rate = 23, loop = 3  },
   { action_name = "Move_Right", rate = 23, loop = 3  },
   { action_name = "Move_Front", rate = 45, loop = 3  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 30, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 5, loop = 1, approach = 150.0  },
   { action_name = "Attack2_Somersault", rate = 3, loop = 1, approach = 200.0  },
   { action_name = "Attack4_SickleKick", rate = 5, loop = 1, approach = 150.0  },
}
