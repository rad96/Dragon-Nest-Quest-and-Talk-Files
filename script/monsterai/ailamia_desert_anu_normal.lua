--AiLamia_Desert_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 100;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 20000
g_Lua_GlobalCoolTime2 = 30000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack2_TailAttack", 1 },
      { "Attack6_Combo", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 20, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 1  },
   { action_name = "Walk_Right", rate = 8, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Attack2_TailAttack", rate = 5, loop = 1  },
   { action_name = "Attack6_Combo", rate = 10, loop = 1  },
   { action_name = "Attack3_BackCut", rate = 15, loop = 1  },
   { action_name = "CustomAction1", rate = 15, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 20, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Attack2_TailAttack", rate = 10, loop = 1  },
   { action_name = "Attack6_Combo", rate = 15, loop = 1  },
   { action_name = "CustomAction1", rate = 35, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 20, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Assault", rate = 70, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 15, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Assault", rate = 70, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 12, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
}
g_Lua_RangeDefense = { 
   { action_name = "Assault", rate = 50, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 50, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack3_BackCut", rate = 10, loop = 1, approach = 100.0 },
   { action_name = "Attack6_Combo", rate = 5, loop = 1, approach = 200.0 },
}
g_Lua_Skill = { 
   { skill_index = 21201,  cooltime = 30000, rate = 40,rangemin = 0, rangemax = 400, target = 3 },
   { skill_index = 21203,  cooltime = 46000, rate = 40, rangemin = 0, rangemax = 800, target = 3, globalcooltime = 1 },
   { skill_index = 21202,  cooltime = 60000, rate = 40, rangemin = 800, rangemax = 3000, target = 3, globalcooltime = 1 },
   { skill_index = 21205,  cooltime = 60000, rate = 40, rangemin = 400, rangemax = 2500, target = 3, globalcooltime = 2 },
}
