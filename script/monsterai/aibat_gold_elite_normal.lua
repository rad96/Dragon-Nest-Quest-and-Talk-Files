--AiBat_Gold_Elite_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 20, loop=1 },
   { action_name = "Walk_Back", rate = 4, loop=1 },
   { action_name = "Attack1", rate = 4, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop=1 },
   { action_name = "Walk_Front", rate = 16, loop=1 },
   { action_name = "Walk_Left", rate = 2, loop=1 },
   { action_name = "Walk_Right", rate = 2, loop=1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 8, loop=1 },
   { action_name = "Walk_Front", rate = 16, loop=2 },
   { action_name = "Walk_Left", rate = 4, loop=1 },
   { action_name = "Walk_Right", rate = 4, loop=1 },
   { action_name = "Assault", rate = 4, loop=1 },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 20, loop=4 },
   { action_name = "Assault", rate = 9, loop=1 },
}
g_Lua_Assault = { 
   { action_name = "Attack1", rate = 8, loop = 1, approach = 50.0  },
   { action_name = "Stand_1", rate = 16, loop = 1, approach = 50.0  },
}
