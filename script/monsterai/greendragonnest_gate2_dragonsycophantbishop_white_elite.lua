--DragonSycophantBishop_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 15, loop = 1  },
   { action_name = "Attack1_Push", rate = 40, loop = 1  },
   -- { action_name = "Attack2_Spell1_Laser", rate = 15, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   -- { action_name = "Attack2_Spell1_Laser", rate = 15, loop = 1  },
   -- { action_name = "Attack3_Spell2_EraserBall", rate = 22, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   -- { action_name = "Attack3_Spell2_EraserBall", rate = 30, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Stand_2", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   -- { action_name = "Attack3_Spell2_EraserBall", rate = 40, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Stand_2", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   -- { action_name = "Move_Left", rate = 5, loop = 1  },
   -- { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 1  },
}
g_Lua_Skill = { 
	  { skill_index = 31331,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 500, target = 2, selfhppercent = 100,limitcount=1},--Aura : HP 회복 (비숍화이트)
	  { skill_index = 20426,  cooltime = 5000, rate = 30, rangemin = 0, rangemax = 1000, target = 3, },-- 라이트닝 볼트
	  { skill_index = 20428,  cooltime = 15000, rate = 30, rangemin = 0, rangemax = 1200, target = 3, },-- 불량문장_전기
	  { skill_index = 20427,  cooltime = 10000, rate = 30, rangemin = 0, rangemax = 1000, target = 3 },-- 라이트닝 필드
}
