--AiLizardman_Red_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 3 },
   { action_name = "Move_Back", rate = 10, loop = 2 },
   { action_name = "Attack1_Piercing", rate = 18, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack13_Swing_Red", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 12, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 9, loop = 2  },
   { action_name = "Move_Back", rate = 7, loop = 2  },
   { action_name = "Assault", rate = 36, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Assault", rate = 36, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Walk_Left", rate = 15, loop = 2, cancellook = 0, approach = 250.0  },
   { action_name = "Walk_Right", rate = 15, loop = 2, cancellook = 0, approach = 250.0  },
   { action_name = "Attack1_Piercing", rate = 30, loop = 2, cancellook = 0, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 20052,  cooltime = 10000, rate = 80,rangemin = 400, rangemax = 800, target = 3 },
   { skill_index = 20058,  cooltime = 15000, rate = -1, rangemin = 400, rangemax = 900, target = 3, selfhppercent = 100 },
}
