--AiWraith_Gray_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 12, loop = 1  },
   { action_name = "Attack1_Cut", rate = 4, loop = 1  },
   { action_name = "Attack4_BackAttack", rate = 4, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 6, loop = 2  },
   { action_name = "Walk_Right", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = 6, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 1  },
   { action_name = "Move_Right", rate = 8, loop = 1  },
   { action_name = "Move_Back", rate = 8, loop = 1  },
   { action_name = "Attack7_LazerFront", rate = 20, loop = 1, cooltime = 25000  },
   { action_name = "Attack8_LazerCircle_Nest", rate = 6, loop = 1, cooltime = 40000  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 8, loop = 1  },
   { action_name = "Move_Right", rate = 8, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },
   { action_name = "Attack8_LazerCircle_Nest", rate = 22, loop = 1, cooltime = 40000  },
   { action_name = "Attack3_ThrowScythe", rate = 7, loop = 1, cooltime = 15000  },
   { action_name = "Attack7_LazerFront", rate = 20, loop = 1, cooltime = 25000  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 1  },
   { action_name = "Move_Right", rate = 10, loop = 1  },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack3_ThrowScythe", rate = 15, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
}
g_Lua_NonDownMeleeDamage = { 
   { action_name = "Attack4_BackAttack", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 10, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Cut", rate = 10, loop = 1, approach = 200.0  },
   { action_name = "Attack4_BackAttack", rate = 5, loop = 1, approach = 200.0  },
}
g_Lua_Skill = { 
   { skill_index = 20186, cooltime = 120000, rangemin = 50, rangemax = 1500, target = 3, rate = 80, td = "LF,FL,FR,RF,RB,BR,BL,LB", multipletarget = 1, selfhppercent = 80 },
}
