--AiOrc_Black_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 450;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1200;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop = 1  },
   { action_name = "Stand_1", rate = 13, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Attack2_bash", rate = 1, loop = 1  },
   { action_name = "Attack3_Air", rate = 3, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 13, loop = 1  },
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Attack2_bash", rate = 2, loop = 1  },
   { action_name = "Attack3_Air", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 17, loop = 2  },
   { action_name = "Attack9_DashAttack", rate = 3, loop = 1  },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
   { action_name = "Assault", rate = 7, loop = 1  },
   { action_name = "Attack9_DashAttack", rate = 3, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
   { action_name = "Assault", rate = 22, loop = 2  },
}
g_Lua_Near6 = { 
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
}

g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 17, loop = 1, approach = 250.0  },
   { action_name = "Attack2_bash", rate = 2, loop = 2, cancellook = 0, approach = 250.0  },
   { action_name = "Attack3_Air", rate = 4, loop = 2, cancellook = 0, approach = 250.0  },
}
