--DarkLairDefence_Lv19_GoblinBone.lua
--고블린,오크,트롤 스켈레톤 3종이 동시에 나오는 곳
--고블린은 앞으로 많이 가며, 근접시 스탠드가 높은 편이다. 좌우이동이 낮으며 공격 빈도도 매우 낮다.

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Walk_Back", 2 },
      { "Attack2_Upper", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Attack1_Slash", rate = 2, loop = 1 },
   { action_name = "CustomAction1", rate = 1, loop = 1, target_condition = "State2" },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Attack1_Slash", rate = 2, loop = 1 },
   { action_name = "CustomAction1", rate = 1, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 12, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Assault", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 7, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 2  },
   { action_name = "Move_Right", rate = 2, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Stand2", rate = 4, loop = 2 },
   { action_name = "Walk_Back", rate = 1, loop = 2 },
   { action_name = "Walk_Front", rate = 15, loop = 2 },
   { action_name = "Move_Front", rate = 15, loop = 2 },
   { action_name = "Walk_Left", rate = 4, loop = 2 },
   { action_name = "Walk_Right", rate = 4, loop = 2 },
   { action_name = "Move_Left", rate = 1, loop = 2 },
   { action_name = "Move_Right", rate = 1, loop = 2 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Slash", rate = 3, loop = 1 },
}
