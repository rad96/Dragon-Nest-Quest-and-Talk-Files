-- Goblin Blue Master AI

g_Lua_NearTableCount = 7;

g_Lua_NearValue1 = 100.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;
g_Lua_NearValue7 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;



g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Move_Back",	rate = 10,		loop = 2 },
	{ action_name = "Attack1_Slash",	rate = 6,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand2",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 20,		loop = 2 },
	{ action_name = "Move_Back",	rate = 20,		loop = 2 },
	{ action_name = "Attack1_Slash",	rate = 13,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand2",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 20,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
        { action_name = "Move_Back",	rate = 20,		loop = 1 },
        { action_name = "Attack2_Upper",	rate = 13,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand2",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 1,		loop = 2 },
        { action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 5,		loop = 2 },
        { action_name = "Move_Back",	rate = 15,		loop = 2 },
	{ action_name = "Assault",	rate = 6,		loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand2",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 2,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 2 },
	{ action_name = "Assault",	rate = 4,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Stand2",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 1,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 1,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Move_Left",	rate = 6,		loop = 3 },
	{ action_name = "Move_Right",	rate = 6,		loop = 3 },
	{ action_name = "Move_Front",	rate = 10,		loop = 3 },
	{ action_name = "Assault",	rate = 4,		loop = 1 },          
}

g_Lua_Near7 = 
{ 
	{ action_name = "Stand2",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 1,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 1,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Front",	rate = 10,		loop = 3 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack2_Upper",	rate = 5,	loop = 1, approach = 200.0 },
        { action_name = "Attack1_Slash",	rate = 5,	loop = 1, approach = 100.0 },
}
      
g_Lua_Skill = 
{
	{ skill_index = 20003, sp = 0, cooltime = 8000, rangemin = 300, rangemax = 1000, target = 3, selfhppercent = 90 },
}