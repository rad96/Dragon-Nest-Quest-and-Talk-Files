--AiHopGoblin_Green_Boss_Easy.lua

g_Lua_NearTableCount = 7

g_Lua_NearValue1 = 0;
g_Lua_NearValue2 = 150;
g_Lua_NearValue3 = 300;
g_Lua_NearValue4 = 550;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;
g_Lua_NearValue7 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Stand2", 0 },
      { "Attack4_DashAttack", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Stand_1", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Stand2", rate = -1, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Bite", rate = 9, loop = 1, target_condition = "State1" },
   { action_name = "Attack2_Nanmu", rate = 12, loop = 1, target_condition = "State1" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Stand2", rate = -1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack1_Bite", rate = 5, loop = 1, target_condition = "State1" },
   { action_name = "Attack2_Nanmu", rate = 5, loop = 1, target_condition = "State1" },
   { action_name = "CustomAction1", rate = 15, loop = 1, selfhppercent = 50, max_missradian = 30   },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
   { action_name = "Stand2", rate = -1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
   { action_name = "Attack4_DashAttack", rate = 7, loop = 1, target_condition = "State1", max_missradian = 30  },
   { action_name = "CustomAction1", rate = 20, loop = 1, selfhppercent = 50, max_missradian = 30   },
   { action_name = "Assault", rate = 5, loop = 1, target_condition = "State1" },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Stand2", rate = -1, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Attack5_JumpAttack", rate = 9, loop = 1, target_condition = "State1", max_missradian = 30  },
   { action_name = "Assault", rate = 8, loop = 1, target_condition = "State1" },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
   { action_name = "Assault", rate = 15, loop = 1  },
}
g_Lua_Near7 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 8, loop = 1  },
   { action_name = "Attack4_DashAttack", rate = 17, loop = 1, max_missradian = 30   },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Bite", rate = 10, loop = 1, approach = 100.0  },
   { action_name = "Attack4_DashAttack", rate = 2, loop = 1, approach = 400.0, max_missradian = 30   },
   { action_name = "Attack2_Nanmu", rate = 8, loop = 1, approach = 150.0  },
}
