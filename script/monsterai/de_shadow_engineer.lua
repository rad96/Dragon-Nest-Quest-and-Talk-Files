
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State =  
 { 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
 State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
 }

 g_Lua_CustomAction = {
-- 에어샷 콤보
   CustomAction1 = {
       { "Skill_AirShot_1" },
       { "Skill_AirShot_2" },
   },
-- 공격 우이동 공격
   CustomAction2 = {
       { "Move_Right" },
       { "Shoot_Stand_Cannon" },
   },
-- 공격 좌이동 공격
   CustomAction3 = {
       { "Move_Left" },
       { "Shoot_Stand_Cannon" },
   },
-- 좌측덤블링어택
   CustomAction4 = {
      { "Tumble_Left" },
      { "Shoot_Stand_Cannon" },
   },
-- 우측덤블링어택
   CustomAction5 = {
      { "Tumble_Right" },
      { "Shoot_Stand_Cannon" },
   },
-- 백덤블링어택
   CustomAction5 = {
      { "Tumble_Back" },
      { "Shoot_Stand_Cannon" },
   },
 }

 g_Lua_GlobalCoolTime1 = 15000
 g_Lua_GlobalCoolTime2 = 6000
 g_Lua_GlobalCoolTime3 = 8000

g_Lua_Near1 = 
{
     { action_name = "Attack_Down", rate = 30, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Stand", rate = 2, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 10, loop = 2 },
     { action_name = "Tumble_Back", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Shoot_Stand_Cannon", rate = 5, loop = 1, cooltime = 7000, globalcooltime = 3 },     
     { action_name = "CustomAction5", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
	 { action_name = "CustomAction1", rate = 15, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 5, loop = 2 },
     { action_name = "Shoot_Stand_Cannon", rate = 15, loop = 1, cooltime = 7000, globalcooltime = 3 },
--측이동 후 공격
     { action_name = "CustomAction2", rate = 15, loop = 1, cooltime = 12000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 15, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Front", rate = 5, loop = 1 },
	 { action_name = "Move_Back", rate = 5, loop = 1 },
	 { action_name = "Shoot_Stand_Cannon", rate = 15, loop = 1, cooltime = 7000, globalcooltime = 3 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 10, loop = 2 },
	 { action_name = "Shoot_Stand_Cannon", rate = 15, loop = 1, cooltime = 7000, globalcooltime = 3 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 4 },
}

g_Lua_NonDownMeleeDamage = {
      { action_name = "CustomAction4", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "CustomAction5", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "Move_Left", rate = 10, loop = 1 },
      { action_name = "Move_Right", rate = 10, loop = 1 },
	  { action_name = "CustomAction1", rate = 20, loop = 1, globalcooltime = 1 },
}
g_Lua_NonDownRangeDamage = {
      { action_name = "Stand", rate = 2, loop = 1 },
      { action_name = "Shoot_Stand_Cannon", rate = 5, loop = 1, globalcooltime = 3 },
}

 g_Lua_Skill = { 
-- 포스아웃
    { skill_index = 31142,  cooltime = 12000, rate = 100, rangemin = 0, rangemax = 200, target = 3, globalcooltime = 2 },
-- 네이팜
    { skill_index = 31143,  cooltime = 20000, rate = 30, rangemin = 650, rangemax = 800, target = 3, globalcooltime = 2 },
-- 스턴그레네이드
    { skill_index = 31144,  cooltime = 20000, rate = 30, rangemin = 500, rangemax = 700, target = 3, globalcooltime = 2 },
-- 스패너스매싱
    { skill_index = 31145,  cooltime = 4000, rate = 80, rangemin = 0, rangemax = 200, target = 3, td = "FL,FR", globalcooltime = 2 },
-- 그라비티 그레네이드
    -- { skill_index = 31161,  cooltime = 20000, rate = 40, rangemin = 500, rangemax = 600, target = 3, td = "FL,FR", globalcooltime = 2 },
-- 마인스로어
    -- { skill_index = 31162,  cooltime = 15000, rate = 40, rangemin = 800, rangemax = 1000, target = 3, td = "FL,FR", globalcooltime = 2 },
-- 캐미컬미사일
    -- { skill_index = 31163,  cooltime = 25000, rate = 40, rangemin = 400, rangemax = 800, target = 3, globalcooltime = 2 },
-- 로켓점프
    { skill_index = 31164,  cooltime = 12000, rate = 80, rangemin = 0, rangemax = 150, target = 3, globalcooltime = 2 },
}