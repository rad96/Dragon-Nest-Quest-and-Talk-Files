--DarkLairDefence_Lv9_GhoulGray.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 2  },
   { action_name = "Attack1_ArmAttack", rate = 6, loop = 1  },
   { action_name = "Attack1_ArmAttack", rate = 9, loop = 1, encountertime = 40000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Attack1_ArmAttack", rate = 4, loop = 1  },
   { action_name = "Attack1_ArmAttack", rate = 9, loop = 1, encountertime = 40000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Move_Front", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
}