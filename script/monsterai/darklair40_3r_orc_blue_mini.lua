--AiOrc_Blue_MINI_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Attack2_bash", rate = 7, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Front", rate = 12, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 6, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 4, loop = 2 },
   { action_name = "Assault", rate = 4, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack2_bash", rate = 5, loop = 2, approach = 250.0  },
   { action_name = "Attack1_Pushed", rate = -1, loop = 1, approach = 150.0 },
}
g_Lua_Skill = { 
   { skill_index = 20077,  cooltime = 60000, rate = 100,rangemin = 0, rangemax = 500, target = 2, hppercent = 50 },
}
