
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|!Air", "Down" },
 State2 = {"!Stay|!Move|!Attack", "!Down" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Down|!Move","Air"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 신 브레이커
  CustomAction6 = {
     { "useskill", lua_skill_index = 23, rate = 100 },
  },
  -- 
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 15, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Right", rate = 5, loop = 1, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 7200, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 17600, target_condition = "State4" },
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
     { action_name = "Move_Left", rate = 20, loop = 1 },
     { action_name = "Move_Right", rate = 20, loop = 1 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 7200, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 17600, globalcooltime = 2, target_condition = "State4" },
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}

g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 3 },
}

g_Lua_Assault = { 
     --{ action_name = "SKill_Dash", rate = -1, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Tumble_Back", rate = 20, loop = 1, globalcooltime = 1 },
	 { action_name = "Skill_StandOfFaith_Guard", rate = 30, loop = 1, cooltime = 15000 },
     { action_name = "CustomAction2", rate = 10, loop = 1  },
     { action_name = "CustomAction3", rate = 10, loop = 1 },
}

g_Lua_NonDownRangeDamage = {

	 { action_name = "useskill", lua_skill_index = 26, rate = 30 },
	 { action_name = "useskill", lua_skill_index = 15, rate = 50 },
}

g_Lua_Skill = { 
-- 0라이트닝 어퍼
     { skill_index = 83501, cooltime = 12000, rate = -1, rangemin = 0, rangemax = 180, target = 3, combo1 = "5,60,1" , combo2 ="12,100,1"},-- 액션프레임끊기 nextcustomaction 시그널
-- 1힐
     { skill_index = 83502, cooltime = 72000, rate = 30, rangemin = 400, rangemax = 1200, target = 3, selfhppercent=60 },

-- 2하프 턴 킥
     { skill_index = 83503, cooltime =100, rate = 30, rangemin = 0, rangemax = 600, target = 3,priority= 2, combo1 = "37,70,1", combo2 = "14,100,0"  },-- 케릭터 어그로유지 LocktargetLook
-- 3엑스 킥
     { skill_index = 83504, cooltime = 2400, rate = 100, rangemin = 0, rangemax = 600, target = 3, combo1 = "37,100,0" , blowcheck = "23" },
-- 4스톰핑 킥	 
	 { skill_index = 83505, cooltime = 2400, rate = 50, rangemin = 0, rangemax = 100, target = 3, priority= 3,  combo1 = "5,50,1", combo2 ="15,60,1", combo3= "0,90,1", combo4 = "30,100, 1" ,target_condition = "State1"  },-- 인풋으로 발동하는 건 액션마다 테이블 다 등록
-- 5다이브 킥
     { skill_index = 83506, cooltime = 4000, rate = 10, rangemin = 0, rangemax = 300, target = 3, combo1 = "2,50,1", combo2 = "9,100,1", target_condition = "State3" },

-- 6차지 볼트
     { skill_index = 83507, cooltime = 19200, rate = 20, rangemin = 0, rangemax = 180, target = 3,  combo1 = "4,100,1", combo2 = "10,50,1", combo3 = "22,100,1", target_condition = "State3"},
	 
-- 7블록
     { skill_index = 83509, cooltime = 54000, rate = 30, rangemin = 300, rangemax = 1200, target = 3 , selfhppercent= 60},
-- 8카운터 블로우
     { skill_index = 83510, cooltime = 14400, rate = -1, rangemin = 0, rangemax = 400, target = 3  },
-- 9슬라이딩 니킥
     { skill_index = 83511, cooltime = 15600, rate = -1, rangemin = 0, rangemax = 700, target = 3, combo1 = "17,70,1", combo2 = "12,100,1" , combo3 = "37,100,0" },
-- 10쉴드 블로우
     { skill_index = 83513, cooltime = 7200, rate = 50, rangemin = 0, rangemax = 120, target = 3,priority= 3, combo1 ="2,50,1", combo2= "37,100,0", target_condition = "State2" },

	 
-- 11홀리 볼트
     { skill_index = 83514, cooltime = 26400, rate = 10, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "10,40,1", combo2 = "25,60,1", combo3= "17,70,0", combo4= "29,100,1" },
-- 12디바인 콤보
     { skill_index = 83515, cooltime = 18000, rate = -1, rangemin = 0, rangemax = 1000, target = 3,  cancellook = 1, combo1 = "37,100,1" },
-- 13디바인 콤보F
     { skill_index = 83554, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3,  cancellook = 1, combo1 = "25,100,1" },	 

	 
-- 14다이렉트 킥
     { skill_index = 83516, cooltime = 0, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "17,80,0" , combo2 = "37,90,1",combo3 = "11,100,1" ,combo4 ="23,100,0" },
-- 15쉴드 차지
     { skill_index = 83522, cooltime = 26400, rate = 20, rangemin = 0, rangemax = 1500, target = 3 , combo1= "9,50,1", combo2 = "37,100,0" },
-- 16디바인 퍼니쉬먼트
     { skill_index = 83525, cooltime = 60000, rate = 100, rangemin = 400, rangemax = 900, target = 3, selfhppercent=60 },

-- 17아머 브레이크
     { skill_index = 83526, cooltime = 12000, rate = 80, rangemin = 0, rangemax = 600, target = 3, combo1 = "37,80,1", combo2 = "26,100,1", combo3 = "15,100,0" },
-- 18컨빅션 오라
     { skill_index = 83531, cooltime = 300000, rate = -1, rangemin = 500, rangemax =900, target = 3, combo1 = "20,100,1" , combo2 = "28,90,0", combo3 = "22,100,0" },
-- 19윌오브 아이언
     { skill_index = 83532, cooltime = 60000, rate = 100, rangemin = 400, rangemax = 900, target = 3, cancellook = 1, selfhppercent=50 },	

-- 20디바인 어센션
     { skill_index = 83534, cooltime = 18000, rate = -1, rangemin = 0, rangemax = 700, target = 3 },
-- 21디바인 벤전스
     { skill_index = 83535, cooltime = 24000, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1 },
	 
-- 22홀리 크로스
     { skill_index = 83538, cooltime = 24000, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "26,80,1", combo2 = "25,20,1", combo3 = "28,30,0" },
-- 23신 브레이커
     { skill_index = 83539, cooltime = 10800, rate = 20, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "37,80,1", combo2 = "28,20,1", combo3 = "29,70,1", combo4 = "15,100,0" },
-- 24저지스 파워
     { skill_index = 83541, cooltime = 24000, rate = 20, rangemin = 400, rangemax = 900, target = 3, cancellook = 1, combo1 = "42,100,0" },
-- 25홀리 크로스EX
     { skill_index = 83542, cooltime = 24000, rate = 10, rangemin = 20, rangemax = 900, target = 3, cancellook = 1, combo1 = "30,70,1", combo2 = "15,80,0", combo3 = "27,100,0"},
	 
-- 26저지먼트 해머
     { skill_index = 83543, cooltime = 18000, rate = 30, rangemin = 0, rangemax = 1200, target = 3, combo1 = "30,60,0" , combo2 = "25,100,0"},
-- 27세크리드 해머링EX
     { skill_index = 83544, cooltime = 48000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "4,50,1", combo2 = "3,60,1", combo3 = "0,80,1", combo4 ="14,100,1", combo5= "10,50,0", combo6 = "9,100,0" },
-- 28라이트닝 차져EX
     { skill_index = 83545, cooltime = 42000, rate = 50, rangemin = 0, rangemax = 700, target = 3 , combo1 = " 25,50,1"},
-- 29스마이트EX
     { skill_index = 83546, cooltime = 36000, rate = 30, rangemin = 0, rangemax = 500, target = 3, combo1 = "37,80,1" ,combo2 = "11,100,1"},	
-- 30슬라이딩 스텝F
     { skill_index = 83547, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "2,30,0", combo2 = "15,50,0", combo3 = "23,100,0"},	
-- 31슬라이딩 스텝B
     { skill_index = 83548, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 =" 46,40,0", combo2 = "47,50,0" },--몬스터는 점프시그널이 안뎀 그래서 벨로시티시그널 써야뎀, 몬스터는 크로스헤어로 타겟잡으면 안뎀
-- 32슬라이딩 스텝L
     { skill_index = 83549, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "28,100,0" },
-- 33슬라이딩 스텝R
     { skill_index = 83550, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "35,30,0", combo2 = "10,30,0", combo3= "46,30,0", combo4= "47,30,0", combo5 = "28,40,0", combo6 = "11,100,0" },
-- 34다운구르기B
     { skill_index = 83551, cooltime = -1, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "0,60,1", combo2 = "36,40,0", combo3 = "28,100,0" },	
-- 35다운 구르기L
     { skill_index = 83553, cooltime = -1, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 ="4,100,1", combo2 = "28,100,0" },
-- 36다운구르기R
     { skill_index = 83552, cooltime = -1, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "38,100,0", combo2 = "8,100,0" },	 
	 
-- 37 평타1
     { skill_index = 83555, cooltime = 0, rate = 50, rangemin = 0, rangemax = 300, target = 3 ,combo1 = "2,20,1", combo2 = "12,40,1", combo3 = "38,100,0" },	
-- 38 평타2
     { skill_index = 83556, cooltime = 0, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "39,100,1", combo2 = "15,100,0"},	
-- 39 평타3
     { skill_index = 83557, cooltime = 0, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1= " 40,100,1",combo2 =" 41,100,0"},--몬스터는 점프시그널이 안뎀 그래서 벨로시티시그널 써야뎀, 몬스터는 크로스헤어로 타겟잡으면 안뎀
-- 40 평타4
     { skill_index = 83558, cooltime = 0, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "11,40,1", combo2 = "12,50,1", combo3 = "29,100,1" , combo4 = "9,100,0" },
-- 41 평타4P
     { skill_index = 83559, cooltime = 0, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "26,30,1", combo2 = "25,70,1",combo3 = "23,100,0"},
-- 42 하프 턴 킥P
     { skill_index = 83521, cooltime =0, rate = 20, rangemin = 0, rangemax = 200, target = 3 , combo1 = "2,100,1" , target_condition = "State3"},	 
	 
	 }