--Event_Sparta2_Goblin_Green_Boss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
  State2 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
}

g_Lua_Near1 = { 
   { action_name = "stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Attack1_Slash", rate = 10, loop = 2 },
}
g_Lua_Near2 = { 
   { action_name = "stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 2  },
   { action_name = "Walk_Right", rate = 1, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 2  },
   { action_name = "Attack2_Upper", rate = 10, loop = 1 },
   { action_name = "Attack7_Nanmu", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Left", rate = 1, loop = 1 },
   { action_name = "Walk_Right", rate = 1, loop = 1 },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 6, loop = 2  },
   { action_name = "Attack4_FastJumpAttack", rate = 12, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Left", rate = 1, loop = 3  },
   { action_name = "Walk_Right", rate = 1, loop = 3  },
   { action_name = "Move_Left", rate = 1, loop = 3  },
   { action_name = "Move_Right", rate = 1, loop = 3  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
   { action_name = "Attack8_ThrowStone", rate = 10, loop = 1, target = 3 },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Upper", rate = 5, loop = 1 },
   { action_name = "Attack1_Slash", rate = 3, loop = 1 },
}