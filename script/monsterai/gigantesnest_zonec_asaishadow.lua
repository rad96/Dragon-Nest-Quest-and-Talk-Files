--AiCharty_Shadow_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack05_Swoop", 1 },
      { "Attack06_Thrusting", 1 },
  },
  CustomAction2 = {
      { "Attack05_Swoop", 1 },
      { "Attack11_Boom", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Back", rate = 6, loop = 2  },
   { action_name = "Attack06_Thrusting", rate = -1, loop = 1, target_condition = "State1" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = -1, loop = 2  },
   { action_name = "Move_Back", rate = 2, loop = 2  },
   { action_name = "CustomAction1", rate = -1, loop = 1  },
   { action_name = "CustomAction2", rate = 5, loop = 1, selfhppercent = 40 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 2  },
   { action_name = "Move_Left", rate = 6, loop = 2  },
   { action_name = "Move_Right", rate = 6, loop = 2  },
   { action_name = "Move_Front", rate = 6, loop = 2  },
   { action_name = "Move_Back", rate = 2, loop = 2  },
   { action_name = "CustomAction1", rate = -1, loop = 1  },
   { action_name = "CustomAction2", rate = 5, loop = 1, selfhppercent = 40 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 9, loop = 2  },
   { action_name = "Walk_Back", rate = -1, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 9, loop = 2  },
   { action_name = "Move_Back", rate = -1, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Move_Left", rate = 6, loop = 2  },
   { action_name = "Move_Right", rate = 6, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 20941,  cooltime = 5000, rate = 60,loop = 1, rangemin = 50, rangemax = 300, target = 3 },
   { skill_index = 20942,  cooltime = 8000, rate = 90, loop = 1, rangemin = 0, rangemax = 300, target = 3 },
   { skill_index = 20943,  cooltime = 20000, rate = 80, loop = 1, rangemin = 200, rangemax = 600, target = 3, selfhppercent = 80 },
   { skill_index = 20944,  cooltime = 30000, rate = -1,  },
   { skill_index = 20945,  cooltime = 30000, rate = -1,  },
}
