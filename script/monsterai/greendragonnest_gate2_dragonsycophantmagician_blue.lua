--/genmon 236008

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 4000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 2 },
   { action_name = "Walk_Right", rate = 5, loop = 2 },
   { action_name = "Walk_Back", rate = 5, loop = 2 },
   { action_name = "Move_Back", rate = 15, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 2 },
   { action_name = "Walk_Right", rate = 4, loop = 2 },
   { action_name = "Move_Left", rate = 1, loop = 1 },
   { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 2 },
}
g_Lua_Skill = { 
--아이스 배리어
   { skill_index = 30949, cooltime = 20000, rate = 100, rangemin = 0, rangemax = 1500, target = 1 },
--마나번
   -- { skill_index = 20556, cooltime = 14000, rate = 30, rangemin = 0, rangemax = 600, max_missradian = 30, target = 3 },
--프리징스파이크
   -- { skill_index = 20551, cooltime = 17000, rate = 60, rangemin = 300, rangemax = 800, target = 3 },
--얼음폭탄
   { skill_index = 20552, cooltime = 30000, rate = 40, rangemin = 300, rangemax = 800, target = 3 },
--서리고리
   -- { skill_index = 30992, cooltime = 22000, rate = 40, rangemin = 0, rangemax = 800, target = 3 },
--아이시클인잭션
   -- { skill_index = 30993, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 800, target = 3 },
}
