--DarkLairDefence_Lv3_OrcGreen.lua
--Abyss기준, 
--기본공격 위주로, 방패를 들고 있기 때문에 3라운드 기준으로는 난이도가 꽤 올라간 상태
--근접은 조금 천천히(방패를 들고 있으니까, 무브보다 워크)

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 4, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Attack2_bash", rate = 6, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Attack2_bash", rate = 6, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 8, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 8, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand2", rate = 8, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 1, loop = 1 },
   { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack2_bash", rate = 30, loop = 2, cancellook = 0, approach = 250.0  },
}
