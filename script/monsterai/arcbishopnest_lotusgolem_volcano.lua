--ArcBishopNest_LotusGolem_Volcano

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 20, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 20, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 20, loop = 1  },
}

g_Lua_Skill = { 
   { skill_index = 30981,  cooltime = 3000, rate = 100,rangemin = 300, rangemax = 2000, target = 3 },
}
