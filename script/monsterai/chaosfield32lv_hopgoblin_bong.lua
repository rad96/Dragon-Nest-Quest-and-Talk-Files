--AiHopGoblin_Yellow_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Walk_Back", 0 },
      { "Attack7_GoblinNanmu", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_Bite", rate = 5, loop = 1  },
   { action_name = "CustomAction1", rate = 5, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
   { action_name = "Attack2_Nanmu", rate = 10, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 3, loop = 1  },
   { action_name = "Assault", rate = 6, loop = 1  },
}

g_Lua_Assault = { 
   { action_name = "Attack1_Bite", rate = 10, loop = 1, approach = 100.0  },
   { action_name = "Walk_Back", rate = 2, loop = 1, approach = 100.0  },
}
