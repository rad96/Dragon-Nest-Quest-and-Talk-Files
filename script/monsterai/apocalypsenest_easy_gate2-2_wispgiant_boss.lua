--AiWispGiant_WK_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 3 },
   { action_name = "Attack3_ShockWave_Nest", rate = 22, loop = 1, cooltime = 15000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 2 },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Attack3_SummonWisp_Nest", rate = 35, loop = 1, cooltime = 15000 },
   { action_name = "Attack3_ShockWave_Nest", rate = 17, loop = 1, cooltime = 15000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Attack3_SummonWisp_Nest", rate = 35, loop = 1, cooltime = 15000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Attack3_SummonWisp_Nest", rate = 35, loop = 1, cooltime = 15000 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Front", rate = 15, loop = 3, approach = 300  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
}
g_Lua_NonDownMeleeDamage = { 
   { action_name = "Move_Back", rate = 15, loop = 3  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Attack3_ShockWave_Nest", rate = 20, loop = 1, cooltime = 20000 },
}
g_Lua_Skill = { 
   { skill_index = 20283,  cooltime = 30000, rate = 100, rangemin = 000, rangemax = 2000, target = 1, notusedskill = "20283,20284" },
   { skill_index = 20284,  cooltime = 45000, rate = 100, rangemin = 000, rangemax = 2000, target = 1, notusedskill = "20283,20284" },
   { skill_index = 20291,  cooltime = 22000, rate = 80, rangemin = 000, rangemax = 500, target = 3, usedskill = 20284 },
   { skill_index = 20292,  cooltime = 22000, rate = 80, rangemin = 000, rangemax = 1500, target = 3, usedskill = 20283 },
   { skill_index = 20290,  cooltime = 30000, rate = 80, rangemin = 000, rangemax = 1000, target = 3, usedskill = 20284 },
   { skill_index = 30403,  cooltime = 18000, rate = 80, rangemin = 000, rangemax = 500, target = 3, td = "LF,FL,FR,RF,BL,BR", usedskill = 20283 },
   { skill_index = 20293,  cooltime = 22000, rate = 50, rangemin = 200, rangemax = 1500, target = 3, td = "LF,FL,FR,RF,BL,BR", multipletarget = 1, selfhppercent = 80 },
   { skill_index = 30323,  cooltime = 22000, rate = 80, rangemin = 000, rangemax = 1000, target = 1, usedskill = "20283,20284", selfhppercent = 70 },
}
