--AiHalfGolem_Stone_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 700;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 2000;
g_Lua_NearValue4 = 4000;
g_Lua_NearValue5 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_OnlyPartsDamage = 2

g_Lua_PartsProcessor = {
   { hp="60,100", ignore="166,167" },
   { hp="0,60", ignore="167", nodamage="167" },
   { hp="0,60", checkblow ="121", ignore="166", nodamage="166", active="167" },
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack03_StompRight", 0 },
      { "Attack01_BashRight", 0 },
  },
  CustomAction2 = {
      { "Attack04_StompLeft", 0 },
      { "Attack02_BashLeft", 0 },
  },
}

g_Lua_GlobalCoolTime1 = 30000


g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000, notusedskill = 30971 },
--   { action_name = "PropAttack_Punch", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 40, notusedskill = 30971 },
--   { action_name = "PropAttack_Lazer", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 20, notusedskill = 30971 },
   { action_name = "Attack05_PunchBreak", rate = 6, loop = 1, cooltime = 15000, td = "FR,FL", notusedskill = 30971, selfhppercent = 100 },
   { action_name = "CustomAction1", rate = 3, loop = 1, cooltime = 15000, td = "FR,RF,RB,BR", notusedskill = 30971, selfhppercentrange = "40,100" },
   { action_name = "CustomAction2", rate = 3, loop = 1, cooltime = 15000, td = "FL,LF,LB,BL", notusedskill = 30971, selfhppercentrange = "40,100" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000, notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Front", rate = 4, loop = 1, cooltime = 20000, td = "LF,FL,FR,RF", selfhppercentrange = "40,100", notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Left", rate = 4, loop = 1, cooltime = 20000, td = "LF,FL,LB", selfhppercentrange = "40,100", notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Right", rate = 4, loop = 1, cooltime = 20000, td = "FR,RF,RB", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "PropAttack_Punch", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 40, notusedskill = 30971 },
--   { action_name = "PropAttack_Lazer", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 20, notusedskill = 30971 },
   { action_name = "CustomAction1", rate = 3, loop = 1, cooltime = 15000, td = "FR,RF,RB,BR", notusedskill = 30971, selfhppercentrange = "40,100" },
   { action_name = "CustomAction2", rate = 3, loop = 1, cooltime = 15000, td = "FL,LF,LB,BL", notusedskill = 30971, selfhppercentrange = "40,100" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000, notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Front", rate = 7, loop = 1, cooltime = 20000, td = "LF,FL,FR,RF", selfhppercentrange = "40,100", notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Left", rate = 7, loop = 1, cooltime = 20000, td = "LF,FL,LB", selfhppercentrange = "40,100", notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Right", rate = 7, loop = 1, cooltime = 20000, td = "FR,RF,RB", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "PropAttack_Punch", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 40, notusedskill = 30971 },
--   { action_name = "PropAttack_Lazer", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 20, notusedskill = 30971 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000, notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Front", rate = 10, loop = 1, cooltime = 20000, td = "LF,FL,FR,RF", selfhppercentrange = "40,100", notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Left", rate = 10, loop = 1, cooltime = 20000, td = "LF,FL,LB", selfhppercentrange = "40,100", notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Right", rate = 10, loop = 1, cooltime = 20000, td = "FR,RF,RB", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "PropAttack_Punch", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 40, notusedskill = 30971 },
--   { action_name = "PropAttack_Lazer", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 20, notusedskill = 30971 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, cooltime = 15000, notusedskill = 30971 },
}

g_Lua_Skill = { 
-- 부위타격표시
   { skill_index = 30985,  cooltime = 9990000, rate = 100,rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 60, limitcount = 1, notusedskill = 30971 },
-- 그로기
--   { skill_index = 30971,  cooltime = 30000, rate = 100,rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 100 },
-- 어스퀘이크
   { skill_index = 30983,  cooltime = 45000, rate = 70, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 20, notusedskill = 30971 },
-- 포이즌 버스트
   { skill_index = 30977,  cooltime = 45000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 20, usedskill = 30971 },

-- 센터레이저
   { skill_index = 30979,  cooltime = 60000, rate = 70, rangemin = 500, rangemax = 4000, target = 3, selfhppercent = 40, notusedskill = 30971, globalcooltime = 1 },
-- 서먼 볼케이노
-- { skill_index = 30984,  cooltime = 75000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 40, notusedskill = 30971 },
-- 광폭화
--   { skill_index = 30974,  cooltime = 90000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 40, notusedskill = 30971 },
-- 포이즌 레인
   { skill_index = 30975,  cooltime = 30000, rate = 50, rangemin = 300, rangemax = 1000, target = 3, selfhppercent = 40, usedskill = 30971 },

-- 외치기
   { skill_index = 30973,  cooltime = 75000, rate = 50, rangemin = 300, rangemax = 4000, target = 3, selfhppercent = 60, notusedskill = 30971, globalcooltime = 1 },
-- 멀티미사일볼
   { skill_index = 30976,  cooltime = 23000, rate = 80, rangemin = 500, rangemax = 4000, target = 3, selfhppercent = 60, usedskill = 30971 },
-- 포이즌 스프링
--   { skill_index = 30978,  cooltime = 23000, rate = 80, rangemin = 500, rangemax = 4000, target = 3, selfhppercent = 60, usedskill = 30971 },

-- 서먼 락
   { skill_index = 30982,  cooltime = 45000, rate = 50, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 80, notusedskill = 30971 },
-- 포이즌 브레스
   { skill_index = 30972,  cooltime = 45000, rate = 50, rangemin = 400, rangemax = 4000, target = 3, selfhppercent = 80, notusedskill = 30971 },
-- 잡아던지기
--   { skill_index = 30980,  cooltime = 30000, rate = 80, rangemin = 500, rangemax = 800, target = 3, selfhppercent = 80, notusedskill = 30971 },
}
