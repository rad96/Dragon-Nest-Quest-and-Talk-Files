--AiTroll_Black_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2,  },
   { action_name = "Walk_Left", rate = 4,  },
   { action_name = "Walk_Right", rate = 4,  },
   { action_name = "Move_Left", rate = 1,  },
   { action_name = "Move_Right", rate = 1,  },
   { action_name = "Attack1_Claw", rate = 12,  },
   { action_name = "Attack6_Bite", rate = 4,  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3,  },
   { action_name = "Walk_Left", rate = 4,  },
   { action_name = "Walk_Right", rate = 4,  },
   { action_name = "Move_Left", rate = 1,  },
   { action_name = "Move_Right", rate = 1,  },
   { action_name = "Assault", rate = 10,  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5,  },
   { action_name = "Walk_Front", rate = 4,  },
   { action_name = "Move_Left", rate = 1,  },
   { action_name = "Move_Right", rate = 1,  },
   { action_name = "Move_Front", rate = 4,  },
   { action_name = "Assault", rate = 5,  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1,  },
   { action_name = "Walk_Front", rate = 4,  },
   { action_name = "Move_Front", rate = 8,  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 5,  },
   { action_name = "Attack6_Bite", rate = 10,  },
}
g_Lua_Skill = { 
   { skill_index = 20147, cooltime = 27000, rate = 7000,rangemin = 0, rangemax = 1200, target = 3, next_lua_skill_index = 1, randomtarget = 1.1 },
   { skill_index = 20148, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1200, target = 3 },
   { skill_index = 20149, cooltime = 30000, rate = 10000, rangemin = 300, rangemax = 1200, target = 3, randomtarget = 1.1 },
   { skill_index = 20109, cooltime = 21000, rate = 70, rangemin = 200, rangemax = 700, target = 3 },
}
