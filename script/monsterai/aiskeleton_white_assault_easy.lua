--AiSkeleton_White_Easy.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1200;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 15, loop = 1  },
   { action_name = "Move_Back", rate = 15, loop = 1  },
   { action_name = "Attack1_Chop", rate = 14, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 15, loop = 1  },
   { action_name = "Walk_Right", rate = 15, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Chop", rate = 16, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Front", rate = 15, loop = 1  },
   { action_name = "Assault", rate = 14, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 30, loop = 3  },
   { action_name = "Move_Front", rate = 35, loop = 3  },
   { action_name = "Assault", rate = 7, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 30, loop = 3  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Chop", rate = 15, loop = 1, cancellook = 0, approach = 150.0  },
}
g_Lua_Skill = { 
   { skill_index = 20046,  cooltime = 10000, rate = 90,rangemin = 000, rangemax = 250, target = 3, selfhppercent = 100, target_condition = "State1" },
   { skill_index = 20046,  cooltime = 25000, rate = 80, rangemin = 000, rangemax = 250, target = 3, selfhppercent = 100 },
   { skill_index = 20045,  cooltime = 25000, rate = 50, rangemin = 400, rangemax = 800, target = 3, selfhppercent = 100 },
   { skill_index = 20044,  cooltime = 25000, rate = 80, rangemin = 100, rangemax = 600, target = 3, selfhppercent = 100 },
}
