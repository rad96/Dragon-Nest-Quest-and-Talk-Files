--DarkLairDefence_Lv9_GhoulBlue.lua
--구울그레이와 함께 나오기 때문에 차별성을 줘야 함
--얼리는 기술 사용 안 하고 원거리 기술만 사용하게 함
--근접시 기본 공격 비율이 그레이보다 낮음

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 2  },
   { action_name = "Attack1_ArmAttack", rate = 2, loop = 1  },
   { action_name = "Attack1_ArmAttack", rate = 13, loop = 1, encountertime = 40000 },

}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Attack1_ArmAttack", rate = 2, loop = 1  },
   { action_name = "Attack1_ArmAttack", rate = 13, loop = 1, encountertime = 40000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 20023,  cooltime = 24000, rate = 50, rangemin = 300, rangemax = 800, target = 3 },
}
