--ArcBishopNest_Gate2_HalfGolem_Green.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 30000
g_Lua_GlobalCoolTime2 = 3000

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Attack2_Blow", rate = 25, loop = 1, cooltime = 8000, globalcooltime = 2 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
}

g_Lua_Skill = { 
   { skill_index = 30915, cooltime = 24000, rate = 80, rangemin = 0, rangemax = 1500, target = 3 },
   { skill_index = 30919, cooltime = 24000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, globalcooltime = 2 },
   { skill_index = 30916, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, encountertime = 30000, globalcooltime = 1 },
   { skill_index = 30918, cooltime = 23000, rate = -1, rangemin = 400, rangemax = 1500, target = 3, selfhppercent = 75 },
   { skill_index = 30917, cooltime = 23000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 75, globalcooltime = 1 },
}