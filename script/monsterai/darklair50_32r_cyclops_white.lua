--AiCyclops_Green_Boss_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 30000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000


g_Lua_Near1 = 
{ 
   { action_name = "Stand_5", rate = 5, loop = 1, td = "FL,FR,RB,BR,BL,LB" },
   { action_name = "Walk_Left", rate = 5, loop = 1, td = "FL,FR,LF,RF" },
   { action_name = "Walk_Right", rate = 5, loop = 1, td = "FL,FR,LF,RF" },
   { action_name = "Attack001_Bash", rate = 20, loop = 1, td = "FL,FR,LF,RF" },
}

g_Lua_Near2 = { 
   { action_name = "Stand_5", rate = 5, loop = 1, td = "FL,FR,RB,BR,BL,LB" },
   { action_name = "Walk_Left", rate = 2, loop = 1, td = "FL,FR,LF,RF" },
   { action_name = "Walk_Right", rate = 2, loop = 1, td = "FL,FR,LF,RF" },
   { action_name = "Move_Front", rate = 10, loop = 1, td = "FL,FR,LF,RF" },
   { action_name = "Attack008_SummonRelic_Charger", rate = -1, loop = 1, globalcooltime = 1 },
}

g_Lua_Near3 = { 
   { action_name = "Stand_5", rate = 2, loop = 1, td = "FL,FR,RB,BR,BL,LB" },
   { action_name = "Move_Front", rate = 10, loop = 3, td = "FL,FR,LF,RF" },
   { action_name = "Attack003_JumpAttack_DarkLair50", rate = 10, loop = 1, cooltime = 20000  },
}

g_Lua_Near4 = { 
   { action_name = "Stand_5", rate = 5, loop = 1, td = "FL,FR,RB,BR,BL,LB" },
   { action_name = "Move_Front", rate = 20, loop = 3, td = "FL,FR,LF,RF" },
}

g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 20, loop = 1, td = "FL,FR,LF,RF", },
   { action_name = "Assault", rate = 30, loop = 1, td = "FL,FR,LF,RF", },
}

g_Lua_Assault = { 
   { action_name = "Attack001_Bash", rate = 10, loop = 1, td = "FL,FR,LF,RF", },
}

g_Lua_Skill = { 
-- 스턴샤우트
   { skill_index = 30310, cooltime = 20000, rate = -1, rangemin = 0, rangemax = 600, target = 3, td = "FL,FR,RB,BR,BL,LB" },
-- 헤비대쉬
   { skill_index = 30312, cooltime = 30000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, td = "FL,FR,RB,BR" },
-- 아이오브일렉트릭
   { skill_index = 30311, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, td = "FL,FR" },
-- 라이트닝스톰
   { skill_index = 30314, cooltime = 40000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, td = "FL,FR,RB,BR,BL,LB" },
}
