--AiDarkElf_Red_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 15, loop = 1  },
   { action_name = "Move_Back", rate = 25, loop = 1  },
   { action_name = "Attack4_SickleKick", rate = 15, loop = 1, max_missradian = 20 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Attack4_SickleKick", rate = 35, loop = 1, max_missradian = 20 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Attack7_ThrowingKnife", rate = 15, loop = 1  },
   { action_name = "Assault", rate = 30, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Attack7_ThrowingKnife", rate = 15, loop = 1  },
   { action_name = "Assault", rate = 30, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_2", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 1  },
   { action_name = "Assault", rate = 30, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack4_SickleKick", rate = 5, loop = 1, approach = 150.0, max_missradian = 30 },
}
g_Lua_Skill = { 
   { skill_index = 20063,  cooltime = 15000, rate = 100,rangemin = 200, rangemax = 1000, target = 3, selfhppercent = 70 },
   { skill_index = 20063,  cooltime = 15000, rate = -1, rangemin = 200, rangemax = 1000, target = 3, selfhppercent = 70 },
   { skill_index = 20064,  cooltime = 3000, rate = 100, rangemin = 0, rangemax = 300, target = 3, selfhppercent = 100 },
}
