-- Manticore Lv1 Normal A.I
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 450.0;
g_Lua_NearValue2 = 650.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 1800.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 320;
g_Lua_AssualtTime = 3000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,RF", existparts="1" },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Back",	rate = 8,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Attack11_LHook",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1" },
	{ action_name = "Attack11_LHook",	rate = 50,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1", target_condition = "State2" },
	{ action_name = "Attack11_RHook",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1" },
	{ action_name = "Attack11_RHook",	rate = 50,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1", target_condition = "State2" },
	{ action_name = "Turn_Left",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
	{ action_name = "Attack4_Tail_L",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,LB,BL,BR", existparts="1"},
	{ action_name = "Turn_Right",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
	{ action_name = "Attack5_Tail_R",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "RF,RB,BL,BR", existparts="1" },
	{ action_name = "Fly_Start",	rate = 40,		loop = 1, custom_state1 = "custom_ground", existparts="1", selfhppercent = 30 },
	{ action_name = "Fly_1",		rate = 10,		loop = 1, custom_state1 = "custom_fly", existparts="1" },
	{ action_name = "Attack7_Swoop",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="1", randomtarget=1.1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Back",	rate = 5,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Attack11_LHook",	rate = 15,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1" },
	{ action_name = "Attack11_LHook",	rate = 50,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1", target_condition = "State2" },
	{ action_name = "Attack11_RHook",	rate = 15,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1" },
	{ action_name = "Attack11_RHook",	rate = 50,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="1", target_condition = "State2" },
	{ action_name = "Turn_Left",	rate = 20,		loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
	{ action_name = "Turn_Right",	rate = 20,		loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
	{ action_name = "Fly_Start",	rate = 25,		loop = 1, custom_state1 = "custom_ground", existparts="1", selfhppercent = 30 },
	{ action_name = "Fly_1",		rate = 10,		loop = 1, custom_state1 = "custom_fly", existparts="1" },
	{ action_name = "Attack7_Swoop",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="1", randomtarget=1.1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Front",	rate = 30,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Back",	rate = 5,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Turn_Left",	rate = 20,		loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
	{ action_name = "Turn_Right",	rate = 20,		loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
	{ action_name = "Attack9_Jump",	rate = 60,		loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", existparts="1", randomtarget=1.1 },
	{ action_name = "Fly_Start",	rate = 30,		loop = 1, custom_state1 = "custom_ground", existparts="1", selfhppercent = 30 },
	{ action_name = "Fly_1",		rate = 10,		loop = 1, custom_state1 = "custom_fly", existparts="1" },
	{ action_name = "Attack7_Swoop",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="1", randomtarget=1.1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Front",	rate = 20,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Turn_Left",	rate = 20,		loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
	{ action_name = "Turn_Right",	rate = 20,		loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
	{ action_name = "Attack9_Jump",	rate = 50,		loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", existparts="1", randomtarget=1.1 },
	{ action_name = "Fly_Start",	rate = 40,		loop = 1, custom_state1 = "custom_ground", existparts="1", selfhppercent = 30 },
	{ action_name = "Fly_1",		rate = 10,		loop = 1, custom_state1 = "custom_fly", existparts="1" },
	{ action_name = "Attack7_Swoop",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="1", randomtarget=1.1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="1" },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Walk_Front",	rate = 20,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="1" },
	{ action_name = "Turn_Left",	rate = 20,		loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="1" },
	{ action_name = "Turn_Right",	rate = 20,		loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="1" },
	{ action_name = "Fly_Start",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="1", selfhppercent = 30 },
	{ action_name = "Fly_1",		rate = 10,		loop = 1, custom_state1 = "custom_fly", existparts="1" },
	{ action_name = "Attack7_Swoop",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="1", randomtarget=1.1 },
}

g_Lua_Skill=
{
	{ skill_index = 30004, SP = 0, cooltime = 50000, rangemin = 0, rangemax = 700, target = 3, selfhppercent = 70,  custom_state1 = "custom_ground", rate = 100, existparts="1" },
	{ skill_index = 30002, SP = 0, cooltime = 10000, rangemin = 0, rangemax = 400, target = 3, selfhppercent = 100,  custom_state1 = "custom_ground", rate = 50, td = "FL,FR,LF,RF", target_condition = "State1", existparts="1" },
{ skill_index = 30005, SP = 0, cooltime = 5000, rangemin = 500, rangemax = 1800, target = 3, selfhppercent = 50, custom_state1 = "custom_ground", rate = 50, td = "FL,FR,LF,RF", existparts="1" },
	{ skill_index = 30006, SP = 0, cooltime = 20000, rangemin = 0, rangemax = 1800, target = 3, selfhppercent = 50,  custom_state1 = "custom_fly", rate = -1, loop = 3, existparts="1" },
}