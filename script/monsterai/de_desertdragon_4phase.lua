--AiBroo_Blue_Normal.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 3500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction =
{
   CustomAction1 = 
   {
	{ "Turn_Left", 1 },
 	{ "useskill", lua_skill_index = 7, rate = 100 },
   },
   CustomAction2 = 
   {
 	{ "Turn_Right", 1 },
 	{ "useskill", lua_skill_index = 7, rate = 100 },
   },
}

-- 1:점프, 2:후방 3: Tunr이후Slash, 4: 활강 5: 트랩 6:라운드와활강 7:브레스 8:쓰론,점프,트랩,브레스, 활강 9:웨이브프론트,백,테일어택
g_Lua_GlobalCoolTime1 = 57000
g_Lua_GlobalCoolTime2 = 7000
g_Lua_GlobalCoolTime3 = 8000
g_Lua_GlobalCoolTime4 = 70000
g_Lua_GlobalCoolTime5 = 40000
g_Lua_GlobalCoolTime6 = 12000
g_Lua_GlobalCoolTime7 = 20000
g_Lua_GlobalCoolTime8 = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "CustomAction1", rate = 50, loop = -1, globalcooltime = 3, priority = 45, td = "LF,LB,BL" },
   { action_name = "CustomAction2", rate = 50, loop = -1, globalcooltime = 3, priority = 45, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_Skill = { 
--0 펑펑펑 -- 태양의 눈 패턴제거
   -- Attack15_Bomb_loop - 물,마공 10 증가
   { skill_index = 32106, cooltime = 86000, globalcooltime = 6, priority = 50, rate = -1, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "50,80" }, 

--1 2 광폭화
   -- Attack06_Rage_Start - 유저ESCAPE, 슈퍼아머파괴력,속성내성증가
   { skill_index = 32138, cooltime = 72000, priority = 60, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 80, next_lua_skill_index = 2 },
   -- Attack09_Rage_End_86343 - 유저물마공감소,유저버프날리기,유저데미지감소,유저힐면역
   { skill_index = 32157, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },

--3,4,5,6 후방공격(3sec), 
   -- Attack18_BackAttack - %데미지, 유저힐금지, 유저물마공감소, 유저데미지감소
   { skill_index = 32189, cooltime = 5000, globalcooltime = 2, rate = 100, priority = 20, rangemin = 0, rangemax = 3000, target = 3, td = "BL,BR", randomtarget= "0.6,1", notusedskill = "32157" },
   -- Attack19_TailSmash - %데미지, 유저힐금지, 유저물마공감소, 유저데미지감소
   { skill_index = 32196, cooltime = 5000, globalcooltime = 2, rate = 100, priority = 20, rangemin = 0, rangemax = 3000, target = 3, td = "BL,BR", randomtarget= "0.6,1", notusedskill = "32157" },
   { skill_index = 32189, cooltime = 5000, globalcooltime = 2, rate = 100, priority = 20, rangemin = 0, rangemax = 3000, target = 3, td = "BL,BR", randomtarget= "0.6,1", usedskill = "32157" },
   { skill_index = 32196, cooltime = 5000, globalcooltime = 2, rate = 100, priority = 20, rangemin = 0, rangemax = 3000, target = 3, td = "BL,BR", randomtarget= "0.6,1", usedskill = "32157" },

--7 모래(208/420/132 = 760 > 13sec)
   -- Attack11_Sand_Start - 유저 얼음감옥, 버프날리기, 킬블로우
   { skill_index = 32159, cooltime = 80000, rate = 90, priority = 30, rangemin = 0, rangemax = 3000, target = 3, encountertime = 15000, multipletarget = "1,1,0,1" },

--8,9,10,11 slash(3sec)
   -- Attack01_Slash - 킬블로우, 202;3021;3221 버프날리기
   { skill_index = 32143, cooltime = 6000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
   { skill_index = 32143, cooltime = 6000, globalcooltime = 3, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR" },
   -- Attack02_StampRight - 킬블로우, 202;3021 버프날리기
   { skill_index = 32144, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FR,RF" },
   -- Attack02_StampLeft - 킬블로우, 202;3021 버프날리기
   { skill_index = 32145, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FL,LF" },

--12,13 WaveFront(3sec)
   -- Attack06_WaveFront
   { skill_index = 32135, cooltime = 11000, globalcooltime = 8, rate = 80, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "0.6,1", td = "LF,RF" },
   -- Attack07_WaveBack 
   { skill_index = 32136, cooltime = 11000, globalcooltime = 8, rate = 80, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "0.6,1", td = "LB,RB" },

--14,15 테일 어택(4sec)
   -- Attack05_TailAttack - %데미지, 유저힐금지, 유저물마공감소, 유저데미지감소 
   { skill_index = 32188, cooltime = 25000, rate = 90, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "0.6,1", td = "LF,RF,LB,RB", notusedskill = "32157" },
   -- Attack05_TailAttack - %데미지, 유저힐금지, 유저물마공감소, 유저데미지감소
   { skill_index = 32199, cooltime = 25000, rate = 90, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "0.6,1", td = "LF,RF,LB,RB", usedskill = "32157" },

--16 브레스 프론트(8sec)
   -- Attack04_BreathFront_Hell_Start - 킬블로우
   { skill_index = 32142, cooltime = 50000, globalcooltime = 7, rate = 70, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR" },

--17 태양(300 > 5sec)
   -- Attack12_Sun - 유저섬광, 유저침묵, 버프날리기
   { skill_index = 32108, cooltime = 40000, rate = 70, rangemin = 0, rangemax = 3000, target = 3 },

--18 활강 프론트
   -- Attack20_Fly_Front
   { skill_index = 32141, cooltime = 50000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR" },

--19 콤보 (10sec)
   -- Attack03_Combo_Start - 유저화상(초당3만- 20초), 킬블로우
   { skill_index = 32102, cooltime = 60000, globalcooltime = 7, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR" },
--20 꼬리 가시(9sec)
   -- Attack08_Thorn_Start - 유저힐금지, 유저물마공감소, 유저데미지감소 
   { skill_index = 32192, cooltime = 60000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,8,0,1" },

--21,22,23 트랩(6sec) 32109 : Left 32132 : Right 32112 : Front
   -- Attack10_Trap_Front
   { skill_index = 32112, cooltime = 40000, globalcooltime = 5, globalcooltime2 = 6, rate = 80, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", randomtarget= "0.6,1" },
   -- Attack10_Trap_Right
   { skill_index = 32132, cooltime = 40000, globalcooltime = 5, globalcooltime2 = 6, rate = 80, rangemin = 0, rangemax = 3000, target = 3, td = "RF,RB", randomtarget= "0.6,1" },
   -- Attack10_Trap_Left   
   { skill_index = 32109, cooltime = 40000, globalcooltime = 5, globalcooltime2 = 6, rate = 80, rangemin = 0, rangemax = 3000, target = 3, td = "LF,LB", randomtarget= "0.6,1" },

--24,25 점프(7sec) 32137 -> 탱커가 정면에 없을 때, 탱커를 쫓아감.
   -- Attack09_Jump
   { skill_index = 32137, cooltime = 10000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 3000, target = 3, td = "RF,RB,LF,LB,BL,BR" },
   -- Attack09_Jump
   { skill_index = 32134, cooltime = 57000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 3000, target = 3, randomtarget= "1.6,0,1", td = "RF,RB,LF,LB" },

--26,27,28,29(20sec)
   -- Attack13_Fly_Start - 유저힐금지, 유저물마공감소, 유저데미지감소
   { skill_index = 32193, cooltime = 80000, globalcooltime = 4, rate = 65, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,8,0,1", selfhppercentrange = "52,100" },
   -- Attack13_Fly2_Start - 유저힐금지, 유저물마공감소, 유저데미지감소
   { skill_index = 32194, cooltime = 80000, globalcooltime = 4, rate = 70, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,8,0,1", selfhppercentrange = "52,100" },
   -- Attack13_Fly3_Start - 유저힐금지, 유저물마공감소, 유저데미지감소   
   { skill_index = 32195, cooltime = 80000, globalcooltime = 4, rate = 65, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,8,0,1", selfhppercentrange = "52,100" },
   -- Attack13_Fly_Breath_Start
   { skill_index = 32140, cooltime = 80000, globalcooltime = 4, rate = -1, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "52,100" },

--30 회전 브레스(36sec)
   -- Attack14_BreathRound_Start - 킬블로우
   { skill_index = 32158, cooltime = 96000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 50 },

--31 기절(펑펑펑 기절)
   -- Stun_Start
   { skill_index = 32115, cooltime = 1000, priority = 50, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
}
