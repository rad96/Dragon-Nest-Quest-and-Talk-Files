--AiAsaiWorrior_Zombie_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 900;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Attack1_Pierce", rate = 6, loop = 1 , max_missradian = 30 },
   { action_name = "Attack2_SelfBomb", rate = 6, loop = 1, selfhppercent = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Assault", rate = 15, loop = 1, selfhppercent = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 1 },
   { action_name = "Assault", rate = 18, loop = 1, selfhppercent = 30 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
   { action_name = "Assault", rate = 12, loop = 1, selfhppercent = 30 },
}
g_Lua_Assault = { 
   { action_name = "Attack2_SelfBomb", rate = 1, loop = 1, selfhppercent = 30, approach = 150 },
   { action_name = "Stand_1", rate = 10, loop = 1, approach = 150 },
   { action_name = "Attack1_Pierce", rate = 5, loop = 1, approach = 150, max_missradian = 30 },
}
