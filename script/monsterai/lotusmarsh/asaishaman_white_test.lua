--AIDragonSycophantCleric_White_Test.lua
--완전 근접 거리에서는 킥, 그 다음 거리는 콤보
--블록 : 20574


g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack|!Air", "Down" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack4_Kick", 0 },
  },
  CustomAction2 = {
      { "Attack5_MaceCombo", 0 },
  },
  CustomAction3 = {
      { "Attack6_ShieldCharge", 0 },
  },
  CustomAction4 = {
      { "Attack5_MiniIceBolt", 0 },
  },
  CustomAction5 = {
      { "Attack5_MiniIceBolt", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Attack3_Down", rate = 30, loop = 2, target_condition = "State1" },
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "CustomAction1", rate = -1, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "CustomAction2", rate = -1, loop = 1, selfhppercentrange = "75,98" },
   { action_name = "CustomAction3", rate = -1, loop = 1, selfhppercentrange = "50,74" },
   { action_name = "CustomAction4", rate = -1, loop = 1, selfhppercentrange = "25,49" },
   { action_name = "CustomAction5", rate = -1, loop = 1, selfhppercentrange = "1,24" },
}
g_Lua_Near2 = { 
   { action_name = "Attack3_Down", rate = 30, loop = 2, target_condition = "State1" },
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "CustomAction1", rate = -1, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "CustomAction2", rate = -1, loop = 1, selfhppercentrange = "75,98" },
   { action_name = "CustomAction3", rate = -1, loop = 1, selfhppercentrange = "50,74" },
   { action_name = "CustomAction4", rate = -1, loop = 1, selfhppercentrange = "25,49" },
   { action_name = "CustomAction5", rate = -1, loop = 1, selfhppercentrange = "1,24" },
}
g_Lua_Near3 = { 
   { action_name = "Attack3_Down", rate = 30, loop = 2, target_condition = "State1" },
   { action_name = "Stand", rate = 2, loop = 2  },
   { action_name = "CustomAction1", rate = -1, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "CustomAction2", rate = -1, loop = 1, selfhppercentrange = "75,98" },
   { action_name = "CustomAction3", rate = -1, loop = 1, selfhppercentrange = "50,74" },
   { action_name = "CustomAction4", rate = -1, loop = 1, selfhppercentrange = "25,49" },
   { action_name = "CustomAction5", rate = -1, loop = 1, selfhppercentrange = "1,24" },
}
g_Lua_Near4 = { 
   { action_name = "Attack3_Down", rate = 30, loop = 2, target_condition = "State1" },
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "CustomAction1", rate = -1, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "CustomAction2", rate = -1, loop = 1, selfhppercentrange = "75,98" },
   { action_name = "CustomAction3", rate = -1, loop = 1, selfhppercentrange = "50,74" },
   { action_name = "CustomAction4", rate = -1, loop = 1, selfhppercentrange = "25,49" },
   { action_name = "CustomAction5", rate = -1, loop = 1, selfhppercentrange = "1,24" },
}

g_Lua_Skill = { 
   { skill_index = 20522, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "99,100" },
   { skill_index = 20525, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "75,98" },
}
