g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
}

g_Lua_Skill = { 
   { skill_index = 20530, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, selfhppercentrange = "99,100" },
   { skill_index = 20531, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, selfhppercent = 98 },
}
