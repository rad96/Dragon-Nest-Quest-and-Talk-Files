--RuneTiger_Blue_Elite_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Back", rate = 12, loop = 1, td = "FL,FR" },
   { action_name = "Attack1_Claw", rate = 22, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Back", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Attack4_FrontMissile", rate = 12, loop = 1, max_missradian = 20, cooltime = 15000, td = "FL,FR" },
   { action_name = "Attack3_MultiMissile", rate = 15, loop = 1, cooltime = 15000, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 3, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 3, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 20, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 8, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Back", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Attack4_FrontMissile", rate = 12, loop = 1, max_missradian = 20, cooltime = 15000, td = "FL,FR" },
   { action_name = "Attack3_MultiMissile", rate = 15, loop = 1, cooltime = 15000, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 3, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 3, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 20, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 15, loop = 2, td = "FL,FR" },
   { action_name = "Move_Left", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Move_Right", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Move_Front", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Attack4_FrontMissile", rate = 22, loop = 1, max_missradian = 20, cooltime = 15000, td = "FL,FR" },
   { action_name = "Assault", rate = 18, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 10, loop = 2, td = "FL,FR" },
   { action_name = "Move_Left", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Move_Right", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Move_Front", rate = 20, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 10, loop = 1, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 20701,  cooltime = 25000, rate = 80,rangemin = 300, rangemax = 1000, target = 3, selfhppercent = 50, td = "FL,FR" },
}
