--LivingBook_Red_Elite_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 20, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = -1, loop = 1  },
   { action_name = "Move_Right", rate = -1, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 1 },
   { action_name = "Attack1_Bite", rate = 22, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 20, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 3, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20615,  cooltime = 10000, rate = 80,rangemin = 0, rangemax = 1200, target = 3, selfhppercent = 100 },
   { skill_index = 20619,  cooltime = 22000, rate = 80, rangemin = 0, rangemax = 400, target = 3, selfhppercent = 80 },
}
