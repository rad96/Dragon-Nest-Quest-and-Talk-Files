--AiAsaiWorrior_Hunter_Elite_Easy.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 5500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Attack2_Slash_Boss", rate = 6, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front2", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Attack1_Pierce_Boss", rate = 4, loop = 1, max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front2", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 14, loop = 1 },
   { action_name = "Assault", rate = 3, loop = 1, max_missradian = 30 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 14, loop = 3 },
   { action_name = "Attack6_JumpAttack_Named", rate = 6, loop = 1, randomtarget = 1.1 },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 14, loop = 3 },
}

g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 5, loop = 1, approach = 300 },
   { action_name = "Attack2_Slash", rate = 10, loop = 1, approach = 300, max_missradian = 30 },
}
g_Lua_Skill = { 
   { skill_index = 20492,  cooltime = 31000, rate = 80,rangemin = 400, rangemax = 1500, target = 3, next_lua_skill_index = 1 },
   { skill_index = 20491,  cooltime = 1000, rate = -1, rangemin = 10, rangemax = 1500, target = 3 },
   { skill_index = 20486,  cooltime = 57000, rate = 40, rangemin = 600, rangemax = 1500, target = 3, selfhppercent = 50, randomtarget = 1.1 },
}
