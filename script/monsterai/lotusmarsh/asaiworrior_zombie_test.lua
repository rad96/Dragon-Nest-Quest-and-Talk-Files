--AiAsaiWorrior_Green_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 4000
g_Lua_PatrolRandTime = 2000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = {
   { action_name = "Stand_1", rate = 7, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack1_Pierce", rate = 4, loop = 1 },
   { action_name = "Attack2_SelfBomb", rate = 30, loop = 1, selfhppercent = 30 },
}
g_Lua_Near2 = {
   { action_name = "Stand_1", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 2 },
   { action_name = "Walk_Left", rate = -1, loop = 1 },
   { action_name = "Walk_Right", rate = -1, loop = 1 },
   { action_name = "Assault", rate = 20, loop = 1, selfhppercent = 30 },
}
g_Lua_Near3 = {
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 2 },
   { action_name = "Walk_Left", rate = -1, loop = 1 },
   { action_name = "Walk_Right", rate = -1, loop = 1 },
   { action_name = "Assault", rate = 30, loop = 1, selfhppercent = 30 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 4 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Assault", rate = 30, loop = 1, selfhppercent = 30 },
}
g_Lua_Assault = { 
   { action_name = "Attack2_SelfBomb", rate = 5, loop = 1, approach = 200 },
}