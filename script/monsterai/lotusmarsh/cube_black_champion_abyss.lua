-- Cube Black Normal AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 200.0;
g_Lua_NearValue2 = 400.0;
g_Lua_NearValue3 = 800.0;
g_Lua_NearValue4 = 1500.0;


g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 120;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Move_Back",	rate = 10,		loop = 2 },
	{ action_name = "Attack2_Bit",	rate = 5,		loop = 1 },
	{ action_name = "Attack5_Small_Laser",	rate = 2,		loop = 1 },
--	{ action_name = "Attack6_BlackHole",	rate = 3,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 1 },
	{ action_name = "Attack5_Small_Laser",	rate = 6,		loop = 1 },
	{ action_name = "Attack4_Big_Laser",	rate = 6,		loop = 1, max_missradian = 20 },
--	{ action_name = "Attack6_BlackHole",	rate = 8,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
        { action_name = "Move_Front",	rate = 10,		loop = 1 },
       	{ action_name = "Assault",	rate = 3,		loop = 1 },
	{ action_name = "Attack4_Big_Laser",	rate = 7,		loop = 1, max_missradian = 20 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
}

g_Lua_Assault =
{ 
        { action_name = "Attack2_Bit",	rate = 3,	loop = 1, cancellook = 1, approach = 100.0 },
}
g_Lua_Skill = { 
--��¦�̴�
   { skill_index = 30503, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, encountertime = 30000 },
}