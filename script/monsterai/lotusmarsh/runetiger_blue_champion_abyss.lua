-- Runetiger_Blue_Normal AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 250.0;
g_Lua_NearValue2 = 500.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 1200.0;


g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;



g_Lua_Near1 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1, td = "FL,FR" },
        { action_name = "Walk_Back",	rate = 10,		loop = 2, td = "FL,FR" },
        { action_name = "Turn_Left",	rate = 20,		loop = -1, td = "LF,BL,LB" },
	{ action_name = "Turn_Right",	rate = 20,		loop = -1, td = "RF,RB,BR" },
        { action_name = "Attack1_Claw",	rate = 7,		loop = 1, td = "FL,FR" },
--        { action_name = "Attack3_MultiMissile",	rate = 10,		loop = 1, td = "FL,FR" },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2, td = "FL,FR" },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 3,		loop = 2, td = "FL,FR" },
        { action_name = "Turn_Left",	rate = 20,		loop = -1, td = "LF,BL,LB" },
	{ action_name = "Turn_Right",	rate = 20,		loop = -1, td = "RF,RB,BR" },
        { action_name = "Attack4_FrontMissile",	rate = 8,		loop = 1, max_missradian = 30, cooltime = 15000, td = "FL,FR" },
--        { action_name = "Attack3_MultiMissile",	rate = 10,		loop = 1, td = "FL,FR" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2, td = "FL,FR" },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2, td = "FL,FR" },
        { action_name = "Turn_Left",	rate = 20,		loop = -1, td = "LF,BL,LB" },
	{ action_name = "Turn_Right",	rate = 20,		loop = -1, td = "RF,RB,BR" },
        { action_name = "Attack4_FrontMissile",	rate = 10,		loop = 1, max_missradian = 20, cooltime = 15000, td = "FL,FR" },
--        { action_name = "Attack3_MultiMissile",	rate = 10,		loop = 1, td = "FL,FR" },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Move_Left",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "Move_Right",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "Move_Front",	rate = 10,		loop = 2, td = "FL,FR" },
        { action_name = "Turn_Left",	rate = 20,		loop = -1, td = "LF,BL,LB" },
	{ action_name = "Turn_Right",	rate = 20,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Claw",	rate = 5,	loop = 1, approach = 200.0 },
}

g_Lua_Skill =
{ 
   { skill_index = 20701,  cooltime = 60000, rate = 90,rangemin = 300, rangemax = 1000, target = 3, selfhppercent = 50, td = "FL,FR" },
      --��¦�̴�
   { skill_index = 30503, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, encountertime = 30000 },
}

