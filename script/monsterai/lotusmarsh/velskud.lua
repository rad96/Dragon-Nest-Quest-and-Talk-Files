-- NPC Geraint Normal AI

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_Near1 = 
{ 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack3_StompNSlash", rate = 10, loop = 1, cooltime = 6000 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 3 },
   { action_name = "Walk_Left", rate = 5,loop = 1 },
   { action_name = "Walk_Right", rate = 5,loop = 1 },
   { action_name = "Attack3_StompNSlash", rate = 30, loop = 1, cooltime = 6000 },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Move_Front", rate = 25, loop = 1 },
   { action_name = "Walk_Front",rate = 5,loop = 1 },
}
g_Lua_Near5 = 
{ 
   { action_name = "Move_Front", rate = 25, loop = 1 },
   { action_name = "Walk_Front",rate = 5,loop = 1 },
}