--Mimic_Normal_Elite_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 20, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack01_RushBite", rate = 7, loop = 1, max_missradian = 30 },
   { action_name = "Attack08_SpiningTongue", rate = 15, loop = 1, cooltime = 10000 },
   { action_name = "Attack02_DownBite", rate = 75, loop = 1, target_condition = "State1"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 20, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack01_RushBite", rate = 12, loop = 1, max_missradian = 30 },
   { action_name = "Attack09_StickyTongue", rate = 15, loop = 1, cooltime = 15000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Attack09_StickyTongue", rate = 18, loop = 1, cooltime = 15000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 1  },
}
