-- Cube Black Normal AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 200.0;
g_Lua_NearValue2 = 400.0;
g_Lua_NearValue3 = 800.0;
g_Lua_NearValue4 = 1500.0;


g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 120;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Righk",	rate = 5,		loop = 2 },
	{ action_name = "Attack2_Bit_Named",	rate = 15,		loop = 1	},
	{ action_name = "",	rate = 15, loop = 1, target = 3 },
--	{ action_name = "Attack6_BlackHole",	rate = 3,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 1 },
	-- { action_name = "Walk_Back",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 1 },
	-- { action_name = "Attack6_Air_Laser1_Clone[1]",	rate = 20, loop = 1, cooltime = 10000, target = 3 },
	{ action_name = "Attack7_Eraser_Named",	rate = 50, loop = 1, cooltime = 10000, target = 3 },
	{ action_name = "Attack4_Big_Laser",	rate = 15,cooltime = 8000,		loop = 1, target = 3},
--	{ action_name = "Attack6_BlackHole",	rate = 8,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
    { action_name = "Move_Front",	rate = 10,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 15,		loop = 2 },
}

g_Lua_Assault =
{ 
     { action_name = "Attack2_Bit_Named",	rate = 5,	loop = 1, cancellook = 1, approach = 100.0 },
}
