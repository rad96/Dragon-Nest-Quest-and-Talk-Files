--AiAsaiWorrior_Green_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack2_Slash_Boss", rate = 4, loop = 1 },
   { action_name = "Attack1_Pierce_Boss", rate = 4, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Attack8_Double_Boomerang", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Assault", rate = 2, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 3 },
   { action_name = "Move_Front", rate = 8, loop = 3 },
   { action_name = "Assault", rate = 4, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Slash_Boss", rate = 10, loop = 1, approach = 150 },
   { action_name = "Attack1_Pierce_Boss", rate = 10, loop = 1, approach = 400 },
}

g_Lua_Skill = { 
-- 점프어택 웨이브
   { skill_index = 20487,  cooltime = 23000, rate = 60, rangemin = 900, rangemax = 1500, target = 3, randomtarget = 1.1 },
-- 고기 함정
   { skill_index = 20491,  cooltime = 54000, rate = 50, rangemin = 0, rangemax = 3500, target = 3 },
-- 난무
   { skill_index = 20495,  cooltime = 34000, rate = 50, rangemin = 0, rangemax = 600, target = 3 }, 
}
