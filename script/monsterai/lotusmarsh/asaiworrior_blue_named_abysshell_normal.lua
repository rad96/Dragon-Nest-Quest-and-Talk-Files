--AiAsaiWorrior_Blue_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 900;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Back", rate = 3, loop = 1 },
   { action_name = "Attack1_Pierce", rate = 6, loop = 1, max_missradian = 20 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Walk_Front", rate = 7, loop = 2 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 5, loop = 1, approach = 150 },
   { action_name = "Attack1_Pierce", rate = 5, loop = 1, max_missradian = 20, approach = 150 },
}
g_Lua_Skill = { 
   { skill_index = 20483,  cooltime = 21000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, next_lua_skill_index = 1 },
   { skill_index = 20482,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3 },
   { skill_index = 20470,  cooltime = 30000, rate = 80,rangemin = 0, rangemax = 400, target = 3, selfhppercent = 50 },
   { skill_index = 20471,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 400, target = 2, selfhppercent = 50, limitcount = 1 },
}
