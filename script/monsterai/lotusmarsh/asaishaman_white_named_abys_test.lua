--AiAsaiShaman_Blue_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Move_Back", rate = 5, loop = 2 },
   { action_name = "Stand_1", rate = 3, loop = 2 },
   { action_name = "Walk_Left", rate = 1, loop = 2 },
   { action_name = "Walk_Right", rate = 1, loop = 2 },
   { action_name = "Move_Left", rate = 1, loop = 1 },
   { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2 },
   { action_name = "Walk_Front", rate = 2, loop = 2 },
   { action_name = "Walk_Left", rate = 2, loop = 2 },
   { action_name = "Walk_Right", rate = 2, loop = 2 },
   { action_name = "Move_Front", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 5, loop = 2 },
   { action_name = "Walk_Left", rate = 2, loop = 2 },
   { action_name = "Walk_Right", rate = 2, loop = 2 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
}
g_Lua_Skill = { 
--약병 던지기(감전)
   { skill_index = 20522, cooltime = 7000, rate = 30, rangemin = 100, rangemax = 800, max_missradian = 30, target = 3 },
-- 구름소환
  { skill_index = 20535, cooltime = 10000, rate = 40 , rangemin = 0, rangemax = 900, target = 3 , selfhppercent = 50 },
--버프
   { skill_index = 20528, cooltime = 12000, rate =50, rangemin = 0, rangemax = 1500, target = 2,  },
--디버프
   { skill_index = 20531, cooltime = 12000, rate = 40, rangemin = 0, rangemax = 1500, target = 3,  selfhppercent = 70 },
}