--AiDrakeHatchling_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 7, loop = 1 },
   { action_name = "Walk_Right", rate = 7, loop = 1 },
   { action_name = "Walk_Back", rate = 7, loop = 2 },
   { action_name = "Move_Back", rate = 3, loop = 1 },
   { action_name = "Attack1_Bite", rate = 15, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 2 },
   { action_name = "Walk_Right", rate = 10, loop = 2 },
   { action_name = "Assault", rate = 15, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 8, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 2 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Assault", rate = 15, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 3 },
   { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Bite", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 20405,  cooltime = 20000, rate = 20,rangemin = 100, rangemax = 300, target = 3 },
   { skill_index = 20404,  cooltime = 15000, rate = -1, rangemin = 500, rangemax = 900, target = 3, selfhppercent = 50 },
   --��¦�̴�
   { skill_index = 30503, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, encountertime = 30000 },
}
