-- Runeelf_Blue_ Normal AI

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 400.0;
g_Lua_NearValue3 = 600.0;
g_Lua_NearValue4 = 1000.0;
g_Lua_NearValue5 = 1500.0;


g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;


g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" },
	State2 = {"!Stay|!Move|!Stiff|!Down|!Attack", "Air" },
	State3 = {"!Stay|!Move|!Stiff|!Attack", "Stun" },
}


g_Lua_Near1 = 
{ 
	{ action_name = "Stand_1",	rate = 15,		loop = 1 },
        { action_name = "Walk_Back",	rate = 15,		loop = 2 },
	{ action_name = "Move_Back",	rate = 5,		loop = 1 },
        { action_name = "Attack1_Punch",	rate = 10,		loop = 1 },
--	{ action_name = "Attack3_HundredKick",	rate = 20,		loop = 1, cooltime = 20000 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 1 },
	{ action_name = "Attack1_Punch",	rate = 5,		loop = 1 },
	{ action_name = "Attack2_Kick",	rate = 8,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
	{ action_name = "Move_Front",	rate = 10,		loop = 2 },
	{ action_name = "Attack2_Kick",	rate = 12,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 20,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 1 },
	{ action_name = "Move_Right",	rate = 3,		loop = 1 },
	{ action_name = "Move_Front",	rate = 20,		loop = 3 },
	{ action_name = "Assault",	rate = 12,		loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
	{ action_name = "Move_Front",	rate = 20,		loop = 3 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Punch",	rate = 5,	loop = 1, approach = 300.0 },
}
g_Lua_Skill = { 
--��¦�̴�
   { skill_index = 30503, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, encountertime = 30000 },
}