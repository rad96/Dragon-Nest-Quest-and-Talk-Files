--AiAsaiShaman_Lich_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 4000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 6, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 2 },
   { action_name = "Walk_Right", rate = 10, loop = 2 },
   { action_name = "Walk_Back", rate = 15, loop = 2 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "Move_Back", rate = 10, loop = 1 },
   { action_name = "Attack1_DeathHand", rate = 3, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 2 },
   { action_name = "Walk_Right", rate = 10, loop = 2 },
   { action_name = "Move_Left", rate = 3, loop = 1 },
   { action_name = "Move_Right", rate = 3, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Walk_Left", rate = 10, loop = 2 },
   { action_name = "Walk_Right", rate = 3, loop = 2 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 3 },
   { action_name = "Walk_Left", rate = 2, loop = 2 },
   { action_name = "Walk_Right", rate = 2, loop = 2 },
   { action_name = "Move_Front", rate = 5, loop = 2 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
}
g_Lua_Skill = { 
-- �ƻ��̻��� �ິ������(����)
   { skill_index = 20532,  cooltime = 17000, rate = 15,rangemin = 300, rangemax = 800, target = 3 },
-- �ƻ��̻��ո�ġ ������ ��(�����)
   { skill_index = 20533,  cooltime = 22000, rate = 10, rangemin = 300, rangemax = 800, target = 3, limitcount = 3, selfhppercent = 30 },
}
