--AiAsaiShaman_Blue_Normal.lua
-- /genmon 304802
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 2 },
   { action_name = "Walk_Left", rate = 7, loop = 2 },
   { action_name = "Walk_Right", rate = 7, loop = 2 },
   { action_name = "Walk_Back", rate = 20, loop = 2 },
   { action_name = "Move_Left", rate = 8, loop = 1 },
   { action_name = "Move_Right", rate = 8, loop = 1 },
   { action_name = "Move_Back", rate = 15, loop = 2 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 12, loop = 1  },
   { action_name = "Move_Right", rate = 12, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 2 },
   { action_name = "Walk_Left", rate = 8, loop = 3 },
   { action_name = "Walk_Right", rate = 8, loop = 3 },
   { action_name = "Walk_Front", rate = 10, loop = 3 },
   { action_name = "Move_Left", rate = 12, loop = 1 },
   { action_name = "Move_Right", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 2, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Left", rate = 10, loop = 2 },
   { action_name = "Walk_Right", rate = 10, loop = 2 },
   { action_name = "Walk_Front", rate = 5, loop = 2 },
   { action_name = "Move_Left", rate = 7, loop = 1 },
   { action_name = "Move_Right", rate = 7, loop = 1 },
   { action_name = "Move_Front", rate = 3, loop = 1 },
}
g_Lua_Skill = { 
--약병 던지기(감전)
   { skill_index = 20522, cooltime = 7000, rate = 30, rangemin = 100, rangemax = 800, target = 3 },
-- 구름소환
  { skill_index = 20535, cooltime = 10000, rate = 40 , rangemin = 0, rangemax = 900, target = 3  },
--버프 @아군과 자신 이속증가
   { skill_index = 20528, cooltime = 20000, rate = 50, rangemin = 0, rangemax = 1200, target = 2,  },
--디버프 @적 버프 1개 지우기
   { skill_index = 20531, cooltime = 20000, rate = 40, rangemin = 0, rangemax = 1200, target = 3,  },
}