--RuneTiger_White_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2500;
g_Lua_NearValue5 = 3500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Turn_Left", rate = 10, loop = 10, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 10, loop = 10, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Turn_Left", rate = 10, loop = 10, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 10, loop = 10, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Turn_Left", rate = 10, loop = 10, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 10, loop = 10, td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Turn_Left", rate = 10, loop = 10, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 10, loop = 10, td = "RF,RB,BR" },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Turn_Left", rate = 10, loop = 10, td = "LF,BL,LB" },
   { action_name = "Turn_Right", rate = 10, loop = 10, td = "RF,RB,BR" },
}

g_Lua_Skill = { 
   { skill_index = 20709,  cooltime = 25000, rate = 100, rangemin = 0, rangemax = 3800, target = 3, td = "FL,FR" },
}
