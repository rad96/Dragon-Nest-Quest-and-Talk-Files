--AiLiving_Book_Black_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 6, loop = 2  },
--   { action_name = "Move_Back", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 2 },
--   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "Attack2_Crash", rate = 10, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
--   { action_name = "Move_Right", rate = 2, loop = 2  },
--   { action_name = "Move_Front", rate = 10, loop = 1  },
--   { action_name = "Move_Back", rate = 10, loop = 1 },
   { action_name = "Attack2_Crash", rate = 20, loop = 1, max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 20, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 30, loop = 2  },
--   { action_name = "Move_Right", rate = 10, loop = 2  },
--   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 20, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 30, loop = 2  },
--   { action_name = "Move_Right", rate = 10, loop = 2  },
--   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 20618, cooltime = 15000, rate = 80, rangemin = 0, rangemax = 500, target = 3 },
--   { skill_index = 20616, cooltime = 20000, rate = 80, rangemin = 000, rangemax = 800, target = 3 },
--��¦�̴�
   { skill_index = 30503, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, encountertime = 30000 },
}