--AiDragonSycophantPuni_Red.lua
--이동도 느리고, 공격 빈도도 낮다. 하지만 한번 한번의 공격이 치명적이다. 한번 맞기 시작하면 계속해서 맞게 될 수도 있다.
--주요스킬 : 찍기, 횡베기, 점프찍기, 돌진찍기, 훨윈드, 광폭화, 암흑점프

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack2_Slash", 0 },
      { "Attack1_Chopping", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "CustomAction1", rate = 4, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 4, loop = 2  },
   { action_name = "Attack1_Chopping", rate = 4, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 10, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Walk_Left", rate = 6, loop = 2  },
   { action_name = "Walk_Right", rate = 6, loop = 2  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 8, loop = 3  },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 8, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack3_JumpChopping", rate = 8, loop = 1, approach = 600.0 },
   { action_name = "Move_Left", rate = 5, loop = 3  },
   { action_name = "Move_Right", rate = 5, loop = 3  },
   { action_name = "CustomAction1", rate = 6, loop = 1, approach = 150.0  },
}
g_Lua_Skill = { 
   { skill_index = 20383,  cooltime = 10000, rate = 60, rangemin = 300, rangemax = 600, target = 3, selfhppercent = 100 },
   { skill_index = 20384,  cooltime = 30000, rate = 80, rangemin = 0, rangemax = 1000, target = 1, selfhppercent = 33 },
   { skill_index = 20382,  cooltime = 8000, rate = 40, rangemin = 150, rangemax = 500, target = 3, selfhppercent = 100 },
}