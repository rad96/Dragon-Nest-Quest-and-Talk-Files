--AiAsaiWorrior_Green_Easy.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
   { action_name = "Attack2_Slash", rate = 4, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Assault", rate = 7, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 3, loop = 3 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 5, loop = 1, approach = 150 },
   { action_name = "Attack2_Slash", rate = 3, loop = 1, max_missradian = 30, approach = 150 },
}
g_Lua_Skill = { 
   { skill_index = 20472,  cooltime = 45000, rate = 80,rangemin = 400, rangemax = 800, target = 3 },
   { skill_index = 20473,  cooltime = 45000, rate = -1, rangemin = 0, rangemax = 800, target = 3 },
}
