--AiDragonSycophantMagician_Blue_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 2 },
   { action_name = "Walk_Left", rate = 1, loop = 2 },
   { action_name = "Walk_Right", rate = 1, loop = 2 },
   { action_name = "Walk_Back", rate = 5, loop = 2 },
   { action_name = "Move_Left", rate = 1, loop = 1 },
   { action_name = "Move_Right", rate = 1, loop = 1 },
   { action_name = "Move_Back", rate = 5, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2 },
   { action_name = "Walk_Front", rate = 2, loop = 2 },
   { action_name = "Walk_Left", rate = 2, loop = 2 },
   { action_name = "Walk_Right", rate = 2, loop = 2 },
   { action_name = "Move_Front", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 5, loop = 2 },
   { action_name = "Walk_Left", rate = 2, loop = 2 },
   { action_name = "Walk_Right", rate = 2, loop = 2 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
}
g_Lua_Skill = { 
--마나번
   { skill_index = 20550, cooltime = 12000, rate = 80, rangemin = 300, rangemax = 800, target = 3 },
--프리징 스파이크
   { skill_index = 20551, cooltime = 20000, rate = 30, rangemin = 700, rangemax = 800, target = 3, selfhppercent = 50 },
}