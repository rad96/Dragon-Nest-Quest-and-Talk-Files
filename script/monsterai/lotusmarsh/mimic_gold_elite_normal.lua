--Mimic_Gold_Elite_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 20, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack01_RushBite", rate = 15, loop = 1, max_missradian = 30 },
   { action_name = "Attack02_DownBite", rate = 45, loop = 1, target_condition = "State1"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 20, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack01_RushBite", rate = 18, loop = 1, max_missradian = 30 },
   { action_name = "Attack03_BombRush", rate = -1, loop = 1, cooltime = 15000 },
   { action_name = "Attack04_SpectrumShower", rate = -1, loop = 1, cooltime = 20000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Attack03_BombRush", rate = -1, loop = 1, cooltime = 15000 },
   { action_name = "Attack04_SpectrumShower", rate = -1, loop = 1, cooltime = 20000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20733,  cooltime = 30000, rate = -1,rangemin = 0, rangemax = 600, target = 3, selfhppercent = 50 },
   { skill_index = 20731,  cooltime = 20000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 100 },
   { skill_index = 20732,  cooltime = 20000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 100 },
}
