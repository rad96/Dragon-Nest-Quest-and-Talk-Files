--AiAsaiShaman_Lich_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 250
g_Lua_AssualtTime = 4000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Back", rate = 5, loop = 1 },
   { action_name = "Move_Back", rate = 2, loop = 1 },
   { action_name = "Attack1_Pierce", rate = 4, loop = 1 , max_missradian = 20 },
   { action_name = "Attack1_DeathHand", rate = 2, loop = 1 , max_missradian = 20, cooltime = 10000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 7, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 3, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 2 },
   { action_name = "Assault", rate = 5, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 3 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Pierce", rate = 4, loop = 1,  max_missradian = 20, approach = 150 },
   { action_name = "Attack1_DeathHand", rate = 2, loop = 1 , max_missradian = 20, approach = 150 },
   { action_name = "Stand_1", rate = 10, loop = 1, approach = 150 },
}
g_Lua_Skill = { 
   { skill_index = 20532,  cooltime = 20000, rate = 80,rangemin = 300, rangemax = 800, target = 3, selfhppercent = 50 },
   { skill_index = 20533,  cooltime = 30000, rate = -1, rangemin = 0, rangemax = 800, target = 4, selfhppercent = 50 },
}
