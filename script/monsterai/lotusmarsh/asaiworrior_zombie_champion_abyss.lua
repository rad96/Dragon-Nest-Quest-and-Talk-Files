--AiAsaiWorrior_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Back", rate = 7, loop = 2 },
   { action_name = "Move_Back", rate = 7, loop = 1 },
   { action_name = "Attack2_Slash", rate = 15, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 7, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Back", rate = 7, loop = 2 },
   { action_name = "Move_Back", rate = 7, loop = 1 },
   { action_name = "Assault", rate = 4, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 8, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 2 },
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Attack7_ThrowSword", rate = -1, loop = 1 },
   { action_name = "Attack6_JumpAttack", rate = 9, loop = 1 },
   { action_name = "Assault", rate = 4, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 3, loop = 3 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 6, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Attack2_Slash", rate = 2, loop = 1 },
}

g_Lua_Skill = { 
--��¦�̴�
   { skill_index = 30503, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, encountertime = 30000 },
}