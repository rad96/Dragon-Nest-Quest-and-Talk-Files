--AiLiving_Book_Black_Normal.lua
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
    { action_name = "Stand_1", rate = 3, loop = 2  },
    { action_name = "Walk_Left", rate = 7, loop = 2 }, 
    { action_name = "Walk_Right", rate = 7, loop = 2 },
    { action_name = "Walk_Back", rate = 20, loop = 2  },
	{ action_name = "Move_Left", rate = 8, loop = 2 },
	{ action_name = "Move_Right", rate = 8, loop = 1 },
	{ action_name = "Move_Back", rate = 15, loop = 2 },
	{ action_name = "Attack2_Crash", rate = 10, loop = 1, max_missradian = 10 },
}
g_Lua_Near2 = { 
	{ action_name = "Stand_1", rate = 5, loop = 2 },
	{ action_name = "Walk_Left", rate = 8, loop = 2 },    
	{ action_name = "Walk_Right", rate = 8, loop = 2 },
	{ action_name = "Walk_Back", rate = 10, loop = 2 },
    { action_name = "Move_Left", rate = 12, loop = 1 },
    { action_name = "Move_Right", rate = 12, loop = 1 },
    { action_name = "Move_Back", rate = 3, loop = 2 },
}
g_Lua_Near3 = { 
	{ action_name = "Stand_1", rate = 5, loop = 2 },
	{ action_name = "Walk_Left", rate = 8, loop = 3 },   
	{ action_name = "Walk_Right", rate = 8, loop = 3  },
	{ action_name = "Walk_Front", rate = 10, loop = 3 },
	{ action_name = "Move_Left", rate = 12, loop = 1 },
	{ action_name = "Move_Right", rate = 12, loop = 1  },
	{ action_name = "Move_Front", rate = 2, loop = 1  },
}
g_Lua_Near4 = { 
	{ action_name = "Walk_Left", rate = 10, loop = 2 },   
	{ action_name = "Walk_Right", rate = 10, loop = 2  },
	{ action_name = "Walk_Front", rate = 5, loop = 2  },
	{ action_name = "Move_Left", rate = 7, loop = 2  },
	{ action_name = "Move_Right", rate = 7, loop = 2  },
	{ action_name = "Move_Front", rate = 5, loop = 2  },
}
g_Lua_Skill = { 
-- 칠링미스트
   { skill_index = 20622,  cooltime = 12000, rate = 10, rangemin = 150, rangemax = 500, target = 3 },
-- 블리자드
   { skill_index = 20621,  cooltime = 15000, rate = 40, rangemin = 0, rangemax = 800, target = 3 },
   
}