--AiAsaiWorrior_Green_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 9999000
g_Lua_GlobalCoolTime2 = 9999000
g_Lua_GlobalCoolTime3 = 9999000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack2_Slash_Boss", rate = 13, loop = 1 },
   { action_name = "Attack1_Pierce_Boss", rate = 13, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 9, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Attack8_Double_Boomerang", rate = 13, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 3 },
   { action_name = "Move_Front", rate = 8, loop = 3 },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Slash_Boss", rate = 10, loop = 1, approach = 150 },
   { action_name = "Attack1_Pierce_Boss", rate = 10, loop = 1, approach = 400 },
}

g_Lua_Skill = { 
-- 호밍 부메랑(슈퍼하울 시작-> 슈퍼하울 끝-> 호밍 부메랑)
   { skill_index = 20489,  cooltime = 38000, rate = 100, rangemin = 0, rangemax = 3500, target = 3, next_lua_skill_index = 1, selfhppercent = 70 },
   { skill_index = 20490,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3500, target = 3, next_lua_skill_index = 2 },
   { skill_index = 20486,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3500, target = 3, multipletarget = "1,3" },
-- 동물 소환(체력75% 정면)
   { skill_index = 20494,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, next_lua_skill_index = 4, limitcount = 1, globalcooltime = 1 },
   { skill_index = 20488,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 동물 소환(체력75% 측면)
   { skill_index = 20494,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, next_lua_skill_index = 6, limitcount = 1, globalcooltime = 1 },
   { skill_index = 20493,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 동물 소환(체력25% 정면)
   { skill_index = 20494,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 25, next_lua_skill_index = 8, limitcount = 1, globalcooltime = 2 },
   { skill_index = 20488,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 동물 소환(체력25% 측면)
   { skill_index = 20494,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 25, next_lua_skill_index = 10, limitcount = 1, globalcooltime = 2 },
   { skill_index = 20493,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 신경독
   { skill_index = 20492,  cooltime = 29000, rate = 80, rangemin = 400, rangemax = 1200, target = 3, next_lua_skill_index = 12, randomtarget = 1.1 },
-- 고기 함정
   { skill_index = 20491,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5500, target = 3 },
-- 점프어택 웨이브
   { skill_index = 20487,  cooltime = 13000, rate = 60, rangemin = 900, rangemax = 1500, target = 3, randomtarget = 1.1 },
-- 대지분쇄 부메랑
   { skill_index = 20484,  cooltime = 25000, rate = 100, rangemin = 0, rangemax = 3500, target = 3, encountertime = 25000 },
-- 난무
   { skill_index = 20495,  cooltime = 19000, rate = 50, rangemin = 0, rangemax = 600, target = 3 },
}