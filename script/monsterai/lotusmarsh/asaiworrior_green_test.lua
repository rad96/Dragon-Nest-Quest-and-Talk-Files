--AiDrakeHatchling_Green.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 120;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_AssaultTime = 3000
g_Lua_ApproachValue = 120

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack6_FireTrample", 0 },
  },
  CustomAction2 = {
      { "Attack7_FireBurst", 0 },
  },
  CustomAction3 = {
      { "Attack4_IceBreath", 0 },
  },
  CustomAction4 = {
      { "Attack5_MiniIceBolt", 0 },
  },
  CustomAction5 = {
      { "Walk_Front", 0 },
      { "Walk_Left", 0 },
      { "Walk_Right", 0 },
      { "Walk_Back", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "CustomAction1", rate = -1, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "CustomAction2", rate = -1, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "CustomAction3", rate = -1, loop = 1, selfhppercentrange = "50,74" },
   { action_name = "CustomAction4", rate = -1, loop = 1, selfhppercentrange = "25,49" },
   { action_name = "CustomAction5", rate = -1, loop = 1, selfhppercentrange = "1,24" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1  },
   { action_name = "CustomAction1", rate = -1, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "CustomAction2", rate = -1, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "CustomAction3", rate = -1, loop = 1, selfhppercentrange = "50,74" },
   { action_name = "CustomAction4", rate = -1, loop = 1, selfhppercentrange = "25,49" },
   { action_name = "CustomAction5", rate = -1, loop = 1, selfhppercentrange = "1,24" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1  },
   { action_name = "CustomAction1", rate = -1, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "CustomAction2", rate = -1, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "CustomAction3", rate = -1, loop = 1, selfhppercentrange = "50,74" },
   { action_name = "CustomAction4", rate = -1, loop = 1, selfhppercentrange = "25,49" },
   { action_name = "CustomAction5", rate = -1, loop = 1, selfhppercentrange = "1,24" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
   { action_name = "CustomAction1", rate = -1, loop = 1, selfhppercentrange = "99,100" },
   { action_name = "CustomAction2", rate = -1, loop = 1, selfhppercentrange = "75,99" },
   { action_name = "CustomAction3", rate = -1, loop = 1, selfhppercentrange = "50,74" },
   { action_name = "CustomAction4", rate = -1, loop = 1, selfhppercentrange = "25,49" },
   { action_name = "CustomAction5", rate = -1, loop = 1, selfhppercentrange = "1,24" },
}

g_Lua_Skill = { 
   { skill_index = 20472, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 500, target = 1, selfhppercentrange = "99,100" },
   { skill_index = 20473, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 500, target = 1, selfhppercentrange = "75,98" },
}
