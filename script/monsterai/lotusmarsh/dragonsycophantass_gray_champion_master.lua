--AiAiDragonSycophantAss_Gray_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Move_Back", 0 },
      { "Attack3_JumpAttack", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 2 },
   { action_name = "Move_Left", rate = 6, loop = 1 },
   { action_name = "Move_Right", rate = 6, loop = 1 },
   { action_name = "Move_Back", rate = 2, loop = 1 },
   { action_name = "Attack1_3Combo", rate = 9, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 3, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "CustomAction1", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 2 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 2 },
   { action_name = "Move_Right", rate = 3, loop = 2 },
   { action_name = "Attack3_JumpAttack", rate = 3, loop = 1  },
   { action_name = "Assault", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 7, loop = 3 },
   { action_name = "Move_Left", rate = 3, loop = 3 },
   { action_name = "Move_Right", rate = 3, loop = 2 },
   { action_name = "Walk_Front", rate = 3, loop = 2 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Assault", rate = 4, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 7, loop = 3 },
   { action_name = "Move_Left", rate = 3, loop = 3 },
   { action_name = "Move_Right", rate = 3, loop = 2 },
   { action_name = "Walk_Front", rate = 3, loop = 2 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_3Combo", rate = 2, loop = 1, approach = 200 },
   { action_name = "Walk_Left", rate = 6, loop = 1, approach = 200 },
   { action_name = "Walk_Right", rate = 6, loop = 1, approach = 200 },
   { action_name = "Move_Back", rate = 2, loop = 1, approach = 200 },
}
g_Lua_Skill = { 
   { skill_index = 20453,  cooltime = 12000, rate = -1,rangemin = 300, rangemax = 900, target = 3 },
   { skill_index = 20454,  cooltime = 3000, rate = 50, rangemin = 0, rangemax = 500, target = 1, selfhppercent = 20, limitcount = 1 },
--è�Ǿ�_��¦�̴�
   { skill_index = 30503, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, encountertime = 30000 },
}
