--AiCube_White_Elite_Easy.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Attack2_Bit_Named", rate = 8, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 8, loop = 2  },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Attack8_Swing", rate = 8, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 6, loop = 1 },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Move_Front", rate = 7, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 6, loop = 2  },
   { action_name = "Move_Front", rate = 8, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20921,  cooltime = 33000, rate = 100,rangemin = 400, rangemax = 900, target = 3 },
   { skill_index = 20922,  cooltime = 33000, rate = -1, rangemin = 400, rangemax = 900, target = 3 },
   { skill_index = 20923,  cooltime = 45000, rate = -1, rangemin = 0, rangemax = 400, target = 3 },
   { skill_index = 20924,  cooltime = 45000, rate = -1, rangemin = 0, rangemax = 900, target = 3 },
   { skill_index = 20925,  cooltime = 27000, rate = 100, rangemin = 0, rangemax = 300, target = 3 },
   { skill_index = 20930,  cooltime = 27000, rate = -1, rangemin = 0, rangemax = 500, target = 3 },
   { skill_index = 20927,  cooltime = 45000, rate = -1, rangemin = 0, rangemax = 1200, target = 3, selfhppercent = 50 },
   { skill_index = 20928,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 75 },
   { skill_index = 20929,  cooltime = 70000, rate = -1, rangemin = 400, rangemax = 1500, target = 3, selfhppercent = 75 },
}
