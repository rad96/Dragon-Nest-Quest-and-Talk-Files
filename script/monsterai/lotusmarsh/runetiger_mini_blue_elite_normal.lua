--AiRunTiger_Blue_Elite_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4,  },
   { action_name = "Walk_Left", rate = 4,  },
   { action_name = "Walk_Right", rate = 4,  },
   { action_name = "Walk_Front", rate = 5,  },
   { action_name = "Attack1_Claw", rate = 13,  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3,  },
   { action_name = "Walk_Left", rate = 7,  },
   { action_name = "Walk_Right", rate = 7,  },
   { action_name = "Walk_Front", rate = 9,  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3,  },
   { action_name = "Walk_Left", rate = 5,  },
   { action_name = "Walk_Right", rate = 5,  },
   { action_name = "Walk_Front", rate = 18,  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3,  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 5,  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5,  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 20,  },
}
