--AiDrakeHatchling_Red_Easy.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Attack1_Bite", rate = 2, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 2 },
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
   { action_name = "Assault", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 2 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Assault", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 3 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Bite", rate = 2, loop = 1, approach = 200, max_missradian = 30 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 200 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 200 },
}
g_Lua_Skill = { 
   { skill_index = 20405,  cooltime = 40000, rate = 20,rangemin = 100, rangemax = 300, target = 3, max_missradian = 30 },
   { skill_index = 20404,  cooltime = 25000, rate = -1, rangemin = 500, rangemax = 900, target = 3, max_missradian = 30, selfhppercent = 50 },
}
