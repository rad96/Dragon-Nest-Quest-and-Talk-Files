--DragonSycophantPuni_Green_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack2_Slash", 0 },
      { "Attack3_JumpChopping", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_Chopping", rate = 7, loop = 1  },
   { action_name = "CustomAction1", rate = 15, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_Chopping", rate = 20, loop = 1  },
   { action_name = "Assault", rate = 7, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Assault", rate = 27, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Assault", rate = 45, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "CustomAction1", rate = 5, loop = 1, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 20383,  cooltime = 60000, rate = 100,rangemin = 0, rangemax = 2000, target = 1, selfhppercent = 50 },
   { skill_index = 20384,  cooltime = 20000, rate = 80, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100 },
   { skill_index = 20382,  cooltime = 60000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 70 },
   { skill_index = 20381,  cooltime = 60000, rate = -1, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 40 },
   { skill_index = 20386,  cooltime = 60000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 100 },
}
