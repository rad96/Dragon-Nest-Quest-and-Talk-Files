--Runetiger_Asai_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_4", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Attack1_Claw_Asai", rate = 9, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_4", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Attack6_DashAttack", rate = 9, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_4", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 3 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 13, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_4", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 6 },
   { action_name = "Walk_Right", rate = 6 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 13, loop = 1 },
}

g_Lua_Assault = { 
   { action_name = "Attack1_Claw", rate = 10, loop = 1, approach = 250.0  },
}