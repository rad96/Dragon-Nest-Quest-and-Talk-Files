--AiAsaiShaman_Blue_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Move_Back", rate = 5, loop = 2 },
   { action_name = "Stand_1", rate = 5, loop = 2 },
   { action_name = "Walk_Left", rate = 1, loop = 2 },
   { action_name = "Walk_Right", rate = 1, loop = 2 },
   { action_name = "Move_Left", rate = 1, loop = 1 },
   { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2 },
   { action_name = "Walk_Front", rate = 2, loop = 2 },
   { action_name = "Walk_Left", rate = 2, loop = 2 },
   { action_name = "Walk_Right", rate = 2, loop = 2 },
   { action_name = "Move_Front", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 5, loop = 2 },
   { action_name = "Walk_Left", rate = 2, loop = 2 },
   { action_name = "Walk_Right", rate = 2, loop = 2 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
}
g_Lua_Skill = { 
--약병 던지기
   { skill_index = 20522, cooltime = 19000, rate = 100, rangemin = 100, rangemax = 800, max_missradian = 30, target = 3 },
--빙산 소환
   { skill_index = 20525, cooltime = 20000, rate = 100, rangemin = 300, rangemax = 800, target = 3, limitcount = 3, selfhppercent = 30 },
--버프
   { skill_index = 20528, cooltime = 12000, rate = 100, rangemin = 0, rangemax = 1500, target = 4, limitcount = 1, selfhppercent = 50 },
   --반짝이는
   { skill_index = 30503, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, encountertime = 30000 },
}