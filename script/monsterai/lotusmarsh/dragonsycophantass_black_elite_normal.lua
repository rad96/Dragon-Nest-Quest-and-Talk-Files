--AiDragonSycophantAss_Black.lua
--근접 거리에서 Stand 액션 없음, 지속적인 움직임

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Down|!Attack", "Air" }, 
  State3 = {"!Stay|!Move|!Stiff|!Attack", "Stun" }, 
}

g_Lua_Near1 = { 
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Back", rate = 5, loop = 2 },
   { action_name = "Move_Left", rate = 5, loop = 2 },
   { action_name = "Move_Right", rate = 5, loop = 2 },
   { action_name = "Move_Back", rate = 5, loop = 2 },
   { action_name = "Attack1_3Combo", rate = 4, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
   { action_name = "Attack3_JumpAttack", rate = 4, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 2 },
   { action_name = "Move_Left", rate = 3, loop = 2 },
   { action_name = "Move_Right", rate = 3, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Move_Left", rate = 3, loop = 2 },
   { action_name = "Move_Right", rate = 3, loop = 2 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 20454, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 1000, target = 1, selfhppercent = 20,limitcount=1},
}