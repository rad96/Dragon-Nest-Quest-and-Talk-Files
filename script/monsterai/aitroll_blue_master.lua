--AiTroll_Blue_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Claw", rate = 9, loop = 1, target_condition = "State1" },
   { action_name = "Attack3_RollingAttackS_N", rate = 2, loop = 1, target_condition = "State1", cooltime = 10000 },
   { action_name = "Attack6_Bite", rate = -1, loop = 1, target_condition = "State1" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
   { action_name = "Attack3_RollingAttack_N", rate = 2, loop = 1, target_condition = "State1", cooltime = 10000 },
   { action_name = "Attack3_RollingAttackS_N", rate = 8, loop = 1, target_condition = "State1", cooltime = 10000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 7, loop = 3  },
   { action_name = "Move_Right", rate = 7, loop = 3  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
   { action_name = "Move_Back", rate = 3, loop = 3  },
   { action_name = "Attack3_RollingAttack_N", rate = 6, loop = 1, target_condition = "State1", cooltime = 10000 },
   { action_name = "Attack3_RollingAttackS_N", rate = 6, loop = 1, target_condition = "State1", cooltime = 10000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = -1, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 3  },
   { action_name = "Move_Right", rate = 5, loop = 3  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
   { action_name = "Move_Back", rate = 1, loop = 3  },
   { action_name = "Attack3_RollingAttack", rate = 9, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack3_RollingAttack", rate = 7, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
