--AiGoblin_Black_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_Slash", rate = 12, loop = 1  },
   { action_name = "Attack2_Upper", rate = 12, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Attack4_FastJumpAttack", rate = 1, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 2, loop = 2 },
   { action_name = "Move_Front", rate = 2, loop = 1 },
   { action_name = "Attack4_FastJumpAttack", rate = 8, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Stand2", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 4, loop = 2  },
   { action_name = "Assault", rate = 4, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Upper", rate = 5, loop = 1 },
   { action_name = "Attack1_Slash", rate = 3, loop = 1 },
}
