--Event_BossRush_Goblin_Green.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 1500;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 1, loop = 1 },
   { action_name = "Move_Back", rate = 45, loop = 2 },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 1, loop = 1 },
   { action_name = "Move_Back", rate = 45, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Left", rate = 3, loop = 2 },
   { action_name = "Walk_Right", rate = 3, loop = 2 },
   { action_name = "Move_Left", rate = 3, loop = 2 },
   { action_name = "Move_Right", rate = 3, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 45, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand2", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 45, loop = 2 },
}