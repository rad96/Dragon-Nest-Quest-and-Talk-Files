-- Manticore Dummy A.I
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 400.0;
g_Lua_NearValue2 = 700.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 1800.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 320;
g_Lua_AssualtTime = 3000;


g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR" },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Back",	rate = 15,		loop = 1, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Turn_Left",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td1 = "LF", td2 = "LB", td3 = "BL" },
	{ action_name = "Attack4_Tail_L",	rate = 10,		loop = 1, custom_state1 = "custom_ground", td1 = "LF", td2 = "LB", td3 = "BL", td4 = "BR" },
	{ action_name = "Turn_Right",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td1 = "RF", td2 = "RB", td3 = "BR" },
	{ action_name = "Attack5_Tail_R",	rate = 10,		loop = 1, custom_state1 = "custom_ground", td1 = "RF", td2 = "RB", td3 = "BL", td4 = "BR" },
	{ action_name = "Fly_Start",	rate = -1,		loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Fly",		rate = -1,		loop = 1, custom_state1 = "custom_fly" },
	{ action_name = "Attack7_Swoop",	rate = 5,		loop = 1, custom_state1 = "custom_fly", max_missradian = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 10,		loop = 1, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR" },
	{ action_name = "Walk_Left",	rate = 10,		loop = 3, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Right",	rate = 10,		loop = 3, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Front",	rate = 20,		loop = 3, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Back",	rate = 5,		loop = 1, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Turn_Left",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td1 = "LF", td2 = "LB", td3 = "BL" },
	{ action_name = "Turn_Right",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td1 = "RF", td2 = "RB", td3 = "BR" },
	{ action_name = "Fly_Start",	rate = -1,		loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Fly",		rate = 5,		loop = 1, custom_state1 = "custom_fly" },
	{ action_name = "Attack9_Dash",	rate = -1,		loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Attack7_Swoop",	rate = 10,		loop = 1, custom_state1 = "custom_fly", max_missradian = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 10,		loop = 1, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR" },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Front",	rate = 30,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Back",	rate = 5,		loop = 1, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Turn_Left",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td1 = "LF", td2 = "LB", td3 = "BL" },
	{ action_name = "Turn_Right",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td1 = "RF", td2 = "RB", td3 = "BR" },
	{ action_name = "Attack9_Dash",	rate = -1,		loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Fly_Start",	rate = -1,		loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Fly",		rate = 5,		loop = 1, custom_state1 = "custom_fly" },
	{ action_name = "Attack7_Swoop",	rate = 10,		loop = 1, custom_state1 = "custom_fly", max_missradian = 1 },

}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR" },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Front",	rate = 20,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Turn_Left",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td1 = "LF", td2 = "LB", td3 = "BL" },
	{ action_name = "Turn_Right",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td1 = "RF", td2 = "RB", td3 = "BR" },
	{ action_name = "Attack9_Dash",	rate = -1,		loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Fly_Start",	rate = -1,		loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Fly",		rate = 5,		loop = 1, custom_state1 = "custom_fly" },
	{ action_name = "Attack7_Swoop",	rate = 10,		loop = 1, custom_state1 = "custom_fly", max_missradian = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 10,		loop = 1, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR" },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Walk_Front",	rate = 20,		loop = 2, custom_state1 = "custom_ground", td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ action_name = "Turn_Left",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td1 = "LF", td2 = "LB", td3 = "BL" },
	{ action_name = "Turn_Right",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td1 = "RF", td2 = "RB", td3 = "BR" },
	{ action_name = "Fly_Start",	rate = -1,		loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Fly",		rate = 5,		loop = 1, custom_state1 = "custom_fly" },
	{ action_name = "Attack7_Swoop",	rate = 10,		loop = 1, custom_state1 = "custom_fly", max_missradian = 1 },
}

g_Lua_Skill=
{
	{ skill_index = 30001, SP = 0, cooltime = 30000, rangemin = 0, rangemax = 1000, target = 3, hppercent = 100,  custom_state1 = "custom_ground",rate = -1, td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ skill_index = 30002, SP = 0, cooltime = 30000, rangemin = 0, rangemax = 600, target = 3, hppercent = 100,  custom_state1 = "custom_ground", rate = -1, td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ skill_index = 30003, SP = 0, cooltime = 30000, rangemin = 500, rangemax = 1000, target = 3, hppercent = 100,  custom_state1 = "custom_ground", rate = -1, td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ skill_index = 30004, SP = 0, cooltime = 60000, rangemin = 0, rangemax = 600, target = 3, hppercent = 100,  custom_state1 = "custom_ground", rate = 50, td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ skill_index = 30005, SP = 0, cooltime = 30000, rangemin = 600, rangemax = 1800, target = 3, hppercent = 100, custom_state1 = "custom_ground", rate = -1, td1 = "FL", td2 = "FR", td3 = "LF", td4 = "RF" },
	{ skill_index = 30006, SP = 0, cooltime = 5000, rangemin = 0, rangemax = 1800, target = 3, hppercent = 100,  custom_state1 = "custom_fly", rate = -1 },
}