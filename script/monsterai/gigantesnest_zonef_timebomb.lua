--Mimic_Gold_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 31861,  cooltime = 5000, rate = 100,rangemin = 0, rangemax = 3000, target = 3 },
 }
