g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300 --몬스터가 돌격시에 타겟에게 다가가는 최소거리
g_Lua_AssualtTime = 9000 --Assault 명령을 받고 돌격 할 때 타겟을 쫓아가는 시간. 


g_Lua_Near1 = {  
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Back", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 1, loop = 2 },
   { action_name = "Assault", rate = 1000, loop = 1 ,  cooltime = 10000 },
}
 
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
}

g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 5  },
}

g_Lua_Assault = { { lua_skill_index = 1, rate = 10, approach = 350},} -- 텔레포트

g_Lua_Skill = { --buff dance skill
   --{ skill_index = 36801,  cooltime = 5000, rate = 1, loop = 1, rangemin = 0, rangemax = 2000, target = 3}, 
   { skill_index = 36807,  cooltime = 10000, rate = 1, loop = 1, rangemin = 0, rangemax = 350, target = 1}, 
}


--[[
g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300 --몬스터가 돌격시에 타겟에게 다가가는 최소거리
g_Lua_AssualtTime = 5000 --Assault 명령을 받고 돌격 할 때 타겟을 쫓아가는 시간. 

g_Lua_CustomAction = {
  CustomAction1 = { 
      { "Attack16_Teleport_Front_Start", 0 },
      { "Attack16_Teleport_Front_Loop", 10 },
	  { "Attack16_Teleport_End", 0 },
  },
  
  CustomAction2 = {
      { "Walk_Right", 0 },
      { "Attack1_Bite", 0 },
  },
 }

g_Lua_Near1 = {  
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Back", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 1, loop = 2 },
   { action_name = "Attack4_BiteFull", rate = 10, loop = 1  },
   { action_name = "CustomAction1", rate = 1, loop = 1  },
}
 
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "CustomAction1", rate = 1, loop = 1  },
}

g_Lua_Near3 = { 
   { action_name = "Walk_Front", rate = 10, loop = 5  },
}


g_Lua_Skill = { --buff dance skill
   { skill_index = 36801,  cooltime = 5000, rate = 1, loop = 1, rangemin = 0, rangemax = 2000, target = 3}, 
}
--]]