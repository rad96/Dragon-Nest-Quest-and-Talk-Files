--AiShadow_Fire_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 450;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_Claw", rate = 22, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 11, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Walk_Left", rate = 15, loop = 2  },
   { action_name = "Walk_Right", rate = 15, loop = 2  },
   { action_name = "Walk_Back", rate = 26, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 13, loop = 1  },
   { action_name = "Attack1_Claw", rate = 60, loop = 1  },
   { action_name = "Attack2_Curve", rate = 60, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 20, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack1_Claw", rate = 90, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 13, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = -1, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
}
g_Lua_NearNonDownMeleeDamage = { 
   { action_name = "Attack1_Claw", rate = 27, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 20, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack1_Claw", rate = 34, loop = 1  },
   { action_name = "Move_Left", rate = 15, loop = 2  },
   { action_name = "Move_Right", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20242,  cooltime = 17000, rate = 70,rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 50 },
   { skill_index = 20243,  cooltime = 17000, rate = 70, rangemin = 200, rangemax = 700, target = 3, selfhppercent = 100 },
   { skill_index = 20244,  cooltime = 17000, rate = 100, rangemin = 000, rangemax = 400, target = 3, selfhppercent = 100 },
}
