
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
 State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_CustomAction = {
-- 3연타 공격
  CustomAction1 = {
      { "Shoot_Stand_BigBow" },
      { "Shoot_Stand_BigBow" },
      { "Shoot_Finish_BigBow" },
  },
-- 공격 우이동 공격
  CustomAction2 = {
      { "Move_Right" },
      { "Shoot_Stand_BigBow" },
  },
-- 공격 좌이동 공격
  CustomAction3 = {
      { "Move_Left" },
      { "Shoot_Stand_BigBow" },
  },
-- 공격 정면 이동어택
  CustomAction4 = {
     { "Move_Front" },
     { "Shoot_Stand_BigBow" },
  },
-- 공격 뒤로 이동 어택
  CustomAction5 = {
     { "Move_Back" },
     { "Shoot_Stand_BigBow" },
  },
-- 백 덤블링 어택
  CustomAction6 = {
     { "Tumble_Back" },
     { "Shoot_Stand_BigBow" },
  },
}

g_Lua_GlobalCoolTime1 = 8000
g_Lua_GlobalCoolTime2 = 13000

g_Lua_Near1 = 
{
     { action_name = "Attack_Down", rate = 30, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Stand", rate = 2, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 10, loop = 2 },
     { action_name = "Tumble_Back", rate = 10, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Left", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 2, loop = 1, cooltime = 20000, globalcooltime = 1 },
     { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, cooltime = 7000 },
     { action_name = "Attack_SideKick", rate = 12, loop = 1, cooltime = 30000, globalcooltime = 1 },
     { action_name = "CustomAction5", rate = 10, loop = 1, cooltime = 12000, globalcooltime = 1 },
     { action_name = "CustomAction6", rate = 10, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Back", rate = 1, loop = 1 },
     { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, cooltime = 7000 },
--3연타 공격
     { action_name = "CustomAction1", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
--우측이동 후 3타
     { action_name = "CustomAction2", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 8, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
     { action_name = "Move_Front", rate = 5, loop = 1 },
--3연타 공격
     { action_name = "CustomAction1", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
     { action_name = "CustomAction2", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
     { action_name = "CustomAction3", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
     { action_name = "CustomAction4", rate = 8, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "CustomAction1", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
     { action_name = "CustomAction4", rate = 8, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 4 },
}

g_Lua_NonDownMeleeDamage = {
      { action_name = "CustomAction4", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "CustomAction5", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "Move_Left", rate = 10, loop = 1 },
      { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_NonDownRangeDamage = {
      { action_name = "Stand", rate = 2, loop = 1 },
      { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, globalcooltime = 1 },
      { action_name = "CustomAction1", rate = 10, loop = 1, globalcooltime = 1 },
}

g_Lua_Skill = {
-- 아울즈레이지
   { skill_index = 31048,  cooltime = 60000, rate = 100, rangemin = 000, rangemax = 1500, target = 2, selfhppercent = 100, globalcooltime = 2 },
-- 애로우샤워 35000
   { skill_index = 31044,  cooltime = 30000, rate = 60, rangemin = 500, rangemax = 1500, target = 3, selfhppercent = 30, globalcooltime = 2 },
-- 스위프트샷 18000
   { skill_index = 31045,  cooltime = 23000, rate = 80, rangemin = 0, rangemax = 650, target = 3, selfhppercent = 30, td = "FL,FR,LF,RF,RB,BR,BL,LB", globalcooltime = 2 },
-- 레피드 샷
   { skill_index = 31047,  cooltime = 15000, rate = 80, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR", selfhppercent = 70, globalcooltime = 2 },
-- 익스텐션에로우
   { skill_index = 31046,  cooltime = 17000, rate = 40, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR", globalcooltime = 2 },
-- 차지샷 15000 (600+0+300)
   { skill_index = 31041,  cooltime = 15000, rate = 90, rangemin = 300, rangemax = 800, target = 3, globalcooltime = 2 },
 
-- 트윈샷 4500 (600+80+200)
--   { skill_index = 31031,  cooltime = 30000, rate = 30, rangemin = 0, rangemax = 800, target = 3 },
-- 윌로우스핀킥 12000
   { skill_index = 31034,  cooltime = 20000, rate = 20, rangemin = 0, rangemax = 300, target = 3 },
-- 멀티샷 28000(무기사거리 포함X)
   { skill_index = 31032,  cooltime = 35000, rate = 30, rangemin = 0, rangemax = 500, target = 3 },
-- 매직애로우 13000 (600+500+0)
   { skill_index = 31033,  cooltime = 15000, rate = 40, rangemin = 0, rangemax = 1100, target = 3 },
}