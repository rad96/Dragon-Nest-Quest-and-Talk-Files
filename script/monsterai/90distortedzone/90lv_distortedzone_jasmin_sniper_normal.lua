--AiJasmin_Sniper_Adept_Normal.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Move_Left", rate = 8, loop = 1, cooltime = 10000 },
   { action_name = "Move_Right", rate = 8, loop = 1, cooltime = 10000 },
   { action_name = "Walk_Back", rate = 9, loop = 1, cooltime = 8000 },
   { action_name = "Attack02_MehaHammer", rate = 5, loop = 1, cooltime = 10000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 9, loop = 1 },
   { action_name = "Walk_Right", rate = 9, loop = 1 },
   { action_name = "Attack01_QuickShot", rate = 8, loop = 1, cooltime = 13000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 1 },
}

g_Lua_Skill = { 
	{ skill_index = 500469, cooltime = 20000, rate = 30, rangemin = 200, rangemax = 1200, target = 3 }, 
}