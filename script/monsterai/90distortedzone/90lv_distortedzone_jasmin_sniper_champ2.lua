-- 90lv_DistortedZone_Jasmin_Sniper_Champ2.lua
g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand_1", rate = 8, loop = 1 },
	{ action_name = "Walk_Back", rate = 9, loop = 1, cooltime = 5000 },
	{ action_name = "Move_Left", rate = 6, loop = 1, globalcooltime = 1 },
	{ action_name = "Move_Right", rate = 6, loop = 1, globalcooltime = 1 },
	{ action_name = "Attack02_MehaHammer", rate = 20, loop = 1, cooltime = 10000 },
}
g_Lua_Near2 = { 
	{ action_name = "Stand_1", rate = 4, loop = 1 },
	{ action_name = "Walk_Front", rate = 2, loop = 2 },
	{ action_name = "Walk_Left", rate = 9, loop = 1 },
	{ action_name = "Walk_Right", rate = 9, loop = 1 },
	{ action_name = "Attack01_QuickShot", rate = 20, loop = 1, cooltime = 13000 },
}
g_Lua_Near3 = { 
	{ action_name = "Stand_1", rate = 4, loop = 1 },
	{ action_name = "Walk_Front", rate = 15, loop = 1 },
	{ action_name = "Walk_Left", rate = 9, loop = 1 },
	{ action_name = "Walk_Right", rate = 9, loop = 1 },
	{ action_name = "Attack06_Poison", rate = 20, loop = 1, cooltime = 24000 },
}
g_Lua_Skill = { 
	{ skill_index = 33331,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1 },
	{ skill_index = 500469, cooltime = 15000, rate = 30, rangemin = 200, rangemax = 1200, target = 3 }, 
}
