-- 90lv_DistortedZone_Crow_Owl_elite.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Move_Back", rate = 10, loop = 1 },
   { action_name = "Attack01_Normal", rate = 15, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 1 },
   { action_name = "Move_Right", rate = 3, loop = 1 },
   { action_name = "Attack05_Stab", rate = 10, loop = 1 },

}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Move_Front", rate = 7, loop = 3 },

}

g_Lua_Skill = { 
   { skill_index = 33582,  cooltime = 30000, rate = 70, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "0,60" },
}
