--90lv_DistortedZone_TyphoonKim_Boss_Abyss.lua
--/genmon 704209
g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 1200.0;
g_Lua_NearValue4 = 3000.0;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 8000

g_Lua_GlobalCoolTime1 = 50000

g_Lua_Near1 = 
	{ 
		{ action_name = "Stand", rate = 36, loop = 1 },
		{ action_name = "Walk_Left", rate = 15, loop = 1 },
		{ action_name = "Walk_Right", rate = 15, loop = 1 },
	}
g_Lua_Near2 = 
	{ 
		{ action_name = "Stand", rate = 28, loop = 1 },	
		{ action_name = "Walk_Left", rate = 18, loop = 1 },
		{ action_name = "Walk_Right", rate = 18, loop = 1 },		
	}
g_Lua_Near3 = 
	{ 
		{ action_name = "Stand", rate = 30, loop = 1 },
		{ action_name = "Move_Front", rate = 13, loop = 1 },
		{ action_name = "Move_Left", rate = 15, loop = 1 },
		{ action_name = "Move_Right", rate = 15, loop = 1 },		
	}
g_Lua_Near4 = 
	{ 
		{ action_name = "Stand", rate = 20, loop = 1 },
		{ action_name = "Move_Front", rate = 15, loop = 1 },
		{ action_name = "Walk_Front", rate = 25, loop = 1 },
	}
g_Lua_Skill = 
	{ 
	   { skill_index = 500557,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, },--Attack014_WhirlWind_Start
	   { skill_index = 500555,  cooltime = 90000, rate = 70, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 60, next_lua_skill_index = 0, },--Attack007_Stomp_DistortedZone
	   { skill_index = 500552,  cooltime = 10000, rate = 30, rangemin = 0, rangemax = 300, target = 3, },--Attack003_FullSwing
	   { skill_index = 500556,  cooltime = 20000, rate = 50, rangemin = 800, rangemax = 1200, target = 3,},--Attack013_JumpChop_DistortedZone
	   { skill_index = 500551,  cooltime = 25000, rate = 70, rangemin = 0, rangemax = 500, target = 3, },--Attack002_Combo 
	   { skill_index = 500553,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 1200, target = 3, encountertime = 7000 },--Attack004_StraightArm
	   { skill_index = 500554,  cooltime = 35000, rate = 90, rangemin = 500, rangemax = 2000, target = 3, },--Attack005_GreatBomb
	}