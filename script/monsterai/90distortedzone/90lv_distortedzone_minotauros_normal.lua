-- 90lv_DistortedZone_Minotauros_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Stand_1", rate = 6, loop = 1 },
	{ action_name = "Walk_Left", rate = 8, loop = 2 },
	{ action_name = "Walk_Right", rate = 8, loop = 2 },
	{ action_name = "Walk_Back", rate = 10, loop = 1 },
	{ action_name = "Attack2_Slash_90lv_DistortedZone", rate = 5, loop = 1 },
	{ action_name = "Attack1_bash_90lv_DistortedZone", rate = 6, loop = 1 },
}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Stand_1", rate = 10, loop = 1 },
	{ action_name = "Walk_Front", rate = 8, loop = 1 },
	{ action_name = "Attack2_Slash_90lv_DistortedZone", rate = 5, loop = 1 },
	{ action_name = "Attack1_bash_90lv_DistortedZone", rate = 6, loop = 1 },
}
g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Stand_1", rate = 6, loop = 1 },
	{ action_name = "Walk_Left", rate = 8, loop = 1 },
	{ action_name = "Walk_Right", rate = 8, loop = 1 },
	{ action_name = "Move_Front", rate = 16, loop = 1 },
}
g_Lua_Near4 = { 
	{ action_name = "Stand", rate = 1, loop = 1 },
	{ action_name = "Stand_1", rate = 6, loop = 1 },
	{ action_name = "Move_Front", rate = 8, loop = 1 },
	{ action_name = "Assault", rate = 12, loop = 1, cooltime = 15000 },
}
g_Lua_Assault = { 
	{ action_name = "Stand", rate = 15, loop = 1, approach = 300.0 },  
	{ action_name = "Attack1_bash_90lv_DistortedZone", rate = 10, loop = 1 },
}
