-- 90lv_DistortedZone_Minotauros_Black_Normal.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Stand_1", rate = 10, loop = 1 },
	{ action_name = "Move_Left", rate = 6, loop = 2  },
	{ action_name = "Move_Right", rate = 6, loop = 2 },
	{ action_name = "Move_Back", rate = 5, loop = 2 },
	{ action_name = "Attack4_Pushed", rate = 12, loop = 1 },
	{ action_name = "Attack1_bash_Red", rate = 12, loop = 1 },

}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Stand_1", rate = 10, loop = 1 },
	{ action_name = "Move_Left", rate = 6, loop = 2 },
	{ action_name = "Move_Right", rate = 6, loop = 2 },
	{ action_name = "Attack1_bash_Red", rate = 12, loop = 1, cooltime = 6000 },
}
g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Move_Left", rate = 5, loop = 2 },
	{ action_name = "Move_Right", rate = 5, loop = 2 },
	{ action_name = "Move_Front", rate = 18, loop = 1 },
	{ action_name = "Assault", rate = 10, loop = 1, cooltime = 10000 },
}

g_Lua_Assault = { 
	{ action_name = "Stand", rate = 2, loop = 1, approach = 300.0 },
	{ action_name = "Attack4_Pushed", rate = 8, loop = 1, approach = 100.0 },
}

g_Lua_Skill = { 
   { skill_index = 500496, cooltime = 15000, rate = 80, rangemin = 100, rangemax = 1000, target = 3 },
}
