--AiOgre_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_3", rate = 4, loop = 1},
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_3", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 20, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
}

g_Lua_Assault = { 
   { lua_skill_index = 1, rate = 16, approach = 300 },
}

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Skill = {
   { skill_index = 500571, cooltime = 25000, rate = 100, rangemin = 400, rangemax = 800, target = 3, randomtarget = "1.6,0,1", td = "LF,RF,LB,RB" },-- ���� ������Ʈ
   { skill_index = 500572, cooltime = 11000, rate = 70, rangemin = 0, rangemax = 1100, target = 3, td = "FL,FR" },-- ����Ʈ�� ��Ʈ
   { skill_index = 500573, cooltime = 12000, rate = 60, rangemin = 0, rangemax = 400, target = 3 },-- ������Ʈ
   { skill_index = 500574, cooltime = 30000, globalcooltime = 1, rate = 70, priority = 30, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80 },-- ���շ��� - ����Ʈ��
   { skill_index = 500575, cooltime = 30000, globalcooltime = 1, rate = 70, priority = 30, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 40 },-- ���շ��� - Ȧ��
   { skill_index = 500576, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 1100, target = 3 },-- Ȧ�� ����Ʈ
}


