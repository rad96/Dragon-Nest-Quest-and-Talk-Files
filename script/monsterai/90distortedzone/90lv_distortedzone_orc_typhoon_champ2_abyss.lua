--TypoonKimNest_2Gate_TypoonKim_Orc_soldier.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 450;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Guard_Walk_Back", rate = 10, loop = 1  },
   { action_name = "Guard_Stand", rate = 10, loop = 1  },
   { action_name = "Attack2_bash", rate = 4, loop = 1  },
   { action_name = "Attack1_Pushed", rate = 3, loop = 1  },
   { action_name = "Attack3_Air", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Guard_Stand", rate = 10, loop = 1  },
   { action_name = "Guard_Walk_Front", rate = 15, loop = 1  },
   { action_name = "Guard_Walk_Left", rate = 5, loop = 2  },
   { action_name = "Guard_Walk_Right", rate = 5, loop = 2  },
   { action_name = "Guard_Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Pushed", rate = 3, loop = 1  },
   { action_name = "Attack2_bash", rate = 6, loop = 1  },
   { action_name = "Attack3_Air", rate = 2, loop = 1  },
   { action_name = "Attack9_DashAttack", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Guard_Stand", rate = 10, loop = 1  },
   { action_name = "Guard_Walk_Left", rate = 5, loop = 2  },
   { action_name = "Guard_Walk_Right", rate = 5, loop = 2  },
   { action_name = "Guard_Walk_Front", rate = 15, loop = 2  },
   { action_name = "Guard_Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack2_bash", rate = 1, loop = 1  },
   { action_name = "Assault", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 3  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
}

g_Lua_MeleeDefense = { 
   { action_name = "Attack3_Air", rate = -1, loop = 1  },
   { action_name = "Guard_Walk_Left", rate = 5, loop = 2  },
   { action_name = "Guard_Walk_Right", rate = 5, loop = 2  },
}
g_Lua_RangeDefense = { 
   { action_name = "Assault", rate = 30, loop = 1  },
   { action_name = "Guard_Walk_Left", rate = 1, loop = 2  },
   { action_name = "Guard_Walk_Right", rate = 1, loop = 2  },
   { action_name = "Guard_Walk_Front", rate = 10, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 30, loop = 1  },
   { action_name = "Guard_Walk_Left", rate = 1, loop = 2  },
   { action_name = "Guard_Walk_Right", rate = 1, loop = 2  },
   { action_name = "Guard_Walk_Front", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack7_Nanmu_Black", rate = 30, loop = 2, cancellook = 0, approach = 250.0  },
   { action_name = "Attack9_DashAttack", rate = 30, loop = 2, cancellook = 0, approach = 250.0  },
}
g_Lua_Skill = { 
	  { skill_index = 33331, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1 },--챔피언 
}