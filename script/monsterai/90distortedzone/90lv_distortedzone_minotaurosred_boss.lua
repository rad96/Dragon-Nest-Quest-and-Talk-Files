--90lv_DistortedZone_MinotaurosRed_Boss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 3, loop = 1 },
	{ action_name = "Walk_Back", rate = 5, loop = 2 },
	{ action_name = "Move_Left", rate = 3, loop = 3, td = "FR,RF", cooltime = 5000 },
	{ action_name = "Move_Right", rate = 3, loop = 3, td = "FL,LF", cooltime = 5000 },
	{ action_name = "Attack4_Pushed_90lv_DistortedZone_Boss", rate = 12, loop = 1, cooltime = 6000 },
	{ action_name = "Attack8_Stamp_90lv_DistortedZone_Boss", rate = 12, loop = 1, cooltime = 8000 },
}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Walk_Front", rate = 5, loop = 2 },
	{ action_name = "Move_Left", rate = 3, loop = 3, td = "FR,RF", cooltime = 5000 },
	{ action_name = "Move_Right", rate = 3, loop = 3, td = "FL,LF", cooltime = 5000 },
	{ action_name = "Attack1_bash_Red_90lv_DistortedZone_Boss", rate = 16, loop = 1, cooltime = 6000 },
	{ action_name = "Attack2_Slash_90lv_DistortedZone_Boss", rate = 16, loop = 1, cooltime = 6000 },
}
g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 2, loop = 1 },
	{ action_name = "Move_Front", rate = 15, loop = 1 },
	{ action_name = "Assault", rate = 20, loop = 1 },
}
g_Lua_Near4 = { 
	{ action_name = "Stand", rate = 2, loop = 1 },
	{ action_name = "Move_Front", rate = 20, loop = 1 },
	{ action_name = "Assault", rate = 15, loop = 1 },
}

g_Lua_Assault = { 
	{ action_name = "Attack1_bash_Red_90lv_DistortedZone_Boss", rate = 8, loop = 1, approach = 250.0 },
	{ lua_skill_index = 0, rate = 8, loop = 1, approach = 450.0, selfhppercentrange = "0,80" },
}

g_Lua_GlobalCoolTime1 = 8000 --Attack1_GiantBash,Attack2_GiantSlash
g_Lua_GlobalCoolTime2 = 20000 --IceBash

g_Lua_Skill = { 

-- next_lua_Skill_index
	-- 0:Attack1_GiantBash_90lv_DistortedZone_Boss
	{ skill_index = 500482, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 1500, target = 3 },
	-- 1: Attack3_DashAttack_90lv_DistortedZone_Boss
	{ skill_index = 500485, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 1500, target = 3 },

-- Ai
	--Attack1_GiantBash_90lv_DistortedZone_Boss
	{ skill_index = 500482, cooltime = 8000, rate = 30, rangemin = 0, rangemax = 600, target = 3, encountertime = 10000 },
	--Attack2_GiantSlash_90lv_DistortedZone_Boss
	{ skill_index = 500484, cooltime = 8000, globalcooltime = 1, rate = 30, rangemin = 100, rangemax = 600, target = 3, encountertime = 12000 },
	{ skill_index = 500484, cooltime = 8000, globalcooltime = 1, rate = 30, rangemin = 0, rangemax = 600, target = 3, next_lua_skill_index = 0 },
	--Attack3_DashAttack_90lv_DistortedZone_Boss
	{ skill_index = 500485, cooltime = 10000, rate = 50, rangemin = 200, rangemax = 1500, target = 3, selfhppercentrange = "0,98" },
	--Attack9_FireBash_90lv_DistortedZone_Boss
	{ skill_index = 500489, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 500, target = 3, selfhppercentrange = "0,80" },
	--Attack9_IceBash_90lv_DistortedZone_Boss
	{ skill_index = 500490, cooltime = 20000, globalcooltime = 2, rate = 50, rangemin = 0, rangemax = 500, target = 3, selfhppercentrange = "51,90" },
	{ skill_index = 500490, cooltime = 20000, globalcooltime = 2, rate = 50, rangemin = 0, rangemax = 500, target = 3, selfhppercentrange = "0,50", next_lua_skill_index = 1 },
	--Attack13_Explosionbash_Start_90lv_DistortedZone_Boss
	{ skill_index = 500492, cooltime = 43000, rate = 150, rangemin = 100, rangemax = 500, target = 3, selfhppercentrange = "0,75" },
	--Attack10_Boom_Start_90lv_DistortedZone_Boss
	{ skill_index = 500491, cooltime = 32000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,2,0,1", selfhppercentrange = "0,90" },
	--Attack7_Buff_90lv_DistortedZone_Boss
	{ skill_index = 500487, cooltime = 10000, rate = 300, rangemin = 0, rangemax = 3000, target = 1, selfhppercentrange = "0,40", limitcount = 1 },

}
