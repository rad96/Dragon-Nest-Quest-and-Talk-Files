--AiOrc_Gray_Elite_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_Upper", rate = 6, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Upper", rate = 7, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack2_ThrowBomb_90DistortedZone", rate = 9, loop = 1,cooltime = 15000  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 3, loop = 3  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
}
g_Lua_MeleeDefense = { 
   { action_name = "Attack1_Upper", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
}
g_Lua_RangeDefense = { 
   { action_name = "Attack2_ThrowBomb_90DistortedZone", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack2_ThrowBomb_90DistortedZone", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 20074,  cooltime = 50000, rate = 20,rangemin = 300, rangemax = 1200, target = 3, selfhppercent = 50, limitcount = 5 },
}
