-- 90lv_DistortedZone_Mecharoid_vIII_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Stand_1", rate = 8, loop = 1 },
	{ action_name = "Walk_Left", rate = 10, loop = 1 },
	{ action_name = "Walk_Right", rate = 10, loop = 1 },
	{ action_name = "Walk_Back", rate = 8, loop = 1 },
	{ action_name = "Attack04_Shot_90lv_DistortedZone", rate = 6, loop = 1 },
}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 10, loop = 1 },
	{ action_name = "Stand_1", rate = 8, loop = 1 },
	{ action_name = "Walk_Front", rate = 15, loop = 1 },
	{ action_name = "Walk_Left", rate = 12, loop = 1 },
	{ action_name = "Walk_Right", rate = 12, loop = 1 },
	{ action_name = "Attack04_Shot_90lv_DistortedZone", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 4, loop = 1 },
	{ action_name = "Walk_Front", rate = 8, loop = 1 },
}
g_Lua_Skill = { 
	{ skill_index = 21052,  cooltime = 20000, rate = 8, rangemin = 0, rangemax = 250, target = 3, encounter = 5000 },
	{ skill_index = 21053,  cooltime = 35000, rate = 5, rangemin = 0, rangemax = 1200, target = 3, encounter = 15000, max_missradian = 30 },
	{ skill_index = 21054,  cooltime = 13000, rate = 10, rangemin = 0, rangemax = 1200, target = 3, encounter = 2000, max_missradian = 30 },
}
