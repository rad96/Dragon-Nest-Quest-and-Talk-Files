--AiShadow_Dark_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 450;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1  },
   { action_name = "Walk_Back", rate = 12, loop = 1  },
   { action_name = "Walk_Right", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 6, loop = 1  },
   { action_name = "Attack1_Claw", rate = 6, loop = 1  },

}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1  },
   { action_name = "Walk_Front", rate = 12, loop = 1  },
   { action_name = "Walk_Right", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 1  },
   { action_name = "Attack2_Curve", rate = 6, loop = 1  },
   { action_name = "Attack4_Pierce", rate = 7, loop = 1  },

}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Attack4_Pierce", rate = 5, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Assault", rate = 6, loop = 1  },

}
g_Lua_Assault = { 
	{ action_name = "Stand_1", rate = 5, loop = 1, approach = 200  },
	{ action_name = "Attack4_Pierce", rate = 10, loop = 1, approach = 300  },
}


g_Lua_Skill = { 
   { skill_index = 20240,  cooltime = 22000, rate = 15,rangemin = 500, rangemax = 1000, target = 3 },
   { skill_index = 20241,  cooltime = 30000, rate = 20, rangemin = 50, rangemax = 400, target = 3, selfhppercent = 70 },
}
