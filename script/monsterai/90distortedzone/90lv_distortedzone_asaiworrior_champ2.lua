-- 90lv_DistortedZone_AsaiWorrior_Purple_Champ2.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Back", rate = 5, loop = 1 },
   { action_name = "Attack2_Slash", rate = 15, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Assault", rate = 20, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Assault", rate = 22, loop = 1  },
}

g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 5, loop = 1, approach = 150 },
   { action_name = "Attack7_ThrowSword", rate = 10, loop = 1, max_missradian = 30, approach = 150 },
}

g_Lua_Skill = { 
	{ skill_index = 33331,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1 },
}
