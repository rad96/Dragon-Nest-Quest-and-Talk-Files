-- 90lv_DistortedZone_Wraith_Normal.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Attack1_Cut", rate = 4, loop = 1  },
   { action_name = "Attack4_BackAttack", rate = 5, loop = 1  },
}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 6, loop = 1  },
	{ action_name = "Walk_Left", rate = 8, loop = 2  },
	{ action_name = "Walk_Right", rate = 8, loop = 2  },
	{ action_name = "Walk_Front", rate = 5, loop = 2  },
	{ action_name = "Move_Front", rate = 10, loop = 1  },
	{ action_name = "Attack5_DashAttack", rate = 7, loop = 1  },
	{ action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 1  },
   { action_name = "Move_Right", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },

}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 3, loop = 1, approach = 200.0  },
   { action_name = "Attack1_Cut", rate = 3, loop = 1, approach = 200.0  },
   { action_name = "Attack4_BackAttack", rate = 10, loop = 1, approach = 200.0  },
}
