--90lv_DistortedZone_Minotauros_White_Champ2.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 350;
g_Lua_NearValue3 = 500;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 1, loop = 1  },
	{ action_name = "Walk_Left", rate = 5, loop = 2  },
	{ action_name = "Walk_Right", rate = 5, loop = 2  },
	{ action_name = "Walk_Back", rate = 13, loop = 2  },
	{ action_name = "Attack4_Pushed", rate = 18, loop = 1  },
}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 5, loop = 1  },
	{ action_name = "Walk_Left", rate = 6, loop = 2  },
	{ action_name = "Walk_Right", rate = 6, loop = 2  },
	{ action_name = "Walk_Front", rate = 15, loop = 2  },
	{ action_name = "Attack1_Bash", rate = 18, loop = 1  },
	{ action_name = "Attack9_BigBash_Normal_90lv_DistortedZone", rate = 15, loop = 1  },
}
g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 1, loop = 1  },
	{ action_name = "Walk_Front", rate = 15, loop = 2  },
	{ action_name = "Move_Left", rate = 5, loop = 2  },
	{ action_name = "Move_Right", rate = 5, loop = 2  },
	{ action_name = "Attack3_DashAttack", rate = 20, loop = 1  },
}
g_Lua_Near4 = { 
	{ action_name = "Stand", rate = 1, loop = 1  },
	{ action_name = "Move_Front", rate = 20, loop = 2  },
	{ action_name = "Attack3_DashAttack", rate = 10, loop = 1  },
	{ action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Assault = { 
	{ action_name = "Attack1_Bash", rate = 8, loop = 1, approach = 250.0  },
	{ action_name = "Attack9_BigBash_Normal_90lv_DistortedZone", rate = 8, loop = 1, approach = 350.0  },
}
g_Lua_Skill = { 
	{ skill_index = 500496,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 600, target = 3, selfhppercent = 80 },
	{ skill_index = 33331,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1 },
}
