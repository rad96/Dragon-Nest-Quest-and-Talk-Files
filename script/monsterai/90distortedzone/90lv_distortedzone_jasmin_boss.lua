--90lv_DistortedZone_Jasmin_Boss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Back", rate = 1, loop = 2 },
	{ action_name = "Tumble_Back_90lv_DistortedZone", rate = 8, loop = 1 },
}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Tumble_Left", rate = 10, td = "FR,FL,RF", loop = 2 },
	{ action_name = "Tumble_Right", rate = 10, td = "FR,FL,LF", loop = 2 },
	{ action_name = "Walk_Back", rate = 5, loop = 1 },
	{ action_name = "Walk_Front", rate = 6, loop = 2 },
	{ action_name = "Attack06_SpannerSwing_90lv_DistortedZone", rate = 16, loop = 1, cooltime = 8000 },
	{ action_name = "Attack01_MagmaPunch1_90lv_DistortedZone", rate = 13, loop = 1, cooltime = 12000 },
}
g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 12, loop = 1 },
	{ action_name = "Walk_Front", rate = 8, loop = 2 },
}

g_Lua_Near4 = { 
	{ action_name = "Stand", rate = 2, loop = 1 },
	{ action_name = "Assault", rate = 10, loop = 1, cooltime = 12000 },
}

g_Lua_Assault = { 
	{ action_name = "Attack01_MagmaPunch1_90lv_DistortedZone", rate = 20, loop = 1, approach = 300 },
	{ action_name = "Attack07_SpannerJump2_90lv_DistortedZone", rate = 20, loop = 1, approach = 400 },
}

g_Lua_GlobalCoolTime1 = 12000 -- Attack01_MagmaPunch2_90lv_DistortedZone
g_Lua_GlobalCoolTime2 = 54000 -- Attack03_poisonPool_start_90lv_DistortedZone
g_Lua_GlobalCoolTime3 = 28000 -- Attack08_Combo_90lv_DistortedZone

g_Lua_Skill = { 

--next_lua_skill_index
	{ skill_index = 500456, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 }, -- 0:Attack07_SpannerJump_90lv_DistortedZone
	{ skill_index = 500459, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 }, -- 1:Attack09_Tumble_Back_90lv_DistortedZone

-- Ai
	-- Attack01_MagmaPunch2_90lv_DistortedZone
	{ skill_index = 500451, cooltime = 16000, globalcooltime = 1, rate = 30, rangemin = 0, rangemax = 600, encountertime = 16000, target = 3, selfhppercentrange = "91,100" },
	{ skill_index = 500451, cooltime = 16000, globalcooltime = 1, rate = 30, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "61,90", next_lua_skill_index = 0 },
 	{ skill_index = 500451, cooltime = 12000, globalcooltime = 1, rate = 30, rangemin = 0, rangemax = 600, encountertime = 16000, target = 3, selfhppercentrange = "0,60" },

	-- Attack08_Combo_90lv_DistortedZone
	{ skill_index = 500457, cooltime = 28000, globalcooltime = 3, rate = 35, rangemin = 0, rangemax = 800, target = 3, randomtarget= "1.6,0,1", encountertime = 20000, selfhppercentrange = "61,85" }, 
	{ skill_index = 500457, cooltime = 28000, globalcooltime = 3, rate = 35, rangemin = 0, rangemax = 800, target = 3, randomtarget= "1.6,0,1", selfhppercentrange = "0,60", next_lua_skill_index = 0 }, 
	{ skill_index = 500452, cooltime = 38000, rate = 80, rangemin = 0, rangemax = 5000, target = 1, selfhppercentrange = "0,95" }, -- Attack05_Buff_90lv_DistortedZone
	{ skill_index = 500458, cooltime = 40000, rate = 50, rangemin = 0, rangemax = 300, target = 3, multipletarget = "1,2,0,1", selfhppercentrange = "0,70", next_lua_skill_index = 1 }, -- Attack02_BuffZone_90lv_DistortedZone 
	
	-- Attack04_SummonNeo_90lv_DistortedZone
	{ skill_index = 500455, cooltime = 32000, rate = 50, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,4,0,1", selfhppercentrange = "0,85"  },

	-- Attack03_poisonPool_start_90lv_DistortedZone
	{ skill_index = 500454, cooltime = 54000, globalcosoltime = 2, rate = 100, priority = 50, rangemin = 0, rangemax = 600, target = 3, selfhppercentrange = "51,70", limitcount = 1 }, 

}
