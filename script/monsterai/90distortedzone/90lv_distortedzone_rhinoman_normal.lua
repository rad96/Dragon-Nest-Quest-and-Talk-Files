-- 90lv_DistortedZone_RhinoMan_Normal.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 7, loop = 1  },
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 12, loop = 1  },
   { action_name = "Attack1_bash", rate = 10, loop = 1  },
   { action_name = "Attack14_TripleSwing", rate = 10, loop = 1, cooltime = 6000 },
}

g_Lua_Near2= { 
   { action_name = "Stand", rate = 8, loop = 1  },
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Attack14_TripleSwing", rate = 10, loop = 1, cooltime = 6000 },
   { action_name = "Attack10_Shot", rate = 12, loop = 1, cooltime = 8000  },
}

g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Attack10_Shot", rate = 10, loop = 1  },
   { action_name = "Assault", rate = 15, loop = 1  },
}

g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 12, loop = 1, approach = 200  },
   { action_name = "Attack1_bash", rate = 8, loop = 1, approach = 250  },
}
g_Lua_Skill = { 
-- Ai
	-- Attack11_WideAreaShot
	{ skill_index = 500116,  cooltime = 16000, rate = 90,rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,80" },
	-- Attack12_SupportCannonShot
	{ skill_index = 500117,  cooltime = 46000, rate = 90, rangemin = 0, rangemax = 800, target = 3, selfhppercentrange = "0,40" },
	-- Attack13_FrontBreakShot
	{ skill_index = 500118,  cooltime = 32000, rate = 90, rangemin = 200, rangemax = 800, target = 3, selfhppercentrange = "0,60" },
}
