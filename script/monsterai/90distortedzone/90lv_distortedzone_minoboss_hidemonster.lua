-- 90lv_DistortedZone_MinoBoss_HideMonster.lua
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 50;
g_Lua_NearValue2 = 5000;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 50
g_Lua_AssualtTime = 5000
 
g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 500493,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 5000, target = 3 },
}