-- 90lv_DistortedZone_Mecharoid_vII_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 12, loop = 1 },
   { action_name = "Move_Left", rate = 8, loop = 1 },
   { action_name = "Move_Right", rate = 8, loop = 1 },
   { action_name = "Attack01_WeaponAttack", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Left", rate = 8, loop = 2 },
   { action_name = "Move_Right", rate = 8, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 3 },
   { action_name = "Assault", rate = 8, loop = 3 },
}

g_Lua_Assault = { 
	{ action_name = "Stand_1", rate = 2, loop = 1, approach = 200 },
	{ action_name = "Attack01_WeaponAttack", rate = 20, loop = 1, approach = 300, max_missradian = 20 },
}

g_Lua_Skill = { 
   { skill_index = 21052,  cooltime = 15000, rate = 15, rangemin = 0, rangemax = 300, target = 3, encounter = 10000, max_missradian = 30 },
}
