-- 90lv_DistortedZone_Ghoul_White_Champ1_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 10000

g_Lua_GlobalCoolTime1 = 6000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop=2 },
   { action_name = "Walk_Back", rate = 10, loop=2 },
   { action_name = "Attack2_ArmSlash", rate = 19, loop=1 },
   { action_name = "Attack1_ArmAttack", rate = 13, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 10, loop=1 },
   { action_name = "Walk_Right", rate = 10, loop=1 },
   { action_name = "Walk_Back", rate = 10, loop=1 },
   { action_name = "Attack4_JumpBite", rate = 19, loop=1 },
   { action_name = "Attack1_ArmAttack", rate = 13, loop=1 },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Front", rate = 20, loop=1 },
   { action_name = "Move_Front", rate = 10, loop=2 },
   { action_name = "Move_Left", rate = 10, loop=1 },
   { action_name = "Move_Right", rate = 10, loop=1 },
   { action_name = "Assault", rate = 13, loop=1 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 30, loop=2 },
   { action_name = "Move_Left", rate = 10, loop=1 },
   { action_name = "Move_Right", rate = 10, loop=1 },
   { action_name = "Assault", rate = 13, loop=1 },
}
g_Lua_Assault = { 
   { action_name = "Attack6_HighJumpBite", rate = 6, loop=1,cancellook = 0,approach = 500 },
   { action_name = "Attack7_Sliding", rate = 10, loop=1,cancellook = 0,approach = 250 },
}
