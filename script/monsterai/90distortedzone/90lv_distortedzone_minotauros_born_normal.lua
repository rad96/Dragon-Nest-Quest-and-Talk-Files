-- 90lv_DistortedZone_Minotauros_Normal.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
	{ action_name = "Stand_1", rate = 8, loop = 1 },
	{ action_name = "Walk_Left", rate = 10, loop = 2 },
	{ action_name = "Walk_Right", rate = 10, loop = 2 },
	{ action_name = "Walk_Back", rate = 15, loop = 2 },
}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Stand_1", rate = 8, loop = 1 },
	{ action_name = "Walk_Front", rate = 12, loop = 2 },
	{ action_name = "Walk_Left", rate = 6, loop = 2 },
	{ action_name = "Walk_Right", rate = 6, loop = 2 },
	{ action_name = "Attack9_BigBash_Born_90lv_DistortedZone", rate = 8, loop = 1, cooltime = 12000 },
}
g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 10, loop = 1 },
	{ action_name = "Walk_Front", rate = 15, loop = 2 },
	{ action_name = "Assault", rate = 15, loop = 1, cooltime = 10000 },
}

g_Lua_Assault = { 
	{ action_name = "Stand", rate = 12, loop = 1, approach = 200.0 },
	{ action_name = "Attack9_BigBash_Born_90lv_DistortedZone", rate = 8, loop = 1, approach = 200.0 },
}

