-- 90lv_DistortedZone_Tank_Red_event.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 16, loop = 1 },
}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 5, loop = 1 },
}

g_Lua_GlobalCoolTime1 = 8000 

g_Lua_Skill = { 
   { skill_index = 500466,  cooltime = 8000, globalcooltime = 1, rate = 30, rangemin = 0, rangemax = 10000, target = 3, randomtarget= "1.6,0,1" },
   { skill_index = 500466,  cooltime = 8000, globalcooltime = 1, rate = 30, rangemin = 0, rangemax = 100, target = 3, randomtarget= "1.6,0,1" },
}
