-- 90lv_DistortedZone_Ent_Gray_elite.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssaultTime = 10000

g_Lua_CustomAction = {
	CustomAction1 = {
		{ "Attack03_Kick_90lv_DistortedZone", 0 },
		{ "Attack01_RightHand", 0 },
	},
}
g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Stand_1", rate = 12, loop = 1 },
	{ action_name = "Walk_Back", rate = 2, loop = 1 },
	{ action_name = "Walk_Left", rate = 6, loop = 1 },
	{ action_name = "Walk_Right", rate = 6, loop = 1 },
	{ action_name = "Attack01_RightHand", rate = 5, loop = 1, cooltime = 12000 },

}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Stand_1", rate = 12, loop = 1 },
	{ action_name = "Walk_Front", rate = 8, loop = 2 },
	{ action_name = "Walk_Left", rate = 8, loop = 1 },
	{ action_name = "Walk_Right", rate = 8, loop = 1 },
	{ action_name = "Attack01_RightHand", rate = 5, loop = 1 },
	{ action_name = "Attack03_Kick_90lv_DistortedZone", rate = 5, loop = 1, cooltime = 6000 },
}
g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 6, loop = 1 },
	{ action_name = "Stand_1", rate = 10, loop = 1 },
	{ action_name = "Walk_Front", rate = 15, loop = 2 },
	{ action_name = "Attack04_ClodThrow_90lv_DistortedZone", rate = 15, loop = 1, cooltime = 12000 },
}
g_Lua_Near4 = { 
	{ action_name = "Stand", rate = 8, loop = 1 },
	{ action_name = "Walk_Front", rate = 10, loop = 1 },
	{ action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}

g_Lua_Assault = { 
   { action_name = "CustomAction1", rate = 13, loop = 1, approach = 250.0 },
}
