-- 90lv_DistortedZone_Houndmeca_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 10, loop = 1  },
	{ action_name = "Provoke", rate = 5, loop = 1  },
	{ action_name = "Attack2_BigBite_90lv_DistortedZone", rate = 20, loop = 1  },
	{ action_name = "Provoke", rate = 15, loop = 1, target_condition = "State1"  },
}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 5, loop = 1  },
	{ action_name = "Provoke", rate = 10, loop = 1  },
	{ action_name = "Move_Front", rate = 10, loop = 1  },
	{ action_name = "Attack2_BigBite_90lv_DistortedZone", rate = 8, loop = 1  },
	{ action_name = "Attack6_JumpBite_90lv_DistortedZone", rate = 20, loop = 1  },
	{ action_name = "Provoke", rate = 15, loop = 1, target_condition = "State1"  },
}
g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 5, loop = 1  },
	{ action_name = "Attack1_BiteCombo_90lv_DistortedZone", rate = 10, loop = 1  },
	{ action_name = "Move_Front", rate = 30, loop = 1  },
	{ action_name = "Attack6_JumpBite_90lv_DistortedZone", rate = 5, loop = 1  },
	{ action_name = "Assault", rate = 1, loop = 1  },
}
g_Lua_Near4 = { 
	{ action_name = "Stand", rate = 1, loop = 1  },
	{ action_name = "Move_Front", rate = 10, loop = 2  },
	{ action_name = "Assault", rate = 15, loop = 2  },
}

g_Lua_Assault = { 
	{ action_name = "Attack2_BigBite_90lv_DistortedZone", rate = 5, loop = 1, cancellook = 1, approach = 250.0  },
}
