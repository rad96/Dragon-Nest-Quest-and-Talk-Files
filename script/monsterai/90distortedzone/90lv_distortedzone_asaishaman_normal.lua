-- 90lv_DistortedZone_AsaiShaman_normal.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 3000;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 4000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 12, loop = 1 },
}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
}

g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 2 },
}

g_Lua_Skill = { 
	{ skill_index = 500511,  cooltime = 15000, rate = 12, rangemin = 100, rangemax = 800, target = 3, notusedskill = 500512 }, -- Attack4_Blue_Summon
	{ skill_index = 500512,  cooltime = 40000, rate = 80, rangemin = 0, rangemax = 1500, target = 1, selfhppercentrange= "0,40" }, -- Attack1_Blue_BuffAura
	{ skill_index = 500513,  cooltime = 20000, rate = 12, rangemin = 200, rangemax = 1000, target = 3, usedskill = 500512 }, -- Attack2_Blue_ThrowMojoBottle
}
