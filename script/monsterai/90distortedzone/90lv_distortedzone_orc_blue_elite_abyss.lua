--90lv_DistortedZone_Orc_Blue_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 600;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 21, loop = 1  },
   { action_name = "Move_Back", rate = 39, loop = 1  },
   { action_name = "Attack2_bash", rate = 20, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Walk_Front", rate = 51, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Walk_Back", rate = 36, loop = 2  },
   { action_name = "Attack2_bash", rate = 46, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 24, loop = 2  },
   { action_name = "Walk_Back", rate = 9, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 2  },
   { action_name = "Move_Right", rate = 8, loop = 2  },
   { action_name = "Move_Front", rate = 60, loop = 2  },
   { action_name = "Move_Back", rate = 21, loop = 2  },
   { action_name = "Assault", rate = 38, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 3  },
   { action_name = "Walk_Left", rate = 2, loop = 3  },
   { action_name = "Walk_Right", rate = 2, loop = 3  },
   { action_name = "Walk_Front", rate = 30, loop = 3  },
   { action_name = "Move_Left", rate = 6, loop = 3  },
   { action_name = "Move_Right", rate = 6, loop = 3  },
   { action_name = "Move_Front", rate = 90, loop = 3  },
}
g_Lua_MeleeDefense = { 
   { action_name = "Move_Left", rate = 6, loop = 2  },
   { action_name = "Move_Right", rate = 6, loop = 2  },
}
g_Lua_RangeDefense = { 
   { action_name = "Assault", rate = 40, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 40, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_bash", rate = 10, loop = 2, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 500561,  cooltime = 30000, rate = 70,rangemin = 0, rangemax = 500, target = 2, hppercent = 80 }, --1프로짜리힐 엘리트는 hp80에서 노멀은 50프로로 설정할것.
}
