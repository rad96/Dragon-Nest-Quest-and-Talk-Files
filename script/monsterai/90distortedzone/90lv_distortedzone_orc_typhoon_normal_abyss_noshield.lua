--90lv_DistortedZone_Orc_Typhoon_Abyss_Shield.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Stand", rate = 10, loop = 1  },
   { action_name = "Attack2_bash", rate = 4, loop = 1  },
   { action_name = "Attack3_Air", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack2_bash", rate = 6, loop = 1  },
   { action_name = "Attack9_DashAttack", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
}
g_Lua_MeleeDefense = { 
   { action_name = "Attack3_Air", rate = -1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
}
g_Lua_RangeDefense = { 
   { action_name = "Assault", rate = 30, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 2  },
   { action_name = "Walk_Right", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 30, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 2  },
   { action_name = "Walk_Right", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack9_DashAttack", rate = 30, loop = 2, cancellook = 0, approach = 250.0  },
}
