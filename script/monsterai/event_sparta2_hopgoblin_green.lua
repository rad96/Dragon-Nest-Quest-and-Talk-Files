--Event_Sparta2_HopGoblin_Green.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Bite", rate = 10, loop = 1, approach = 100.0  },
}
