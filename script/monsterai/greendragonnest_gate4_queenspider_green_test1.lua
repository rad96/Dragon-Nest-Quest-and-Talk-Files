--GreenDragonNest_Gate4_QueenSpider_Green.lua
--/genmon 236012
g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 450;
g_Lua_NearValue4 = 700;
g_Lua_NearValue5 = 1000;
g_Lua_NearValue6 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   -- { action_name = "Stand", rate = 3, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 2  },
   -- { action_name = "Walk_Right", rate = 5, loop = 2  },
   -- { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Slash_Lv3", rate = 10, loop = 2  },
}
g_Lua_Near2 = { 
   -- { action_name = "Stand", rate = 3, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 2  },
   -- { action_name = "Walk_Right", rate = 5, loop = 2  },
   -- { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Attack1_Slash_Lv3", rate = 5, loop = 2  },   
}
g_Lua_Near3 = { 
   -- { action_name = "Stand", rate = 5, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 2  },
   -- { action_name = "Walk_Front", rate = 10, loop = 2  },
   -- { action_name = "Walk_Right", rate = 5, loop = 2  },
   -- { action_name = "Attack6_Absorb", rate = 5, loop = 2  },
      { action_name = "Attack1_Slash_Lv3", rate = 5, loop = 2  },   
}
g_Lua_Near4 = { 
   -- { action_name = "Stand", rate = 5, loop = 2  },
   -- { action_name = "Walk_Left", rate = 5, loop = 2  },
   -- { action_name = "Walk_Right", rate = 5, loop = 2  },
   -- { action_name = "Walk_Front", rate = 10, loop = 2  },
   -- { action_name = "Walk_Back", rate = 5, loop = 2  },
   -- { action_name = "Attack3_JumpAttack_Lv2", rate = 5, loop = 2  },
      { action_name = "Attack1_Slash_Lv3", rate = 5, loop = 2  },   
}
g_Lua_Near5 = { 
   -- { action_name = "Stand", rate = 5, loop = 2  },
   -- { action_name = "Walk_Left", rate = 5, loop = 2  },
   -- { action_name = "Walk_Right", rate = 5, loop = 2  },
   -- { action_name = "Walk_Front", rate = 15, loop = 2  }
   -- { action_name = "Attack3_JumpAttack_Lv2", rate = 5, loop = 2  },
      { action_name = "Attack1_Slash_Lv3", rate = 5, loop = 2  },   
}
g_Lua_Near6 = { 
   -- { action_name = "Stand", rate = 5, loop = 2  },
   -- { action_name = "Walk_Left", rate = 3, loop = 3  },
   -- { action_name = "Walk_Right", rate = 3, loop = 3  },
   -- { action_name = "Walk_Front", rate = 10, loop = 3  },
      { action_name = "Attack1_Slash_Lv3", rate = 5, loop = 2  },   
}
-- g_Lua_Skill = { 
   -- { skill_index = 31411,  cooltime = 60000, rate = 100,rangemin = 200, rangemax = 800, target = 3, selfhppercent = 100,encountertime = 20000},--�Ź̻��
   -- { skill_index = 31412,  cooltime = 60000, rate = 100,rangemin = 200, rangemax = 800, target = 3, selfhppercent = 30},--����ȭ
   -- { skill_index = 31413,  cooltime = 40000, rate = 50,rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100},--������
   -- { skill_index = 31414,  cooltime = 25000, rate = 50,rangemin = 0, rangemax = 500, target = 3, selfhppercent = 30},--�Ź��ٻѸ���
   -- { skill_index = 31415,  cooltime = 50000, rate = 40,rangemin = 500, rangemax = 1200, target = 3, selfhppercent = 100},--�Ѹ�����
   -- { skill_index = 31416,  cooltime = 60000, rate = 100,rangemin = 200, rangemax = 800, target = 3, selfhppercent = 20},--����
-- }
