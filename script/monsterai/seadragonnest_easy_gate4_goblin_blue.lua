--SeaDragonNest_Gate4_Goblin_Blue.lua

g_Lua_NearTableCount = 7

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 350;
g_Lua_NearValue4 = 550;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;
g_Lua_NearValue7 = 10000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
  State2 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Walk_Back", 2 },
      { "Attack2_Upper", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 30, loop = 2  },
   { action_name = "Move_Back", rate = 30, loop = 2  },
   { action_name = "Attack1_Slash", rate = 11, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "stand_1", rate = 9, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 54, loop = 2  },
   { action_name = "Move_Back", rate = 66, loop = 2  },
   { action_name = "Attack1_Slash", rate = 27, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "stand_1", rate = 9, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Back", rate = 72, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 60, loop = 1  },
   { action_name = "Attack2_Upper", rate = 29, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "stand_1", rate = 9, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Assault", rate = 25, loop = 2      },
}
g_Lua_Near5 = { 
   { action_name = "stand_1", rate = 9, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
   { action_name = "Assault", rate = 12, loop = 2  },
}
g_Lua_Near6 = { 
   { action_name = "stand_1", rate = 9, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
   { action_name = "Assault", rate = 14, loop = 3  },
}
g_Lua_Near7 = { 
   { action_name = "stand_1", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Slash", rate = 5, loop = 1, cancellook = 0, approach = 250.0  },
   { action_name = "Attack2_Upper", rate = 5, loop = 1, cancellook = 1, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 20003,  cooltime = 6000, rate = 56,rangemin = 300, rangemax = 1000, target = 3, selfhppercent = 90 },
}
