--AiCrocer_Brown_Elite_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Walk_Back", 0 },
      { "Attack01_Swing", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Attack03_ShieldAttack", rate = 12, loop = 1 },
   { action_name = "CustomAction1", rate = 8, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Attack01_Swing", rate = 12, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Stand_2", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Assault", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 2 },
   { action_name = "Assault", rate = 8, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 3 },
   { action_name = "Assault", rate = 8, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack02_TailAttack", rate = 10, loop = 1, approach = 300 },
   { action_name = "Walk_Back", rate = 2, loop = 1, approach = 200 },
}
g_Lua_Skill = { 
   { skill_index = 35320,  cooltime = 30000, rate = 60,rangemin = 0, rangemax = 1000, target = 3, randomtarget = "1.6,0,1" },
   { skill_index = 35320,  cooltime = 25000, rate = 70, rangemin = 0, rangemax = 600, target = 3, selfhppercent = 80 },
   { skill_index = 35320,  cooltime = 40000, rate = 40, rangemin = 0, rangemax = 1000, target = 1, selfhppercent = 60 },
}
