--AiFrogFlower_Red_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 6, loop = 1 },
   { action_name = "Move_Right", rate = 6, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Move_Back_3", rate = 12, loop = 2 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 6, loop = 1 },
   { action_name = "Move_Right", rate = 6, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Move_Back_1", rate = 6, loop = 2 },
   { action_name = "Move_Back_2", rate = 12, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 2 },
   { action_name = "Walk_Right", rate = 6, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 4 },
}
g_Lua_Skill = { 
   { skill_index = 35384,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 8000, target = 1, next_lua_skill_index= 1, limitcount = 1 },
   { skill_index = 35386,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 8000, target = 1 },
   { skill_index = 35297,  cooltime = 8000, rate = 80,rangemin = 600, rangemax = 1200, target = 3, selfhppercent = 99 },
}
