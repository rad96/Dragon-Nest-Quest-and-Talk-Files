--AiEvildog_Black_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 350;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 950;
g_Lua_NearValue5 = 1250;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 350
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Attack01_Bite", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 2 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
   { action_name = "Attack02_Spit", rate = 4, loop = 1, max_missradian = 10 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 5 },
   { action_name = "Assault", rate = 5, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 5 },
   { action_name = "Assault", rate = 5, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack01_Bite", rate = 5, loop = 1, approach = 200 },
   { action_name = "Stand", rate = 3, loop = 1, approach = 200 },
}
