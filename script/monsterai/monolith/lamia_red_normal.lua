--lamia_red

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1400;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 4000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 350
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 5000
g_Lua_GlobalCoolTime2 = 20000

g_Lua_CustomAction ={
  CustomAction1 = { -- Walk_Back 액션 뒤 이어서 Attack03_JumpingBall 공격을 한다.
      { "Attack01_TailAttack", 0 },
	  { "Move_Back", 1 },
	  { "Stand", 1 },
  },
}

g_Lua_State = { -- 캐릭터 상태 조건 정의
  State1 = {"Move"}, 
  State2 = {"Attack"}, 
}


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Back", rate = 15, loop = 2, cooltime = 7000, target_condition = "State1" },
   --{ action_name = "Move_Back", rate = 30, loop = 3, target_condition = "State2" },
   { action_name = "Move_Back", rate = 10, loop = 1},
   { action_name = "Walk_Back", rate = 3, loop = 1 },
   { action_name = "Attack01_TailAttack", rate = 15, loop = 1, cooltime = 3000 },
   { action_name = "CustomAction1", rate = 100, loop = 1, cooltime = 7000, target_condition = "State2" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 2 },
   { action_name = "Walk_Right", rate = 6, loop = 2 },
   { action_name = "Walk_Back", rate = 10, loop = 3 },
   { action_name = "Move_Back", rate = 10, loop = 3},
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 2 },
   { action_name = "Walk_Right", rate = 10, loop = 2 },
   { action_name = "Move_Back", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 10, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Move_Left", rate = 6, loop = 1 },
   { action_name = "Move_Right", rate = 6, loop = 1 },
   { action_name = "Move_Back", rate = 2, loop = 1 },
}

g_Lua_Near5 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Move_Front", rate = 20, loop = 2 },
}

g_Lua_Skill = {
   --용암 파도 (근접 범위 공격)
   { skill_index = 35206, cooltime = 30000, rate = 100, rangemin = 0, rangemax = 600, target = 3, encountertime = 6000, globalcooltime = 2 },
   --화염구 발사 2연타 (원거리 공격)
   { skill_index = 35203, cooltime = 17000, rate = 100, rangemin = 500, rangemax = 3000, target = 3, encounrtertime=5000, globalcooltime = 1},
   --화염구 발사 4연타 (원거리 공격)
   { skill_index = 35204, cooltime = 25000, rate = 100, rangemin = 1000, rangemax = 3000, target = 3, encountertime = 5000, globalcooltime = 1},
   --분화구 소환
   { skill_index = 35205, cooltime = 30000, rate = 100, rangemin = 400, rangemax = 1000, target = 3, encountertime = 10000, globalcooltime = 2, limitcount = 1, selfhppercentrange = "1,25" },
   --분화구 소환
   { skill_index = 35205, cooltime = 30000, rate = 100, rangemin = 400, rangemax = 1000, target = 3, encountertime = 10000, globalcooltime = 2, limitcount = 1, selfhppercentrange = "51,80" },
   --보호막_1(생성)
   { skill_index = 35207, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 3000, target = 2, encountertime = 20000, selfhppercentrange = "1,25", limitcount = 1 },
   --보호막_2(생성)
   { skill_index = 35207, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 3000, target = 2, encountertime = 20000, selfhppercentrange = "50,70", limitcount = 1 },
   }
