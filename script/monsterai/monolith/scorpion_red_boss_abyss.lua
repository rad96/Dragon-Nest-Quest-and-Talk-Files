-- unstable

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue3 = 4000;


g_Lua_PatrolBaseTime = 5000; -- 패트롤 시간
g_Lua_PatrolRandTime = 3000; -- g_Lua_PatrolBaseTime 값 ~ (g_Lua_PatrolBaseTime + g_Lua_PatrolRandTime) 사이 값 중 랜덤 시간으로 설정된 좌표로 이동한다. (몬스터 이동 시간 설정)
g_Lua_ApproachValue = 300; -- 몬스터가 다가가는 최소 거리
g_Lua_AssaultTime = 6000;-- Assualt 명령을 받고 돌격 할 때 쫒아가는 시간 설정


g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	     rate = 3,		loop = 1 },
	{ action_name = "Move_Front",	 rate = 2,		loop = 1 },
	{ action_name = "Walk_Front",	 rate = 2,		loop = 1 },
	
}
g_Lua_Near2 = 
{
	{ action_name = "Stand",	     rate = 2,		loop = 1 },
	{ action_name = "Move_Front",	 rate = 10,		loop = 1 },
	{ action_name = "Walk_Front",	 rate = 8,		loop = 1 },
}

g_Lua_Near3 =
{
	{ action_name = "Stand",	     rate = 3,		loop = 1 },
	{ action_name = "Move_Front",	 rate = 10,		loop = 2 },
	{ action_name = "Walk_Front",	 rate = 8,		loop = 1 },
	{ action_name = "Assault", rate = 15, loop = 1, notusedskill  = "35250"    },
}

g_Lua_Near4 =
{
	{ action_name = "Stand",	     rate = 3,		loop = 1 },
	{ action_name = "Move_Front",	 rate = 10,		loop = 2 },
	{ action_name = "Assault", rate = 15, loop = 1, notusedskill  = "35250"    },
}

g_Lua_Assault = { 
   { lua_skill_index = "0", rate = 10, loop = 1, approach = 350.0  },
   { lua_skill_index = "2", rate = 20, loop = 1, approach = 350.0  },
   { lua_skill_index = "5", rate = 30, loop = 1, approach = 350.0  },
}

g_Lua_GlobalCoolTime1 = 30000

g_Lua_Skill = 
{
 -- ***************** 일반 상태일 경우 사용 스킬 *****************
    -- 기본 콤보 공격1 (L - R - 밀쳐내기)
   { skill_index = 35247, cooltime = 10000, rate = 30, rangemin = 0, rangemax = 400, target = 3, combo1 = "1,100,0", notusedskill  = "35250"   }, --0
   { skill_index = 35262, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3 }, --1
   
   
    -- 기본 콤보 공격2 (R - L - 밀쳐내기)
   { skill_index = 35248 , cooltime = 10000, rate = 30, rangemin = 0, rangemax = 400, target = 3, combo1 = "3,100,0", notusedskill  = "35250"   }, --2
   { skill_index = 35261, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3 }, --3
   
   
   -- * 밀쳐내기 (기본 콤보용 스킬)
   { skill_index = 35249, cooltime = 8000, rate = 30, rangemin = 0, rangemax = 400, target = 3,  td = "FL,FR,LF,RF", notusedskill  = "35250"   }, --4

 
    -- * 돌진
   { skill_index = 35253, cooltime = 70000, rate = 30, rangemin = 0, rangemax = 5000, target = 3, encountertime = 10000, randomtarget = "0.5,0", notusedskill  = "35250"}, 
    -- * 스팀 
   { skill_index = 35251, cooltime = 50000, rate = 40, rangemin = 0, rangemax = 400, target = 3, priority = 20, notusedskill  = "35250", encountertime = 10000 },
    -- * 빔 - HP 75미만 일 경우 사용
   { skill_index = 35258, cooltime = 40000, rate = 80, rangemin = 500, rangemax = 10000, target = 3, notusedskill  = "35250", encountertime = 15000 },
    -- * 기습 - HP 50미만 일 경우 사용
   { skill_index = 35255, cooltime = 90000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = "70", notusedskill  = "35250" , globalcooltime = 1 },
 
 
 -- ***************** 광폭화 상태일 경우 사용 스킬 *****************
    -- * 돌진(광폭화)
   { skill_index = 35254, cooltime = 30000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.5,0", priority = 40, encountertime = 10000, usedskill  = "35250"},
    -- * 튕겨내기(광폭화)
   { skill_index = 35252, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 30, randomtarget = "0.5,0", usedskill  = "35250" },

 

 -- ***************** 광폭화 하기  ***************** 
    -- * 광혹화 하기 HP 75%
   { skill_index = 35259, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority = 90, selfhppercentrange = "60,75", notusedskill  = "35250", combo1 = "12,100,0", limitcount = 1, globalcooltime = 1 },
   { skill_index = 35250, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, combo1 = "13,100,0" },
   { skill_index = 35260, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 10000, target = 3},
   
    -- * 광혹화 하기 HP 50%
   { skill_index = 35259, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority = 85, selfhppercentrange = "35,50", notusedskill  = "35250", combo1 = "12,100,0", limitcount = 1, globalcooltime = 1 },
   
    -- * 광혹화 하기 HP 30%
   { skill_index = 35259, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority = 80, selfhppercentrange = "1,25", notusedskill  = "35250", combo1 = "12,100,0", limitcount = 1, globalcooltime = 1 },
}
