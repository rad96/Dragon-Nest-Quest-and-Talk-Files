--AiUnstable_Red_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 550;
g_Lua_NearValue4 = 950;
g_Lua_NearValue5 = 1350;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack|!Air", "Down" }, 
  State2 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Move_Back", 1 },
      { "Attack13_TakeDown", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "CustomAction1", rate = 6, loop = 1 },
   { action_name = "Attack1_Kick", rate = 12, loop = 1 },
   { action_name = "Attack12_Swing", rate = 18, loop = 1 },
   { action_name = "Attack11_BiteOff", rate = 149, loop = 1, priority = 100 , cooltime = 7000, target_condition = "State1" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "Attack10_TripUp", rate = 12, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 2 },
   { action_name = "Attack13_TakeDown", rate = 6, loop = 1 },
   { action_name = "Assault", rate = 12, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 3 },
   { action_name = "Assault", rate = 8, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 4 },
   { action_name = "Assault", rate = 8, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack12_Swing", rate = 8, loop = 1, approach = 180 },
   { action_name = "Attack10_TripUp", rate = 6, loop = 1, approach = 280 },
   { action_name = "Stand", rate = 1, loop = 1, approach = 180 },
}

g_Lua_RandomSkill ={
{
{
   { skill_index = 35214, rate= 50 }, 
   { skill_index = 35215, rate= 50 },
},
{
   { skill_index = 35212, rate= 50 }, 
   { skill_index = 35213, rate= 50 },
},
}
}

g_Lua_Skill = { 
   { skill_index = 35214,  cooltime = 12000, rate = 100,skill_random_index = 1, rangemin = 0, rangemax = 500, target = 3, target_condition = "State2", combo1 = "1,100,0" },
   { skill_index = 35212,  cooltime = 1000, rate = -1, skill_random_index = 2, rangemin = 0, rangemax = 1500, target = 3 },
}
