--AiCreep_Red_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1150;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 450
g_Lua_AssualtTime = 60000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Turn_Left", rate = 15, loop = -1 },
   { action_name = "Turn_Right", rate = 15, loop = -1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Turn_Left", rate = 15, loop = -1 },
   { action_name = "Turn_Right", rate = 15, loop = -1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 2 },
   { action_name = "Turn_Left", rate = 15, loop = -1 },
   { action_name = "Turn_Right", rate = 15, loop = -1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 3 },
   { action_name = "Turn_Left", rate = 15, loop = -1 },
   { action_name = "Turn_Right", rate = 15, loop = -1 },
}

g_Lua_GlobalCoolTime1 = 24000


g_Lua_Skill = { 
   { skill_index = 35336,  cooltime = 13000, rate = 70,rangemin = 0, rangemax = 500, target = 3, td = "FR,FL" },
   { skill_index = 35337,  cooltime = 22000, rate = 60, rangemin = 500, rangemax = 1300, target = 3, td = "FR,FL" },
   { skill_index = 35338,  cooltime = 31000, rate = 90, rangemin = 0, rangemax = 700, target = 3, td = "BR,BL", randomtarget = "1.6,0,1" },
   { skill_index = 35339,  cooltime = 17000, rate = 80, rangemin = 500, rangemax = 1200, target = 3, td = "FR,FL" },
   { skill_index = 35340,  cooltime = 1000, rate = 80, globalcooltime = 1, rangemin = 500, rangemax = 700, target = 3, randomtarget = "1.6,0,1", td = "FR,FL" },
   { skill_index = 35341,  cooltime = 1000, rate = 80, globalcooltime = 1, rangemin = 700, rangemax = 1000, target = 3, randomtarget = "1.6,0,1", td = "FR,FL" },
   { skill_index = 35342,  cooltime = 1000, rate = 80, globalcooltime = 1, rangemin = 1000, rangemax = 1300, target = 3, randomtarget = "1.6,0,1", td = "FR,FL" },
   { skill_index = 35343,  cooltime = 61000, rate = 60, rangemin = 0, rangemax = 1000, target = 3, multipletarget = "1,4,0,1" },
   { skill_index = 35344,  cooltime = 112000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 75 },
   { skill_index = 35345,  cooltime = 93000, rate = 70, rangemin = 0, rangemax = 2000, target = 3, selfhppercent = 70, randomtarget = "1.6,0,1", multipletarget = "1,4,0,1" },
}
