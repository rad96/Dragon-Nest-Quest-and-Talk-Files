--AiKnollFlamer_Red_Elite_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 350;
g_Lua_NearValue2 = 450;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 350
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "Move_Back", rate = 8, loop = 2 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "Move_Back", rate = 6, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
}

g_Lua_RandomSkill ={
{
{ -- ȭ����� ���� / ����  ��ų ����
   { skill_index = 35201, rate= 40 }, 
   { skill_index = 35202, rate= 60 },
},
}
}

g_Lua_Skill = { 
   { skill_index = 35201,  cooltime = 24000, rate = 60,rangemin = 0, rangemax = 500, target = 3, skill_random_index = 1 },
   { skill_index = 35347,  cooltime = 12000, rate = 70, rangemin = 0, rangemax = 800, target = 3 },
}
