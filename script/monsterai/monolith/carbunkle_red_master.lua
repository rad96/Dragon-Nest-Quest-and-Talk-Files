--AiGoblin_Green_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 9, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 9, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 3 },
   { action_name = "Move_Front", rate = 8, loop = 2 },
}

g_Lua_Skill = { 
   { skill_index = 35243,  cooltime = 10000, rate = 50, rangemin = 0 , rangemax = 1000, target = 3 },
   { skill_index = 35244,  cooltime = 20000, rate = 50, rangemin = 0 , rangemax = 1000, target = 3 },
   { skill_index = 35245,  cooltime = 20000, rate = 50, rangemin = 0 , rangemax = 1000, target = 3,selfhppercent=80, encountertime=10000 },
}

