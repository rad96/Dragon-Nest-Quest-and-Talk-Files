--AiLizardFlag_Blue_Elite_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 4, loop = 2 },
   { action_name = "Move_Right", rate = 4, loop = 2 },
   { action_name = "Move_Back", rate = 12, loop = 2 },
   { action_name = "Attack13_BigSwing", rate = 8, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
   { action_name = "Move_Back", rate = 12, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Stand_2", rate = 6, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 2 },
   { action_name = "Walk_Right", rate = 2, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 35376,  cooltime = 1000, rate = 100,rangemin = 0, rangemax = 500, target = 1, limitcount = 1, priority = 20 },
   { skill_index = 35377,  cooltime = 18000, rate = 60, rangemin = 0, rangemax = 400, target = 3 },
   { skill_index = 35378,  cooltime = 20000, rate = 80, rangemin = 500, rangemax = 1100, target = 4 },
   { skill_index = 35379,  cooltime = 12000, rate = 70, rangemin = 400, rangemax = 1100, target = 3 },
   { skill_index = 35380,  cooltime = 20000, rate = 70, rangemin = 0, rangemax = 900, target = 3 },
}
