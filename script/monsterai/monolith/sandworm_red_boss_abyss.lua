--Sandworm_Red_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 1400;
g_Lua_NearValue2 = 2000;
g_Lua_NearValue3 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 600
g_Lua_AssaultTime = 60000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Skill = { 
   { skill_index = 35229,  cooltime = 12000, rate = 80,rangemin = 0, rangemax = 700, target = 3, td = "LF,FL,FR,RF" },
   { skill_index = 35220,  cooltime = 16000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, td = "FL,FR", skill_random_index = 1, randomtarget = "1.6,0,1" },
   { skill_index = 35225,  cooltime = 40000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, td = "LF,FL,FR,RF", selfhppercent = 70 },
   { skill_index = 35226,  cooltime = 15000, rate = 80, rangemin = 700, rangemax = 1500, target = 3, td = "LF,FL,FR,RF" },
   { skill_index = 35233,  cooltime = 45000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 50, td = "LF,FL,FR,RF" },
   { skill_index = 35231,  cooltime = 20000, rate = 70, rangemin = 0, rangemax = 700, target = 3 },
   { skill_index = 35223,  cooltime = 30000, rate = 80, rangemin = 400, rangemax = 1500, target = 3, td = "LF,FL,FR,RF" },
   { skill_index = 35235,  cooltime = 1000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, limitcount = 1 },
   { skill_index = 35235,  cooltime = 1000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 50, limitcount = 1 },
   { skill_index = 35235,  cooltime = 1000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 25, limitcount = 1 },
   { skill_index = 35238,  cooltime = 40000, rate = 50, rangemax = 5000, target = 3, encountertime = 30000, multipletarget = "2,2,0,1" },
   { skill_index = 35239,  cooltime = 15000, globalcooltime = 1, rate = 90, rangemin = 1000, rangemax = 5000, target = 3, randomtarget = "1.6,0,1" },
}
