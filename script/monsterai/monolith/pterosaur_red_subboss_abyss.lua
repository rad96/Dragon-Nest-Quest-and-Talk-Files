--AiPterosaur_Red_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 900;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 2300;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Stand_1", rate = 2, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 5, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 5, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Assault = { 
   { action_name = "Attack01_Slash", rate = 8, loop = 1, approach = 400 },
   { action_name = "Stand", rate = 2, loop = 1, approach = 400 },
}

g_Lua_RandomSkill ={
{
{    { skill_index = 35332, rate = 50 },   { skill_index = 35334, rate = 50 },   },
{    { skill_index = 35329, rate = 30 },   { skill_index = 35330, rate = 70 },   },
}
}

g_Lua_GlobalCoolTime1 = 15000

g_Lua_Skill = { 
   { skill_index = 35326,  cooltime = 6000, rate = 70,rangemin = 0, rangemax = 400, target = 3, td = "FR,FL" },
   { skill_index = 35327,  cooltime = 14000, rate = 60, rangemin = 0, rangemax = 400, target = 3, td = "FR,FL" },
   { skill_index = 35332,  cooltime = 40000, rate = 50, globalcooltime = 1, skill_random_index = 1, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1" },
   { skill_index = 35328,  cooltime = 12000, rate = 90, rangemin = 0, rangemax = 400, target = 3, td = "BL,BR", randomtarget = "0.6,0,1" },
   { skill_index = 35330,  cooltime = 40000, rate = 80, globalcooltime = 1, skill_random_index = 2, rangemin = 400, rangemax = 1000, target = 3, randomtarget = "0.6,0,1" },
   { skill_index = 35331,  cooltime = 30000, rate = 60, rangemin = 0, rangemax = 1300, target = 3, td = "FR,FL" },
}
