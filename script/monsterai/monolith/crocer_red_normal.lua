
g_Lua_NearTableCount = 5
g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2100;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 300;
g_Lua_AssualtTime = 8000;

g_Lua_GlobalCoolTime1 = 33000
g_Lua_GlobalCoolTime2 = 33000

g_Lua_CustomAction =
{
   CustomAction1 = 
   {{ "Attack02_TailAttack", 0 },{ "Attack01_Swing", 0 },},
   CustomAction2 =
   {{ "Attack02_TailAttack", 0 },{ "Attack03_ShieldAttack", 0 },},
}

g_Lua_Assault = {
   { action_name = "Move_Front", rate = 10, loop = 1, approach = 300.0  },
}

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 9, loop = 1 },
   { action_name = "Walk_Right", rate = 9, loop = 1 },
   { action_name = "CustomAction1", rate = 5, loop = 1, cooltime = 60000 },
   { action_name = "CustomAction2", rate = 5, loop = 1, cooltime = 60000 },
}


g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Assault", rate = 13, loop = 1 },
}


g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 15, loop = 1 },
}


g_Lua_Near4 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 15, loop = 1 },
}


g_Lua_Near5 = 
{ 
   { action_name = "Stand1", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 40, loop = 1 },
}

g_Lua_Skill = { 
   -- 휘두르기
   { skill_index = 35317, cooltime = 10000, rate = 50, rangemin = 0, rangemax = 200, target = 3, encountertime=10000 }, 
   -- 꼬리치기
   { skill_index = 35318, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 200, target = 3, globalcooltime = 1, encountertime=20000 },
   -- 방패찍고 방패치기
   { skill_index = 35319, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 200, target = 3, globalcooltime = 1, encountertime=20000 },
   -- 방패막으며 돌진
   { skill_index = 35320, cooltime = 50000, rate = 50, rangemin = 0, rangemax = 1000, target = 3, encountertime=30000, globalcooltime = 2 },
   -- 철퇴내려찍기
   { skill_index = 35321, cooltime = 60000, rate = 50, rangemin = 0, rangemax = 300, target = 3, encountertime=30000, globalcooltime = 2 },
   -- 방패막기 카운터 - HP 50%
   { skill_index = 35322, cooltime = 45000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, selfhppercentrange = "51,80", limitcount = 1}, 
   -- 방패막기 카운터 - HP 50%
   { skill_index = 35322, cooltime = 45000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, selfhppercentrange = "26,50", limitcount = 1}, 
   -- 방패막기 카운터 - - HP 25%
   { skill_index = 35322, cooltime = 45000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, selfhppercentrange = "1,25", limitcount = 1}, 
   }