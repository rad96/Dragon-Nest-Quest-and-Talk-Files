--AiLizardSoldier_Red_NoShield_Elite_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 1 },
   { action_name = "Move_Right", rate = 3, loop = 1 },
   { action_name = "Move_Back", rate = 1, loop = 1 },
   { action_name = "Attack01_Chopping", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
   { action_name = "Assault", rate = 5, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 4 },
   { action_name = "Assault", rate = 5, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack01_Chopping", rate = 5, loop = 1, approach = 200 },
   { action_name = "Stand", rate = 3, loop = 1, approach = 200 },
}
g_Lua_Skill = { 
   { skill_index = 35168,  cooltime = 20000, rate = 70,rangemin = 50, rangemax = 300, target = 3 },
   { skill_index = 35167,  cooltime = 12000, rate = 60, rangemin = 50, rangemax = 300, target = 3 },
}
