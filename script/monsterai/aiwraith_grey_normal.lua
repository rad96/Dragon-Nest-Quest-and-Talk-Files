-- Wraith Grey Normal AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 1500.0;

g_Lua_LookTargetNearState = 4;
g_Lua_WanderingDistance = 2000.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 3000;



g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	{ action_name = "Move_Back",	rate = 8,		loop = 1 },
	{ action_name = "Attack1_Cut",	rate = 5, 		loop = 1 },
	{ action_name = "Attack4_BackAttack",	rate = 4,		loop = 1 },
	{ action_name = "Attack8_LazerCircle",	rate = 12,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Walk_Left",	rate = 12,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 12,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 8,		loop = 1 },
	{ action_name = "Move_Right",	rate = 8,		loop = 1 },
	{ action_name = "Move_Front",	rate = 10,		loop = 1 },
	{ action_name = "Move_Back",	rate = 8,		loop = 1 },
	{ action_name = "Attack8_LazerCircle",	rate = 12,		loop = 1 },
	{ action_name = "Attack7_LazerFront",	rate = 12,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 7,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 8,		loop = 1 },
	{ action_name = "Move_Right",	rate = 8,		loop = 1 },
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Back",	rate = 3,		loop = 1 },
	{ action_name = "Attack7_LazerFront",	rate = 12,		loop = 1 },
	{ action_name = "Assault",	rate = 12,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 10,		loop = 1 },
	{ action_name = "Move_Right",	rate = 10,		loop = 1 },
	{ action_name = "Assault",	rate = 10,		loop = 1 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Cut",	rate = 5,		loop = 1, approach = 200.0 },
	{ action_name = "Attack4_BackAttack",	rate = 3,		loop = 1, approach = 200.0 },
}

g_Lua_Skill=
{
	{ skill_index = 20186, SP = 0, cooltime = 20000, rangemin = 200, rangemax = 1000, target = 3, rate = 50 },
}