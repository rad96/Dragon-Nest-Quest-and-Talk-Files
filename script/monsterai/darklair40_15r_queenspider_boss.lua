--AiQueenSpider_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Attack1_Slash_Lv2", rate = 8, loop = 1  },
   { action_name = "Attack2_Blow", rate = 8, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack1_Slash_Lv2", rate = 15, loop = 1  },
   { action_name = "Attack2_Blow", rate = 15, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Front", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack2_Blow", rate = 8, loop = 1  },
   { action_name = "Attack3_JumpAttack_Lv2", rate = 8, loop = 1, randomtarget = "1.1" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Attack3_JumpAttack_Lv2", rate = 2, loop = 1, randomtarget = "1.1" },
}

g_Lua_Skill = { 
   { skill_index = 30201,  cooltime = 20000, rate = 100,rangemin = 100, rangemax = 800, target = 3 },
}
