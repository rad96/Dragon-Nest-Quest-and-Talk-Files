-- Cleric Relic of Hold AI

g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 500.0;
g_Lua_NearValue2 = 1500.0;

g_Lua_LookTargetNearState = 2;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;

g_Lua_Near1 = 
{
    { action_name = "Stand", rate = 5, loop = 1 }, 
}


g_Lua_Near2 = 
{ 
    { action_name = "Stand", rate = 5, loop = 1 }, 
}

g_Lua_Skill=
{
   { skill_index = 33804, rate = 100, cooltime = 1000, rangemin = 0, rangemax = 6000, target = 3, limitcount = 1 },
   { skill_index = 33805, rate = -1, cooltime = 1000, rangemin = 0, rangemax = 6000, target = 3 },
}