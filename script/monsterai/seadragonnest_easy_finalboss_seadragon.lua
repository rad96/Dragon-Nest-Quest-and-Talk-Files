--AiHalfGolem_Stone_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

--4Phase
g_Lua_Near1 = { 
   { action_name = "1Phse_Stand", rate = 100, loop = 2, selfhppercentrange = "80,100"  },
}
g_Lua_Near2 = { 
   { action_name = "1Phse_Stand", rate = 100, loop = 2, selfhppercentrange = "80,100"  },
}
g_Lua_Near3 = { 
   { action_name = "1Phse_Stand", rate = 100, loop = 2, selfhppercentrange = "80,100"  },
}
g_Lua_Near4 = { 
   { action_name = "1Phse_Stand", rate = 100, loop = 2, selfhppercentrange = "80,100"  },
}
g_Lua_Near5 = { 
   { action_name = "1Phse_Stand", rate = 100, loop = 2, selfhppercentrange = "80,100"  },
}
--1Phase
g_Lua_Near1 = { 
   { action_name = "4Phse_Stand", rate = 100, loop = 2, selfhppercent = 80  },
}
g_Lua_Near2 = { 
   { action_name = "4Phse_Stand", rate = 100, loop = 2, selfhppercent = 80  },
}
g_Lua_Near3 = { 
   { action_name = "4Phse_Stand", rate = 100, loop = 2, selfhppercent = 80  },
}
g_Lua_Near4 = { 
   { action_name = "4Phse_Stand", rate = 100, loop = 2, selfhppercent = 80  },
}
g_Lua_Near5 = { 
   { action_name = "4Phse_Stand", rate = 100, loop = 2, selfhppercent = 80  },
}