--GreenDragonNest_Gate4_QueenSpider_Green.lua
--/genmon 236040
g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 450;
g_Lua_NearValue4 = 700;
g_Lua_NearValue5 = 1000;
g_Lua_NearValue6 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 1  },
   -- { action_name = "Walk_Right", rate = 5, loop = 1  },
   -- { action_name = "Walk_Back", rate = 10, loop = 2  },
   -- { action_name = "Attack1_Slash_Lv3_GreenDragon", rate = 30 },
   -- { action_name = "Attack3_JumpAttack_Lv2", rate = 10 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 1  },
   -- { action_name = "Walk_Right", rate = 5, loop = 1  },
   -- { action_name = "Walk_Back", rate = 5, loop = 2  },
   -- { action_name = "Attack1_Slash_Lv3_GreenDragon", rate = 25 },   
   -- { action_name = "Attack3_JumpAttack_Lv2", rate = 10 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   -- { action_name = "Walk_Left", rate = 5, loop = 1  },
   -- { action_name = "Walk_Front", rate = 10, loop = 1  },
   -- { action_name = "Walk_Right", rate = 5, loop = 1 },
   -- { action_name = "Attack3_JumpAttack_Lv2", rate = 10 },
   -- { action_name = "Attack6_Absorb", rate = 20 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 2  },
   -- { action_name = "Walk_Left", rate = 5, loop = 1  },
   -- { action_name = "Walk_Right", rate = 5, loop = 1  },
   -- { action_name = "Walk_Front", rate = 10, loop = 1  },
   -- { action_name = "Walk_Back", rate = 5, loop = 1 },
   -- { action_name = "Attack3_JumpAttack_Lv2", rate = 10 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 5, loop = 2  },
   -- { action_name = "Walk_Left", rate = 5, loop = 1  },
   -- { action_name = "Walk_Right", rate = 5, loop = 1  },
   -- { action_name = "Walk_Front", rate = 15, loop = 1  },
   -- { action_name = "Attack3_JumpAttack_Lv2", rate = 10},
}
g_Lua_Near6 = { 
   { action_name = "Stand", rate = 5, loop = 2  },
   -- { action_name = "Walk_Left", rate = 3, loop = 3  },
   -- { action_name = "Walk_Right", rate = 3, loop = 3  },
   -- { action_name = "Walk_Front", rate = 10, loop = 3  },
   -- { action_name = "Attack3_JumpAttack_Lv2", rate = 10 },
}

g_Lua_Skill = { 
   -- { skill_index = 31411,  cooltime = 60000, rate = 60,rangemin = 200, rangemax = 5000, target = 3, selfhppercent = 100,encountertime = 20000,},--�Ź̻��
   -- { skill_index = 31412,  cooltime = 60000, rate = 100,rangemin = 200, rangemax = 5000, target = 1, selfhppercent = 25},--����ȭ
   -- { skill_index = 31413,  cooltime = 40000, rate = 50,rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100},--������
   { skill_index = 31414,  cooltime = 10000, rate = 50,rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 100},--�Ź��ٻѸ���
      { skill_index = 31418,  cooltime = 15000, rate = 50,rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 100},--�Ź��ٻѸ���2
   -- { skill_index = 31415,  cooltime = 15000, rate = 80,rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 50},--�Ѹ�����
      -- { skill_index = 31417, cooltime = 1, rate = 100, rangemin = 0, rangemax = 9000, target = 1, limitcount = 1 },--���̽�ų.
   -- { skill_index = 31416,  cooltime = 60000, rate = 100,rangemin = 200, rangemax = 800, target = 3, selfhppercent = 3},--����
}
