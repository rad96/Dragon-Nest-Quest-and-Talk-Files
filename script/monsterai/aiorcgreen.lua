-- Orc AI

g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 0.0;
g_Lua_NearValue2 = 150.0;
g_Lua_NearValue3 = 250.0;
g_Lua_NearValue4 = 450.0;
g_Lua_NearValue5 = 900.0;
g_Lua_NearValue6 = 1200.0;


g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;

g_Lua_CustomAction =
{
	CustomAction1 = 
	{
		{ "Attack2", 1},
		{ "Attack1", 1},
	},

	CustomAction2 = 
	{
		{ "Attack2", 1},
		{ "Attack3_Air", 1},
	},
	 
	CustomAction3 = 
	{
		{ "Attack3_Air", 1},
		{ "Attack1", 1},
		{ "Move_Back", 1},
		{ "Attack6_HeavyAttack", 1},
	},

	CustomAction4 = 
	{
		{ "Attack2", 1},
		{ "Attack3_Air", 1},
		{ "Attack1", 1},
		{ "Attack4_Haul", 1},
		{ "Attack6_HeavyAttack", 1},
	},

	CustomAction5 = 
	{
		{ "Attack6_HeavyAttack", 1},
		{ "Move_Front", 1},
		{ "Attack3_Air", 1},
	}
}

g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Back",	rate = 10,		loop = 1 },
	{ action_name = "Move_Back",	rate = 10,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Attack1_Pushed",	rate = 10,		loop = 1 },
	{ action_name = "Attack3_Air",	rate = 15,		loop = 1 },
	{ action_name = "CustomAction1", rate = 0,		loop = 1 },
	{ action_name = "CustomAction2", rate = 0,		loop = 1 },
	{ action_name = "CustomAction4", rate = 0,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Attack2_bash",	rate = 10,		loop = 1,	cancellook = 1 },
--	{ action_name = "Attack7_Nanmu",	rate = 5,		loop = 1 },
	{ action_name = "CustomAction3", rate = 0,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 1,		loop = 2 },
	{ action_name = "Move_Left",	rate = 1,		loop = 2 },
	{ action_name = "Move_Right",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 3,		loop = 2 },
	{ action_name = "Move_Back",	rate = 1,		loop = 2 },
--	{ action_name = "Attack7_Nanmu",	rate = 1,		loop = 1 },
	{ action_name = "Attack6_HeavyAttack",	rate = 10,		loop = 1 },
	{ action_name = "Attack4_Haul",	rate = 5,		loop = 1 },
	{ action_name = "Assault",	rate = 10,		loop = 1 },
}


g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 1,		loop = 2 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
	{ action_name = "Move_Front",	rate = 25,		loop = 2 },
	{ action_name = "Move_Back",	rate = 1,		loop = 2 },
	{ action_name = "Assault",	rate = 20,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 3 },
	{ action_name = "Move_Right",	rate = 3,		loop = 3 },
	{ action_name = "Move_Front",	rate = 30,		loop = 3 },
}

g_Lua_MeleeDefence =
{ 
	{ action_name = "Attack1_Pushed",	rate = 30,		loop = 1 },
	{ action_name = "Attack3_Air",	rate = 50,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
}

g_Lua_RangeDefence =
{      
        { action_name = "Assault",	rate = 50,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 20,		loop = 2 },

}

g_Lua_Assault =
{ 
	{ action_name = "Attack6_HeavyAttack",	rate = 5,	loop = 1, cancellook = 0, approach = 400.0 },
	{ action_name = "Attack6_HeavyAttack",	rate = 5,	loop = 1, cancellook = 1, approach = 400.0 },
	{ action_name = "Attack2_bash",	rate = 30,		loop = 2, cancellook = 1, approach = 250.0 },
}
