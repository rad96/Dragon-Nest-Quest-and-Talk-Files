--DarkLairDefence_Lv5_HopGoblinGreen3.lua
--기본공격 : 물기, 난무
--어퍼 고블린, 무조건 근접하려고 한다.
--어설트로 어퍼를 사용


g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 1, loop = 1  },
   { action_name = "Move_Back", rate = 13, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Attack1_Bite", rate = 13, loop = 1, target_condition = "State1" },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 1, loop = 1  },
   { action_name = "Move_Back", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = 6, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Attack1_Bite", rate = 8, loop = 1, target_condition = "State1" },
   { action_name = "Attack2_Nanmu", rate = 8, loop = 1, target_condition = "State1" },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 3, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Attack3_Upper_3", rate = 13, loop = 1, cooltime = 12000  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 3  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Attack3_Upper_3", rate = 8, loop = 1, cooltime = 12000  },
}
g_Lua_Near5 = { 
   { action_name = "Stand2", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 3 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
}