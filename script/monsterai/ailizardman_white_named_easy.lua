--AiLizardman_White_Named_Easy.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 500;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack2_Pushed", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack2_Pushed", rate = 2, loop = 1 },
   { action_name = "Attack1_Piercing", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 2 },
   { action_name = "Move_Front", rate = 8, loop = 2 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 2 },
   { action_name = "Move_Front", rate = 8, loop = 2 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 3, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 2 },
}
g_Lua_MeleeDefense = { 
   { action_name = "Attack2_Pushed", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 1, loop = 1 },
   { action_name = "Move_Right", rate = 1, loop = 1 },
}
g_Lua_RangeDefense = { 
   { action_name = "Assault", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 4, loop = 2 },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 4, loop = 2 },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Pushed", rate = 15, loop = 1 },
   { action_name = "Attack1_Piercing", rate = 30, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 20082,  cooltime = 25000, rate = 100,rangemin = 300, rangemax = 1000, target = 3 },
   { skill_index = 20089,  cooltime = 27000, rate = 50, rangemin = 600, rangemax = 1600, target = 3, randomtarget = 1.1 },
}
