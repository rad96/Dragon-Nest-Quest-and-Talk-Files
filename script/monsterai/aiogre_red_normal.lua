--AiOgre_Red_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_3", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 16, loop = 1  },
   { action_name = "Attack1_bash", rate = 12, loop = 1  },
   { action_name = "Attack6_HornAttack", rate = 6, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_3", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_bash", rate = 8, loop = 1  },
   { action_name = "Attack6_HornAttack", rate = 14, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_3", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
   { action_name = "Attack6_HornAttack", rate = 6, loop = 1  },
   { action_name = "Assault", rate = 18, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_3", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 15, loop = 1  },
   { action_name = "Walk_Right", rate = 15, loop = 1  },
   { action_name = "Walk_Front", rate = 50, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Assault", rate = 38, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_3", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 1  },
   { action_name = "Assault", rate = 27, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Front", rate = 30, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Assault", rate = 20, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_bash", rate = 3, loop = 1, approach = 250.0  },
   { action_name = "Attack6_HornAttack", rate = 5, loop = 1, approach = 300.0  },
}
g_Lua_Skill = { 
   { skill_index = 20131,  cooltime = 15000, rate = 100,rangemin = 0, rangemax = 800, target = 3, selfhppercent = 100 },
   { skill_index = 20132,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 80 },
   { skill_index = 20133,  cooltime = 50000, rate = -1, rangemin = 0, rangemax = 1500, target = 1, selfhppercent = 70 },
}
