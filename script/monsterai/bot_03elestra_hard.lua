
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" },
 State2 = {"Move|Attack", "Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Down|!Move","Air"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 이베이전슬래쉬
  CustomAction6 = {
     { "useskill", lua_skill_index = 28, rate = 100 },
  },
  -- 딥스
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 28, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
     { action_name = "Attack_Down", rate = 10, loop = 1, cooltime = 23000, target_condition = "State1" },
     { action_name = "Move_Left", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Right", rate = 5, loop = 1, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 15000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 16000, target_condition = "State2" },
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 20, loop = 1 },
     { action_name = "Move_Right", rate = 20, loop = 1 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 15000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 12000, globalcooltime = 2, target_condition = "State2" },
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}

g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
     --{ action_name = "SKill_Dash", rate = -1, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Tumble_Back", rate = 20, loop = 1, globalcooltime = 1 },
	 { action_name = "Skill_ShockWave", rate = 30, loop = 1, cooltime = 15000,},
     { action_name = "CustomAction2", rate = 10, loop = 1  },
     { action_name = "CustomAction3", rate = 10, loop = 1 },
}

g_Lua_NonDownRangeDamage = {

	 { action_name = "useskill", lua_skill_index = 11, rate = 30 },
	 { action_name = "useskill", lua_skill_index = 10, rate = 50 },
}

g_Lua_Skill = { 
-- 0플레임 웜
     { skill_index = 83001, cooltime = 5600, rate = 10, rangemin = 0, rangemax = 600, target = 3, combo1 = "17,60,1", target_condition = "State1" ,priority= 1  },-- 액션프레임끊기 nextcustomaction 시그널
	 
-- 1블링크_B
    { skill_index = 83002, cooltime = 2080, rate = -1, rangemin = 0, rangemax = 600, target = 3, combo1 = "31,50,0", combo2 = "30,50,0", combo3 = "18,50,0" },
-- 2블링크_F
    { skill_index = 83003, cooltime = 2080, rate = 30, rangemin = 0, rangemax = 900, target = 3, globalcooltime = 1 , combo1 = "31,50,0", combo2 = "30,50,0", combo3 = "18,50,0"},-- 케릭터 어그로유지 LocktargetLook
-- 3블링크_R
     { skill_index = 83004, cooltime = 2080, rate = -1, rangemin = 0, rangemax = 600, target = 3, combo1 = "31,50,0", combo2 = "30,50,0", combo3 = "18,50,0" },
-- 4블링크_L	 
	 { skill_index = 83005, cooltime = 2080, rate = -1, rangemin = 0, rangemax = 600, target = 3, combo1 = "31,50,0", combo2 = "30,50,0", combo3 = "18,50,0"},-- 인풋으로 발동하는 건 액션마다 테이블 다 등록

-- 5슬랩어택
     { skill_index = 83006, cooltime = 3000, rate = 70, rangemin = 0, rangemax = 150, target = 3, combo1 = "17,50,1", combo2 = "15,60,1", combo3 = "39,50,1" },
-- 6힐 어택
     { skill_index = 83008, cooltime = 2400, rate = -1, rangemin = 0, rangemax = 70, target = 3, combo1 = "31,100,1", priority= 1 , target_condition = "State1"  },
-- 7해머어택
     { skill_index = 83009, cooltime = 2400, rate = -1, rangemin = 0, rangemax = 100, target = 3,  combo1 ="5,50,1", combo2 ="15,60,1", combo3= "38,60,1" },
-- 8점프
     { skill_index = 83054, cooltime = 5000, rate = 30, rangemin = 0, rangemax = 200, target = 3, combo1 ="7,100,0", target_condition = "State3" },
-- 9카운터 스펠
     { skill_index = 83010, cooltime = 12800, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "2,60,1", combo2 = "38,100,1" },

-- 10에스케이프_B
     { skill_index = 83011, cooltime = 11200, rate = -1, rangemin = 0, rangemax = 700, target = 3},
-- 11에스케이프_F
     { skill_index = 83012, cooltime = 11200, rate = -1, rangemin = 0, rangemax = 700, target = 3},
-- 12에스케이프_R
     { skill_index = 83013, cooltime = 11200, rate = -1, rangemin = 0, rangemax = 700, target = 3},
-- 13에스케이프_L
     { skill_index = 83014, cooltime = 11200, rate = -1, rangemin = 0, rangemax = 700, target = 3 },	 

	 
-- 14에어리얼 이베이전
     { skill_index = 83015, cooltime = 14400, rate = 30, rangemin = 0, rangemax = 900, target = 3, combo1 = "27,100,1" },
-- 15포이즌 미사일
     { skill_index = 83016, cooltime = 12800, rate = 50, rangemin = 0, rangemax = 700, target = 3 , priority= 3 , combo1 =" 24,30,1", combo2= "39,50,1", combo3 = "23,50,1", combo4 = "37,60,1", combo5 = "40,100,1", target_condition = "State1" },
-- 16휠링 스태프
     { skill_index = 83018, cooltime = 6400, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "38,100,1" },

-- 17평타
     { skill_index = 83055, cooltime = 0, rate = 50, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "15,50,1", combo2 = "38,100,1",  target_condition = "State3" },

-- 18쇼크 웨이브
     { skill_index = 83019, cooltime = 18400, rate = 50, rangemin = 0, rangemax = 200, target = 3, cancellook = 1, combo1 = "2,60,1", combo2 = "8,70,1", combo3 = "20,80,1", combo4 = "37,100,1"},
-- 19글레이셜 스파이크
     { skill_index = 83020, cooltime = 5200, rate = 20, rangemin = 300, rangemax = 1200, target = 3, cancellook = 1 },
-- 20포스 익스플로전
     { skill_index = 83021, cooltime = 14400, rate = 10, rangemin = 350, rangemax = 900, target = 3, cancellook = 1 },	

-- 21파이어 월
     { skill_index = 83026, cooltime = 26400, rate = 20, rangemin = 0, rangemax = 300, target = 3, combo1 = "8,50,1", combo2 ="15,60,1", combo3 = "38,100,0" },
-- 22플레임 로드
     { skill_index = 83029, cooltime = 41600, rate = 30, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "38,50,1", combo2 = "39,100,1" , target_condition = "State3" },
-- 23인페르노
     { skill_index = 83030, cooltime = 19200, rate = 30, rangemin = 0, rangemax = 700, target = 3, cancellook = 1, combo1 = "38,50,1", combo2 = "2,60,1",priority= 3 , target_condition = "State1"},
-- 24파이어 버드
     { skill_index = 83031, cooltime = 16000, rate = 10, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "26,80,1", combo2 = "25,20,1", combo3 = "28,30,0" },

-- 25이그니션
     { skill_index = 83032, cooltime = 36000, rate = 50, rangemin = 200, rangemax = 700, target = 3, cancellook = 1, combo1 = "17,100,1", blowcheck = "42", target_condition = "State4" },
-- 26아이시 프렉션
     { skill_index = 83036, cooltime = 36000, rate = 50, rangemin = 200, rangemax = 700, target = 3, cancellook = 1, combo1 = "17,100,1", blowcheck = "144", target_condition = "State4" },

-- 27 글레이셜 웨이브
     { skill_index = 83033, cooltime = 19200, rate = 30, rangemin = 0, rangemax = 270, target = 3, combo1 = "36,50,1",combo2= "5,60,1", combo3 =" 38,80,0", combo4 = "40,100,1" },
-- 28 프리징스파이크ex
     { skill_index = 83034, cooltime = 12000, rate = 20, rangemin = 50, rangemax = 600, target = 3, globalcooltime = 2, target_condition = "State2" },
-- 29 프리징 실드
     { skill_index = 83038, cooltime = 300000, rate = -1, rangemin = 50, rangemax = 1200, target = 3,  priority= 3 },

-- 30 플레임 스파크
     { skill_index = 83041, cooltime = 6400, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "17,80,1", combo2 = "20,100,0" },	
-- 31 프로스트 윈드
     { skill_index = 83042, cooltime = 6400, rate = -1, rangemin = 0, rangemax = 150, target = 3, combo1 = "5,60,1", combo2= "15, 70,1", combo3 ="17,100,1" },	

-- 32 파이어 볼 시작
     { skill_index = 83044, cooltime = 20000, rate = 20, rangemin = 300, rangemax = 1200, target = 3, combo1= "33,50,0", combo2 = "34,100,0" },--몬스터는 점프시그널이 안뎀 그래서 벨로시티시그널 써야뎀, 몬스터는 크로스헤어로 타겟잡으면 안뎀
-- 33 파이어 볼 노말 차징
     { skill_index = 83045, cooltime = 20000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "21,100,0", usedskill = "83044"},
-- 34 파이어 볼 풀 차징
     { skill_index = 83046, cooltime = 20000, rate = -1, rangemin = 300, rangemax = 1200, target = 3, combo1 = "38,100,0" ,usedskill = "83044"},

-- 35 아이스 배리어
     { skill_index = 83047, cooltime = 48000, rate = 20, rangemin = 300, rangemax = 900, target = 3, target_condition = "State4" },	
-- 36 아이스 스피어
     { skill_index = 83049, cooltime = 39200, rate = -1, rangemin = 0, rangemax = 500, target = 3, combo1 ="38,70,0", combo2 = "40,100,0" },

-- 37 프리징 필드EX
     { skill_index = 83050, cooltime = 22400, rate = 20, rangemin = 100, rangemax = 800, target = 3, combo1 = "36,100,1", combo2 = "31,50,0", combo3= "40,100,0" },	 
-- 38 프리징 소드EX
     { skill_index = 83048, cooltime = 12000, rate = 30, rangemin = 0, rangemax = 800, target = 3, combo1 ="15,100,1", combo2 ="30,100,0" },
-- 39 프리징 스파이크EX
     { skill_index = 83051, cooltime = 12000, rate = 20, rangemin = 70, rangemax = 600, target = 3, combo1 =" 15,100,1", combo2 ="40,100,0", globalcooltime = 2 },

-- 40 칠링 미스트EX
     { skill_index = 83052, cooltime = 41600, rate = 20, rangemin = 0, rangemax = 580, target = 3, combo1 = "41,100,0", resetcombo =1 },	 
-- 41 칠링 미스트EX 추가타
     { skill_index = 83053, cooltime = 41600, rate = -1, rangemin = 0, rangemax = 600, target = 3 ,combo1 = "38,70,0", combo2 = "37,100,0", resetcombo =1},	

	 }