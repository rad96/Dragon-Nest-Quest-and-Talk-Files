--SeaDragonNest_Gate5_HideMonster_ShadowTrap.lua
--/genmon 235740
--g_Lua_ApproachValue = 150

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_3", rate = 5, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_3", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_3", rate = 5, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_3", rate = 5, loop = 2  },
}
g_Lua_Skill = { 
--상태효과무시스킬
   { skill_index = 30524,  cooltime = 999000, rate = 100, rangemin = 0, rangemax = 1200, target = 3 },
--그림자함정 shoot
   { skill_index = 30532, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 1900, target = 3, announce = "100,10000",td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = "1,4" },
}
