-- Celberos Hard
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 400.0;
g_Lua_NearValue2 = 800.0;
g_Lua_NearValue3 = 1300.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 320;
g_Lua_AssualtTime = 3000;


g_Lua_CustomAction =
{
	CustomAction1 = 
	{
      			{ "Attack_Down_Left", 0},
			{ "Attack_Down_Right", 0},	 		
	},
	CustomAction2 = 
	{
      			{ "Attack_Down_Right", 0},
			{ "Attack_Down_Left", 0},	 		
	},
	CustomAction3 = 
	{
      			{ "Attack_Cry", 0},
      			{ "Walk_Back", 0},
			{ "Attack_Jump", 0},	 		
	},
	CustomAction4 = 
	{
      			{ "Attack_Cry", 0},
      			{ "Walk_Back", 0},
	},
}
g_Lua_Near1 = 
{ 
	{ action_name = "Stand_2",	rate = 5,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "CustomAction3",	rate = 7,		loop = 1, td = "LF,FL,FR,RF,RB,BR,BL,LB", cooltime = 25000, selfhppercent = 100, existparts="122"  },
	{ action_name = "CustomAction4",	rate = 30,		loop = 1, td = "LF,FL,FR,RF,RB,BR,BL,LB", cooltime = 25000, selfhppercent = 60, existparts="122" },
	{ action_name = "Attack_Punch",	rate = 10,		loop = 1, td = "LF,FL,FR,RF", selfhppercent = 99 },
	{ action_name = "CustomAction1",	rate = 22,	loop = 1, td = "FL,FR", selfhppercent = 99 },
	{ action_name = "CustomAction2",	rate = 22,	loop = 1, td = "FL,FR", selfhppercent = 99 },
	{ action_name = "Turn_Left",	rate = 5,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 5,		loop = -1, td = "RF,RB,BR" },
	{ action_name = "Attack_Back_Left",	rate = 22,		loop = 1, td = "LF,LB,BL" },
	{ action_name = "Attack_Back_Right",	rate = 22,		loop = 1, td = "RF,RB,BR" },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_2",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Front",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "CustomAction3",	rate = 7,		loop = 1, td = "LF,FL,FR,RF,RB,BR,BL,LB", cooltime = 25000, selfhppercent = 100, existparts="122"  },
	{ action_name = "CustomAction4",	rate = 30,		loop = 1, td = "LF,FL,FR,RF,RB,BR,BL,LB", cooltime = 25000, selfhppercent = 60, existparts="122" },
	{ action_name = "Attack_Punch",	rate = 10,		loop = 1, td = "LF,FL,FR,RF", selfhppercent = 99 },
	{ action_name = "Attack_Bite",	rate = 20,		loop = 1, td = "LF,FL,FR,RF", selfhppercent = 40, existparts="121,122,123" },
	{ action_name = "CustomAction1",	rate = 22,	loop = 1, td = "FL,FR", selfhppercent = 99 },
	{ action_name = "CustomAction2",	rate = 22,	loop = 1, td = "FL,FR", selfhppercent = 99 },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_2",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Front",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
	{ action_name = "Attack_Jump",	rate = 20,		loop = 1, td = "FL,FR" },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_2",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Move_Front",	rate = 20,		loop = 2, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
	{ action_name = "Attack_Jump",	rate = 15,		loop = 1, td = "FL,FR" },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand_2",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Move_Front",	rate = 30,	        loop = 2, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 5,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 5,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Skill=
{

	-- 아이스 브레스
	{ skill_index = 30106, SP = 0, cooltime = 30000, rangemin = 100, rangemax = 2500, target = 3, rate = 60, td = "LF,FL,FR,RF", existparts="121", selfhppercent = 80 },
	-- 라이트닝 필드
	{ skill_index = 30112, SP = 0, cooltime = 30000, rangemin = 100, rangemax = 2500, target = 3, rate = 60, td = "LF,FL,FR,RF,RB,BR,BL,LB", existparts="122", selfhppercent = 40 },
	-- 파이어 대쉬
	{ skill_index = 30109, SP = 0, cooltime = 30000, rangemin = 000, rangemax = 3000, target = 3, rate = 60, td = "LF,FL,FR,RF", existparts="123", selfhppercent = 60 },

	-- 아이스볼트
	{ skill_index = 30105, SP = 0, cooltime = 10000, rangemin = 100, rangemax = 2500, target = 3, rate = 60, td = "LF,FL,FR,RF,RB,BR,BL,LB", existparts="121", multipletarget = 1, selfhppercent = 20 },
	-- 아이스볼트
	{ skill_index = 30105, SP = 0, cooltime = 25000, rangemin = 100, rangemax = 2500, target = 3, rate = 45, td = "LF,FL,FR,RF,RB,BR,BL,LB", existparts="121", multipletarget = 1, selfhppercent = 60 },
	-- 아이스볼트
	{ skill_index = 30105, SP = 0, cooltime = 40000, rangemin = 100, rangemax = 2500, target = 3, rate = 30, td = "LF,FL,FR,RF,RB,BR,BL,LB", existparts="121", multipletarget = 1, selfhppercent = 80 },

	-- 낙뢰
	{ skill_index = 30111, SP = 0, cooltime = 10000, rangemin = 100, rangemax = 2500, target = 3, rate = 60, td = "LF,FL,FR,RF,RB,BR,BL,LB", existparts="122", selfhppercent = 20 },
	-- 낙뢰
	{ skill_index = 30111, SP = 0, cooltime = 30000, rangemin = 100, rangemax = 2500, target = 3, rate = 45, td = "LF,FL,FR,RF,RB,BR,BL,LB", existparts="122", selfhppercent = 60 },
	-- 낙뢰
	{ skill_index = 30111, SP = 0, cooltime = 40000, rangemin = 100, rangemax = 2500, target = 3, rate = 30, td = "LF,FL,FR,RF,RB,BR,BL,LB", existparts="122", selfhppercent = 80 },
	
	-- 파이어월
	{ skill_index = 30107, SP = 0, cooltime = 10000, rangemin = 000, rangemax = 800, target = 3, rate = 60, td = "LF,FL,FR,RF,RB,BR,BL,LB", existparts="123", selfhppercent = 20 },
	-- 파이어월
	{ skill_index = 30107, SP = 0, cooltime = 25000, rangemin = 000, rangemax = 800, target = 3, rate = 45, td = "LF,FL,FR,RF,RB,BR,BL,LB", existparts="123", selfhppercent = 60 },
	-- 파이어월
	{ skill_index = 30107, SP = 0, cooltime = 40000, rangemin = 000, rangemax = 800, target = 3, rate = 30, td = "LF,FL,FR,RF,RB,BR,BL,LB", existparts="123", selfhppercent = 80 },

	-- 화염구 난무
	{ skill_index = 30108, SP = 0, cooltime = 15000, rangemin = 300, rangemax = 2000, target = 3, rate = 50, td = "FL,FR", existparts="123", selfhppercent = 95 },
	-- 프리징 볼
	{ skill_index = 30104, SP = 0, cooltime = 15000, rangemin = 500, rangemax = 2000, target = 3, rate = 50, td = "LF,FL,FR,RF", existparts="121", multipletarget = 1, selfhppercent = 95 },
	-- 다크라이트닝
	{ skill_index = 30110, SP = 0, cooltime = 15000, rangemin = 100, rangemax = 2000, target = 3, rate = 50, td = "FL,FR", existparts="122", selfhppercent = 95 },

}

