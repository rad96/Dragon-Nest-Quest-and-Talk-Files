--DarkLairDefence_Lv27_GargoyleRed.lua
--타임아웃 모드, 레드는 보스로 4마리만 나옴, 화염구 위주의 공격을 하다가 시간이 종료되면 광역 스킬을 사용하기 시작
--대쉬공격등은 타임아웃 이전에 사용하지 않는다

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 6, loop = 2  },
   { action_name = "Walk_Right", rate = 6, loop = 2  },
   { action_name = "Attack3_Combo_N", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Back", rate = 4, loop = 2  },
   { action_name = "Move_Back", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_DashAttack", rate = 11, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20124,  cooltime = 9000, rate = 80, rangemin = 0, rangemax = 800, target = 3 },
   { skill_index = 20125,  cooltime = 17000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, encountertime = 100000 },
}
