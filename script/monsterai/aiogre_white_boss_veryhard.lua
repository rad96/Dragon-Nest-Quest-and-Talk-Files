-- Ogre White Elite VeryHard AI

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 250.0;
g_Lua_NearValue2 = 400.0;
g_Lua_NearValue3 = 600.0;
g_Lua_NearValue4 = 800.0;
g_Lua_NearValue5 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 3000;


g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 7,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 1 },
        { action_name = "Walk_Back",	rate = 16,		loop = 1 },
	{ action_name = "Attack1_Bash",	rate = 10,		loop = 1 },
	{ action_name = "Attack2_Berserk",	rate = 2,		loop = 1, cooltime = 10000 },
	{ action_name = "Attack6_HornAttack",	rate = 3,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 15,		loop = 1 },
        { action_name = "Walk_Back",	rate = 10,		loop = 1 },
        { action_name = "Attack1_Bash",	rate = 1,		loop = 1 },
	{ action_name = "Attack2_Berserk",	rate = 5,		loop = 1, cooltime = 10000 },
	{ action_name = "Attack6_HornAttack",	rate = 12,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Front",	rate = 30,		loop = 2 },
	{ action_name = "Attack2_Berserk",	rate = 10,		loop = 1, cooltime = 10000 },
	{ action_name = "Attack6_HornAttack",	rate = 2,		loop = 1 },
        { action_name = "Assault",	rate = 6,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Front",	rate = 20,		loop = 2 },
	{ action_name = "Move_Front",	rate = 50,		loop = 1 },
	{ action_name = "Attack2_Berserk",	rate = 7,		loop = 1, cooltime = 10000 },
	{ action_name = "Attack7_JumpAttack",	rate = 17,		loop = 1 },
        { action_name = "Assault",	rate = 6,		loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Front",	rate = 30,		loop = 1 },
        { action_name = "Assault",	rate = 15,		loop = 1 },
}

g_Lua_NonDownRangeDamage = 				
{ 				
	{ action_name = "Move_Front",	rate = 30,		loop = 1 },
	{ action_name = "Move_Left",	rate = 3,		loop = 3 },
	{ action_name = "Move_Right",	rate = 3,		loop = 3 },
	{ action_name = "Assault",	rate = 10,		loop = 1 },
	{ action_name = "Attack7_JumpAttack",	rate = 20,		loop = 1 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Bash",	rate = 5,	loop = 1, approach = 250.0 },
	{ action_name = "Attack6_HornAttack",	rate = 3,	loop = 1, approach = 300.0 },
}
