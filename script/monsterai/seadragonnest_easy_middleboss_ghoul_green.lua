--SeaDragonNest_MiddleBoss_Ghoul_Green.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Attack1_ArmAttack", rate = 4, loop = 1  },
   { action_name = "Attack2_ArmSlash", rate = 5, loop = 1, max_missradian = 10 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Attack5_BoneThrow_SeaDragon", rate = 4, loop = 1, cooltime = 8000 },
   { action_name = "Attack7_Sliding", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 7, loop = 2  },
   { action_name = "Attack5_BoneThrow_SeaDragon", rate = 4, loop = 1, cooltime = 9000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 7, loop = 2  },
}