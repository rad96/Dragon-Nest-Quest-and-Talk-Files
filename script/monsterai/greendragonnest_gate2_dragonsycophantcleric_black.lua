--/genmon 236009
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
   -- { action_name = "Attack4_Kick", rate = 27, loop = 1 },
   { action_name = "Attack5_MaceCombo", rate = 100, loop = 1, },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Attack6_ShieldCharge", rate = 27, loop = 1},
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   -- { action_name = "Attack7_JudgmentHammer_ArcBishopHell", rate = 27, loop = 1},
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 2 },
   -- { action_name = "Attack7_JudgmentHammer_ArcBishopHell", rate = 10, loop = 1 },
}

g_Lua_Skill = { 
   -- { skill_index = 30991, cooltime = 30000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, limitcount = 1 },--디바인 아바타
   { skill_index = 20574,  cooltime = 30000, rate = 25,rangemin = 0, rangemax = 1500, target = 1, selfhppercent = 50 },--블럭  
   -- { skill_index = 30996, cooltime = 20000, rate = 100, rangemin = 0, rangemax = 1500, target = 3,limitcount =1,SelfHppercent=30 },--토르해머
   -- { skill_index = 31345, cooltime = 15000, rate = 100, rangemin = 500, rangemax = 700, target = 3,SelfHppercent=100 },--홀리크로스
   -- { skill_index = 31346, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1500, target = 3,SelfHppercent=100 },--그랜드 크로스
   -- { skill_index = 20572, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 1500, target = 3,SelfHppercent=100 },--렐릭오브라이트닝.
}