--Runeelf_Black_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1300;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = -1, loop = 1  },
   { action_name = "Move_Right", rate = -1, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1 },
   { action_name = "Attack1_Punch", rate = 15, loop = 1  },
   { action_name = "Attack4_SpinBudKick", rate = -1, loop = 1, cooltime = 12000 },
   { action_name = "Attack6_Laser", rate = 15, loop = 1, cooltime = 12000   },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack1_Punch", rate = 4, loop = 1  },
   { action_name = "Attack8_BusterAttack", rate = 17, loop = 1  },
   { action_name = "Attack4_SpinBudKick", rate = 15, loop = 1, cooltime = 12000 },
   { action_name = "Attack6_Laser", rate = 15, loop = 1, cooltime = 12000   },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 1 },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Attack8_BusterAttack", rate = 15, loop = 1  },
   { action_name = "Attack4_SpinBudKick", rate = -1, loop = 1, cooltime = 12000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = -1, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 17, loop = 1 },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Assault", rate = 15, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = -1, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 3  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Punch", rate = 10, loop = 1, approach = 250.0  },
}