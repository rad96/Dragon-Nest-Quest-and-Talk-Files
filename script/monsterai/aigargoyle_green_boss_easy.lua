--AiGargoyle_Green_Boss_Easy.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 16, loop = 2  },
   { action_name = "Move_Left", rate = -1, loop = 1  },
   { action_name = "Move_Right", rate = -1, loop = 1  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Attack1_DashAttack_N", rate = 9, loop = 1  },
   { action_name = "Attack3_Combo_N", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 8, loop = 2  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_DashAttack_N", rate = 17, loop = 1  },
   { action_name = "Attack3_Combo_N", rate = 17, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 3  },
   { action_name = "Walk_Right", rate = 5, loop = 3  },
   { action_name = "Walk_Back", rate = 5, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 3  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Attack1_DashAttack_N", rate = 20, loop = 1  },
   { action_name = "Attack3_Combo_N", rate = 20, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20116,  cooltime = 10000, rate = 80,rangemin = 0, rangemax = 800, target = 3, selfhppercent = 100 },
   { skill_index = 20117,  cooltime = 40000, rate = 80, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100 },
   { skill_index = 20118,  cooltime = 30000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 100 },
   { skill_index = 20119,  cooltime = 50000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 50 },
}
