--AiGoblin_Purple_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 1700;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack7_Nanmu_Short", 0 },
      { "Stand_1", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 16, loop = 1 },
   { action_name = "Stand3", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "CustomAction1", rate = 4, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 16, loop = 1 },
   { action_name = "Stand3", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 16, loop = 1 },
   { action_name = "Stand3", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack6_ThrowBigstone_Miss", rate = 7, loop = 1, max_missradian = 30, cooltime = 4000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 16, loop = 1 },
   { action_name = "Stand3", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
}