--AiGargoyle_Gray_Elite_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 3  },
   { action_name = "Walk_Right", rate = 2, loop = 3  },
   { action_name = "Walk_Back", rate = 8, loop = 3  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 8, loop = 2  },
   { action_name = "Attack1_DashAttack_N", rate = 5, loop = 1  },
   { action_name = "Attack3_Combo_N", rate = 14, loop = 1  },
   { action_name = "Attack2_SummonRock_Named", rate = 14, loop = 1  },
   { action_name = "Attack2_SummonRock_Named", rate = -1, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Back", rate = 5, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 3  },
   { action_name = "Move_Left", rate = 7, loop = 2  },
   { action_name = "Move_Right", rate = 7, loop = 2  },
   { action_name = "Move_Front", rate = 14, loop = 2  },
   { action_name = "Move_Back", rate = 14, loop = 2  },
   { action_name = "Attack1_DashAttack", rate = 14, loop = 1  },
   { action_name = "Attack3_Combo", rate = 20, loop = 1  },
   { action_name = "Attack2_SummonRock_Named", rate = 20, loop = 1  },
   { action_name = "Attack2_SummonRock_Named", rate = 2, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 3  },
   { action_name = "Walk_Right", rate = 5, loop = 3  },
   { action_name = "Walk_Back", rate = 5, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 3  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Attack1_DashAttack", rate = 30, loop = 2  },
   { action_name = "Attack3_Combo", rate = 30, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20126,  cooltime = 20000, rate = 100,rangemin = 0, rangemax = 1500, target = 1, selfhppercent = 50 },
}
