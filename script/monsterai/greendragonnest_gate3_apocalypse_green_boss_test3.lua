-- /genmon 236010
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 500.0;
g_Lua_NearValue2 = 800.0;
g_Lua_NearValue3 = 1200.0;
g_Lua_NearValue4 = 1700.0;
g_Lua_NearValue5 = 5000.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 800;
g_Lua_AssualtTime = 5000;

g_Lua_Near1 = 
	{
		{ action_name = "Stand_Open",rate = 10, loop = 1 }, 
	}
g_Lua_Near2 = 
	{ 

		{ action_name = "Stand_Open",rate = 10, loop = 1 }, 
	}
g_Lua_Near3 = 
	{
		{ action_name = "Stand_Open",rate = 10, loop = 1 },
	}
g_Lua_Near4 = 
	{
		{ action_name = "Stand_Open",rate = 10, loop = 1 }, 
	}
g_Lua_Near5 = 
	{ 
		{ action_name = "Stand_Open",rate = 10, loop = 1 }, 
	}
g_Lua_Skill = 
	{

		{ skill_index = 31391, cooltime = 5000, rate = 80, rangemin = 0, rangemax = 5000, target = 3 }, -- 외치기(밀쳐내기)
		{ skill_index = 31397, cooltime = 10000, rate = 50, rangemin = 0, rangemax = 5000, target = 3 },  -- 점프

	}
   