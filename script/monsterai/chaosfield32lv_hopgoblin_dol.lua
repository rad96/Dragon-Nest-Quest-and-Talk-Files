g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 1, loop = 1  },
   { action_name = "Move_Back", rate = 16, loop = 2  },
   { action_name = "Walk_Back", rate = 8, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Attack1_Bite", rate = 8, loop = 1, target_condition = "State1" },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 1, loop = 1  },
   { action_name = "Move_Back", rate = 16, loop = 2  },
   { action_name = "Walk_Back", rate = 8, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Attack1_Bite", rate = 8, loop = 1, target_condition = "State1" },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 3, loop = 1  },
   { action_name = "Walk_Back", rate = 12, loop = 2  },
   { action_name = "Walk_Front", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 1, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 3  },
   { action_name = "Move_Left", rate = 4, loop = 1  },
   { action_name = "Move_Right", rate = 4, loop = 1  },
   { action_name = "Attack8_GoblinThrowSton", rate = 15, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand2", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 3 },
   { action_name = "Move_Left", rate = 3, loop = 1 },
   { action_name = "Move_Right", rate = 3, loop = 1 },
   { action_name = "Attack8_GoblinThrowSton", rate = 20, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Bite", rate = 10, loop = 1, approach = 100.0  },
   { action_name = "Attack2_Nanmu", rate = 8, loop = 1, approach = 150.0  },
}
g_Lua_Skill = { 
{ skill_index = 20230,  cooltime = 18000, rate = 100,rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 80 },
}
