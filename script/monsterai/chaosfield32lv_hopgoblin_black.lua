--AiHopGoblin_Black_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack3_Upper_3", 0 },
      { "Attack3_Upper_4", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Attack1_Bite", rate = 9, loop = 1, cooltime = 6000  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Attack2_Nanmu_Black", rate = 15, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Attack3_Upper_3", rate = 13, loop = 1  },
   { action_name = "CustomAction1", rate = 8, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 1  },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 9, loop = 1  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Bite", rate = 5, loop = 1, approach = 100.0  },
   { action_name = "Attack2_Nanmu_Black", rate = 10, loop = 1, approach = 400.0  },
}
g_Lua_Skill = { 
   { skill_index = 20230,  cooltime = 20000, rate = 100,rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 50 },
}
