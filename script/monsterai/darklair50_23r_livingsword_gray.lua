--LivingSword_Gray_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 20, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Attack2_Pierce", rate = 22, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 25, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Attack2_Pierce", rate = 10, loop = 1, max_missradian = 30 },
   { action_name = "Attack3_SpinPierce", rate = 17, loop = 1, max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 26, loop = 1  },
   { action_name = "Attack3_SpinPierce", rate = 15, loop = 1, max_missradian = 20 },
   { action_name = "Assault", rate = 17, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 25, loop = 3 },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Pierce", rate = 5, loop = 1, approach = 200.0, max_missradian = 30 },
}
g_Lua_Skill = { 
   { skill_index = 20607,  cooltime = 20000, rate = 80,rangemin = 0, rangemax = 500, target = 2, selfhppercent = 70 },
}
