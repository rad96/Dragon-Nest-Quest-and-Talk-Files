--AiBoarManGunner_Gray_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack01_Shot_Named", rate = 30, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 20, loop = 2  },
   { action_name = "Attack01_Shot_Named", rate = 65, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 1  },
   { action_name = "Attack01_Shot_Named", rate = 90, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 30, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20341,  cooltime = 10000, rate = 100, rangemin = 200, rangemax = 1500, target = 3, selfhppercent = 100 },
}
