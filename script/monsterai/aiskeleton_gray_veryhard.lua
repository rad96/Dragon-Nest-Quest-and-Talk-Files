-- Skeleton Gray Master AI

g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 300.0;
g_Lua_NearValue3 = 700.0;
g_Lua_NearValue4 = 900.0;
g_Lua_NearValue5 = 1200.0;
g_Lua_NearValue6 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" },
}


g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Back",	rate = 20,		loop = 2 },
	{ action_name = "Move_Back",	rate = 20,		loop = 2 },
	{ action_name = "Attack1_Chop",	rate = 11,		loop = 1 },
	{ action_name = "Attack4_Upper",	rate = -1,		loop = 1 },
	{ action_name = "Attack2_Cut",	rate = 25,		loop = 1,  cooltime = 5000, target_condition = "State1" },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Walk_Left",	rate = 20,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 20,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 20,		loop = 2 },
	{ action_name = "Attack1_Chop",	rate = 9,		loop = 1 },
	{ action_name = "Attack4_Upper",	rate = -1,		loop = 1 },
	{ action_name = "Attack8_Whirlwind",	rate = 7,		loop = 1 },
	{ action_name = "Attack2_Cut",	rate = 5,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 20,		loop = 1 },
	{ action_name = "Assault",	rate = 3,		loop = 1 },
	{ action_name = "Attack8_Whirlwind",	rate = 7,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 1,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Back",	rate = 1,		loop = 3 },
	{ action_name = "Assault",	rate = 12,		loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 30,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Front",	rate = 35,		loop = 3 },
	{ action_name = "Assault",	rate = 15,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 30,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Front",	rate = 30,		loop = 3 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Chop",	rate = 5,	loop = 1, approach = 150.0 },
	{ action_name = "Attack4_Upper",	rate = -1,	loop = 1, approach = 150.0 },
}