

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 120;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
      { "Skill_Dash" },
      { "Skill_DashSlash" },
      { "Skill_DashCombo" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
      { "Tumble_Left" },
      { "Skill_DashCombo" },
  },
-- 우측덤블링어택
  CustomAction3 = {
      { "Tumble_Right" },
      { "Skill_DashCombo" },
  },
}



g_Lua_Near1 = 
{ 
-- 0 ~ 120 :
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Attack_SideKick",	rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "CustomAction2",	rate = 20,		loop = 1,	selfhppercentrange = "0,70", cooltime = 30000 },
	{ action_name = "CustomAction3",	rate = 20,		loop = 1,	selfhppercentrange = "0,70", cooltime = 30000 },
	{ action_name = "Move_Back",		rate = 30,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Tumble_Front",		rate = 10,		loop = 1,	selfhppercentrange = "0,60", cooltime = 30000 },
	{ action_name = "Tumble_Back",		rate = 10,		loop = 1,	selfhppercentrange = "0,60", cooltime = 30000 },
	{ action_name = "Tumble_Left",		rate = 10,		loop = 1,	selfhppercentrange = "0,60", cooltime = 30000 },
	{ action_name = "Tumble_Right",		rate = 10,		loop = 1,	selfhppercentrange = "0,60", cooltime = 30000 },
}
g_Lua_Near2 = 
{ 
-- 120 ~ 500 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "CustomAction2",	rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "CustomAction3",	rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Back",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Tumble_Front",		rate = 10,		loop = 1,	selfhppercentrange = "0,40", cooltime = 20000 },
	{ action_name = "Tumble_Back",		rate = 10,		loop = 1,	selfhppercentrange = "0,40", cooltime = 20000 },
	{ action_name = "Tumble_Left",		rate = 10,		loop = 1,	selfhppercentrange = "0,40", cooltime = 20000 },
	{ action_name = "Tumble_Right",		rate = 10,		loop = 1,	selfhppercentrange = "0,40", cooltime = 20000 },
}

g_Lua_Near3 = 
{ 
-- 500 ~ 800 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "CustomAction2",	rate = 10,		loop = 1,	selfhppercentrange = "0,30", cooltime = 30000  },
	{ action_name = "CustomAction3",	rate = 10,		loop = 1,	selfhppercentrange = "0,30", cooltime = 30000  },
	{ action_name = "Move_Left",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Assault", 		rate = 10, 		loop = 1,	selfhppercentrange = "0,70" },
}

g_Lua_Near4 = 
{ 
-- 800 ~ 1200 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "CustomAction1",	rate = 30,		loop = 1,	selfhppercentrange = "0,70" },
	{ action_name = "Move_Front",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Assault", 		rate = 20, 		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near5 = 
{ 
-- 1200 ~ 10000 : 
	{ action_name = "Stand",		rate = 8,		loop = 2,	selfhppercentrange = "0,100" },
	{ action_name = "CustomAction1",	rate = 20,		loop = 1,	selfhppercentrange = "0,70" },
	{ action_name = "Assault", 		rate = 30, 		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },

}

g_Lua_Assault = { 
   { action_name = "Skill_DashSlash", rate = 30, loop = 1, approach = 300   },
}

g_Lua_BeHitSkill = { 
   { lua_skill_index = 9, rate = 100, skill_index = 30316  },
}


g_Lua_Skill = { 
-- 덤블링 앞쪽
   { skill_index = 31121,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "RB,BR,BL,LB"  },
-- 덤블링 뒤쪽
   { skill_index = 31122,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FL,FR,LF,RF"  },
-- 덤블링 오른쪽
   { skill_index = 31123,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FL,LF,RB,BR,BL"  },
-- 덤블링 왼쪽
   { skill_index = 31124,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FR,RF,RB,BR,BL"  },
-- 임팩트펀치
   { skill_index = 31001,  cooltime = 6000, rate = 100, rangemin = 0, rangemax = 200,target = 3  },
-- 헤비슬래쉬
   { skill_index = 31002,  cooltime = 10000, rate = 100, rangemin = 200, rangemax = 600,target = 3  },
-- 서클브레이크
   { skill_index = 31004,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 800,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- 임팩트웨이브
   { skill_index = 31003,  cooltime = 8000, rate = 100, rangemin = 300, rangemax = 1000,target = 3, selfhppercentrange = "0,90"  },

-- 이클립스
   { skill_index = 31012,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 800,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,80" },
-- 딥스트레이트콤보
   { skill_index = 31013,  cooltime = 18000, rate = 100, rangemin = 300, rangemax = 1000,target = 3, td = "FL,FR" },
-- 라인드라이브
   { skill_index = 31014,  cooltime = 45000, rate = 100, rangemin = 100, rangemax = 800,target = 3, td = "FL,FR", selfhppercentrange = "0,70" },
-- 크레센트클리브
   { skill_index = 31011,  cooltime = 18000, rate = 100, rangemin = 300, rangemax = 1500, target = 3, td = "FL,FR,LF,RF" },
-- 하프문슬래쉬
   { skill_index = 31015,  cooltime = 45000, rate = 100, rangemin = 500, rangemax = 1200,target = 3, td = "FL,FR,LF,RF"  },
}