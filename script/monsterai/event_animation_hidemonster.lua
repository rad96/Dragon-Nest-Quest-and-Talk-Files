
g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 5000;
g_Lua_NearValue2 = 9000;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;

g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
}

g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 4, loop = 1 },
}

g_Lua_Skill =
{
     { skill_index = 31611, rate = -1, cooltime = 10000, rangemin = 0, rangemax = 5000, target = 3 },	 
} 
  