

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 2000;
g_Lua_NearValue2 = 4000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 1000
g_Lua_AssualtTime = 5000
g_Lua_GlobalCoolTime1 = 25000
g_Lua_OnlyPartsDamage = 1

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 1},
	{ action_name = "Turn_Left",		rate = 2, cooltime = 25000, globalcooltime = 1, custom_state1 = "custom_ground",	loop = -1,	td = "BL", encountertime="40000" },
	{ action_name = "Turn_Right",		rate = 2, cooltime = 25000,  globalcooltime = 1, custom_state1 = "custom_ground",	loop = -1,	td = "BR", encountertime="40000"  },
	-- { action_name = "5Phase_Attack029_TailStrike", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", cooltime = 10000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", cooltime = 10000, rate = 4,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF" },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 4, cooltime = 10000,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR" },
	{ action_name = "1Phase_Attack029_GraptoThrow_Right", rate = 2,custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", cooltime = 10000 },
	{ action_name = "1Phase_Attack028_GraptoThrow_Left", rate = 2,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", cooltime = 10000 },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 2,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", cooltime = 10000 },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", cooltime = 10000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", cooltime = 10000 },
}

g_Lua_Near2 = 
{ 
 	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 1},
	{ action_name = "Turn_Left",		rate = 2, cooltime = 25000, globalcooltime = 1, custom_state1 = "custom_ground",	loop = -1,	td = "BL", encountertime="40000" },
	{ action_name = "Turn_Right",		rate = 2, cooltime = 25000,  globalcooltime = 1, custom_state1 = "custom_ground",	loop = -1,	td = "BR", encountertime="40000"  },
	-- { action_name = "5Phase_Attack029_TailStrike", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", cooltime = 10000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", cooltime = 10000, rate = 4,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF" },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 4, cooltime = 10000,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR" },
	{ action_name = "1Phase_Attack029_GraptoThrow_Right", rate = 2,custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", cooltime = 10000 },
	{ action_name = "1Phase_Attack028_GraptoThrow_Left", rate = 2,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", cooltime = 10000 },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 2,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", cooltime = 10000 },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", cooltime = 10000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", cooltime = 10000 },
}


