--AiHalfGolem_Stone_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 700;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 2000;
g_Lua_NearValue4 = 4000;
g_Lua_NearValue5 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_OnlyPartsDamage = 2

g_Lua_PartsProcessor = {
   { hp="60,100", ignore="163,164" },
   { hp="0,60", ignore="164", nodamage="164" },
   { hp="0,60", checkblow ="121", ignore="163", nodamage="163", active="164" },
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack07_SummonRock", 0 },
      { "Attack01_BashRight", 0 },
  },
  CustomAction2 = {
      { "Attack07_SummonRock", 0 },
      { "Attack02_BashLeft", 0 },
  },
  CustomAction3 = {
      { "Attack04_StompLeft", 0 },
      { "Attack03_StompRight", 0 },
  },
}

g_Lua_GlobalCoolTime1 = 12000


g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000, notusedskill = 30971 },
--   { action_name = "PropAttack_Punch", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 40, notusedskill = 30971 },
--   { action_name = "PropAttack_Lazer", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 20, notusedskill = 30971 },
   { action_name = "Attack05_PunchBreak", rate = 8, loop = 1, cooltime = 15000, td = "FR,FL", notusedskill = 30971, selfhppercent = 100 },
   { action_name = "CustomAction1", rate = 4, loop = 1, cooltime = 15000, td = "FR,RF,RB,BR", notusedskill = 30971, selfhppercentrange = "40,100" },
   { action_name = "CustomAction2", rate = 4, loop = 1, cooltime = 15000, td = "FL,LF,LB,BL", notusedskill = 30971, selfhppercentrange = "40,100" },
   { action_name = "CustomAction3", rate = 4, loop = 1, cooltime = 15000, td = "FR,RF,RB,BR,FL,LF,LB,BL", notusedskill = 30971, selfhppercentrange = "40,100" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000, notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Front_Hell", rate = 5, loop = 1, cooltime = 20000, td = "LF,FL,FR,RF", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "Attack10_EyeLaser_Left", rate = 5, loop = 1, cooltime = 20000, td = "LF,FL,LB", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "Attack10_EyeLaser_Right", rate = 5, loop = 1, cooltime = 20000, td = "FR,RF,RB", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "PropAttack_Punch", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 40, notusedskill = 30971 },
--   { action_name = "PropAttack_Lazer", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 20, notusedskill = 30971 },
   { action_name = "CustomAction1", rate = 4, loop = 1, cooltime = 15000, td = "FR,RF,RB,BR", notusedskill = 30971, selfhppercentrange = "40,100" },
   { action_name = "CustomAction2", rate = 4, loop = 1, cooltime = 15000, td = "FL,LF,LB,BL", notusedskill = 30971, selfhppercentrange = "40,100" },
   { action_name = "CustomAction3", rate = 4, loop = 1, cooltime = 15000, td = "FR,RF,RB,BR,FL,LF,LB,BL", notusedskill = 30971, selfhppercentrange = "40,100" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000, notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Front_Hell", rate = 10, loop = 1, cooltime = 20000, td = "LF,FL,FR,RF", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "Attack10_EyeLaser_Left", rate = 10, loop = 1, cooltime = 20000, td = "LF,FL,LB", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "Attack10_EyeLaser_Right", rate = 10, loop = 1, cooltime = 20000, td = "FR,RF,RB", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "PropAttack_Punch", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 40, notusedskill = 30971 },
--   { action_name = "PropAttack_Lazer", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 20, notusedskill = 30971 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, cooltime = 15000, notusedskill = 30971 },
   { action_name = "Attack10_EyeLaser_Front_Hell", rate = 10, loop = 1, cooltime = 20000, td = "LF,FL,FR,RF", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "Attack10_EyeLaser_Left", rate = 10, loop = 1, cooltime = 20000, td = "LF,FL,LB", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "Attack10_EyeLaser_Right", rate = 10, loop = 1, cooltime = 20000, td = "FR,RF,RB", selfhppercentrange = "40,100", notusedskill = 30971 },
--   { action_name = "PropAttack_Punch", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 40, notusedskill = 30971 },
--   { action_name = "PropAttack_Lazer", rate = 8, loop = 1, cooltime = 30000, selfhppercent = 20, notusedskill = 30971 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, cooltime = 15000, notusedskill = 30971 },
}

g_Lua_Skill = { 
-- 부위타격표시
   { skill_index = 30985,  cooltime = 9990000, rate = 100,rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 60, limitcount = 1, notusedskill = 30971 },
-- 그로기
--   { skill_index = 30971,  cooltime = 30000, rate = 100,rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 100 },
-- 포이즌 버스트
   { skill_index = 30977,  cooltime = 30000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 20, usedskill = 30971, blowcheck = "44" },
-- 광폭화
   { skill_index = 30974,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 20, notusedskill = 30971 },

-- 어스퀘이크
   { skill_index = 30983,  cooltime = 30000, rate = 70, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 40, notusedskill = 30971, multipletarget = "1" },
-- 센터레이저
   { skill_index = 30979,  cooltime = 60000, rate = 70, rangemin = 500, rangemax = 4000, target = 3, selfhppercent = 40, notusedskill = 30971, globalcooltime = 1 },
-- 포이즌 스프링
   { skill_index = 30978,  cooltime = 15000, rate = 80, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 40, usedskill = 30971 },

-- 외치기
   { skill_index = 30973,  cooltime = 40000, rate = 50, rangemin = 200, rangemax = 4000, target = 3, selfhppercent = 60, notusedskill = 30971, globalcooltime = 1 },
-- 포이즌 브레스
   { skill_index = 30972,  cooltime = 30000, rate = 50, rangemin = 400, rangemax = 4000, target = 3, selfhppercent = 60, notusedskill = 30971 },
-- 멀티미사일볼
   { skill_index = 30976,  cooltime = 15000, rate = 80, rangemin = 500, rangemax = 4000, target = 3, selfhppercent = 60, usedskill = 30971 },
-- 포이즌 레인
   { skill_index = 30975,  cooltime = 20000, rate = 50, rangemin = 300, rangemax = 1000, target = 3, selfhppercent = 60, usedskill = 30971 },


-- 프랍 소환
   { skill_index = 30986,  cooltime = 50000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 80, notusedskill = 30971, globalcooltime = 1 },
-- 서먼 볼케이노
   { skill_index = 30984,  cooltime = 50000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 80, notusedskill = 30971 },
-- 서먼 락
--   { skill_index = 30982,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 80, notusedskill = 30971 },
-- 잡아던지기
   { skill_index = 30980,  cooltime = 20000, rate = 80, rangemin = 500, rangemax = 800, target = 3, selfhppercent = 80, notusedskill = 30971 },
}
