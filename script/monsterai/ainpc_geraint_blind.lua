--AiNPC_Geraint_Blind.lua

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 400.0;
g_Lua_NearValue3 = 800.0;
g_Lua_NearValue4 = 1000.0;
g_Lua_NearValue5 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;

g_Lua_Near1 = 
{ 
	{ action_name = "Stand5",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Back_Blind",	rate = 15,		loop = 1 },
	{ action_name = "Attack1_Upper_Blind",	rate = 10,		loop = 1 },
	{ action_name = "Attack3_Slash_Blind",	rate = 9,		loop = 1 },
	{ action_name = "Attack5_Casting2_Blind",	rate = 5,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand5",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Front_Blind",	rate = 12,		loop = 2 },
	{ action_name = "Walk_Left_Blind",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right_Blind",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back_Blind",	rate = 5,		loop = 1 },
	{ action_name = "Attack1_Upper_Blind",	rate = 14,		loop = 1 },
	{ action_name = "Attack3_Slash_Blind",	rate = 8,		loop = 1 },
	{ action_name = "Attack5_Casting2_Blind",	rate = 16,		loop = 1 },
	{ action_name = "Attack4_Lazer_Blind",	rate = 5,		loop = 1 },
}


g_Lua_Near3 = 
{ 
	{ action_name = "Stand5",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left_Blind",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right_Blind",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front_Blind",	rate = 20,		loop = 2 },
	{ action_name = "Walk_Back_Blind",	rate = 5,		loop = 1 },
	{ action_name = "Move_Front_Blind",	rate = 15,		loop = 2 },
	{ action_name = "Attack3_Slash_Blind",	rate = 8,		loop = 1 },
	{ action_name = "Attack5_Casting2_Blind",	rate = 15,		loop = 1 },
	{ action_name = "Attack4_Lazer_Blind",	rate = 15,		loop = 1 },
}


g_Lua_Near4 = 
{ 
	{ action_name = "Stand5",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left_Blind",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right_Blind",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front_Blind",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back_Blind",	rate = 1,		loop = 1},
	{ action_name = "Move_Front_Blind",	rate = 25,		loop = 2 },
	{ action_name = "Attack3_Slash_Blind",	rate = 18,		loop = 1 },
	{ action_name = "Attack4_Lazer_Blind",	rate = 12,		loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand5",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Left_Blind",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Right_Blind",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Front_Blind",	rate = 10,		loop = 2 },
	{ action_name = "Move_Front_Blind",	rate = 30,		loop = 2 },
}