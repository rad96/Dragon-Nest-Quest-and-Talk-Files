
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i�� _back�� ��Ʈ���ϳ����� ���ܽ�Ų��. �ɸ��Ϳ� �������� �ڷ� �̵����� ���ϰ�..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|!Air", "Down" },
 State2 = {"!Air", "!Down" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"!Down|Hit","Air|Down"}, 
 State5 = {"!Move","Air"},
}

g_Lua_CustomAction = {
-- �뽬����
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- ��������������
  CustomAction2 = {
     { "Tumble_Left" },
  },
-- ��������������
  CustomAction3 = {
     { "Tumble_Right" },
  },
-- �⺻ ���� 2��Ÿ 145������
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- �⺻ ���� 3��Ÿ 232������
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- ��Ÿ ����
  CustomAction6 = {
     { "useskill", lua_skill_index = 17, rate = 100 },
  },
  -- �������� ���� �ļ�Ÿ
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 10, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 10000

g_Lua_Near1 = 
{ 
     { action_name = "Attack_Down", rate = -1, loop = 1, cooltime = 23000, target_condition = "State1" },
	 { action_name = "Move_Left", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Right", rate = 5, loop = 1, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4"},
	 { action_name = "Tumble_Back", rate = 15, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Left", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Right", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 --{ action_name = "CustomAction6", rate = 100, loop = 1, target_condition = "State5" },
     --{ action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 2000, target_condition = "State1" },
--1Ÿ
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2Ÿ
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3Ÿ
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4Ÿ
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
{ action_name = "Tumble_Front", rate = 5, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4"},
	 { action_name = "Tumble_Back", rate = 15, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Left", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Right", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}
g_Lua_Near3 = 
{ 
     { action_name = "Move_Left", rate = 5, loop = 3 },
     { action_name = "Move_Right", rate = 5, loop = 3 },
    { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4"},
	 { action_name = "Tumble_Back", rate = 15, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Left", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Right", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
     --{ action_name = "Assault",  rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
	 { action_name = "Move_Left", rate = 10, loop = 3 },
     { action_name = "Move_Right", rate = 10, loop = 3 },
	 { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4"},
	 { action_name = "Tumble_Back", rate = 15, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Left", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Right", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
     --{ action_name = "Assault",  rate = 5,  loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
	 { action_name = "Move_Left", rate = 10, loop = 3 },
     { action_name = "Move_Right", rate = 10, loop = 3 },
	 { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4"},
	 { action_name = "Tumble_Back", rate = 15, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Left", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Right", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
}

g_Lua_Assault = { 
     --{ action_name = "", rate = 30, loop = 1, approach = 300 },
     { action_name = "", rate = 10, loop = 1, approach = 150 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Tumble_Front", rate = 60, loop = 1, cooltime = 2600, globalcooltime=1 },
     --{ action_name = "Move_Back", rate = 10, loop = 1 },
     { action_name = "CustomAction2", rate = 10, loop = 1 , globalcooltime=1 },
     { action_name = "CustomAction3", rate = 10, loop = 1 , globalcooltime=1},
}

g_Lua_NonDownRangeDamage = {
     --{ action_name = "Assault", rate = 15, loop = 1 },
	 { action_name = "useskill", lua_skill_index = 7, rate = 50 },
	 { action_name = "useskill", lua_skill_index = 16, rate = 50 },
	 { action_name = "Tumble_Left", rate = 10, cooltime = 2600, globalcooltime=1 },
     { action_name = "Tumble_Right", rate = 10, cooltime = 2600, globalcooltime=1},
}

g_Lua_RandomSkill ={
{
{ -- Ʈ���̾ޱۼ� ���� �׽�Ʈ
   { skill_index = 34211, rate= 20 }, 
   { skill_index = 34212, rate= 20 },
   { skill_index = 34213, rate= 15 },
   { skill_index = 34214, rate= 15 },
   { skill_index = 34215, rate= 15 },
   { skill_index = 34216, rate= 15 },
},
}
}

g_Lua_Skill = { 

-- 0����_��
     { skill_index = 82501, cooltime = 15000, rate = 30, rangemin = 0, rangemax = 1200, target = 3, combo1 = "50,50,1", combo2 = "51,50,1" , combo3 = "14,100,0" },
-- 1����_��
     { skill_index = 82502, cooltime = 15000, rate = 10, rangemin = 0, rangemax = 1200, target = 3, combo1 = "51,50,1", combo2 = "50,50,1", combo3 = "14,100,0" },
-- 2������Ÿ
     { skill_index = 82503, cooltime = 0, rate = -1, rangemin = 0, rangemax = 600, target = 3, combo1 = "8,50,0", combo2 = "14,100,0" },
-- 3Ʈ�� ��
     { skill_index = 82504, cooltime = 4500, rate = 30, rangemin = 200, rangemax = 1000, target = 3, combo1 = "16,50,1" , combo2 = "12,70,0", combo3 = "11,100,1" }, 
-- 4��Ƽ �� 
	 { skill_index = 82505, cooltime = 28000, rate = 50, rangemin = 0, rangemax = 200, target = 3, combo1 = "27,50,1", combo2 = " 38,100,1" },
-- 5���ο� ����ű
     { skill_index = 82506, cooltime = 12000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "44,60,1", combo2 = "27,80,1", combo3 = "46,100,1" },
-- 6���� ű
     { skill_index = 82510, cooltime = 0, rate = 100, rangemin = 0, rangemax = 130, target = 3, cancellook = 1, combo1 = "5,50,1", combo2 = "13,70,1", combo3 = "47,100,0" , target_condition = "State5	"}, 
-- 7������_��
     { skill_index = 82508, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 1000, target = 3, combo1 ="25,30,0",combo2 = "18,90, 0", combo3 ="16,100,0", globalcooltime=1  },
-- 8����Ʈ ű
     { skill_index = 82509, cooltime = 6000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 =" 4,60,1", combo2 = "5,100,1"  },
-- 9�ο� ����ű
     { skill_index = 82512, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 100, target = 3, combo1 = "5,50,1", combo2 = "20,60,1", combo3 = "46,100,1",  target_condition = "State1" },
-- 10���� ����ű
     { skill_index = 82518, cooltime = 0, rate = -1, rangemin = 0, rangemax = 600, target = 3, combo1 = "4,60,1" },
	 
-- 11�Ǿ�� ��
     { skill_index = 82516, cooltime = 10000, rate = 5, rangemin = 300, rangemax = 1000, target = 3, combo1 = "12,0,1", combo2 = "13,100,1", combo3 = "7,100,0" },
-- 12���� �ַο�
     { skill_index = 82517, cooltime = 13000, rate = 20, rangemin = 0, rangemax = 1000, target = 3, combo1 = "3,100,0", target_condition = "State3"  },
-- 13���� ��
     { skill_index = 96087, cooltime = 15000, rate = 30, rangemin = 0, rangemax = 1000, target = 3, combo1 = "5,50,1", combo2 = "27,100,1", target_condition = "State3"},	 	 
-- 14����� ü�� ��
     { skill_index = 82521, cooltime = 20000, rate = -1, rangemin = 0, rangemax = 1500, cancellook = 1, target = 3 },
-- 15���̵�� �� EX
     { skill_index = 82543, cooltime = 24000, rate = 50, rangemin = 0, rangemax = 1500, target = 3 , combo1 = "13,50,0", combo2 = "26,100,0", priority= 3, selfhppercent=99},
-- 16����ũ ��
     { skill_index = 82524, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "3,50,0", combo2 = "15,50,0" , combo3 = "18,100,1" },
-- 17���ʹ� ��
     { skill_index = 82525, cooltime = 22000, rate = 40, rangemin = 0, rangemax = 1200, target = 3, cancellook = 1, combo1 = "23,50,1",combo2 = "27,80,1", combo3= "13,100,0", target_condition = "State2" },
-- 18�ַο� ����
     { skill_index = 82526, cooltime = 45000, rate = 30, rangemin = 700, rangemax = 1000, target = 3, cancellook = 1 ,  priority= 3 },--blowcheck = "70"
-- 19�ͽ��ټ� �ַο� EX
     { skill_index = 82538, cooltime = 15000, rate = 20, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = " 17,60,0", combo2 = "12,100,0" },
-- 20������Ʈ �� EX
     { skill_index = 82528, cooltime = 18000, rate = -1, rangemin = 0, rangemax = 200, target = 3, cancellook = 1, combo1 = "5,60,1",combo2= "48,80,1", combo3 = " 21,100,1", resetcombo =1 },	 
-- 21������Ʈ �� EX combo
     { skill_index = 82542, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 =" 38,50,1", combo2 = "3,100,0", resetcombo =1},
-- 22 ���ǵ� �� EX
     { skill_index = 82540, cooltime = 15000, rate = 20, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "17,50,1", combo2 = "24,100,0"  },
-- 23 ������ ���̵�
     { skill_index = 82532, cooltime = 60000, rate = -1, rangemin = 300, rangemax = 1500, target = 3, cancellook = 1, combo1 = "7,50,0", combo2 = "26,100,0" },
-- 24 �ҽ�����
     { skill_index = 82533, cooltime = 20000, rate = 20, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "3,80,0" , combo2 = "12,100,0", target_condition = "State3" },
-- 25 Ʈ���� �ޱ� ��
     { skill_index = 82548, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "14,100,0" , globalcooltime=2},
-- 26 ����Ľ�
     { skill_index = 82535, cooltime = 50000, rate = -1, rangemin = 200, rangemax = 1200, target = 3, cancellook = 1, combo1 = "31,100,0", priority= 2},
-- 27 ��Ŭ ��
     { skill_index = 82536, cooltime = 18000, rate = 20, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "7,50,0",combo3 = "16,70,0",  combo2 = "26,100,1"},
-- 28 ������ �긮��
     { skill_index = 82537, cooltime =120000, rate = 100, rangemin = 0, rangemax = 900, target = 3 },
-- 29 ��������� �ַο�
     { skill_index = 82539, cooltime = 38000, rate = 30, rangemin = 50, rangemax = 1200, target = 3, cancellook = 1, combo1 = "3,50,0", combo2 = "17,100,0" },
-- 30 ����Ľ�����
     { skill_index = 82545, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3, cancellook = 1, combo1 = "31,100,0" , usedskill = "82535"},
-- 31 ����Ľ�����1
     { skill_index = 82546, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3, cancellook = 1, combo1 = "30,100,0", usedskill = "82535" },
-- 32 Ʈ���̾ޱۼ�R
     { skill_index = 82534, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "5,50,0", combo2 = "3,100,0" , globalcooltime=2},-- randomskill�ñ׳η� ������ ��ų�� Ȯ������ -1�� �����ؾߵ�
-- 33 Ʈ���̾ޱۼ�L
     { skill_index = 82547, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "5,50,0", combo2 = "3,100,0", globalcooltime=2 },
-- 34������R
     { skill_index = 82552, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 400, target = 3  },
-- 35������L
     { skill_index = 82553, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 400, target = 3  },
-- 36 ���Ӽ�Ʈ ű
     { skill_index = 82511, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 500, target = 3, combo1 = "45,100,0"   },--8000
-- 37 �����ũ
     { skill_index = 96081, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 250, target = 3  , combo1 = "8,50,1", combo2 = "39,100,0"  },--8000
-- 38 ���ε� �� EX 
     { skill_index = 96082, cooltime = 13000, rate = 30, rangemin = 0, rangemax = 600, target = 3 , combo1 ="46,60,1", combo1 = "36,100,1"  },--13000
-- 39 ����� ű
     { skill_index = 96083, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 250, target = 3  , combo1 = "48,70,1", combo2 = "44,90,1", combo3 = " 53,100,1", combo4 = " 16,100,0"},--15000
-- 40 ĵ�� ��Ŭ ��
     { skill_index = 96084, cooltime = 45000, rate = -1, rangemin = 0, rangemax = 400, target = 3  },--45000
-- 41 ü�� ������ F
     { skill_index = 96085, cooltime = 8857, rate = 30, rangemin = 200, rangemax = 1200, target = 3, combo1 = " 50,50,1", combo2 = "51,50,1" , combo3 = "39,100,0", globalcooltime=2 },--8857
-- 42 ü�� ������ B
     { skill_index = 96086, cooltime = 8857, rate = 20, rangemin = 0, rangemax = 1200, target = 3 , combo1 = " 50,50,1", combo2 = "51,50,1" , combo3 = "39,100,0", globalcooltime=2 },--8857
-- 43 ���� �� EX
     { skill_index = 96087, cooltime = 15000, rate = -1, rangemin = 0, rangemax = 400, target = 3  },--15000
-- 44 ����Ŭ�� ű EX
     { skill_index = 96088, cooltime = 23000, rate = 30, rangemin = 0, rangemax = 600, target = 3 , combo1 = " 38,50,1",combo2 = "53,70,1", combo3 = " 51,80,1", combo4 = "46,100,0" },--23000
-- 45 ���� ���� ��Ʈ ű EX
     { skill_index = 96089, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 = "37,100,1"  },--10000
-- 46 �㸮���� ����
     { skill_index = 96090, cooltime = 37000, rate = 50, rangemin = 0, rangemax = 250, target = 3 , combo1 = "36,100,1" ,target_condition = "State4"},--37000
-- 47 ű �� �� EX
     { skill_index = 96091, cooltime = 9000, rate = 40, rangemin = 0, rangemax = 500, target = 3 ,combo1 ="39,60,1", combo2 = "46,70,1", combo3 = " 44,100,1" },--9000
-- 48 �����̷� ���ؽ� EX
     { skill_index = 96092, cooltime = 23000, rate = 30, rangemin = 0, rangemax = 400, target = 3 , combo1 =" 13, 60,1", combo2 = "49,100,1" , target_condition = "State3", resetcombo =1 },--23000
-- 49 �����̷� ���ؽ� EX A
     { skill_index = 96093, cooltime = 23000, rate = -1, rangemin = 0, rangemax = 400, target = 3 , combo1 = "47,60,1", combo2 = " 26,70,1", combo3 ="44,100,1", resetcombo =1 },--23000
-- 50 ž ���Ǵ� F
     { skill_index = 96094, cooltime = 6000, rate = -1, rangemin = 0, rangemax = 600, target = 3 , combo1 = "52,50,1", combo2 = " 37,70,0", combo3 = "14,100,1" , globalcooltime=1 },--6000
-- 51 ž ���Ǵ� B
     { skill_index = 96095, cooltime = 6000, rate = -1, rangemin = 0, rangemax = 600, target = 3 , combo1 = "52,50,1", combo2 = " 37,70,0", combo3 = "14,100,1" , globalcooltime=1 },--6000
-- 52 ���Ǹ� ���̾
     { skill_index = 96096, cooltime = 7000, rate = 20, rangemin = 0, rangemax = 900, target = 3 , combo1 = "39,50,0", combo2 = "47,100,0" },--7000
-- 53 �����̷� ����
     { skill_index = 96097, cooltime = 900000, rate = 50, rangemin = 0, rangemax = 400, target = 3 , selfhppercent=60 },--900000

	 
	 }