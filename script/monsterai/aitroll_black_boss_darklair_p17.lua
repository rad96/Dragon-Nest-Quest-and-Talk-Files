--AiTroll_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Move_Left_NotTarget", rate = 4, loop = 2 , max_missradian = 360   },
   { action_name = "Move_Right_NotTarget", rate = 4, loop = 2  , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 10, loop = 2   , max_missradian = 360},
   { action_name = "Attack5_Rolling_Air_NotTarget", rate = 15, loop = 1  , max_missradian = 360 },
}
g_Lua_Near2 = { 
  { action_name = "Move_Left_NotTarget", rate = 5, loop = 2   , max_missradian = 360},
   { action_name = "Move_Right_NotTarget", rate = 5, loop = 2 , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 3, loop = 2  },
   { action_name = "Attack5_Rolling_Air_NotTarget", rate = 3, loop = 1  , max_missradian = 360 },
   { action_name = "Attack5_Rolling_Air_NotTarget", rate = 15, loop = 1  , max_missradian = 360 },
}
g_Lua_Near3 = { 
   { action_name = "Move_Left_NotTarget", rate = 7, loop = 3  , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 7, loop = 3  , max_missradian = 360 },
   { action_name = "Move_Front_NotTarget", rate = 5, loop = 3  , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 3, loop = 3   , max_missradian = 360},
   { action_name = "Attack5_Rolling_Air_NotTarget", rate = 15, loop = 1   , max_missradian = 360},
}
g_Lua_Near4 = { 
   { action_name = "Move_Left_NotTarget", rate = 5, loop = 3  , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 5, loop = 3 , max_missradian = 360  },
   { action_name = "Move_Front_NotTarget", rate = 5, loop = 3  , max_missradian = 360 },
   { action_name = "Move_Back_NotTarget", rate = 1, loop = 3  , max_missradian = 360 },
   { action_name = "Attack5_Rolling_Air_NotTarget", rate = 15, loop = 1  , max_missradian = 360 },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack5_Rolling_Air_NotTarget", rate = 16, loop = 1  , max_missradian = 360 },
   { action_name = "Move_Left_NotTarget", rate = 1, loop = 2  , max_missradian = 360 },
   { action_name = "Move_Right_NotTarget", rate = 1, loop = 2  , max_missradian = 360 },
   { action_name = "Move_Front_NotTarget", rate = 10, loop = 2  , max_missradian = 360 },
}
