--AiSpider_Red_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 350;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1150;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 350
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Attack3_DashBite", rate = 3, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = -1, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Assault", rate = 3, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
   { action_name = "Assault", rate = 2, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 5 },
   { action_name = "Assault", rate = 3, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack3_DashBite", rate = 5, loop = 1, approach = 350 },
   { action_name = "Stand", rate = 5, loop = 1, approach = 350 },
}
g_Lua_Skill = { 
   { skill_index = 35270,  cooltime = 18000, rate = -1,rangemin = 350, rangemax = 650, target = 3, max_missradian = 10 },
}
