--DarkLairDefence_Lv23_HarpyRed.lua
--타임아웃 모드, 하피 화이트보다 개체수가 적게 나온다.
--태풍 공격보다는, 디버프, 호밍 공격 등으로 방해를 하는 형태로 간다.
--일정 시간이 지나면 공격 빈도가 높아진다.
--일정 시간이 지나면 아군 전체에 이동속도 증가 스킬을 사용한다.
--20212 : 화염기둥, 20215 : 자신의 크리티컬 상승, 20213 : 유혹, 20214 : 수면

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 30, loop = 3  },
   { action_name = "Move_Back", rate = 25, loop = 3  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Attack1_Throw_R", rate = 13, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 25, loop = 3  },
   { action_name = "Move_Back", rate = 15, loop = 3  },
   { action_name = "Move_Front", rate = 2, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 12, loop = 3  },
   { action_name = "Walk_Right", rate = 12, loop = 3  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 2, loop = 2  },
   { action_name = "Attack1_Throw_R", rate = 17, loop = 1  },
   { action_name = "Attack3_FallDown", rate = 2, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 30, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
   { action_name = "Walk_Left", rate = 15, loop = 3  },
   { action_name = "Walk_Right", rate = 15, loop = 3  },
   { action_name = "Move_Left", rate = 2, loop = 2  },
   { action_name = "Move_Right", rate = 2, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
   { action_name = "Walk_Left", rate = 15, loop = 3  },
   { action_name = "Walk_Right", rate = 15, loop = 3  },
   { action_name = "Move_Left", rate = 2, loop = 2  },
   { action_name = "Move_Right", rate = 2, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Walk_Back", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 3  },
   { action_name = "Walk_Right", rate = 10, loop = 3  },
   { action_name = "Attack1_Throw_R", rate = 45, loop = 1  },
}
g_Lua_Skill = { 
--유혹, 스킬을 못 쓰게 만든다
   { skill_index = 20214,  cooltime = 18000, rate = 80,rangemin = 0, rangemax = 1000, target = 3 },
--크리티컬 상승 버프(스킬 새로 만들어서 스킬 인덱스 바꿔야함) 시작후 100초 지나고 사용
   { skill_index = 20215,  cooltime = 18000, rate = 80,rangemin = 0, rangemax = 1000, target = 3, encountertime = 100000 },
}
