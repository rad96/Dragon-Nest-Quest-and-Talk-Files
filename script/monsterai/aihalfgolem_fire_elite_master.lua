--AiHalfGolem_Fire_Elite_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1400;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 2 },
   { action_name = "Walk_Right", rate = 4, loop = 2 },
   { action_name = "Attack2_Blow", rate = 24, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
   { action_name = "Assault", rate = 5, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 4 },
   { action_name = "Assault", rate = 5, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Blow", rate = 8, loop = 1, approach = 250 },
   { action_name = "Stand", rate = 4, loop = 1, approach = 250 },
   { action_name = "Stand_1", rate = 2, loop = 1, approach = 250 },
}
g_Lua_Skill = { 
   { skill_index = 20764,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 3000, target = 3 },
   { skill_index = 20763,  cooltime = 30000, rate = 60, rangemin = 0, rangemax = 1100, target = 3, next_lua_skill_index = 0 },
   { skill_index = 20761,  cooltime = 25000, rate = 70, rangemin = 0, rangemax = 1100, target = 3 },
   { skill_index = 20765,  cooltime = 10000, rate = 80, rangemin = 600, rangemax = 1100, target = 3 },
   { skill_index = 20762,  cooltime = 25000, rate = 60, rangemin = 0, rangemax = 1000, target = 3 },
}
