--GuildWar_BigDoor_Boss

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
}

g_Lua_Near3 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 33001, cooltime = 6000, rate = 100, rangemin = 0, rangemax = 1500, target = 1, limitcount = 1 },
   { skill_index = 33014, cooltime = 6000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, encountertime = 30000 },
}