-- Celberos Normal
g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 700.0;
g_Lua_NearValue3 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 320;
g_Lua_AssualtTime = 3000;


g_Lua_CustomAction =
{
	CustomAction1 = 
	{
      			{ "Attack_Down_Left", 0},
			{ "Attack_Down_Right", 0},	 		
	},
	CustomAction2 = 
	{
      			{ "Attack_Down_Right", 0},
			{ "Attack_Down_Left", 0},	 		
	},
}
g_Lua_Near1 = 
{ 
	{ action_name = "Stand_2",	rate = 2,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Back",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Attack_Cry",	rate = 10,		loop = 1, td = "LF,FL,FR,RF,RB,BR,BL,LB", cooltime = 30000, selfhppercent = 100 },
	{ action_name = "Attack_Punch",	rate = 15,		loop = 1, td = "LF,FL,FR,RF", selfhppercent = 99 },
	{ action_name = "CustomAction1",	rate = 20,	loop = 1, td = "FL,FR", selfhppercent = 99 },
	{ action_name = "CustomAction2",	rate = 20,	loop = 1, td = "FL,FR", selfhppercent = 99 },
	{ action_name = "Turn_Left",	rate = 5,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 5,		loop = -1, td = "RF,RB,BR" },
	{ action_name = "Attack_Back_Left",	rate = 15,		loop = 1, td = "LF,LB,BL" },
	{ action_name = "Attack_Back_Right",	rate = 15,		loop = 1, td = "RF,RB,BR" },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_2",	rate = 2,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Front",	rate = 2,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "Attack_Cry",	rate = 5,		loop = 1, td = "LF,FL,FR,RF,RB,BR,BL,LB", cooltime = 30000, selfhppercent = 100 },
	{ action_name = "Attack3_FireWall_DarkLair40",	rate = 50,		loop = 1, cooltime = 20000, selfhppercent = 60, existparts="6" },
	{ action_name = "Attack_Punch",	rate = 10,		loop = 1, td = "LF,FL,FR,RF", selfhppercent = 99 },
	{ action_name = "Attack_Bite",	rate = 20,		loop = 1, td = "LF,FL,FR,RF", selfhppercent = 40, existparts="4,5,6" },
	{ action_name = "CustomAction1",	rate = 10,	loop = 1, td = "FL,FR", selfhppercent = 99 },
	{ action_name = "CustomAction2",	rate = 10,	loop = 1, td = "FL,FR", selfhppercent = 99 },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_2",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Front",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
	{ action_name = "Attack_Jump",	rate = 20,		loop = 1, td = "FL,FR" },
}

g_Lua_Skill=
{
	-- ȭ���� ����
	{ skill_index = 30108, SP = 0, cooltime = 15000, rangemin = 300, rangemax = 2000, target = 3, rate = 50, td = "FL,FR", existparts="6", selfhppercent = 80 },
	-- ����¡ ��
	{ skill_index = 30104, SP = 0, cooltime = 15000, rangemin = 500, rangemax = 2000, target = 3, rate = 50, td = "LF,FL,FR,RF", existparts="4", multipletarget = 1, selfhppercent = 80 },
	-- ��ũ����Ʈ��
	{ skill_index = 30110, SP = 0, cooltime = 15000, rangemin = 100, rangemax = 2000, target = 3, rate = 50, td = "FL,FR", existparts="5", selfhppercent = 80 },
}

