--AiKoboldWarrior_Green_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 50;
g_Lua_NearValue2 = 150;
g_Lua_NearValue3 = 200;
g_Lua_NearValue4 = 300;
g_Lua_NearValue5 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 2  },
   { action_name = "Move_Right", rate = 8, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Move_Back", rate = 2, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 2  },
   { action_name = "Move_Right", rate = 8, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 1, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 20030,  cooltime = 4000, rate = 100,rangemin = 0, rangemax = 10000, target = 3, selfhppercent = 100 },
}
