--AiSpittler_Purple_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;
g_Lua_NearValue6 = 5500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_3", rate = 20, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Attack2_Bite", rate = 19, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Stand_On", rate = 10, loop = 1, custom_state1 = "custom_underground" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_3", rate = 10, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Stand_On", rate = 10, loop = 1, custom_state1 = "custom_underground" },
}
g_Lua_Near3 = { 
   { action_name = "Stand_3", rate = 10, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Stand_On", rate = 10, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Attack5_Teleport", rate = 10, loop = 1, custom_state1 = "custom_ground" },
}
g_Lua_Near4 = { 
   { action_name = "Stand_3", rate = 10, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Stand_On", rate = 10, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Attack5_Teleport", rate = 15, loop = 1, custom_state1 = "custom_ground" },
}
g_Lua_Near5 = { 
   { action_name = "Stand_3", rate = 10, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Stand_On", rate = 10, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Attack5_Teleport", rate = 15, loop = 1, custom_state1 = "custom_ground" },
}
g_Lua_Near6 = { 
   { action_name = "Stand_3", rate = 10, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Attack5_Teleport", rate = 35, loop = 4, custom_state1 = "custom_ground" },
}

g_Lua_Skill = { 
   { skill_index = 20249,  cooltime = 17000, rate = -1,rangemin = 0, rangemax = 600, target = 4, selfhppercent = 75, custom_state1 = "custom_ground" },
}
