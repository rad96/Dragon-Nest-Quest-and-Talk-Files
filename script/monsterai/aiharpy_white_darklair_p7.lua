--AiHarpy_White_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Attack3_FallDown_DarkLair", rate = 20, loop = 1 , max_missradian = 360 },
   { action_name = "Attack4_Tornado_DarkLair", rate = 20, loop = 1 , max_missradian = 360 },
}
g_Lua_Near2 = { 
   { action_name = "Attack3_FallDown_DarkLair", rate = 15, loop = 1 , max_missradian = 360 },
   { action_name = "Attack4_Tornado_DarkLair", rate = 15, loop = 1 , max_missradian = 360 },
}
g_Lua_Near3 = { 
   { action_name = "Attack3_FallDown_DarkLair", rate = 15, loop = 1 , max_missradian = 360 },
   { action_name = "Attack4_Tornado_DarkLair", rate = 15, loop = 1 , max_missradian = 360 },
}
