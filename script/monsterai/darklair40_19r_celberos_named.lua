-- Celberos Normal
g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 400.0;
g_Lua_NearValue2 = 800.0;
g_Lua_NearValue3 = 2000.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 320;
g_Lua_AssualtTime = 3000;

g_Lua_Near1 = 
{ 
	{ action_name = "Stand_2",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Attack_Punch",	rate = 30,		loop = 1, td = "LF,FL,FR,RF" },
	{ action_name = "Walk_Front",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 5,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 5,		loop = -1, td = "RF,RB,BR" },
	{ action_name = "Attack_Back_Left",	rate = 20,		loop = 1, td = "LF,LB,BL" },
	{ action_name = "Attack_Back_Right",	rate = 20,		loop = 1, td = "RF,RB,BR" },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_2",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Front",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 3,		loop = 1, td = "FL,FR" },
	{ action_name = "Attack_Punch",	rate = 25,		loop = 1, td = "LF,FL,FR,RF" },
	{ action_name = "Turn_Left",	rate = 7,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 7,		loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_2",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Front",	rate = 10,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right",	rate = 1,		loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left",	rate = 15,		loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right",	rate = 15,		loop = -1, td = "RF,RB,BR" },
}



