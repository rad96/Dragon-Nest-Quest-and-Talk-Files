--GreenDragonNest_FinalBoss_GreenDragon_1Phase.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 30000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Near1 = { 
   { action_name = "Stand_Cave_1", rate = 10, loop = 1, selfhppercent = 100  },
   { action_name = "Attack01_LeftArmAttack", rate = 20, loop = 1, selfhppercentrange = "0,100", encountertime = 5000  },
   { action_name = "Attack02_RightArmAttack", rate = 20, loop = 1, selfhppercentrange = "0,100", encountertime = 5000  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_Cave_1", rate = 10, loop = 1, selfhppercent = 100  },
   { action_name = "Attack01_LeftArmAttack", rate = 20, loop = 1, selfhppercentrange = "0,100", encountertime = 5000  },
   { action_name = "Attack02_RightArmAttack", rate = 20, loop = 1, selfhppercentrange = "0,100", encountertime = 5000  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_Cave_1", rate = 10, loop = 1, selfhppercent = 100  },
   { action_name = "Attack01_LeftArmAttack", rate = 20, loop = 1, selfhppercentrange = "0,100", encountertime = 5000  },
   { action_name = "Attack02_RightArmAttack", rate = 20, loop = 1, selfhppercentrange = "0,100", encountertime = 5000  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_Cave_1", rate = 10, loop = 1, selfhppercent = 100  },
   { action_name = "Attack01_LeftArmAttack", rate = 20, loop = 1, selfhppercentrange = "0,100", encountertime = 5000  },
   { action_name = "Attack02_RightArmAttack", rate = 20, loop = 1, selfhppercentrange = "0,100", encountertime = 5000  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_Cave_1", rate = 10, loop = 1, selfhppercent = 100  },
   { action_name = "Attack01_LeftArmAttack", rate = 20, loop = 1, selfhppercentrange = "0,100", encountertime = 5000  },
   { action_name = "Attack02_RightArmAttack", rate = 20, loop = 1, selfhppercentrange = "0,100", encountertime = 5000  },
}

g_Lua_Skill = {
-- 파이어 웨이브
   { skill_index = 31501,  cooltime = 50000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "50,95"},
-- 아이스 웨이브
   { skill_index = 31502,  cooltime = 50000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "50,95"},
 
-- 외치기(연출용)
   { skill_index = 31505,  cooltime = 50000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "50,80"},

-- 포이즌 브레스_파이어웨이브
   { skill_index = 31503,  cooltime = 50000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "50,95", next_lua_skill_index = 0, globalcooltime = 1 },
-- 포이즌 브레스_아이스웨이브
   { skill_index = 31503,  cooltime = 30000, 	rate = -1, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "50,95", next_lua_skill_index = 1, globalcooltime = 1 },

-- 외치기
   { skill_index = 31504,  cooltime = 50000, 	rate = 100, 	rangemin = 0, rangemax = 40000,target = 3, selfhppercentrange = "50,95"},
}