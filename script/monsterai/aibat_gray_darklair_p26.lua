--AiBat_Gray_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1300;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Attack1", rate = 45, loop = 2  },
   { action_name = "Walk_Back", rate = 15, loop = 3  },
   { action_name = "Move_Back", rate = 15, loop = 3  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 3  },
   { action_name = "Walk_Right", rate = 10, loop = 3  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 3  },
   { action_name = "Move_Right", rate = 20, loop = 3  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 25, loop = 2  },
   { action_name = "Attack2_Dash", rate = 90, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 20, loop = 3  },
   { action_name = "Walk_Right", rate = 20, loop = 3  },
   { action_name = "Walk_Front", rate = 20, loop = 3  },
   { action_name = "Move_Left", rate = 20, loop = 3  },
   { action_name = "Move_Right", rate = 20, loop = 3  },
   { action_name = "Move_Front", rate = 20, loop = 3  },
   { action_name = "Move_Back", rate = 20, loop = 2  },
   { action_name = "Attack2_Dash", rate = 90, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 3  },
   { action_name = "Move_Right", rate = 20, loop = 3  },
   { action_name = "Move_Front", rate = 50, loop = 3  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 1, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1", rate = 5, loop = 1, cancellook = 0, approach = 50.0  },
}
