--AiPlayerMonster_BowMaster_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 1500;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500;
g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssaultTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Shoot_Stand_BigBow", 0 },
      { "Shoot_Stand_BigBow", 0 },
      { "Shoot_Finish_BigBow", 0 },
  },
  CustomAction2 = {
      { "Move_Right", 0 },
      { "Shoot_Stand_BigBow", 0 },
  },
  CustomAction3 = {
      { "Move_Left", 0 },
      { "Shoot_Stand_BigBow", 0 },
  },
  CustomAction4 = {
      { "Move_Front", 0 },
      { "Shoot_Stand_BigBow", 0 },
  },
  CustomAction5 = {
      { "Move_Back", 0 },
      { "Shoot_Stand_BigBow", 0 },
  },
  CustomAction6 = {
      { "Tumble_Back", 0 },
      { "Shoot_Stand_BigBow", 0 },
  },
}

g_Lua_GlobalCoolTime1 = 8000
g_Lua_GlobalCoolTime2 = 13000

g_Lua_Near1 = { 
   { action_name = "Attack_Down", rate = 21, loop = 1, cooltime = 23000, target_condition = "State1" },
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "Move_Back", rate = 10, loop = 2 },
   { action_name = "Shoot_Stand_BigBow", rate = 3, loop = 1, cooltime = 7000 },
   { action_name = "Attack_SideKick", rate = 8, loop = 1, cooltime = 30000, globalcooltime = 1 },
   { action_name = "CustomAction5", rate = 7, loop = 1, cooltime = 30000, globalcooltime = 1 },
   { action_name = "CustomAction6", rate = 7, loop = 1, cooltime = 30000, globalcooltime = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "Move_Back", rate = 1, loop = 1 },
   { action_name = "Shoot_Stand_BigBow", rate = 3, loop = 1, cooltime = 7000 },
   { action_name = "CustomAction1", rate = 3, loop = 1, cooltime = 12000, globalcooltime = 1 },
   { action_name = "CustomAction2", rate = 3, loop = 1, cooltime = 12000, globalcooltime = 1 },
   { action_name = "CustomAction3", rate = 3, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "CustomAction1", rate = 3, loop = 1, cooltime = 12000, globalcooltime = 1 },
   { action_name = "CustomAction2", rate = 3, loop = 1, cooltime = 12000, globalcooltime = 1 },
   { action_name = "CustomAction3", rate = 3, loop = 1, cooltime = 12000, globalcooltime = 1 },
   { action_name = "CustomAction4", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 2 },
   { action_name = "CustomAction1", rate = 3, loop = 1, cooltime = 12000, globalcooltime = 1 },
   { action_name = "CustomAction4", rate = 5, loop = 1, cooltime = 12000, globalcooltime = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 20, loop = 4 },
}

g_Lua_NonDownMeleeDamage = {
      { action_name = "CustomAction4", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "CustomAction5", rate = 3, loop = 1, globalcooltime = 1 },
      { action_name = "Move_Left", rate = 10, loop = 1 },
      { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_NonDownRangeDamage = {
      { action_name = "Stand", rate = 2, loop = 1 },
      { action_name = "Shoot_Stand_BigBow", rate = 5, loop = 1, globalcooltime = 1 },
      { action_name = "CustomAction1", rate = 10, loop = 1, globalcooltime = 1 },
}

g_Lua_Skill = { 
   { skill_index = 31044,  cooltime = 37000, rate = 60,rangemin = 500, rangemax = 1500, target = 3, selfhppercent = 50, globalcooltime = 2 },
   { skill_index = 31045,  cooltime = 16000, rate = 80, rangemin = 0, rangemax = 650, target = 3, selfhppercent = 30, td = "FL,FR,LF,RF,RB,BR,BL,LB", globalcooltime = 2 },
   { skill_index = 31047,  cooltime = 10000, rate = 80, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR", selfhppercent = 70, globalcooltime = 2 },
   { skill_index = 31046,  cooltime = 11000, rate = 40, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR", globalcooltime = 2 },
   { skill_index = 31041,  cooltime = 10000, rate = 90, rangemin = 300, rangemax = 800, target = 3, globalcooltime = 2 },
   { skill_index = 31034,  cooltime = 13000, rate = 20, rangemin = 0, rangemax = 300, target = 3 },
   { skill_index = 31032,  cooltime = 24000, rate = 30, rangemin = 0, rangemax = 500, target = 3 },
   { skill_index = 31033,  cooltime = 10000, rate = 40, rangemin = 0, rangemax = 1100, target = 3 },
}
