-- Minotauros AI

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 350.0;
g_Lua_NearValue2 = 700.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 1300.0;
g_Lua_NearValue5 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 300;
g_Lua_AssualtTime = 3000;

g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
            { action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
            { action_name = "Attack1_Bash",	rate = 30,		loop = 1 },
            { action_name = "Attack3_DashAttack",	rate = 10,		loop = 1, cancellook = 1 },
	{ action_name = "Attack4_Pushed",	rate = 30,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Left",	rate = 2,		loop = 2 },
	{ action_name = "Move_Right",	rate = 2,		loop = 2 },
	{ action_name = "Move_Front",	rate = 15,		loop = 1 },
	{ action_name = "Attack2_Slash",	rate = 40,		loop = 1, cancellook = 1 },
	{ action_name = "Attack3_DashAttack",	rate = 60,		loop = 1, cancellook = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 0,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 20,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 25,		loop = 2 },
	{ action_name = "Assault",	rate = 40,		loop = 1 },
	{ action_name = "Attack3_DashAttack",	rate = 60,		loop = 1, cancellook = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 1,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	{ action_name = "Move_Front",	rate = 20,		loop = 2 },
            { action_name = "Assault",	rate = 40,		loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Walk_Front",	rate = 1,		loop = 2 },
	{ action_name = "Move_Front",	rate = 1,		loop = 2 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Bash",	rate = 5,	loop = 1, cancellook = 0, approach = 250.0 },
	{ action_name = "Attack3_DashAttack",	rate = 5,	loop = 1, cancellook = 1, approach = 500.0 },
}