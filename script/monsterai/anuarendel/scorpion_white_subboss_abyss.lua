--AiSpittler_Green_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 4500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

-- (1)돌진,스톰프,회전 (2)망치 (3)망치->돌진,스톰프,회전 (4)돌진(광폭) (5)돌진 (6)회전 (7) 슬래쉬,콤보,대포
g_Lua_GlobalCoolTime1 = 20000
g_Lua_GlobalCoolTime2 = 59000
g_Lua_GlobalCoolTime3 = 25000
g_Lua_GlobalCoolTime4 = 50000
g_Lua_GlobalCoolTime5 = 40000
g_Lua_GlobalCoolTime6 = 20000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1, selfhppercentrange = "25,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,24" },
   { action_name = "Walk_Left", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 4, loop = 1, td = "FL,FR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1, selfhppercentrange = "25,100" },
   { action_name = "Stand", rate = 2, loop = 1, selfhppercentrange = "0,24" },
   { action_name = "Walk_Front", rate = 10, loop = 2 , td = "FL,FR"},
   { action_name = "Walk_Left", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Move_Front", rate = 4, loop = 1, td = "FL,FR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2, td = "FL,FR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3, td = "FL,FR" },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 4, td = "FL,FR" },
}

g_Lua_Skill = { 
-- 스톰프(8sec) 
   { skill_index = 33650, cooltime = 50000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "50,75", priority = 10 }, -- 들어갔다가 바로 위로 올라감
   { skill_index = 33651, cooltime = 30000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,50", priority = 10 }, -- 들어간 후 일정 시간(5초 추적)
-- 돌진 10, 20, 30이상(9sec)
   { skill_index = 33652, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 0, rangemax = 1100, target = 3, randomtarget = 1.5, selfhppercentrange = "25,100", priority = 10 }, -- 돌진 10
   { skill_index = 33653, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 1100, rangemax = 2100, target = 3, randomtarget = 1.5, selfhppercentrange = "25,100", priority = 10 }, -- 돌진 20
   { skill_index = 33654, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 2100, rangemax = 3100, target = 3, randomtarget = 1.5, selfhppercentrange = "25,100", priority = 10 }, -- 돌진 30
   { skill_index = 33655, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 0, rangemax = 1100, target = 3, randomtarget = 1.5, selfhppercentrange = "0,24", priority = 10 }, -- 돌진 10 후 화냄
   { skill_index = 33656, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 1100, rangemax = 2100, target = 3, randomtarget = 1.5, selfhppercentrange = "0,24", priority = 10 }, -- 돌진 20 후 화냄
   { skill_index = 33657, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 2100, rangemax = 3100, target = 3, randomtarget = 1.5, selfhppercentrange = "0,24", priority = 10 }, -- 돌진 30 후 화냄
-- 슬래쉬
   { skill_index = 33646, cooltime = 14000, rate = 70, rangemin = 0, rangemax = 500, target = 3, td = "FL,FR", priority = 10, selfhppercentrange = "0,100" }, -- 앞뒤 동시 공격
   { skill_index = 33646, cooltime = 8000, rate = 70, rangemin = 0, rangemax = 500, target = 3, td = "FL,FR", priority = 10, selfhppercentrange = "0,24" },
-- 콤보
   { skill_index = 33649, cooltime = 21000, rate = 60, rangemin = 0, rangemax = 500, target = 3, td = "FL,FR", priority = 10, selfhppercentrange = "25,100" }, -- 정면 콤보 공격
   { skill_index = 33649, cooltime = 15000, rate = 60, rangemin = 0, rangemax = 500, target = 3, td = "FL,FR", priority = 10, selfhppercentrange = "0,24" },
-- 대포, 31979  대포 돌진 10미터용
   { skill_index = 33647, cooltime = 25000, rate = 70, rangemin = 500, rangemax = 1200, target = 3, randomtarget = 1.5, td = "FL,FR", priority = 10, selfhppercentrange = "25,100" }, -- 꼬리 미사일
   { skill_index = 33647, cooltime = 15000, rate = 70, rangemin = 500, rangemax = 1200, target = 3, randomtarget = 1.5, td = "FL,FR", priority = 10, selfhppercentrange = "0,24" },
-- 꼬리치기
   { skill_index = 33648, cooltime = 10000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, td = "BL,BR", randomtarget = 0.6, priority = 10, selfhppercentrange = "25,100" }, -- 후방 꼬리 휘두르기
   { skill_index = 33648, cooltime = 7000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, td = "BL,BR", randomtarget = 0.6, priority = 10, selfhppercentrange = "0,24" },
-- 회전(14sec)
   { skill_index = 33658, cooltime = 60000, rate = 70, rangemin = 0, rangemax = 2000, target = 3, multipletarget = "1,8,0,1", selfhppercentrange = "0,50", priority = 30 }, -- 회전하며 일정 시간 움직임
   { skill_index = 33659, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 2000, target = 3, multipletarget = "1,8,0,1", selfhppercentrange = "50,100", priority = 10 }, -- 회전 하고 바로 종료
}
