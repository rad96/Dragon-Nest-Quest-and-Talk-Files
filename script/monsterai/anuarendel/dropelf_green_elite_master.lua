--AiDropElf_Green_Elite_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 20, loop = 2 },
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 8, loop = 2 },
   { action_name = "Attack02_VineWhip", rate = 20, loop = 1, notusedskill = "33641" },
   { action_name = "Attack04_SnatchLeg", rate = 50, loop = 1, usedskill = "33641" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 15, loop = 1 },
  { action_name = "Walk_Front", rate = 20, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Left", rate = 8, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 2 },
}
g_Lua_BeHitSkill =
 {
  { lua_skill_index = 2, rate = 100, skill_index = 33643 },
 }
g_Lua_Skill = { 
   { skill_index = 33641,  cooltime = 5000, rate = 100,rangemin = 0,  rangemax = 3000,  target = 1, limitcount =1, selfhppercent = 50 },
   { skill_index = 33643,  cooltime = 15000, rate = 60, rangemin = 400,  rangemax = 800,  target = 3, selfhppercent = 90, notusedskill = "33641" },
   { skill_index = 33644,  cooltime = 1000, rate = -1, rangemin = 0,  rangemax = 3000,  target = 3 },
}
