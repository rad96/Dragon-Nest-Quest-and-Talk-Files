--AiGolem_Moss_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1400;
g_Lua_NearValue5 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 350
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_70Ex_Event", rate = 6, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_70Ex_Event", rate = 6, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_70Ex_Event", rate = 5, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_70Ex_Event", rate = 1, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_70Ex_Event", rate = 1, loop = 1 },
}

