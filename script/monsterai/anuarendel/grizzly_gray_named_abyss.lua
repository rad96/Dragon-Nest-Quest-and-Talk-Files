--AiGrizzly_Gray_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 350;
g_Lua_NearValue2 = 650;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1300;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 350
g_Lua_AssaultTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack03_Kick", 0 },
      { "Attack01_RightHand", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop=1,td = "FR,FL" },
   { action_name = "Walk_Back", rate = 6, loop=1,td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop=1,td = "FR,FL" },
   { action_name = "Walk_Right", rate = 4, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
   { action_name = "Attack01_Bash", rate = 22, loop=1,td = "FR,FL" },
   { action_name = "Attack02_Crawling", rate = 19, loop=1,td = "FR,FL" },
   { action_name = "Attack09_Combo", rate = 13, loop=1,td = "FR,FL" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 3, loop=1,td = "FR,FL" },
   { action_name = "Walk_Front", rate = 8, loop=1,td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop=1,td = "FR,FL" },
   { action_name = "Walk_Right", rate = 4, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
   { action_name = "Attack01_Bash", rate = 13, loop=1,td = "FR,FL" },
   { action_name = "Attack02_Crawling", rate = 22, loop=1,td = "FR,FL" },
   { action_name = "Attack05_SweepBite", rate = 19, loop = 1,randomtarget=1.5,globalcooltime=1,td = "FR,FL" },
   { action_name = "Attack09_Combo", rate = 13, loop=1,td = "FR,FL" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop=1,td = "FR,FL" },
   { action_name = "Walk_Front", rate = 8, loop=2,td = "FR,FL" },
   { action_name = "Move_Front", rate = 7, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
   { action_name = "Assault", rate = 13, loop = 1,randomtarget=1.5,globalcooltime=1,td = "FR,FL" },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 10, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
   { action_name = "Assault", rate = 13, loop = 1,randomtarget=1.5,globalcooltime=1,td = "FR,FL" },
}
g_Lua_Assault = { 
   { action_name = "Attack01_Bash", rate = 13, approach = 400.0,td = "FR,FL" },
}

g_Lua_GlobalCoolTime1 = 6000

g_Lua_Skill = { 
   { skill_index = 33624,  cooltime = 35000, rate = -1,rangemin = 0, rangemax = 5000, target = 3, encountertime=8000, td = "LF,FL,FR,RF,RB,BR,BL,LB",next_lua_skill_index=1  },
   { skill_index = 33623,  cooltime = 55000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "0,50", td = "FR,FL", randomtarget=1.5 },
   { skill_index = 33621,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, encountertime=10000, },
   { skill_index = 33622,  cooltime = 15000, rate = 55, rangemin = 0, rangemax = 3000, target = 3, td = "FR,FL",randomtarget=1.5 },
}
