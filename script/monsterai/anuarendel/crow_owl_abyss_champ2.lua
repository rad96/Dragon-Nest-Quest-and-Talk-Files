--AiCrow_Owl_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop=1 },
   { action_name = "Walk_Back", rate = 10, loop=2 },
   { action_name = "Walk_Left", rate = 3, loop=1 },
   { action_name = "Walk_Right", rate = 3, loop=1 },
   { action_name = "Mpve_Back", rate = 10, loop=1 },
   { action_name = "Attack01_Normal", rate = 13, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop=1 },
   { action_name = "Walk_Front", rate = 5, loop=1 },
   { action_name = "Move_Left", rate = 3, loop=1 },
   { action_name = "Move_Right", rate = 3, loop=1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 3, loop=1 },
   { action_name = "Walk_Front", rate = 5, loop=1 },
   { action_name = "Move_Front", rate = 9, loop=1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop=1 },
   { action_name = "Move_Front", rate = 7, loop=3 },
}
g_Lua_Skill = { 
   { skill_index = 33582,  cooltime = 53000, rate = 70,rangemin = 0, rangemax = 600, target = 3,encountertime=6000 },
   { skill_index = 33583,  cooltime = 32000, rate = 100, rangemin = 0, rangemax = 500, target = 3 },
   { skill_index = 33331, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1 },
}
