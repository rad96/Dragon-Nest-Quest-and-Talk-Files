--AiFrogMagician_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 20, loop = 2 },
   { action_name = "Walk_Left", rate = 4, loop = 2 },
   { action_name = "Walk_Right", rate = 4, loop = 2 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 14, loop = 1 },
   { action_name = "Attack01_YoYo", rate = 20, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 20, loop = 2 },
   { action_name = "Walk_Left", rate = 4, loop = 2 },
   { action_name = "Walk_Right", rate = 4, loop = 2 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 2 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2 },
   { action_name = "Move_Front", rate = 4, loop = 1 },
}
g_Lua_NonDownMeleeDamage = { 
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
}
g_Lua_Skill = { 
   { skill_index = 33561,  cooltime = 10000, rate = 50,rangemin = 600, rangemax = 1200, target = 3 },
   { skill_index = 33565,  cooltime = 60000, rate = 25, rangemin = 0, rangemax = 500, target = 4, selfhppercent = 75 },
   { skill_index = 33567,  cooltime = 18000, rate = 25, rangemin = 500, rangemax = 1000, target = 3 },
}
