--AiScorpion_Red_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 4000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssualtTime = 60000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 4, loop = 1, td = "FL,FR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Move_Front", rate = 4, loop = 1, td = "FL,FR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 2, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 5, loop = 1, td = "FL,FR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 3, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 5, loop = 1, td = "FL,FR" },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 5, loop = 1, td = "FL,FR" },
}
g_Lua_Assault = { 
   { action_name = "Attack11_Shoving", rate = 8, loop = 1, approach = 350, td = "FL,FR" },
   { action_name = "Stand", rate = 2, loop = 1, approach = 350 },
}

g_Lua_GlobalCoolTime1 = 15000

g_Lua_Skill = { 
   { skill_index = 35247,  cooltime = 1000, rate = 60, globalcooltime = 1, rangemin = 200, rangemax = 400, target = 3 },
   { skill_index = 35248,  cooltime = 1000, rate = 60, globalcooltime = 1, rangemin = 0, rangemax = 200, target = 3 },
   { skill_index = 35249,  cooltime = 10000, rate = 70, rangemin = 0, rangemax = 400, target = 3,  td = "FL,FR" },
   { skill_index = 35253,  cooltime = 31000, rate = 30, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1", notusedskill  = "35250" },
   { skill_index = 35251,  cooltime = 22000, rate = 40, rangemin = 0, rangemax = 400, target = 3 },
   { skill_index = 35258,  cooltime = 27000, rate = 80, rangemin = 600, rangemax = 10000, target = 3, td = "FL,FR,RF,LF", randomtarget = "1.6,0,1" },
   { skill_index = 35255,  cooltime = 40000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = "70" },
   { skill_index = 35254,  cooltime = 31000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.5,0", priority = 40, encountertime = 10000, usedskill  = "35250" },
}
