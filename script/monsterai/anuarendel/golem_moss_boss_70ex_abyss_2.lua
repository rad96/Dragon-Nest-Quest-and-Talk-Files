--AiGolem_Moss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1400;
g_Lua_NearValue5 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 350
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "Attack1_Punch", rate = 25, loop = 1, td = "FR,FL,LF,RF" },
   { action_name = "Attack17_RightHand", rate = 5, loop = 1, td = "FR,FL,RF" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "Walk_Front", rate = 7, loop = 1 },
   { action_name = "Attack17_RightHand", rate = 25, loop = 1, td = "FR,FL,RF" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
   { action_name = "Assault", rate = 25, loop = 1, selfhppercentrange = "0,59"  },
   { action_name = "Assault", rate = 5, loop = 1, selfhppercentrange = "60,100"  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 1 },
   { action_name = "Assault", rate = 25, loop = 1, selfhppercentrange = "0,59"  },
   { action_name = "Assault", rate = 10, loop = 1, selfhppercentrange = "60,100"  },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 25, loop = 1 },
   { action_name = "Assault", rate = 25, loop = 1, selfhppercentrange = "0,59"  },
   { action_name = "Assault", rate = 15, loop = 1, selfhppercentrange = "60,100"  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Punch", rate = 5, loop = 1, approach = 250, td = "FR,FL,LF,RF" },
   { action_name = "Attack17_RightHand", rate = 6, loop = 1, approach = 450, td = "FR,FL,RF" },
}

g_Lua_GlobalCoolTime1 = 80000

g_Lua_Skill = { 
   { skill_index = 35007,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 35164,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 35008,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 35004,  cooltime = 70000, rate = 60, rangemin = 0, rangemax = 20000, target = 3, selfhppercentrange = "0,75", multipletarget = "1,4", globalcooltime = 1, next_lua_skill_index = 1 },
   { skill_index = 35019,  cooltime = 70000, rate = 70, rangemin = 0, rangemax = 20000, target = 3, selfhppercentrange = "0,75", multipletarget = "1,4", globalcooltime = 1, next_lua_skill_index = 0 },
   { skill_index = 35020,  cooltime = 60000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,50", next_lua_skill_index = 2 },
   { skill_index = 35005,  cooltime = 30000, rate = 20, rangemin = 300, rangemax = 1000, target = 3, randomtarget = "1.6,0,1" },
   { skill_index = 35006,  cooltime = 25000, rate = 30, rangemin = 0, rangemax = 400, target = 3, randomtarget = "1.6,0,1" },
   { skill_index = 35021,  cooltime = 55000, rate = 40, rangemin = 0, rangemax = 1500, target = 3, td = "FR,FL" },
   { skill_index = 35022,  cooltime = 55000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, td = "FR,FL,LF,RF" },
}
