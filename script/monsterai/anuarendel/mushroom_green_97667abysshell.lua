g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 800;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 =
{
   { action_name = "Stand", rate = 10, loop = 1  },
}
g_Lua_Near2 =
{
   { action_name = "Stand", rate = 10, loop = 1  },
}
g_Lua_Near3 =
{
   { action_name = "Stand", rate = 10, loop = 1  },
}
g_Lua_Near4 =
{
   { action_name = "Stand", rate = 10, loop = 1  },
}

g_Lua_Skill = { 
   { skill_index = 33773, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, encountertime = 4000 },
}