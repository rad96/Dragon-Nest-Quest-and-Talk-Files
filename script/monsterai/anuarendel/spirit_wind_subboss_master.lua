--AiSpirit_Wind_Boss_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 250
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 8, loop = 1, selfhppercentrange = "30,100"  },
   { action_name = "Stand", rate = 8, loop = 1, selfhppercentrange = "0,29"  },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "Move_Back", rate = 20, loop = 1 },
   { action_name = "Attack01_X_cut", rate = 27, loop = 1 },
   { action_name = "Attack08_Drill", rate = 27, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 8, loop = 1, selfhppercentrange = "30,100"  },
   { action_name = "Stand", rate = 8, loop = 1, selfhppercentrange = "0,29"  },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 15, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 33728,  cooltime = 18000, rate = 60,rangemin = 600, rangemax = 1200, target = 3, randomtarget = 1.5, priority = 10 },
   { skill_index = 33729,  cooltime = 50000, rate = 60, rangemin = 0, rangemax = 400, target = 3, priority = 10  },
   { skill_index = 33724,  cooltime = 12000, rate = 80, next_lua_skill_index= 3, rangemin = 900, rangemax = 1200, randomtarget = "0.5,0", target = 3, priority = 15 },
   { skill_index = 33725,  cooltime = 1000, rate = -1, next_lua_skill_index= 4, rangemin = 0, rangemax = 1500, randomtarget = "0.5,0", target = 3 },
   { skill_index = 33725,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, randomtarget = "0.5,0", target = 3, selfhppercent = 30 },
   -- { skill_index = 33721,  cooltime = 62000, rate = 60, rangemin = 600, rangemax = 1500, target = 3, selfhppercent = 80, randomtarget= "1.6,0,1", priority = 10 },
   { skill_index = 33726,  cooltime = 50000, rate = 60, rangemin = 0, rangemax = 1800, target = 3, priority = 20, selfhppercentrange = "0,70" },
   -- { skill_index = 33727,  cooltime = 60000, rate = 60, next_lua_skill_index= 8, rangemin = 0, rangemax = 1800, target = 3, limitcount = 3, selfhppercentrange = "0,49" },
   -- { skill_index = 33723,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
}
