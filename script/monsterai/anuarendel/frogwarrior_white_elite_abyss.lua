--AiFrogWarrior_White_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 250
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Stand5", 0 },
      { "Stand3", 0 },
  },
  CustomAction2 = {
      { "Stand5", 0 },
      { "Stand4", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Stand2", rate = 6, loop = 1 },
   { action_name = "CustomAction1", rate = 6, loop = 1 },
   { action_name = "Walk_Back", rate = 6, loop = 2 },
   { action_name = "Walk_Left", rate = 9, loop = 2 },
   { action_name = "Walk_Right", rate = 9, loop = 2 },
   { action_name = "Attack01_Pierce", rate = 15, loop = 1, max_missradian = 20 },
   { action_name = "Attack02_Double_Pierce", rate = 15, loop = 1, max_missradian = 20 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "CustomAction1", rate = 6, loop = 1 },
   { action_name = "CustomAction2", rate = 6, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 2 },
   { action_name = "Walk_Right", rate = 6, loop = 2 },
   { action_name = "Move_Left", rate = 6, loop = 1 },
   { action_name = "Move_Right", rate = 6, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Stand3", rate = 6, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Assault", rate = 23, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand1", rate = 10, loop = 1 },
   { action_name = "Stand2", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack01_Pierce", rate = 20, loop = 1, approach = 250, max_missradian = 20 },
   { action_name = "Stand2", rate = 6, loop = 1, approach = 300 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 300 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 300 },
}
g_Lua_Skill = { 
   { skill_index = 33541,  cooltime = 10000, rate = 50,rangemin = 400, rangemax = 500, target = 3 },
   { skill_index = 33543,  cooltime = 20000, rate = 25,  rangemin = 200, rangemax = 300, target = 3  },
}
