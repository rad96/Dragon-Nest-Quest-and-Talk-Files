--GreenDragonNest_Gate4_QueenSpider_Green.lua
--/genmon 236040
g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 450;
g_Lua_NearValue4 = 700;
g_Lua_NearValue5 = 1000;
g_Lua_NearValue6 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 10000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Attack1_Slash_Lv2", rate = 20, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Attack1_Slash_Lv2", rate = 25 },   
   { action_name = "Attack3_JumpAttack", rate = 15 , loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Attack3_JumpAttack", rate = 15 , loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 5, loop = 1 },
   { action_name = "Attack3_JumpAttack", rate = 15, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 3, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Attack3_JumpAttack", rate = 13, loop = 1 },
}
g_Lua_Near6 = { 
   { action_name = "Stand", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
}

g_Lua_Skill = { 
   { skill_index = 31412,  cooltime = 60000, rate = 100,rangemin = 200, rangemax = 5000, target = 1, limitcount=1, selfhppercent = 50 },--����ȭ
   { skill_index = 31414,  cooltime = 25000, rate = 50,rangemin = 300, rangemax = 700, target = 3, },--�Ź��ٻѸ���
   { skill_index = 31415,  cooltime = 20000, rate = 80,rangemin = 0, rangemax = 5000, target = 3,},--�Ѹ�����
}
