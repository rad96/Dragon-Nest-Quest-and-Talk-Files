--AiClawwolf_Black_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
   { action_name = "Attack06_Scratch", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 2 },
   { action_name = "Walk_Right", rate = 4, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 18, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 18, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Move_Left", rate = 10, loop = 1, approach = 300 },
   { action_name = "Move_Right", rate = 10, loop = 1, approach = 300 },
   { action_name = "Attack06_Scratch", rate = 20, loop = 1, approach = 300 },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
}

g_Lua_NonDownMeleeDamage = { 
   { action_name = "Attack06_Scratch", rate = 10, loop = 1},
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
}

g_Lua_BeHitSkill =
	{
		{ lua_skill_index = 1, rate = 100, skill_index = 33661 },
		{ lua_skill_index = 2, rate = 100, skill_index = 33662 },
	}
g_Lua_Skill = { 
   { skill_index = 33661,  cooltime = 24000, rate = 60,rangemin = 600, rangemax = 1200, target = 3, selfhppercent = 80 },
   { skill_index = 33662,  cooltime = 1000, rate = -1,  rangemin = 0, rangemax = 1500, target = 3  },
   { skill_index = 33663,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3  },
   { skill_index = 33664,  cooltime = 24000, rate = 60, rangemin = 600, rangemax = 900, target = 3  },
   { skill_index = 33665,  cooltime = 12000, rate = 35,  rangemin = 400, rangemax = 900, target = 3 },
   { skill_index = 33666,  cooltime = 12000, rate = 35, rangemin = 0, rangemax = 300, target = 3  },
   { skill_index = 33667,  cooltime = 18000, rate = 35, rangemin = 0, rangemax = 300, target = 3, selfhppercent = 80  },
}
