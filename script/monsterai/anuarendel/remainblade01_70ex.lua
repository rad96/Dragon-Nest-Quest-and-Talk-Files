--AiGargoyle_Gray_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
}

g_Lua_Skill = { 

   { skill_index = 35141,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 전진
   { skill_index = 35142,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 후진
   { skill_index = 35143,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 좌측
   { skill_index = 35144,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 우측
   { skill_index = 35145,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 원상복귀
   { skill_index = 35146,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 보호막
   { skill_index = 35147,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 스턴 > 보호막 연속스킬
   
}
