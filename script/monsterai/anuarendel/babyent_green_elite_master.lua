--AiBabyEnt_Green_Elite_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 12, loop = 1 },
   { action_name = "Stand2", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 9, loop = 1 },
   { action_name = "Walk_Right", rate = 9, loop = 1 },
   { action_name = "Walk_Back", rate = 5, loop = 1 },
   { action_name = "Attack2_Scream", rate = 6, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 9, loop = 1 },
   { action_name = "Stand2", rate = 9, loop = 1 },
   { action_name = "Walk_Left", rate = 12, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Attack1_BodyCheck", rate = 10, loop = 1, max_missradian = 30 },
   { action_name = "Assault", rate = 15, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Assault", rate = 15, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Scream", rate = 2, loop = 1, approach = 200 },
   { action_name = "Walk_Left", rate = 6, loop = 1, approach = 200 },
   { action_name = "Walk_Right", rate = 6, loop = 1, approach = 200 },
}

g_Lua_Skill = { 
   { skill_index = 33502,  cooltime = 5000, rate = 50,rangemin = 200, rangemax = 800, target = 3, selfhppercent = 30 },
}
