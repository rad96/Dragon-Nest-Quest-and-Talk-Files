--AiCrow_Eagle_Elite_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 450;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 10000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack2_Combo", 0 },
      { "Attack3_Snatch", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop=1 },
   { action_name = "Walk_Front", rate = 2, loop=1 },
   { action_name = "Walk_Back", rate = 3, loop=1 },
   { action_name = "Walk_Left", rate = 5, loop=2 },
   { action_name = "Walk_Right", rate = 5, loop=2 },
   { action_name = "Move_Back", rate = 3, loop=1 },
   { action_name = "Attack01_Normal", rate = 9, loop=1 },
   { action_name = "Attack2_Combo", rate = 19, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop=1 },
   { action_name = "Walk_Front", rate = 8, loop=1 },
   { action_name = "Walk_Back", rate = 5, loop=2 },
   { action_name = "Move_Left", rate = 6, loop=2 },
   { action_name = "Move_Right", rate = 6, loop=2 },
   { action_name = "CustomAction1", rate = 9, loop=1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop=1 },
   { action_name = "Move_Front", rate = 7, loop=2 },
   { action_name = "Move_Left", rate = 3, loop=2 },
   { action_name = "Move_Right", rate = 3, loop=2 },
   { action_name = "Assault", rate = 13, loop=1 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 8, loop=3 },
}
g_Lua_Assault = { 
   { action_name = "CustomAction1", rate = 13, approach = 200.0 },
}
g_Lua_Skill = { 
   { skill_index = 33601,  cooltime = 50000, rate = 60,rangemin = 200, rangemax = 1500, target = 3, encountertime=10000  },
   { skill_index = 33602,  cooltime = 62000, rate = 100, rangemin = 300, rangemax = 1000, target = 3,selfhppercentrange = "0,75" },
}
