
g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1500;
g_Lua_NearValue3 = 2500;
g_Lua_NearValue4 = 5000;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;

g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Left", rate = 4, loop = 1 },
     { action_name = "Move_Right", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 6, loop = 1 },
}

g_Lua_Near2 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Left", rate = 4, loop = 1 },
     { action_name = "Move_Right", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 6, loop = 1 },
}

g_Lua_Near3 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Left", rate = 4, loop = 1 },
     { action_name = "Move_Right", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 6, loop = 1 },
}

g_Lua_Near4 = 
{
     { action_name = "Stand", rate = 6, loop = 1 },
     { action_name = "Move_Left", rate = 4, loop = 1 },
     { action_name = "Move_Right", rate = 4, loop = 1 },
     { action_name = "Move_Front", rate = 6, loop = 1 },
}