--AiQueenSpider_Elite_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 450;
g_Lua_NearValue4 = 700;
g_Lua_NearValue5 = 1000;
g_Lua_NearValue6 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 15, loop = 2  },
   { action_name = "Walk_Right", rate = 15, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack1_Slash_Lv2", rate = 10, loop = 1  },
   { action_name = "Attack2_Blow", rate = 13, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Attack1_Slash_Lv2", rate = 9, loop = 1  },
   { action_name = "Attack2_Blow", rate = 8, loop = 1  },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Attack2_Blow", rate = 11, loop = 1  },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Stand_2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Attack3_JumpAttack_Lv2", rate = 30, loop = 1  },
   { action_name = "Assault", rate = 4, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Stand_2", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Attack3_JumpAttack_Lv2", rate = 40, loop = 1  },
   { action_name = "Assault", rate = 3, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Stand", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Slash_Lv2", rate = 5, loop = 1, cancellook = 1, approach = 150.0  },
   { action_name = "Attack2_Blow", rate = 10, loop = 1, cancellook = 0, approach = 150.0  },
}
g_Lua_Skill = { 
   { skill_index = 30461,  cooltime = 24000, rate = 90, rangemin = 400, rangemax = 1200, target = 3, selfhppercent = 100 },
}
