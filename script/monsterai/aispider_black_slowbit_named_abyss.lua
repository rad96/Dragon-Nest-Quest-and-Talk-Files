--AiSpider_Black_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 80;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 350;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack3_DashBite", 1 },
      { "Walk_Back", 2 },
  },
  CustomAction2 = {
      { "Attack5_JumpAttack", 1 },
      { "Move_Back", 2 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Attack2_Bite", rate = 12, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 1  },
   { action_name = "Move_Right", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 1  },
   { action_name = "Walk_Right", rate = 8, loop = 1  },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 7, loop = 1  },
   { action_name = "Move_Left", rate = 19, loop = 1  },
   { action_name = "Move_Right", rate = 19, loop = 1  },
   { action_name = "Move_Front", rate = 4, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 20, loop = 1  },
   { action_name = "Move_Right", rate = 20, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Attack5_JumpAttack", rate = 2, loop = 1, max_missradian = 30  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 30, loop = 1  },
   { action_name = "Move_Left", rate = 18, loop = 1  },
   { action_name = "Move_Right", rate = 18, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20097,  cooltime = 3000, rate = 80,rangemin = 0, rangemax = 350, target = 3, selfhppercent = 100 },
   { skill_index = 20098,  cooltime = 5000, rate = 80, rangemin = 200, rangemax = 350, target = 3, selfhppercent = 100 },
   { skill_index = 20099,  cooltime = 5000, rate = 80, rangemin = 350, rangemax = 800, target = 3, selfhppercent = 100 },
}
