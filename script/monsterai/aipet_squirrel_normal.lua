-- Pet_Squirrel AI

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 0.0;
g_Lua_NearValue2 = 100.0;
g_Lua_NearValue3 = 250.0;
g_Lua_NearValue4 = 400.0;
g_Lua_NearValue5 = 1200.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 5000;

g_Lua_CustomAction =
{
		
}




g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Attack1_BiteCombo",	rate = 10,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
            { action_name = "Attack1_BiteCombo",	rate = 10,		loop = 1 },
}

g_Lua_Near3 = 
{ 

	{ action_name = "Stand",	rate = 5,		loop = 1 },
            { action_name = "Move_Front",	       rate = 10,		loop = 2 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
            { action_name = "Move_Front",	       rate = 10,		loop = 2 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Move_Front",	      rate = 10,		loop = 3 },
            { action_name = "Assault",	rate = 5,		loop = 1 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_BiteCombo",	rate = 5,	loop = 1, cancellook = 0, approach = 100.0 },         
}
       