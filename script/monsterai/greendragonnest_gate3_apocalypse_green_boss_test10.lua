-- /genmon 236010
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 800.0;
g_Lua_NearValue2 = 1200.0;
g_Lua_NearValue3 = 1700.0;
g_Lua_NearValue4 = 2500.0;
g_Lua_NearValue5 = 5000.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 800;
g_Lua_AssualtTime = 5000;

g_Lua_GlobalCoolTime1 = 35000 
g_Lua_GlobalCoolTime2 = 6000
g_Lua_GlobalCoolTime3 = 10000

g_Lua_CustomAction = 
{
	CustomAction1 = 
	{
		{"Attack01_Bite_Open",0},
		{"Attack05_Spin_Open",0},
	},
}
g_Lua_Near1 = 
	{
		-- { action_name = "Attack08_EarthQuake",rate = 10, loop = 1,notusedskill="31382",selfhppercentrange="80,100"}, 
		-- { action_name = "Stand_Open",rate = 10, loop = 1 }, 
		-- { action_name = "Attack01_Bite_Open",rate = 15, loop = 1,td="FL,FR",selfhppercentrange="40,100" }, 
		-- { action_name = "Attack_BasicLeft",rate = 25,td = "LF,FL,LB", loop = 1 }, 
		-- { action_name = "Attack_BasicRight",rate = 25,td = "FR,RF,RB", loop = 1 }, 				
		-- { action_name = "Attack05_Spin_Open",rate = 30, loop = 1,selfhppercentrange="40,100" }, 
		-- { action_name = "CustomAction1",rate = 15, loop = 1,cooltime=15000,selfhppercentrange="0,40" }, 
		-- { action_name = "Attack02_ClawMelee_Open",rate = 30, loop = 1 }, 
	}
g_Lua_Near2 = 
	{ 
		-- { action_name = "Attack08_EarthQuake",rate = 10, loop = 1,notusedskill="31382" }, 
		-- { action_name = "Stand_Open",rate = 10, loop = 1 }, 
		-- { action_name = "Attack01_Bite_Open",rate = 15, loop = 1,cooltime=15000,selfhppercentrange="40,100" }, 
		-- { action_name = "Attack05_Spin_Open",rate = 10, loop = 1,selfhppercentrange="40,100" }, 
		-- { action_name = "CustomAction1",rate = 15, loop = 1,cooltime=15000,selfhppercentrange="0,40" }, 
		-- { action_name = "Attack16_EyeLaser_All",rate = 10, loop = 1,selfhppercentrange="0,40" }, 
		-- { action_name = "Attack_BasicLeft",rate = 25,td = "LF,FL,LB", loop = 1 }, 
		-- { action_name = "Attack_BasicRight",rate = 25,td = "FR,RF,RB", loop = 1 }, 	
		-- { action_name = "Attack16_EyeLaser_Front",rate = 10,	 loop = 1,	td = "LF,FL,FR,RF",	selfhppercentrange="40,100" }, 
		-- { action_name = "Attack16_EyeLaser_Line_Three",rate = 10,	 loop = 1, globalcooltime=2}, 
		-- { action_name = "Attack16_EyeLaser_Line_Two",rate = 10, 		loop = 1,globalcooltime=2 }, 
		-- { action_name = "Attack16_EyeLaser_Front_Fury",rate = 10, loop = 1,td = "LF,FL,FR,RF",selfhppercentrange="0,40" }, 
		-- { action_name = "Attack16_EyeLaser_Left_Fury",rate = 10, loop = 1,td = "LF,FL,LB",selfhppercentrange="0,40"}, 
		-- { action_name = "Attack16_EyeLaser_Right_Fury",rate = 10, loop = 1,td = "FR,RF,RB",selfhppercentrange="0,40"},
		-- { action_name = "Attack03_ClawRange_Open",rate = 10,multipletarget = "1,4", loop = 1 }, 
		-- { action_name = "Attack16_EyeLaser_Line_Three",rate = 10,	 loop = 1, 		selfhppercentrange="40,100" }, 
			{ action_name = "Attack03_ClawRange_Open_P1",rate = 10,	 loop = 1, globalcooltime=3}, 
			{ action_name = "Attack03_ClawRange_Open_P2",rate = 10,	 loop = 1, globalcooltime=3},
			-- { action_name = "Attack03_ClawRange_Open_P3",rate = 10,	 loop = 1, globalcooltime=3},
			-- { action_name = "Attack03_ClawRange_Open_P4",rate = 10,	 loop = 1, globalcooltime=3},
			-- { action_name = "Attack03_ClawRange_Open_P5",rate = 10,	 loop = 1, globalcooltime=3},
	}
g_Lua_Near3 = 
	{
		-- { action_name = "Attack08_EarthQuake",rate = 10, loop = 1,notusedskill="31382" }, 
		-- { action_name = "Stand_Open",rate = 10, loop = 1 },
		-- { action_name = "Attack06_Jump_Open",rate = 10, loop = 1 }, 
		-- { action_name = "CustomAction1",rate = 10, loop = 1 }, 
		-- { action_name = "Attack16_EyeLaser_All",rate = 10, loop = 1,selfhppercentrange="0,40" }, 
		-- { action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2}, 
		-- { action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1, globalcooltime=2}, 
		-- { action_name = "Attack16_EyeLaser_Right",rate = 10, loop = 1,td = "FR,RF,RB",selfhppercentrange="40,100" },
		-- { action_name = "Attack16_EyeLaser_Front_Fury",rate = 10, loop = 1,td = "LF,FL,FR,RF",selfhppercentrange="0,40" }, 
		-- { action_name = "Attack16_EyeLaser_Left_Fury",rate = 10, loop = 1,td = "LF,FL,LB",selfhppercentrange="0,40"}, 
		-- { action_name = "Attack16_EyeLaser_Right_Fury",rate = 10, loop = 1,td = "FR,RF,RB",selfhppercentrange="0,40"},
		-- { action_name = "Attack03_ClawRange_Open",rate = 10, loop = 1 }, 
		{ action_name = "Attack03_ClawRange_Open_P1",rate = 10,	 loop = 1, globalcooltime=3}, 
		{ action_name = "Attack03_ClawRange_Open_P2",rate = 10,	 loop = 1, globalcooltime=3},
		-- { action_name = "Attack03_ClawRange_Open_P3",rate = 10,	 loop = 1, globalcooltime=3},
		-- { action_name = "Attack03_ClawRange_Open_P4",rate = 10,	 loop = 1, globalcooltime=3},
		-- { action_name = "Attack03_ClawRange_Open_P5",rate = 10,	 loop = 1, globalcooltime=3},
	}
g_Lua_Near4 = 
	{
		-- { action_name = "Attack08_EarthQuake",rate = 10, loop = 1,notusedskill="31382" }, 
		-- { action_name = "Stand_Open",rate = 10, loop = 1 }, 
		-- { action_name = "Attack20_Center_Eye_Magic_Open",multipletarget = "1,8",rate = 30, loop = 1 }, 
		-- { action_name = "Attack16_EyeLaser_All",rate = 10, loop = 1,selfhppercentrange="0,40" }, 
		-- { action_name = "Attack16_EyeLaser_Line_Three",rate = 10, loop = 1,globalcooltime=2}, 
		-- { action_name = "Attack16_EyeLaser_Line_Two",rate = 10, loop = 1,globalcooltime=2}, 
		-- { action_name = "Attack16_EyeLaser_Right",rate = 10, loop = 1,td = "FR,RF,RB",selfhppercentrange="40,100" }, 
		-- { action_name = "Attack16_EyeLaser_Front_Fury",rate = 10, loop = 1,td = "LF,FL,FR,RF",selfhppercentrange="0,40" }, 
		-- { action_name = "Attack16_EyeLaser_Left_Fury",rate = 10, loop = 1,td = "LF,FL,LB",selfhppercentrange="0,40"}, 
		-- { action_name = "Attack16_EyeLaser_Right_Fury",rate = 10, loop = 1,td = "FR,RF,RB",selfhppercentrange="0,40"},
		-- { action_name = "Attack03_ClawRange_Open",rate = 10, loop = 1 }, 
		-- { action_name = "Stand_Open",rate = 10, loop = 1 }, 
		{ action_name = "Attack03_ClawRange_Open_P1",rate = 10,	 loop = 1, globalcooltime=3}, 
		{ action_name = "Attack03_ClawRange_Open_P2",rate = 10,	 loop = 1, globalcooltime=3},
		-- { action_name = "Attack03_ClawRange_Open_P3",rate = 10,	 loop = 1, globalcooltime=3},
		-- { action_name = "Attack03_ClawRange_Open_P4",rate = 10,	 loop = 1, globalcooltime=3},
		-- { action_name = "Attack03_ClawRange_Open_P5",rate = 10,	 loop = 1, globalcooltime=3},
	}
g_Lua_Near5 = 
	{ 
		-- { action_name = "Attack08_EarthQuake",rate = 10, loop = 1,notusedskill="31382" }, 
		-- { action_name = "Stand_Open",rate = 10, loop = 1 }, 
		-- { action_name = "Attack03_ClawRange_Open",rate = 10, loop = 1 }, 
		-- { action_name = "Stand_Open",rate = 10, loop = 1 }, 
		-- { action_name = "Attack16_EyeLaser_Line_Three",rate = 10,	 loop = 1,globalcooltime=2 }, 
		-- { action_name = "Attack16_EyeLaser_Line_Two",rate = 10, 		loop = 1, globalcooltime=2}, 
		-- { action_name = "Stand_Open",rate = 10, loop = 1 }, 
		{ action_name = "Attack03_ClawRange_Open_P1",rate = 10,	 loop = 1, globalcooltime=3}, 
		{ action_name = "Attack03_ClawRange_Open_P2",rate = 10,	 loop = 1, globalcooltime=3},
		-- { action_name = "Attack03_ClawRange_Open_P3",rate = 10,	 loop = 1, globalcooltime=3},
		-- { action_name = "Attack03_ClawRange_Open_P4",rate = 10,	 loop = 1, globalcooltime=3},
		-- { action_name = "Attack03_ClawRange_Open_P5",rate = 10,	 loop = 1, globalcooltime=3},
	}
-- g_Lua_Skill = {
   -- { skill_index = 31400, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 4500, next_lua_skill_index = 1,target = 3 }, -- 즉사 플래어
   -- { skill_index = 31383, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 4500, next_lua_skill_index = 2,target = 3 }, -- (개안)눈감기
   -- { skill_index = 31401, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 4500									  ,target = 3 }, -- 잠자기(그로기)
   -- { skill_index = 31397, cooltime = 40000, rate = -1, rangemin = 0, rangemax = 2500, target = 3 },  -- 점프
   -- { skill_index = 31396,  cooltime = 40000, rate = 50, rangemin = 0 ,rangemax= 5500,target = 3,next_lua_skill_index = 3,multipletarget = "1,8",encountertime=30000 }, --호밍빔
   -- { skill_index = 31382, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 4500   									  ,target = 3 }, -- (폐안)눈뜨기.
   -- { skill_index = 31385, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 4500, next_lua_skill_index = 0,target = 3,encountertime=40000,globalcooltime=1 }, --잔상공격 ->넥스트액션 즉사플래어
   -- { skill_index = 31391, cooltime = 45000, rate = 80, rangemin = 0, rangemax = 5500, target = 3 ,globalcooltime=1,encountertime=20000 }, -- 외치기(밀쳐내기)
   -- { skill_index = 31402, cooltime = 40000, rate = 100, rangemin = 0, rangemax = 2500, target = 3,selfhppercent=40,notusedskill="31402"},  --격노
   -- { skill_index = 31384, cooltime = 60000, rate = 30, rangemin = 0, rangemax = 4500, target = 3,selfhppercentage="0,40" }, --아포칼립스
-- }
   