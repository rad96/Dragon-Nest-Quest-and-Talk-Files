-- Manticore Normal A.I
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 450.0;
g_Lua_NearValue2 = 650.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 1800.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 320;
g_Lua_AssualtTime = 3000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,RF", existparts="10" },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Back",	rate = 8,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Attack11_LHook_Lord",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10" },
	{ action_name = "Attack11_LHook_Lord",	rate = 50,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10", target_condition = "State2" },
	{ action_name = "Attack11_RHook_Lord",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10" },
	{ action_name = "Attack11_RHook_Lord",	rate = 50,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10", target_condition = "State2" },
	{ action_name = "Turn_Left",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="10" },
	{ action_name = "Attack04_Tail_L_Lord",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,LB,BL,BR", existparts="10"},
	{ action_name = "Turn_Right",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="10" },
	{ action_name = "Attack05_Tail_R_Lord",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "RF,RB,BL,BR", existparts="10" },
	{ action_name = "Attack01_BigGravityBall",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10" },
	{ action_name = "Attack15_Gust",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack03_HeavyStamp",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack02_BlackHole",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Fly_Start",	rate = -1,		loop = 1, custom_state1 = "custom_ground", existparts="11" },
	{ action_name = "Fly",		rate = 5,		loop = 1, custom_state1 = "custom_fly", existparts="11" },
	{ action_name = "Attack07_HeavySwoop",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="10", randomtarget=1.1 },
	{ action_name = "Attack16_AirGust",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="11", multipletarget = 1 },
	{ action_name = "Attack12_AirGravityBall",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="11", multipletarget = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="10" },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Back",	rate = 5,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Attack11_LHook_Lord",	rate = 15,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10" },
	{ action_name = "Attack11_LHook_Lord",	rate = 50,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10", target_condition = "State2" },
	{ action_name = "Attack11_RHook_Lord",	rate = 15,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10" },
	{ action_name = "Attack11_RHook_Lord",	rate = 50,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10", target_condition = "State2" },
	{ action_name = "Turn_Left",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="10" },
	{ action_name = "Turn_Right",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="10" },
	{ action_name = "Attack01_BigGravityBall",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10" },
	{ action_name = "Attack13_Spectrum",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack14_RaiseGravity1",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack14_RaiseGravity2",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack15_Gust",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack06_GravityThorn",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10" },
	{ action_name = "Attack03_HeavyStamp",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack02_BlackHole",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Fly_Start",	rate = -1,		loop = 1, custom_state1 = "custom_ground", existparts="11" },
	{ action_name = "Fly",		rate = 5,		loop = 1, custom_state1 = "custom_fly", existparts="11" },
	{ action_name = "Attack07_HeavySwoop",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="10", randomtarget=1.1 },
	{ action_name = "Attack16_AirGust",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="11", multipletarget = 1 },
	{ action_name = "Attack12_AirGravityBall",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="11", multipletarget = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 10,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="10" },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Front",	rate = 30,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Back",	rate = 5,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Turn_Left",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="10" },
	{ action_name = "Turn_Right",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="10" },
	{ action_name = "Attack09_HeavyJump",	rate = 60,		loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", existparts="10", randomtarget=1.1 },
	{ action_name = "Attack01_BigGravityBall",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10" },
	{ action_name = "Attack13_Spectrum",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack14_RaiseGravity1",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack14_RaiseGravity2",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack15_Gust",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack06_GravityThorn",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10" },
	{ action_name = "Attack03_HeavyStamp",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack02_BlackHole",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Fly_Start",	rate = -1,		loop = 1, custom_state1 = "custom_ground", existparts="11" },
	{ action_name = "Fly",		rate = 5,		loop = 1, custom_state1 = "custom_fly", existparts="11" },
	{ action_name = "Attack07_HeavySwoop",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="10", randomtarget=1.1 },
	{ action_name = "Attack16_AirGust",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="11", multipletarget = 1 },
	{ action_name = "Attack12_AirGravityBall",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="11", multipletarget = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="10" },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Front",	rate = 20,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Turn_Left",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="10" },
	{ action_name = "Turn_Right",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="10" },
	{ action_name = "Attack09_HeavyJump",	rate = 70,		loop = 1, custom_state1 = "custom_ground", max_missradian = 1, td = "FL,FR,LF,RF", existparts="10", randomtarget=1.1 },
	{ action_name = "Attack01_BigGravityBall",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10" },
	{ action_name = "Attack13_Spectrum",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack14_RaiseGravity1",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack14_RaiseGravity2",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack15_Gust",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack06_GravityThorn",	rate = 20,		loop = 1, custom_state1 = "custom_ground", td = "LF,FL,FR,RF", existparts="10" },
	{ action_name = "Attack03_HeavyStamp",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Attack02_BlackHole",	rate = 20,		loop = 1, custom_state1 = "custom_ground", existparts="10" },
	{ action_name = "Fly_Start",	rate = -1,		loop = 1, custom_state1 = "custom_ground", existparts="11" },
	{ action_name = "Fly",		rate = 5,		loop = 1, custom_state1 = "custom_fly", existparts="11" },
	{ action_name = "Attack07_HeavySwoop",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="10", randomtarget=1.1 },
	{ action_name = "Attack16_AirGust",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="11", multipletarget = 1 },
	{ action_name = "Attack12_AirGravityBall",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="11", multipletarget = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 10,		loop = 1, custom_state1 = "custom_ground", td = "FL,FR", existparts="10" },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Walk_Front",	rate = 20,		loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", existparts="10" },
	{ action_name = "Turn_Left",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL", existparts="10" },
	{ action_name = "Turn_Right",	rate = 10,		loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR", existparts="10" },
	{ action_name = "Fly_Start",	rate = -1,		loop = 1, custom_state1 = "custom_ground", existparts="11" },
	{ action_name = "Fly",		rate = 5,		loop = 1, custom_state1 = "custom_fly", existparts="11" },
	{ action_name = "Attack07_HeavySwoop",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="10", randomtarget=1.1 },
	{ action_name = "Attack16_AirGust",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="11", multipletarget = 1 },
	{ action_name = "Attack12_AirGravityBall",	rate = 20,		loop = 1, custom_state1 = "custom_fly", existparts="11", multipletarget = 1 },
}

