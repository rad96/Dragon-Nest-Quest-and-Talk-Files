--SeaDragonNest_Boss_Golem_Green.lua
--기본공격이 거리가 되는 근거리, 원거리 최대 사거리의 중거리, 그 이상의 원거리
--Assult로 돌진후 회전공격 하도록(특징적인 공격)
--기본공격은 1~3페이즈의 확률을 동일하게, 4페이즈는 확률 상승
--칵퉤꽃 소환 시그널 ID : 2357310, 시그널 받고 10초 후 칵퉤꽃 소환
--구울 소환 시그널 ID : 2357311
--/genmon 235737

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 5000

g_Lua_SkillProcessor = {
   { skill_index = 30515, changetarget = "2000,0" },
}

g_Lua_CustomAction =
{
   CustomAction1 = 
   {
      { "Walk_Back", 0},
      { "Attack6_SpinAttack_SeaDragon", 0},
   },
}

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 5, loop = 1, selfhppercentrange = "75,100" },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 7000, selfhppercentrange = "25,100" },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 7000, selfhppercentrange = "25,100" },
--근접펀치, 근거리에서만 사용
   { action_name = "Attack1_SeaDragon", rate = 20, loop = 1, cooltime = 3000, selfhppercentrange = "75,100" },
   { action_name = "Attack1_SeaDragon", rate = 20, loop = 1, cooltime = 1000, selfhppercentrange = "25,75" },
   { action_name = "CustomAction1", rate = 20, loop = 1, cooltime = 10000, selfhppercentrange = "25,95" },
   { action_name = "CustomAction1", rate = 30, loop = 1, cooltime = 5000, selfhppercentrange = "1,24" },
   { action_name = "Attack2_SeaDragon", rate = 20, loop = 1, cooltime = 10000, selfhppercentrange = "25,100", encountertime = 10000  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 5, loop = 1, selfhppercentrange = "75,100" },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 7000, selfhppercentrange = "25,100" },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 7000, selfhppercentrange = "25,100" },
--근거리,중거리 견제 공격. 중거리에서 사용 빈도가 높다
   { action_name = "Attack2_SeaDragon", rate = 20, loop = 1, cooltime = 14000, selfhppercentrange = "75,100", encountertime = 10000 },
   { action_name = "Attack2_SeaDragon", rate = 20, loop = 1, cooltime = 10000, selfhppercentrange = "25,75" },
   { action_name = "Attack2_SeaDragon", rate = 30, loop = 1, cooltime = 5000, selfhppercentrange = "0,24" },
--돌진후 회전 공격
   { action_name = "Assault", rate = 10, loop = 1, cooltime = 27000, hppercent = 95 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 5, loop = 1, selfhppercentrange = "75,100" },
   { action_name = "Move_Front", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 2, cooltime = 7000, selfhppercentrange = "25,100" },
   { action_name = "Move_Right", rate = 3, loop = 2, cooltime = 7000, selfhppercentrange = "25,100" },
--돌진후 회전 공격
   { action_name = "Assault", rate = 20, loop = 1, cooltime = 27000, hppercent = 95 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 2, cooltime = 7000, selfhppercentrange = "25,100" },
   { action_name = "Move_Right", rate = 2, loop = 2, cooltime = 7000, selfhppercentrange = "25,100" },
}
g_Lua_Assault = { 
   { action_name = "Attack6_SpinAttack_SeaDragon", rate = 30, loop = 1, approach = 250.0  },
}

g_Lua_Skill = { 
--독안개(2페이즈에서만 사용, 칵퉤꽃 소환 시그널)
   { skill_index = 30522, cooltime = 30000, limitcount = 1, rate = 100, rangemin = 0, rangemax = 3500, target = 3, selfhppercentrange = "55,75", td = "FL,FR,LF,RF,RB,BR,BL,LB" },
--독안개(3페이즈 사용)
   { skill_index = 30514, next_lua_skill_index = 2, cooltime = 30000, limitcount = 1, rate = 100, rangemin = 0, rangemax = 3500, target = 3, selfhppercentrange = "26,50", td = "FL,FR,LF,RF,RB,BR,BL,LB" },
--파고들기(3페이즈의 독 안개 이후 연속 사용)
   { skill_index = 30515, cooltime = 30000, limitcount = 1, rate = -1, rangemin = 0, rangemax = 5000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
--독안개(4페이즈 사용)
   { skill_index = 30553, cooltime = 30000, limitcount = 1, rate = 100, rangemin = 0, rangemax = 3500, target = 3, selfhppercentrange = "1,25", td = "FL,FR,LF,RF,RB,BR,BL,LB" },
--파고들기(3페이즈 2번째 사용하는 파고들기, 사용이후 50초마다 한 번씩 사용)
   { skill_index = 30518, cooltime = 61000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "26,32", td = "FL,FR,LF,RF,RB,BR,BL,LB"  },
--대격노(체력 25%부터 사용)
   { skill_index = 30520, cooltime = 30000, rate = 100, rangemin = 0, rangemax = 4500, target = 3, selfhppercentrange = "0,24" },
--대지의분노(체력 25%부터 사용)
   { skill_index = 30519, cooltime = 20000, rate = 80, rangemin = 0, rangemax = 4500, target = 3, multipletarget = 1, selfhppercentrange = "0,24",td = "FL,FR,LF,RF,RB,BR,BL,LB" },
--가시지옥
   { skill_index = 30511, cooltime = 32000, rate = 40, rangemin = 500, rangemax = 4500, target = 3, multipletarget = "1,2", td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "9,75" },
--독 포자
   { skill_index = 30512, next_lua_skill_index = 9, cooltime = 24000, rate = 80, rangemin = 200, rangemax = 2000, target = 3, encountertime = 20000 },
--어그로리셋, 독 포자 이후 연속 사용
   { skill_index = 30548, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4500, target = 3 },
--지진(체력 25%까지만 사용)
   { skill_index = 30513, cooltime = 10000, rate = 60, rangemin = 0, rangemax = 4500, target = 3, multipletarget = 1, selfhppercentrange = "25,75",td = "FL,FR,LF,RF,RB,BR,BL,LB", encountertime = 21000 },
--광폭화(게임시작후 6분 지나면 사용)
   { skill_index = 30521, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 4500, target = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB", encountertime = 360000, notusedskill = "30521", limitcount = 1 },
}