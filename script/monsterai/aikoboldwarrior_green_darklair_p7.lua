--AiKoboldWarrior_Green_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 50;
g_Lua_NearValue2 = 150;
g_Lua_NearValue3 = 200;
g_Lua_NearValue4 = 300;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Move_Left", 2 },
      { "Attack2_Sever", 1 },
  },
  CustomAction2 = {
      { "Move_Right", 2 },
      { "Attack2_Sever", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Move_Back", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 2  },
   { action_name = "Move_Right", rate = 8, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Move_Back", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 2  },
   { action_name = "Move_Right", rate = 8, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 1, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
}
g_Lua_Near6 = { 
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Sever", rate = 4, loop = 1, cancellook = 1, approach = 150.0  },
   { action_name = "Attack1_Chop", rate = 5, loop = 1, cancellook = 1, approach = 100.0  },
}
g_Lua_Skill = { 
   { skill_index = 20030,  cooltime = 4000, rate = 100,rangemin = 0, rangemax = 10000, target = 3, selfhppercent = 100 },
}
