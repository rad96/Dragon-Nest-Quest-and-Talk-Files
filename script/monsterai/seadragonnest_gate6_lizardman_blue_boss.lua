--AiLizardman_Blue_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 10, loop = 3  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack2_Pushed", rate = 45, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack2_Pushed", rate = 18, loop = 1  },
   { action_name = "Attack1_Piercing", rate = 50, loop = 1  },
   { action_name = "Attack5_JumpAttack", rate = 27, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Attack5_JumpAttack", rate = 62, loop = 2  },
   { action_name = "Assault", rate = 45, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 90, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
}
g_Lua_MeleeDefense = { 
   { action_name = "Attack2_Pushed", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
}
g_Lua_RangeDefense = { 
   { action_name = "Attack5_JumpAttack", rate = 45, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack5_JumpAttack", rate = 45, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Pushed", rate = 5, loop = 2, cancellook = 0, approach = 250.0  },
   { action_name = "Attack1_Piercing", rate = 30, loop = 2, cancellook = 0, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 30559,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3, announce = "100,10000" },
   { skill_index = 20080,  cooltime = 15000, rate = 100,rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100, checkweapon=2 },
   { skill_index = 20081,  cooltime = 10000, rate = 100, rangemin = 400, rangemax = 900, target = 3, selfhppercent = 100 },
   { skill_index = 30561,  cooltime = 20000, rate = 80, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 75 },
   { skill_index = 30561,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, selfhppercent = 25 },
}