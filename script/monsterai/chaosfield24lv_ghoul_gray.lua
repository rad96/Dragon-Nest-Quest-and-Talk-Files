--AiGhoul_Gray_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 600;
g_Lua_NearValue5 = 1000;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Attack4_JumpBite", rate = 8, loop = 1, max_missradian = 30 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
   { action_name = "Attack4_JumpBite", rate = 1, loop = 1, max_missradian = 30, target_condition = "State1" },
   { action_name = "Attack6_HighJumpBite", rate = 10, loop = 1, max_missradian = 20, target_condition = "State1" },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
   { action_name = "Attack6_HighJumpBite", rate = 8, loop = 1, max_missradian = 20, target_condition = "State1" },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
}