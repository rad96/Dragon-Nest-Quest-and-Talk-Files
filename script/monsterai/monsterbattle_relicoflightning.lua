-- Cleric Relic of Lightnig AI

g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 500.0;
g_Lua_NearValue2 = 1500.0;



g_Lua_LookTargetNearState = 2;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;



g_Lua_Near1 = 
{
	{ action_name = "Stand",		rate = 5,		loop = 1 }, 
}


g_Lua_Near2 = 
{ 
	{ action_name = "Stand",		rate = 10,		loop = 1 },
}

g_Lua_Skill=
{
	{ skill_index = 35388, cooltime = 1000, rangemin = 0, rangemax = 800, target = 3, rate = 100, next_lua_skill_index = 1 },
	{ skill_index = 35388, cooltime = 1000, rangemin = 0, rangemax = 800, target = 3, rate = -1, next_lua_skill_index = 2 },
	{ skill_index = 35388, cooltime = 1000, rangemin = 0, rangemax = 800, target = 3, rate = -1, next_lua_skill_index = 3 },
	{ skill_index = 35388, cooltime = 1000, rangemin = 0, rangemax = 800, target = 3, rate = -1, next_lua_skill_index = 4 },
	{ skill_index = 35388, cooltime = 1000, rangemin = 0, rangemax = 800, target = 3, rate = -1, next_lua_skill_index = 5 },
}  
    