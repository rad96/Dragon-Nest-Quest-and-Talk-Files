--DarkLairDefence_Lv12_KoboldWarriorGray.lua
--코볼트 닌자만 나오는 구간, 근접시 좌우 이동이 많고 뒤로 이탈하는 것도 많다.
--원거리에서는 달리기와 걷기가 비슷한 수준
--지속적인 이동을 위해 스탠드는 낮춘다

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Move_Left", 2 },
      { "Attack2_Sever", 1 },
  },
  CustomAction2 = {
      { "Move_Right", 2 },
      { "Attack2_Sever", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Back", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "CustomAction1", rate = 3, loop = 1, target_condition = "State1"  },
   { action_name = "CustomAction2", rate = 3, loop = 1, target_condition = "State1"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 4, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 1  },
   { action_name = "Move_Right", rate = 8, loop = 1  },
   { action_name = "Attack1_Chop", rate = 11, loop = 1  },
   { action_name = "Attack2_Sever", rate = 9, loop = 1  },
   { action_name = "CustomAction1", rate = 150, loop = 1, target_condition = "State1"  },
   { action_name = "CustomAction2", rate = 150, loop = 1, target_condition = "State1"  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Assault", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 6, loop = 2  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 6, loop = 2  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Sever", rate = 4, loop = 1, cancellook = 1, approach = 150.0  },
   { action_name = "Attack1_Chop", rate = 5, loop = 1, cancellook = 1, approach = 100.0  },
}
g_Lua_Skill = { 
   { skill_index = 20031,  cooltime = 8000, rate = 100, rangemin = 0, rangemax = 500, target = 1, selfhppercent = 100 },
}
