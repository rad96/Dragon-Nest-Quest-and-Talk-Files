--AiWraith_Gray_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Move_Left_NotTarget", rate = 5, loop = 1, max_missradian = 360  },
   { action_name = "Move_Right_NotTarget", rate = 5, loop = 1, max_missradian = 360  },
   { action_name = "Move_Back_NotTarget", rate = 8, loop = 1, max_missradian = 360  },
   { action_name = "Attack8_LazerCircle", rate = 62, loop = 1, max_missradian = 360  },
   { action_name = "Attack7_LazerFront_NotTarget", rate = 62, loop = 1, max_missradian = 360  },
}
g_Lua_Near2 = { 
   { action_name = "Move_Left_NotTarget", rate = 12, loop = 1, max_missradian = 360  },
   { action_name = "Move_Right_NotTarget", rate = 12, loop = 1, max_missradian = 360  },
   { action_name = "Move_Front_NotTarget", rate = 10, loop = 1, max_missradian = 360  },
   { action_name = "Move_Back_NotTarget", rate = 8, loop = 1, max_missradian = 360  },
   { action_name = "Attack8_LazerCircle", rate = 62, loop = 1, max_missradian = 360  },
   { action_name = "Attack7_LazerFront_NotTarget", rate = 62, loop = 1, max_missradian = 360  },
}
g_Lua_Near3 = { 
   { action_name = "Move_Left_NotTarget", rate = 10, loop = 1, max_missradian = 360  },
   { action_name = "Move_Right_NotTarget", rate = 10, loop = 1, max_missradian = 360  },
   { action_name = "Move_Front_NotTarget", rate = 5, loop = 1, max_missradian = 360  },
   { action_name = "Move_Back_NotTarget", rate = 3, loop = 1, max_missradian = 360  },
   { action_name = "Attack8_LazerCircle", rate = 62, loop = 1, max_missradian = 360  },
   { action_name = "Attack7_LazerFront_NotTarget", rate = 62, loop = 1, max_missradian = 360  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front_NotTarget", rate = 10, loop = 2, max_missradian = 360  },
   { action_name = "Move_Left_NotTarget", rate = 10, loop = 1, max_missradian = 360  },
   { action_name = "Move_Right_NotTarget", rate = 10, loop = 1, max_missradian = 360  },
   { action_name = "Attack8_LazerCircle", rate = 62, loop = 1, max_missradian = 360  },
   { action_name = "Attack7_LazerFront_NotTarget", rate = 62, loop = 1, max_missradian = 360  },
}
