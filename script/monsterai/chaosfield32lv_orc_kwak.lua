--AiOrc_Black_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Guard_Stand", rate = 3, loop = 1  },
   { action_name = "Attack2_bash", rate = 7, loop = 1, cooltime = 5000  },
   { action_name = "Attack3_Air", rate = 12, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Guard_Stand", rate = 2, loop = 1  },
   { action_name = "Guard_Walk_Left", rate = 2, loop = 1  },
   { action_name = "Guard_Walk_Right", rate = 2, loop = 1  },
   { action_name = "Attack2_bash", rate = 7, loop = 1  },
   { action_name = "Attack3_Air", rate = 9, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Guard_Stand", rate = 2, loop = 1 },
   { action_name = "Guard_Walk_Front", rate = 17, loop = 2 },
   { action_name = "Attack2_bash", rate = 3, loop = 1 },
   { action_name = "Attack9_DashAttack_Kwak", rate = 13, loop = 1, cooltime = 15000, selfhppercent = 75 },
   { action_name = "Assault", rate = 5, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Guard_Stand", rate = 2, loop = 1 },
   { action_name = "Guard_Walk_Front", rate = 6, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 25, loop = 2  },
   { action_name = "Assault", rate = 22, loop = 1  },
}
g_Lua_MeleeDefense = { 
   { action_name = "Attack3_Air", rate = 10, loop = 1  },
   { action_name = "Guard_Walk_Left", rate = 2, loop = 1  },
   { action_name = "Guard_Walk_Right", rate = 2, loop = 1  },
}
g_Lua_RangeDefense = { 
   { action_name = "Assault", rate = 12, loop = 1  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Assault", rate = 12, loop = 1  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_bash", rate = 10, loop = 1, cancellook = 0, approach = 250.0  },
   { action_name = "Attack3_Air", rate = 30, loop = 1, cancellook = 0, approach = 250.0  },
}
