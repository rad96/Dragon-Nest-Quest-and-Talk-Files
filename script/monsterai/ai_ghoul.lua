
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;


g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"Down|Air","!Move"},  
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 12000

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1 },
	 { action_name = "Move_Right",	rate = 2,		loop = 1 },
	 { action_name = "Move_Front",	rate = 2,		loop = 1 },
     { action_name = "Skill_Tumble_Front",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Back",	rate = 10,		loop = 1},
	 { action_name = "Skill_Tumble_Left",	rate = 10,		loop = 1 },
	 { action_name = "Skill_Tumble_Right",	rate = 10,		loop = 1},
	 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1},
}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2},
}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 2 },
	{ action_name = "Move_Right",	rate = 5,		loop = 2 },
	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Left",	rate = 3,		loop = 2 },
	{ action_name = "Move_Right",	rate = 3,		loop = 2 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5 },
}


g_Lua_Skill =
{

 --0 암 어택
	{skill_index=96056,  cooltime = 3000, rangemin = 0, rangemax = 250, target = 3, rate = 30, combo1 = "8,50,1", combo2 = " 3,100,1"},
--1암 슬래쉬
    {skill_index=96057,  cooltime = 5000, rangemin = 0, rangemax = 300, target = 3, rate = 30, combo1 = "8,50,1",combo2 = " 0,100,1"  },
--2 바인딩
	{skill_index=96058, cooltime = 10000, rangemin = 0, rangemax = 500, target = 3, rate = 30, combo1 = "10,50,1", combo2 = "3,80,1", combo3 = "4,100,1" },
--3 번
    {skill_index=96059, cooltime = 3000, rangemin = 0, rangemax = 200, target = 3, rate = -1, combo1 = "0,80,1", combo2 = "8,100,0" },
--4 버닝 런
	{skill_index=96060, cooltime = 20000, rangemin = 0, rangemax = 900, target = 3, rate = 30, combo1 = "0,100,0"},	
--5 랫츠댄스
	{skill_index=96061, cooltime = 30000, rangemin = 200, rangemax = 1200, target = 3, rate = -1,combo1 = " 9,100,0" },	
--6 대쉬 바이트
    {skill_index=96062, cooltime = 10000, rangemin = 0, rangemax = 600, target = 3, rate = 30, combo1 = "8,50,1", combo2 = " 12,100,1" },
--7 자가폭발
	{skill_index=96063, cooltime = 15000, rangemin = 100, rangemax = 1200, target = 3, rate =60, combo1 = " 9,100,0", blowcheck = "144 " },
--8 가스
	{skill_index=96064, cooltime = 3000, rangemin = 0, rangemax = 200, target = 3, rate = 20,combo1 ="2,60,1",combo2 = "0,70,1", combo3 = " 3,100,1" },
--9 점프 물기(하이)
	{skill_index=96065, cooltime = 15000, rangemin = 200, rangemax = 900, target = 3, rate = 40, cancellook = 0, combo1 = "11,60,0", combo2 = " 3,100,0" },
--10 힛다운
	{skill_index=96066, cooltime = 12000, rangemin = 0, rangemax = 500, target = 3, rate = 20, cancellook = 0, combo1 = " 8,60,1", combo2 = " 12,100,0" },	
--11점프 물기
	{skill_index=96067, cooltime = 15000, rangemin = 0, rangemax = 600, target = 3, rate = -1, cancellook = 0, combo1 = " 8,100,0" },
--12 슬라이딩
	{skill_index=96068, cooltime = 5000, rangemin = 0, rangemax = 600, target = 3, rate = -1, cancellook = 0, combo1 = " 3,60,1", combo2 =" 11,80,1", combo3 = " 9,100,0" },
--13 트랩
	{skill_index=96069, cooltime = 10000, rangemin = 0, rangemax = 1200, target = 3, rate = 40, cancellook = 0, combo1 =" 2, 60,0", combo2 = " 0,100,1" },
--14 하이드
	{skill_index=96071, cooltime = 25000, rangemin = 0, rangemax = 1200, target = 3, rate = 20, cancellook = 0, combo1 =" 4,100,1" },
--15 그랜드크로스
	{skill_index=96074, cooltime = 15000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, cancellook = 0, combo1 =" 2,50,0", combo2 =" 1,100,0" },
--16 아이스 웨이브
	{skill_index=96075, cooltime = 25000, rangemin = 100, rangemax = 1200, target = 3, rate = 30, cancellook = 0, combo1 =" 7,100,1" },	
} 
 