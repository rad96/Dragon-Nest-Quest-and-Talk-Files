-- Kali_1stQuest_Hound_Elite AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 100.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 700.0;
g_Lua_NearValue4 = 1400.0;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;    
g_Lua_AssualtTime = 5000;




g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Provoke",	rate = 5,		loop = 1 },
	{ action_name = "Attack1_BiteCombo", rate = 5, loop = 0 },
	{ action_name = "Attack11_SprinkleSands_KaliQuest", rate = 5, loop = 0 },

}


g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Move_Front",	rate = 5,		loop = 3 },
	{ action_name = "Attack9_Fire_KaliQuest_Fail",	rate = 5,	loop = 0,	cancellook = 1 },
	{ action_name = "Attack10_Fire_KaliQuest_Succ",	rate = 2,	loop = 0,	cancellook = 1 },
	{ action_name = "Attack4_Headbutt",	rate = 2,	loop = 0,	cancellook = 1 },

}


g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 10,		loop = 3 },
	{ action_name = "Attack4_Headbutt",	rate = 2,	loop = 0,	cancellook = 1 },
}


g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 30,		loop = 5 },
	{ action_name = "Provoke",	rate = 5,	loop = 0 },
}