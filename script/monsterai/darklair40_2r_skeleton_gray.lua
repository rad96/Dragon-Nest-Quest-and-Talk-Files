--AiSkeleton_Gray_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
   { action_name = "Attack1_Chop", rate = 4, loop = 1  },
   { action_name = "Attack4_Upper", rate = 4, loop = 1  },
   { action_name = "Attack2_Cut", rate = 4, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_Chop", rate = 6, loop = 1  },
   { action_name = "Attack4_Upper", rate = 10, loop = 1  },
   { action_name = "Attack2_Cut", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Attack8_Whirlwind", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 4, loop = 2  },
   { action_name = "Assault", rate = 2, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Chop", rate = 3, loop = 1, cancellook = 0, approach = 150.0  },
   { action_name = "Attack4_Upper", rate = 5, loop = 1, cancellook = 0, approach = 150.0  },
}
