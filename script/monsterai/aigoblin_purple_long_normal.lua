--AiGoblin_Purple_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 5500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 9, loop = 1 },
   { action_name = "Walk_Right", rate = 9, loop = 1 },
   { action_name = "Attack7_Nanmu", rate = 4, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 9, loop = 1 },
   { action_name = "Walk_Front2", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 12,  },
   { action_name = "Assault", rate = 3, loop = 1, max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 6, loop = 1 },
   { action_name = "Walk_Front2", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 19, loop = 1 },
   { action_name = "Assault", rate = 6, loop = 1, max_missradian = 30 },
   { action_name = "Attack6_ThrowBigstone", rate = 4, loop = 1, max_missradian = 30 },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 19, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 19, loop = 1 },
}

g_Lua_Assault = { 
   { action_name = "Attack7_Nanmu", rate = 12, loop = 1, approach = 200, max_missradian = 30 },
   { action_name = "Stand2", rate = 6, loop = 1, approach = 200 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 200 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 200 },
}
