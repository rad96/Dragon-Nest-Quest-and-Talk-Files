--AiKoboldArcher_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 5000;
g_Lua_NearValue4 = 50000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack1", rate = 5, loop = 1  },
   { action_name = "Attack1_Miss1", rate = 5, loop = 1  },
   { action_name = "Attack2_MultiShot", rate = 20, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Attack1", rate = 7, loop = 1  },
   { action_name = "Attack1_Miss1", rate = 7, loop = 1  },
   { action_name = "Attack2_MultiShot", rate = 10, loop = 1  },
   { action_name = "Attack3_ArrowShower", rate = 10, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Move_Front", rate = 20, loop = 2  },
   { action_name = "Attack3_ArrowShower", rate = 25, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 30, loop = 6 },
}
