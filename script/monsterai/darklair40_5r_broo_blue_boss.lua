--AiBroo_Blue_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 1  },
   { action_name = "Move_Right", rate = 4, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Attack3_IceSword_Named", rate = 12, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Walk_Front", rate = 2, loop = 1  },
   { action_name = "Attack3_IceSword_Named", rate = 6, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Walk_Front", rate = 2, loop = 1  },
}
g_Lua_Skill = { 
-- 아이스월
   { skill_index = 20153,  cooltime = 15000, rate = 80,rangemin = 0, rangemax = 400, target = 3, selfhppercent = 80 },
-- 아이스볼트
   { skill_index = 20162,  cooltime = 10000, rate = 50, rangemin = 0, rangemax = 800, target = 3 },
-- 눈보라
   { skill_index = 20155,  cooltime = 15000, rate = 50, rangemin = 0, rangemax = 1000, target = 3 },
}
