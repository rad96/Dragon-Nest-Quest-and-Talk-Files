
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i는 _back을 액트파일내에서 제외시킨다. 케릭터와 겹쳐질시 뒤로 이동하지 못하게..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" },
 State2 = {"Stay|Move|Attack", "!Down","!Air" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"Down|!Move","Air"},
}

g_Lua_CustomAction = {
-- 대쉬어택
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- 좌측덤블링어택
  CustomAction2 = {
     { "Tumble_Left" },
  },
-- 우측덤블링어택
  CustomAction3 = {
     { "Tumble_Right" },
  },
-- 기본 공격 2연타 145프레임
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- 기본 공격 3연타 232프레임
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- 신 브레이커
  CustomAction6 = {
     { "useskill", lua_skill_index = 40, rate = 100 },
  },
  -- 
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 30, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2080
g_Lua_GlobalCoolTime2 = 12800

g_Lua_Near1 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
     { action_name = "Move_Left", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Right", rate = 5, loop = 1, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 12000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 24000, target_condition = "State4" },
--1타
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2타
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3타
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4타
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
     { action_name = "Move_Left", rate = 20, loop = 1 },
     { action_name = "Move_Right", rate = 20, loop = 1 },
	 { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1},
	 { action_name = "CustomAction6", rate = 50, loop = 1, cooltime = 12000, target_condition = "State4" },
     { action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 24000, target_condition = "State4" },
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}

g_Lua_Near3 = 
{ 
     { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1 },
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 5, loop = 1 },
     { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Tumble_Front", rate = 30, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Tumble_Left", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1, target_condition = "State4" },
     { action_name = "Tumble_Right", rate = 40, loop = 1, cooltime = 2600, globalcooltime = 1,  target_condition = "State4"},
     { action_name = "Move_Front", rate = 20, loop = 3 },
     { action_name = "Move_Left", rate = 10, loop = 1 },
     { action_name = "Move_Right", rate = 10, loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1 },
}

g_Lua_Assault = { 
     --{ action_name = "SKill_Dash", rate = -1, loop = 1, approach = 150, globalcooltime = 1 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Tumble_Back", rate = 20, loop = 1, globalcooltime = 1 },	 
     { action_name = "CustomAction2", rate = 10, loop = 1  },
     { action_name = "CustomAction3", rate = 10, loop = 1 },
}

g_Lua_NonDownRangeDamage = {

	 { action_name = "useskill", lua_skill_index = 34, rate = 30 },
	 { action_name = "useskill", lua_skill_index = 3, rate = 50 },
}

g_Lua_Skill = { 
-- 0 스피디 컷
     { skill_index = 85086, cooltime = 4800, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "20,30,1", combo2= " 40,100,0", resetcombo =1},-- 액션프레임끊기 nextcustomaction 시그널
-- 1 더티 트릭
     { skill_index = 85087, cooltime = 8000, rate = 10, rangemin = 0, rangemax = 300, target = 3, combo1 = "2,60,0", combo2 = " 40,100,0", target_condition = "State3", resetcombo =1},
-- 2 더티 트릭 콤보
     { skill_index = 85069, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, combo1 = "32,90,0", combo2 = " 40,100,0", resetcombo =1 },-- 케릭터 어그로유지 LocktargetLook
-- 3 피어싱 스타
     { skill_index = 85003, cooltime = 6400, rate = 10, rangemin = 200, rangemax = 1200, target = 3, combo1 ="25,50,0", combo2 = "28,100,0"},
-- 4 팬 오브 엣지
	 { skill_index = 85004, cooltime = 12000, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "30,70,1", combo2 = "5,70,0", combo3 = " 40,100,0" , resetcombo =1},-- 인풋으로 발동하는 건 액션마다 테이블 다 등록
-- 5 쉐도우 핸드
     { skill_index = 85005, cooltime = 14400, rate = -1, rangemin = 0, rangemax = 1000, target = 3, combo1 = "1,100,0" },

-- 6 트리플 엣지 F
     { skill_index = 85006, cooltime = 9600, rate = -1, rangemin = 0, rangemax = 1200, target = 3},
-- 7 트리플 엣지 B
     { skill_index = 85079, cooltime = 9600, rate = -1, rangemin = 0, rangemax = 1200, target = 3 },
-- 8 트리플 엣지 L
     { skill_index = 85080, cooltime = 9600, rate = -1, rangemin = 0, rangemax = 1200, target = 3},
-- 9 트리플 엣지 R
     { skill_index = 85081, cooltime = 9600, rate = -1, rangemin = 0, rangemax = 1200, target = 3},

-- 10 스프린트
     { skill_index = 85007, cooltime = 4000, rate = -1, rangemin = 0, rangemax = 200, target = 3, combo1 = "18,100,0" },
-- 11페인트
     { skill_index = 85008, cooltime = 192000, rate = -1, rangemin = 0, rangemax = 400, target = 3 },
-- 12 레이드
     { skill_index = 85009, cooltime = 60000, rate = 10, rangemin = 400, rangemax = 1200, target = 3 },
-- 13 고어 킥
     { skill_index = 85010, cooltime = 0, rate = 30, rangemin = 0, rangemax = 100, target = 3, combo1 = "27,50,1", combo2 = "51,100,0"},	  

-- 14 덤블링 F
     { skill_index = 85082, cooltime = 2080, rate = 10, rangemin = 0, rangemax = 400, target = 3, combo1 = "6,30,0", combo2 = "34,40,0" , combo3 = "47,70,0" , combo4 = "40,100,0", resetcombo =1, globalcooltime = 1 },
-- 15 덤블링 B
     { skill_index = 85083, cooltime = 2080, rate = 10, rangemin = 0, rangemax = 400, target = 3, combo1 = "7,30,0", combo2 = "35,40,0" , combo3 = "47,70,0" , combo4 = "40,100,0", resetcombo =1, globalcooltime = 1},
-- 16 덤블링 L
     { skill_index = 85084, cooltime = 2080, rate = 10, rangemin = 0, rangemax = 400, target = 3, combo1 = "8,30,0", combo2 = "36,40,0" , combo3 = "47,70,0" , combo4 = "40,100,0", resetcombo =1, globalcooltime = 1},
-- 17 덤블링 R
     { skill_index = 85085, cooltime = 2080, rate = 10, rangemin = 0, rangemax = 400, target = 3, combo1 = "9,30,0", combo2 = "37,40,0" , combo3 = "47,70,0" , combo4 = "40,100,0", resetcombo =1, globalcooltime = 1},

-- 18 서클 고어
     { skill_index = 85013, cooltime = 7200, rate = -1, rangemin = 0, rangemax = 400, target = 3 },
-- 19 리프
     { skill_index = 85014, cooltime = 2400, rate = -1, rangemin = 0, rangemax = 100, target = 3, blowcheck = "23" },	
-- 20 스모크
     { skill_index = 85015, cooltime = 12800, rate = -1, rangemin = 200, rangemax = 1200, target = 3, combo1 = "49,100,0" },

-- 21 페이크 클록 F
     { skill_index = 85016, cooltime = 10400, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1 },
-- 22 페이크 클록 B
     { skill_index = 85070, cooltime = 10400, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1 },
-- 23 페이크 클록 L
     { skill_index = 85071, cooltime = 10400, rate = -1, rangemin = 0, rangemax = 200, target = 3, cancellook = 1},
-- 24 페이크 클록 R
     { skill_index = 85072, cooltime = 10400, rate = -1, rangemin = 0, rangemax = 700, target = 3, cancellook = 1},

-- 25 오픈 엣지
     { skill_index = 85021, cooltime = 7200, rate = 10, rangemin = 200, rangemax = 1200, target = 3 , combo1 = "28,80,0" , combo2 = " 40,100,0", resetcombo =1},
-- 26 어플러즈
     { skill_index = 85022, cooltime = 10400, rate = 20, rangemin = 0, rangemax = 600, target = 3 , combo1 = "29,60,1", combo2 ="49,70,1", combo3 = "30,100,1", combo4 = " 40,100,0", resetcombo =1},
-- 27 레이크
     { skill_index = 85023, cooltime = 14400, rate = 10, rangemin = 0, rangemax = 900, target = 3, combo1 = "49,50,0" ,combo2 = "30,80,0", combo3 = " 40,100,0", resetcombo =1},
-- 28 레인 드롭
     { skill_index = 85024, cooltime = 16000, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "25,70,0", combo2 = " 49,80,0", combo3 = " 40,100,0" , resetcombo =1},
-- 29 퍼니쉬 먼트
     { skill_index = 85025, cooltime = 17600, rate = 10, rangemin = 0, rangemax = 700, target = 3, combo1 = "51,60,1", comb2 = "49,70,1", combo3 = "28,80,0", combo4 = " 40,100,0", resetcombo =1 },	
	 
-- 30 모탈 블로우
     { skill_index = 85028, cooltime = 19200, rate = 10, rangemin = 0, rangemax = 300, target = 3, combo1 = " 31,100,1", combo2 = " 40,100,0", resetcombo =1},	
-- 31모탈 블로우 쿨타임초기화
     { skill_index = 85077, cooltime = 19200, rate = -1, rangemin = 0, rangemax = 400, target = 3, combo1 =" 51,60,1", combo2 = "44,100,1", combo3 = " 40,100,0" , resetcombo =1},--몬스터는 점프시그널이 안뎀 그래서 벨로시티시그널 써야뎀, 몬스터는 크로스헤어로 타겟잡으면 안뎀

-- 32 리시브 엣지
     { skill_index = 85033, cooltime = 	36000, rate = -1, rangemin = 0, rangemax = 400, target = 3 },
-- 33 페이드
     { skill_index = 85034, cooltime = 24000, rate = 20, rangemin = 150, rangemax = 1200, target = 3 , selfhppercent= 80 },

-- 34 일루전 스텝 F
     { skill_index = 85035, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 800, target = 3},	
-- 35 일루전 스텝 B
     { skill_index = 85073, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 700, target = 3},
-- 36 일루전 스텝 L
     { skill_index = 85074, cooltime =8000, rate = -1, rangemin = 0, rangemax = 300, target = 3},	 
-- 37 일루전 스텝 R
     { skill_index = 85075, cooltime =8000, rate = -1, rangemin = 0, rangemax = 700, target = 3 },	

-- 38 엑세스 체인
     { skill_index = 85036, cooltime = 19200, rate = 20, rangemin = 300, rangemax = 900, target = 3, combo1 = "39,100,0" },	
-- 39 엑세스 체인 콤보
     { skill_index = 85067, cooltime = 19200, rate = -1, rangemin = 0, rangemax = 900, target = 3, combo1 = "51,60,1" , combo2 = "45,100,1", combo3 = " 40,100,0", resetcombo =1},--몬스터는 점프시그널이 안뎀 그래서 벨로시티시그널 써야뎀, 몬스터는 크로스헤어로 타겟잡으면 안뎀

-- 40 버닝 콜
     { skill_index = 85037, cooltime = 16000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 ="28,60,0", combo2 = "49,70,0", combo3 = "27,80,0", combo4 = " 38,100,0" , resetcombo =1},

-- 41 데디케이트 크로우
     { skill_index = 85038, cooltime = 72000, rate = -1, rangemin = 400, rangemax = 1200, target = 3 , selfhppercent= 80, combo1 = " 40,100,0", resetcombo =1},
-- 42 데디케이트 쉐도우
     { skill_index = 85039, cooltime =36000, rate = -1, rangemin = 400, rangemax = 1200, target = 3, selfhppercent= 60, combo1 = " 40,100,0" , resetcombo =1, target_condition = "State4"},	 
-- 43 아소니스트
     { skill_index = 85046, cooltime =24000, rate = 30, rangemin = 300, rangemax = 900, target = 3, combo1 = " 44,60,0", combo2 ="29,70,0", combo3 = " 40,100,0" , resetcombo =1},	 

-- 44 아트플 체이서
     { skill_index = 85047, cooltime =16000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "50,60,1", combo2 = " 29,60,1", combo3 = "15,80,1", combo4 = "30,100,1",combo5 = " 40,100,0" , resetcombo =1},	 
-- 45 쉬프트 블로우EX
     { skill_index = 85048, cooltime =8000, rate = 10, rangemin = 0, rangemax = 300, target = 3 , combo1 = "46,20,1", combo2 = "30,50,1", combo3 = "51,100,1", combo4 = " 40,100,0", resetcombo =1},	 
-- 46 쉬프트 블로우 EX 막타
     { skill_index = 85078, cooltime =8000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "51,100,0" , resetcombo =1},
-- 47 플레임 로커스트 EX
     { skill_index = 85050, cooltime =9600, rate = -1, rangemin = 0, rangemax = 300, target = 3 , combo1 = "48,100,0" },
-- 48 플레임 로커스트 EX 콤보
     { skill_index = 85068, cooltime = 9600, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 ="51,50,1", combo2 = "30,60,1", combo3= "50,100,1", combo4 = " 40,100,0", resetcombo =1},
-- 49 블레이드 러너 EX
     { skill_index = 85051, cooltime = 14400, rate = 10, rangemin = 0, rangemax = 600, target = 3 , combo1 = "44,20,1", combo2 = "51,60,1", combo3 = "45,100,1" , combo4 = " 40,100,0", resetcombo =1},	
-- 50 이즈나 드롭 EX
     { skill_index = 85053, cooltime = 24000, rate = 10, rangemin = 0, rangemax = 300, target = 3, combo1 = "57,10,1", combo2 = "51,40,1", combo3 = " 45,50,1", combo4 = "26,60,0", combo5 = " 49,80,0", combo6 = " 40,100,0", resetcombo =1},	 

-- 51 평타 1
     { skill_index = 85054, cooltime = 0, rate = 100, rangemin = 0, rangemax = 300, target = 3, combo1 = "52,100,0", target_condition = "State3"},
-- 52 평타 2
     { skill_index = 85055, cooltime = 0, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "53,100,1", combo2 = "15,50,0", combo3 = "53,80,0", combo4 = " 40,100,0", resetcombo =1},	 
-- 53 평타 3
     { skill_index = 85056, cooltime = 0, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "54,100,0"},	 
-- 54 평타 4
     { skill_index = 85057, cooltime = 0, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "45,30,1", combo2 = "44,50,1", combo3 = "30,60,1", combo4 = "51,70,1",combo5 = "47,90,0", combo6 = " 40,100,0", resetcombo =1},	 

-- 55 점프
     { skill_index = 85058, cooltime = 20000, rate = 10, rangemin = 0, rangemax = 200, target = 3, combo1 = "10,50,0", combo2 = "55,100,0" },
-- 56 점프 어택
     { skill_index = 85059, cooltime = 0, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "10,100,0" },

-- 57 이즈나 드롭 EX 스탠드
     { skill_index = 85076, cooltime = 24000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "49,70,0", combo2 = "15,80,0", combo3 = " 40,100,0", resetcombo =1},
-- 58 스와이프
     { skill_index = 85011, cooltime = 2400, rate = 50, rangemin = 0, rangemax = 120, target = 3, combo1 = "4,100,1", combo2 = " 40,100,0" , target_condition = "State1", resetcombo =1},


	 }