--AiDarkElf_Red_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 25, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 2  },
   { action_name = "Move_Right", rate = 8, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 2  },
   { action_name = "Attack4_SickleKick", rate = 90, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 6, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 3  },
   { action_name = "Move_Right", rate = 8, loop = 3  },
   { action_name = "Move_Front", rate = 15, loop = 3  },
   { action_name = "Move_Back", rate = 18, loop = 3  },
   { action_name = "Attack7_ThrowingKnife", rate = 90, loop = 3  },
   { action_name = "Assault", rate = 27, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 3, loop = 3  },
   { action_name = "Walk_Back", rate = 3, loop = 3  },
   { action_name = "Move_Left", rate = 14, loop = 3  },
   { action_name = "Move_Right", rate = 14, loop = 3  },
   { action_name = "Move_Front", rate = 24, loop = 3  },
   { action_name = "Move_Back", rate = 20, loop = 2  },
   { action_name = "Attack7_ThrowingKnife", rate = 115, loop = 2  },
   { action_name = "Assault", rate = 30, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 3  },
   { action_name = "Walk_Back", rate = 3, loop = 3  },
   { action_name = "Move_Left", rate = 20, loop = 3  },
   { action_name = "Move_Right", rate = 20, loop = 3  },
   { action_name = "Move_Front", rate = 40, loop = 3  },
   { action_name = "Move_Back", rate = 7, loop = 2  },
   { action_name = "Assault", rate = 180, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack4_SickleKick", rate = 5, loop = 1, approach = 150.0  },
}
g_Lua_Skill = { 
   { skill_index = 20063,  cooltime = 8000, rate = 100,rangemin = 200, rangemax = 1000, target = 3, selfhppercent = 100 },
   { skill_index = 20064,  cooltime = 3000, rate = 100, rangemin = 0, rangemax = 300, target = 3, selfhppercent = 100 },
   { skill_index = 20062,  cooltime = 15000, rate = 100, rangemin = 000, rangemax = 1000, target = 2, rate = 100, debuffcheck=1 },
}
