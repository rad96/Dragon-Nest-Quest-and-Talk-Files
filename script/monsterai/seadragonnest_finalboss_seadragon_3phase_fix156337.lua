

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 900;
g_Lua_NearValue2 = 1300;
g_Lua_NearValue3 = 1800;
g_Lua_NearValue4 = 2500;
g_Lua_NearValue5 = 10000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 1000
g_Lua_AssualtTime = 5000
g_Lua_NoAggroStand = 1
g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_Near1 = 
{ 
-- 0 ~ 1000
-- 페이즈3 : 물리 공격 중심, 턴도 자유롭게
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 2,	selfhppercentrange = "40,100" },
	{ action_name = "Turn_Left",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL", },
	{ action_name = "Turn_Right",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR", },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "40,100", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "40,100", cooltime = 10000 },
	{ action_name = "1Phase_Attack029_GraptoThrow_Right", rate = 12,custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", cooltime = 10000 },
	{ action_name = "1Phase_Attack028_GraptoThrow_Left", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", cooltime = 10000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "62,70", cooltime = 90000 },
	{ action_name = "Fly_Stand",		rate = 30,		custom_state1 = "custom_Fly",		loop = 1,	selfhppercentrange = "0,70" },

-- 페이즈4 : 타겟을 향해 적극적으로 움직인다.
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 1,	selfhppercentrange = "20,40" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate =10,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 5,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "20,40", cooltime = 20000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 5,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", selfhppercentrange = "20,40", cooltime = 20000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "20,50", cooltime = 50000 },
-- 페이즈5 : 스킬 공격을 위해 일반액션 비중이 줄어든다.
	{ action_name = "Stand",		rate = 10,		custom_state1 = "custom_ground",	loop = 1,	selfhppercentrange = "0,20" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 18,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "0,20", cooltime = 20000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", selfhppercentrange = "0,20", cooltime = 20000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,20", cooltime = 40000 },
}

g_Lua_Near2 = 
{ 
-- 1000 ~ 1300
-- 페이즈3 : 거의 움직이지 않은 상태에서 가끔식 물리 공격
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 2,	selfhppercentrange = "40,100" },
	{ action_name = "Turn_Left",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL",  },
	{ action_name = "Turn_Right",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR", },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 5,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "40,100", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 5,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "40,100", cooltime = 10000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "62,70", cooltime = 90000 },
	{ action_name = "Fly_Stand",		rate = 30,		custom_state1 = "custom_Fly",		loop = 1,	selfhppercentrange = "0,70" },
-- 페이즈4 : 타겟을 향해 적극적으로 움직인다.
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 1,	selfhppercentrange = "20,40" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate =10,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 4,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "20,40", cooltime = 20000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 4,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", selfhppercentrange = "20,40", cooltime = 20000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "20,50", cooltime = 50000 },
-- 페이즈5 : 스킬 공격을 위해 일반액션 비중이 줄어든다.
	{ action_name = "5Phase_Attack029_TailStrike", rate = 16,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "Stand",		rate = 10,		custom_state1 = "custom_ground",	loop = 1,	selfhppercentrange = "0,20" },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 5,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "0,20", cooltime = 20000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 5,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", selfhppercentrange = "0,20", cooltime = 20000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,20", cooltime = 40000 },
}

g_Lua_Near3 = 
{ 
-- 1400 ~ 1800
-- 페이즈3 : 거의 움직이지 않은 상태에서 가끔식 물리 공격
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 2,},
	{ action_name = "Turn_Left",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL", selfhppercentrange = "40,100" },
	{ action_name = "Turn_Right",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR", selfhppercentrange = "40,100" },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", cooltime = 10000 },
	{ action_name = "3Phase_Attack013_StampWithRH",	rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "3Phase_Attack014_StampWithLH",	rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "20,100", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8,		custom_state1 = "custom_ground",	loop = 1,	td = "LF", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "62,70", cooltime = 90000 },
	{ action_name = "Fly_Stand",		rate = 30,		custom_state1 = "custom_Fly",		loop = 1,	selfhppercentrange = "0,70" },
-- 페이즈4 : 타겟을 향해 적극적으로 움직인다.
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 1,	selfhppercentrange = "20,40" },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 5,		custom_state1 = "custom_ground",	loop = 1,	td = "LF", selfhppercentrange = "20,40", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 5,	custom_state1 = "custom_ground",	loop = 1,	td = "RF", selfhppercentrange = "20,40", cooltime = 20000 },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "20,50", cooltime = 50000 },
-- 페이즈5 : 스킬 공격을 위해 일반액션 비중이 줄어든다.
	{ action_name = "5Phase_Attack029_TailStrike", rate = 22,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "Stand",		rate = 10,		custom_state1 = "custom_ground",	loop = 1,	selfhppercentrange = "0,20" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "0,20", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "LF", selfhppercentrange = "0,20", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "RF", selfhppercentrange = "0,20", cooltime = 20000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,20", cooltime = 40000 },
}

g_Lua_Near4 = 
{ 
-- 1800 ~ 2500
-- 페이즈3 : 거의 움직이지 않은 상태에서 가끔식 물리 공격
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 2,	selfhppercentrange = "40,100" },
	{ action_name = "Turn_Left",		rate = 30,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL", selfhppercentrange = "40,100" },
	{ action_name = "Turn_Right",		rate = 30,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR", selfhppercentrange = "40,100" },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "40,100", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "40,100", cooltime = 10000 },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,LF", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "62,70", cooltime = 90000 },
	{ action_name = "Fly_Stand",		rate = 30,		custom_state1 = "custom_Fly",		loop = 1,	selfhppercentrange = "0,70" },
-- 페이즈4 : 타겟을 향해 적극적으로 움직인다.
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 1,	selfhppercentrange = "20,40" },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 20,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate =12,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "20,40", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FL,LF", selfhppercentrange = "0,40", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "0,40", cooltime = 20000 },
	{ action_name = "Turn_Left",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR", selfhppercentrange = "0,40" },
	{ action_name = "Turn_Right",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL", selfhppercentrange = "0,40" },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "20,50", cooltime = 50000 },
-- 페이즈5 : 스킬 공격을 위해 일반액션 비중이 줄어든다.
	{ action_name = "5Phase_Attack029_TailStrike", rate = 30,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "Stand",		rate = 10,		custom_state1 = "custom_ground",	loop = 1,	selfhppercentrange = "0,20" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 30,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "0,20", cooltime = 20000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,20", cooltime = 40000 },
}

g_Lua_Near5 = 
{ 
-- 2500 ~ 3000
-- 페이즈3 : 거의 움직이지 않은 상태에서 가끔식 물리 공격
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 1,	selfhppercentrange = "40,100" },
	{ action_name = "Turn_Left",		rate = 30,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR", selfhppercentrange = "40,100" },
	{ action_name = "Turn_Right",		rate = 30,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL", selfhppercentrange = "40,100" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,LF", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "40,100", cooltime = 20000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "40,100", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "40,100", cooltime = 10000 },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "62,70", cooltime = 90000 },
	{ action_name = "Fly_Stand",		rate = 30,		custom_state1 = "custom_Fly",		loop = 1,	selfhppercentrange = "0,70" },
-- 페이즈4 : 타겟을 향해 적극적으로 움직인다.
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate =12,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 1,	selfhppercentrange = "20,100" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 30,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "20,40", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FL,LF", selfhppercentrange = "0,40", cooltime = 20000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "0,40", cooltime = 20000 },
	{ action_name = "5Phase_Attack029_TailStrike", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "20,40", cooltime = 10000 },
	{ action_name = "Turn_Left",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "RF,RB,BR", selfhppercentrange = "0,40" },
	{ action_name = "Turn_Right",		rate = 10,		custom_state1 = "custom_ground",	loop = -1,	td = "LF,LB,BL", selfhppercentrange = "0,40" },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "20,50", cooltime = 50000 },
-- 페이즈5 : 스킬 공격을 위해 일반액션 비중이 줄어든다.
	{ action_name = "5Phase_Attack029_TailStrike", rate = 16,	custom_state1 = "custom_ground",	loop = 1,	td = "LB,BL,BR,RB", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "3Phase_Attack015_TailAttack_Left", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "BL,LB,LF", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "3Phase_Attack016_TailAttack_Right", rate = 15,	custom_state1 = "custom_ground",	loop = 1,	td = "RF,RB,BR", selfhppercentrange = "0,20", cooltime = 10000 },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 20,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "0,20", cooltime = 20000 },
	{ action_name = "Stand",		rate = 10,		custom_state1 = "custom_ground",	loop = 1,	selfhppercentrange = "0,20" },
	{ action_name = "Fly",			rate = 10,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,20", cooltime = 40000 },
}


g_Lua_BeHitSkill = { 
   { lua_skill_index = 1, rate = 100, skill_index = 30587  },
}

g_Lua_Skill = { 
-- _________조건스킬모음________
-- 0. 드래곤브레스4
   { skill_index = 30601,  cooltime = 1000, rate = -1, custom_state1 = "custom_fly",	rangemin = 0, rangemax = 10000,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- 1. 꼬리공격_Loop
   { skill_index = 30588,  cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 4000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- 2. 드래곤브레스1
   { skill_index = 30592,  cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- 3. 착륙
   { skill_index = 30602,  cooltime = 1000, rate = -1, custom_state1 = "custom_fly",	rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- 4. 꼬리공격_Start
   { skill_index = 30587,  cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 2000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- 5. 얼음감옥
   { skill_index = 30591,  cooltime = 1000, rate = -1, custom_state1 = "custom_ground", rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- 6. 드래곤브레스3-->6
   { skill_index = 30600,  cooltime = 1000, rate = -1, custom_state1 = "custom_fly",	rangemin = 0, rangemax = 10000,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = "1,1,0,1", next_lua_skill_index = 0, blowcheck = "100" },
-- ___________페이즈3.0__________
-- 아이스볼트2
   { skill_index = 30598,  cooltime = 18000, rate = 70, custom_state1 = "custom_ground", rangemin = 0, rangemax = 1000,	target = 3, selfhppercentrange = "50,75", td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = "1,4,0,1" },
-- 드래곤브레스4
   { skill_index = 30601,  cooltime = 1000, rate = 100, custom_state1 = "custom_fly",	 rangemin = 0, rangemax = 10000,target = 3, selfhppercentrange = "50,75", td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- 소용돌이 소환
   { skill_index = 30606,  cooltime = 30000, rate = 90, custom_state1 = "custom_ground", rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "50,75", td = "FL,FR,LF,RF", next_lua_skill_index = 4 },
-- 날개공격
   { skill_index = 30589,  cooltime = 90000, rate = 30, custom_state1 = "custom_ground", rangemin = 0, rangemax = 4000,	target = 3, selfhppercentrange = "50,75", td = "FL,FR,LF,RF,RB,BR,BL,LB", next_lua_skill_index = 5 },
-- 포효(페이즈 3.75)
   { skill_index = 30576,  cooltime = 90000, rate = 75,	custom_state1 = "custom_ground", rangemin = 0, rangemax = 3000,	target = 3, selfhppercentrange = "50,75", td = "FL,FR,LF,RF,RB,BR,BL,LB", next_lua_skill_index = 2 },
-- ___________페이즈4.0__________
-- 드래곤브레스4
   { skill_index = 30601,  cooltime = 1000, rate = 100, custom_state1 = "custom_fly",	 rangemin = 0, rangemax = 10000,target = 3, selfhppercentrange = "30,50", td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- 포효
   { skill_index = 30576,  cooltime = 60000, rate = 75,	custom_state1 = "custom_ground", rangemin = 0, rangemax = 3000,	target = 3, selfhppercentrange = "30,50", td = "FL,FR,LF,RF,RB,BR,BL,LB", next_lua_skill_index = 2, limitcount = 1   },
-- 소용돌이 소환
   { skill_index = 30606,  cooltime = 50000, rate = 90, custom_state1 = "custom_ground", rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "30,50", td = "FL,FR,LF,RF", next_lua_skill_index = 4  },
-- 아이스볼트2
   { skill_index = 30598,  cooltime = 30000, rate = 60, custom_state1 = "custom_ground", rangemin = 0, rangemax = 4000, target = 3, selfhppercentrange = "30,50", td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = "1,5,0,1" },
-- 날개공격
   { skill_index = 30589,  cooltime = 120000, rate = 60, custom_state1 = "custom_ground", rangemin = 0, rangemax = 4000,	target = 3, selfhppercentrange = "30,50", td = "FL,FR,LF,RF,RB,BR,BL,LB", next_lua_skill_index = 5  },
-- 비늘 공격
   { skill_index = 30593,  cooltime = 1000, rate = 90,	custom_state1 = "custom_fly",	 rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "20,50", td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = "1,1,0,1", next_lua_skill_index = 3 },
-- 포효
   { skill_index = 30576,  cooltime = 80000, rate = 50,	custom_state1 = "custom_ground", rangemin = 0, rangemax = 3000,	target = 3, selfhppercentrange = "30,50", td = "FL,FR,LF,RF,RB,BR,BL,LB", next_lua_skill_index = 2, limitcount = 1  },
-- ___________페이즈5.0__________
-- 드래곤브레스3록온
   { skill_index = 30586,  cooltime = 1000, rate = 70,	custom_state1 = "custom_fly",	 rangemin = 0, rangemax = 10000,target = 3, selfhppercentrange = "0,30", td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = "1,1,0,1", next_lua_skill_index = 6 },
-- 좌측 브레스
   { skill_index = 30604,  cooltime = 40000, rate = 45, custom_state1 = "custom_ground", rangemin = 0, rangemax = 1300,	target = 3, selfhppercentrange = "0,30", td = "LF,BL,LB" },
-- 우측 브레스
   { skill_index = 30605,  cooltime = 40000, rate = 45, custom_state1 = "custom_ground", rangemin = 0, rangemax = 1300,	target = 3, selfhppercentrange = "0,30", td = "RF,RB,BR" },
-- 날개공격
   { skill_index = 30589,  cooltime = 90000, rate = 60, custom_state1 = "custom_ground", rangemin = 0, rangemax = 4000,	target = 3, selfhppercentrange = "0,30", td = "FL,FR,LF,RF,RB,BR,BL,LB", next_lua_skill_index = 5 },
-- 포효
   { skill_index = 30576,  cooltime = 40000, rate = 70,	custom_state1 = "custom_ground", rangemin = 0, rangemax = 3000,	target = 3, selfhppercentrange = "0,30", td = "FL,FR,LF,RF,RB,BR,BL,LB", next_lua_skill_index = 2, limitcount = 1  },
-- 소용돌이 소환
   { skill_index = 30606,  cooltime = 40000, rate = 90, custom_state1 = "custom_ground", rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "0,30", td = "FL,FR,LF,RF", next_lua_skill_index = 4, limitcount = 1 },
-- 아이스볼트2
   { skill_index = 30598,  cooltime = 20000, rate = 70, custom_state1 = "custom_ground", rangemin = 0, rangemax = 4000, target = 3, selfhppercentrange = "0,30", td = "FL,FR,LF,RF,RB,BR,BL,LB", multipletarget = "1,6,0,1" },
}