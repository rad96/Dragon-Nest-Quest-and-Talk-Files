--AiGoblin_Blue_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand2", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
   { action_name = "Attack1_Slash", rate = 29, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
   { action_name = "Walk_Right", rate = 1, loop = 1  },
   { action_name = "Attack1_Slash", rate = 8, loop = 1  },
   { action_name = "Attack2_Upper", rate = 8, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 8, loop = 1  },
   { action_name = "Move_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2  },
}

g_Lua_Assault = { 
   { action_name = "Attack1_Slash", rate = 5, loop = 1, cancellook = 0, approach = 250.0  },
   { action_name = "Attack2_Upper", rate = 5, loop = 1, cancellook = 1, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 20003,  cooltime = 6000, rate = 50, rangemin = 300, rangemax = 1000, target = 3 },
}
