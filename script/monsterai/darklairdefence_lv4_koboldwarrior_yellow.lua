--DarkLairDefence_Lv4_KoboldWarrior_Yellow.lua
--코볼트 아쳐가 함께 나오기 때문에 유저들이 아쳐에게 근접하지 못 하도록 최대한 근접하도록
--기본타입보다는 조금 더 대쉬타입에 가깝게
--근접한 후 좌우 이동과 뒤로 빠지는게 많도록, 양손단검을 쓰기 때문에 재빠르다는 느낌을 줄 수 있어야 한다

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 650;
g_Lua_NearValue4 = 1050;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Move_Left", 2 },
      { "Attack2_Sever", 1 },
  },
  CustomAction2 = {
      { "Move_Right", 2 },
      { "Attack2_Sever", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "CustomAction1", rate = 3, loop = 1, target_condition = "State1"  },
   { action_name = "CustomAction2", rate = 3, loop = 1, target_condition = "State1"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Back", rate = 4, loop = 2  },
   { action_name = "Move_Back", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_Chop", rate = 3, loop = 1  },
   { action_name = "Attack2_Sever", rate = 3, loop = 1  },
   { action_name = "CustomAction1", rate = 1, loop = 1, target_condition = "State1"  },
   { action_name = "CustomAction2", rate = 1, loop = 1, target_condition = "State1"  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 1  },
   { action_name = "Assault", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1  },
   { action_name = "Walk_Right", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Sever", rate = 4, loop = 1, cancellook = 1, approach = 150.0  },
   { action_name = "Attack1_Chop", rate = 5, loop = 1, cancellook = 1, approach = 100.0  },
}