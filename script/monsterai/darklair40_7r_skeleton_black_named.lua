--AiSkeleton_Black_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack2_Cut", rate = 23, loop = 1  },
   { action_name = "Attack1_Chop", rate = 23, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 20, loop = 3  },
   { action_name = "Walk_Right", rate = 20, loop = 3  },
   { action_name = "Attack1_Chop", rate = 20, loop = 1  },
   { action_name = "Attack10_UpperBash", rate = 37, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 3  },
   { action_name = "Walk_Right", rate = 5, loop = 3  },
   { action_name = "Walk_Front", rate = 20, loop = 1  },
   { action_name = "Attack10_UpperBash", rate = 10, loop = 1  },
   { action_name = "Assault", rate = 15, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 5, loop = 3  },
   { action_name = "Move_Right", rate = 5, loop = 3  },
   { action_name = "Move_Front", rate = 15, loop = 3  },
   { action_name = "Assault", rate = 35, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 30, loop = 3  },
   { action_name = "Move_Left", rate = 5, loop = 3  },
   { action_name = "Move_Right", rate = 5, loop = 3  },
   { action_name = "Move_Front", rate = 35, loop = 3  },
   { action_name = "Assault", rate = 60, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Chop", rate = 5, loop = 1, approach = 150.0  },
   { action_name = "Attack10_UpperBash", rate = 5, loop = 1, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 20053,  cooltime = 10000, rate = 100,rangemin = 300, rangemax = 700, target = 3, selfhppercent = 100 },
}