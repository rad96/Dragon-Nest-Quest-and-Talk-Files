g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300 --몬스터가 돌격시에 타겟에게 다가가는 최소거리
g_Lua_AssualtTime = 1000 --Assault 명령을 받고 돌격 할 때 타겟을 쫓아가는 시간. 


g_Lua_CustomAction = {
  CustomAction1 = { 
      { "Walk_Left", 0 },
      { "Attack1_Bite", 0 },
  },
  
  CustomAction2 = {
      { "Walk_Right", 0 },
      { "Attack1_Bite", 0 },
  },
 
 }


g_Lua_Near1 = { --옆으로 이동하고 물거나 그냥 물거나
   { action_name = "Stand", rate = 8, loop = 3  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
}

g_Lua_Near2 = { --돌격
   { action_name = "Stand", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
}

g_Lua_Near3 = { --너무 멀면 대기
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 1, loop = 1  },
}


g_Lua_Skill = 
{ 
   { skill_index = 36807,  cooltime = 2000, rate = 1, rangemin = 0, rangemax = 3000, target = 1 }, 
   { skill_index = 36808,  cooltime = 2000, rate = 1, rangemin = 0, rangemax = 3000, target = 1 }, 
}
