--AiHalfGolem_Red_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop=1 },
   { action_name = "Walk_Left", rate = 5, loop=1 },
   { action_name = "Walk_Right", rate = 5, loop=1 },
   { action_name = "Attack1_WavePunch", rate = 13, loop=1, cooltime = 10000 },
   { action_name = "Attack2_Blow", rate = 27, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop=1 },
   { action_name = "Walk_Front", rate = 15, loop=1 },
   { action_name = "Walk_Left", rate = 10, loop=1 },
   { action_name = "Walk_Right", rate = 10, loop=1 },
   { action_name = "Move_Front", rate = 7, loop=1 },
   { action_name = "Move_Left", rate = 5, loop=1 },
   { action_name = "Move_Right", rate = 5, loop=1 },
}
g_Lua_Near1 = { 
   { action_name = "Attack1_WavePunch", rate = 87, loop=1, cooltime = 10000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop=1 },
   { action_name = "Walk_Front", rate = 20, loop=1 },
   { action_name = "Walk_Left", rate = 8, loop=2 },
   { action_name = "Walk_Right", rate = 8, loop=1 },
   { action_name = "Move_Front", rate = 7, loop=1 },
   { action_name = "Attack1_WavePunch", rate = 87, loop=1, cooltime = 10000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop=4 },
   { action_name = "Walk_Front", rate = 20, loop=4 },
   { action_name = "Move_Front", rate = 20, loop=4 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5, loop=1 },
   { action_name = "Walk_Front", rate = 15, loop=2 },
   { action_name = "Move_Front", rate = 20, loop=2 },
}
g_Lua_Skill = { 
   { skill_index = 32236,  cooltime = 32000, rate = 160,rangemin = 0, rangemax = 2000, target = 3, limitcount = 3 },
}
