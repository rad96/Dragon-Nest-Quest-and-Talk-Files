--ArcBishop_Green_Boss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 15, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Stand_2", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Stand_2", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 1  },
}
g_Lua_Skill = { 
-- 포이즌 버스트 -
   { skill_index = 30937,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 25, blowcheck = "44" },
-- 서먼 리치 -
   { skill_index = 30939,  cooltime = 40000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 25 },

-- 멀티 포이즌 볼 -
   { skill_index = 30936,  cooltime = 30000, rate = 80, rangemin = 0, rangemax = 2000, target = 3, selfhppercent = 50 },
-- 서먼 좀비 -
   { skill_index = 30938,  cooltime = 40000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "1,50" },

-- 포이즌 웨이브 - 
   { skill_index = 30933,  cooltime = 15000, rate = 80, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 75 },
-- 호밍 미사일 - 
   { skill_index = 30932,  cooltime = 25000, rate = 50, rangemin = 300, rangemax = 2000, target = 3, selfhppercent = 75, multipletarget = "1" },

-- 포이즌 스프링 -
   { skill_index = 30934,  cooltime = 20000, rate = 70, rangemin = 300, rangemax = 2000, target = 3, selfhppercent = 100 },
-- 포이즌 레인 -
   { skill_index = 30935,  cooltime = 25000, rate = 70, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100 },
-- 포이즌 블로우 -
   { skill_index = 30931,  cooltime = 7000, rate = 80,rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100 },
}
