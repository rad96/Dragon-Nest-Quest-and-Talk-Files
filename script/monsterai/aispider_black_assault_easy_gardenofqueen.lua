-- AiSpider_Black_Assault_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Attack2_Bite", rate = 2, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 1  },
   { action_name = "Walk_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Front", rate = 6, loop = 1  },
   { action_name = "Move_Front", rate = 2, loop = 1  },
   { action_name = "Attack3_DashBite", rate = 2, loop = 1, max_missradian = 30  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 4, loop = 1  },
   { action_name = "Move_Right", rate = 4, loop = 1  },
   { action_name = "Walk_Front", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Attack5_JumpAttack", rate = 2, loop = 1, max_missradian = 30  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 8, loop = 4  },
}
g_Lua_Skill = { 
   { skill_index = 20094,  cooltime = 1000, rate = 100,rangemin = 0, rangemax = 5000, target = 1, limitcount = 1 },
}