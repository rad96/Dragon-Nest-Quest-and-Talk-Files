--AiMinotauros_Red_Boss_Nest.lua

g_Lua_NearTableCount = 5
g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1100;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2100;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 400;
g_Lua_AssualtTime = 5000;

g_Lua_GlobalCoolTime1 = 33000

g_Lua_CustomAction =
{
   CustomAction1 = 
   {{ "Attack005_Upper", 0 },{ "Attack004_SwingFire_Enchant", 0 },},
   CustomAction2 =
   {{ "Attack003_JumpAttack", 0 },{ "Attack017_Chop", 0 },},
}

g_Lua_Near1 = 
{ 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 9, loop = 1 },
   { action_name = "Walk_Right", rate = 9, loop = 1 },
   { action_name = "Attack1_bash", rate = 5, loop = 1 },
   -- { action_name = "CustomAction1", rate = 23, loop = 1 ,cooltime=20000,notusedskill=33263},
   -- { action_name = "Attack004_TurningSlash", rate = 8, loop = 3 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
   { action_name = "Attack003_JumpAttack", rate = 6, loop = 1 },
   -- { action_name = "Attack004_SwingFire_Enchant", rate = 15, loop = 1,usedskill="33262" },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Attack003_JumpAttack", rate = 7, loop = 1 },
   -- { action_name = "CustomAction1", rate = 15, loop = 1 },
   -- { action_name = "Attack004_SwingFire", rate = 15, loop = 1 ,usedskill="33262"},
   -- { action_name = "Attack004_SwingFire_Enchant", rate = 15, loop = 1,usedskill="33262" },
   -- { action_name = "Attack004_TurningSlash", rate = 8, loop = 3 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   -- { action_name = "Attack004_SwingFire_Enchant", rate = 3, loop = 1,usedskill="33262" },
   -- { action_name = "Attack004_TurningSlash", rate = 8, loop = 3 },
}
g_Lua_Near5 = 
{ 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 40, loop = 1 },
   -- { action_name = "CustomAction1", rate = 15, loop = 1 ,notusedskill="33263"},
   -- { action_name = "Attack004_TurningSlash", rate = 8, loop = 3 },
}

g_Lua_Skill = { 
   { skill_index = 33258, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 1300, target = 3 ,next_lua_skill_index=1},--0 무브프론트
   { skill_index = 33309, cooltime = 3000 ,rate = -1, rangemin = 100, rangemax = 1300, target = 3 },--1 비열한공격
   { skill_index = 33260, cooltime = 120000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, next_lua_skill_index=0, globalcooltime=1,},--2 대점프
   { skill_index = 33308, cooltime = 100000, rate = 100, rangemin = 0, rangemax = 3000, target = 3,usedskill=33254,multipletarget = "1,8"},--3 메테오
   { skill_index = 33251, cooltime = 9000, rate = 40, rangemin = 0, rangemax = 1500, target = 3, encountertime=10000, notusedskill=33254 ,},   --4 플레임대쉬
   { skill_index = 33252, cooltime = 9000, rate = 40, rangemin = 0, rangemax = 1500, target = 3, usedskill=33254},--5 플레임대쉬강화
   { skill_index = 33253, cooltime = 34000, rate = 55, rangemin = 0, rangemax = 1000, target = 3,next_lua_skill_index=6 },--6 파이어브레스
   { skill_index = 33255, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, },   --7 찍기강화
   { skill_index = 33264, cooltime = 40000 ,rate = 40, rangemin = 0, rangemax = 400, target = 3,next_lua_skill_index=9 },--8 어퍼
   { skill_index = 33265, cooltime = 1000 ,rate = -1, rangemin = 0, rangemax = 1300, target = 3 },--9 스윙파이어인챈트
   { skill_index = 33261, cooltime = 60000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, usedskill=33254, multipletarget = "1,4",},   --9 볼케이노
   { skill_index = 33254, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 1,selfhppercentrange = "0,60",notsedskill=33254, limitcount=1 }, --10 이프리트
   { skill_index = 33257, cooltime = 120000, rate = 90, rangemin = 0, rangemax = 1500, target = 3,usedskill=33254,encountertime=10000 },      --11 이그니션
   }