--AiClawwolf_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 1500;
g_Lua_NearValue2 = 3000;



g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
}
   
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 34620,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 1, limitcount = 1 }, -- 시작시 버프
   { skill_index = 34621,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3 , randomtarget = "1.6,0,1" }, -- 스나이핑  샷
}
