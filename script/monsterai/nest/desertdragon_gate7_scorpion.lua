--AiSpittler_Green_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 4500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 30000
g_Lua_GlobalCoolTime2 = 50000
g_Lua_GlobalCoolTime3 = 50000
g_Lua_GlobalCoolTime4 = 50000
g_Lua_GlobalCoolTime5 = 35000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 4, loop = 1, td = "FL,FR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2 , td = "FL,FR"},
   { action_name = "Walk_Left", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Move_Front", rate = 4, loop = 1, td = "FL,FR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2, td = "FL,FR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3, td = "FL,FR" },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 4, td = "FL,FR" },
}

g_Lua_Skill = { 
-- 대포, 31979  대포 돌진 10미터용
   { skill_index = 31966, cooltime = 25000, rate = 70, rangemin = 500, rangemax = 1200, target = 3, td = "FL,FR", priority = 5 },
-- 슬래쉬
   { skill_index = 31980, cooltime = 6000, rate = 60, rangemin = 0, rangemax = 500, target = 3, td = "FL,FR", priority = 10 },
-- 콤보
   { skill_index = 31974, cooltime = 15000, rate = 80, rangemin = 0, rangemax = 500, target = 3, td = "FL,FR", priority = 5 },
-- 꼬리치기
   { skill_index = 31968, cooltime = 10000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, td = "BL,BR", randomtarget = 1, priority = 12 },
-- 회전
   { skill_index = 31982, cooltime = 30000, globalcooltime = 6, rate = 50, rangemin = 0, rangemax = 2000, target = 3, multipletarget = "1,8,0,1", priority = 9 },
}
