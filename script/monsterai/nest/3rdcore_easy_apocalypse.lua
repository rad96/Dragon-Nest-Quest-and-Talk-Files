-- Apocalypse Normal A.I
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 600.0;
g_Lua_NearValue2 = 1100.0;
g_Lua_NearValue3 = 1600.0;
g_Lua_NearValue4 = 2100.0;
g_Lua_NearValue5 = 3100.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 600;
g_Lua_AssualtTime = 5000;

g_Lua_Near1 = 
{ 
	{ action_name = "Stand_4", rate = 4, loop = 1 },
	{ action_name = "Walk_Left", rate = 4, loop = 2, td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Right", rate = 4, loop = 2, td = "FL,FR,LF,RF" },
	{ action_name = "Attack018_Bite_NestBossRush", rate = 15, loop = 1, td = "FL,FR", selfhppercent = 80 },
	{ action_name = "Attack001_ClawMelee_NestBossRush", rate = 15, loop = 1, td = "FL,FR" },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand_4", rate = 4, loop = 1 },
	{ action_name = "Move_Front", rate = 8, loop = 1, td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Left", rate = 2, loop = 2, td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Right", rate = 2, loop = 2, td = "FL,FR,LF,RF" },
	{ action_name = "Attack002_ClawRange1", rate = 8, loop = 1, td = "FL,FR" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_4", rate = 4, loop = 1, td = "FL,RF" },
	{ action_name = "Move_Front", rate = 10, loop = 2, td = "FL,FR,LF,RF" },
	{ action_name = "Assault", rate = 12, loop = 1, td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Left", rate = 2, loop = 1, td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Right", rate = 2, loop = 1, td = "FL,FR,LF,RF" },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_4", rate = 4, loop = 1, td = "FL,RF" },
	{ action_name = "Move_Front", rate = 10, loop = 3, td = "FL,FR,LF,RF" },
	{ action_name = "Assault", rate = 12, loop = 1, td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Left", rate = 2, loop = 1, td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Right", rate = 2, loop = 1, td = "FL,FR,LF,RF" },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand_4", rate = 4, loop = 1, td = "FL,RF" },
	{ action_name = "Move_Front", rate = 10, loop = 5, td = "FL,FR,LF,RF" },
	{ action_name = "Assault", rate = 16, loop = 1, td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Left", rate = 2, loop = 1, td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Right", rate = 2, loop = 1, td = "FL,FR,LF,RF" },
}

g_Lua_Assault = { 
   { action_name = "Attack001_ClawMelee_NestBossRush", rate = 4, loop = 1, approach = 600 },
   { action_name = "Attack018_Bite_NestBossRush", rate = 12, loop = 1, approach = 600, selfhppercent = 80 },
}

g_Lua_SkillProcessor = {
   { skill_index = 21343, changetarget = "3000,1" },
}

g_Lua_RandomSkill ={
{
{
   { skill_index = 21356, rate = 50, next_lua_skill_index = 0 }, 
   { skill_index = 21357, rate = 50, next_lua_skill_index = 1 },
},
{
   { skill_index = 21355, rate = 50 }, 
   { skill_index = 21358, rate = 50 },
},
}
}

g_Lua_GlobalCoolTime1 = 22000
g_Lua_GlobalCoolTime2 = 13000

g_Lua_Skill = { 
-- 레이저1 Attack006_Laser1
   { skill_index = 21344, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 8000, target = 3 },
   { skill_index = 21345, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 8000, target = 3 },

-- 레이저 레디
   { skill_index = 21356, skill_random_index = 1, cooltime = 1000, globalcooltime = 2, rate = 100, priority = 50, rangemin = 0, rangemax = 8000, target = 3, selfhppercent = 80, limitcount = 1 },
   { skill_index = 21356, skill_random_index = 1, cooltime = 1000, globalcooltime = 2, rate = 100, priority = 50, rangemin = 0, rangemax = 8000, target = 3, selfhppercent = 60, limitcount = 1 },
   { skill_index = 21356, skill_random_index = 1, cooltime = 1000, globalcooltime = 2, rate = 100, priority = 50, rangemin = 0, rangemax = 8000, target = 3, selfhppercent = 40, limitcount = 1 },
   { skill_index = 21356, skill_random_index = 1, cooltime = 1000, globalcooltime = 2, rate = 100, priority = 50, rangemin = 0, rangemax = 8000, target = 3, selfhppercent = 20, limitcount = 1 },


-- 플래어 Attack015_Flare_Start
-- { skill_index = 21347, cooltime = 1000, rate = 100, priority = 50, rangemin = 0, rangemax = 7500, target = 3, limitcount = 1, encountertime = 480000 },

-- 샤우트 Attack005_Shout_NestBossRush
   { skill_index = 21341, cooltime = 50000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 6000, target = 3 },
-- 스트레치 아웃 Attack004_StretchOut
   { skill_index = 21342, cooltime = 18000, rate = 70, rangemin = 0, rangemax = 1200, target = 3 },

-- 어설트
   { skill_index = 21354, cooltime = 25000, rate = 50, rangemin = 0, rangemax = 1200, target = 3, randomtarget = "1.6,0,1", td = "FL,FR" },
-- 점프 Attack020_Jump_NestBossRush
   { skill_index = 21355, skill_random_index = 2, cooltime = 32000, rate = 70, rangemin = 0, rangemax = 1200, target = 3, randomtarget = "1.6,0,1", td = "FL,FR" },

-- 이베포레이션1 Attack008_Evaporation1_NestBossRush
   { skill_index = 21350, cooltime = 1000, globalcooltime = 1, rate = 90, rangemin = 0, rangemax = 6000, target = 3, selfhppercentrange = "80,100", multipletarget = "1,4,0,1" },
   { skill_index = 21351, cooltime = 1000, globalcooltime = 1, rate = 90, rangemin = 0, rangemax = 6000, target = 3, selfhppercentrange = "60,80", multipletarget = "1,4,0,1" },
   { skill_index = 21352, cooltime = 1000, globalcooltime = 1, rate = 90, rangemin = 0, rangemax = 6000, target = 3, selfhppercentrange = "40,60", multipletarget = "1,4,0,1" },

-- 크로우세퍼릿 Attack011_ClawSeperate(Hell)
   { skill_index = 21349, cooltime = 35000, rate = 90, rangemin = 0, rangemax = 4500, target = 3, selfhppercent = 38 },

-- 훨윈드 Attack009_Whirlwind_Start
   { skill_index = 21343, cooltime = 45000, rate = 70, rangemin = 600, rangemax = 1500,target = 3, randomtarget = "1.6,0,1", selfhppercent = 58 },

-- 아포칼립스 Attack012_Apocalypse_Start(Hell)
   { skill_index = 21353, cooltime = 1000, globalcooltime = 1, rate = 90, rangemin = 0, rangemax = 6000, target = 3, selfhppercentrange = "0,40", multipletarget = "1,4,0,1" },
}