g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 10000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 3000



g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 8, loop = 1, selfhppercentrange = "50,100" },
   { action_name = "Stand", rate = 8, loop = 1, selfhppercentrange = "0,50" },
   { action_name = "Walk_Left", rate = 3, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Right", rate = 3, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 10, loop = 1, td = "FR,FL"  },
   { action_name = "Move_Front", rate = 10, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1, td = "FR,FL" },
   { action_name = "Move_Front", rate = 12, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 10, loop = 1, td = "FR,FL"  },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2, td = "FR,FL" },
   { action_name = "Assault", rate = 10, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}

g_Lua_Assault = { 
   { lua_skill_index = 4, rate = 16, approach = 400 },
}

g_Lua_GlobalCoolTime1 = 6000

g_Lua_Skill = {
-- 블리자드
   { skill_index = 34044, cooltime = 50000, globalcooltime = 1, rate = 90, rangemin = 0, rangemax = 1000, target = 3, td = "LF,LB,RF,RB,BL,BR" },
-- 콤보(Assualt용/ai용)
   { skill_index = 34041, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 500, target = 3 },
   { skill_index = 34041, cooltime = 16000, rate = 80, rangemin = 0, rangemax = 500, target = 3, td = "FR,FL" },
-- 던지기
   { skill_index = 34046, cooltime = 28000, globalcooltime = 1, rate = 60, rangemin = 0, rangemax = 500, target = 3, td = "FR,FL" },
-- 프리징소드
   { skill_index = 34045, cooltime = 20000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, td = "FR,FL", selfhppercent = 74 },
-- 점프어택
   { skill_index = 34042, cooltime = 60000, rate = 90, rangemin = 0, rangemax = 4500, target = 3, randomtarget = "1.6,0,1", selfhppercent = 49 },
-- 스톰프
   { skill_index = 34043, cooltime = 36000, rate = -1, rangemin = 0, rangemax = 4500, target = 3, selfhppercent = 48 },
}

