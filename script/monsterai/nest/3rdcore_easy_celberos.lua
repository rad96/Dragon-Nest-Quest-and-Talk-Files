-- Celberos Hard
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1100;
g_Lua_NearValue3 = 1700;
g_Lua_NearValue4 = 2800;
g_Lua_NearValue5 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssualtTime = 5000


g_Lua_CustomAction =
{
	CustomAction1 = 
	{
	       	{ "Attack_Down_Left", 0},
	 	{ "Attack_Down_Right", 0},   
	},
	CustomAction2 = 
	{
	       	{ "Attack_Down_Right", 0},
	 	{ "Attack_Down_Left", 0},   
	},
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,50" },
	{ action_name = "Stand_4", rate = 4, loop = 1, selfhppercentrange = "50,100" },
	{ action_name = "Walk_Left", rate = 8, loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right", rate = 8, loop = 1, td = "FL,FR" },
	{ action_name = "Attack_Punch_NestBossRush", rate = 7, loop = 1, td = "FL,FR" },
	{ action_name = "CustomAction1", rate = 5, loop = 1, td = "FL", cooltime = 11000, encountertime = 10000 },
	{ action_name = "CustomAction2", rate = 5, loop = 1, td = "FR", cooltime = 11000, encountertime = 10000 },
	{ action_name = "Attack_Bite_NestBossRush", rate = 5, loop = 1, td = "FL,FR", selfhppercent = 40 },
	{ action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
	{ action_name = "Attack_Back_Left", rate = 20, loop = 1, cooltime = 7000, td = "BL", randomtarget = "0.6,0,1" },
	{ action_name = "Attack_Back_Right", rate = 20, loop = 1, cooltime = 7000, td = "BR", randomtarget = "0.6,0,1" },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,50" },
	{ action_name = "Stand_4", rate = 4, loop = 1, selfhppercentrange = "50,100" },
	{ action_name = "Walk_Front", rate = 8, loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left", rate = 2, loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right", rate = 2, loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1 },
	{ action_name = "Walk_Front", rate = 12, loop = 2, td = "FL,FR" },
	{ action_name = "Walk_Left", rate = 2, loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right", rate = 2, loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1 },
	{ action_name = "Move_Front", rate = 8, loop = 2, td = "FL,FR" },
	{ action_name = "Assault", rate = 6, loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Left", rate = 2, loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right", rate = 2, loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },

}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1 },
	{ action_name = "Move_Front", rate = 8, loop = 3, td = "FL,FR" },
	{ action_name = "Assault", rate = 6, loop = 1, td = "FL,FR"  },
	{ action_name = "Walk_Left", rate = 1, loop = 1, td = "FL,FR" },
	{ action_name = "Walk_Right", rate = 1, loop = 1, td = "FL,FR" },
	{ action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}

g_Lua_Assault = { 
   { action_name = "Attack_Punch_NestBossRush", rate = 16, approach = 500 },
   { action_name = "Attack_Bite_NestBossRush", rate = 16, selfhppercent = 40, approach = 500 },
}
g_Lua_GlobalCoolTime1 = 15000

g_Lua_Skill=
{
	-- 낙뢰 Attack2_Thunder
	{ skill_index = 21305, cooltime = 1000, globalcooltime = 1, rangemin = 0, rangemax = 9000, target = 3, rate = 100, priority = 50, selfhppercent = 75, limitcount = 1 },
	{ skill_index = 21305, cooltime = 1000, globalcooltime = 1, rangemin = 0, rangemax = 9000, target = 3, rate = 100, priority = 50, selfhppercent = 50, limitcount = 1 },
	{ skill_index = 21305, cooltime = 1000, globalcooltime = 1, rangemin = 0, rangemax = 9000, target = 3, rate = 100, priority = 50, selfhppercent = 25, limitcount = 1 },

	-- 점프어택 Attack_Jump
	{ skill_index = 21311, cooltime = 50000, rangemin = 900, rangemax = 1200, target = 3, rate = 80, td = "FL,FR,LF,RF", randomtarget = "1.6,0,1" },
	-- 다크라이트닝 Attack2_DarkLightning
	{ skill_index = 21309, cooltime = 44000, rangemin = 0, rangemax = 2000, target = 3, rate = 70, td = "FL,FR", selfhppercent = 95 },
	-- 라이트닝 필드 Attack2_LightningField
	{ skill_index = 21302 , cooltime = 60000, rangemin = 0, rangemax = 1000, target = 3, rate = 65 },

	-- 크라이
	{ skill_index = 21310, cooltime = 70000, globalcooltime = 1, rangemin = 0, rangemax = 2000, target = 3, rate = 80, selfhppercent = 78 },
	-- 파이어 대쉬 Attack3_FireDash
	{ skill_index = 21303 , cooltime = 60000, rangemin = 0, rangemax = 3000, target = 3, rate = 60, td = "LF,FL,FR,RF", selfhppercent = 78 },

	-- 아이스볼트 Attack1_IceBolt
	{ skill_index = 21304, cooltime = 80000, rangemin = 0, rangemax = 2500, target = 3, rate = 80, td = "LF,FL,FR,RF,RB,BR,BL,LB", randomtarget = "1.6,0,1", multipletarget = "1,4,0,1", selfhppercent = 58 },
	-- 프리징 볼 Attack1_FreezingBall
	{ skill_index = 21308, cooltime = 70000, globalcooltime = 1, rangemin = 500, rangemax = 2000, target = 3, rate = 90, td = "LF,FL,FR,RF", multipletarget = "1,4,0,1", selfhppercent = 58 },

	-- 파이어월 Attack3_FireWall
	{ skill_index = 21306 , cooltime = 50000, rangemin = 0, rangemax = 900, target = 3, rate = 60, selfhppercent = 38 },

	-- 화염구 난무 Attack3_FireBall
--	{ skill_index = 21307 , cooltime = 15000, rangemin = 300, rangemax = 2000, target = 3, rate = 50, td = "FL,FR", selfhppercent = 95 },
	-- 아이스 브레스 Attack1_IceBreath
--	{ skill_index = 21301, cooltime = 30000, rangemin = 100, rangemax = 2500, target = 3, rate = 60, td = "LF,FL,FR,RF", selfhppercent = 58 },

}

