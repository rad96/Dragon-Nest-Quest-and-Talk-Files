g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 1400;
g_Lua_NearValue2 = 2000;
g_Lua_NearValue3 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000
g_Lua_NoAggroStand = 1

g_Lua_PartsProcessor = {
   { hp = "0,100", ignore="181,182", nodamage="181,182" },
}

g_Lua_Near1 = { 
   { action_name = "Stand_LockTarget_EventArea", rate = 16, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_LockTarget_EventArea", rate = 16, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_LockTarget_EventArea", rate = 16, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 32080, cooltime = 35000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 1, encountertime = 10000 },
   { skill_index = 32072, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
}
