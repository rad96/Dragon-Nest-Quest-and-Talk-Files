--AiFrogMagician_Green_Abyss.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 5000;


g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}

g_Lua_Skill = {
   { skill_index = 301703, cooltime = 5000, rate = 70, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "0.6,0,1", next_lua_skill_index=1 }, -- 11
   { skill_index = 301717,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index=2 }, -- 12
   { skill_index = 301716,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 13
}