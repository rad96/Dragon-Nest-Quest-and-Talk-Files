--AiWispGiant_WK_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Move_Front", rate = 10, loop = 2 },
}
g_Lua_Near2 = { 
   { action_name = "Move_Front", rate = 10, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Move_Front", rate = 10, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 10, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 10, loop = 3  },
}

g_Lua_Skill = { 
   { skill_index = 301581,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3},
   { skill_index = 301582,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3},
}
