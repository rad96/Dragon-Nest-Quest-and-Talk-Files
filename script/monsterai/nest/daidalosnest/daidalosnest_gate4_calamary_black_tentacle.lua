--AiPterosaur_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 2400;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_GlobalCoolTime1 = 5000

g_Lua_Skill = { 
       -- Attack01_Swing_Start
   { skill_index = 301591,  cooltime = 10000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 1000, target = 3, randomtarget = "0.6,0,1", td = "FR,FL" }, -- 0
       -- Attack02_Thrusting_3m_Loop
   { skill_index = 301592,  cooltime = 10000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 4000, target = 3, randomtarget = "0.6,0,1", td = "FR,FL" }, -- 1
       -- Attack02_Thrusting_6m_Loop
   { skill_index = 301593,  cooltime = 10000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 700, target = 3, randomtarget = "0.6,0,1", td = "FR,FL" }, -- 2
       -- Attack02_Thrusting_9m_Loop
   { skill_index = 301594,  cooltime = 10000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 1000, target = 3, randomtarget = "0.6,0,1", td = "FR,FL" }, -- 3
}