--AiPterosaur_Red_Abyss.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000
g_Lua_NearValue2 = 6000


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 301731,  cooltime = 1000, rate = 70, rangemin = 0, rangemax = 5000, target = 3 },
}
