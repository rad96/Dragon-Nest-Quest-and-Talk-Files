--AiPterosaur_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Stand3", rate = 2, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Assault = { 
   { action_name = "Stand", rate = 10, loop = 1, approach = 100 },
}
g_Lua_GlobalCoolTime1 = 120000
g_Lua_GlobalCoolTime2 = 40000

g_Lua_Skill = { 
       -- Attack01_Slash
   { skill_index = 301551,  cooltime = 15000, rate = 70, rangemin = 0, rangemax = 500, target = 3, td = "FR,FL" }, -- 0
       -- Attack02_Twist
   { skill_index = 301552,  cooltime = 35000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercent=90 }, -- 1
       -- Attack03_CycloneDance_Start
   { skill_index = 301553,  cooltime = 45000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 2
       -- Attack04_Stick
   { skill_index = 301554,  cooltime = 55000, rate = 70, rangemin = 0, rangemax = 5000, target = 3 }, -- 3
       -- -- Attack05_DrillRun_Start
   -- { skill_index = 301555,  cooltime = 65000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercent=75, randomtarget = "0.6,0,1" }, -- 4
       -- Attack06_DrillUp_Start
   { skill_index = 301556,  cooltime = 65000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1", selfhppercent = 50, priority = 90 }, -- 4
       -- Attack07_Flower_Start
   { skill_index = 301563,  cooltime = 90000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercentrange="51,75", limitcount = 1, next_lua_skill_index=9 }, -- 5
   { skill_index = 301563,  cooltime = 90000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercentrange="26,50", limitcount = 1, next_lua_skill_index=9 }, -- 6
   { skill_index = 301563,  cooltime = 90000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercentrange="13,25", limitcount = 1, next_lua_skill_index=9 }, -- 7
   { skill_index = 301563,  cooltime = 90000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercentrange="5,12", limitcount = 1, next_lua_skill_index=9 }, -- 8
   { skill_index = 301557,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 9
       -- Attack08_Laser_Start
   { skill_index = 301558,  cooltime = 45000, rate = 70, rangemin = 0, rangemax = 1500, target = 3, td = "FR,FL", randomtarget = "0.6,0,1" }, -- 10
       -- Attack12_CenterRun
   { skill_index = 301565,  cooltime = 90000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index=14 }, -- 11
       -- Attack10_Tentacle
   { skill_index = 301560,  cooltime = 80000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, td = "FR,FL", randomtarget = "0.6,0,1" }, -- 12
       --Attack11_meteorUmbrella_Start
   { skill_index = 301561,  cooltime = 150000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercent=50, priority = 90, randomtarget = "0.6,0,1" }, -- 13
       -- Attack12_CenterRun Attack09_Storm_Start
   { skill_index = 301559,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 14
}
