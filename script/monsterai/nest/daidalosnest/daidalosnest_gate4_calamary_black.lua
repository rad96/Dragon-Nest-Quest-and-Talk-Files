--AiPterosaur_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 2400;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Stand3", rate = 2, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Assault = { 
   { action_name = "Stand", rate = 10, loop = 1, approach = 100 },
}


g_Lua_Skill = { 
       -- Attack01_Slash
   { skill_index = 301571,  cooltime = 5000, rate = 70, rangemin = 0, rangemax = 500, target = 3, td = "FR,FL", randomtarget = "0.6,0,1" }, -- 0
       -- Attack02_Twist
   { skill_index = 301572,  cooltime = 35000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 1
       -- Attack03_CycloneDance_Start
   { skill_index = 301573,  cooltime = 45000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 2
}
