--DaidalosNest_Gate2_Bear_White_Boss.lua
--/genmon 505521

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue3 = 8000;

g_Lua_PatrolBaseTime = 2000
g_Lua_PatrolRandTime = 2000
g_Lua_ApproachValue = 350
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 8, loop = 1,},
   { action_name = "Assault", rate = 15, loop = 1, cooltime = 8000 },
}
g_Lua_Assault = {  { lua_skill_index = 1, rate = 100, approach = 500},} --Attack02_StomachAss

g_Lua_GlobalCoolTime1 = 120000
g_Lua_GlobalCoolTime2 = 35000

g_Lua_Skill = {
   { skill_index = 301465,  cooltime = 55000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,  }, --0 Attack07_LineSuccess
   { skill_index = 301469,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,  }, --1 Attack02_StomachAss
   { skill_index = 301470,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,  }, --2 Attack06_AngerRushLinear_Start
   { skill_index = 301464,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, usedskill="301463" ,globalcooltime = 2}, -- Attack07_LineFailed
   { skill_index = 301464,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, usedskill="301466" ,globalcooltime = 2}, -- Attack07_LineFailed
   { skill_index = 301464,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, usedskill="301467" ,globalcooltime = 2}, -- Attack07_LineFailed
   -- { skill_index = 301459,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, priority = 50,next_lua_skill_index=1}, --0 Attack09_Run_Start
   -- { skill_index = 301462,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, priority = 40, }, --1 Attack09_State
   
   { skill_index = 301463,  cooltime = 120000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority=100, selfhppercentrange = "0,75", limitcount = 1, globalcooltime = 1, globalcooltime2 = 2 }, -- Attack08_LineStartHRMR_Start
   { skill_index = 301466,  cooltime = 120000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority=90, selfhppercentrange = "0,50", limitcount = 1, globalcooltime = 1, globalcooltime2 = 2 }, -- Attack08_LineStartHLML_Start
   { skill_index = 301467,  cooltime = 120000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority=80, selfhppercentrange = "0,25", limitcount = 1, globalcooltime = 1, globalcooltime2 = 2 }, -- Attack08_LineStartHRML_Start
   { skill_index = 301471,  cooltime = 40000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1", encountertime = 8000, next_lua_skill_index = 2 }, -- Attack06_AngerRushLinearReady
   { skill_index = 301454,  cooltime = 55000, rate = 70, rangemin = 0, rangemax = 10000, target = 3, randomtarget = "0.6,0,1" ,selfhppercentrange= "0,75"}, --  Attack04_JumpStomach_Start
   { skill_index = 301460,  cooltime = 60000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "2,4,0,1",selfhppercentrange = "0,50", }, -- Attack07_RepeatThrusting_Patun_Start
   { skill_index = 301451,  cooltime = 8000, rate = 40, rangemin = 0, rangemax = 600, target = 3, td = "FR,FL" }, -- Attack01_Thrusting
   { skill_index = 301469,  cooltime = 50000, rate = 30, rangemin = 0, rangemax = 500, target = 3, randomtarget = "0.6,0,1" }, -- Attack02_Stomach
   { skill_index = 301453,  cooltime = 10000, rate = 70, rangemin = 0, rangemax = 800, target = 3, td = "BR,BL"}, -- Attack03_Fart
   { skill_index = 301455,  cooltime = 30000, rate = 90, rangemin = 0, rangemax = 500, target = 3, }, -- Attack05_Combo
   { skill_index = 301459,  cooltime = 40000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, }, -- Attack09_Run_Start
}
