--AiPterosaur_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 2400;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 2000
g_Lua_NoAggroStand = 1

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Move_Front", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Assault = { 
   { action_name = "Stand", rate = 10, loop = 1, approach = 100 },
}
g_Lua_GlobalCoolTime1 = 120000
g_Lua_GlobalCoolTime2 = 10000
g_Lua_GlobalCoolTime3 = 30000

g_Lua_RandomSkill ={
{
{    { skill_index = 302001, rate = 50 },   { skill_index = 302009, rate = 50 },  },
}
}

g_Lua_Skill = { 
	-- Attack1_SpinRun_Start (전진스핀)
	{ skill_index = 301401, cooltime = 25000, rate = 70, rangemin = 0, rangemax = 700, target = 3, td = "FR,FL", globalcooltime = 2 }, -- 0
	-- Attack1_SpinRun_Nomal_Start (제자리 회전)
    { skill_index = 301410,  cooltime = 15000, rate = 70, rangemin = 0, rangemax = 300, target = 3 }, -- 1
	-- Attack10_Gun_Start (횡이동 난사)
	{ skill_index = 301415,  cooltime = 45000, rate = 70, rangemin = 0, rangemax = 2000, target = 3, globalcooltime = 2 }, -- 2
    -- Attack3_LaserShot (직선 레이저)
    { skill_index = 301403,  cooltime = 35000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 2}, -- 3
	-- Attack2_SiegeMode_HighLong_Nomal_Start (메카덕 소환)
    { skill_index = 302010,  cooltime = 45000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 4   
    -- Attack4_Jump (스톰프)
    { skill_index = 301404,  cooltime = 55000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, td = "FR,FL", selfhppercent= 50, priority = 90}, -- 5
	-- Attack2_SiegeMode_HighLong_HardCore_Start (원결계 치고 미사일난사)
    { skill_index = 302002,  cooltime = 75000, globalcooltime = 3, rate = 90, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "2,4,0,1", selfhppercent= 75, priority = 80 }, -- 6


    -- -- Attack5_Rush_Start (호밍 대시)
    -- { skill_index = 301405,  cooltime = 50000, globalcooltime = 2, rate = 80, rangemin = 0, rangemax = 5000, target = 3, selfhppercent= 75, randomtarget = "0.6,0,1", priority = 80 }, -- 7
    -- Attack9_RocketRush_Start (빠른 직선형 대시)
    { skill_index = 301412,  cooltime = 50000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 50, randomtarget = "0.6,0,1", priority = 90 }, -- 7
    -- -- Attack9_RocketRush_LockTarget_Start (바닥에 불장판 러시)
    -- { skill_index = 301414,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, --  9
    -- Attack9_RocketRush_LockTarget2_Start(불장판 안까는 러시)
    { skill_index = 301419,  cooltime = 45000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 5000, target = 3,}, -- 8
    -- Attack8_UnderShot_Start
    { skill_index = 302007,  cooltime = 60000, rate = 90, rangemin = 0, rangemax = 5000, target = 3, selfhppercent= 75, randomtarget = "0.6,0,1", priority = 80 }, -- 9
    -- Attack6_Shotgun_Start (좁은 지역에서 샷건 피하기_하드코어)
    { skill_index = 302001, skill_random_index = 1, cooltime = 70000, globalcooltime = 3, rate = 90, rangemin = 0, rangemax = 5000, target = 3, selfhppercent= 50, priority = 90 }, -- 10   어택9 로켓러시 락타겟 콤보로 연결돼 있었음

    -- Attack7_SwingLong_Start (줄패턴)
    { skill_index = 301413,  cooltime = 120000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 75, limitcount = 1 }, -- 11
    { skill_index = 301413,  cooltime = 120000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 50, limitcount = 1 }, -- 12
    { skill_index = 301413,  cooltime = 120000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 25, limitcount = 1 }, -- 13
    { skill_index = 301413,  cooltime = 120000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 12, limitcount = 1 }, -- 14
}
