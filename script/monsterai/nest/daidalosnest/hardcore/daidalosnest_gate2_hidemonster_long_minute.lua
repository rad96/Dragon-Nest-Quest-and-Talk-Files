--2관문 줄패턴-분침	505528
--DaidalosNest_Gate2_HideMonster_Minute.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 5000;


g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}
g_Lua_Skill = {
   { skill_index = 301491, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, },--T291 멈춤 Attack_Stop
   -- { skill_index = 301492, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, },--T292 시침 우회전 Attack_HourRight_Start
   -- { skill_index = 301493, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, },--T293 시침 우회전 속도빠름 Attack_HourRightFast_Start
   -- { skill_index = 301494, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, },--T294 시침 좌회전 Attack_HourLeft_Start
   -- { skill_index = 301495, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, },--T295 시침 좌회전 속도빠름 Attack_HourLeftFast_Start
   { skill_index = 301496, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, },--T296 분침 우회전 Attack_MinuteRight_Start
   { skill_index = 301497, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, },--T297 분침 우회전 속도빠름 Attack_MinuteRightFast_Start
   { skill_index = 301498, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, },--T298 분침 좌회전 Attack_MinuteLeft_Start
   { skill_index = 301499, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, },--T299 분침 좌회전 속도빠름 Attack_MinuteLeftFast_Start
}

