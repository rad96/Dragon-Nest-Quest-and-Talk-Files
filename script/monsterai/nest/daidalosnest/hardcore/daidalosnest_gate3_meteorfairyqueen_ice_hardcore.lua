--AiPterosaur_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Assault = { 
   { action_name = "Stand", rate = 10, loop = 1, approach = 100 },
}
g_Lua_GlobalCoolTime1 = 120000
g_Lua_GlobalCoolTime2 = 30000
g_Lua_GlobalCoolTime3 = 15000
g_Lua_GlobalCoolTime4 = 15000
g_Lua_GlobalCoolTime5 = 40000
g_Lua_GlobalCoolTime6 = 60000
g_Lua_GlobalCoolTime7 = 70000
g_Lua_GlobalCoolTime8 = 30000

g_Lua_Skill = {
       -- Attack01_IceWall
   { skill_index = 301501,  cooltime = 45000,  rate = 70, rangemin = 0, rangemax = 500, target = 3, td = "FR,FL", next_lua_skill_index=1 }, -- 0
       -- Attack02_IceWallBoom
   { skill_index = 301502,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "2,2,0,1" }, -- 1
       -- Attack03_IceRoad_Start
   { skill_index = 301503,  cooltime = 35000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 2
       -- Attack04_FearIce_HardCore
   { skill_index = 302134,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 3
       -- Attack06_Rush
   { skill_index = 301506,  cooltime = 90000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 3 }, -- 4
       -- Attack07_SomersaultKick_Start
   { skill_index = 301507,  cooltime = 40000, rate = 70, rangemin = 0, rangemax = 400, target = 3, randomtarget = "0.6,0,1" }, -- 5
       -- Attack08_IceSpear_Start
   { skill_index = 301513,  cooltime = 90000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 75, limitcount = 1, next_lua_skill_index = 10 }, --6
   { skill_index = 301513,  cooltime = 90000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 50, limitcount = 1, next_lua_skill_index = 10 }, -- 7
   { skill_index = 301513,  cooltime = 90000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 25, limitcount = 1, next_lua_skill_index = 10 }, -- 8
   { skill_index = 301513,  cooltime = 90000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 12, limitcount = 1, next_lua_skill_index = 10 }, -- 9
   { skill_index = 302131,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 10
       -- Attack09_CounterKick
   { skill_index = 301510,  cooltime = 15000, rate = 70, rangemin = 0, rangemax = 400, target = 3, next_lua_skill_index = 12, randomtarget = "0.6,0,1" }, -- 11
   { skill_index = 301509,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 12
       -- Attack11_IcePrison_Start
   { skill_index = 301511,  cooltime = 90000, globalcooltime = 2,  rate = 70, rangemin = 0, rangemax = 400, target = 3, next_lua_skill_index = 14, priority = 80 }, -- 13
   { skill_index = 301512,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 14
       -- Attack12_IceCircle
   { skill_index = 301514,  cooltime = 25000, rate = 70, rangemin = 0, rangemax = 400, target = 3, randomtarget = "0.6,0,1" }, -- 15
       -- Attack13_IceBerg
   { skill_index = 301515,  cooltime = 25000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1", selfhppercentrange= "75,100" }, -- 16
       -- Attack05_DarkHand_Start
   { skill_index = 301518,  cooltime = 150000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 95 }, -- 17
       -- Attack13_IceBerg_HardCore
   { skill_index = 302133,  cooltime = 25000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1", selfhppercentrange= "0,74" }, -- 16  
       -- -- Attack09_CounterKick_HardCore
   -- { skill_index = 302132,  cooltime = 25000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 16      
  }