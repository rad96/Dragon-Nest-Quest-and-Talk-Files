-- /genmon 236010
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 800.0;
g_Lua_NearValue2 = 1200.0;
g_Lua_NearValue3 = 1700.0;
g_Lua_NearValue4 = 2500.0;
g_Lua_NearValue5 = 5000.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 800;
g_Lua_AssualtTime = 5000;

g_Lua_GlobalCoolTime1 = 45000 
g_Lua_GlobalCoolTime2 = 5000
g_Lua_GlobalCoolTime3 = 11000
g_Lua_GlobalCoolTime4 = 55000

g_Lua_Near1 = 
	{
		{ action_name = "Stand",rate = 10, loop = 1 }, 
	}
g_Lua_Near2 = 
	{ 
		{ action_name = "Stand",rate = 10, loop = 1 }, 
	}
g_Lua_Near3 = 
	{
		{ action_name = "Stand",rate = 10, loop = 1 },
	}
g_Lua_Near4 = 
	{
		{ action_name = "Stand",rate = 10, loop = 1 }, 
	}
g_Lua_Near5 = 
	{ 
		{ action_name = "Stand",rate = 10, loop = 1 }, 
	}

g_Lua_GlobalCoolTime1 = 10000
	
	
g_Lua_Skill = {
       -- Attack01_BasicLeft
   { skill_index = 301601,  cooltime = 20000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "0.6,0,1" }, -- 0
       -- Attack01_BasicRight
   { skill_index = 301602,  cooltime = 20000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "0.6,0,1" }, -- 1
       -- Attack02_ClawMelee
   { skill_index = 301603,  cooltime = 30000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 2
       -- Attack03_ClawRange
   { skill_index = 301604,  cooltime = 30000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 3
       -- Attack04_Eye_Magic
   { skill_index = 301605,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "2,4,0,1", next_lua_skill_index=6 }, -- 4
       -- Attack05_Spin
   { skill_index = 301606,  cooltime = 40000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 5000, priority = 90, target = 3, randomtarget = "0.6,0,1" }, -- 5
       -- Attack07_Laser_start
   { skill_index = 302167,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 6
       -- Attack08_Run_end
   { skill_index = 301608,  cooltime = 60000, globalcooltime = 1 ,rate = 70, rangemin = 0, rangemax = 5000, target = 3, priority = 95, next_lua_skill_index=8, randomtarget = "0.6,0,1" }, -- 7
       -- Attack07_Breath_Start
   { skill_index = 302168,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index=11, randomtarget = "0.6,0,1" }, -- 8
       -- Attack09_Stomp
   -- { skill_index = 301610,  cooltime = 40000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 9
       -- Attack07_Missile_Start
   { skill_index = 302169,  cooltime = 40000, rate = 70, rangemin = 0, rangemax = 10000, target = 3, multipletarget = "2,4,0,1" }, -- 9
       -- Attack07_Vacuum_Start
   { skill_index = 301612,  cooltime = 40000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index=4, randomtarget = "0.6,0,1" }, -- 10
       -- Attack07_Breath2_Start
   { skill_index = 302170,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 11
       -- -- Attack09_Bite
   -- { skill_index = 301614,  cooltime = 10000, rate = 70, rangemin = 0, rangemax = 500, target = 3, randomtarget = "0.6,0,1" }, -- 13
}
   