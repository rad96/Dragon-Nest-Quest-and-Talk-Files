--AiPterosaur_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 5400;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Assault = { 
   { action_name = "Stand", rate = 10, loop = 1, approach = 100 },
}
g_Lua_GlobalCoolTime1 = 50000
g_Lua_GlobalCoolTime2 = 50000
g_Lua_GlobalCoolTime3 = 35000
g_Lua_GlobalCoolTime4 = 200000

-- g_Lua_RandomSkill ={
-- {
-- {    { skill_index = 301703, rate = 50 },   { skill_index = 301704, rate = 50 },   },
-- }
-- }

g_Lua_Skill = {
-- 중앙 전투존 + 석상 머리 위에서 사용하는 스킬
    -- Attack01_Swing
   { skill_index = 302171, cooltime = 10000, rate = 70, rangemin = 0, rangemax = 500, target = 3 }, -- 0
    -- Attack02_Throw
   { skill_index = 302172, cooltime = 25000, rate = 70, rangemin = 0, rangemax = 10000, target = 3, multipletarget = "2,4,0,1" }, -- 1
    -- Attack05_Stomp_Start
   { skill_index = 301706, cooltime = 45000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1", notusedskill  = "301714, 301715" }, -- 2

-- 석상 머리 위에서만 사용하는 스킬
     -- Attack03_Rush_Fail_Head_Start
   { skill_index = 301719, cooltime = 35000, rate = 70, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "0.6,0,1", usedskill  = "301714, 301715" }, -- 3    
   
-- 중앙 전투존에서만 사용하는 스킬
--[강 스킬]
    -- Attack09_HandStanding_Start
   { skill_index = 302177, cooltime = 70000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 500, target = 3, priority = 90, notusedskill  = "301714, 301715", randomtarget = "0.6,0,1", selfhppercent = 50 }, -- 4
     -- Attack05_Stomp_Straight_Start
   { skill_index = 301712, cooltime = 70000, globalcooltime = 1,  rate = 70, rangemin = 0, rangemax = 1000, target = 3, priority = 85, randomtarget = "0.6,0,1", notusedskill  = "301714, 301715", selfhppercent = 75 }, -- 5  
--[중 스킬]
    -- Attack03_Rush_Fail_Start
   { skill_index = 301704, cooltime = 60000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "0.6,0,1", notusedskill  = "301714, 301715", selfhppercentrange="51,90" }, -- 6
    -- Attack06_SummonCard_Start
   { skill_index = 302181, cooltime = 60000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 5000, target = 3, notusedskill  = "301714, 301715", selfhppercent = 80 }, -- 7
    -- Attack08_JumpAttack
   { skill_index = 301723, cooltime = 60000, globalcooltime = 2, rate = 70, rangemin = 300, rangemax = 1000, target = 3, priority = 85, randomtarget = "0.6,0,1", notusedskill  = "301714, 301715", selfhppercent = 95 }, --8
   { skill_index = 301709,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 9
    -- Attack07_CardBreath_Start
   { skill_index = 302180, cooltime = 60000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 5000, target = 3, notusedskill  = "301714, 301715", randomtarget = "0.6,0,1" }, -- 10
--[약 스킬]
     -- Attack04_Combo_Start
  -- { skill_index = 301705, cooltime = 35000, globalcooltime = 3, rate = 70, rangemin = 0, rangemax = 5000, target = 3, notusedskill  = "301714, 301715" }, -- 11
    -- Attack03_Rush_Start
   { skill_index = 302173, cooltime = 45000, globalcooltime = 3, rate = 70, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "0.6,0,1", next_lua_skill_index=12 }, -- 11
   { skill_index = 302175,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1", next_lua_skill_index=13 }, -- 12
   { skill_index = 302174,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1" }, -- 13
   
--[줄패턴 시작]
    -- Attack07_MagicShow_Start
   { skill_index = 301708,  cooltime = 180000, globalcooltime = 4, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 80, limitcount = 1 }, -- 14
   { skill_index = 301708,  cooltime = 180000, globalcooltime = 4, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 60, limitcount = 1 }, -- 15
   { skill_index = 301708,  cooltime = 180000, globalcooltime = 4, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 20, limitcount = 1 }, -- 16
   
--[스페셜 스킬]302176
   { skill_index = 302176, cooltime = 90000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, priority = 90, notusedskill  = "301714, 301715", randomtarget = "0.6,0,1", priority = 70, selfhppercent = 80 , next_lua_skill_index=18 }, -- 17
   { skill_index = 301705,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 18
}
