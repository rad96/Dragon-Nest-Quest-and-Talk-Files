g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 2000;
g_Lua_NearValue2 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_RandomSkill ={
{
{
   { skill_index = 34811, rate = 50, next_lua_skill_index = 1 }, 
   { skill_index = 34811, rate = 50, next_lua_skill_index = 3 },
},
}
}


g_Lua_GlobalCoolTime1 = 85000 -- Attack20_Laser_L2R, Attack20_Laser_R2L
g_Lua_GlobalCoolTime2 = 10000 -- Attack01_BashRight, Attack02_BashLeft
g_Lua_GlobalCoolTime3 = 13000 -- Attack18_Saw_Right, Attack17_Saw_Left
g_Lua_GlobalCoolTime4 = 18000 --Attack12_Bomb,Attack20_Laser_L2R, Attack20_Laser_R2L

g_Lua_Skill = { 
-- Attack20_Laser_L2R, Attack20_Laser_R2L
   { skill_index = 34808, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 34810, next_lua_skill_index = 0, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 34807, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 34809, next_lua_skill_index = 2, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 34804, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- Attack08_Ruin
   { skill_index = 34798, cooltime = 55000, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, encountertime = 20000 },
-- Attack13_Berserk
   { skill_index = 34803, cooltime = 60000, rate = 100, priority = 40, rangemin = 0, rangemax = 5000, target = 1, selfhppercent = 45, notusedskill = "34803" },
-- Attack20_Laser_L2R, Attack20_Laser_R2L(22sec)
   { skill_index = 34811, next_lua_skill_index = 1, cooltime = 1000, rate = 100, priority = 30, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, limitcount = 1 },
   { skill_index = 34811, next_lua_skill_index = 3, cooltime = 1000, globalcooltime = 4, rate = 100, priority = 30, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 50, limitcount = 1 },
   { skill_index = 34811, skill_random_index = 1, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 4, rate = 100, priority = 30, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 25 },
-- Attack11_Earthquake
   { skill_index = 34801, cooltime = 40000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 73 },
-- Attack12_Bomb
   { skill_index = 34802, cooltime = 40000, globalcooltime = 4, rate = 90, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,8,0,1", selfhppercent = 48 },
-- Attack01_BashRight(34791->34966), Attack02_BashLeft(34792->34967)
   { skill_index = 34966, cooltime = 10000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6,0,1", td = "FR,RF,RB" },
   { skill_index = 34967, cooltime = 10000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6,0,1", td = "FL,LF,LB" },
-- Attack18_Saw_Right, Attack17_Saw_Left
   { skill_index = 34793, cooltime = 13000, globalcooltime = 3, rate = 70, rangemin = 0, rangemax = 5000, target = 3, td = "FR" },
   { skill_index = 34794, cooltime = 13000, globalcooltime = 3, rate = 70, rangemin = 0, rangemax = 5000, target = 3, td = "FL" },
-- Attack07_Stomp 
   { skill_index = 34797, cooltime = 40000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,8,0,1" },
-- Attack09_Vortex
   { skill_index = 34799, cooltime = 36000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, encountertime = 15000 },
-- Attack10_Laser_Center
   { skill_index = 34800, cooltime = 20000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, td = "FR,FL" },
-- Attack19_Cross
   { skill_index = 34806, cooltime = 20000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6,0,1", td = "FR,FL,RF,LF" },
}