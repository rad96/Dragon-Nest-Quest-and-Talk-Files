--AiCalamary_Red_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1600;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Attack1_Slash", rate = 19, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 1, loop = 1, approach = 300 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 300 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 300 },
   { action_name = "Attack1_Slash", rate = 16, loop = 1, approach = 300 },
}
g_Lua_Skill = { 
   -- 더듬이 미사일 : 레드
   { skill_index = 20881,  cooltime = 10000, rate = 80,rangemin = 0, rangemax = 1200, target = 3, randomtarget=1.1, selfhppercentrange = "50,100"  },
   -- 화염 돌풍
   { skill_index = 20874,  cooltime = 18000, rate = 80, rangemin = 0, rangemax = 900, target = 3, randomtarget=1.1 },
    -- 휠윈드 레드
   { skill_index = 33105,  cooltime = 38000, rate = 80, rangemin = 0, rangemax = 700, target = 3, randomtarget=1.1 },
   -- { skill_index = 20879,  cooltime = 38000, rate = 80, rangemin = 0, rangemax = 700, target = 3, randomtarget=1.1 },
   -- 포자 : 레드
   { skill_index = 20873,  cooltime = 15000, rate = 60, rangemin = 200, rangemax = 1500, target = 3, selfhppercent = 50, randomtarget=1.1 },
   -- 촉수 공격  화염
   { skill_index = 33142,  cooltime = 15000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 50, randomtarget=1.1 },
   -- 레드 미니언 소환
   -- { skill_index = 33206,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, randomtarget=1.1 },
   -- 레드 아이스볼 
   { skill_index = 33208,  cooltime = 35000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,50" , randomtarget=1.1 },
}
