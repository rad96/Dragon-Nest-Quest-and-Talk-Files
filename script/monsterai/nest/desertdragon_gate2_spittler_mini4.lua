--AiSpittler_Green_Boss_Abyss.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 2500;
g_Lua_NearValue2 = 3500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 28000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 5, loop = 2, custom_state1 = "custom_ground",  },
   { action_name = "Attack3_WaterWall_Desert", rate = 8, loop = 1, custom_state1 = "custom_ground", cooltime = 28000, globalcooltime = 1, encountertime = 21000 },
   { action_name = "Stand_On", rate = 50, loop = 1, custom_state1 = "custom_underground",  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 2, custom_state1 = "custom_ground",  },
   { action_name = "Attack3_WaterWall_Desert", rate = 8, loop = 1, custom_state1 = "custom_ground", cooltime = 28000, globalcooltime = 1, encountertime = 21000 },
   { action_name = "Stand_On", rate = -1, loop = 1, custom_state1 = "custom_underground",  },
}

g_Lua_Skill = { 
   { skill_index = 31943, cooltime = 20000, rate = 55, rangemin = 0, rangemax = 2000, target = 3, custom_state1 = "custom_ground", encountertime = 26000 },
}