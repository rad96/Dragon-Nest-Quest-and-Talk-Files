--/genmon 503747

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000
g_Lua_NearValue2 = 6000


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 8000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 16, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 36062,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, },--Attack_VolNEST_3rd_06_Go
      { skill_index = 36069,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, },--Attack_VolNEST_3rd_Wait_Start
}