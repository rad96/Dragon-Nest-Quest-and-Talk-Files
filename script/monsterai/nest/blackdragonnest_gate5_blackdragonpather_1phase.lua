g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 9000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1,custom_state1 = "custom_Ground" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1,custom_state1 = "custom_Ground" },
}

 g_Lua_RandomSkill ={
{
{
   { skill_index = 34865, rate = 50, next_lua_skill_index = 1 }, 
   { skill_index = 34866, rate = 50, next_lua_skill_index = 2 },
},
}
}

g_Lua_GlobalCoolTime1= 18000
g_Lua_GlobalCoolTime2= 22000

g_Lua_Skill = {
-- 1Phase BuffCheck
	{ skill_index = 34870, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3, custom_state1 = "custom_Ground" },
	{ skill_index = 34871, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3, custom_state1 = "custom_Ground" },
	{ skill_index = 34872, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3, custom_state1 = "custom_Ground" },
	{ skill_index = 34873, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3, custom_state1 = "custom_Ground" },

--Attack017_DASH(95percent)
	{ skill_index = 34867, cooltime = 1000, globalcooltime = 1, rate = 100, priority = 100, rangemin = 0, rangemax = 9000, target = 3, custom_state1 = "custom_Ground", limitcount = 1, selfhppercent = 95, next_lua_skill_index = 0 },
--Attack017_LeftMove/Attack017_RightMove(90percent)
	{ skill_index = 34865, skill_random_index = 1, cooltime = 1000, globalcooltime = 1, rate = 100, priority = 90, rangemin = 0, rangemax = 9000, target = 3, custom_state1 = "custom_Ground", limitcount = 1, selfhppercent = 90, next_lua_skill_index = 1, usedskill = "34870" },
--Attack017_DASH(85percent)
	{ skill_index = 34868, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 2, rate = 100, priority = 80, rangemin = 0, rangemax = 9000, target = 3, custom_state1 = "custom_Ground", limitcount = 1, selfhppercent = 85, next_lua_skill_index = 3, usedskill = "34870,34871", notusedskill = "34872" },
	{ skill_index = 34868, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 2, rate = 100, priority = 80, rangemin = 0, rangemax = 9000, target = 3, custom_state1 = "custom_Ground", limitcount = 1, selfhppercent = 85, next_lua_skill_index = 3, usedskill = "34870,34872", notusedskill = "34871" },
--Attack18_Start2Phase_Start
	{ skill_index = 34869, cooltime = 1000, globalcooltime = 1, rate = 100, priority = 70, rangemin = 0, rangemax = 9000, target = 3, custom_state1 = "custom_Ground", limitcount = 1, selfhppercent = 77, usedskill = "34870,34871,34873", notusedskill = "34872" },
	{ skill_index = 34869, cooltime = 1000, globalcooltime = 1, rate = 100, priority = 70, rangemin = 0, rangemax = 9000, target = 3, custom_state1 = "custom_Ground", limitcount = 1, selfhppercent = 77, usedskill = "34870,34872,34873", notusedskill = "34871" },

--Attack16_SuperBreath_Start
	{ skill_index = 34863, cooltime = 40000, globalcooltime = 1, globalcooltime2 = 2, rate = 100, priority = 60, rangemin = 0, rangemax = 6000, target = 3, custom_state1 = "custom_Ground", encountertime = 40000 },

--Attack06_Meteor_Start
	{ skill_index = 34848, cooltime = 10000, rate = 100, priority = 50, rangemin = 1200, rangemax = 9000, target = 3, multipletarget = "1,8,0,1", custom_state1 = "custom_Ground", encountertime = 10000 },
--Attack05_ShortBreath_Front
	{ skill_index = 34845, cooltime = 10000, rate = 50, rangemin = 0, rangemax = 1200, target = 3, custom_state1 = "custom_Ground", encountertime = 7000 },
--Attack001_Bite
	{ skill_index = 34841, cooltime = 12000, rate = 70, rangemin = 0, rangemax = 800, target = 3, custom_state1 = "custom_Ground", td = "FL,FR", randomtarget = "1.6,0,1", encountertime = 7000 },
--Attack002_Bash_Left
	{ skill_index = 34842, cooltime = 12000, rate = 70, rangemin = 0, rangemax = 1200, target = 3, custom_state1 = "custom_Ground", td = "FL,LF", randomtarget = "1.6,0,1", encountertime = 7000 },
--Attack003_Bash_Right
	{ skill_index = 34843, cooltime = 12000, rate = 70, rangemin = 0, rangemax = 1200, target = 3, custom_state1 = "custom_Ground", td = "FR,RF", randomtarget = "1.6,0,1", encountertime = 7000 },

}


