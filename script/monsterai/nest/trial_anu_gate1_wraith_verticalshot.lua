--AiGoblin_Green_Normal.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000
g_Lua_NearValue2 = 5000


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 4, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 34163,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 1, limitcount=1, combo1 = "1,100,0" }, 
   { skill_index = 34164,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 1, limitcount=1}, 
}

