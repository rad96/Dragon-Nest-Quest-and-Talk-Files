--AiBroo_Blue_Normal.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 3500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction =
{
   CustomAction1 = 
   {
	{ "Turn_Left", 1 },
 	{ "useskill", lua_skill_index = 7, rate = 100 },
   },
   CustomAction2 = 
   {
 	{ "Turn_Right", 1 },
 	{ "useskill", lua_skill_index = 7, rate = 100 },
   },
   CustomAction3 = 
   {
	{ "Turn_Left_Invincible", 1 },
 	{ "useskill", lua_skill_index = 7, rate = 100 },
   },
   CustomAction4 = 
   {
 	{ "Turn_Right_Invincible", 1 },
 	{ "useskill", lua_skill_index = 7, rate = 100 },
   },
}

-- 1:����, 2:�Ĺ� 3: Tunr����Slash, 4: Ȱ�� 5: Ʈ�� 6:�����,������ 7:�극�� 8:���̺�����Ʈ,��,���Ͼ���
g_Lua_GlobalCoolTime1 = 57000
g_Lua_GlobalCoolTime2 = 7000
g_Lua_GlobalCoolTime3 = 8000
g_Lua_GlobalCoolTime4 = 70000
g_Lua_GlobalCoolTime5 = 40000
g_Lua_GlobalCoolTime6 = 12000
g_Lua_GlobalCoolTime7 = 20000
g_Lua_GlobalCoolTime8 = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1, notusedskill = "32130" },
   { action_name = "Stand_1_Invincible", rate = 6, loop = 1, usedskill = "32130" },
   { action_name = "CustomAction1", rate = 50, priority = 45, loop = -1, globalcooltime = 3, td = "LF,LB,BL", notusedskill = "32130" },
   { action_name = "CustomAction2", rate = 50, priority = 45, loop = -1, globalcooltime = 3, td = "RF,RB,BR", notusedskill = "32130" },
   { action_name = "CustomAction3", rate = 50, priority = 45, loop = -1, globalcooltime = 3, td = "LF,LB,BL", usedskill = "32130" },
   { action_name = "CustomAction4", rate = 50, priority = 45, loop = -1, globalcooltime = 3, td = "RF,RB,BR", usedskill = "32130" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1, notusedskill = "32130" },
   { action_name = "Stand_Invincible", rate = 2, loop = 1, usedskill = "32130" },
}

g_Lua_Skill = { 
-- 2 ������(32117:���ο�)
   { skill_index = 32117, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
   { skill_index = 32154, cooltime = 1000, globalcooltime = 6, rate = 100, priority = 50, rangemin = 0, rangemax = 3000, target = 3, notusedskill = "32130", selfhppercent = 85, limitcount = 1 },
   { skill_index = 32154, cooltime = 1000, globalcooltime = 6, rate = 100, priority = 50, rangemin = 0, rangemax = 3000, target = 3, notusedskill = "32130", selfhppercent = 70, next_lua_skill_index = 0, limitcount = 1 },

-- 4 �Ĺ����(3sec)
   { skill_index = 32105, cooltime = 5000, globalcooltime = 2, rate = 100, priority = 20, rangemin = 0, rangemax = 3000, target = 3, td = "BL,BR", randomtarget= "0.6,1", notusedskill = "32130" },
   { skill_index = 32133, cooltime = 5000, globalcooltime = 2, rate = 100, priority = 20, rangemin = 0, rangemax = 3000, target = 3, td = "BL,BR", randomtarget= "0.6,1", notusedskill = "32130" },

-- 6 �Ĺ����(3sec)
   { skill_index = 32110, cooltime = 5000, globalcooltime = 2, rate = 100, priority = 20, rangemin = 0, rangemax = 3000, target = 3, td = "BL,BR", randomtarget= "0.6,1", usedskill = "32130" },
   { skill_index = 32111, cooltime = 5000, globalcooltime = 2, rate = 100, priority = 20, rangemin = 0, rangemax = 3000, target = 3, td = "BL,BR", randomtarget= "0.6,1", usedskill = "32130" },

-- 13 slash(3sec)
   { skill_index = 32143, cooltime = 6000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
   { skill_index = 32143, cooltime = 6000, globalcooltime = 3, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", encountertime = 27000, notusedskill = "32130" },
   { skill_index = 32144, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FR,RF", encountertime = 27000, notusedskill = "32130" },
   { skill_index = 32145, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FL,LF", encountertime = 27000, notusedskill = "32130" },
   { skill_index = 32146, cooltime = 6000, globalcooltime = 3, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", encountertime = 27000, usedskill = "32130" },
   { skill_index = 32147, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FR,RF", encountertime = 27000, usedskill = "32130" },
   { skill_index = 32148, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FL,LF", encountertime = 27000, usedskill = "32130" },

-- WaveFront(3sec)
   { skill_index = 32135, cooltime = 11000, globalcooltime = 8, rate = 80, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "0.6,1", td = "LF,RF", notusedskill = "32130" },
   { skill_index = 32136, cooltime = 11000, globalcooltime = 8, rate = 80, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "0.6,1", td = "LB,RB", notusedskill = "32130" },

-- ���� ����(4sec)
   { skill_index = 32104, cooltime = 25000, rate = 90, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "0.6,1", td = "LF,RF,LB,RB", notusedskill = "32130" },

-- �극�� ����Ʈ(8sec)
   { skill_index = 32103, cooltime = 50000, globalcooltime = 7, rate = 70, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", notusedskill = "32130", selfhppercent = 85 },

-- Ȱ�� ����Ʈ
   { skill_index = 32141, cooltime = 50000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", selfhppercent = 95, notusedskill = "32130" },
-- �޺� (10sec)
   { skill_index = 32102, cooltime = 60000, globalcooltime = 7, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", notusedskill = "32130" },
-- ���� ����(9sec)
   { skill_index = 32116, cooltime = 60000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,8,0,1", notusedskill = "32130" },
-- Ʈ��(6sec) 32109 : Left 32132 : Right 32112 : Front
   { skill_index = 32112, cooltime = 40000, globalcooltime = 5, globalcooltime2 = 6, rate = 80, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", randomtarget= "0.6,1", notusedskill = "32130" },
   { skill_index = 32132, cooltime = 40000, globalcooltime = 5, globalcooltime2 = 6, rate = 80, rangemin = 0, rangemax = 3000, target = 3, td = "RF,RB", randomtarget= "0.6,1", notusedskill = "32130" },
   { skill_index = 32109, cooltime = 40000, globalcooltime = 5, globalcooltime2 = 6, rate = 80, rangemin = 0, rangemax = 3000, target = 3, td = "LF,LB", randomtarget= "0.6,1", notusedskill = "32130" },

-- ����(7sec) 32137 -> ��Ŀ�� ���鿡 ���� ��, ��Ŀ�� �Ѿư�.
   { skill_index = 32137, cooltime = 10000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 3000, target = 3, td = "RF,RB,LF,LB,BL,BR", notusedskill = "32130" },
   { skill_index = 32134, cooltime = 57000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 3000, target = 3, randomtarget= "1.6,1,1", td = "RF,RB,LF,LB", notusedskill = "32130" },

-- Ȱ��(20sec)
   { skill_index = 32119, cooltime = 80000, globalcooltime = 4, rate = 65, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,8,0,1", selfhppercent = 84, notusedskill = "32130" },
   { skill_index = 32120, cooltime = 80000, globalcooltime = 4, rate = 70, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,8,0,1", selfhppercent = 84, notusedskill = "32130" },
   { skill_index = 32121, cooltime = 80000, globalcooltime = 4, rate = 65, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,8,0,1", selfhppercent = 84, notusedskill = "32130" },
   { skill_index = 32140, cooltime = 80000, globalcooltime = 4, rate = -1, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 84, notusedskill = "32130" },

-- ����, ����, ��������
   { skill_index = 32115, cooltime = 20000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
   { skill_index = 32130, cooltime = 20000, rate = -1, rangemin = 0, rangemax = 3000, target = 1 },
   { skill_index = 32131, cooltime = 20000, rate = -1, rangemin = 0, rangemax = 3000, target = 1 },
}
