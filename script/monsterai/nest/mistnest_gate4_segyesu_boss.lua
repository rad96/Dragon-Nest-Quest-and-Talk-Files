--MistNest_Boss_Test.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_GlobalCoolTime1 = 7000 -- 4phase 긁기의 글타임
g_Lua_GlobalCoolTime2 = 35000 -- 1phase 스킬들의 기본 쿨타임 (엔트의 기본 공략 쿨타임은 30초)
g_Lua_GlobalCoolTime3 = 16000 -- 3phase 스킬들의 기본 쿨타임 (늑대인간의 기본 공략 쿨타임은 10~15초)
g_Lua_GlobalCoolTime4 = 30000 -- 4phase - 2페이즈와 동일한 페이즈공간에 있어 추가 되는 3페이즈 내려치기
g_Lua_GlobalCoolTime5 = 27000 -- 2phase 스킬들의 기본 쿨타임 (레이스-트릭제거의 기본 공략 쿨타임은 25초)
g_Lua_GlobalCoolTime6 = 30000 -- 4phase - Attack13_Wave
g_Lua_GlobalCoolTime7 = 50000 -- 4phase - Attack13_HoleBomb
g_Lua_GlobalCoolTime8 = 23000 -- 4phase - 뿌리묶기+내려치기 (4초+5초+10초), 구멍폭탄(약20초), 웨이브(7~8초), 팝콘(약20초), 밀쳐내기가 중첩되지 않도록 조절

g_Lua_Skill = { 
-- 연속 스킬
   { skill_index = 34686,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 팝콘 터뜨리기
   { skill_index = 34690,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 5000, target = 3 }, -- 브레스 좌측
-- 2,4,6,8,10 // Attack09_HoleBomb_1 // 4.3sec(260f) // 광역 폭발(2->5->7)(3->6->8)(x6->1->7~변경3->9->7)(8->2->4)(x3->1->7~변경6->3->1)
   { skill_index = 34570, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 3 },
   { skill_index = 34572, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 34571, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 5 },
   { skill_index = 34573, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 34574, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 7 },
   { skill_index = 34572, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 34567, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 9 },
   { skill_index = 34569, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 34568, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 11 },
   { skill_index = 34566, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },

-- (34556)Attack20_Right_Center_01 // 5sec(300f) // 바라보고 오른쪽 그라비티->내려치기 
-- (34557)Attack21_Right_Center_02 // 5sec(300f) // 중앙 오른쪽 그라비티->내려치기
-- (34558)Attack22_Right_Center_03 // 5sec(300f) // 바라보고 왼쪽 그라비티->내려치기
   { skill_index = 34556, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, 
   { skill_index = 34557, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 34558, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },   

-- Attack09_HoleBomb_1 // 4.3sec(260f) // 광역 폭발(2->5->7)(3->6->8)(x6->1->7~변경3->9->7)(8->2->4)(x3->1->7~변경6->3->1)
   { skill_index = 34567, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 2 },
   { skill_index = 34568, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 4 },
   { skill_index = 34568, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 6 },
   { skill_index = 34573, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 8 }, 
   { skill_index = 34571, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 10 },

-- // ----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- >>(4 Phase 스킬)<<

-- Attack13_Shout 밀쳐내면서 버프해제
   { skill_index = 34693, cooltime = 30000, globalcooltime = 8, rate = 20, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90, next_lua_skill_index = 15 },
   { skill_index = 34693, cooltime = 30000, globalcooltime = 8, rate = 30, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90, next_lua_skill_index = 16 },
   { skill_index = 34693, cooltime = 30000, globalcooltime = 8, rate = 40, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90, next_lua_skill_index = 17 },
   { skill_index = 34693, cooltime = 30000, globalcooltime = 8, rate = 50, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90, next_lua_skill_index = 18 },
   { skill_index = 34693, cooltime = 30000, globalcooltime = 8, rate = 60, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90, next_lua_skill_index = 19 },

-- (34684) Attack12_4Phase_Popcone
   { skill_index = 34684, cooltime = 30000, globalcooltime = 8, rate = 90, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 0, selfhppercent = 60 },      

-- Attack13_Wave_1 웨이브
   { skill_index = 34552, cooltime = 20000, globalcooltime = 6, globalcooltime2 = 8, rate = 60, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 60 },
   { skill_index = 34553, cooltime = 20000, globalcooltime = 6, globalcooltime2 = 8, rate = 65, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 60 },
   { skill_index = 34554, cooltime = 20000, globalcooltime = 6, globalcooltime2 = 8, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 60 },
   { skill_index = 34555, cooltime = 20000, globalcooltime = 6, globalcooltime2 = 8, rate = 75, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 60 },
   { skill_index = 34575, cooltime = 20000, globalcooltime = 6, globalcooltime2 = 8, rate = 80, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 60 },

-- (34692)Attack13_Vine // 5sec(300f) // 묶기 // 동일 스킬은 쿨이 같이 돔 // 내려치기 콤보
   { skill_index = 34692, cooltime = 20000, globalcooltime = 1, rate = 30, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90, next_lua_skill_index = 12 },
   { skill_index = 34692, cooltime = 20000, globalcooltime = 1, rate = 40, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90, next_lua_skill_index = 13 },
   { skill_index = 34692, cooltime = 20000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90, next_lua_skill_index = 14 },
   
-- (34687)Attack18_Left_Center_01 // 5.5sec (330f) // 왼팔_1 (바라보고 오른쪽 > 왼쪽) // 2페이즈 긁기
-- (34689)Attack19_Left_Center_02 // 5.5sec (330f) // 왼팔_2 (바라보고 왼쪽 > 오른쪽)
   { skill_index = 34687, cooltime = 18000, globalcooltime = 1, rate = 30, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90 },
   { skill_index = 34689, cooltime = 18000, globalcooltime = 1, rate = 30, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90 },

-- // ----------------------------------------------------------------------------------------------------------------------------------------------------------------
   
-- >>(1 Phase 스킬)<<
   { skill_index = 34694,  cooltime = 35000, rate = 70, rangemin = 0, rangemax = 2500, target = 3, globalcooltime = 2, usedskill = "34559", notusedskill = "34560,34561,34562" }, -- 안쪽 긁기
   { skill_index = 34695,  cooltime = 35000, rate = 80, rangemin = 0, rangemax = 2500, target = 3, globalcooltime = 2, usedskill = "34559", notusedskill = "34560,34561,34562" }, -- 바깥쪽 긁기
   
   { skill_index = 34695,  cooltime = 35000, rate = 70, rangemin = 2500, rangemax = 5000, target = 3, globalcooltime = 2, usedskill = "34559", notusedskill = "34560,34561,34562" }, -- 바깥쪽 긁기
   { skill_index = 34694,  cooltime = 35000, rate = 80, rangemin = 2500, rangemax = 5000, target = 3, globalcooltime = 2, usedskill = "34559", notusedskill = "34560,34561,34562" }, -- 안쪽 긁기
      
   { skill_index = 34683,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, encountertime = 20000, randomtarget = "1.6,0,1", priority = 30, usedskill = "34559", notusedskill = "34560,34561,34562" }, -- 땅 찍기 -- 엔트 수그리기 오더
   
-- >>(2 Phase 스킬)<<
   { skill_index = 34696,  cooltime = 27000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 5, usedskill = "34559,34560", notusedskill = "34561,34562" }, -- 왼팔_1 (바라보고 오른쪽 > 왼쪽), 이후 바라보고 왼쪽에 브레스
   { skill_index = 34697,  cooltime = 27000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 5, usedskill = "34559,34560", notusedskill = "34561,34562" }, -- 왼팔_2 (바라보고 왼쪽 > 오른쪽), 이후 바라보고 오른쪽에 브레스
      
-- >>(3 Phase 스킬)<<
   { skill_index = 34698,  cooltime = 16000, rate = 85, rangemin = 0, rangemax = 2500, target = 3, globalcooltime = 3, td = "RF", usedskill = "34559,34560,34561", notusedskill = "34562" }, -- 안쪽부터 1
   { skill_index = 34699,  cooltime = 16000, rate = 90, rangemin = 0, rangemax = 2500, target = 3, globalcooltime = 3, td = "RF,RB", usedskill = "34559,34560,34561", notusedskill = "34562" }, -- 안쪽부터 2
   { skill_index = 34700,  cooltime = 16000, rate = 95, rangemin = 0, rangemax = 2500, target = 3, globalcooltime = 3, td = "RF,RB", usedskill = "34559,34560,34561", notusedskill = "34562" }, -- 안쪽부터 3
   { skill_index = 34551,  cooltime = 16000, rate = 100, rangemin = 0, rangemax = 2500, target = 3, globalcooltime = 3, td = "RB", usedskill = "34559,34560,34561", notusedskill = "34562" }, -- 안쪽부터 4
   
   { skill_index = 34551,  cooltime = 16000, rate = 85, rangemin = 2500, rangemax = 5000, target = 3, globalcooltime = 3, td = "RB", usedskill = "34559,34560,34561", notusedskill = "34562" }, -- 안쪽부터 4
   { skill_index = 34700,  cooltime = 16000, rate = 90, rangemin = 2500, rangemax = 5000, target = 3, globalcooltime = 3, td = "RF,RB", usedskill = "34559,34560,34561", notusedskill = "34562" }, -- 안쪽부터 3
   { skill_index = 34699,  cooltime = 16000, rate = 95, rangemin = 2500, rangemax = 5000, target = 3, globalcooltime = 3, td = "RF,RB", usedskill = "34559,34560,34561", notusedskill = "34562" }, -- 안쪽부터 2
   { skill_index = 34698,  cooltime = 16000, rate = 100, rangemin = 2500, rangemax = 5000, target = 3, globalcooltime = 3, td = "RF", usedskill = "34559,34560,34561", notusedskill = "34562" }, -- 안쪽부터 1
}