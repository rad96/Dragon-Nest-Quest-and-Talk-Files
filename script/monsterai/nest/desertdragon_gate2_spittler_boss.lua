--AiSpittler_Green_Boss_Abyss.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

-- (1)스톰프 (2)연속폭발 (3) 라바 (4)라바,연속폭발,웨이브 (5)외치기,라바
g_Lua_GlobalCoolTime1 = 45000
g_Lua_GlobalCoolTime2 = 35000
g_Lua_GlobalCoolTime3 = 30000
g_Lua_GlobalCoolTime4 = 25000
g_Lua_GlobalCoolTime5 = 10000

g_Lua_SkillProcessor = {
   { skill_index = 31937, changetarget = "2000,1" },
}

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 24, loop = 3, custom_state1 = "custom_ground" },
   { action_name = "Attack2_Bite_Desert", rate = 16, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Stand_On", rate = 10, loop = 1, custom_state1 = "custom_underground" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 10, loop = 3, custom_state1 = "custom_ground" },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground" },
   { action_name = "Stand_On", rate = 10, loop = 1, custom_state1 = "custom_underground" },
}

g_Lua_Skill = { 

-- 침뱉기
   { skill_index = 31931, cooltime = 15000, rate = 70, rangemin = 400, rangemax = 1000, target = 3, custom_state1 = "custom_ground", randomtarget = 1.6, priority = 10 },
-- 외치기(5sec)
   { skill_index = 31932, cooltime = 20000, globalcooltime = 5, rate = 50, rangemin = 0, rangemax = 2000, target = 3, custom_state1 = "custom_ground", priority = 10 },
-- 라바 (10sec)
   { skill_index = 31942, cooltime = 65000, globalcooltime = 3, rate = 100, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", selfhppercent = 75, priority = 60 },
-- 스톰프 1회, 3회(7sec)
   { skill_index = 31934, cooltime = 45000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 2000, target = 3, custom_state1 = "custom_ground", selfhppercent = 74, priority = 10 },
   { skill_index = 31936, cooltime = 45000, globalcooltime = 1, rate = 60, rangemin = 0, rangemax = 2000, target = 3, custom_state1 = "custom_ground", selfhppercent = 74, priority = 10 },
-- 연속 폭발 1, 2 (12sec)
   { skill_index = 31938, cooltime = 35000, globalcooltime = 2, rate = 40, rangemin = 0, rangemax = 2000, target = 3, custom_state1 = "custom_ground", encountertime = 7000, priority = 10 },
   { skill_index = 31939, cooltime = 35000, globalcooltime = 2, rate = 40, rangemin = 0, rangemax = 2000, target = 3, custom_state1 = "custom_ground", encountertime = 7000, priority = 10 },
-- 밀쳐내기(75/50/25) 15sec
   { skill_index = 31937, cooltime = 45000, rate = 60, rangemin = 0, rangemax = 2000, target = 3, selfhppercent = 24, custom_state1 = "custom_ground", multipletarget = "1,4,0,1", priority = 30 },
-- 웨이브(등 뒤에서 피하기 11sec)
   { skill_index = 31933, cooltime = 55000, rate = 80, rangemin = 0, rangemax = 2000, target = 3, selfhppercent = 49, custom_state1 = "custom_ground", priority = 20 },
}
