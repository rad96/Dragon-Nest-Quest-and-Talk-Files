--AiSpittler_Green_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 4500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

-- (1)����,������,ȸ�� (2)��ġ (3)��ġ->����,������,ȸ�� (4)����(����) (5)���� (6)ȸ�� (7) ������,�޺�,����
g_Lua_GlobalCoolTime1 = 20000
g_Lua_GlobalCoolTime2 = 59000
g_Lua_GlobalCoolTime3 = 25000
g_Lua_GlobalCoolTime4 = 50000
g_Lua_GlobalCoolTime5 = 40000
g_Lua_GlobalCoolTime6 = 20000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1, selfhppercentrange = "25,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,24" },
   { action_name = "Walk_Left", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 4, loop = 1, td = "FL,FR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1, selfhppercentrange = "25,100" },
   { action_name = "Stand", rate = 2, loop = 1, selfhppercentrange = "0,24" },
   { action_name = "Walk_Front", rate = 10, loop = 2 , td = "FL,FR"},
   { action_name = "Walk_Left", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 4, loop = 1, td = "FL,FR" },
   { action_name = "Move_Front", rate = 4, loop = 1, td = "FL,FR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2, td = "FL,FR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3, td = "FL,FR" },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 4, td = "FL,FR" },
}

g_Lua_Skill = { 
-- ������(8sec) 
   { skill_index = 31976, cooltime = 50000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "25,72", priority = 10 },
   { skill_index = 31976, cooltime = 40000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,24", priority = 10 },
-- ���� ����(35sec)
   { skill_index = 31965, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1, selfhppercent = 75, priority = 60 },
   { skill_index = 31965, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1, selfhppercent = 50, priority = 60 },
   { skill_index = 31965, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1, selfhppercent = 25, priority = 60 },
-- ��ġ 1,2,3(16sec)
   { skill_index = 31962, cooltime = 59000, globalcooltime = 1, globalcooltime2 = 2, rate = 65, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 48, priority = 20 },
   { skill_index = 31963, cooltime = 59000, globalcooltime = 1, globalcooltime2 = 2, rate = 68, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 48, priority = 20 },
   { skill_index = 31964, cooltime = 59000, globalcooltime = 1, globalcooltime2 = 2, rate = 71, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 48, priority = 20 },
-- ���� 10, 20, 30�̻�(9sec)
   { skill_index = 31977, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 0, rangemax = 1100, target = 3, randomtarget = 1.5, selfhppercentrange = "25,100", priority = 10 },
   { skill_index = 31978, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 1100, rangemax = 2100, target = 3, randomtarget = 1.5, selfhppercentrange = "25,100", priority = 10 },
   { skill_index = 31979, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 2100, rangemax = 3100, target = 3, randomtarget = 1.5, selfhppercentrange = "25,100", priority = 10 },
   { skill_index = 31967, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 0, rangemax = 1100, target = 3, randomtarget = 1.5, selfhppercentrange = "0,24", priority = 10 },
   { skill_index = 31973, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 1100, rangemax = 2100, target = 3, randomtarget = 1.5, selfhppercentrange = "0,24", priority = 10 },
   { skill_index = 31975, cooltime = 40000, globalcooltime = 5, rate = 60, rangemin = 2100, rangemax = 3100, target = 3, randomtarget = 1.5, selfhppercentrange = "0,24", priority = 10 },
-- ������
   { skill_index = 31980, cooltime = 14000, rate = 70, rangemin = 0, rangemax = 500, target = 3, td = "FL,FR", priority = 10, selfhppercentrange = "25,100" },
   { skill_index = 31980, cooltime = 8000, rate = 70, rangemin = 0, rangemax = 500, target = 3, td = "FL,FR", priority = 10, selfhppercentrange = "0,24" },
-- �޺�
   { skill_index = 31974, cooltime = 21000, rate = 60, rangemin = 0, rangemax = 500, target = 3, td = "FL,FR", priority = 10, selfhppercentrange = "25,100" },
   { skill_index = 31974, cooltime = 15000, rate = 60, rangemin = 0, rangemax = 500, target = 3, td = "FL,FR", priority = 10, selfhppercentrange = "0,24" },
-- ����, 31979  ���� ���� 10���Ϳ�
   { skill_index = 31966, cooltime = 25000, rate = 70, rangemin = 500, rangemax = 1200, target = 3, randomtarget = 1.5, td = "FL,FR", priority = 10, selfhppercentrange = "25,100" },
   { skill_index = 31966, cooltime = 15000, rate = 70, rangemin = 500, rangemax = 1200, target = 3, randomtarget = 1.5, td = "FL,FR", priority = 10, selfhppercentrange = "0,24" },
-- ����ġ��
   { skill_index = 31968, cooltime = 10000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, td = "BL,BR", randomtarget = 0.6, priority = 10, selfhppercentrange = "25,100" },
   { skill_index = 31968, cooltime = 7000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, td = "BL,BR", randomtarget = 0.6, priority = 10, selfhppercentrange = "0,24" },
-- ȸ��(14sec)
   { skill_index = 31981, cooltime = 60000, rate = 70, rangemin = 0, rangemax = 2000, target = 3, multipletarget = "1,8,0,1", selfhppercentrange = "0,24", priority = 30 },
   { skill_index = 31982, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 2000, target = 3, multipletarget = "1,8,0,1", selfhppercentrange = "25,100", priority = 10 },
}
