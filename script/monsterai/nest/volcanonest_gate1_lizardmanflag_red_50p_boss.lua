-- /genmon 503701

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 8000

g_Lua_Near1 = 
{ 
    { action_name = "Stand", rate = 18, loop = 1 },
}
g_Lua_Near2 = 
{ 
    { action_name = "Stand", rate = 14, loop = 1 },    
}
g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 13, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Stand", rate = 15, loop = 1 },
}

g_Lua_Skill = {
    { skill_index = 36118, cooltime = 15000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, },--Attack09_ThrowSpear_Cliff_Volcano
}


