-- /genmon 503702

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 8000

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 18, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1 },    
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 3, loop = 1 },
   { action_name = "Assault", rate = 13, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Stand", rate = 3, loop = 1  },
    { action_name = "Assault", rate = 8, loop = 1 },
	{ action_name = "Move_Front", rate = 25, loop = 1 },
}

g_Lua_Assault = 
{ 
   { lua_skill_index = 0, rate = 10, approach = 700 },
}

g_Lua_GlobalCoolTime1 = 31000 

g_Lua_Skill = {
    -- //next// -- 
	{ skill_index = 36080, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3,},----Attack03_JumpAttack2_Volcano
	{ skill_index = 36081, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3, },-- Attack01_Slash_Volcano
	{ skill_index = 36082, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3, },-- Attack03_JumpAttack2_Volcano
	-- // Normal // --
	{ skill_index = 36081, cooltime = 10000, rate = 40, rangemin = 0, rangemax = 300, target = 3, },-- Attack01_Slash_Volcano(1,2,3,4)
    { skill_index = 36082, cooltime = 34000, rate = 50, rangemin = 500, rangemax = 1200, target = 3, next_lua_skill_index = 2 },-- Attack03_JumpAttack2_Volcano
    { skill_index = 36083, cooltime = 50000, rate = 60, rangemin = 300, rangemax = 1000, target = 3, randomtarget = "1.5,0,1" },--Attack04_Wep_Volcano(1,2,3,4)
    { skill_index = 36079, cooltime = 50000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, },--Attack08_RollingAttack_Volcano  >화상데미지 15000버전
    -- { skill_index = 36098, cooltime = 30000, rate = 40, rangemin = 0, rangemax = 2000, target = 3,priority = 10,encountertime=10000 },--Attack02_Chop_VolNest
	{ skill_index = 36085, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3, },-- Attack10_DigGround_Start_VolNest
	{ skill_index = 36395, cooltime = 80000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, encountertime = 20000 },-- 신규스킬 마킹  Attack09_ExplosionMarking_Trial 화상데미지 15000적용
	{ skill_index = 36285, cooltime = 70000, rate = 50, rangemin = 0, rangemax = 2000, target = 3, },-- 신규스킬 소환2배 Attack05_SpawnDouble_Trial
}


