--AiMinotauros_Red_Boss_Nest.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 3500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 20000 -- ����&�������.

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Attack001_Bash", rate = 4, loop = 1 },
   { action_name = "Attack003_Chop", rate = 5, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 3 },
   { action_name = "Walk_Right", rate = 8, loop = 3 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 3 },
   { action_name = "Walk_Right", rate = 8, loop = 3 },
   { action_name = "Move_Left", rate = 10, loop = 1 },
   { action_name = "Move_Right", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 35, loop = 1 }, 
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 3 },
   { action_name = "Walk_Right", rate = 8, loop = 3 },
   { action_name = "Move_Left", rate = 10, loop = 1 },
   { action_name = "Move_Right", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 35, loop = 2 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 3 },
   { action_name = "Walk_Right", rate = 8, loop = 3 },
   { action_name = "Move_Left", rate = 10, loop = 1 },
   { action_name = "Move_Right", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 35, loop = 2 },
}

g_Lua_Skill = { 
   { skill_index = 33266, cooltime = 300, rate = -1, rangemin = 0, rangemax = 3000, target = 3,},--�羵�������
   { skill_index = 33270, cooltime = 90000, rate = 100, rangemin = 0, rangemax = 3000, target = 3,next_lua_skill_index=0,encountertime=35000 },--�����ϴ�!
   { skill_index = 33271, cooltime = 30000, rate = 40, rangemin = 0, rangemax = 1500, target = 3,encountertime=15000,selfhppercentrange = "0,50", globalcooltime=1},--����
   { skill_index = 33269, cooltime = 60000, rate = 45, rangemin = 0, rangemax = 800, target = 3,encountertime=5000 },--Shout : ��������.
   { skill_index = 33272, cooltime = 45000, rate = 50, rangemin = 0, rangemax = 15000, target = 3,selfhppercentrange = "0,75",globalcooltime=1 },--�������: "���� ������"
   { skill_index = 33273, cooltime = 37000, rate = 40, rangemin = 0, rangemax = 1500, target = 3,selfhppercentrange = "0,50",},--���������� : "���߹��� �� ��"
}