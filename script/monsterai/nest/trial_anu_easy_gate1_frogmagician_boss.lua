g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 900;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 4, loop = 1, selfhppercentrange = "25,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,24" },
   { action_name = "Move_Left", rate = 6, loop = 2, globalcooltime = 3 },
   { action_name = "Move_Right", rate = 6, loop = 2, globalcooltime = 4 },
   { action_name = "Move_Back", rate = 2, loop = 1 },
   { action_name = "Move_Teleport_Left", rate = 6, loop = 1, td = "RF,RB", globalcooltime = 3 },
   { action_name = "Move_Teleport_Right", rate = 6, loop = 1, td = "LF,LB", globalcooltime = 4 },
   { action_name = "Attack01_YoYo", rate = 10, loop = 1},
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 2, globalcooltime = 3 },
   { action_name = "Move_Right", rate = 2, loop = 2, globalcooltime = 4 },
   { action_name = "Move_Teleport_Left", rate = 6, loop = 1, td = "RF,RB", globalcooltime = 3 },
   { action_name = "Move_Teleport_Right", rate = 6, loop = 1, td = "LF,LB", globalcooltime = 4 },
   { action_name = "Walk_Left", rate = 4, loop = 2 },
   { action_name = "Walk_Right", rate = 4, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
}

g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
}

g_Lua_SkillProcessor = {
   { skill_index = 34001, changetarget = "100,0" },
   { skill_index = 34004, changetarget = "100,0" },
} 

-- 궁
g_Lua_GlobalCoolTime1 = 15000
-- 블라
g_Lua_GlobalCoolTime2 = 35000
-- 텔레
g_Lua_GlobalCoolTime3 = 10000
g_Lua_GlobalCoolTime4 = 10000
-- 창과방패
g_Lua_GlobalCoolTime5 = 15000
-- 무엇이나올까(알쏭달쏭)
g_Lua_GlobalCoolTime6 = 40000
-- 무엇이나올까(알쏭달쏭) / 서먼홀 분리
g_Lua_GlobalCoolTime7 = 10000

 g_Lua_RandomSkill ={
{
{ -- 알쏭달쏭 패턴 3개 랜덤스킬 묶음 index =1
   { skill_index = 34157, rate= 30 }, 
   { skill_index = 34158, rate= 35 },
   { skill_index = 34159, rate= 35 },
},
{ -- 텔레포트  패턴 2개 랜덤스킬 묶음 index = 2
   { skill_index = 34165, rate= 25 },  --좌로 텔레포트
   { skill_index = 34166, rate= 25 },  --우로 텔레포트 
   { skill_index = 34170, rate= 50 },  --맵의 특정 위치로 텔레포트
},
}
}

g_Lua_Skill = { 
-- 순간이동(아직 ㄴㄴ해)
   { skill_index = 34006, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 90, rangemin = 0, rangemax = 5000, target = 1, limitcount = 1, selfhppercent = 75  },
   { skill_index = 34006, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 80, rangemin = 0, rangemax = 5000, target = 1, limitcount = 1, selfhppercent = 50 },
   { skill_index = 34006, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 70, rangemin = 0, rangemax = 5000, target = 1, limitcount = 1, selfhppercent = 25 },
-- 창병 소환
   { skill_index = 34009, cooltime = 100000, globalcooltime = 5, globalcooltime2=7, rate = 100, priority = 30, rangemin = 0, rangemax = 5000, target = 1, selfhppercentrange = "50,100" }, -- 5초씩 줄어든게 원본
   { skill_index = 34009, cooltime = 80000, globalcooltime = 5, globalcooltime2=7, rate = 100, priority = 30, rangemin = 0, rangemax = 5000, target = 1, selfhppercentrange = "0,49" }, -- 5초씩 줄어든게 원본
-- 마이크로
   { skill_index = 34156, cooltime = 30000, rate = 70, rangemin = 0, rangemax = 600, target = 3 },
-- 버블버블 다크
   { skill_index = 34001, cooltime = 30000, rate = 70, rangemin = 600, rangemax = 1200, target = 3, randomtarget = "1.6,0,1" },
-- 서먼블랙홀
   { skill_index = 34016, cooltime = 70000, globalcooltime = 2, globalcooltime2 = 7, globalcooltime2 = 7, rate = 70, rangemin = 0, rangemax = 600, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 8 },
   { skill_index = 34002, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4500, target = 3 },
   { skill_index = 34002, cooltime = 70000, globalcooltime = 2, globalcooltime2 = 7, rate = 70, rangemin = 500, rangemax = 1000, target = 3, randomtarget = "1.6,0,1" },
-- 버프삭제
   { skill_index = 34170, cooltime = 100000, rate = 90, rangemin = 0, rangemax = 5000, target = 3 },
-- 무엇이나올까
   { skill_index = 34157, skill_random_index = 1, cooltime = 90000,  globalcooltime = 5, globalcooltime2 = 7, rate = 70, priority = 30, rangemin = 0, rangemax = 5000, target = 1, next_lua_skill_index = 12},
   { skill_index = 34165, skill_random_index = 2, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4500, target = 3 },
-- 방벽 추가
   { skill_index = 34018, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 1, selfhppercent = 49, limitcount = 1 },
}



