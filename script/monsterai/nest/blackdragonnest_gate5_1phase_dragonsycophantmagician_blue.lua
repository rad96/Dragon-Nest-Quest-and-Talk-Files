--AiDragonSycophantMagician_Blue_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 5200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 4000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 4 },
}

g_Lua_Skill = { 
   { skill_index = 34921, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
   { skill_index = 34934, cooltime = 7000, rate = 80, rangemin = 0, rangemax = 1200, target = 3 },
   { skill_index = 34932, cooltime = 17000, rate = 60, rangemin = 400, rangemax = 800, target = 3 },
   { skill_index = 34933, cooltime = 30000, rate = 40, rangemin = 300, rangemax = 1500, target = 3, encountertime = 20000 },
}
