--/genmon 503781

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 6000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop=1,td = "FR,FL",notusedskill = 36185 },
   { action_name = "Stand_EX", rate = 3, loop=1,td = "FR,FL",usedskill = 36185 },
   { action_name = "Walk_Left", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Walk_Right", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 3, loop=1,td = "FR,FL",notusedskill = 36185 },
   { action_name = "Stand_EX", rate = 3, loop=1,td = "FR,FL",usedskill = 36185 },
   { action_name = "Walk_Front", rate = 8, loop=1,td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Walk_Right", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 8, loop = 1,td = "FR,FL",notusedskill = 36185 },
   { action_name = "Stand_EX", rate = 8, loop = 1,td = "FR,FL",usedskill = 36185 },
   { action_name = "Move_Front", rate = 13, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1, loop=1,td = "FR,FL",notusedskill = 36185 },
   { action_name = "Stand_EX", rate = 1, loop=1,td = "FR,FL",usedskill = 36185 },
   { action_name = "Move_Front", rate = 15, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}

g_Lua_GlobalCoolTime1 = 50000
g_Lua_GlobalCoolTime2 = 65000 

g_Lua_RandomSkill =
{{
   {{ skill_index = 36171, rate = 50, next_lua_skill_index = 0 },{ skill_index = 36186, rate = 50 ,next_lua_skill_index = 0,},},---1.Attack04_VolNest_ShortBreathEXL_Start //Attack04_VolNest_ShortBreathEXR_Start
   {{ skill_index = 36167, rate = 40, next_lua_skill_index = 0 },{ skill_index = 36167, rate = 60 ,next_lua_skill_index = 1 },},---2.Attack03_VolNest_Flutter_Start  //stomp or SelfStreem
   {{ skill_index = 36167, rate = 40, next_lua_skill_index = 0 },{ skill_index = 36167, rate = 60 ,next_lua_skill_index = 2 },},---3.Attack03_VolNest_Flutter_Start  //stomp or SelfStreem
   -- {{ skill_index = 36188, rate = 25, next_lua_skill_index = 0 },{ skill_index = 36189, rate = 25 ,next_lua_skill_index = 0},{ skill_index = 36190, rate = 25 ,next_lua_skill_index = 0 },{ skill_index = 36191, rate = 25 ,next_lua_skill_index = 0 },},---4.Attack06_VolNest_LinearBreath12_Start 12,3,6,9
}}
g_Lua_Skill = { 
	--  Next_Lua_Skill --FG
	{ skill_index = 36168, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 ,custom_state1 = "custom_Fly", td = "FL,FR,LF,RF,RB,BR,BL,LB" },--Attack03_VolNest_Stomp -- 0
	{ skill_index = 36172, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_Fly", next_lua_skill_index = 0, td = "FL,FR,LF,RF,RB,BR,BL,LB" },--Attack05_VolNest_SelfStreem_Start --1
	{ skill_index = 36173, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_Fly", next_lua_skill_index = 0, td = "FL,FR,LF,RF,RB,BR,BL,LB" },--Attack05_VolNest_SelfStreemEX_Start --2
	{ skill_index = 36180, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_Ground", td = "FL,FR,LF,RF,RB,BR,BL,LB"},--Attack10_VolNest_Spawn --3
	--  LinePattern -- GG
    { skill_index = 36175, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80, limitcount = 1, priority=99, custom_state1 = "custom_Ground" ,td = "FL,FR,LF,RF,RB,BR,BL,LB" },--Attack07_VolNest_CircleBreathReady80_Start
    { skill_index = 36176, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 60, limitcount = 1, priority=98, custom_state1 = "custom_Ground" ,td = "FL,FR,LF,RF,RB,BR,BL,LB"},--Attack07_VolNest_CircleBreathReady60_Start
    { skill_index = 36177, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 40, limitcount = 1, priority=97, custom_state1 = "custom_Ground" ,td = "FL,FR,LF,RF,RB,BR,BL,LB"},--Attack07_VolNest_CircleBreathReady40_Starts
	{ skill_index = 36178, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 20, limitcount = 1, priority=96, custom_state1 = "custom_Ground" ,td = "FL,FR,LF,RF,RB,BR,BL,LB" ,globalcooltime= 1,},--Attack07_VolNest_CircleBreathReady20_Start
	{ skill_index = 36179, cooltime = 85000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 19, priority=95 ,custom_state1 = "custom_Ground" ,td = "FL,FR,LF,RF,RB,BR,BL,LB", globalcooltime= 1,},--Attack07_VolNest_CircleBreathReady19_Start
	-- GF --  NODA
	{ skill_index = 36167, cooltime = 50000,  rate = 50, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "40,100", custom_state1 = "custom_Ground", td = "FL,FR,LF,RF,RB,BR,BL,LB", skill_random_index = 2 },--Attack03_VolNest_Flutter_Start
	{ skill_index = 36167, cooltime = 50000,  rate = 50, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,40", custom_state1 = "custom_Ground", td = "FL,FR,LF,RF,RB,BR,BL,LB", skill_random_index = 3 },--Attack03_VolNest_Flutter_Start
	-- Pattern --  GG
	{ skill_index = 36185, cooltime = 1000,  rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority=110, notusedskill = 36185, selfhppercent=40, custom_state1 = "custom_Ground" ,td = "FL,FR,LF,RF,RB,BR,BL,LB" },--Attack10_VolNest_Fury
	-- ShortBreath -- GF
	{ skill_index = 36170, cooltime = 55000,  rate = 40, rangemin = 0, rangemax = 5000, target = 3, notusedskill = 36185, custom_state1 = "custom_Ground", next_lua_skill_index = 0, },--Attack04_VolNest_ShortBreath_Start
	{ skill_index = 36171, cooltime = 55000,  rate = 40, rangemin = 0, rangemax = 5000, target = 3, usedskill = 36185, custom_state1 = "custom_Ground", skill_random_index = 1 },--Attack04_VolNest_ShortBreathEXL_Start
	{ skill_index = 36186, cooltime = 55000,  rate = -1, rangemin = 0, rangemax = 5000, target = 3, usedskill = 36185, custom_state1 = "custom_Ground", },--Attack04_VolNest_ShortBreathEXR_Start
	-- Normal -- GG 
	{ skill_index = 36161, cooltime = 8000,   rate = 40, rangemin = 0, rangemax = 600, target = 3, notusedskill = 36185, custom_state1 = "custom_Ground" ,td ="FL,RF"},  --Attack00_VolNest_Slash_Start
	{ skill_index = 36162, cooltime = 8000,   rate = 40, rangemin = 0, rangemax = 1200, target = 3, usedskill = 36185, custom_state1 = "custom_Ground" ,td ="FL,RF"},     --Attack00_VolNest_SlashEX_Start
	{ skill_index = 36163, cooltime = 15000,  rate = 50, rangemin = 0, rangemax = 600, target = 3, notusedskill = 36185, custom_state1 = "custom_Ground",td ="FL,RF"},  --Attack01_VolNest_DoubleSlash_Start
	{ skill_index = 36164, cooltime = 15000,  rate = 50, rangemin = 0, rangemax = 1200, target = 3, usedskill = 36185 ,custom_state1 = "custom_Ground",td ="FL,RF"},     --Attack01_VolNest_DoubleSlashEX_Start
	{ skill_index = 36165, cooltime = 30000,  rate = 70, rangemin = 0, rangemax = 1500, target = 3, notusedskill = 36185 ,custom_state1 = "custom_Ground",td ="FL,RF",}, --Attack02_VolNest_Dash_Start
	{ skill_index = 36166, cooltime = 30000,  rate = 70, rangemin = 0, rangemax = 1500, target = 3, usedskill = 36185 ,custom_state1 = "custom_Ground",td ="FL,RF"},      --Attack02_VolNest_DashWing_Start
	
	{ skill_index = 36180, cooltime = 65000, rate = 30, rangemin = 0, rangemax = 5000, target = 3, notusedskill = 36185, custom_state1 = "custom_Ground", td = "FL,FR,LF,RF,RB,BR,BL,LB",globalcooltime=2},--Attack07_VolNest_OverPower_Start --3
	{ skill_index = 36181, cooltime = 65000, rate = 30, rangemin = 0, rangemax = 5000, target = 3, usedskill = 36185, custom_state1 = "custom_Ground", td = "FL,FR,LF,RF,RB,BR,BL,LB",globalcooltime=2},--Attack07_VolNest_OverPowerEX_Start --4
	-- GG --
	{ skill_index = 36182, cooltime = 65000,  rate = 80, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "40,80",notusedskill = 36185, multipletarget = "2,2,0,1", custom_state1 = "custom_Ground",td = "FL,FR,LF,RF,RB,BR,BL,LB" },--Attack08_VolNest_PlantFire_Start
	{ skill_index = 36183, cooltime = 65000,  rate = 80, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,40",usedskill = 36185, multipletarget = "2,4,0,1", custom_state1 = "custom_Ground",td = "FL,FR,LF,RF,RB,BR,BL,LB"  },--Attack08_VolNest_PlantFireEX_Start
	-- Finally -- GG
	{ skill_index = 36184, cooltime = 90000,  rate = 80, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,20", custom_state1 = "custom_Ground",  next_lua_skill_index = 0, td = "FL,FR,LF,RF,RB,BR,BL,LB"},--Attack09_VolNest_RushandDash_Start
}
