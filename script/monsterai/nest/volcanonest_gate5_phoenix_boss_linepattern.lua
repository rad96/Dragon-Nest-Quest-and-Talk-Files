--/genmon 503788

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000
g_Lua_NearValue2 = 6000


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 600
g_Lua_AssualtTime = 5000



g_Lua_Near1 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 36194, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, next_lua_skill_index = 1,priority=100}, --Attack06_CircleBreath_Start_VolNest
   { skill_index = 36195, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, }, --Attack03_StompEND_VolNest 
}
