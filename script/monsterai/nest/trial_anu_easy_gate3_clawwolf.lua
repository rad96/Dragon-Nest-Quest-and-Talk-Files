--AiClawwolf_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 2000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Move_Back", rate = 4, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1, selfhppercentrange = "50,100" },
   { action_name = "Move_Front", rate = 6, loop = 1, selfhppercentrange = "50,100"} ,
   { action_name = "Walk_Front", rate = 6, loop = 1, selfhppercentrange = "0,49"},
   { action_name = "Move_Front", rate = 10, loop = 1, selfhppercentrange = "0,49"},
   
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 10, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2, selfhppercentrange = "50,100" },
   { action_name = "Move_Front", rate = 6, loop = 2, selfhppercentrange = "50,100" },
   { action_name = "Walk_Front", rate = 10, loop = 2, selfhppercentrange = "0,49"},
   { action_name = "Move_Front", rate = 10, loop = 2, selfhppercentrange = "0,49"},
   { action_name = "Assault", rate = 5, loop = 1, selfhppercentrange = "50,100" },
   { action_name = "Assault", rate = 15, loop = 1, selfhppercentrange = "0,49"},
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 7, loop = 1, selfhppercentrange = "50,100" },
   { action_name = "Assault", rate = 15, loop = 1, selfhppercentrange = "0,49"},
}
g_Lua_Assault = { 
    { action_name = "Attack03_TripleSteb", rate = 40, loop = 1, approach = 250 },
}

g_Lua_GlobalCoolTime1 = 40000
g_Lua_GlobalCoolTime2 = 10000

 g_Lua_RandomSkill ={
{
{ -- ���� ���� ���� 1 
   { skill_index = 34185, rate= 35 }, 
   { skill_index = 34186, rate= 35 },
   { skill_index = 34187, rate= 30 },
},
}
}

g_Lua_Skill = { 
   
   { skill_index = 34181, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 500, target = 1,limitcount=1, priority=20 }, -- ������ �ߵ�
   { skill_index = 34184, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 90, limitcount = 1, encountertime= 20000 }, -- �׸��� ��ȯ
   { skill_index = 34626,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "51,75", priority = 50, limitcount = 1 }, -- Ʈ����
   { skill_index = 34627,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "25,50",  priority = 50, limitcount =1 }, -- Ʈ���� - �����ϴ� �ñ� 
   { skill_index = 34183,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,24", priority = 50, limitcount = 1 }, -- Ʈ����
   
   { skill_index = 33666,  cooltime = 12000, rate = 35, rangemin = 0, rangemax = 400, target = 3, priority = 10 }, -- 3�� ���
   { skill_index = 33667,  cooltime = 24000, rate = 35, rangemin = 0, rangemax = 500, target = 3, priority = 10 }, -- �޺� 
   
   { skill_index = 34636,  cooltime = 24000, rate = 45, rangemin = 600, rangemax = 1200, target = 3, selfhppercent = 90, priority = 20 }, -- ���
   { skill_index = 34634,  cooltime = 80000, rate = 50, rangemin = 600, rangemax = 1500, target = 3, selfhppercent = 75, priority = 20, randomtarget = "0.2,0,1", next_lua_skill_index= 9 }, -- ���������
   { skill_index = 34635,  cooltime = 1000, rate = -1 ,rangemin = 0, rangemax = 5000, randomtarget = "0.2,0,1", target = 3, next_lua_skill_index= 10 },
   { skill_index = 34637,  cooltime = 1000, rate = -1 ,rangemin = 0, rangemax = 5000, randomtarget = "0.2,0,1", target = 3 },

   { skill_index = 34622, cooltime = 70000, rate = 35, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "75,95", priority = 20, encountertime = 30000, globalcooltime = 2 }, -- ȣ�� 1������
   { skill_index = 34623, cooltime = 70000, rate = 35, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "50,74",  priority = 20, globalcooltime = 2  }, -- ȣ�� 2������
   { skill_index = 34624, cooltime = 70000, rate = 35, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "25, 49",  priority = 20, globalcooltime = 2  }, -- ȣ�� 3������ 
   { skill_index = 34625, cooltime = 70000, rate = 35, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0, 24",  priority = 20, globalcooltime = 2 }, -- ȣ�� 4������ 
 
   { skill_index = 34185, cooltime = 150000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "75,100", globalcooltime = 1, globalcooltime2 = 2, priority = 25, encountertime= 35000 }, -- ���� 1������ - ����2
   
   { skill_index = 34185, cooltime = 130000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "50,74", globalcooltime = 1, globalcooltime2 = 2, priority = 25 }, -- ���� 2������ - ����2
   { skill_index = 34186, cooltime = 130000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "50,74", globalcooltime = 1, globalcooltime2 = 2, priority = 25 }, -- ���� 2������ - ����2

   { skill_index = 34185, cooltime = 110000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "25,49", globalcooltime = 1, globalcooltime2 = 2, priority = 25 }, -- ���� 3������ - ����2
   { skill_index = 34187, cooltime = 110000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "25,49", globalcooltime = 1, globalcooltime2 = 2, priority = 25 }, -- ���� 3������ -����3
   { skill_index = 34188, cooltime = 110000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "25,49",  globalcooltime = 1, globalcooltime2 = 2, priority = 25 }, -- ���� 3������ -����4
   
   { skill_index = 34188, cooltime = 110000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "0,24",  globalcooltime = 1, globalcooltime2 = 2, priority = 25 }, -- ���� 4������ -����4
}
