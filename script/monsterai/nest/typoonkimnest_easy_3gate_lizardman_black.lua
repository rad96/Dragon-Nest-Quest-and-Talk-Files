--AiLizardman_Black_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1000;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 8, loop = 3  },
   { action_name = "Move_Back", rate = 8, loop = 2  },
   { action_name = "Attack11_ShieldChim", rate = 2, loop = 1, checkweapon=2  },
   { action_name = "Attack1_Piercing", rate = 4, loop = 1  },
   { action_name = "Attack2_Pushed", rate = 3, loop = 1, checkweapon=2  },
   { action_name = "Attack13_BigSwing", rate = 4, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 8, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 8, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 6, loop = 2  },
   { action_name = "Attack11_ShieldChim", rate = 4, loop = 1, checkweapon=2  },
   { action_name = "Attack1_Piercing", rate = 2, loop = 1  },
   { action_name = "Attack12_ShieldCharge", rate = 2, loop = 1 },
   { action_name = "Attack13_BigSwing", rate = 8, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 6, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 12, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 9, loop = 2  },
   { action_name = "Move_Back", rate = 7, loop = 2  },
   { action_name = "Attack13_BigSwing", rate = 8, loop = 2  },
   { action_name = "Attack3_DashAttack", rate = 10, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Assault", rate = 11, loop = 1  },
   { action_name = "Attack3_DashAttack", rate = 10, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_MeleeDefense = { 
   { action_name = "Attack11_ShieldChim", rate = -1, loop = 1, checkweapon=2  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
}
g_Lua_RangeDefense = { 
   { action_name = "Attack12_ShieldCharge", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack12_ShieldCharge", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack5_JumpAttack", rate = 30, loop = 2, cancellook = 0, approach = 250.0  },
   { action_name = "Attack9_ThrowSpear", rate = 10, loop = 2, cancellook = 0, approach = 250.0  },
}
