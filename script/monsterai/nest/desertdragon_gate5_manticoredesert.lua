g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 3000;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 500;
g_Lua_AssualtTime = 3000;

g_Lua_GlobalCoolTime1 = 50000
g_Lua_GlobalCoolTime2 = 90000
-- 슬라이딩
g_Lua_GlobalCoolTime3 = 30000
-- 헤비스템프,레이즈그라비티
g_Lua_GlobalCoolTime4 = 10000
-- 날기
g_Lua_GlobalCoolTime5 = 70000


g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 5, loop = 2, custom_state1 = "custom_ground" },
	{ action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
	{ action_name = "Walk_Left", rate = 1, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Right", rate = 1, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Back", rate = 4, loop = 1, custom_state1 = "custom_ground", td = "FL,RF" },
	{ action_name = "Attack16_SlidingBash", rate = 12, priority = 10, loop = 1, custom_state1 = "custom_ground", td = "FL,LF,FR,RF", globalcooltime = 3, encountertime = 15000 },
	{ action_name = "Attack07_LHook_EX", rate = 7, priority = 10, loop = 1, custom_state1 = "custom_ground", td = "FL" },
	{ action_name = "Attack08_RHook_EX", rate = 7, priority = 10, loop = 1, custom_state1 = "custom_ground", td = "FR" },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 2, loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
	{ action_name = "Walk_Left", rate = 1, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Right", rate = 1, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Front", rate = 3, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Attack16_SlidingBash", rate = 6, priority = 10, loop = 1, custom_state1 = "custom_ground", td = "FL,LF,FR,RF", globalcooltime = 3, encountertime = 15000 },
	{ action_name = "Attack07_LHook_EX", priority = 10, rate = 3, loop = 1, custom_state1 = "custom_ground", td = "FL" },
	{ action_name = "Attack08_RHook_EX", priority = 10, rate = 3, loop = 1, custom_state1 = "custom_ground", td = "FR" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 2, loop = 1, custom_state1 = "custom_ground", td = "FL,FR" },
	{ action_name = "Attack16_SlidingBash", rate = 6, priority = 10, loop = 1, custom_state1 = "custom_ground", td = "FL,LF,FR,RF", globalcooltime = 3, encountertime = 15000 },
	{ action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
	{ action_name = "Walk_Left", rate = 2, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Right", rate = 2, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Front", rate = 4, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 2, loop = 1, custom_state1 = "custom_ground" },
	{ action_name = "Turn_Left", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 10, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
	{ action_name = "Walk_Front", rate = 5, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
}

g_Lua_Skill = { 
-- 0 블랙홀 EX -> 9sec (발사체 7초)
	{ skill_index = 32082, cooltime = 69000, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", selfhppercent = 70, randomtarget = "1.6", multipletarget = "1,1,0,1" },
-- 1 2 꼬리치기
	{ skill_index = 32097, cooltime = 10000, rate = 90, priority = 10, rangemin = 0, rangemax = 600, target = 3, randomtarget = "0.6", td = "BL,LB", custom_state1 = "custom_ground" },
	{ skill_index = 32098, cooltime = 10000, rate = 90, priority = 10, rangemin = 0, rangemax = 600, target = 3, randomtarget = "0.6", td = "BR,RB", custom_state1 = "custom_ground" },

-- 3 헤비 점프EX -> 3.5sec
	{ skill_index = 32084, cooltime = 30000, rate = 80, priority = 10, rangemin = 1000, rangemax = 3000, target = 3, randomtarget = "1.6", custom_state1 = "custom_ground", td = "FL,LF,FR,RF" },
-- 4 헤비 스탬프EX -> 4.5sec
	{ skill_index = 32083, cooltime = 40000, globalcooltime = 4, rate = 80, priority = 10, rangemin = 0, rangemax = 1000, target = 3, custom_state1 = "custom_ground", encountertime = "10000" },
-- 5 빅 그라비티 볼EX -> 3.5sec
	{ skill_index = 32081, cooltime = 50000, rate = 60, priority = 10, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", multipletarget = "1,2,0,1", encountertime = "10000" },
-- 6 레이즈 그라비티EX(구슬 4개) -> 8.3sec
	{ skill_index = 32087, cooltime = 30000, globalcooltime = 4, rate = 60, priority = 10, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground" },

-- 7 거스트EX(밀쳐내고 전방에 바람) -> 6.5sec
	{ skill_index = 32088, cooltime = 90000, rate = 40, priority = 10, rangemin = 0, rangemax = 3000, target = 3, custom_state1 = "custom_ground", td = "FL,LF,FR,RF", selfhppercent = 73 },
-- 8 스펙트럼EX(밀쳐내고 점프 발사체 발사) -> 7sec
	{ skill_index = 32086, cooltime = 32000, rate = 70, priority = 10, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", selfhppercent = 70 },

-- 9 10 11 날기
	{ skill_index = 32093, cooltime = 70000, globalcooltime = 5, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", selfhppercentrange = "51,75", next_lua_skill_index = 12 },
	{ skill_index = 32093, cooltime = 70000, globalcooltime = 5, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", selfhppercentrange = "26,50", next_lua_skill_index = 13 },
	{ skill_index = 32093, cooltime = 70000, globalcooltime = 5, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", selfhppercentrange = "0,25", next_lua_skill_index = 15 },
-- 12 에어 그라비티볼EX(난이도 쉬움)
	{ skill_index = 32085, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_fly", multipletarget = "1,8,0,1" },
-- 13 에어 그라비티볼EX(난이도 쉬움)
	{ skill_index = 32096, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_fly", multipletarget = "1,8,0,1", next_lua_skill_index = 14 },
-- 14 에어 거스트EX1
	{ skill_index = 32089, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_fly", multipletarget = "1,4,0,1" },
-- 15 황사
	{ skill_index = 32090, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_fly", next_lua_skill_index = 14 },
}
