--AiCalamary_Blue_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1600;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Attack1_Slash", rate = 9, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 1, loop = 1, approach = 300 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 300 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 300 },
   { action_name = "Attack1_Slash", rate = 16, loop = 1, approach = 300 },
}
g_Lua_Skill = { 
   -- ������ �̻��� : ����
   { skill_index = 20882,  cooltime = 24000, rate = 80,rangemin = 300, rangemax = 1200, target = 3, randomtarget=1.1, selfhppercentrange = "50,100" },
   -- �ñ� ��ǳ
   { skill_index = 20876,  cooltime = 30000, rate = 80, rangemin = 0, rangemax = 700, target = 3, randomtarget=1.1 },
   -- ������ ����
   { skill_index = 33106,  cooltime = 72000, rate = 80, rangemin = 0, rangemax = 700, target = 3, randomtarget=1.1 },
   -- { skill_index = 20879,  cooltime = 72000, rate = 80, rangemin = 0, rangemax = 700, target = 3, randomtarget=1.1 },
   -- �ñ� ������
   { skill_index = 33154,  cooltime = 44000, rate = 60, rangemin = 100, rangemax = 1100, target = 3, selfhppercent = 50, randomtarget=1.1 },
   -- �˼� ���� �ñ�
   { skill_index = 33141,  cooltime = 36000, rate = 60, rangemin = 100, rangemax = 1100, target = 3, selfhppercent = 50, randomtarget=1.1 },
   -- ���� �̴Ͼ� ��ȯ
   -- { skill_index = 33207,  cooltime = 90000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, randomtarget=1.1 },
   -- ���� ���̽��� 
   { skill_index = 33209,  cooltime = 70000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,50" , randomtarget=1.1 },
}