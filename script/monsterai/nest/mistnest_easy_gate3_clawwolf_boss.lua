--AiClawwolf_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 2000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Move_Back", rate = 4, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1, selfhppercentrange = "50,100", notusedskill = "34638" },
   { action_name = "Move_Front", rate = 6, loop = 1, selfhppercentrange = "50,100", notusedskill = "34638" },
   { action_name = "Walk_Front", rate = 6, loop = 1, selfhppercentrange = "0,49", usedskill = "34638" },
   { action_name = "Move_Front", rate = 10, loop = 1, selfhppercentrange = "0,49", usedskill = "34638" },
   
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 10, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2, selfhppercentrange = "50,100", notusedskill = "34638" },
   { action_name = "Move_Front", rate = 6, loop = 2, selfhppercentrange = "50,100", notusedskill = "34638" },
   { action_name = "Walk_Front", rate = 10, loop = 2, selfhppercentrange = "0,49", usedskill = "34638" },
   { action_name = "Move_Front", rate = 10, loop = 2, selfhppercentrange = "0,49", usedskill = "34638" },
   { action_name = "Assault", rate = 5, loop = 1, selfhppercentrange = "50,100", notusedskill = "34638" },
   { action_name = "Assault", rate = 15, loop = 1, selfhppercentrange = "0,49", usedskill = "34638" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 7, loop = 1, selfhppercentrange = "50,100", notusedskill = "34638" },
   { action_name = "Assault", rate = 15, loop = 1, selfhppercentrange = "0,49", usedskill = "34638" },
}
g_Lua_Assault = { 
    { action_name = "Attack03_TripleSteb", rate = 40, loop = 1, approach = 250 },
}


g_Lua_GlobalCoolTime1 = 180000 -- 늑대 울음
g_Lua_GlobalCoolTime2 = 140000 -- 사용처 없음
g_Lua_GlobalCoolTime3 = 120000 -- 광분
g_Lua_GlobalCoolTime4 = 20000 -- 광분, 늑대울음, 호출 공용 쿨타임


g_Lua_Skill = { 
   { skill_index = 34638,  cooltime = 5000, rate = -1, rangemin = 0,  rangemax = 3000,  target = 1, limitcount =1, selfhppercent = 50, priority = 60 }, -- 변신
   
   { skill_index = 34626,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "51,75", notusedskill = "34638", priority = 50, limitcount = 1 }, -- 트랩샷
   { skill_index = 34627,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 50, notusedskill = "34638" , priority = 55, limitcount =1 }, -- 트랩샷 - 변신하는 시기 
   { skill_index = 34633,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,25", usedskill = "34638" , priority = 50, limitcount = 1 }, -- 트랩샷
   
   
   { skill_index = 33666,  cooltime = 12000, rate = 35, rangemin = 0, rangemax = 400, target = 3, priority = 10 }, -- 3단 찌르기
   { skill_index = 33667,  cooltime = 24000, rate = 35, rangemin = 0, rangemax = 500, target = 3, priority = 10 }, -- 콤보 
   
   { skill_index = 34636,  cooltime = 24000, rate = 45, rangemin = 600, rangemax = 1200, target = 3, selfhppercent = 90, priority = 20 }, -- 기습
   { skill_index = 34640,  cooltime = 40000, rate = 45, rangemin = 2000, rangemax = 5000, target = 3, selfhppercentrange = "50,90", notusedskill = "34638", priority = 30, randomtarget = "1.6,1,1" }, -- 안개 가르기
   { skill_index = 34634,  cooltime = 80000, rate = 50, rangemin = 600, rangemax = 1500, target = 3, selfhppercentrange = "0,49",usedskill = "34638", priority = 20, randomtarget = "0.2,0,1", next_lua_skill_index= 9  }, -- 어퍼충격파
   { skill_index = 34635,  cooltime = 1000, rate = -1 ,rangemin = 0, rangemax = 3000,randomtarget = "0.2,0,1", target = 3, next_lua_skill_index= 10 },
   { skill_index = 34637,  cooltime = 1000, rate = -1 ,rangemin = 0, rangemax = 3000,randomtarget = "0.2,0,1", target = 3 },

   { skill_index = 34622, cooltime = 40000, rate = 35, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "75,100", notusedskill = "34638", priority = 20, encountertime = 7000, globalcooltime = 4 }, -- 호출 1페이즈
   { skill_index = 34623, cooltime = 40000, rate = 35, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "50,74", notusedskill = "34638", priority = 20, globalcooltime = 4  }, -- 호출 2페이즈
   { skill_index = 34624, cooltime = 70000, rate = 35, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "25, 49", usedskill = "34638", priority = 20, globalcooltime = 4  }, -- 호출 3페이즈 
   { skill_index = 34625, cooltime = 70000, rate = 35, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0, 24", usedskill = "34638", priority = 20, globalcooltime = 4  }, -- 호출 4페이즈 
   
   { skill_index = 34628,  cooltime = 180000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "50,90", notusedskill = "34638", priority = 25, globalcooltime = 4  }, -- 늑대울음 1/2 페이즈 - 패턴 1   
   
   { skill_index = 34628,  cooltime = 180000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "25,50", usedskill = "34638", globalcooltime = 4, globalcooltime2 = 1, priority = 25 }, -- 늑대울음 3 페이즈 - 패턴 1
   { skill_index = 34653,  cooltime = 180000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "25,50", usedskill = "34638", globalcooltime = 4, globalcooltime2 = 1, priority = 25 }, -- 늑대울음 3 페이즈 - 패턴 2
   { skill_index = 34658,  cooltime = 180000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "25,50", usedskill = "34638", globalcooltime = 4, globalcooltime2 = 1, priority = 25  }, -- 늑대울음 3 페이즈 - 패턴 3
   
   { skill_index = 34653,  cooltime = 180000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,24", usedskill = "34638", globalcooltime = 4, globalcooltime2 = 1, priority = 25 }, -- 늑대울음 4 페이즈 - 패턴 2
   { skill_index = 34658,  cooltime = 180000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,24", usedskill = "34638", globalcooltime = 4, globalcooltime2 = 1, priority = 25 }, -- 늑대울음 4 페이즈 - 패턴 3
   
   { skill_index = 34641, cooltime = 120000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "25,49", usedskill = "34638", globalcooltime = 4, globalcooltime2 = 3, priority = 25,   }, -- 광분 3페이즈 - 패턴 1
   { skill_index = 34642, cooltime = 120000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "25,49", usedskill = "34638", globalcooltime = 4, globalcooltime2 = 3, priority = 25,  }, -- 광분 3페이즈 - 패턴 2
     
   { skill_index = 34642, cooltime = 110000, rate = 40, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,24", usedskill = "34638", globalcooltime = 4, globalcooltime2 = 3, priority = 25, }, -- 광분 4페이즈 - 패턴2
   { skill_index = 34643, cooltime = 110000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,24", usedskill = "34638", globalcooltime = 4, globalcooltime2 = 3, priority = 25, }, -- 광분 4페이즈 -패턴3
   { skill_index = 34644, cooltime = 110000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,24", usedskill = "34638", globalcooltime = 4, globalcooltime2 = 3, priority = 25, }, -- 광분 4페이즈 -패턴4
}
