--DesertDragon_Wraith_Desert.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1700;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssaultTime = 5000

g_Lua_SkillProcessor = {
   { skill_index = 34610, changetarget = "1700,0" },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1, selfhppercentrange = "50,100" },
   { action_name = "Stand", rate = 2, loop = 1, selfhppercentrange = "0,49" },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Assault", rate = 9, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Assault = { 
   { lua_skill_index = 5, rate = 16, approach = 400 }
}

g_Lua_GlobalCoolTime1 = 150000 -- 가로베기 연속 처음에 시작하는 용도로 쓸 때
g_Lua_GlobalCoolTime2 = 66000 -- 가로베기 연속, 감옥, 구울 소환
g_Lua_GlobalCoolTime3 = 10000

g_Lua_Skill=
{
-- 콤보용 연속 스킬
   { skill_index = 34654,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 5000, target = 3, combo1 = "1,100,0" }, -- 끌어당기기
   { skill_index = 34600,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 5000, target = 3 }, -- 돌진
   
-- 가로베기 연속시 콥보용
   { skill_index = 34608, cooltime = 1000, rate = -1,rangemin = 0, rangemax = 5000, target = 3 }, -- A
   { skill_index = 34595, cooltime = 1000, rate = -1,rangemin = 0, rangemax = 5000, target = 3 }, -- B
   { skill_index = 34594, cooltime = 1000, rate = -1,rangemin = 0, rangemax = 5000, target = 3 }, -- C

-- 콤보어택 // (34614)어설트 (34598)일반 공격
   { skill_index = 34614, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "0,100,0" },
   { skill_index = 34598, cooltime = 26000, rate = 70, rangemin = 0, rangemax = 400, target = 3, combo1 = "0,100,0" },
-- 가로베기 연속(20sec)
   { skill_index = 34605, cooltime = 1000, globalcooltime = 2, globalcooltime2 = 1, rate = 100, priority = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, combo1 = "2,30,0", combo2 = "3,40,0", combo3 = "4,100,0", limitcount = 1, notusedskill = "34603" },
   { skill_index = 34605, cooltime = 180000, globalcooltime = 2, globalcooltime2 = 1, rate = 60, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, combo1 = "2,30,0", combo2 = "3,45,0", combo3 = "4,100,0", notusedskill = "34603" },
-- 감옥보내기(30sec)
   { skill_index = 34606, cooltime = 100000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 50, randomtarget = "1.6,0,1", notusedskill = "34603", next_lua_skill_index = 10 },
   { skill_index = 34593,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2500, target = 3, randomtarget = "1.6,0,1" }, -- 타겟변경
-- 구울소환(20sec)
   { skill_index = 34603, cooltime = 140000, globalcooltime = 2, rate = 80, rangemin = 0, rangemax = 5000, target = 3, encountertime = 15000 },

-- 가로베기
   { skill_index = 34611, cooltime = 60000, rate = 70, rangemin = 0, rangemax = 3000, target = 3, notusedskill = "34603" },
-- 세로베기   
   { skill_index = 34612, cooltime = 70000, rate = 60, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", notusedskill = "34603" },
   
}