--TypoonKimNest_2Gate_BoarMan_Warrior.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 450;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1200;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack3_Air", 0 },
      { "Attack7_Nanmu_Black", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 10, loop = 1  },
   { action_name = "Attack02_DoubleSwing", rate = 10, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack02_DoubleSwing", rate = 11, loop = 1  },
   { action_name = "Attack01_Whirlwind", rate = 9, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Walk_Back", rate = 10, loop = 2  },
   { action_name = "Attack01_Whirlwind", rate = 9, loop = 1  },
   { action_name = "Attack03_TripleSwing", rate = 11, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Attack03_TripleSwing", rate = 8, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
   { action_name = "Move_Back", rate = 1, loop = 2  },
}
g_Lua_Near6 = { 
   { action_name = "Stand", rate = 3, loop = 3  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 10, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
}
