--AiSpittler_Green_Boss_Abyss.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 1300;
g_Lua_NearValue2 = 8500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 32078, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 8000, target = 3, limitcount = 1 },
}
