--DesertDragon_Wraith_Desert.lua
--데저트 드래곤 네스트 - 레이스 데저트
--503071
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack1_Cut_DesertDragon", rate = 12, loop = 1  },
   { action_name = "Attack4_BackAttack_DesertDragon", rate = 15, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 6, loop = 2  },
   { action_name = "Walk_Right", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = 6, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 8, loop = 1  },
   { action_name = "Move_Right", rate = 8, loop = 1  },
   { action_name = "Attack3_ThrowScythe_DesertDragon", rate = 9, loop = 1, cooltime = 16000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  }, 
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 8, loop = 1  },
   { action_name = "Attack5_DashAttack_DesertDragon", rate = 9, loop = 1, cooltime = 19000 },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Attack3_ThrowScythe_DesertDragon", rate = 20, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
}
g_Lua_NonDownMeleeDamage = { 
   { action_name = "Attack4_BackAttack_DesertDragon", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 10, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Cut_DesertDragon", rate = 10, loop = 1, approach = 200.0  },
   { action_name = "Attack4_BackAttack_DesertDragon", rate = 5, loop = 1, approach = 200.0  },
   { action_name = "Attack5_DashAttack_DesertDragon", rate = 4, loop = 1, approach = 200.0  },
}
