g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 1000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 14, loop = 1 },
   { action_name = "Attack1_BiteCombo", rate = 16, loop = 1, cooltime = 3000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 14, loop = 1 },
   { action_name = "Walk_Front", rate = 16, loop = 1, cooltime = 3000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 16, loop = 2, cooltime = 3000 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 16, loop = 3, cooltime = 3000 },
}

g_Lua_Skill = { 
   { skill_index = 34020, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 1 },
}
