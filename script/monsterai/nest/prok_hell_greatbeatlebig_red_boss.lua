--AiGreatBeatle_Green_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Attack01_Slash", rate = 19, loop = 1, max_missradian = 10 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 19, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2 },
   { action_name = "Move_Front", rate = 16, loop = 1 },
   { action_name = "Assault", rate = 19, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 8, loop = 1, approach = 200 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 200 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 200 },
   { action_name = "Attack01_Slash", rate = 12, loop = 1, max_missradian = 10, approach = 200 },
}
g_Lua_Skill = {
   --화염공격
   -- { skill_index = 33107,  cooltime = 45000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, encountertime = 15000, globalcooltime = 1 , selfhppercentrange = "85,100"},
   -- { skill_index = 33107,  cooltime = 45000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, encountertime = 10000, globalcooltime = 1 , selfhppercentrange = "60,75"},
   -- { skill_index = 33107,  cooltime = 45000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, encountertime = 10000, globalcooltime = 1 , selfhppercentrange = "35,50"},
   -- { skill_index = 33107,  cooltime = 45000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, encountertime = 10000, globalcooltime = 1 , selfhppercentrange = "0,25"},
   --강한콤보공격
   { skill_index = 33138,  cooltime = 20000, rate = 50, rangemin = 0, rangemax = 800, target = 3 }, 
   --강한도약공격
   { skill_index = 33139,  cooltime = 30000, rate = 50, rangemin = 400, rangemax = 1500, target = 3, loop = 1 },
   --쫄 소환
   -- { skill_index = 33111,  cooltime = 65000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, encountertime = 30000, globalcooltime = 1 },
   --파고들기
   -- { skill_index = 33218,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, encountertime = 33000, globalcooltime = 1, selfhppercentrange = "85,100" },
   -- { skill_index = 33218,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, encountertime = 33000, globalcooltime = 1, selfhppercentrange = "60,75" },
   -- { skill_index = 33218,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, encountertime = 33000, globalcooltime = 1 , selfhppercentrange = "35,50" },
   -- { skill_index = 33218,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, encountertime = 33000, globalcooltime = 1 , selfhppercentrange = "0,25" },
   --돌진 공격
   { skill_index = 33152,  cooltime = 25000, rate = 50, rangemin = 400, rangemax = 1500, target = 3, encountertime = 10000 },
}
