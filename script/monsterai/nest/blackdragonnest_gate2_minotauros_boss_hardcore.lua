--/genmon 503611
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 750;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1650;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
   { action_name = "Stand_1", rate = 4, loop = 1, selfhppercentrange = "50,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,50" },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand_1", rate = 2, loop = 1, selfhppercentrange = "50,100" },
   { action_name = "Stand", rate = 2, loop = 1, selfhppercentrange = "0,50" },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 3 },
   { action_name = "Assault", rate = 10, loop = 1 },
}

g_Lua_GlobalCoolTime1 = 20000
g_Lua_GlobalCoolTime2 = 16000

g_Lua_Assault = 
{ 
   { lua_skill_index = 0, rate = 10, approach = 300 },
   { lua_skill_index = 1, rate = 5, approach = 600 },
}

g_Lua_Skill = {
--Attack001_Bash_BD(Assault)
    { skill_index = 34731, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3,},
--Attack017_Combo_BD(Assault)
    { skill_index = 34740, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3 },
--Attack006_FireWall_NextAction(34744)Attack007_Stomp_BD(34961)
    { skill_index = 34744, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, next_lua_skill_index = 3 },
    { skill_index = 34961, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 8000, target = 3 },
--Attack019_WeaponPower_BD_Start//T24(�� �������°�)
    { skill_index = 34742, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, priority = 100 },
    { skill_index = 34730, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, priority = 100 },
--Attack011_CallMassiveFire_BD_Start//T21T26(��ǳ��)
    { skill_index = 34738, cooltime = 1000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 90, selfhppercent = 75, limitcount = 1 },
    { skill_index = 34738, cooltime = 1000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 90, selfhppercent = 50, limitcount = 1, next_lua_skill_index = 4 },
    { skill_index = 34738, cooltime = 1000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 90, selfhppercent = 25, limitcount = 1, next_lua_skill_index = 5 },

--Attack018_Aim_BD(����ȭ)
    { skill_index = 34741, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 6000, target = 1, priority = 80, selfhppercent = 45, encountertime = 300000, notusedskill="34741" },

--FearShout_BD 1������	
    { skill_index = 34733, cooltime = 50000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 74 },
--Attack008_BigBash_BD
    { skill_index = 34736, cooltime = 50000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 49 },

--Attack001_Bash_BD
    { skill_index = 34731, cooltime = 8000, rate = 40, rangemin = 0, rangemax = 300, target = 3,},
--Attack017_Combo_BD
    { skill_index = 34740, cooltime = 20000, rate = 40, rangemin = 0, rangemax = 600, target = 3 },
--Attack010_FireMeteor_BD_Start//T26
    { skill_index = 34962, cooltime = 40000, rate = 60, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", multipletarget = "1,4,0,1", encountertime = 20000 },
--Attack020_CallCow_BD
    { skill_index = 34743, cooltime = 50000, globalcooltime = 1, rate = 40, rangemin = 0, rangemax = 5000, target = 3 },
--Attack006_FireWall_BD(140+40+165, 6sec)
    { skill_index = 34734, cooltime = 34000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 2 },
--Attack015_Dash_BD_Start
    { skill_index = 34739, cooltime = 20000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1" },
}