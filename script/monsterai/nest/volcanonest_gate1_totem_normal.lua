--/genmon 503708

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000
g_Lua_NearValue2 = 6000


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
}


g_Lua_Skill = { 
   { skill_index = 36127, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, priority=300,next_lua_skill_index=1  },--Attack01_VolNest_HitReady_Start
   { skill_index = 36126, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, priority=200,  },--Attack01_VolNest_Hit
   { skill_index = 36130, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, },--Attack03_VolNest_TotemBreak_Start
   { skill_index = 36125, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, },--Attack03_VolNest_TotemBreak
}