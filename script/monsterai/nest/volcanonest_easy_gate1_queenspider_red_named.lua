-- /genmon 503702

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 500
g_Lua_AssaultTime = 8000

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 18, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1 },    
}
g_Lua_Near3 = 
{ 
   { action_name = "Move_Front", rate = 3, loop = 1 },
   { action_name = "Assault", rate = 13, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Assault", rate = 15, loop = 1 },
	{ action_name = "Move_Front", rate = 3, loop = 1 },
}

g_Lua_Assault = 
{ 
   { lua_skill_index = 2, rate = 5, approach = 600},
}

g_Lua_RandomSkill =
{{{
   { skill_index = 36086, rate = 16 },--06
   { skill_index = 36087, rate = 12 },--12
   { skill_index = 36088, rate = 12 },--09
   { skill_index = 36089, rate = 12 },--03
   { skill_index = 36090, rate = 12 },--07
   { skill_index = 36091, rate = 12 },--05
   { skill_index = 36092, rate = 12 },--02
   { skill_index = 36093, rate = 12 },--10
},}}

g_Lua_GlobalCoolTime1 = 31000 

g_Lua_Skill = {
	-- // MAIN_SKILL // --
    { skill_index = 36086, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,skill_random_index=1 },-- Attack11_TotemShield1_Start_Volcano --(T112)
    { skill_index = 36094, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, priority = 100, target = 3,selfhppercent = 50, limitcount = 1,next_lua_skill_index= 0 },--Attack02_TotemShieldReady_Volcano --50P(T103)
    -- //next// -- 
	{ skill_index = 39097, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3, priority = 1 },-- Attack03_JumpAttack2_Volcano
	-- // Normal // --
	{ skill_index = 36081, cooltime = 10000, rate = 20, rangemin = 0, rangemax = 500, target = 3, },-- Attack01_Slash_Volcano(1,2,3,4)
    { skill_index = 36082, cooltime = 24000, rate = 40, rangemin = 500, rangemax = 1200, target = 3, next_lua_skill_index = 2 },-- Attack03_JumpAttack_Volcano
    { skill_index = 36083, cooltime = 40000, rate = 60, rangemin = 300, rangemax = 1000, target = 3, randomtarget = "1.5,0,1" },--Attack04_Wep_Volcano(1,2,3,4)
    { skill_index = 36084, cooltime = 30000, rate = 70, rangemin = 0, rangemax = 1200, target = 3, },--Attack08_RollingAttack_Volcano
    { skill_index = 36098, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 3000, target = 3 },--Attack02_Chop_VolNest
}


