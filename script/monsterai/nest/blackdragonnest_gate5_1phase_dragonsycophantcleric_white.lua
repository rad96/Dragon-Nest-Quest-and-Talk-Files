--AiDragonSycophantCleric_White_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 300;
g_Lua_NearValue4 = 1450;
g_Lua_NearValue5 = 5200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Attack4_Kick_BlackDragon", rate = 16, loop = 1 },
   { action_name = "Attack5_MaceCombo_BlackDragon", rate = 8, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Attack5_MaceCombo_BlackDragon", rate = 16, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Attack6_ShieldCharge_BlackDragon", rate = 27, loop = 1 },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 4 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack6_ShieldCharge_BlackDragon", rate = 7, loop = 1, approach = 250 },
   { action_name = "Attack4_Kick_BlackDragon", rate = 4, loop = 1, approach = 150 },
   { action_name = "Walk_Left", rate = 3, loop = 1, approach = 150 },
   { action_name = "Walk_Right", rate = 3, loop = 1, approach = 150 },
}

g_Lua_Skill = { 
   { skill_index = 34921, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
}