--AiGoblin_Green_Normal.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000
g_Lua_NearValue2 = 5000


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 100, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 100, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 34172,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 1 }, 
   { skill_index = 34652,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 1 },  
}

