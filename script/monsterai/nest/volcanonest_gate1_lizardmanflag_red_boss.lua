-- /genmon 503701

g_Lua_NearTableCount = 4
g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 5000;
g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 8000

g_Lua_Near1 = 
{ 
    { action_name = "Stand", rate = 18, loop = 1 },
	{ action_name = "Walk_Left", rate = 8, loop = 1 },
    { action_name = "Walk_Right", rate = 8, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2 },
   { action_name = "Move_Front", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 3 },
   { action_name = "Assault", rate = 8,loop = 1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 3 },
   { action_name = "Assault", rate = 7, loop = 1 },
}
g_Lua_Assault = 
{ 
   { lua_skill_index = 4, rate = 10, approach = 500 },
}

g_Lua_RandomSkill =
{
{
{
   { skill_index = 36110, rate = 16 },--06
   { skill_index = 36111, rate = 12 },--12
   { skill_index = 36112, rate = 12 },--09
   { skill_index = 36113, rate = 12 },--03
   { skill_index = 36114, rate = 12 },--07
   { skill_index = 36115, rate = 12 },--05
   { skill_index = 36116, rate = 12 },--02
   { skill_index = 36117, rate = 12 },--10
},
{
   { skill_index = 36110, rate = 16 ,next_lua_skill_index= 2},--06
   { skill_index = 36111, rate = 12 ,next_lua_skill_index= 2},--12
   { skill_index = 36112, rate = 12 ,next_lua_skill_index= 2},--09
   { skill_index = 36113, rate = 12 ,next_lua_skill_index= 2},--03
   { skill_index = 36114, rate = 12 ,next_lua_skill_index= 2},--07
   { skill_index = 36115, rate = 12 ,next_lua_skill_index= 2},--05
   { skill_index = 36116, rate = 12 ,next_lua_skill_index= 2},--02
   { skill_index = 36117, rate = 12 ,next_lua_skill_index= 2},--10
},
{
   { skill_index = 36102, rate = 30 ,},--Attack08_SummonTotem_Slow_Volcano
   { skill_index = 36103, rate = 70 ,},--Attack08_SummonTotem_Volcano
},
}
}

g_Lua_GlobalCoolTime1 = 20000
g_Lua_GlobalCoolTime2 = 90000 

g_Lua_Skill = {
	-- // MAIN_SKILL // --
    { skill_index = 36110, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, priority = 90, target = 3,skill_random_index=1 },-- Attack14_TotemDefensel_Start_Volcano --75p (0)
    { skill_index = 36110, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, priority = 90, target = 3,skill_random_index=2, },-- Attack14_TotemDefensel_Start_Volcano --50p (1)
    { skill_index = 36109, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, priority = 80, target = 3, next_lua_skill_index= 3 },--Attack16_Laught (2)
    { skill_index = 36107, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, priority = 80, target = 3, },--Attack13_BigSwing_Volcano_Event --DownFloor (3)
	{ skill_index = 36106, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 800, target = 3, },--Attack13_BigSwing_Volcano(4)
    { skill_index = 36108, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, priority = 100, target = 3,selfhppercent = 75, limitcount = 1, next_lua_skill_index= 0 },--Attack08_TotemShieldReady_Volcano --75p(T101)
    { skill_index = 36108, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, priority = 100, target = 3,selfhppercent = 50, limitcount = 1, next_lua_skill_index= 1, },--Attack08_TotemShieldReady_Volcano --50p(T101)
    { skill_index = 36108, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 3000, priority = 100, target = 3,selfhppercent = 25, next_lua_skill_index= 0 },--Attack08_TotemShieldReady_Volcano --25pLoop(T101)
	-- // Normal // --
    { skill_index = 36101, cooltime = 15000, rate = 60, rangemin = 0, rangemax = 800, target = 3, },-- Attack03_DashAttack_Volcano(1,2,3,4)
    { skill_index = 36102, cooltime = 25000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, skill_random_index=3, randomtarget = "1.6,0,1", multipletarget = "1,2,0,1",  },--Attack08_SummonTotem_Slow_Volcano(1,2,3,4)
    { skill_index = 36103, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, skill_random_index=3 },--Attack08_SummonTotem_Volcano(1,2,3,4)
    { skill_index = 36106, cooltime = 15000, rate = 30, rangemin = 0, rangemax = 800, target = 3, },--Attack13_BigSwing_Volcano(1,2,3,4)
    { skill_index = 36104, cooltime = 25000, rate = 70, rangemin = 0, rangemax = 3000, target = 3, },--Attack09_ThrowSpear_Volcano(1,2,3,4)
}


