g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 750;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1650;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 3 },
   { action_name = "Assault", rate = 10, loop = 1 },
}

g_Lua_Assault = 
{ 
   { lua_skill_index = 0, rate = 10, approach = 300 },
   { lua_skill_index = 1, rate = 10, approach = 300 },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 10000
g_Lua_GlobalCoolTime3 = 5000

g_Lua_Skill = {
--Attack001_ChopBD(Assault)
   { skill_index = 34766, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },
--Attack015_ComboBD(Assault)
   { skill_index = 34782, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },
--Attack004_AssaultBD_Start
   { skill_index = 34770, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },
--Attack010_BindTargetBD
   { skill_index = 34777, cooltime = 1000, globalcooltime1 = 3, rate = 100, priority = 100, rangemin = 0, rangemax = 6000, target = 3, multipletarget = "1,8,0,1", selfhppercent = 50, limitcount = 1 }, 
   { skill_index = 34777, cooltime = 1000, globalcooltime1 = 3, rate = 100, priority = 100, rangemin = 0, rangemax = 6000, target = 3, multipletarget = "1,8,0,1", selfhppercent = 25, limitcount = 1 },
--Attack007_BuffABD(3.5sec)
   { skill_index = 34773, cooltime = 60000, globalcooltime = 2, rate = 100, priority = 90, rangemin = 0, rangemax = 6000, target = 1, selfhppercent = 45, notusedskill = "34773" },
--Attack005_WhirlWindBD_Start(13sec)
   { skill_index = 34771 , cooltime = 63000, globalcooltime = 1, globalcooltime2 = 2, rate = 90, rangemin = 0, rangemax = 6000, target = 3, selfhppercent = 49 },
--Attack013_DeathGroundBD_Start(4sec)
   { skill_index = 34780, cooltime = 44000, globalcooltime = 1, rate = 90, rangemin = 0, rangemax = 6000, target = 3, multipletarget = "1,8,0,1", selfhppercent = 24  },
--Attack014_CurseShakeBD_Start(13.5sec)
   { skill_index = 34781, cooltime = 63000, globalcooltime = 1, globalcooltime2 = 3, rate = 90, rangemin = 0, rangemax = 6000, target = 3, randomtarget = "1.6,0,1" },
--Attack001_ChopBD.lua(3.5sec)
   { skill_index = 34766, cooltime = 10000, rate = 60, rangemin = 0, rangemax = 400, target = 3, encountertime = 5000 },
--Attack011_MegaBombChopBD(6.5sec)
   { skill_index = 34778, cooltime = 45000, rate = 60, rangemin = 0, rangemax = 6000, target = 3  },
--Attack012_ComeOnBD(7.5sec)
   { skill_index = 34779, cooltime = 30000, rate = 60, rangemin = 0, rangemax = 1200, target = 3, next_lua_skill_index = 2, encountertime = 5000 },
--Attack003_JumpChopBD(4sec)
   { skill_index = 34783, cooltime = 16000, rate = 70, rangemin = 800, rangemax = 1200, target = 3, randomtarget = "1.6,0,1", encountertime = 5000 },
--Attack015_ComboBD(5.5sec)
   { skill_index = 34782, cooltime = 35000, rate = 60, rangemin = 0, rangemax = 1200, target = 3, encountertime = 5000 },
--Attack006_NandongBD_Start(8.5sec)
   { skill_index = 34772, cooltime = 45000, rate = 60, rangemin = 0, rangemax = 1200, target = 3, randomtarget = "1.6,0,1", encountertime = 5000 },
--Attack008_GhostBreathBD(4sec)
   { skill_index = 34775, cooltime = 60000, rate = 70, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", encountertime = 5000 },
}