--AiDropElf_Green_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 20, loop = 2 },
   { action_name = "Attack02_VineWhip", rate = 50, loop = 1, notusedskill = "33641" },
   { action_name = "Attack04_SnatchLeg", rate = 50, loop = 1, usedskill = "33641" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 15, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 20, loop = 2 },
}

g_Lua_Skill = { 
   { skill_index = 33641,  cooltime = 5000, rate = 100,rangemin = 0,  rangemax = 3000,  target = 1, limitcount =1, selfhppercent = 50 },
}
