g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 1100;
g_Lua_NearValue2 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200

-- (1)������ ġ��,��� (2)�޼� ġ��,��� (3)����,��ȣ�� (4)��ġ
g_Lua_GlobalCoolTime1 = 10000
g_Lua_GlobalCoolTime2 = 10000
g_Lua_GlobalCoolTime3 = 8000
g_Lua_GlobalCoolTime4 = 60000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 2 },
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 16, loop = 2 },
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_Skill = { 
-- 0 ����
   { skill_index = 32003, cooltime = 80000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,8,0,1", globalcooltime = 3, encountertime = 20000, notusedskill = "32004,32218", priority = 20 },
-- 1 2 ����ȭ
   { skill_index = 32004, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 1, notusedskill = "32004,32218", limitcount = 1, priority = 60 },
   { skill_index = 32218, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 1, notusedskill = "32218", limitcount = 1, priority = 60, encountertime = 600000 },
-- 3 4 5 ��ȣ��
   { skill_index = 32001, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, limitcount = 1, globalcooltime = 3, priority = 50 },
   { skill_index = 32001, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 50, limitcount = 1, globalcooltime = 3, priority = 50, next_lua_skill_index = 1 },
   { skill_index = 32001, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 25, limitcount = 1, globalcooltime = 3, priority = 50 },
-- 6 ����
   { skill_index = 32010, cooltime = 40000, rate = 40, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", notusedskill = "32004,32218", priority = 10 },
-- 7 ������
   { skill_index = 32011, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 74, notusedskill = "32004,32218", priority = 10 },
-- 8 9 10 11 12 13 ��ġ
   { skill_index = 32022, cooltime = 60000, rate = 55, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 74, globalcooltime = 4, notusedskill = "32004,32218", priority = 10 },
   { skill_index = 32025, cooltime = 60000, rate = 56, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 74, globalcooltime = 4, notusedskill = "32004,32218", priority = 10 },
   { skill_index = 32027, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 74, globalcooltime = 4, notusedskill = "32004,32218", priority = 10 },
   { skill_index = 32023, cooltime = 60000, rate = 58, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 74, globalcooltime = 4, notusedskill = "32004,32218", priority = 10 },
   { skill_index = 32024, cooltime = 60000, rate = 59, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 74, globalcooltime = 4, notusedskill = "32004,32218", priority = 10 },
   { skill_index = 32026, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 74, globalcooltime = 4, notusedskill = "32004,32218", priority = 10 },
-- 14 15 �÷�ġ�� �޺�
   { skill_index = 32002, cooltime = 25000, rate = 50, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", next_lua_skill_index = 15, notusedskill = "32004,32218", priority = 10 },
   { skill_index = 32012, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 16 17 ġ�� ������ �޺�
   { skill_index = 32007, cooltime = 30000, rate = 75, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 15000, next_lua_skill_index = 17, globalcooltime = 1, notusedskill = "32004,32218", priority = 10 },
   { skill_index = 32006, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 18 19 ġ�� �޼� �޺�
   { skill_index = 32008, cooltime = 30000, rate = 75, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 15000, next_lua_skill_index = 19, globalcooltime = 2, notusedskill = "32004,32218", priority = 10 },
   { skill_index = 32005, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 20 ������
   { skill_index = 32009, cooltime = 25000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, td = "FR,FL", notusedskill = "32004,32218", priority = 10 },
-- 21 ��� ������
   { skill_index = 32005, cooltime = 7000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 1, notusedskill = "32004,32218", priority = 10 },
-- 22 ��� �޼�
   { skill_index = 32006, cooltime = 7000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 2, notusedskill = "32004,32218", priority = 10 },
-- 23 ġ�� ������
   { skill_index = 32007, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 1, notusedskill = "32004,32218", priority = 10 },
-- 24 ġ�� �޼�
   { skill_index = 32008, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 2, notusedskill = "32004,32218", priority = 10 },



-- 1�� ����ȭ
-- 25 ����
   { skill_index = 32030 , cooltime = 80000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,8,0,1", globalcooltime = 3, encountertime = 20000, notusedskill = "32218", usedskill = "32004", priority = 20 },
-- 26 ����
   { skill_index = 32036, cooltime = 40000, rate = 40, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", notusedskill = "32218", usedskill = "32004", priority = 10 },
-- 27 ������
   { skill_index = 32037, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, notusedskill = "32218", usedskill = "32004", priority = 10 },
-- 28 29 30 31 32 33 ��ġ
   { skill_index = 32039, cooltime = 60000, rate = 55, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, notusedskill = "32218", usedskill = "32004", priority = 10 },
   { skill_index = 32042, cooltime = 60000, rate = 56, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, notusedskill = "32218", usedskill = "32004", priority = 10 },
   { skill_index = 32044, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, notusedskill = "32218", usedskill = "32004", priority = 10 },
   { skill_index = 32040, cooltime = 60000, rate = 58, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, notusedskill = "32218", usedskill = "32004", priority = 10 },
   { skill_index = 32041, cooltime = 60000, rate = 59, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, notusedskill = "32218", usedskill = "32004", priority = 10 },
   { skill_index = 32043, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, notusedskill = "32218", usedskill = "32004", priority = 10 },
-- 34 35 �÷�ġ�� �޺�
   { skill_index = 32029, cooltime = 25000, rate = 50, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", next_lua_skill_index = 35, notusedskill = "32218", usedskill = "32004", priority = 10 },
   { skill_index = 32038, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 36 37 ġ�� ������ �޺�
   { skill_index = 32033, cooltime = 30000, rate = 75, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 15000, next_lua_skill_index = 37, globalcooltime = 1, notusedskill = "32218", usedskill = "32004", priority = 10 },
   { skill_index = 32032, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 38 39 ġ�� �޼� �޺�
   { skill_index = 32034, cooltime = 30000, rate = 75, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 15000, next_lua_skill_index = 39, globalcooltime = 2, notusedskill = "32218", usedskill = "32004", priority = 10 },
   { skill_index = 32031, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 40 ������
   { skill_index = 32035, cooltime = 25000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, td = "FR,FL", notusedskill = "32218", usedskill = "32004", priority = 10 },
-- 41 ��� ������
   { skill_index = 32031, cooltime = 7000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 1, notusedskill = "32218", usedskill = "32004", priority = 10 },
-- 42 ��� �޼�
   { skill_index = 32032, cooltime = 7000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 2, notusedskill = "32218", usedskill = "32004", priority = 10 },
-- 43 ġ�� ������
   { skill_index = 32033, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 1, notusedskill = "32218", usedskill = "32004", priority = 10 },
-- 44 ġ�� �޼�
   { skill_index = 32034, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 2, notusedskill = "32218", usedskill = "32004", priority = 10 },

-- 2�� ����ȭ
-- 45 ����
   { skill_index = 32202 , cooltime = 80000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,8,0,1", globalcooltime = 3, encountertime = 20000, usedskill = "32218", priority = 20 },
-- 46 ����
   { skill_index = 32208, cooltime = 40000, rate = 40, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", usedskill = "32218", priority = 10 },
-- 47 ������
   { skill_index = 32209, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, usedskill = "32218", priority = 10 },
-- 48 49 50 51 52 53 ��ġ
   { skill_index = 32211, cooltime = 60000, rate = 55, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, usedskill = "32218", priority = 10 },
   { skill_index = 32214, cooltime = 60000, rate = 56, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, usedskill = "32218", priority = 10 },
   { skill_index = 32216, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, usedskill = "32218", priority = 10 },
   { skill_index = 32212, cooltime = 60000, rate = 58, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, usedskill = "32218", priority = 10 },
   { skill_index = 32213, cooltime = 60000, rate = 59, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, usedskill = "32218", priority = 10 },
   { skill_index = 32215, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 4, usedskill = "32218", priority = 10 },
-- 54 55 �÷�ġ�� �޺�
   { skill_index = 32201, cooltime = 25000, rate = 50, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", next_lua_skill_index = 55, usedskill = "32218", priority = 10 },
   { skill_index = 32210, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 56 57 ġ�� ������ �޺�
   { skill_index = 32205, cooltime = 30000, rate = 75, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 15000, next_lua_skill_index = 57, globalcooltime = 1, usedskill = "32218", priority = 10 },
   { skill_index = 32204, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 58 59 ġ�� �޼� �޺�
   { skill_index = 32206, cooltime = 30000, rate = 75, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 15000, next_lua_skill_index = 59, globalcooltime = 2, usedskill = "32218", priority = 10 },
   { skill_index = 32203, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 60 ������
   { skill_index = 32207, cooltime = 25000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, td = "FR,FL", usedskill = "32218", priority = 10 },
-- 61 ��� ������
   { skill_index = 32203, cooltime = 7000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 1, usedskill = "32218", priority = 10 },
-- 62 ��� �޼�
   { skill_index = 32204, cooltime = 7000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 2, usedskill = "32218", priority = 10 },
-- 63 ġ�� ������
   { skill_index = 32205, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 1, usedskill = "32218", priority = 10 },
-- 64 ġ�� �޼�
   { skill_index = 32206, cooltime = 7000, rate = 40, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", encountertime = 5000, globalcooltime = 2, usedskill = "32218", priority = 10 },

-- ����
   { skill_index = 32046, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 1 },
}