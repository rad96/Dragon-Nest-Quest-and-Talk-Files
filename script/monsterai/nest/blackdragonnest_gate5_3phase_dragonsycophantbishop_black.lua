--DragonSycophantBishop_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1100;
g_Lua_NearValue4 = 4100;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1  },
   { action_name = "Stand_1", rate = 4, loop = 1, encountertime = 10000 },
   { action_name = "Walk_Left", rate = 4, loop = 1, encountertime = 10000  },
   { action_name = "Walk_Right", rate = 4, loop = 1, encountertime = 10000  },
   { action_name = "Attack2_Spell1_Laser_BlackDragon", rate = 6, loop = 1, encountertime = 10000 },
}
g_Lua_Near2 = { 
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Stand", rate = 8, loop = 1, encountertime = 10000  },
}
g_Lua_Near3 = { 
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Stand", rate = 15, loop = 1, encountertime = 10000  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 10, loop = 3 },
   { action_name = "Stand", rate = 15, loop = 1, encountertime = 10000  },
}

g_Lua_Skill = { 
     { skill_index = 34948, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 1 },
     { skill_index = 34941, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 9000, target = 3, prioriy = 100, limitcount = 1, encountertime = 10000, next_lua_skill_index = 0 },
     { skill_index = 34946, cooltime = 10000, rate = 70, rangemin = 0, rangemax = 700, target = 3, encountertime = 15000 },
     { skill_index = 34944, cooltime = 25000, rate = 50, rangemin = 0, rangemax = 700, target = 3, encountertime = 15000 },
     { skill_index = 34945, cooltime = 30000, rate = 60, rangemin = 0, rangemax = 500, target = 3, encountertime = 15000 },
     { skill_index = 34947, cooltime = 15000, rate = 100, rangemin = 500, rangemax = 1000, target = 3, encountertime = 15000 },
}
