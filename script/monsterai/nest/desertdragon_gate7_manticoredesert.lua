g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 3500;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 500;
g_Lua_AssualtTime = 3000;

g_Lua_GlobalCoolTime1 = 5000

g_Lua_Near1 = 
{ 
     { action_name = "Stand_1", rate = 5, loop = 1, td = "FL,RF" },
     { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,LB,BL" },
     { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
     { action_name = "Walk_Left", rate = 3, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right", rate = 3, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Attack07_LHook_EX", rate = 15, loop = 1, td = "FL,LF", globalcooltime = 1 },
     { action_name = "Attack08_RHook_EX", rate = 15, loop = 1, td = "FR,RF", globalcooltime = 1 },
}

g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1, td = "FL,RF" },
     { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,LB,BL" },
     { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
     { action_name = "Walk_Left", rate = 3, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right", rate = 3, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Walk_Front", rate = 5, loop = 2, td = "FL,FR,LF,RF" },
     { action_name = "Attack07_LHook_EX", rate = 10, loop = 1, td = "FL,LF", globalcooltime = 1 },
     { action_name = "Attack08_RHook_EX", rate = 10, loop = 1, td = "FR,RF", globalcooltime = 1 },
}

g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1, td = "FL,RF" },
     { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,LB,BL" },
     { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
     { action_name = "Walk_Left", rate = 5, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right", rate = 5, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Walk_Front", rate = 20, loop = 2, td = "FL,FR,LF,RF" },
}

g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 3, loop = 1, td = "FL,RF" },
     { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,LB,BL" },
     { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
     { action_name = "Walk_Left", rate = 3, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right", rate = 3, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Walk_Front", rate = 20, loop = 3, td = "FL,FR,LF,RF" },
}

g_Lua_Near5 = 
{ 
     { action_name = "Stand", rate = 2, loop = 1, td = "FL,RF" },
     { action_name = "Turn_Left", rate = 10, loop = -1, td = "LF,LB,BL" },
     { action_name = "Turn_Right", rate = 10, loop = -1, td = "RF,RB,BR" },
     { action_name = "Walk_Front", rate = 20, loop = 4, td = "FL,FR,LF,RF" },
}

g_Lua_Skill = { 
   { skill_index = 32185,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 1, limitcount = 1 },
}
