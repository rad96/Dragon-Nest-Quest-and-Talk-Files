--AiDragonSycophantMagician_Blue_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 4000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 34931, cooltime = 20000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, encountertime = 15000 },
   { skill_index = 34933, cooltime = 10000, rate = 40, rangemin = 400, rangemax = 1500, target = 3, encountertime = 2000 },
   { skill_index = 34932, cooltime = 10000, rate = 60, rangemin = 300, rangemax = 700, target = 3, encountertime = 2000 },
   { skill_index = 34935, cooltime = 10000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, encountertime = 2000 },
}
