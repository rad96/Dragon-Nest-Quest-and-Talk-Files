-- /genmon 51310

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 8000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 18, loop = 1 },
	{ action_name = "Walk_Left", rate = 13, loop = 1 },
	{ action_name = "Walk_Right", rate = 13, loop = 1 },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1 },	
	{ action_name = "Walk_Front", rate = 13, loop = 1 },
    { action_name = "Walk_Left", rate = 13, loop = 1 },
	{ action_name = "Walk_Right", rate = 13, loop = 1 },
}
g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1 },	
   	{ action_name = "Walk_Front", rate = 13, loop = 1 },
	{ action_name = "Move_Front", rate = 4, loop = 1 },
}
g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1 },	
	{ action_name = "Move_Front", rate = 15, loop = 1 },
}

g_Lua_GlobalCoolTime1 = 50000 
g_Lua_GlobalCoolTime2 = 40000
g_Lua_GlobalCoolTime3 = 19000
g_Lua_GlobalCoolTime4 = 20000  

g_Lua_Skill = {
    --// Next_Skill // --
    { skill_index = 36293, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, },--Attack13_WhirlAttack_VolNest (1,2,3,4)
    { skill_index = 36021, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, limitcount=1 },--Attack11_SummonGolem_VolNest75_Start--Summon(2)
    { skill_index = 36022, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, limitcount=1 },--Attack11_SummonGolem_VolNest50_Start--Summon(3)
    { skill_index = 36023, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, limitcount=1},--Attack11_SummonGolem_VolNest25_Start--Summon(4)
    { skill_index = 36024, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3,  },--Attack11_SummonGolem_VolNest24_Start--Summon(5)
    --// TriggerAction //--
    { skill_index = 36016, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, priority=300, next_lua_skill_index = 1, selfhppercent = 75, limitcount=1 },--Attack09_SummonGolem_VolNest_Ready--Move 2)
    { skill_index = 36016, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, priority=300, next_lua_skill_index = 2, selfhppercent = 50, limitcount=1 },--Attack09_SummonGolem_VolNest_Ready--Move 3)
    { skill_index = 36016, cooltime = 1000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 3000, target = 3, priority=300, next_lua_skill_index = 3, selfhppercent = 25, limitcount=1 },--Attack09_SummonGolem_VolNest_Ready--Move 4)
    { skill_index = 36016, cooltime = 10000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 3000, target = 3, priority=200, next_lua_skill_index = 4, selfhppercent = 25, },--Attack09_SummonGolem_VolNest_Ready--Move 5)
    --// CoolTimeSkill //--
    { skill_index = 36292, cooltime = 110000, globalcooltime = 4, rate = 45, rangemin = 0, rangemax = 800, target = 3, encountertime = 10000, randomtarget = "1.5,0,1" },--Attack06_DampseyRoll_VolNest_Start (1,2,3,4)
    { skill_index = 36019, cooltime = 70000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, encountertime = 5000 },--Attack14_JumpAttack_VolNest (1,2,3,4)
    { skill_index = 36020, cooltime = 90000, globalcooltime = 3, rate = 30, rangemin = 0, rangemax = 3000, target = 3, encountertime = 20000 },--Attack15_Howl_VolNest (1,2,3,4)
    { skill_index = 36015, cooltime = 100000, rate = 90, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,75"},--Attack07_HandStompMo_VolNest_Start  (2,3,4)
    { skill_index = 36026, cooltime = 80000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "51,100", },--Attack04_EarthQuake_VolNest (3)
    { skill_index = 36027, cooltime = 80000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,50", },--Attack04_EarthQuake_VolNest (4)
    { skill_index = 36398, cooltime = 80000, globalcooltime = 4, rate = 100, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 48,},
    --// BasicSkill //--
    { skill_index = 36295, cooltime = 25000, globalcooltime = 3, rate = 45, rangemin = 800, rangemax = 5000, target = 3, selfhppercent = 75, td = "FR,FL", }, --Attack17_CombAttack_VolNest_Start_01
    { skill_index = 36296, cooltime = 25000, globalcooltime = 3, rate = 45, rangemin = 1200, rangemax = 5000, target = 3, selfhppercent = 75, td = "FR,FL", }, --Attack17_CombAttack_VolNest_Start_02
    { skill_index = 36290, cooltime = 24000, rate = 60, rangemin = 0, rangemax = 500, target = 3, },--Attack01_RightHook_VolNest  (1,2,3,4)
    { skill_index = 36291, cooltime = 36000, rate = 55, rangemin = 0, rangemax = 800, target = 3,next_lua_skill_index = 0 },--Attack02_LeftUpper_VolNest  (1,2,3,4)
    { skill_index = 36017, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 500, target = 3, },--Attack11_SummonGolem_Fail
}


