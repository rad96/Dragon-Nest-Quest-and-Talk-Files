--AiFrogWarrior_Green_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 250
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 34167, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 1 },
}
