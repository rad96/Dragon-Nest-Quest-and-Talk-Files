--/genmon 503786
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 10000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_RandomSkill =
{{{
   { skill_index = 36226, rate = 25 },--Attack_VolNEST_5rd_RushDash12
   { skill_index = 36227, rate = 25 },--Attack_VolNEST_5rd_RushDash03
   { skill_index = 36228, rate = 25 },--Attack_VolNEST_5rd_RushDash06
   { skill_index = 36229, rate = 25 },--Attack_VolNEST_5rd_RushDash09
},}}

g_Lua_Skill = { 
   { skill_index = 36226,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, skill_random_index = 1},
}

