-- 가짜 개구리

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 6500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
}

g_Lua_Skill = { 
-- Attack06_YogaFake_Start
   { skill_index = 34010, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 3000, target = 1, encountertime = 3000, limitcount = 1 },
-- Attack06_YogaFake_Complete
   { skill_index = 34014, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 1 },
}
