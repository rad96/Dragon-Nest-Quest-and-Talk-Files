--리메이크 아크비숍 네스트 3관문 아크비숍
--RemakeArcBishopNest_Gate3_ArcBishop_Green_Boss.lua
--MONSTER ID : 505306


g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 6000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 2, loop = 1 },
	{ action_name = "Move_Left", rate = 13, loop = 1 },
	{ action_name = "Move_Right", rate = 13, loop = 1 },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 3, loop = 1 },	
    { action_name = "Move_Front", rate = 13, loop = 1 },
}
g_Lua_Near3 = 
{ 
    { action_name = "Move_Left", rate = 6, loop = 1 },
	{ action_name = "Move_Right", rate = 6, loop = 1 },
   	{ action_name = "Move_Front", rate = 15, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Move_Left", rate = 3, loop = 1 },
	{ action_name = "Move_Right", rate = 3, loop = 1 },
	{ action_name = "Move_Front", rate = 20, loop = 2 },
}



g_Lua_GlobalCoolTime1 = 5000 -- 3, 4, 8, 10 // 발사체 발사 후 그라비티로 못피하는 패턴이 발생, 몬스터가 투명해지면 발사체도 같이 투명해지는 현상 방지
g_Lua_GlobalCoolTime2 = 30000 -- 11, 12, 13 // 줄패턴 글로벌 쿨타임



g_Lua_Skill = { 
-- 데칼  Attack4_RollingDecal // 체력 50% 이하로 떨어지면 사용하는 9번스킬의 연계
	{ skill_index = 301154, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.6,0,1", next_lua_skill_index = 1 }, -- 0 -> 1
	
-- 스테이 Attack6_Stay_Start, Loop, End // 0번 스킬 사용 후 발사체 날아가는 시간동안 무적상태로 몬스터 대기, 공격 불가능
	{ skill_index = 301155, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 1
	
-- 넉백 공격 Attack5_KnockBackAttack // 데미지 없는 유저 넉백, 환기용 스킬
	{ skill_index = 301158,  cooltime = 25000, rate = 100, rangemin = 0, rangemax = 1000, target = 3 }, -- 2 
	
-- 포이즌 블로우 Attack1_PoisionBlow_Remake // 모든 유저 대상 바닥에 피해야 하는 장판
	{ skill_index = 301131,  cooltime = 20000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, globalcooltime = 1, multipletarget = "1,4,0,1" }, -- 3 
	
-- 호밍 미사일 Attack2_HomingMissile_Remake // 단일유저 대상 호밍 미사일
	{ skill_index = 301144,  cooltime = 35000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, globalcooltime = 1, randomtarget = "0.6,0,1", selfhppercentrange = "75, 100"  }, -- 4
	
-- 포이즌 레이저 Attack2_PoisionLaser // 장거리 직선 레이저, 탱커를 대상으로 사용함.
	{ skill_index = 301132,  cooltime = 15000, rate = 80, rangemin = 0, rangemax = 3000, target = 3 }, -- 5
	
-- 포이즌 웨이브 Attack3_PoisonWaveSmall_Remake // 일직선 발사체, 50%이하로 떨어지면 사용하지 않는다.
	{ skill_index = 301157,  cooltime = 20000, rate = 60, rangemin = 0, rangemax = 2000, target = 3,  selfhppercentrange = "50,100"  }, -- 6
	
-- 포이즌 웨이브 Attack3_PoisonWave_Remake // 사방향 발사체, 6번 스킬의 강화판임.
	{ skill_index = 301133,  cooltime = 10000, rate = 60, rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "0, 50"  },	 -- 7
	
-- 포이즌 익스플로전 Attack2_Event_Poision_Expolosion_Start // 유저 모이게 하는 그라비티 스킬, 50%까지는 연계가 없음 // 50% 이하에서는 유저 중앙으로 모이게 한 후, 데칼 스킬을 사용한다.
	{ skill_index = 301134,  cooltime = 60000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, globalcooltime = 1,  selfhppercentrange = "50,100"   }, -- 8
	{ skill_index = 301134,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, priority = 100 , next_lua_skill_index = 0,  selfhppercentrange = "0, 50" }, -- 9 -> 0

-- 텔레포트 Attack8_Teleport_Start // 중앙으로 이동 후 광역데미지, 그라비티, 환기용 스킬
	{ skill_index = 301148,  cooltime = 80000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 1,  selfhppercentrange = "0, 75"  }, -- 10

-- 나와라 분신 Attack7_SummonLich_Remake // 슬로우 오라를 사용하는 쫄 몬스터, 25%마다 사용하며 30초 쿨타임.
	{ skill_index = 301137, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 80, selfhppercent = 75, limitcount = 1, globalcooltime = 2 }, -- 11
	{ skill_index = 301137, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 80, selfhppercent = 50, limitcount = 1, globalcooltime = 2 }, -- 12
	{ skill_index = 301137, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 80, selfhppercent = 25, limitcount = 1, globalcooltime = 2 }, -- 13
}

