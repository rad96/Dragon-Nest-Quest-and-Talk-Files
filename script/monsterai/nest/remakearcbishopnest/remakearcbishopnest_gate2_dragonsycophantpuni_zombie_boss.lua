
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 4000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 30000
g_Lua_GlobalCoolTime2 = 10000
g_Lua_GlobalCoolTime3 = 50000
g_Lua_GlobalCoolTime4 = 90000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Attack01_Chopping", rate = 4, cooltime = 8000, loop = 1  },
   { action_name = "Attack02_Slash", rate = 4, loop = 1  },
   { action_name = "Attack03_JumpChopping", rate = 2, cooltime = 12000, loop = 1, randomtarget = 1.1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 4, loop = 2  },
   { action_name = "Attack01_Chopping", rate = 6, cooltime = 8000, loop = 1  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 6, loop = 1  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 8, loop = 3  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack01_Chopping", rate = 5, loop = 1, cooltime = 8000, approach = 600.0 },
   { action_name = "Move_Left", rate = 6, loop = 3  },
   { action_name = "Move_Right", rate = 6, loop = 3  },
}

g_Lua_GlobalCoolTime1 = 50000

g_Lua_Skill = { 
--����ȭ, 2�ܰ�--
   -- { skill_index = 301169, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 8000, target = 1, next_lua_skill_index = 1, globalcooltime = 1 },
   --{ skill_index = 301161, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 8000, target = 1,usedskill = "301169", notusedskill = "301161", globalcooltime = 1 },
   --{ skill_index = 301162, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 8000, target = 1, usedskill = "301161", notusedskill = "301162", globalcooltime = 1 },
   --{ skill_index = 301163, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 8000, target = 1, usedskill = "301162", notusedskill = "301163", globalcooltime = 1 },
--����ȭ, 1�ܰ� �߻�--
   --{ skill_index = 301172, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 8000, target = 1, encountertime = 30000, globalcooltime = 1, limitcount = 1 },
  --{ skill_index = 301162, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 8000, target = 1, usedskill = "301172", notusedskill = "301162", globalcooltime = 1 },
--�罽���� ���� ���� ���� ���
   { skill_index = 301165, cooltime = 80000, rate = 50, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 75, multipletarget = 1, combo1 = "14,100,0" }, -- 0
   { skill_index = 301166, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 1
--������� (1������~)
   { skill_index = 301164, cooltime = 45000, rate = 60, rangemin = 300, rangemax = 1200, target = 3, next_lua_skill_index = 3, randomtarget = 1.1 }, -- 2
   { skill_index = 301171, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3 }, -- 3
--���� ����(�ٴ� �����)
   { skill_index = 301168, cooltime = 35000, rate = 60, rangemin = 500, rangemax = 1000, target = 3, multipletarget = "1,4" }, -- 4
--����� ��ǳ(���� ��ų)
   { skill_index = 301170, cooltime = 60000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 75, limitcount = 1, combo1 = "9,100,0" }, -- 5
   { skill_index = 301170, cooltime = 60000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 50, limitcount = 1, combo1 = "9,100,0" }, -- 6
   { skill_index = 301170, cooltime = 60000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 25, limitcount = 1, combo1 = "9,100,0" }, -- 7
   { skill_index = 301170, cooltime = 60000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 12, limitcount = 1, combo1 = "9,100,0" }, -- 8
   { skill_index = 301167, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 9
--������
   { skill_index = 301182, cooltime = 95000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, randomtarget = 1.0, selfhppercent = 50, globalcooltime = 3, combo1 = "11,100,0" }, -- 10
   { skill_index = 301174, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 11
--�������_����
   { skill_index = 301175, cooltime = 90000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80, globalcooltime = 4,  randomtarget = 1.1 }, -- 12
   --{ skill_index = 301178, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "15,100,0",  randomtarget = 1.1 }, -- 14
   --{ skill_index = 301179, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,  randomtarget = 1.1 }, -- 15
--����_����
   { skill_index = 301176, cooltime = 150000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 60, globalcooltime = 4 }, -- 13
--�޺�
   --{ skill_index = 301180, cooltime = 90000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90, globalcooltime = 3 }, -- 15
--���
   { skill_index = 301183, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "1,100,0" }, -- 14
}