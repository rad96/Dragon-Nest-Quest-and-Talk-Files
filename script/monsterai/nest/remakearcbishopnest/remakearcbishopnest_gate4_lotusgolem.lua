--리메이크 아크비숍 네스트 3관문 아크비숍
--리메이크 아크비숍 네스트 4관문 로터스골렘
--RemakeArcBishopNest_Gate4_LotusGolem.lua
--몬스터 ID : 505310

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_OnlyPartsDamage = 2 -- 파츠만 데미지를 입는 시스템. 본체는 암만 건드려도 소용 없다.

g_Lua_PartsProcessor = { -- MonsterPartsTable 에 설정된 211,212 번 파트에 관한 내용임. 211번은 복부, 212번은 머리통의 아크비숍
   { hp="80,100", ignore="211,212" }, -- 80 ~ 100%에서는 211, 212 번 파트가 데미지를 받지 않음. 복부에 공격시 데미지는 들어간다. ( 이건 OnlyPartsDamage 로 관리되는 부분 )
   { hp="0,80", ignore="211,212" }, -- 80% 미만일 때에는 212 파트는 데미지를 받지 않는다.
   { hp="0,80", checkblow ="121", ignore="211", nodamage="211", active="212" }, -- 121번 상태이상효과(STAND_CHANGE, Groggy 액션중)가 있을 때에는 211번 파트가 데미지를 받지 않으며, 212번 파트가 활성화된다.
   { hp="0,80", checkblow ="100", ignore="212", nodamage="212", active="211" }, -- 121번 상태이상효과(STAND_CHANGE, Groggy 액션중)가 있을 때에는 211번 파트가 데미지를 받지 않으며, 212번 파트가 활성화된다.
}

-- 기존 아크비숍에서 변경된 그로기 시간 : 30초 -> 15초

g_Lua_GlobalCoolTime1 = 30000 -- PropAttack_Punch
g_Lua_GlobalCoolTime2 = 20000 -- Attack07_SummonRock_Remake
g_Lua_GlobalCoolTime3 = 20000 -- Attack03_StompRight, Attack04_StompLeft 공용 쿨타임
g_Lua_GlobalCoolTime4 = 5000 -- Attack02_BashLeft
g_Lua_GlobalCoolTime5 = 5000 -- Attack02_BashRight
g_Lua_GlobalCoolTime6 = 15000 -- Attack08_Shout_Remake, Attack06_PoisonBreath_Remake 겹치지 않도록 조정
g_Lua_GlobalCoolTime7 = 90000 -- 줄패턴용 글로벌쿨타임 타이머


g_Lua_Near1 = { 
	{ action_name = "Stand_1", rate = 1, loop = 1, notusedskill = 301196 },
	{ action_name = "Attack05_PunchBreak_Remake", rate = 80, loop = 1, cooltime = 10000, td = "FR,FL", notusedskill = 301196  },
	{ action_name = "Attack07_SummonRock_Remake", rate = 80, loop = 1, globalcooltime = 2, td = "FR,RF,RB,BR", notusedskill = 301196 },
	{ action_name = "Attack02_BashLeft", rate = 100, loop = 1, globalcooltime = 4, td = "FL,LF,LB", notusedskill = 301196, priority = 100 },
	{ action_name = "Attack01_BashRight", rate = 100, loop = 1, globalcooltime = 5, td = "FR,RF,RB", notusedskill = 301196, priority = 100 },
	{ action_name = "Attack03_StompRight", rate = 60, loop = 1, globalcooltime = 3, td = "FR,RF,RB,BR", notusedskill = 301196, selfhppercentrange = "60,100" },
	{ action_name = "Attack04_StompLeft", rate = 60, loop = 1, globalcooltime = 3, td = "FL,LF,LB,BL", notusedskill = 301196, selfhppercentrange = "60,100" },
}

g_Lua_Near2 = { 
	{ action_name = "Stand_1", rate = 1, loop = 1, notusedskill = 301196 },
	{ action_name = "Attack07_SummonRock_Remake", rate = 100, loop = 1, globalcooltime = 2, td = "FR,RF,RB,BR", notusedskill = 301196 },
	{ action_name = "Attack03_StompRight", rate = 60, loop = 1, globalcooltime = 3, td = "FR,RF,RB,BR", notusedskill = 301196, selfhppercentrange = "60,100" },
	{ action_name = "Attack04_StompLeft", rate = 60, loop = 1, globalcooltime = 3, td = "FL,LF,LB,BL", notusedskill = 301196, selfhppercentrange = "60,100" },
	{ action_name = "Attack02_BashLeft", rate = 100, loop = 1, globalcooltime = 4, td = "FL,LF,LB", notusedskill = 301196, priority = 100  },
	{ action_name = "Attack01_BashRight", rate = 100, loop = 1, globalcooltime = 5, td = "FR,RF,RB", notusedskill = 301196, priority = 100  },
}

g_Lua_Near3 = { 
	{ action_name = "Stand_1", rate = 1, loop = 1, notusedskill = 301196 },
	{ action_name = "Attack07_SummonRock_Remake", rate = 100, loop = 1, globalcooltime = 2, td = "FR,RF,RB,BR", notusedskill = 301196 },
}

g_Lua_RandomSkill =
{
	{
		{
		   { skill_index = 301051, rate = 20, next_lua_skill_index = 0 }, --2~6시 프랍펀치
		   { skill_index = 301052, rate = 20, next_lua_skill_index = 0 }, 
		   { skill_index = 301053, rate = 20, next_lua_skill_index = 0 }, 
		   { skill_index = 301054, rate = 20, next_lua_skill_index = 0 }, 
		   { skill_index = 301055, rate = 20, next_lua_skill_index = 0 } 
		},
		{
			{ skill_index = 301056, rate = 20 }, --7~11시 프랍펀치
			{ skill_index = 301057, rate = 20 }, 
			{ skill_index = 301058, rate = 20 }, 
			{ skill_index = 301059, rate = 20 }, 
			{ skill_index = 301060, rate = 20 } 
		},
		{
			{ skill_index = 301066, rate = 25, next_lua_skill_index = 3 }, -- 줄패턴 랜덤 트리거
			{ skill_index = 301067, rate = 25, next_lua_skill_index = 3 }, 
			{ skill_index = 301068, rate = 25, next_lua_skill_index = 3 }, 
			{ skill_index = 301069, rate = 25, next_lua_skill_index = 3 }, 
		},
		{
			{ skill_index = 301070, rate = 25, next_lua_skill_index = 4 }, -- 줄패턴 랜덤 트리거
			{ skill_index = 301071, rate = 25, next_lua_skill_index = 4 }, 
			{ skill_index = 301072, rate = 25, next_lua_skill_index = 4 }, 
			{ skill_index = 301073, rate = 25, next_lua_skill_index = 4 }, 
		},
		{
			{ skill_index = 301066, rate = 25, next_lua_skill_index = 5 }, -- 줄패턴 랜덤 트리거
			{ skill_index = 301067, rate = 25, next_lua_skill_index = 5 }, 
			{ skill_index = 301068, rate = 25, next_lua_skill_index = 5 }, 
			{ skill_index = 301069, rate = 25, next_lua_skill_index = 5 }, 
		},
	}
}

g_Lua_Skill = { 
-- 그로기 						** ( MonsterPartsTable 에서 관리된다.)
--   { skill_index = 301196,  cooltime = 30000, rate = 100,rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 100 },
-- 프랍펀치
	{ skill_index = 301056,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3 , notusedskill = 301196, skill_random_index = 2 }, -- Attack10_EyeLaser_Front_Remake
-- Blow용 스탠드
	{ skill_index = 301061,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3 }, -- Stand
-- 줄패턴 첫번째 ~ 다섯번째
	{ skill_index = 301066,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3, skill_random_index = 3 }, -- Stand	
	{ skill_index = 301070,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3, skill_random_index = 4 }, -- Stand	
	{ skill_index = 301066,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3, skill_random_index = 5 }, -- Stand	
-- 줄패턴 끝
	{ skill_index = 301065,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3 }, -- Stand
-- 줄패턴 끝
	{ skill_index = 301065,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3 }, -- Stand
-- 어스퀘이크
	{ skill_index = 301198,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3, notusedskill = 301196 }, -- Attack11_Earthquake_Remake
-- 어스퀘이크 준비
	{ skill_index = 301080,  cooltime = 40000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, notusedskill = 301196, selfhppercentrange = "0, 60", combo1 = "7, 100, 0"  }, -- Attack11_Earthquake_Remake
-- 프랍펀치
	{ skill_index = 301051,  cooltime = 40000, rate = 100, rangemin = 0, rangemax = 4000, target = 3 , notusedskill = 301196, selfhppercent = 60,  next_lua_skill_index = 0, skill_random_index = 1 }, -- Attack10_EyeLaser_Front_Remake
-- 아이레이저
	{ skill_index = 301197,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 4000, target = 3 , notusedskill = 301196 }, -- Attack10_EyeLaser_Front_Remake
-- 샤우트
	{ skill_index = 301186,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, notusedskill = 301196, globalcooltime = 6 ,  multipletarget = "1,4,0,1", encountertime = 20000  }, -- Attack08_Shout_Remake
-- 포이즌 브레스
	{ skill_index = 301188,  cooltime = 60000, rate = 100, rangemin = 500, rangemax = 4000, target = 3, notusedskill = 301196,  globalcooltime = 6, encountertime = 20000  }, -- Attack06_PoisonBreath_Remake
-- 광폭화 
	{ skill_index = 301187,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 4000, target = 3 , notusedskill = 301196, selfhppercentrange = "0, 60" }, -- Attack13_Berserk_Remake
-- 볼케이노
	{ skill_index = 301081,  cooltime = 35000, rate = 100, rangemin = 0, rangemax = 1000, target = 3 , notusedskill = 301196 }, -- Attack09_SummonVolcano
-- 센터레이저
	{ skill_index = 301199,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, notusedskill = 301196, next_lua_skill_index = 1, selfhppercentrange = "0, 80"  }, -- Attack12_CenterLazer_Remake
-- 마미손
	{ skill_index = 301064,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 4000, target = 3 , limitcount = 1, selfhppercent = 60, priority = 100, notusedskill = 301196 , next_lua_skill_index = 2, globalcooltime = 7 }, 
	{ skill_index = 301064,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 4000, target = 3 , limitcount = 1, selfhppercent = 40, priority = 100, notusedskill = 301196 , next_lua_skill_index = 2, globalcooltime = 7 }, 
	{ skill_index = 301064,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 4000, target = 3 , limitcount = 1, selfhppercent = 20, priority = 100, notusedskill = 301196 , next_lua_skill_index = 2, globalcooltime = 7 }, 
}
