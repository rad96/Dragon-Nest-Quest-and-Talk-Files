--RemakeArcBishopNest_Gate3_ArcBishop_Green_Small.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 7000;

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 25, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
}

g_Lua_GlobalCoolTime1 = 5000

g_Lua_Skill = { 
	{ skill_index = 301153, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1 },
	{ skill_index = 301152,  rate = 100, rangemin = 300, rangemax = 4000, target = 3 , cooltime = 15000, randomtarget = "0.6,0,1" }, -- Attack8_BlinkNStomp_Start
}