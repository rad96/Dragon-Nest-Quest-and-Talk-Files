
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 4000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 50000
g_Lua_GlobalCoolTime2 = 10000
g_Lua_GlobalCoolTime3 = 15000
g_Lua_GlobalCoolTime4 = 90000
g_Lua_GlobalCoolTime5 = 8000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Attack01_Chopping", rate = 4, globalcooltime = 5, loop = 1  },
   { action_name = "Attack02_Slash", rate = 4, loop = 1  },
   { action_name = "Attack03_JumpChopping_Hell", rate = 10, cooltime = 12000, loop = 1, randomtarget = 1.1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 4, loop = 2  },
   { action_name = "Attack01_Chopping", rate = 6, globalcooltime = 5, loop = 1  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 6, loop = 1  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 8, loop = 3  },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack01_Chopping", rate = 5, loop = 1, approach = 600.0 },
   { action_name = "Move_Left", rate = 6, loop = 3  },
   { action_name = "Move_Right", rate = 6, loop = 3  },
}

g_Lua_Skill = { 
-- 줄패턴 실제 Attack09_FireStorm_Start // 줄패턴, 특정 구역으로 이동해서 피해야 함.
	{ skill_index = 301167, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 0
--사슬포박 대기, 이후 찍기 콤보 Stand_3, Attack11_JumpAttack // 사슬 포박 이후에 점프 회피 패턴
	{ skill_index = 301183, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "2,100,0" }, -- 안씀
	{ skill_index = 301814, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 2
--끌어당긴 후 슬래쉬 Attack02_Slash // 사슬로 끌어당긴 이후에 슬래쉬
	{ skill_index = 301811, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3 }, -- 3
--휠윈드 실제 Attack05_Whirlwind_Start // 발사체 + 유저 쫓아오며 주변 데미지
	{ skill_index = 301174, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, -- 4

--------##실제 사용 스킬##--------

-- 줄패턴 어둠의 폭풍 (선행) Attack03_EventSignal4 // 줄패턴 선행 한번 찍기 모션, 이후 중앙으로 순간이동
	{ skill_index = 301815, cooltime = 1000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 75, limitcount = 1, combo1 = "0,100,0" }, -- 5 -> 0
	{ skill_index = 301815, cooltime = 1000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 50, limitcount = 1, combo1 = "0,100,0" }, -- 6 -> 0
	{ skill_index = 301815, cooltime = 1000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 25, limitcount = 1, combo1 = "0,100,0" }, -- 7 -> 0
--사슬포박 Attack10_ChainCriminal // 사슬 던지기, 데미지는 약하며 AD패턴임
	{ skill_index = 301165, cooltime = 80000, rate = 50, rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 75, multipletarget = 1, combo1 = "2,100,0" }, -- 8 -> 2
--끌어당기기 Attack12_ChainAttack // 사슬 던져서 당겨오기, 데미지는 없음
	{ skill_index = 301164, cooltime = 45000, rate = 60, rangemin = 300, rangemax = 1200, target = 3, randomtarget = 1.1, next_lua_skill_index = 3,  }, -- 9 -> 3
--휠윈드 선행 Attack13_Combo_Ready // 휠윈드 선행, 번쩍번쩍 이펙트를 보여줌
	{ skill_index = 301182, cooltime = 95000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, randomtarget = 1.0, selfhppercent = 50, globalcooltime = 3, combo1 = "4,100,0" }, -- 10 -> 4
--썩어가는 대지(바닥 만들기) Attack08_Rotting_ArcBishopHell // 바닥에 불장판을 깔아놓음.
	{ skill_index = 301168, cooltime = 35000, rate = 60, rangemin = 500, rangemax = 1000, target = 3, multipletarget = "1,4" }, -- 11
--점프찍기_장판 Attack06_JumpFire_Start // 제자리에서 뛰다가 장판 만들기
	{ skill_index = 301175, cooltime = 90000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, randomtarget = 1.1 }, -- 12
--돌진_슈아 Attack04_Assault_Start // 일격필살, 20초간 공격하다가 슈아 못풀면 폭발데미지
	{ skill_index = 301813, cooltime = 150000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 50 }, -- 13
--콤보 Attack13_Combo_Hell // 그라비티로 당겨온 후 180도 후려패기
   { skill_index = 301812, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 5000, target = 3,  globalcooltime = 3, selfhppercent = 50 }, -- 15
}