--ArcBishopNest_Gate2_HalfGolem_Green.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 40000

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
}

g_Lua_RandomSkill ={
{
{    { skill_index = 301125, rate = 50 },   { skill_index = 301126, rate = 50 },   },
{    { skill_index = 301114, rate = 50 },   { skill_index = 301116, rate = 50 },   },
}
}

g_Lua_GlobalCoolTime1 = 50000

g_Lua_Skill = { 
--버섯 날리기
   { skill_index = 301111, cooltime = 65000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, multipletarget = "2,4,0,1" },
--찍기(원거리 견제용)
   { skill_index = 301115, cooltime = 76000, rate = 60, rangemin = 0, rangemax = 3000, target = 3 },
--모기향
   { skill_index = 301112, cooltime = 100000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, encountertime = 30000, globalcooltime = 1 },
--돌진 (skill_random_index = 2)
   { skill_index = 301114, skill_random_index = 2, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 80, randomtarget = 1.1  },
   --화산폭발 (skill_random_index = 2)
   --{ skill_index = 301116, cooltime = 15000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, globalcooltime = 4 },
   --점프찍기 (skill_random_index = 2)
   --{ skill_index = 301128, cooltime = 35000, rate = 80, rangemin = 500, rangemax = 1000, target = 3, globalcooltime = 4 },
--작은 버섯 소환
   --{ skill_index = 301113, cooltime = 75000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "8,50", globalcooltime = 1 },
--땅 뒤집기
   { skill_index = 301123, cooltime = 55000, rate = 80, rangemin = 0, rangemax = 5000, target = 3 },
--추격_준비
   { skill_index = 301124, cooltime = 60000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 75, limitcount = 1, combo1 = "9,100,0",  randomtarget = "1.1,0,1"  }, --추격 준비 동작
   { skill_index = 301124, cooltime = 60000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 50, limitcount = 1, combo1 = "9,100,0",  randomtarget = "1.1,0,1"  },
   { skill_index = 301124, cooltime = 60000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 25, limitcount = 1, combo1 = "9,100,0",  randomtarget = "1.1,0,1"  },
   { skill_index = 301124, cooltime = 60000, globalcooltime = 1, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 95, selfhppercent = 12, limitcount = 1, combo1 = "9,100,0",  randomtarget = "1.1,0,1"  },
   { skill_index = 301117, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, --추격
--휘두르기 (skill_random_index = 1)
   { skill_index = 301125, skill_random_index = 1, cooltime = 15000, rate = 80, rangemin = 0, rangemax = 300, target = 3 },
--머리박치기 (skill_random_index = 1)
   --{ skill_index = 301126, cooltime = 15000, rate = 80, rangemin = 0, rangemax = 300, target = 3 },
--고함
   -- { skill_index = 301127, cooltime = 65000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80 },
}