g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000


g_Lua_Near1 =
{
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Stand", rate = 1, loop = 1  },
}
g_Lua_Near2 =
{
   { action_name = "Stand", rate = 1, loop = 1  },
}
g_Lua_Near3 =
{
   { action_name = "Stand", rate = 1, loop = 1  },
}
g_Lua_Near4 =
{
   { action_name = "Stand", rate = 1, loop = 1  },
}

g_Lua_Skill=
{
   { skill_index = 301120, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, encountertime = 16000 },
   { skill_index = 301121, cooltime = 10000, rate = 80, rangemin = 0, rangemax = 200, target = 3 },
}