
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 1200;
g_Lua_NearValue2 = 4000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 6000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Attack06_Laser_Front", cooltime = 6000, globalcooltime = 1, rate = 16, loop = 1, td = "FL,FR", selfhppercent = 99 },
   { action_name = "Attack08_Laser_Right", cooltime = 6000, globalcooltime = 1, rate = 16, loop = 1, td = "LF", selfhppercent = 99 },
   { action_name = "Attack07_Laser_Left", cooltime = 6000, globalcooltime = 1, rate = 16, loop = 1, td = "RF", selfhppercent = 99 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 3, loop = 2  },
}

g_Lua_Skill = { 
   { skill_index = 32184, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5200, target = 1, limitcount = 1, priority = 50 },
   { skill_index = 32165, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5200, target = 3, selfhppercentrange = "99,100" },
   { skill_index = 32167, cooltime = 25000, rate = 100, rangemin = 0, rangemax = 1200, target = 3, multipletarget = "1,1,0,1", randomtarget = 0.6, selfhppercent = 99 },
   { skill_index = 32164, cooltime = 50000, rate = 90, rangemin = 0, rangemax = 1500, target = 3, multipletarget = 1, selfhppercent = 99 },
}
