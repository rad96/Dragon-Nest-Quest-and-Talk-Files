--AiPterosaur_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 2400;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 450
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {
      { "!Stay", "!Move", "!Stiff", "Attack", "!Down" }
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Stand_1", rate = 2, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Assault = { 
   { action_name = "Stand", rate = 10, loop = 1, approach = 400 },
}

g_Lua_GlobalCoolTime1 = 300000

g_Lua_Skill = { 
    -- Attack01_Splash
   { skill_index = 300261,  cooltime = 5000, rate = 70, rangemin = 0, rangemax = 400, target = 3 }, -- 0
    -- Attack05_15Lunge
   { skill_index = 300265,  cooltime = 20000, rate = 70, rangemin = 0, rangemax = 400, target = 3 }, -- 4
    -- Attack15_Combo
   { skill_index = 300275,  cooltime = 25000, rate = 70, rangemin = 0, rangemax = 700, target = 3, td = "FR,FL" }, -- 16
    -- Attack01_Splash_PowerFull
   { skill_index = 300276,  cooltime = 45000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, td = "FR,FL" }, -- 17
}
