--/genmon 505053
--RedDragonNest_Gate2_Hidemonster_ICE.lua
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 5000;


g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}

g_Lua_Skill = {
   { skill_index = 300133,  cooltime = 6000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,},--Attack_RD_Ice_Target03_Start
   { skill_index = 300134,  cooltime = 6000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,},--Attack_RD_Ice_Target09_Start
}