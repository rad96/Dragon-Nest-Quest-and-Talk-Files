--/genmon 505093
--R:\Gameres\Resource\Char\Monster\ElectricWallBomb\ElectricWallBomb.act
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 5000;


g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}

g_Lua_Skill = {
   { skill_index = 300256,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3 },--Attack00_Boom_Start
   { skill_index = 300257,  cooltime = 3000, rate = 100, rangemin = 0, rangemax = 8000, target = 3, encountertime= 300000 },--Attack01_SelfDestroyedAttack00_Boom_Start
   { skill_index = 300258,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3 },--Attack00_Boom2_Start
}