--AiPterosaur_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 8000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Stand3", rate = 2, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Assault = { 
   { action_name = "Stand", rate = 10, loop = 1, approach = 100 },
}
g_Lua_GlobalCoolTime1 = 30000
g_Lua_GlobalCoolTime2 = 50000
g_Lua_GlobalCoolTime3 = 120000
g_Lua_GlobalCoolTime4 = 40000
g_Lua_GlobalCoolTime5 = 40000
g_Lua_GlobalCoolTime6 = 60000
g_Lua_GlobalCoolTime7 = 70000
g_Lua_GlobalCoolTime8 = 30000
g_Lua_GlobalCoolTime9 = 90000

g_Lua_Skill = { 
   -- $$$$$$$$$$$$ 기본 공격 (노말) $$$$$$$$$$$$$$
   { skill_index = 300261,  cooltime = 5000, rate = 70, rangemin = 0, rangemax = 400, target = 3 }, -- 0 0-- Attack01_Splash
   { skill_index = 300275,  cooltime = 45000, globalcooltime = 8, rate = 70, rangemin = 0, rangemax = 700, target = 3, td = "FR,FL", combo1 = "29,100,0", encountertime=40000, notusedskill  = "300282" }, -- 1-- Attack15_Combo
   -- $$$$$$$$$$$$ 기본 공격 (원거리 견제) $$$$$$$$$$$$$$
   -- + [Group 1]
   { skill_index = 300262,  cooltime = 35000, globalcooltime = 1,  rate = 70, rangemin = 0, rangemax = 1000, target = 3, randomtarget = "0.6,0,1", encountertime=30000, combo1 = "29,100,0" }, -- 2-- Attack02_JumpAttack
   { skill_index = 300305,  cooltime = 35000, globalcooltime = 1,  rate = 70, rangemin = 0, rangemax = 2000, target = 3,  td = "BR,BL", randomtarget = "0.6,0,1", encountertime=30000, combo1 = "29,100,0" }, -- 3-- Attack02_JumpAttack_Back
   { skill_index = 300276,  cooltime = 35000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 1000, target = 3, td = "FR,FL",  combo1 = "29,100,0", randomtarget = "0.6,0,1", encountertime=30000 }, -- 4-- Attack01_Splash_PowerFull   
   -- + [Group 2]   
   { skill_index = 300266,  cooltime = 70000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 2000, target = 3, multipletarget = "2,4,0,1", encountertime=50000, combo1 = "29,100,0", notusedskill  = "300282" }, -- 5-- Attack06_UpAttack_Combo
   { skill_index = 300267,  cooltime = 70000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 8000, target = 3, encountertime=70000, combo1 = "29,100,0", notusedskill  = "300282" }, -- 6-- Attack07_SummonFire
   { skill_index = 300269,  cooltime = 70000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 8000, target = 3, randomtarget = "0.6,0,1", encountertime=40000, combo1 = "29,100,0", notusedskill  = "300282" }, -- 7-- Attack09_MagicBreath
   -- + [Group 3]
   { skill_index = 300283,  cooltime = 130000, rate = 70, rangemin = 0, rangemax = 8000, target = 3, encountertime=30000, notusedskill  = "300282", combo1 = "29,100,0"  }, -- 8-- Attack02_JumpAttack_Teleport 
   { skill_index = 300304,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 8000, target = 3, encountertime=40000 }, -- 9-- Attack14_Howling_S_Start
   -- $$$$$$$$$$$$ 기본 공격 (돌진형) $$$$$$$$$$$$$$
   -- + [Group 4]     
   { skill_index = 300263,  cooltime = 45000, globalcooltime = 4, rate = 70, rangemin = 0, rangemax = 1200, target = 3, combo1 = "29,100,0", randomtarget = "0.6,0,1" , encountertime=30000 }, -- 10-- Attack03_LineDrive_Fast
   { skill_index = 300277,  cooltime = 100000, globalcooltime = 4, rate = 50, rangemin = 0, rangemax = 1000, target = 3, combo1 = "29,100,0", randomtarget = "0.6,0,1", encountertime=30000, notusedskill  = "300282" }, -- 11-- Attack03_LineDrive
   { skill_index = 300264,  cooltime = 100000, globalcooltime = 4, rate = 50, rangemin = 0, rangemax = 1200, target = 3, combo1 = "14,100,0", randomtarget = "0.6,0,1", encountertime=30000, notusedskill  = "300282" }, -- 12-- Attack04_JumpAttack
   { skill_index = 300310,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 1200, target = 3, combo1 = "31,100,0" }, -- 13-- Attack04_JumpAttack_Combo2
   -- + [Group 5]   
   { skill_index = 300265,  cooltime = 45000, globalcooltime = 5, rate = 70, rangemin = 0, rangemax = 700, target = 3, randomtarget = "0.6,0,1", combo1 = "29,100,0" }, -- 14-- Attack05_15Lunge
   { skill_index = 300273,  cooltime = 45000, globalcooltime = 5, rate = 70, rangemin = 0, rangemax = 1000, target = 3, combo1 = "29,100,0" }, -- 15-- Attack13_BackTumbling
    -- $$$$$$$$$$$$  특수 공격 $$$$$$$$$$$$$$
   -- + [Group 6]
   { skill_index = 300301,  cooltime = 200000, globalcooltime = 6, rate = -1, rangemin = 0, rangemax = 100, target = 3, selfhppercent= 1,  combo1 = "29,100,0" }, -- 16-- Attack08_DeathFirePrison
   { skill_index = 300270,  cooltime = 300000, globalcooltime = 6, rate = 70, rangemin = 0, rangemax = 8000, target = 3, selfhppercent=75 }, -- 17-- Attack10_BerserkerMode
   { skill_index = 300282,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3 }, -- 18-- BerserkerMode_Success
  -- + [Group 7]
   { skill_index = 300284,  cooltime = 90000, globalcooltime = 7, rate = 70, rangemin = 0, rangemax = 8000, target = 3, encountertime=30000, notusedskill  = "300282", combo1 = "29,100,0"  }, -- 19-- Attack02_JumpAttack_Teleport_Loop
   { skill_index = 300306,  cooltime = 90000, globalcooltime = 7, rate = 70, rangemin = 0, rangemax = 8000, target = 3, combo1 = "7,100,0", notusedskill  = "300282" }, -- 20-- Attack07_Gravity
    -- $$$$$$$$$$$$  특수 공격 - 줄패턴 $$$$$$$$$$$$$$   
    
   { skill_index = 300271,  cooltime = 5000, globalcooltime = 6, globalcooltime2 = 9, rate = 100, rangemin = 0, rangemax = 8000, target = 3, priority = 95, selfhppercent = 75, limitcount = 1 }, -- 21-- Attack11_Maguma
   { skill_index = 300271,  cooltime = 5000, globalcooltime = 6, globalcooltime2 = 9, rate = 100, rangemin = 0, rangemax = 8000, target = 3, priority = 94, selfhppercent = 50, limitcount = 1 }, -- 22-- Attack11_Maguma
   { skill_index = 300271,  cooltime = 5000, globalcooltime = 6, globalcooltime2 = 9, rate = 100, rangemin = 0, rangemax = 8000, target = 3, priority = 93, selfhppercent = 25, limitcount = 1 }, -- 23   -- Attack11_Maguma
   { skill_index = 300272,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 3,  combo1 = "28,100,0" }, -- 24
   { skill_index = 300274,  cooltime = 3000, globalcooltime = 9, rate = 70, rangemin = 0, rangemax = 8000, target = 3, priority = 92, selfhppercent = 15, limitcount = 1 }, -- 25-- Attack14_Howling
    -- $$$$$$$$$$$$  광폭화 $$$$$$$$$$$$$$      
   -- + [Group 8]
   { skill_index = 300308,  cooltime = 55000, globalcooltime = 7, rate = 100, rangemin = 0, rangemax = 3000, target = 3, usedskill  = "300282", combo1 = "29,100,0" }, -- 26-- Attack07_GravityBerserker
   { skill_index = 300309,  cooltime = 45000, globalcooltime = 8, rate = 70, rangemin = 0, rangemax = 700, target = 3, td = "FR,FL", encountertime=40000, usedskill  = "300282", combo1 = "29,100,0" }, -- 27-- Attack15_Combo_EX
    -- $$$$$$$$$$$$  대기 $$$$$$$$$$$$$$    
   { skill_index = 300279,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3 }, -- 28-- Stand_2
   { skill_index = 300280,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3 }, -- 29-- Stand_3
   { skill_index = 300281,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3 }, -- 30-- Stand_4 - 광폭화 셀프 버프용 액션
    -- $$$$$$$$$$$$  콤보용 스킬  $$$$$$$$$$$$$$   
   { skill_index = 300311,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 1200, target = 3, combo1 = "32,100,0" }, -- 31-- Attack04_JumpAttack_Combo3
   { skill_index = 300312,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 1200, target = 3, combo1 = "29,100,0" }, -- 32-- Attack04_JumpAttack_Combo4
}
