--AiSpirit_Wind_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 8000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 250
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 8, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "Move_Back", rate = 20, loop = 1 },
   { action_name = "Attack01_X_cut", rate = 18, loop = 1 },
   { action_name = "Attack08_Drill", rate = 18, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 8, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 15, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 33725,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 1500, randomtarget = "0.5,0", target = 3 },--Attack06_Air_missile
   { skill_index = 300406, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 9000, target = 1},--Attack02_RedDNest_6rdSTATE (이펙트용)
   -- { skill_index = 300407, cooltime = 5000, rate = 100, prioriy =500, rangemin = 0, rangemax = 9000, target = 3, limitcount = 1, next_lua_skill_index = 1 },--Attack02_RedDNest_6rd (오라)
   { skill_index = 300407, cooltime = 5000, rate = 100, prioriy =500, rangemin = 0, rangemax = 9000, target = 3, limitcount = 1, next_lua_skill_index = 1,encountertime = 10000  },--Attack02_RedDNest_6rd (오라)
   { skill_index = 33724,  cooltime = 14000, rate = 80, rangemin = 900, rangemax = 1200, randomtarget = "0.5,0", target = 3 , next_lua_skill_index= 0,encountertime = 10000 },--Attack06_Air_missile
   { skill_index = 33728,  cooltime = 22000, rate = 60, rangemin = 600, rangemax = 1200, target = 3, randomtarget = 1.5,encountertime = 10000 },--Attack03_Vacuum
   { skill_index = 33729,  cooltime = 62000, rate = 60, rangemin = 0, rangemax = 400, target = 3,encountertime = 10000 },--Attack04_Air_blossom
}
