--AiDeathKnight_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 8000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 3000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1,  },
   { action_name = "Attack12_Hammer_Boss", rate = 12, loop = 1 },
   { action_name = "Attack13_Combo", rate = 20, loop = 1, cooltime = 10000, globalcooltime = 1,encountertime = 15000 },
   { action_name = "Walk_Left", rate = 6, loop = 2 },
   { action_name = "Walk_Right", rate = 6, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1,  },
   { action_name = "Attack13_Combo", rate = 20, loop = 1, cooltime = 10000, globalcooltime = 1,encountertime = 15000 },
   { action_name = "Walk_Left", rate = 6, loop = 2 },
   { action_name = "Walk_Right", rate = 6, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
   { action_name = "Assault", rate = 16, loop = 1,encountertime = 15000  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 6, loop = 2 },
   { action_name = "Assault", rate = 16, loop = 1,encountertime = 15000  },
}
g_Lua_Assault = { 
   { action_name = "Attack12_Hammer_Boss", rate = 8, loop = 1, approach = 400 },
}

g_Lua_SkillProcessor = {
   { skill_index = 34025, changetarget = "1000,0" },
}

g_Lua_GlobalCoolTime1 = 12000
g_Lua_GlobalCoolTime2 = 18000

g_Lua_Skill = { 
   { skill_index = 300406, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 1 },--Attack02_RedDNest_6rdSTATE (이펙트용)
   -- { skill_index = 300408, cooltime = 5000, rate = 100, prioriy = 500, rangemin = 0, rangemax = 9000, target = 3, limitcount = 1, next_lua_skill_index = 0 },--Attack02_RedDNest_6rd (오라)
   { skill_index = 300408, cooltime = 5000, rate = 100, prioriy = 500, rangemin = 0, rangemax = 9000, target = 3, limitcount = 1, },--Attack02_RedDNest_6rd (오라)
   { skill_index = 34022, cooltime = 25000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,2,0,1",encountertime = 15000 }, --Attack02_Marble
   { skill_index = 34023, cooltime = 35000, rate = 80, priority = 15, rangemin = 0, rangemax = 1200, target = 3,encountertime = 15000 }, --Attack08_Yawm
   { skill_index = 34025, cooltime = 40000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1", encountertime = 5000, selfhppercent = 75,encountertime = 15000 },--Attack12_Push_Start
}
   