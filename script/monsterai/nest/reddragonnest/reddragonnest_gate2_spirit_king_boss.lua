--/genmon 505051
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 20, loop = 1 },
	{ action_name = "Walk_Left", rate = 5, loop = 1 },
	{ action_name = "Walk_Right", rate = 5, loop = 1 },
	{ action_name = "Walk_Back", rate = 5, loop = 1 },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 23, loop = 1 },	
	{ action_name = "Walk_Front", rate = 8, loop = 1 },
	{ action_name = "Walk_Left", rate = 6, loop = 1 },
	{ action_name = "Walk_Right", rate = 6, loop = 1 },
}
g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 30, loop = 1 },	
   	{ action_name = "Walk_Front", rate = 13, loop = 1 },
    { action_name = "Walk_Left", rate = 5, loop = 1 },
	{ action_name = "Walk_Right", rate = 5, loop = 1 },
	{ action_name = "Assault", rate = 10, loop = 1, cooltime = 10000},
}
g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 8, loop = 1 },	
	{ action_name = "Move_Front", rate = 15, loop = 1, },
}
g_Lua_RandomSkill =
{{
	{{ skill_index = 300115, rate = 50, next_lua_skill_index = 6 },{ skill_index = 300128, rate = 50, next_lua_skill_index = 5 },},
	{{ skill_index = 300109, rate = 50, },{ skill_index = 300110, rate = 50, },},
	{{ skill_index = 300101, rate = 50, },{ skill_index = 300103, rate = 50, },},
	{{ skill_index = 300083, rate = 33,next_lua_skill_index=4, },{ skill_index = 300084, rate = 34,next_lua_skill_index=4, },{ skill_index = 300085, rate = 33,next_lua_skill_index=4, },},
}}
--/genmon 505051
--/go 14551 4 
g_Lua_GlobalCoolTime1 = 30000
g_Lua_GlobalCoolTime2 = 20000
g_Lua_GlobalCoolTime3 = 40000
g_Lua_GlobalCoolTime4 = 30000
g_Lua_GlobalCoolTime5 = 60000--������

g_Lua_Assault = { { lua_skill_index = 3, rate = 10, approach = 450},} --Attack01_RedDNest_Bash
g_Lua_Skill = { 

	--ChainAction-- 
	{ skill_index = 300129, cooltime = 3000, priority = 3, rate = -1, rangemin = 0, rangemax = 5000, target = 3,combo1="1,100,0" },--Attack14_RedDNest_ShooterICE_Start--00
	{ skill_index = 300121, cooltime = 3000, priority = 4, rate = -1, rangemin = 0, rangemax = 5000, target = 3,combo1="2,100,0"},--Attack14_RedDNest_ShooterFIRE_Start--01
	{ skill_index = 300119, cooltime = 3000, priority = 5, rate = -1, rangemin = 0, rangemax = 5000, target = 3,  },--Attack14_RedDNest_ShooterFINISH_Start--02
    { skill_index = 300093, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 350, target = 3, },--Attack01_RedDNest_Bash--03 (Assault)
	{ skill_index = 300113, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,multipletarget = "1,8,0,1",},--Attack13_RedDNest_RiseFire_Start
	{ skill_index = 300089, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, },--Attack04_RedDNest_Absorb_Start--05
	{ skill_index = 300099, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, },--Attack08_RedDNest_LinearWave--06
	--LimitAction--
	{ skill_index = 300096, cooltime = 3000, priority = 30, rate = 100, rangemin = 0, rangemax = 5000, target = 3, globalcooltime=4, selfhppercentrange = "51,75", limitcount = 1 },--Attack07_RedDNest_Limited_Start
	{ skill_index = 300096, cooltime = 3000, priority = 20, rate = 100, rangemin = 0, rangemax = 5000, target = 3, globalcooltime=4, selfhppercentrange = "26,50", limitcount = 1 },--Attack07_RedDNest_Limited_Start
	{ skill_index = 300096, cooltime = 120000, priority = 10, rate = 100, rangemin = 0, rangemax = 5000, target = 3, globalcooltime=4, selfhppercentrange = "0,25" },--Attack07_RedDNest_Limited_Start
	--Normal-Action--
	{ skill_index = 300081, cooltime = 12000, rate = 40, rangemin = 0, rangemax = 300, target = 3, },--Attack01_RedDNest_Bash
	{ skill_index = 300087, cooltime = 40000, rate = 20, rangemin = 500, rangemax = 1000, target = 3, randomtarget = "1.6,0,1" },--Attack03_RedDNest_LeftWave
	{ skill_index = 300107, cooltime = 25000, rate = 40, rangemin = 200, rangemax = 500, target = 3, randomtarget = "1.6,0,1" },--Attack11_RedDNest_Combo
	{ skill_index = 300125, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,75", combo1="0,100,0"},--Attack14_RedDNest_ShooterReady
	--RandomAction--
	---1)
	{ skill_index = 300115, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6,0,1", globalcooltime=1, skill_random_index = 1 },--Attack13_RedDNest_DashFire_Start
	{ skill_index = 300128, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6,0,1", globalcooltime=1 },--Attack13_RedDNest_DashIce_Start
	---2)
	{ skill_index = 300109, cooltime = 90000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,2,0,1", globalcooltime=1, selfhppercentrange = "0,50", skill_random_index = 2, },--Attack12_RedDNest_beIceAura_Start
	{ skill_index = 300110, cooltime = 90000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,2,0,1", globalcooltime=1, selfhppercentrange = "0,50" },--Attack12_RedDNest_beFireAura_Start
	---3)
	{ skill_index = 300105, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,8,0,1", globalcooltime=3, selfhppercentrange = "0,50", },--Attack10_RedDNest_GroundLast_Start
	{ skill_index = 300101, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,8,0,1", globalcooltime=3, selfhppercentrange = "50,100", skill_random_index = 3, },--Attack10_RedDNest_GroundFire_Start
	{ skill_index = 300103, cooltime = 50000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,8,0,1",  globalcooltime=3, },--Attack10_RedDNest_GroundIce_Start 
	---4)
	{ skill_index = 300083, cooltime = 70000, rate = 40, rangemin = 0, rangemax = 5000, target = 3, skill_random_index=4, encountertime = 40000, globalcooltime=1 },--Attack02_RedDNest_Howl (8) 
	{ skill_index = 300084, cooltime = 70000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 1},--Attack02_RedDNest_Howl (6)
	{ skill_index = 300085, cooltime = 70000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, globalcooltime = 1 },--Attack02_RedDNest_Howl (4)
	{ skill_index = 300127, cooltime = 60000, priority = 100, rate = 100, rangemin = 0, rangemax = 6000, target = 3,usedskill="300109",globalcooltime=5  },--Attack14_RedDNest_AuraHit
	{ skill_index = 300127, cooltime = 60000, priority = 100, rate = 100, rangemin = 0, rangemax = 6000, target = 3,usedskill="300110",globalcooltime=5  },--Attack14_RedDNest_AuraHit
}
