--AiPterosaur_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 2400;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 450
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Stand_1", rate = 2, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Assault", rate = 8, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Assault = { 
   { action_name = "Attack01_Slash", rate = 8, loop = 1, approach = 400 },
   { action_name = "Stand", rate = 2, loop = 1, approach = 400 },
}
-- Attack06_FlyHighFire & Attack06_FlyHigh_Loop
g_Lua_GlobalCoolTime1 = 50000
-- Attack02_Combo & Attack02_Combo_R & Attack02_Combo_L
g_Lua_GlobalCoolTime2 = 20000
 
g_Lua_Skill = { 
    -- Attack01_Slash
   { skill_index = 300021,  cooltime = 5000, rate = 70,rangemin = 0, rangemax = 500, target = 3, td = "FR,FL" }, -- 0
    -- Attack02_Combo
   { skill_index = 300048,  cooltime = 32000, globalcooltime = 2, rate = 80, rangemin = 300, rangemax = 1000, target = 3, td = "FR,FL", randomtarget = "0.6,0,1"}, -- 1
    -- Attack03_TailAttack
   { skill_index = 300049,  cooltime = 10000, rate = 90, rangemin = 0, rangemax = 400, target = 3, td = "BL,BR", randomtarget = "0.6,0,1" }, -- 2
    -- Attack04_Glide
   { skill_index = 300050,  cooltime = 45000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "0.6,0,1" }, -- 3
    -- Attack05_Breath
   { skill_index = 300051,  cooltime = 54000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, td = "FR,FL,LF,RF" , randomtarget = "0.6,0,1"}, -- 4
    -- Attack06_FlyHighFire
   { skill_index = 300052,  cooltime = 55000, globalcooltime = 1, rate = 50, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1", selfhppercent = 70, combo1 = "17,100,0" }, -- 5
    -- Attack07_Shout
   { skill_index = 300033,  cooltime = 180000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 90, combo1 = "4,100,0" }, -- 6
    -- Attack08_UpperCut
   { skill_index = 300028,  cooltime = 20000, rate = 70, rangemin = 0, rangemax = 400, target = 3, td = "FR,FL", combo1 = "16,100,0"}, -- 7
    -- Attack09_AirBreath
   { skill_index = 300057,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, priority = 95, selfhppercent = 75, limitcount = 1}, -- 8
   { skill_index = 300053,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, priority = 92, selfhppercent = 50, limitcount = 1}, -- 9
   { skill_index = 300057,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, priority = 90, selfhppercent = 25, limitcount = 1}, -- 10
   { skill_index = 300053,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, priority = 88, selfhppercent = 13, limitcount = 1}, -- 11
    -- Attack10_RevoltStamp_Combo
   { skill_index = 300060,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, combo1 = "18,100,0" }, -- 12
    -- Attack02_Combo_R
   { skill_index = 300058,  cooltime = 32000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange="55,70", priority = 85, limitcount = 1, combo1 = "12,100,0" }, -- 13
    -- Attack02_Combo_L
   { skill_index = 300059,  cooltime = 32000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange="30,45", priority = 80, limitcount = 1, combo1 = "12,100,0" }, -- 14
    -- Attack11_DeathWings
   { skill_index = 300055,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3}, -- 15
    -- Attack06_FlyHigh_Loop
   { skill_index = 300027,  cooltime = 55000, globalcooltime = 1, rate = 70, rangemin = 0, rangemax = 1500, target = 3, combo1 = "17,100,0"}, -- 16
    -- Stand_1
   { skill_index = 300046,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3}, -- 17
    -- Stand_Wait
   { skill_index = 300047,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4000, target = 3, combo1 = "15,100,0"}, -- 18
}