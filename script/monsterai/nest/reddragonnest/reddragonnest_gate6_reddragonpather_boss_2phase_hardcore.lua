-- /genmon 505132
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 2000
g_Lua_NearValue2 = 10000

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 800;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = 
{	{ action_name = "Stand",rate = 10, loop = 1 }, }
g_Lua_Near2 = 
{	{ action_name = "Stand",rate = 10, loop = 1 }, }

g_Lua_GlobalCoolTime1 = 8000 
g_Lua_GlobalCoolTime2 = 90000
g_Lua_GlobalCoolTime3 = 80000

g_Lua_RandomSkill =
{{
	{{ skill_index = 300379, rate = 25, },{ skill_index = 300380, rate = 25, },{ skill_index = 300381, rate = 25, },{ skill_index = 300382, rate = 25, },}, -- 순수이동
	{{ skill_index = 300396, rate = 25, next_lua_skill_index = 0 },{ skill_index = 300397, rate = 25, next_lua_skill_index = 0 },{ skill_index = 300398, rate = 25, next_lua_skill_index = 0  },{ skill_index = 300399, rate = 25, next_lua_skill_index = 0  },}, --브레스위한이동
	{{ skill_index = 300376, rate = 33, },{ skill_index = 300377, rate = 34, },{ skill_index = 300378, rate = 33, },}, -- Escape
}}	
g_Lua_Skill = {
   --Next_Skill--
   { skill_index = 300389, cooltime = 5000, rate = -1, priority = 100, rangemin = 0, rangemax = 8000, target = 3, },  -- Attack012_FlyBreath_Start@@@
   { skill_index = 300395, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, multipletarget = "1,4,0,1"},  -- Attack011_FlyEndureWind_Start@@@**HardCore
   --Limited --
   { skill_index = 300396, cooltime = 60000, rate = 100 ,priority = 10, rangemin = 0, rangemax = 8000, target = 3 ,selfhppercent = 99, skill_random_index = 2, next_lua_skill_index = 0 },  -- Attack008_Fly_MT03_Start  (T627) --> Attack012_FlyBreath_Start
   { skill_index = 300382, cooltime = 90000, rate = 100, rangemin = 0, rangemax = 8000, target = 3, next_lua_skill_index = 1,selfhppercent = 95 },  -- Attack008_Fly_MT12_Start (T626) --> Attack011_FlyEndureWind_Start@@@
   { skill_index = 300404, cooltime = 5000, rate = 100, priority = 200, rangemin = 0, rangemax = 16000, target = 3,selfhppercent = 80 },  -- Attack010_Fly_Howl_Start #
   -- move_Random--
   { skill_index = 300379, cooltime = 90000 ,rate = 100 , rangemin = 0, rangemax = 8000, target = 3, selfhppercent = 90 ,skill_random_index = 1, },  -- Attack008_Fly_MT03_Start  (T627)
   --Normal--
   { skill_index = 300393, cooltime = 15000, globalcooltime = 1, rate = 45, rangemin = 0, rangemax = 800, target = 3, td = "FR,RF,RB" },  -- Attack009_Fly_LeftAttack_Start (8M) 2.5-2@@@**HardCore
   { skill_index = 300394, cooltime = 15000, globalcooltime = 1, rate = 45, rangemin = 0, rangemax = 800, target = 3, td = "FL,LF,LB" },  -- Attack009_Fly_RightAttack_Start (8M)2.5-2@@@**HardCore
   { skill_index = 300385, cooltime = 35000, globalcooltime = 1, rate = 40, rangemin = 0, rangemax = 8000, target = 3 },  -- Attack009_Fly_RPressAttack_Start
   --NaturalRandom--
   { skill_index = 300376, cooltime = 40000, rate = 60, rangemin = 0, rangemax = 8000, target = 3, skill_random_index = 3, multipletarget = "1,8,0,1" },  -- Attack010_Fly_Howl_Start 7<>@@@**HardCore
   { skill_index = 300377, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 8000, target = 3 },  -- Attack010_Fly_Howl_Start 6<>@@@**HardCore
   { skill_index = 300378, cooltime = 60000, rate = -1, rangemin = 0, rangemax = 8000, target = 3 },  -- Attack010_Fly_Howl_Start 5<>@@@**HardCore
}   