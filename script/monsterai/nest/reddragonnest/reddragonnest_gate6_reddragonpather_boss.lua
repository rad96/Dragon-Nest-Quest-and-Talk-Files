-- /genmon 505131
g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 10000.0;
g_Lua_NearValue2 = 15000.0;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssualtTime = 3000


g_Lua_Near1 = 
	{
		{ action_name = "Stand",rate = 10, loop = 1 , }, 
		{ action_name = "Turn_Left", rate = 100, loop=-1, td = "LF,LB,BL" },
		{ action_name = "Turn_Right", rate = 100, loop=-1, td = "RF,RB,BR" },
	}
g_Lua_Near2 = 
	{ 
		{ action_name = "Stand",rate = 10, loop = 1 , }, 
	}

g_Lua_GlobalCoolTime1 = 25000 --PressAttack + GroundBreath + Fly
g_Lua_GlobalCoolTime2 = 65000 --Limit
g_Lua_GlobalCoolTime3 = 100000 --Limit

g_Lua_Skill = {
   { skill_index = 300354, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, multipletarget = "1,4,0,1", },  -- Attack009_Fly_RightAttack_Start--0
   { skill_index = 300361, cooltime = 5000, rate = -1, priority = 400, rangemin = 0, rangemax = 8000, target = 3, },  -- Attack015_FlyEnergyAim_Start--1
   { skill_index = 300363, cooltime = 5000, rate = -1, priority = 393, rangemin = 0, rangemax = 8000, target = 3, },  -- Attack016_Limited_Start--2
   -- Damaged --
   { skill_index = 300340, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, },  -- Attack000_Stun
   -- Pause--
   { skill_index = 300344, cooltime = 55000, rate = 30, rangemin = 0, rangemax = 15000, target = 3, selfhppercentrange = "40,80"},  -- Attack004_Howl_Start~
   { skill_index = 300345, cooltime = 55000, rate = 30, rangemin = 0, rangemax = 15000, target = 3, selfhppercentrange = "0,40",multipletarget = "1,8,0,1"},  -- Attack004_HowlWing_Start~
   --Side//Front--
   { skill_index = 300341, cooltime = 12000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, td = "FL,FR" },  -- Attack001_Bite--@@
   { skill_index = 300342, cooltime = 10000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, td = "FL, LF"  ,randomtarget = "0.6,0,1" },  -- Attack002_RightBash--7
   { skill_index = 300343, cooltime = 10000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, td = "FR, RF" ,randomtarget = "0.6,0,1"},  -- Attack003_LeftBash--8
   { skill_index = 300360, cooltime = 55000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 15000, target = 3, randomtarget = "0.6,0,1" },  -- Attack014_PressAttack_Start
   -- { skill_index = 300360, cooltime = 55000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 15000, target = 3, selfhppercentrange = "0,40", multipletarget = "1,2,0,1",},  -- Attack014_PressAttack_Start
   --Back--
   { skill_index = 300359, cooltime = 5000, rate = 100, priority=100, rangemin = 0, rangemax = 15000, target = 3, randomtarget= "0.6,0,1", td = "BL,BR" },  -- Attack013_TailBash
   --Chain--
   -- { skill_index = 300352, cooltime = 25000, rate = 50, globalcooltime = 1, rangemin = 800, rangemax = 8000, target = 3, td = "FL,FR,", selfhppercentrange = "0,40", },  -- Attack005_ShortBreath_Start
   { skill_index = 300346, cooltime = 45000, rate = 100, globalcooltime = 1, rangemin = 0, rangemax = 15000, target = 3, td = "FL,FR," },  -- Attack005_FrontShortBreath_Start
   --Air Add
   { skill_index = 300357, cooltime = 55000, rate = 50, globalcooltime = 1, globalcooltime2 = 2, rangemin = 0, rangemax = 15000, target = 3 ,multipletarget = "1,4,0,1",},  -- Attack011_FlyWind_Start
   { skill_index = 300355, cooltime = 90000, rate = 60, globalcooltime = 1, rangemin = 0, rangemax = 15000, target = 1, selfhppercent = 70, multipletarget = "1,4,0,1", },  -- Attack010_Fly_Howl_Start--���±�ü
   { skill_index = 300353, cooltime = 65000, rate = 50, globalcooltime = 1, globalcooltime2 = 2, rangemin = 0, rangemax = 15000, target = 3, selfhppercent = 60, combo1="0,100,0" },  -- Attack009_Fly_LeftAttack_Start
   { skill_index = 300356, cooltime = 90000, rate = 50, globalcooltime = 1, rangemin = 0, rangemax = 15000, target = 3, selfhppercent = 50 },  -- Attack011_FlyMassiveWind_Start@@
   -- limit --
   { skill_index = 300344, cooltime = 150000, rate = 100, priority = 400, globalcooltime = 3, rangemin = 0, rangemax = 15000, target = 3, selfhppercentrange = "40,75", next_lua_skill_index = 1},  -- Attack004_HowlWing_Start-->Attack015_FlyEnergyAim_Start
   { skill_index = 300344, cooltime = 150000, rate = 100, priority = 393, globalcooltime = 3, rangemin = 0, rangemax = 15000, target = 3, selfhppercentrange = "0,40", next_lua_skill_index = 2},  -- Attack004_HowlWing_Start-->Attack016_Limited_Start
}
   