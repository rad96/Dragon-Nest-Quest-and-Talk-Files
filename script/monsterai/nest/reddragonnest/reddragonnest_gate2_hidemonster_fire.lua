--/genmon 505052
--RedDragonNest_Gate2_Hidemonster_FIRE.lua
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 5000;


g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}

g_Lua_Skill = {
   { skill_index = 300131,  cooltime = 6000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,},--Attack_RD_Fire_Target03_Start
   { skill_index = 300132,  cooltime = 6000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,},--Attack_RD_Fire_Target09_Start
}