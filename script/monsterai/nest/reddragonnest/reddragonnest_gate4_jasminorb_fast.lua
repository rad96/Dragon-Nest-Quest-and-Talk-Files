--/genmon 505092
--St_Spirit_RedDNest_IceTeam.act
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 5000;


g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}

-- g_Lua_BeHitSkill =
	-- {{ lua_skill_index = 1, rate = 100, skill_index =300251 },}
g_Lua_GlobalCoolTime1 = 20000 
g_Lua_Skill = {
   { skill_index = 300254,  cooltime = 3000, rate = 100, rangemin = 0, rangemax = 5000, target = 3,encountertime=2000 ,selfhppercentrange = "100,100",},--Attack01_Ready_Start
   { skill_index = 300255,  cooltime = 3000, rate = 100, rangemin = 0, rangemax = 5000, target = 3,selfhppercentrange = "0,99",},--Attack01_Ready2_Start
   { skill_index = 300252,  cooltime = 9000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,encountertime= 5000,globalcooltime=1}, --Attack02_Boom
   { skill_index = 300253,  cooltime = 9000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,encountertime= 5000,globalcooltime=1}, --Attack02_Charge
}