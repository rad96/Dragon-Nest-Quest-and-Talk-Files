--/genmon 505051
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 10000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 20, loop = 1 },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 23, loop = 1 },	
}
g_Lua_RandomSkill =
{{
	{{ skill_index = 300115, rate = 50, next_lua_skill_index = 6 },{ skill_index = 300128, rate = 50, next_lua_skill_index = 5 },},
	{{ skill_index = 300109, rate = 50, },{ skill_index = 300111, rate = 50, },},
	{{ skill_index = 300101, rate = 50, },{ skill_index = 300103, rate = 50, },},
	{{ skill_index = 300083, rate = 33,next_lua_skill_index=4, },{ skill_index = 300084, rate = 34,next_lua_skill_index=4, },{ skill_index = 300085, rate = 33,next_lua_skill_index=4, },},
}}
--/genmon 505051
--/go 14551 4 
g_Lua_GlobalCoolTime1 = 20000
g_Lua_GlobalCoolTime2 = 20000
g_Lua_GlobalCoolTime3 = 40000
g_Lua_GlobalCoolTime4 = 30000

g_Lua_Skill = { 
	--ChainAction-- 
	{ skill_index = 300129, cooltime = 3000, priority = 3, rate = -1, rangemin = 0, rangemax = 10000, target = 3,combo1="1,100,0" },--Attack14_RedDNest_ShooterICE_Start--00
	{ skill_index = 300121, cooltime = 3000, priority = 4, rate = -1, rangemin = 0, rangemax = 10000, target = 3,combo1="2,100,0"},--Attack14_RedDNest_ShooterFIRE_Start--01
	{ skill_index = 300119, cooltime = 3000, priority = 5, rate = -1, rangemin = 0, rangemax = 10000, target = 3,  },--Attack14_RedDNest_ShooterFINISH_Start--02
	{ skill_index = 300125, cooltime = 15000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, combo1="0,100,0"},--Attack14_RedDNest_ShooterReady
}
