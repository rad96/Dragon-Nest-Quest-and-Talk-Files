g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 2400;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 450
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_Skill = { 
    -- Attack04_JumpAttack
   { skill_index = 300291,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3 }, -- 3
}
