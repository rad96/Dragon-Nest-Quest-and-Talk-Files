--/genmon 505054
--RedDragonNest_Gate2_Hidemonster_SkillSub.lua
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 5000;


g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 5000;


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}

g_Lua_Skill = {
   { skill_index = 300136,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,8,0,1"},--Attack_RD_2rd_SkillGround_Fire (T221)@
   { skill_index = 300137,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,8,0,1"},--Attack_RD_2rd_SkillGround_Ice (T222)@
   { skill_index = 300138,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,8,0,1"},--Attack_RD_2rd_SkillGround_Last (T223)@
   -- { skill_index = 300139,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "1,2,0,1"},--Attack_RD_2rd_LinePattern_Fire (T224)@
   { skill_index = 300139,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3,multipletarget = "1,2,0,1"},--Attack_RD_2rd_LinePattern_Fire (T224)
   { skill_index = 300140,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, },--Attack_RD_2rd_LinePattern_Ice (T225)@
}