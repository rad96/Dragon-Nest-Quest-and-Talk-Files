--/genmon 505091
--Attack01_RedDNest_Bash
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 8000;

g_Lua_PatrolBaseTime = 8000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssualtTime = 8000;

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 20, loop = 1 },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 15, loop = 1 },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}

g_Lua_RandomSkill =
{{
	{{ skill_index = 300212, rate = 50, },{ skill_index = 300213, rate = 50,},}, --1 Attack09_FullLaserL_Start
	{{ skill_index = 300207, rate = 50, },{ skill_index = 300208, rate = 50,},}, --2 Attack05_Stomp_Start
	{{ skill_index = 300205, rate = 50, },{ skill_index = 300206, rate = 50,},}, --3 Attack04_Throw_Start
}}

g_Lua_GlobalCoolTime1 = 50000 
g_Lua_GlobalCoolTime2 = 60000
g_Lua_GlobalCoolTime3 = 80000
g_Lua_GlobalCoolTime4 = 25000
g_Lua_GlobalCoolTime5 = 15000 -- 콤보와 Attack09안겹치게하기

g_Lua_Skill = {
	--Chain & Random Action-- 
   { skill_index = 300212,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, skill_random_index = 1 }, --Attack09_FullLaserL_Start  ~0 
   { skill_index = 300213,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, }, --Attack09_FullLaserR_Start  ~1
   { skill_index = 300229,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, }, --Attack09_FullLaserT_Start  ~2
   { skill_index = 300201,  cooltime = 5000, rate = -1, priority = 99, rangemin = 0, rangemax = 8000, target = 3,  }, --줄패턴실패
   { skill_index = 300235,  cooltime = 5000, rate = -1, priority = 99, rangemin = 0, rangemax = 8000, target = 3,  }, --줄패턴성공
	--LimitAction--
   { skill_index = 300230,  cooltime = 180000, rate = 100, priority = 99, rangemin = 0, rangemax = 8000, target = 3, usedskill="300231",limitcount = 1,}, --Attack_TimeLimit_Start -10분초과
   { skill_index = 300205,  cooltime = 40000, rate = 30, rangemin = 0, rangemax = 8000, target = 3, multipletarget = "1,8,0,1", next_lua_skill_index=1, globalcooltime =1, skill_random_index = 3}, --Attack04_Throw_Start
   { skill_index = 300206,  cooltime = 40000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, multipletarget = "1,8,0,1", next_lua_skill_index=1, globalcooltime =1, }, --Attack04_Throw2_Start
   { skill_index = 300218,  cooltime = 10000, rate = 100, priority = 90, rangemin = 0, rangemax = 8000, target = 3, selfhppercentrange = "0,75", limitcount = 1, globalcooltime = 2  }, --Attack00_LimitM4Square_Start
   { skill_index = 300218,  cooltime = 10000, rate = 100, priority = 80, rangemin = 0, rangemax = 8000, target = 3, selfhppercentrange = "0,50", limitcount = 1, globalcooltime = 2 }, --Attack00_LimitM4Rhombus_Start
   { skill_index = 300218,  cooltime = 10000, rate = 100, priority = 80, rangemin = 0, rangemax = 8000, target = 3, selfhppercentrange = "0,25", limitcount = 1, globalcooltime = 2 }, --Attack00_LimitM8Round_Start
   { skill_index = 300215,  cooltime = 90000, rate = 100, priority = 10, rangemin = 0, rangemax = 8000, target = 3, selfhppercent = 99, globalcooltime = 2, globalcooltime2 = 5 }, --Attack_T411  (Attack09_MirrorVoltexP1_P8)   --TriggerRadomSkill--
   { skill_index = 300219,  cooltime = 70000, rate = 50, rangemin = 0, rangemax = 8000, target = 3, selfhppercentrange = "51,100", next_lua_skill_index = 0 ,globalcooltime = 3 ,ecountertime = 40000 }, --Attack_FullLaser(L,R)
   { skill_index = 300220,  cooltime = 70000, rate = 50, rangemin = 0, rangemax = 8000, target = 3, selfhppercentrange = "0,50", next_lua_skill_index = 2 ,globalcooltime = 3   }, --Attack_FullLaserT
   { skill_index = 300209,  cooltime = 30000, rate = 100,  rangemin = 0, rangemax = 8000, target = 3, td = "RF,RB" ,globalcooltime = 4 }, --Attack06_LeftGAS_Start ~4
   { skill_index = 300210,  cooltime = 30000, rate = 100,  rangemin = 0, rangemax = 8000, target = 3, td = "LF,LB"  ,globalcooltime = 4 }, --Attack07_RightGAS_Start ~5
   { skill_index = 300211,  cooltime = 30000, rate = 100,  rangemin = 0, rangemax = 8000, target = 3, td = "BL,BR" ,globalcooltime = 4 }, --Attack08_BackGAS_Start ~6
	--Normal-Action--
   { skill_index = 300202,  cooltime = 10000, rate = 35, rangemin = 0, rangemax = 8000, target = 3, td = "FL,FR" }, --Attack01_Laser
   { skill_index = 300203,  cooltime = 15000, rate = 30, rangemin = 0, rangemax = 8000, target = 3, td = "FL,LF"}, --Attack02_RightLaser
   { skill_index = 300204,  cooltime = 15000, rate = 30, rangemin = 0, rangemax = 8000, target = 3, td = "FR,RF" }, --Attack03_LeftLaser
   -- { skill_index = 300207,  cooltime = 50000, rate = 72, rangemin = 0, rangemax = 8000, target = 3, globalcooltime = 5,}, --Attack05_Stomp_Start : Stomp 
   { skill_index = 300208,  cooltime = 50000, rate = 72, rangemin = 0, rangemax = 8000, target = 3, globalcooltime = 5, multipletarget = "1,8,0,1",}, --Attack05_Stomp_Start :  Land  
   { skill_index = 300214,  cooltime = 120000, rate = 100, rangemin = 0, rangemax = 8000, target = 3 ,selfhppercentrange = "0,50", multipletarget = "1,8,0,1",notusedskill = "300231" }, --Attack10_Combo_Start
   --TriggerRadomSkill--
   { skill_index = 300221,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3,  }, --Attack09_MirrorVoltexP1_Start
   { skill_index = 300222,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3,  }, --Attack09_MirrorVoltexP2_Start
   { skill_index = 300223,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3,  }, --Attack09_MirrorVoltexP3_Start
   { skill_index = 300224,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3,  }, --Attack09_MirrorVoltexP4_Start
   { skill_index = 300225,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3,  }, --Attack09_MirrorVoltexP5_Start
   { skill_index = 300226,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3,  }, --Attack09_MirrorVoltexP6_Start
   { skill_index = 300227,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3,  }, --Attack09_MirrorVoltexP7_Start
   { skill_index = 300228,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 8000, target = 3,  }, --Attack09_MirrorVoltexP8_Start
}
