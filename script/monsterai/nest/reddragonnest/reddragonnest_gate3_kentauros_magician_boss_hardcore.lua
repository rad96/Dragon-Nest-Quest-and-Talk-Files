--AiBroo_Blue_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1600;
g_Lua_NearValue3 = 2200;
g_Lua_NearValue4 = 2800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1, selfhppercentrange = "60,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Move_Back", rate = 4, loop = 1 },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1, selfhppercentrange = "60,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 2, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 2, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 16, loop = 1, td = "FL,FR"  },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 16, loop = 1, td = "FL,FR"  },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 16, loop = 1, td = "FL,FR"  },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}

g_Lua_Assault = { 
   { action_name = "Attack01_Bash", rate = 8, loop = 1, approach = 500 },
}

-- Attack02_Slash_Front && Attack02_Slash_Left && Attack02_Slash_Right
g_Lua_GlobalCoolTime1 = 30000

-- Attack11_Shout1 && Attack11_Shout2 && Attack11_Shout3
g_Lua_GlobalCoolTime2 = 140000

-- Attack12_Judgment_Finish_Odd_Number && Attack12_Judgment_Finish_Even_Number
g_Lua_GlobalCoolTime3 = 40000
g_Lua_GlobalCoolTime4 = 30000

g_Lua_RandomSkill ={
{
{    { skill_index = 300154, rate = 25 },   { skill_index = 300160, rate = 25 },    { skill_index = 300169, rate = 25 },    { skill_index = 300170, rate = 25 },  },
{    { skill_index = 300153, rate = 33 },   { skill_index = 300157, rate = 34 },    { skill_index = 300158, rate = 33 },  },
{    { skill_index = 300155, rate = 30 },   { skill_index = 300164, rate = 35 },    { skill_index = 300165, rate = 35 },  },
}
}

g_Lua_Skill = {
--기본 공격
   { skill_index = 300141, cooltime = 9000, rate = 70, rangemin = 0, rangemax = 600, target = 3, td = "FR,FL", encountertime = 3000 }, --0-- Attack01_Bash
   { skill_index = 300142, cooltime = 35000, rate = 70, globalcooltime = 1, rangemin = 0, rangemax = 2000, target = 3, td = "FR,FL", encountertime = 20000, combo1 = "21,100,0" }, --1-- Attack02_Slash_Front
   { skill_index = 300144, cooltime = 35000, rate = 70, globalcooltime = 1, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "0.6,0,1", td = "LF,LB", encountertime = 20000, combo1 = "21,100,0" }, --2-- Attack02_Slash_Left
   { skill_index = 300143, cooltime = 35000, rate = 70, globalcooltime = 1, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "0.6,0,1", td = "RF,RB", encountertime = 20000, combo1 = "21,100,0" }, --3-- Attack02_Slash_Right
   { skill_index = 300145, cooltime = 30000, rate = 70, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "0.6,0,1", td = "BR,BL", encountertime = 20000, combo1 = "21,100,0" }, --4-- Attack03_Tail
   { skill_index = 300152, cooltime = 35000, rate = 60, rangemin = 0, rangemax = 1000, target = 3, td = "FR,FL,LF,LB,RF,RB", randomtarget = "1.6,0,1", encountertime = 20000, combo1 = "6,100,0" }, --5-- Attack10_Combo_Step1
   { skill_index = 300156,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1500, target = 3, combo1 = "21,100,0" }, --6
   { skill_index = 300146, cooltime = 20000, rate = 70, rangemin = 0, rangemax = 600, target = 3, td = "FR,FL", encountertime = 20000, combo1 = "21,100,0" }, --7-- Attack04_JumpAttack
--대시 & 발사체 공격
   { skill_index = 300147, cooltime = 35000, globalcooltime = 4, rate = 60, rangemin = 800, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", encountertime = 20000, combo1 = "20,100,0" }, --8-- Attack05_Spear
   { skill_index = 300148, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, td = "FR,FL,LF,LB,RF,RB", randomtarget = "1.6,0,1", encountertime = 20000, combo1 = "10,100,0" }, --9-- Attack06_Dash
   { skill_index = 300149, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, multipletarget = "2,2,0,1", combo1 = "20,100,0" }, --10-- Attack07_DoubleBomb**HARDCORE
   { skill_index = 300159, cooltime = 35000, globalcooltime = 4, rate = 100, priority = 50, rangemin = 1000, rangemax = 1200, target = 3, td = "FR,FL", selfhppercent = 80, combo1 = "20,100,0" }, --11-- Attack_SummonSpear
--특수 공격 & 줄패턴 공격
   { skill_index = 300154, skill_random_index = 1, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "20,100,0" }, --12-- Attack12_Judgment_Finish_Odd_Number
   { skill_index = 300150, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "20,100,0" }, --13-- Attack08_ForestPrison
   { skill_index = 300167, cooltime = 1000, rate = 100, priority = 99, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, combo1 = "15,100,0", limitcount = 1, notusedskill  = "300166" }, --14-- Attack09_Spear_Spin
   { skill_index = 300151, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "13,100,0" }, --15
   { skill_index = 300153 , skill_random_index = 2, cooltime = 100000, rate = 100, priority = 80, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80, combo1 = "22,100,0" }, --16-- Attack11_Shout
   { skill_index = 300167, cooltime = 1000, rate = 100, priority = 98, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 50, combo1 = "15,100,0", multipletarget = "2,4,0,1", limitcount = 1, notusedskill  = "300166" }, --17-- Attack09_Spear_Spin
   { skill_index = 300167, cooltime = 1000, rate = 100, priority = 97, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 25, combo1 = "15,100,0", limitcount = 1, notusedskill  = "300166" }, --18-- Attack09_Spear_Spin
   { skill_index = 300155, skill_random_index = 3, cooltime = 90000, rate = 100, priority = 90, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80, combo1 = "20,100,0", notusedskill  = "300166" }, --19-- Attack13_Breath_TargetStatue1
-- 후딜 액션
   { skill_index = 300161, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, --20-- Stand1 - 2초
   { skill_index = 300162, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, --21-- Stand3 - 1초
   { skill_index = 300166, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 }, --22-- Stand4 - 2초
   
-- 석상 소환   
   { skill_index = 300163, cooltime = 1000, globalcooltime = 2, rate = 100, priority = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 85, limitcount = 1 }, -- 23-- Attack11_Shout4
   { skill_index = 300167, cooltime = 1000, rate = 100, priority = 96, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 12, combo1 = "15,100,0", limitcount = 1, notusedskill  = "300166" }, --14-- Attack09_Spear_Spin
   { skill_index = 300168, cooltime = 230000, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 60, combo1 = "12,100,0", notusedskill  = "300166" }, --12
}
