--AiOgre_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_3", rate = 4, loop = 1},
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_3", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 10, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 6, loop = 1  },
}

g_Lua_Assault = { 
   { lua_skill_index = 1, rate = 16, approach = 300 },
}

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Skill = {
-- �޺�(0:����Ʈ��/1:�Ʈ��/2:�⺻��)
   { skill_index = 34085, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 34092, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 500, target = 3, combo1 = "0,100,1" },
   { skill_index = 34092, cooltime = 32000, rate = 40, rangemin = 0, rangemax = 500, target = 3, combo1 = "0,100,1", td = "FR,FL" },
-- ��콺������Ʈ(����Ʈ��,�뽬,���)
   { skill_index = 34098, cooltime = 120000, priority = 50, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 50, next_lua_skill_index = 4 },
   { skill_index = 34099, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 7000, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 5 },
   { skill_index = 34082, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- ������Ʈ
   { skill_index = 34083, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 400, target = 3 },
-- ���� ������Ʈ
   { skill_index = 34084, cooltime = 50000, rate = 100, rangemin = 400, rangemax = 800, target = 3, randomtarget = "1.6,0,1", td = "LF,RF,LB,RB" },
-- ���շ��� - ����Ʈ�� / Ȧ��
   { skill_index = 34086, cooltime = 100000, globalcooltime = 1, rate = 70, priority = 30, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 40 },
-- Ȧ�� ����Ʈ
   { skill_index = 34091, cooltime = 80000, rate = 80, rangemin = 0, rangemax = 1100, target = 3 },
-- Ȧ�� ũ�ν�
   { skill_index = 34081, cooltime = 100000, globalcooltime = 1, rate = 40, priority = 40, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75 },
-- ����Ʈ�� ��Ʈ
   { skill_index = 34087, cooltime = 44000, rate = 70, rangemin = 0, rangemax = 1100, target = 3, td = "FL,FR" },
-- �뽬
   { skill_index = 34097, cooltime = 50000, rate = 50, rangemin = 0, rangemax = 1100, target = 3, randomtarget = "1.6,0,1", td = "FR,FL,LF,RF,LB,RB" },
}


