--/genmon 503761

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 6000;

g_Lua_PatrolBaseTime = 6000
g_Lua_PatrolRandTime = 6000
g_Lua_ApproachValue = 500
g_Lua_AssaultTime = 8000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 2, loop = 1 },
	{ action_name = "Walk_Left", rate = 13, loop = 1 },
	{ action_name = "Walk_Right", rate = 13, loop = 1 },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 3, loop = 1 },	
    { action_name = "Walk_Front", rate = 13, loop = 1 },
}
g_Lua_Near3 = 
{ 
    { action_name = "Walk_Left", rate = 6, loop = 1 },
	{ action_name = "Walk_Right", rate = 6, loop = 1 },
   	{ action_name = "Move_Front", rate = 15, loop = 1 },
	{ action_name = "Assault", rate = 13, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Walk_Left", rate = 3, loop = 1 },
	{ action_name = "Walk_Right", rate = 3, loop = 1 },
	{ action_name = "Move_Front", rate = 15, loop = 1 },
	{ action_name = "Assault", rate = 13, loop = 1 },
}

g_Lua_MeleeDefense = { { lua_skill_index = 5, rate = 100  },  }
g_Lua_RangeDefense = { { lua_skill_index = 5, rate = 100  }, }
g_Lua_Assault = { { lua_skill_index = 6, rate = 5, approach = 800},}
g_Lua_RandomSkill =
{{{
   { skill_index = 36221, rate = 25 },--Attack08_CrashWall_VolNest_12_1
   { skill_index = 36222, rate = 25 },--Attack08_CrashWall_VolNest_03_1
   { skill_index = 36223, rate = 25 },--Attack08_CrashWall_VolNest_06_1
   { skill_index = 36224, rate = 25 },--Attack08_CrashWall_VolNest_09_1
},}}

g_Lua_Skill = { 
  -- // NextSkill //--
    { skill_index = 36314,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, priority = 40,},--Attack10_ExplosionCri_Start75_VolNest--0
    { skill_index = 36315,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, priority = 40,},--Attack10_ExplosionCri_Start50_VolNest--1
    { skill_index = 36316,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, priority = 40,},--Attack10_ExplosionCri_Start25_VolNest--2
    { skill_index = 36132,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3,},--Attack10_ExplosionCri_Success_VolNest--3
    { skill_index = 36133,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3,},--Attack10_ExplosionCri_Failure_VolNest--4
    { skill_index = 36143,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, },--Attack06_Counter_VolNest--5
	{ skill_index = 36310,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 500, target = 3, },--Attack01_Swing_VolNest--6
    { skill_index = 36142,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3,},--Attack06_Counter_VolNest_Start--7
	{ skill_index = 36146,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3,},--Attack10_Fail_VolNest --8
    { skill_index = 36311,  cooltime = 30000, rate = -1, rangemin = 0, rangemax = 6000, target = 3,},--Attack12_FireBreath_Top_VolNest_End --9
    { skill_index = 36312, cooltime = 30000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, },--Attack12_FireBreath_Bottom_VolNest_End --10

    { skill_index = 36317,  cooltime = 30000, rate = 65, rangemin = 0, rangemax = 6000, target = 4, next_lua_skill_index= 9, selfhppercent = 95, },
    { skill_index = 36317,  cooltime = 30000, rate = 70, rangemin = 0, rangemax = 6000, target = 4, next_lua_skill_index= 10, selfhppercent = 95, },
  
   
   -- // LinePattern // --
   { skill_index = 36131, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, priority = 100, selfhppercent = 75, limitcount = 1, next_lua_skill_index= 0 },--Attack10_ExplosionCri_Ready_VolNest
   { skill_index = 36131, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, priority = 100, selfhppercent = 50, limitcount = 1, next_lua_skill_index= 1 },--Attack10_ExplosionCri_Ready_VolNest
   { skill_index = 36131, cooltime = 85000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, priority = 100, selfhppercent = 25 ,next_lua_skill_index = 2,  },--Attack10_ExplosionCri_Ready_VolNest
   --// Active// --
    { skill_index = 36137, cooltime = 8000, rate = 50, rangemin = 0, rangemax = 500, target = 3, },--Attack01_Swing_VolNest
	{ skill_index = 36138, cooltime = 15000, rate = 60, rangemin = 0, rangemax = 400, target = 3, },--Attack02_TailAttack_VolNest
	{ skill_index = 36139, cooltime = 20000, rate = 100, rangemin = 0, rangemax = 500, target = 3, },--Attack03_ShieldAttack_VolNest
	{ skill_index = 36140, cooltime = 35000, rate = 80, rangemin = 0, rangemax = 900, target = 3, encountertime=8000 },--Attack04_Charge_VolNest_Start
	{ skill_index = 36144, cooltime = 45000, rate = 80, rangemin = 0, rangemax = 6000, target = 3,multipletarget = "1,4,0,1",encountertime=10000 },--Attack07_Explospell_VolNest
    { skill_index = 36221, cooltime = 35000, rate = 70, rangemin = 0, rangemax = 6000, target = 3, skill_random_index = 1, },--Attack08_CrashWall_VolNest_12_1~(random)
	{ skill_index = 36145, cooltime = 40000, rate = 50, rangemin = 0, rangemax = 6000, target = 3, selfhppercent = 50, },--Attack09_Provoke_VolNest
	{ skill_index = 36141, cooltime = 60000, rate = 50, rangemin = 0, rangemax = 6000, target = 3,selfhppercent = 50, next_lua_skill_index=7 },--Attack11_Quake_VolNest
        { skill_index = 36313, cooltime = 28000, rate = 100, rangemin = 0, rangemax = 500, target = 3, },--Attack13_Circle_VolNest_Start
      --  { skill_index = 36311, cooltime = 20000, rate = 70, rangemin = 0, rangemax = 500, target = 3,},--Attack12_FireBreath_Top_VolNest_End
      -- { skill_index = 36312, cooltime = 20000, rate = 70, rangemin = 0, rangemax = 500, target = 3, },--Attack12_FireBreath_Bottom_VolNest_End
}
--�۷ι� ��Ÿ�� ���߾����.