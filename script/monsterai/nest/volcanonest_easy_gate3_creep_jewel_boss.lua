-- /genmon 503741
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 6000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop=1,td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Walk_Right", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 3, loop=1,td = "FR,FL" },
   { action_name = "Walk_Front", rate = 8, loop=1,td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Walk_Right", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Move_Front", rate = 8, loop=1,td = "FR,FL" },
   { action_name = "Stand", rate = 13, loop = 1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 10, loop=1,td = "FR,FL" },
   { action_name = "Assault", rate = 6, loop = 1, td = "FR,FL",cooltime=8000 },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Assault = { { lua_skill_index = 2, rate = 5, approach = 600},}

g_Lua_GlobalCoolTime1 = 31000 
g_Lua_GlobalCoolTime2 = 20000 
	
g_Lua_Skill = {
	-- // MAIN_SKILL // -
    { skill_index = 36041, cooltime = 70000, rate = 100, rangemin = 0, rangemax = 6000, priority = 100, target = 3, selfhppercentrange = "0,95" },--Attack08_JewelCheck_Start_Volcano --0
	-- NextSkill --
	{ skill_index = 36059, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, },--Attack13_DashFying_VolNest --1
	{ skill_index = 36047, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000,  target = 3, td = "FR,FL" },--Attack01_Slash_VolNest--2
	{ skill_index = 36053, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3,  },--Attack05_JumpAttackMO_Start_VolNest--3
    -- // OrderSkill - receive-
	{ skill_index = 36042, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, priority = 90, target = 3, },--Attack08_JewelTreatA_Volcano
    { skill_index = 36043, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, priority = 90, target = 3, },--Attack08_JewelTreatB_Volcano
    { skill_index = 36044, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, priority = 90, target = 3, },--Attack08_JewelTreatC_Volcano
    { skill_index = 36045, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, priority = 90, target = 3, },--Attack08_JewelTreatD_Volcano
    { skill_index = 36046, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, priority = 90, target = 3, },--Attack08_JewelTreatE_Volcano
	--Skill--
	{ skill_index = 36050, cooltime = 46000, rate = 70, globalcooltime = 1, rangemin = 500, rangemax = 1000, target = 3, td = "FR,FL" },--Attack05_JumpAttack_05M
	{ skill_index = 36051, cooltime = 46000, rate = 70, globalcooltime = 1, rangemin = 700, rangemax = 1500, target = 3, td = "FR,FL" },--Attack05_JumpAttack_08M
	{ skill_index = 36052, cooltime = 46000, rate = 70, globalcooltime = 1, rangemin = 1000, rangemax = 1800, target = 3, td = "FR,FL" },--Attack05_JumpAttack_11M
	{ skill_index = 36055, cooltime = 90000, rate = 80, globalcooltime = 2, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "51,100",td = "FR,FL"},--Attack12_Breath_VolNest
	{ skill_index = 36060, cooltime = 90000, rate = 80, globalcooltime = 2, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "0,50",td = "FR,FL"},--Attack12_BreathEnhance_VolNest
	{ skill_index = 36054, cooltime = 100000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, randomtarget = "1.6,0,1",selfhppercentrange = "0,50", encountertime= 15000,next_lua_skill_index=3,td = "FL,FR,LF,RF,RB,BR,BL,LB"  },--Attack04_JumpAttackMO_Ready
	{ skill_index = 36047, cooltime = 24000, rate = 60, rangemin = 0, rangemax = 500, target = 3, td = "FR,FL" },--Attack01_Slash_VolNest
	{ skill_index = 36058, cooltime = 70000, rate = 90, rangemin = 0, rangemax = 1500, target = 3,  randomtarget = "1.6,0,1",selfhppercentrange = "0,75",td = "FL,FR,LF,RF,RB,BR,BL,LB" },--Attack13_Dash_Start_VolNest
	{ skill_index = 36048, cooltime = 80000, rate = 70, rangemin = 500, rangemax = 3000, target = 3,td = "FL,FR,LF,RF,RB,BR,BL,LB" },--Attack02_Shake_VolNest
	{ skill_index = 36049, cooltime = 1000, rate = 45, rangemin = 0, rangemax = 6000, target = 3, td = "BR,BL", randomtarget = "1.6,0,1"  },--Attack03_Retreat_VolNest
}


