-- /genmon 503741
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 4000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 450
g_Lua_AssualtTime = 6000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop=1 },
   { action_name = "Walk_Left", rate = 4, loop = 1,td = "FL,FR" },
   { action_name = "Walk_Right", rate = 4, loop = 1,td = "FL,FR" },
   { action_name = "Turn_Left", rate = 30, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 30, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop=1 },
   { action_name = "Walk_Front", rate = 10, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 30, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 30, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 30, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 30, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop=1,td = "FR,FL" },
   { action_name = "Assault", rate = 12, loop = 1, td = "FR,FL" },
   { action_name = "Turn_Left", rate = 30, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 30, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Assault = { { lua_skill_index = 2, rate = 5, approach = 450},}

g_Lua_GlobalCoolTime1 = 31000 
g_Lua_GlobalCoolTime2 = 42000 
g_Lua_GlobalCoolTime3 = 30000
g_Lua_GlobalCoolTime4 = 10000
g_Lua_GlobalCoolTime5 = 8000
	
g_Lua_Skill = {
	-- // MAIN_SKILL // -
	{ skill_index = 36041, cooltime = 60000, globalcooltime = 5, globalcooltime2 = 3, rate = 80, rangemin = 0, rangemax = 6000, target = 3, selfhppercentrange = "0,95" },--Attack08_JewelCheck_Start_Volcano
	-- NextSkill --
	{ skill_index = 36059, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },--Attack13_DashFying_VolNest --1
	{ skill_index = 36047, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },--Attack01_Slash_VolNest--2
	{ skill_index = 36304, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },--Attack05_JumpAttackMO_Start_VolNest  36053>36304 루프타임만 바꿨음> --3
	{ skill_index = 36047, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, randomtarget = "1.6,0,1" },
	-- // OrderSkill - receive-
	{ skill_index = 36042, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, priority = 90, target = 3 },--Attack08_JewelTreatA_Volcano
	{ skill_index = 36043, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, priority = 90, target = 3 },--Attack08_JewelTreatB_Volcano
	{ skill_index = 36044, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, priority = 90, target = 3 },--Attack08_JewelTreatC_Volcano
	{ skill_index = 36045, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, priority = 90, target = 3 },--Attack08_JewelTreatD_Volcano
	{ skill_index = 36046, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, priority = 90, target = 3 },--Attack08_JewelTreatE_Volcano
	--Skill--
	{ skill_index = 36047, cooltime = 10000, rate = 80, rangemin = 0, rangemax = 450, target = 3, td = "FR,FL", notusedskill = 36041 },--Attack01_Slash_VolNest
	{ skill_index = 36050, cooltime = 21000, rate = 70, rangemin = 300, rangemax = 800, target = 3, td = "FR,FL", randomtarget = "1.6,0,1", notusedskill = 36041 },--Attack05_JumpAttack_05M
	{ skill_index = 36056, cooltime = 42000, rate = 60, globalcooltime = 2, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "51,100",td = "FR,FL", notusedskill = 36041 },--Attack12_Breath_VolNest
	{ skill_index = 36057, cooltime = 42000, rate = 60, globalcooltime = 2, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "0,50",td = "FR,FL", notusedskill = 36041 },--Attack12_BreathEnhance_VolNest
	{ skill_index = 36054, cooltime = 50000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "1.6,0,1",selfhppercent = 50, encountertime= 15000, next_lua_skill_index = 3, notusedskill = 36041 },--Attack04_JumpAttackMO_Ready
	{ skill_index = 36058, cooltime = 35000, rate = 70, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 75, notusedskill = 36041 },--Attack13_Dash_Start_VolNest
	{ skill_index = 36048, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 1000, target = 3, notusedskill = 36041 },--Attack02_Shake_VolNest
	{ skill_index = 36049, cooltime = 60000, globalcooltime = 3, rate = 100, rangemin = 0, rangemax = 600, target = 3, td = "FL,FR,BR,BL", randomtarget = "0.6,0,1", selfhppercent = 75, notusedskill = 36041 },--Attack03_Retreat_VolNest
	--추가스킬--
	{ skill_index = 36300, cooltime = 70000, rate = 90, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 50 },--마그마브레스 원래 쿨탐 40초
	{ skill_index = 36301, cooltime = 40000, rate = 40, rangemin = 0, rangemax = 1200, target = 3, td = "FL,FR", encountertime = 20000 },--추가스킬 콤파스 Attack07_Compass_Start_Trial
	--보석 체크 후 돌진 / Attack13_Dash_Start_VolNest--
	{ skill_index = 36306, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 90, rangemin = 4000, rangemax = 4500, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 4, usedskill = 36041 },
	{ skill_index = 36306, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 80, rangemin = 3500, rangemax = 4000, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 4, usedskill = 36041 },
	{ skill_index = 36306, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 70, rangemin = 3000, rangemax = 3500, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 4, usedskill = 36041 },
	{ skill_index = 36306, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 60, rangemin = 2500, rangemax = 3000, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 4, usedskill = 36041 },
	{ skill_index = 36306, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 50, rangemin = 2000, rangemax = 2500, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 4, usedskill = 36041 },
	{ skill_index = 36306, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 40, rangemin = 1500, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 4, usedskill = 36041 },
	{ skill_index = 36306, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 30, rangemin = 1000, rangemax = 1500, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 4, usedskill = 36041 },
	{ skill_index = 36306, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 20, rangemin = 500, rangemax = 1000, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 4, usedskill = 36041 },
	{ skill_index = 36306, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 10, rangemin = 0, rangemax = 500, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 4, usedskill = 36041 },
	--테스트용스킬--
	-- { skill_index = 36300, cooltime = 10000, rate = 100, rangemin = 2500, rangemax = 2000, target = 3, selfhppercentrange = "0,100",},--마그마브레스 원래 쿨탐 40초
}


