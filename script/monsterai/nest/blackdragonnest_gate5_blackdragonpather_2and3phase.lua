g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 9000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_CustomAction =
{
   CustomAction1 = 
   {
	{ "Turn_Left", 1 },
 	{ "useskill", lua_skill_index = 0, rate = 100 },
   },
   CustomAction2 = 
   {
 	{ "Turn_Right", 1 },
 	{ "useskill", lua_skill_index = 1, rate = 100 },
   },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
   { action_name = "CustomAction1", rate = 50, loop = 1, cooltime = 15000, priority = 45, td = "LF,LB,BL" },
   { action_name = "CustomAction2", rate = 50, loop = 1, cooltime = 15000, priority = 45, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_RandomSkill ={
{
{
--1 Fly_Dash
   { skill_index = 34856, rate = 50 }, 
   { skill_index = 34857, rate = 50 },
},
{
--2 FlyTurnBreath
   { skill_index = 34852, rate = 50 }, 
   { skill_index = 34853, rate = 50 },
},
{
--3 Attack05_ShortBreath_Left
   { skill_index = 34881, rate = 65 },
   { skill_index = 34846, rate = 35 },
},
{
--4 Attack05_ShortBreath_Right
   { skill_index = 34882, rate = 65 },
   { skill_index = 34847, rate = 35 }, 
},
}
}


g_Lua_GlobalCoolTime1 = 15000 -- Turn이후 Slash
g_Lua_GlobalCoolTime2 = 14000 -- Bite, Bash
g_Lua_GlobalCoolTime3 = 25000 -- ShortBreath
g_Lua_GlobalCoolTime4 = 27000 -- Stomp
g_Lua_GlobalCoolTime5 = 45000 -- Fly 계열

g_Lua_Skill = {
--Turn_Bash
	{ skill_index = 34874, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
	{ skill_index = 34875, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },

--Attack08_Start3Phase_Start
--2Phase 전용 AI
	{ skill_index = 34876, cooltime = 1000, rate = 100, priority = 100, rangemin = 0, rangemax = 9000, target = 3, selfhppercent = 50 },

--Attack04_TailBash (4sec)
	{ skill_index = 34844, cooltime = 5000, rate = 100, priority = 20, rangemin = 0, rangemax = 3000, target = 3, randomtarget= "0.6,0,1", td = "BL,BR", encountertime = 7000 },

--Attack05_ShortBreath_Front(3.8sec), Left, Right
	{ skill_index = 34845, cooltime = 27000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", encountertime = 7000 },
	{ skill_index = 34881, skill_random_index = 3, cooltime = 11000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, td = "LF,LB", randomtarget = "0.6,0,1", encountertime = 7000 },
	{ skill_index = 34882, skill_random_index = 4, cooltime = 11000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, td = "RF,RB", randomtarget = "0.6,0,1", encountertime = 7000 },

--Attack001_Bite(3sec) / Attack002_Bash_Left(2.4sec) / Attack003_Bash_Right(2.4sec)
	{ skill_index = 34841, cooltime = 20000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", encountertime = 7000 },
	{ skill_index = 34842, cooltime = 10000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "FL,LF", randomtarget = "0.6,0,1", encountertime = 7000 },
	{ skill_index = 34843, cooltime = 10000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, td = "FR,RF", randomtarget = "0.6,0,1", encountertime = 7000 },

--Attack06_Meteor_Start(9sec)
	{ skill_index = 34848, cooltime = 47000, rate = 70, rangemin = 0, rangemax = 6000, target = 3, multipletarget = "1,8,0,1", encountertime = 7000 },

--Attack08_Trap_Start(35.75sec)
	{ skill_index = 34850, cooltime = 96000, rate = 100, rangemin = 0, rangemax = 9000, target = 3, multipletarget = "1,8,0,1", selfhppercent = 70 },

--Attack09_Fly_Stomp 34878(10.75sec) -> 탱커가 정면에 없을 때, 탱커를 쫓아감.
	{ skill_index = 34851, cooltime = 57000, globalcooltime = 5, rate = 70, rangemin = 0, rangemax = 3000, target = 3, randomtarget= "0.6,0,1", td = "RF,RB,LF,LB", encountertime = 7000 },

--Attack10_FlyTurnBreath_In_Start(11sec)
	{ skill_index = 34852, skill_random_index = 2, cooltime = 45000, globalcooltime = 5, rate = 70, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 60 },
--Attack12_Fear_Start (Land)(9.6sec)
	{ skill_index = 34854, cooltime = 50000, globalcooltime = 5, rate = 60, rangemin = 0, rangemax = 3000, target = 3, encountertime = 7000 },
--Attack13_Fly_Dash_Start (Land - 1)(10.5 / 16.5sec)
	{ skill_index = 34856, skill_random_index = 1, cooltime = 50000, globalcooltime = 5, rate = 70, rangemin = 0, rangemax = 3000, target = 3, randomtarget= "1.6,0,1", encountertime = 7000 },
}


