--AiBroo_Blue_Normal.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000;
g_Lua_NearValue2 = 3500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Attack_flock", rate = -1, loop = 1, cooltime = 5000 },
   { action_name = "Attack_FrogWarriorAttack_Guardian", rate = -1, loop = 1, cooltime = 20000, encountertime = 5000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 6, loop = 1 },
}