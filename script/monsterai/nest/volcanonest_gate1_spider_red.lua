--/genmon 503713

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 3000;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Attack3_DashBite", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 15, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 10, loop = 1 },
   { action_name = "Move_Right", rate = 10, loop = 1 },
   { action_name = "Walk_Back", rate = 20, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 20, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Front", rate = 10, loop = 3},
   { action_name = "Move_Front", rate = 20, loop = 3 },
}

g_Lua_Skill = { 
   { skill_index = 35315,  cooltime = 15000, rate = 30,rangemin = 300, rangemax = 800, target = 3, encountertime=5000, max_missradian=30 },-- 거미줄 발사??
   { skill_index = 36075,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 6000, target = 3,  }, --Attack_VolNest_Break
   { skill_index = 36076,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, encountertime=25000 },--Attack8_VolNest_Explosion
}



