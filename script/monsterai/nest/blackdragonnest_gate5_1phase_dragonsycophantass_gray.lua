--AiDragonSycophantAss_Gray_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Attack1_3Combo_BlackDragon", rate = 27, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 1, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 1, loop = 4 },
   { action_name = "Assault", rate = 19, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 1, loop = 4 },
   { action_name = "Assault", rate = 19, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_3Combo_BlackDragon", rate = 8, loop = 1, approach = 200, max_missradian = 30 },
   { action_name = "Walk_Left", rate = 3, loop = 1, approach = 200 },
   { action_name = "Walk_Right", rate = 3, loop = 1, approach = 200 },
}

g_Lua_Skill = { 
   { skill_index = 34921, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
   { skill_index = 34923, cooltime = 10000, rate = 100, rangemin = 700, rangemax = 1500, target = 3 },
}
