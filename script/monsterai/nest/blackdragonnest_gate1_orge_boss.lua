g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 2500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
   { action_name = "Stand_1", rate = 6, loop = 1, selfhppercentrange = "50,100" },
   { action_name = "Stand", rate = 6, loop = 1, selfhppercentrange = "0,50" },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1,},
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand_1", rate = 4, loop = 1, selfhppercentrange = "50,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,50" },
   { action_name = "Walk_Front", rate = 10, loop = 1},
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 3 },
   { action_name = "Assault", rate = 16, loop = 1 },
}

g_Lua_GlobalCoolTime1 = 28000 --Attack20_FireIntheHole
g_Lua_GlobalCoolTime2 = 14000 --Attack5_Spit_BD
g_Lua_GlobalCoolTime3 = 25000 --Attack13_Dash_BD	
g_Lua_GlobalCoolTime4 = 14000 --Attack11_ReturnMace


g_Lua_Assault = 
{ 
   { lua_skill_index = 3, rate = 20, approach = 300 },
}

g_Lua_Skill = {
    -- TriggerAction --
    { skill_index = 34715, cooltime = 80000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, priority=300, next_lua_skill_index = 1, selfhppercent = 75 },--Attack10_SummonBarricade
    { skill_index = 34720, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 2, priority=300 },--Attack19_CountMassiveDash_Strart
    { skill_index = 34722, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, priority=300 },--Attack21_MassiveDash_Start
    -- Assault & CustomAction --
    { skill_index = 34711, cooltime = 100, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },--Attack1_Bash_BD
    { skill_index = 34713, cooltime = 100, rate = -1, rangemin = 0, rangemax = 900, target = 3, randomtarget = "1.6,0,1" },--Attack7_JumpAttack_BD
    -- NormalAction --
    { skill_index = 34711, cooltime = 12000, rate = 40, rangemin = 0, rangemax = 300, target = 3, td = "FR,FL" },--Attack1_Bash_BD

    { skill_index = 34721, cooltime = 40000, globalcooltime = 1, rate = 70, rangemin = 800, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "50,100", encountertime = 15000 },--Attack20_FireIntheHole
    { skill_index = 34721, cooltime = 28000, globalcooltime = 1, rate = 70, rangemin = 800, rangemax = 2000, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "0,50" },--Attack20_FireIntheHole

    { skill_index = 34712, cooltime = 20000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 400, target = 3, next_lua_skill_index = 4, selfhppercentrange = "50,100" }, --Attack5_Spit_BD
    { skill_index = 34712, cooltime = 14000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 400, target = 3, next_lua_skill_index = 4, selfhppercentrange = "0,50" },

    { skill_index = 34718, cooltime = 36000, globalcooltime = 3, rate = 50, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "50,100", encountertime=20000 }, --Attack13_Dash_BD		
    { skill_index = 34718, cooltime = 25000, globalcooltime = 3, rate = 50, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "0,50" }, --Attack13_Dash_BD		

    { skill_index = 34716, cooltime = 20000, globalcooltime = 4, rate = 70, rangemin = 0, rangemax = 1200, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "50,100" },--Attack11_ReturnMace
    { skill_index = 34716, cooltime = 14000, globalcooltime = 4, rate = 70, rangemin = 0, rangemax = 1200, target = 3, randomtarget = "1.6,0,1", selfhppercentrange = "0,50" },--Attack11_ReturnMace

    { skill_index = 34719, cooltime = 20000, rate = 40, rangemin = 0, rangemax = 500, target = 3, td = "FR,FL", selfhppercent = 75 },--Attack14_Combo_BD

    { skill_index = 34714, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 6000, target = 3, selfhppercent = 50 },--Attack9_Punch_BD	

    { skill_index = 34717, cooltime = 35000, rate = 70, rangemin = 0, rangemax = 1200, target = 3, td = "FR,FL,LF,RF", randomtarget = "1.6,0,1", selfhppercent = 25 },--Attack12_Thread_BD
}


