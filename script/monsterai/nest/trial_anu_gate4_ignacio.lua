--AiOgre_Black_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssaultTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_3", rate = 4, loop = 1},
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_3", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 20, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 13, loop = 1  },
}

g_Lua_Assault = { 
   { lua_skill_index = 2, rate = 16, approach = 300 },
}

g_Lua_GlobalCoolTime1 = 12000
g_Lua_GlobalCoolTime2 = 8000
g_Lua_GlobalCoolTime3 = 6000

g_Lua_Skill = {
-- 콤보(0:라이트닝/1:어설트용/2:기본용)
   { skill_index = 34206, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 90, limitcount = 1, encountertime=12000 },
   { skill_index = 34206, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 34092, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 500, target = 3, combo1 = "1,100,1" },
   { skill_index = 34092, cooltime = 16000, rate = 40, rangemin = 0, rangemax = 500, target = 3, combo1 = "1,100,1", td = "FR,FL" },
-- 헤븐스저지먼트(대쉬,헤븐)
   { skill_index = 34099, cooltime = 60000, globalcooltime = 1, priority = 50, rate = 100, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6,0,1", selfhppercent = 50, next_lua_skill_index = 5 },
   { skill_index = 34204, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 6  },
   { skill_index = 34206, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 차지볼트
   { skill_index = 34083, cooltime = 25000, rate = 60, rangemin = 0, rangemax = 400, target = 3 },
-- 점프 차지볼트
   { skill_index = 34084, cooltime = 25000, rate = 100, rangemin = 400, rangemax = 800, target = 3, randomtarget = "1.6,0,1", td = "LF,RF,LB,RB" },
-- 서먼렐릭 - 세계수 1줄 패턴
   { skill_index = 34207, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 2, rate = 100, priority = 100, rangemin = 500, rangemax = 5000, target = 3, selfhppercent = 75, limitcount = 1 },
   { skill_index = 34207, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 2, rate = 100, priority = 90, rangemin = 500, rangemax = 5000, target = 3, selfhppercent = 50, limitcount = 1 },
   { skill_index = 34207, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 2, rate = 100, priority = 80, rangemin = 500, rangemax = 5000, target = 3, selfhppercent = 25, limitcount = 1  },
-- 홀리 버스트
   { skill_index = 34091, cooltime = 40000, rate = 80, rangemin = 0, rangemax = 1100, target = 3 },
-- 홀리 크로스
   { skill_index = 34081, cooltime = 50000, globalcooltime = 1, globalcooltime2 = 3, rate = 40, priority = 40, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75 },
-- 라이트닝 볼트
   { skill_index = 34087, cooltime = 22000, rate = 70, rangemin = 0, rangemax = 1100, target = 3, td = "FL,FR" },
-- 그랜드 라이트닝 크로스
   { skill_index = 34201, cooltime = 50000, globalcooltime = 2, globalcooltime2 = 3,  rate = 40, priority = 50, rangemin = 0, rangemax = 5000, target = 4, selfhppercent = 75 },
-- 대쉬
   { skill_index = 34097, cooltime = 25000, rate = 50, rangemin = 0, rangemax = 1100, target = 3, randomtarget = "1.6,0,1", td = "FR,FL,LF,RF,LB,RB" },
}


