--DragonSycophantPuni_Green_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 2200;
g_Lua_NearValue5 = 5200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Attack_BD_Zone5_1Phase_01_Chopping", rate = 15, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 1, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Attack_BD_Zone5_1Phase_03_JumpChopping", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 8, loop = 3 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 1, loop = 8 },
   { action_name = "Assault", rate = 19, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Assault", rate = 19, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack_BD_Zone5_1Phase_01_Chopping", rate = 8, loop = 1, approach = 400 },
   { action_name = "Attack_BD_Zone5_1Phase_03_JumpChopping", rate = 4, loop = 1, approach = 1000 },
}
g_Lua_Skill = { 
   { skill_index = 34921, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 9000, target = 3 },
-- 01_Chopping
   { skill_index = 34954, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 07_Berserk
   { skill_index = 34956, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 9000, target = 1, notusedskill = "34956" },
-- 12_ChainAttack
   { skill_index = 34957, cooltime = 30000, rate = 60, rangemin = 1000, rangemax = 1200, target = 3, next_lua_skill_index = 1, encountertime = 5000 },
-- 05_Whirlwind
   { skill_index = 34955, cooltime = 50000, rate = 70, rangemin = 0, rangemax = 800, target = 3, encountertime = 20000 },
}
