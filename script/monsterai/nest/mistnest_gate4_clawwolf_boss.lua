--AiClawwolf_Black_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 2000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Assault", rate = 30, loop = 1 },
}
g_Lua_Near4 = { 
 { action_name = "Walk_Front", rate = 6, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 30, loop = 1 },
}
g_Lua_Assault = { 
    { action_name = "Attack03_TripleSteb", rate = 40, loop = 1, approach = 250 },
}


g_Lua_Skill = { 
   { skill_index = 34638, cooltime = 1000, rate = 100, priority = 90, rangemin = 0, rangemax = 3000, target = 1, limitcount =1 }, -- 변신
   
   { skill_index = 33666,  cooltime = 8000, rate = 35, rangemin = 0, rangemax = 400, target = 3, priority = 10 }, -- 3단 찌르기
   { skill_index = 33667,  cooltime = 12000, rate = 35, rangemin = 0, rangemax = 500, target = 3, priority = 10 }, -- 콤보 
   
   { skill_index = 34634,  cooltime = 40000, rate = 45, rangemin = 600, rangemax = 1500, target = 3, selfhppercentrange = "0,49",usedskill = "34638", priority = 20, randomtarget = "0.2,0,1", next_lua_skill_index= 4 }, -- 어퍼충격파
   { skill_index = 34635,  cooltime = 1000, rate = -1 ,rangemin = 0, rangemax = 3000,randomtarget = "0.2,0,1", target = 3, next_lua_skill_index= 5 },
   { skill_index = 34637,  cooltime = 1000, rate = -1 ,rangemin = 0, rangemax = 3000,randomtarget = "0.2,0,1", target = 3 },  
   
   { skill_index = 34624, cooltime = 30000,  rate = 35, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "50,100", priority = 30 }, -- 호출 3페이즈 
   { skill_index = 34625, cooltime = 30000,  rate = 35, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0, 49",  priority = 30 }, -- 호출 4페이즈 
        
   { skill_index = 34641, cooltime = 40000, rate = 45,  rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "50,75", usedskill = "34638", randomtarget= "1.6,0,1", priority = 30, }, -- 광분 4페이즈 -패턴1
   
   { skill_index = 34642, cooltime = 40000, rate = 40 , rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "25,49", usedskill = "34638", randomtarget= "1.6,0,1", priority = 30}, -- 광분 4페이즈 -패턴2
   { skill_index = 34644, cooltime = 40000, rate = 60 , rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,24", usedskill = "34638", randomtarget= "1.6,0,1", priority = 30 }, -- 광분 4페이즈 -패턴4 
}
