-- /genmon 503702

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 8000

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 18, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 4, loop = 1 },    
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 3, loop = 1 },
   { action_name = "Assault", rate = 13, loop = 1 },
}
g_Lua_Near4 = 
{ 
    { action_name = "Stand", rate = 3, loop = 1  },
    { action_name = "Assault", rate = 15, loop = 1 },
	{ action_name = "Move_Front", rate = 3, loop = 1 },
}

g_Lua_Assault = 
{ 
   { lua_skill_index = 0, rate = 10, approach = 700 },
}

g_Lua_GlobalCoolTime1 = 31000 

g_Lua_Skill = {
    -- //next// -- 
	{ skill_index = 36097, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3,},----Attack03_JumpAttack2_Volcano
	{ skill_index = 36081, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3, },-- Attack01_Slash_Volcano
	{ skill_index = 36082, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3, },-- Attack03_JumpAttack2_Volcano
	-- // Normal // --
	{ skill_index = 36081, cooltime = 5000, rate = 40, rangemin = 0, rangemax = 300, target = 3, },-- Attack01_Slash_Volcano(1,2,3,4)
    { skill_index = 36082, cooltime = 17000, rate = 50, rangemin = 500, rangemax = 1200, target = 3, next_lua_skill_index = 2 },-- Attack03_JumpAttack2_Volcano
    { skill_index = 36083, cooltime = 25000, rate = 60, rangemin = 300, rangemax = 1000, target = 3, randomtarget = "1.5,0,1" },--Attack04_Wep_Volcano(1,2,3,4)
    { skill_index = 36084, cooltime = 25000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, },--Attack08_RollingAttack_Volcano
    { skill_index = 36098, cooltime = 30000, rate = 40, rangemin = 0, rangemax = 2000, target = 3,priority = 10,encountertime=10000 },--Attack02_Chop_VolNest
	{ skill_index = 36085, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3, },-- Attack10_DigGround_Start_VolNest
}


