--AiUnstable_Black_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name  = "Stand_1", rate = 14, loop = 1 },
   { action_name  = "Walk_Left", rate = 8, loop = 2 },
   { action_name  = "Walk_Right", rate = 8, loop = 2 },
   { action_name  = "Attack1_Kick", rate = 7, loop = 1, randomtarget=1.1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 2 },
   { action_name = "Walk_Left", rate = 3, loop = 2 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Move_Front", rate = 6, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 12, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Attack2_Teleport", rate = 7, loop = 1, randomtarget=1.1 },
   { action_name = "Assault", rate = 9, loop = 1 },
   { action_name = "Move_Teleport", rate = 4, loop = 1, randomtarget=1.1 },
   { action_name = "Attack3_Bite", rate = 5, loop = 1, randomtarget=1.1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Walk_Front", rate = 6, loop = 2 },
   { action_name = "Assault", rate = 7, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Kick", rate = 12, loop = 1, approach = 200, randomtarget=1.1 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 200 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 200 },
   { action_name = "Stand_1", rate = 4, loop = 1, approach = 200 },
}
g_Lua_Skill = { 
   --� ��
   { skill_index = 33135,  cooltime = 35000, rate = 50, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 20, limitcount = 1, randomtarget=1.1 },
   --��Ŭ ��
   --{ skill_index = 33137,  cooltime = 30000, rate = 10, rangemin = 0, rangemax = 500, target = 3 },
}

