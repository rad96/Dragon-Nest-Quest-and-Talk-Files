--/genmon 505221

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 20, loop = 1 },
	{ action_name = "Walk_Left", rate = 5, loop = 1, notusedskill = 301300 },
	{ action_name = "Walk_Right", rate = 5, loop = 1, notusedskill = 301300 },
	{ action_name = "Move_Back", rate = 5, loop = 1,notusedskill = 301300 },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 15, loop = 1 },	
	{ action_name = "Walk_Front", rate = 8, loop = 1, notusedskill = 301300 },
	{ action_name = "Walk_Left", rate = 6, loop = 1, notusedskill = 301300 },
	{ action_name = "Walk_Right", rate = 6, loop = 1, notusedskill = 301300 },
}
g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 15, loop = 1 },	
   	{ action_name = "Move_Front", rate = 13, loop = 1, notusedskill = 301300 },
    { action_name = "Walk_Left", rate = 3, loop = 1, notusedskill = 301300 },
	{ action_name = "Walk_Right", rate = 3, loop = 1, notusedskill = 301300 },
	{ action_name = "Assault", rate = 10, loop = 1, cooltime = 6000, notusedskill = 301300},
}
g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 8, loop = 1 },	
	{ action_name = "Move_Front", rate = 15, loop = 1,notusedskill = 301300 ,},
	{ action_name = "Assault", rate = 10, loop = 1, cooltime = 6000, notusedskill = 301300 },
}

g_Lua_GlobalCoolTime1 = 28000 
g_Lua_GlobalCoolTime2 = 40000 
g_Lua_BeHitSkill = { { lua_skill_index= 0, rate=100, skill_index=301234  },}
g_Lua_Assault = { { lua_skill_index = 5, rate = 10, approach = 700},} --Attack01_RedDNest_Bash
g_Lua_Skill = {
--TriggerOFF 301226 X
   { skill_index = 301232, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, },--Attack008_BigBash_MTnest_Start--0
   { skill_index = 301234, cooltime = 5000, rate = 100, Priority = 10, rangemin = 0, rangemax = 8000, target = 3, usedskill = 301300, notusedskill = 301226 },--Attack019_WaitPlayer_MTnest_Start--1
   { skill_index = 301233, cooltime = 15000, rate = 100, rangemin = 500, rangemax = 4000, target = 3, usedskill = 301226, usedskill = 301300,encountertime=10000 },--Attack010_ThrowAxe_MTnest_Start --7
---Next_Lua&Assault--
   { skill_index = 301230, cooltime = 65000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, next_lua_skill_index=4, notusedskill = 301300, randomtarget = "1.6,0,1", },--Attack007_PullStep_MTnest --2 메시지 필요함
   { skill_index = 301231, cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, },--Attack008_BigBash_MTnest --3
   { skill_index = 301228, cooltime = 8000, rate = -1, rangemin = 0, rangemax = 800, target = 3, notusedskill = 301300 },--Attack002_Chop_MTnest@ --4
--TriggerON  301226 O
   { skill_index = 301227, cooltime = 8000, rate = 60, rangemin = 0, rangemax = 450, target = 3, notusedskill = 301300 },--Attack001_Bash_MTnest@ --5
   -- { skill_index = 301229, cooltime = 25000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, notusedskill = 301300 ,selfhppercentrange = "50,100",globalcooltime=2, },--Attack003_BattleRush_MTnest@ --6
   { skill_index = 301236, cooltime = 30000, rate = 100, rangemin = 0, rangemax = 4000, target = 3, notusedskill = 301300 ,},--Attack015_CutGround_MTnest_Start@ --6
   { skill_index = 301233, cooltime = 45000, rate = 65, rangemin = 0, rangemax = 4000, target = 3, notusedskill = 301300, multipletarget = "1,4,0,1"},--Attack010_ThrowAxe_MTnest_Start --7
   { skill_index = 301225, cooltime = 20000, rate = 75, rangemin = 0, rangemax = 1000, target = 3, notusedskill = 301300,selfhppercent = 75,randomtarget = "1.6,0,1", },-- Attack017_ComboAttack_MTnest
---TriggerSkill--   
  { skill_index = 301226, cooltime = 5000, rate = -1, Priority = 20, rangemin = 0, rangemax = 8000, target = 3, },-- Attack006_HowlSelf_MTnest
}




