--/genmon 505231

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 20, loop = 1 },
	{ action_name = "Walk_Left", rate = 5, loop = 1, },
	{ action_name = "Walk_Right", rate = 5, loop = 1, },
	{ action_name = "Walk_Back", rate = 5, loop = 1, },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 15, loop = 1 },	
	{ action_name = "Walk_Front", rate = 8, loop = 1, },
	{ action_name = "Walk_Left", rate = 6, loop = 1, },
	{ action_name = "Walk_Right", rate = 6, loop = 1, },
}
g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 15, loop = 1 },	
   	{ action_name = "Walk_Front", rate = 13, loop = 1, },
    { action_name = "Walk_Left", rate = 3, loop = 1, },
	{ action_name = "Walk_Right", rate = 3, loop = 1, },
	-- { action_name = "Assault", rate = 10, loop = 1, cooltime = 10000, },
}
g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 8, loop = 1 },	
	{ action_name = "Move_Front", rate = 15, loop = 1, },
	{ action_name = "Assault", rate = 10, loop = 1, cooltime = 10000, },
}

g_Lua_Assault = { { lua_skill_index = 2, rate = 10, approach = 500},} --Attack01_RedDNest_Bash
g_Lua_GlobalCoolTime1 = 90000 
g_Lua_GlobalCoolTime2 = 50000 
-- g_Lua_BeHitSkill = { { lua_skill_index= 0, rate=100, skill_index=301234  },}
g_Lua_Skill = {
--------------------NextLua & Trigger-----------------------------
   { skill_index = 301261,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, priority=99,  },--Attack00_LimitSuccess_MTnest
   { skill_index = 301262,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, priority=100,  },--Attack16_Bowling_MTnest_Start
   { skill_index = 301253,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 500, target = 3,  },--Attack02_Shortblow_MTnest@
   { skill_index = 301871, cooltime = 5000, rate = 100, rangemin = 0, rangemax = 8000, target = 3,priority=200, usedskill=301873 },--Attack08_ReflexDMG_MTnest (301257)--1
--------------------Linepattern----------------------------------
   { skill_index = 301263,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority=100, selfhppercentrange = "0,75", limitcount = 1, globalcooltime = 1,globalcooltime2 = 2, next_lua_skill_index = 1 },--Attack06_WaitGainEnergyMantiNest_Start--75%Linepattern--0
   { skill_index = 301263,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority=101, selfhppercentrange = "0,50", limitcount = 1, globalcooltime = 1,globalcooltime2 = 2, next_lua_skill_index = 1 },--Attack06_WaitGainEnergyMantiNest_Start--75%Linepattern--1
   { skill_index = 301263,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority=102, selfhppercentrange = "0,25", limitcount = 1, globalcooltime = 1,globalcooltime2 = 2, next_lua_skill_index = 1 },--Attack06_WaitGainEnergyMantiNest_Start--25%Linepattern--2
--------------------Normal--------------------------------------
   { skill_index = 301252, cooltime = 8000, rate = 60, rangemin = 0, rangemax = 450, target = 3,  },--Attack01_LinearPunch_MTnest@ --5
   { skill_index = 301253, cooltime = 5000, rate = 50, rangemin = 0, rangemax = 300, target = 3,  },--Attack02_Shortblow_MTnest@ --5
   { skill_index = 301254, cooltime = 60000, rate = 100, rangemin = 500, rangemax = 4000, target = 3, randomtarget = "1.6,0,1", },--Attack03_ThrowBigStone_MTnest --7
   { skill_index = 301255, cooltime = 50000, rate = 100, rangemin = 200, rangemax = 2000, target = 3,  },--Attack04_RollingAttackBoss_MTnest_Start@ --6
   { skill_index = 301256, cooltime = 65000, rate = 70, rangemin = 0, rangemax = 1500, target = 3,selfhppercentrange = "0,75", },-- Attack05_MovingQuake_MTnest
   { skill_index = 301872, cooltime = 55000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1", },-- Attack19_8MJumpHC_MTnest_Start (301259)
   { skill_index = 301873, cooltime = 90000, rate = 100, rangemin = 0, rangemax = 5000, target = 1,priority=80, selfhppercentrange = "0,50",globalcooltime=2  },-- Attack13_ReflexReady_MTnest_Start
  -- { skill_index = 301251, cooltime = 5000, rate = -1, Priority = 20, rangemin = 0, rangemax = 8000, target = 3, limitcount = 1},-- Attack006_HowlSelf_MTnest
}
-- selfhppercentrange = "0,50",