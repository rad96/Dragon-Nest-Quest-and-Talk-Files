--/genmon 505241
g_Lua_NearTableCount = 4
g_Lua_NearValue1 = 500
g_Lua_NearValue2 = 1000
g_Lua_NearValue3 = 3000
g_Lua_NearValue4 = 10000

g_Lua_PatrolBaseTime = 2000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssualtTime = 3000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop=1,td = "FR,FL",custom_state1 = "custom_ground", },
   { action_name = "Stand_Fly", rate = 8, loop = 1,td = "FR,FL",custom_state1 = "custom_fly", },
   { action_name = "Walk_Left", rate = 4, loop = 1,td = "FL,FR,LF,RF", },
   { action_name = "Walk_Right", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 3, loop=1,td = "FR,FL",custom_state1 = "custom_ground",  },
   { action_name = "Stand_Fly", rate = 8, loop = 1,td = "FR,FL",custom_state1 = "custom_fly", },
   { action_name = "Walk_Front", rate = 8, loop=1,td = "FR,FL" },
   { action_name = "Walk_Left", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Walk_Right", rate = 4, loop = 1,td = "FL,FR,LF,RF" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 8, loop = 1,td = "FR,FL",custom_state1 = "custom_ground", },
   { action_name = "Stand_Fly", rate = 8, loop = 1,td = "FR,FL",custom_state1 = "custom_fly", },
   { action_name = "Move_Front", rate = 13, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1, loop=1,td = "FR,FL", custom_state1 = "custom_ground", },
   { action_name = "Stand_Fly", rate = 8, loop = 1,td = "FR,FL",custom_state1 = "custom_fly", },
   { action_name = "Move_Front", rate = 15, loop=1,td = "FR,FL" },
   { action_name = "Turn_Left", rate = 50, loop=-1,td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 50, loop=-1,td = "RF,RB,BR" },
}
g_Lua_GlobalCoolTime1 = 15000 
g_Lua_GlobalCoolTime2 = 20000 
g_Lua_GlobalCoolTime3 = 120000 
-- g_Lua_Assault =  {    { lua_skill_index = 3, rate = 20, approach = 300 },}
g_Lua_Skill = {
    ------------------------------------FLY---------------------------------------------------------------------
   -- FLY END --
   { skill_index = 301278,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_fly", },--Attack018_MTnest_FlyLandingHowl --0
   { skill_index = 301279,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_fly", },--Attack018_MTnest_FlyLong_Stomp --1
   --	FLY START  --
   { skill_index = 301276,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", td = "LF,FL,FR,RF,RB,BR,BL,LB",priority = 300 },--Attack015_MTnest_FlyStart --2
   -- FLY WAIT --
   { skill_index = 301277,  cooltime = 15000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_fly", next_lua_skill_index = 1 },--Attack017_MTnest_FlyThone --3
   { skill_index = 301280,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, custom_state1 = "custom_fly", next_lua_skill_index = 1,multipletarget = "1,4,0,1" },--Attack019_MTnest_FlyWandering --4
   { skill_index = 301281,  cooltime = 15000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_fly", next_lua_skill_index = 0 },--Attack020_MTnest_FlyHowl --5
   { skill_index = 301282,  cooltime = 15000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_fly", next_lua_skill_index = 0 },--Attack020_MTnqest_FlyHowlHowl--6
   ---NextLuaskill
   { skill_index = 301283,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", },--Attack021_MTnest_DashAttack --����
	----------------------------------------------	GROUND  ------------------------------------------------------------
   { skill_index = 301266,  cooltime = 10000, rate =40, rangemin = 0, rangemax = 600, target = 3, custom_state1 = "custom_ground", td = "FL,LF", },--Attack001_LHook_MTnest -- �º���
   { skill_index = 301267,  cooltime = 10000, rate =40, rangemin = 0, rangemax = 600, target = 3, custom_state1 = "custom_ground", td = "FR,RF", },--Attack002_RHook_MTnest -- �캣��
   { skill_index = 301268,  cooltime = 40000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, custom_state1 = "custom_ground", td = "LF,FL,FR,RF,RB,BR,BL,LB" },--Attack003_HeavyStamp_MTnest -- �������� ũ��
   { skill_index = 301271,  cooltime = 30000, rate = 60, rangemin = 600, rangemax = 1500, target = 3, custom_state1 = "custom_ground", td = "FR,FL", encountertime = 15000 },--Attack006_Thorn_MTnest -���� �̻���
   { skill_index = 301272,  cooltime = 50000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", td = "FR,FL,LF,RF", globalcooltime = 2},--Attack008_BlackGust_MTnest --ȸ���� �߻��� ǥȿ
   { skill_index = 301273,  cooltime = 30000, rate = 85, rangemin = 300, rangemax = 1500, target = 3, custom_state1 = "custom_ground", randomtarget = "0.6,0,1", td = "FR,FL,LF,RF", },--ttack009_JumpAttack_MTnest
   { skill_index = 301274,  cooltime = 45000, rate = 90, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", td = "FR,FL,LF,RF", globalcooltime = 2,next_lua_skill_index=7 },--Attack010_BlackCloud_MTnest 
   { skill_index = 301265,  cooltime = 90000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", td = "LF,FL,FR,RF,RB,BR,BL,LB" ,priority = 180, selfhppercentrange = "61,80", globalcooltime = 3, next_lua_skill_index= 2 }, --Attack011_PhantomHowl_MTnest
   { skill_index = 301265,  cooltime = 90000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", td = "LF,FL,FR,RF,RB,BR,BL,LB" ,priority = 200, selfhppercentrange = "41,60", globalcooltime = 3, next_lua_skill_index= 2 }, --Attack011_PhantomHowl_MTnest
   { skill_index = 301265,  cooltime = 90000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", td = "LF,FL,FR,RF,RB,BR,BL,LB" ,priority = 190, selfhppercentrange = "21,40", globalcooltime = 3, next_lua_skill_index= 2 }, --Attack011_PhantomHowl_MTnest
   { skill_index = 301275,  cooltime = 40000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", td = "LF,FL,FR,RF,RB,BR,BL,LB",selfhppercent = 80, },--Attack013_Spectrum_MTnest -- �������� ���¢�ٰ� �������� ���麸�� ���¢��
   { skill_index = 301283,  cooltime = 40000, rate = 80, rangemin = 0, rangemax = 2000, target = 3, custom_state1 = "custom_ground", td = "FL,LF" },--Attack021_MTnest_DashAttack --����
   { skill_index = 301284,  cooltime = 90000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, custom_state1 = "custom_ground", td = "FR,FL,LF,RF",priority = 100, selfhppercent = 20,  },--Attack022_BlackBreath_MTnest_Start --ȥ���ְ� �극��
   -----------------------------------------------------------------------Back--------------------------------------------------------------
   { skill_index = 301269,  cooltime = 20000, rate = 50, rangemin = 0, rangemax = 400, target = 3, custom_state1 = "custom_ground", td = "BL", randomtarget = "0.6,0,1", globalcooltime = 1 },--Attack004_RB_MTnest --�����ֵθ���
   { skill_index = 301270,  cooltime = 20000, rate = 50, rangemin = 0, rangemax = 400, target = 3, custom_state1 = "custom_ground", td = "BR", randomtarget = "0.6,0,1", globalcooltime = 1 },--Attack005_LB_MTnest -- ���� �ֵθ���
}