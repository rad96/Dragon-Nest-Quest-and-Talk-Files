--/genmon 505232

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 20, loop = 1 },
	{ action_name = "Walk_Left", rate = 5, loop = 1, },
	{ action_name = "Walk_Right", rate = 5, loop = 1, },
	{ action_name = "Walk_Back", rate = 5, loop = 1, },
}
g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 15, loop = 1 },	
	{ action_name = "Walk_Front", rate = 8, loop = 1, },
	{ action_name = "Walk_Left", rate = 6, loop = 1, },
	{ action_name = "Walk_Right", rate = 6, loop = 1, },
}
g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 15, loop = 1 },	
   	{ action_name = "Walk_Front", rate = 13, loop = 1, },
    { action_name = "Walk_Left", rate = 3, loop = 1, },
	{ action_name = "Walk_Right", rate = 3, loop = 1, },
	-- { action_name = "Assault", rate = 10, loop = 1, cooltime = 10000, },
}
g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 8, loop = 1 },	
	{ action_name = "Move_Front", rate = 15, loop = 1, },
	-- { action_name = "Assault", rate = 10, loop = 1, cooltime = 10000, },
}

g_Lua_Skill = {
  { skill_index = 301260, cooltime = 40000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, encountertime = 8000},-- Attack04_RollingAttack_MTnest_Start
  { skill_index = 301253, cooltime = 10000, rate = 100, rangemin = 0, rangemax = 300, target = 3,  },--Attack02_Shortblow_MTnest@ --5
}