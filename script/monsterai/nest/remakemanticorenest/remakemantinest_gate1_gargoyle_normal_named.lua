--/genmon 505201

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1500;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 3  },
   { action_name = "Walk_Right", rate = 2, loop = 3  },
   { action_name = "Walk_Back", rate = 8, loop = 3  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 8, loop = 2  },
   { action_name = "Attack1_DashAttack_N", rate = 5, loop = 1  },
   { action_name = "Attack3_Combo_N", rate = 14, loop = 1  },
   { action_name = "Attack2_SummonRock_Named", rate = 14, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Back", rate = 5, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 3  },
   { action_name = "Move_Left", rate = 7, loop = 2  },
   { action_name = "Move_Right", rate = 7, loop = 2  },
   { action_name = "Move_Front", rate = 14, loop = 2  },
   { action_name = "Move_Back", rate = 14, loop = 2  },
   { action_name = "Attack1_DashAttack", rate = 14, loop = 1  },
   { action_name = "Attack3_Combo", rate = 20, loop = 1  },
   { action_name = "Attack2_SummonRock_Named", rate = 20, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 3  },
   { action_name = "Walk_Right", rate = 5, loop = 3  },
   { action_name = "Walk_Back", rate = 5, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 3  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Attack1_DashAttack", rate = 30, loop = 2  },
   { action_name = "Attack3_Combo", rate = 30, loop = 1  },
}
g_Lua_BeHitSkill = {    { lua_skill_index=0, rate=100, skill_index=301205  },}
g_Lua_Skill = {
    --React--
   { skill_index = 301209, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 10000, target = 3,},--Attack02_Normal_ActiveBoss : 노멀가고일 보스활성화명령
   { skill_index = 301205, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 10000, target = 3,priority=100, usedskill=301204 },--Attack00_Normal_ComeN_MTnest_Start : 노멀가고일
   --Normal--
   { skill_index = 301210, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 10000, target = 1,selfhppercentrange = "0,5", },--Attack06_Normal_HpRegain_MTnest_Start_ : 노말가고일 체력회복
    --Trigger--
   { skill_index = 301204, cooltime = 600000, rate = -1, rangemin = 0, rangemax = 10000, target = 3,priority=99 },--Attack00_Normal_ComeNReady_Mtnest : 노멀가고일 
   --Passive--
   { skill_index = 301299, cooltime = 3000, rate = -1, rangemin = 0, rangemax = 10000, target = 1, },--하이랜더(Log)
}

