--AiLamia_Blue_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 1  },
   { action_name = "Walk_Right", rate = 8, loop = 1  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
   { action_name = "Attack001_Smash_Blue_MTnest", rate = 9, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack001_Smash_Blue_MTnest", rate = 9, loop = 1  },
   { action_name = "CustomAction1", rate = 18, loop = 1,cooltime= 12000  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
   { action_name = "Attack005_ChargeCut_Blue_MTnest", rate = 20, loop = 1  },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Move_Back", rate = 2, loop = 1  },
   { action_name = "Assault", rate = 27, loop = 1  },
}
g_Lua_CustomAction = { CustomAction1 = { { "Attack002_TailAttack_MTnest", 1 },},}
g_Lua_Assault = { { action_name = "Attack001_Smash_Blue_MTnest", rate = 10, loop = 1, approach = 300.0 },}


