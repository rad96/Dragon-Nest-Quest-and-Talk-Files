--/genmon 505202
g_Lua_NearTableCount = 4
g_Lua_NearValue1 = 300
g_Lua_NearValue2 = 700
g_Lua_NearValue3 = 2000
g_Lua_NearValue4 = 10000

g_Lua_PatrolBaseTime = 2000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Back", rate = 5, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 21 },
   { action_name = "Move_Left", rate = 4, loop = 1  },
   { action_name = "Move_Right", rate = 4, loop = 1  },
   { action_name = "Move_Front", rate = 14, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 25, loop = 2 },
}

g_Lua_GlobalCoolTime1 = 28000 
g_Lua_GlobalCoolTime2 = 45000 

g_Lua_Skill = {
   { skill_index = 301211,  cooltime = 50000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority=100, selfhppercentrange = "0,75", limitcount = 1, },--Attack06_WaitGainEnergyMantiNest_Start--75%Linepattern--0
   { skill_index = 301211,  cooltime = 50000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority=101, selfhppercentrange = "0,50", limitcount = 1,},--Attack06_WaitGainEnergyMantiNest_Start--75%Linepattern--1
   { skill_index = 301211,  cooltime = 50000, rate = 100, rangemin = 0, rangemax = 10000, target = 3, priority=102, selfhppercentrange = "0,25", limitcount = 1,},--Attack06_WaitGainEnergyMantiNest_Start--25%Linepattern--2
   { skill_index = 301212,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 10000, target = 3,  },--Attack02_Boss_AWAKE_Mtnest : 보스가고일 활성화 --AI에 의해서 발동--3
   { skill_index = 301213,  cooltime = 3000, rate = -1, rangemin = 0, rangemax = 10000, target = 3,  },--Attack02_Boss_AWAKEFail_Mtnest : 보스가고일 실패--못할시 발동--4
   ----------------------------------------------------------NormalPattern--------------------------------------------------------------------------------------
   { skill_index = 301220,  cooltime = 3000, rate = -1,rangemin = 0, rangemax = 1500, target = 3 },--Attack12_SpinAttack--5
   { skill_index = 301216,  cooltime = 20000, rate = 60, rangemin = 0, rangemax = 2000, target = 3, td = "FR,FL", randomtarget = "1.6,0,1" }, --Attack8_Laser
   { skill_index = 301217,  cooltime = 15000, rate = 50, rangemin = 500, rangemax = 1500, target = 3, globalcooltime = 1 },--Attack9_LaserLeft_LongRange
   { skill_index = 301218,  cooltime = 15000, rate = 50, rangemin = 0, rangemax = 700, target = 3, globalcooltime = 1 },--Attack9_LaserLeft_ShortRange
   { skill_index = 301219,  cooltime = 45000, rate = 60, rangemin = 0, rangemax = 500, target = 3, selfhppercentrange = "0,75", next_lua_skill_index = 5 },--Attack11_Punch
   { skill_index = 301224,  cooltime = 20000, rate = 30, rangemin = 0, rangemax = 500, target = 3,  td = "FR,FL,LF,RF" },--Attack4_Shake
   { skill_index = 301214,  cooltime = 45000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,50",multipletarget = "0,4,0,1"},--Attack07_ChaseUpon_MTnest
   { skill_index = 301222,  cooltime = 35000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "50,100", globalcooltime = 2 },--Attack1_DashAttack
   { skill_index = 301223,  cooltime = 35000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "0,50", globalcooltime = 2,randomtarget = "1.6,0,1" },--Attack3_Combo
}
