--AiGreatBeatle_Green_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 55000
g_Lua_GlobalCoolTime2 = 25000
g_Lua_GlobalCoolTime3 = 30000

g_Lua_CustomAction =
{
   CustomAction1 = 
     {
		  { "Fly", 0},
		  { "Attack06_PoisonAmpoll", 0 }, 
		  { "Attack06_PoisonAmpoll", 0 }, 
		  { "Attack06_PoisonAmpoll", 0 }, 
		  { "Fly", 0},
		  { "Attack08_Landing", 0},
     },
	 CustomAction2 = 
     {
          { "Fly", 0},
          { "Attack07_ThrowChainSaw", 0},
		  { "Fly", 0},
	      { "Attack08_Landing", 0},
     },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Left", rate = 3, loop = 1 , custom_state1 = "custom_ground"},
   { action_name = "Walk_Right", rate = 3, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Back", rate = -1, loop = 1, custom_state1 = "custom_ground" },
   -- { action_name = "CustomAction1", rate = 30, loop = 1, custom_state1 = "custom_fly" , globalcooltime = 2 , selfhppercentrange = "75,100" },
   -- { action_name = "CustomAction2", rate = 30, loop = 1, custom_state1 = "custom_fly" , globalcooltime = 3 , selfhppercentrange = "0,69" },
   { action_name = "CustomAction2", rate = 30, loop = 1, custom_state1 = "custom_fly" },
   { action_name = "Attack01_ChainSaw", rate = 19, loop = 1, custom_state1 = "custom_ground"  },
   -- { action_name = "Fly_Start", rate = 19, loop = 1 , globalcooltime = 1, custom_state1 = "custom_ground" , encountertime = 25000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Left", rate = 3, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Right", rate = 3, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Front", rate = 8, loop = 1, custom_state1 = "custom_ground" },
   -- { action_name = "CustomAction1", rate = 30, loop = 1, custom_state1 = "custom_fly" , globalcooltime = 2 , selfhppercentrange = "75,100" },
   -- { action_name = "CustomAction2", rate = 30, loop = 1, custom_state1 = "custom_fly" , globalcooltime = 3 , selfhppercentrange = "0,69" },
   { action_name = "CustomAction2", rate = 30, loop = 1, custom_state1 = "custom_fly" },
   -- { action_name = "Fly_Start", rate = 19, loop = 1 , globalcooltime = 1, custom_state1 = "custom_ground" , encountertime = 25000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Front", rate = 12, loop = 2, custom_state1 = "custom_ground" },
   { action_name = "Assault", rate = 19, loop = 1, custom_state1 = "custom_ground"  },
   -- { action_name = "Fly_Start", rate = 19, loop = 1 , globalcooltime = 1, custom_state1 = "custom_ground" , encountertime = 25000 },
   -- { action_name = "CustomAction1", rate = 30, loop = 1, custom_state1 = "custom_fly" , globalcooltime = 2 , selfhppercentrange = "75,100" },
   -- { action_name = "CustomAction2", rate = 30, loop = 1, custom_state1 = "custom_fly" , globalcooltime = 3 , selfhppercentrange = "0,69" },
   { action_name = "CustomAction2", rate = 30, loop = 1, custom_state1 = "custom_fly" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Front", rate = 12, loop = 2, custom_state1 = "custom_ground" },
   -- { action_name = "Fly_Start", rate = 19, loop = 1 , globalcooltime = 1, custom_state1 = "custom_ground", encountertime = 25000 },
   -- { action_name = "CustomAction1", rate = 30, loop = 1, custom_state1 = "custom_fly" , globalcooltime = 2  , selfhppercentrange = "75,100"},
   -- { action_name = "CustomAction2", rate = 30, loop = 1, custom_state1 = "custom_fly" , globalcooltime = 3 , selfhppercentrange = "0,69" },
   { action_name = "CustomAction2", rate = 30, loop = 1, custom_state1 = "custom_fly" },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 8, loop = 1, approach = 200, custom_state1 = "custom_ground" },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 200, custom_state1 = "custom_ground" },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 200, custom_state1 = "custom_ground" },
   { action_name = "Attack01_ChainSaw", rate = 12, loop = 1, approach = 200 , custom_state1 = "custom_ground" },
}
g_Lua_Skill = {
   -- 방전
   -- { skill_index = 33188,  cooltime = 35000, rate = -1, rangemin = 0, rangemax = 4000, target = 3 , randomtarget=1.1, custom_state1 = "custom_ground"  },
   { skill_index = 33232,  cooltime = 35000, rate = -1, rangemin = 0, rangemax = 4000, target = 3 , randomtarget=1.1, custom_state1 = "custom_ground"  },
   -- 돌진
   { skill_index = 33185,  cooltime = 18000, rate = 100, rangemin = 100, rangemax = 1000, target = 3, randomtarget=1.1, custom_state1 = "custom_ground" },
   -- 피뢰침
   { skill_index = 33186,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, randomtarget=1.1, custom_state1 = "custom_ground" , selfhppercentrange = "75,100" },
   { skill_index = 33231,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 700, target = 3, randomtarget=1.1, custom_state1 = "custom_ground" , selfhppercentrange = "0,70" },
   -- 독액발사
   { skill_index = 33187,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 700, target = 3 , selfhppercentrange = "35,70" , randomtarget=1.1, custom_state1 = "custom_ground" },
   -- 밀어내기
   { skill_index = 33189,  cooltime = 40000, rate = 100, rangemin = 0, rangemax = 4000, target = 3 , randomtarget=1.1, encountertime = 25000, next_lua_skill_index = 0 , selfhppercentrange = "35,70", custom_state1 = "custom_ground" },
   -- 상승 : 1페이즈
   { skill_index = 33193,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 5000, target = 3 , randomtarget=1.1, selfhppercentrange = "60,98", custom_state1 = "custom_ground", limitcount = 1},
   -- 상승 : 3페이즈
   { skill_index = 33193,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 5000, target = 3 , randomtarget=1.1, selfhppercentrange = "0,57", custom_state1 = "custom_ground", limitcount = 1},
}
