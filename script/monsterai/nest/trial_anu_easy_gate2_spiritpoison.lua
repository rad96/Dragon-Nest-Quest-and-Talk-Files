--AiDeathKnight_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssaultTime = 2500

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1, selfhppercentrange = "30,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,29" },
   { action_name = "Attack12_Hammer_Boss", rate = 6, loop = 1 },
   { action_name = "Attack13_Combo", rate = 10, loop = 1, cooltime = 20000 },
   { action_name = "Walk_Left", rate = 6, loop = 2 },
   { action_name = "Walk_Right", rate = 6, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1, selfhppercentrange = "30,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,29" },
   { action_name = "Attack13_Combo", rate = 10, loop = 1, cooltime = 20000 },
   { action_name = "Walk_Left", rate = 6, loop = 2 },
   { action_name = "Walk_Right", rate = 6, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 1 },
   { action_name = "Move_Right", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 2 },
   { action_name = "Assault", rate = 8, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack12_Hammer_Boss", rate = 8, loop = 1, approach = 450 },
}

g_Lua_SkillProcessor = {
   { skill_index = 34025, changetarget = "1000,0" },
}

g_Lua_GlobalCoolTime1 = 5000
g_Lua_GlobalCoolTime2 = 12000
g_Lua_GlobalCoolTime3 = 999990000
g_Lua_GlobalCoolTime4 = 999990000
g_Lua_GlobalCoolTime5 = 999990000

g_Lua_Skill = { 
   -- 한줄패턴 독바닥
   { skill_index = 34173, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
   { skill_index = 34032, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 2, globalcooltime3 = 3,  rate = 65,  priority = 50, rangemin = 0, rangemax = 5000, target = 3,selfhppercentrange = "51,73", limitcount = 1},  
   { skill_index = 34174, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 2, globalcooltime3 = 3,  rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.5,0,1", selfhppercentrange = "51,73", limitcount = 1, next_lua_skill_index = 0 }, -- 독고리
   { skill_index = 34032, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 2, globalcooltime3 = 4,  rate = 75, priority = 50, rangemin = 0, rangemax = 5000, target = 3,selfhppercentrange = "26,47", limitcount = 1}, 
   { skill_index = 34174, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 2, globalcooltime3 = 4,  rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.5,0,1", selfhppercentrange = "26,48", limitcount = 1, next_lua_skill_index = 0 }, -- 독고리
   { skill_index = 34032, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 2, globalcooltime3 = 5,  rate = 80, priority = 50, rangemin = 0, rangemax = 5000, target = 3,selfhppercentrange = "0,23", limitcount = 1}, 
   { skill_index = 34174, cooltime = 1000, globalcooltime = 1, globalcooltime2 = 2, globalcooltime3 = 5,  rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.5,0,1", selfhppercentrange = "0,23", limitcount = 1, next_lua_skill_index = 0 }, -- 독고리
   -- 페이즈구분
   { skill_index = 34028, cooltime = 1000, rate = 100, priority = 80, rangemin = 0, rangemax = 5000, target = 1, selfhppercent = 75, limitcount = 1 }, 
   { skill_index = 34029, cooltime = 1000, rate = 100, priority = 80, rangemin = 0, rangemax = 5000, target = 1, selfhppercent = 50, limitcount = 1 }, 
   { skill_index = 34030, cooltime = 1000, rate = 100, priority = 80, rangemin = 0, rangemax = 5000, target = 1, selfhppercent = 25, limitcount = 1 }, 
   -- 독 장판 시그널
   { skill_index = 34031, cooltime = 50000, globalcooltime = 1, rate = 100, priority = 40, rangemin = 0, rangemax = 5000, target = 1, encountertime = 20000 }, 
   -- 써클
   { skill_index = 34021, cooltime = 110000, globalcooltime = 2, rate = 100, priority = 35, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "0.5,0,1",selfhppercentrange = "0,49" }, 
   -- 독구슬(엔트소환)
   { skill_index = 34175, cooltime = 80000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 3000, target = 3, encountertime = 15000, selfhppercentrange = "50,100" }, 
   -- 구토
   { skill_index = 34023, cooltime = 70000, rate = 80, priority = 15, rangemin = 0, rangemax = 1200, target = 3 }, 
   -- 돌진
   { skill_index = 34171, cooltime = 100000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1" },
   -- 밀쳐내기
   { skill_index = 34025, cooltime = 80000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1", encountertime = 15000, selfhppercent = 50 },
}
   