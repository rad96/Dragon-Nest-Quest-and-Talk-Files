--/genmon 503725


g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 10000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_MeleeDefense = { 
   { action_name = "Defense", rate = 12, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 36340,  cooltime = 6000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, multipletarget = "1,2,0,1", },
   { skill_index = 36341,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, multipletarget = "1,4,0,1", },
   { skill_index = 36342,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3,},
}

