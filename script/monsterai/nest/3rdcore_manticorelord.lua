g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 700.0;
g_Lua_NearValue2 = 1200.0;
g_Lua_NearValue3 = 1700.0;
g_Lua_NearValue4 = 2200.0;
g_Lua_NearValue5 = 3200.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 500;
g_Lua_AssualtTime = 3000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}


g_Lua_CustomAction =
{
	CustomAction1 = 
	{
      			{ "Attack11_LHook_Lord_NestBossRush", 0},
      			{ "Walk_Back", 0},
			{ "Attack11_LHook_Lord_NestBossRush", 0},  		
	},
	CustomAction2 = 
	{
      			{ "Attack11_RHook_Lord", 0},
      			{ "Walk_Back", 0},
			{ "Attack06_GravityThorn", 0}, 
	},
}
g_Lua_Near1 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1, custom_state1 = "custom_ground", td = "FL,RF" },

	{ action_name = "Walk_Left", rate = 8, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Right", rate = 8, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Back", rate = 2, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF", cooltime = 5000 },

	{ action_name = "Turn_Left", rate = 15, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 15, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },

	{ action_name = "Attack11_LHook_Lord_NestBossRush", rate = 15, loop = 1, custom_state1 = "custom_ground", td = "FL,LF" },
	{ action_name = "Attack11_RHook_Lord_NestBossRush", rate = 15, loop = 1, custom_state1 = "custom_ground", td = "FR,RF" },

	{ action_name = "Attack04_Tail_L_Lord", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "BL", cooltime = 7000, randomtarget = "0.6,0,1" },
	{ action_name = "Attack05_Tail_R_Lord", rate = 20, loop = 1, custom_state1 = "custom_ground", td = "BR", cooltime = 7000, randomtarget = "0.6,0,1" },
	{ action_name = "Attack07_HeavySwoop_NestBossRush", rate = 15, loop = 1, custom_state1 = "custom_fly" },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1, custom_state1 = "custom_ground", td = "FL,FR" },

	{ action_name = "Walk_Front", rate = 8, loop = 1, custom_state1 = "custom_ground", td = "FL,FR" },
	{ action_name = "Walk_Left", rate = 2, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Walk_Right", rate = 2, loop = 1, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },

	{ action_name = "Turn_Left", rate = 15, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 15, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },

	{ action_name = "Attack07_HeavySwoop_NestBossRush", rate = 15, loop = 1, custom_state1 = "custom_fly" },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1, custom_state1 = "custom_ground", td = "FL,FR" },
	{ action_name = "Move_Front", rate = 8, loop = 2, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Turn_Left", rate = 15, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 15, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
	{ action_name = "Attack06_GravityThorn", rate = 4, loop = 1, custom_state1 = "custom_ground", td = "FL,FR", randomtarget = "1.6,0,1", cooltime = 7000 },
	{ action_name = "Attack07_HeavySwoop_NestBossRush", rate = 15, loop = 1, custom_state1 = "custom_fly" },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1, custom_state1 = "custom_ground", td = "FL,FR" },
	{ action_name = "Move_Front", rate = 12, loop = 3, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Turn_Left", rate = 15, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 15, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
	{ action_name = "Attack07_HeavySwoop_NestBossRush", rate = 15, loop = 1, custom_state1 = "custom_fly" },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand", rate = 4, loop = 1, custom_state1 = "custom_ground", td = "FL,FR" },
	{ action_name = "Move_Front", rate = 12, loop = 5, custom_state1 = "custom_ground", td = "FL,FR,LF,RF" },
	{ action_name = "Turn_Left", rate = 15, loop = -1, custom_state1 = "custom_ground", td = "LF,LB,BL" },
	{ action_name = "Turn_Right", rate = 15, loop = -1, custom_state1 = "custom_ground", td = "RF,RB,BR" },
	{ action_name = "Attack07_HeavySwoop_NestBossRush", rate = 15, loop = 1, custom_state1 = "custom_fly" },
}

g_Lua_GlobalCoolTime1 = 45000
g_Lua_GlobalCoolTime2 = 20000

g_Lua_Skill = { 
-- 블랙홀 Attack02_BlackHole
   { skill_index = 21324, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
-- 지상 회오리 Attack15_Gust_Hard
   { skill_index = 21328, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },


-- 공중 스톰프 Attack07_HeavySwoop_NestBossRush
   { skill_index = 21332, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 8000, target = 3, custom_state1 = "custom_fly" },
-- 에어 그라비티볼 Attack12_AirGravityBall
   { skill_index = 21330, cooltime = 20000, globalcooltime = 2, rate = 100, rangemin = 0, rangemax = 8000, target = 3, custom_state1 = "custom_fly", selfhppercentrange = "40,100", multipletarget = "1,4,0,1" },
-- 공중 회오리 Attack16_AirGust
   { skill_index = 21331, cooltime = 20000, globalcooltime = 2, rate = 100, rangemin = 0, rangemax = 8000, target = 3, custom_state1 = "custom_fly", selfhppercentrange = "0,39", multipletarget = "1,4,0,1" },
-- 비상1 Fly_Start
   { skill_index = 21329, cooltime = 45000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, custom_state1 = "custom_ground", selfhppercent = 60 },

-- 헤비점프 Attack09_HeavyJump_NestBossRush
   { skill_index = 21333, cooltime = 15000, rate = 80, rangemin = 1000, rangemax = 1500, target = 3, custom_state1 = "custom_ground", td = "FL,FR", randomtarget = "1.6,0,1" },

-- 빅 그라비티볼 Attack01_BigGravityBall 150
   { skill_index = 21323, cooltime = 15000, rate = 70, rangemin = 700, rangemax = 1000, target = 3, custom_state1 = "custom_ground", td = "FL,FR" },
-- 레이즈그라비티1 Attack14_RaiseGravity1
   { skill_index = 21326, cooltime = 45000, globalcooltime = 1, rate = 80, rangemin = 0, rangemax = 1200, target = 3, custom_state1 = "custom_ground", td = "FL,FR,RB,LB", encountertime = 45000 },
-- 레이즈그라비티2 Attack14_RaiseGravity2
   { skill_index = 21327, cooltime = 45000, globalcooltime = 1, rate = 80, rangemin = 0, rangemax = 1200, target = 3, custom_state1 = "custom_ground", td = "BL,BR,RF,LF", encountertime = 45000 },

-- 충격파 Attack03_HeavyStamp
   { skill_index = 21322, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 5500, target = 3, custom_state1 = "custom_ground", selfhppercent = 80 },
-- 블랙홀 Attack02_BlackHole
   { skill_index = 21334, cooltime = 35000, rate = 70, rangemin = 0, rangemax = 3000, target = 3, custom_state1 = "custom_ground", selfhppercent = 80, next_lua_skill_index = 0 },

-- 스펙트럼 Attack13_Spectrum 150
   { skill_index = 21325, cooltime = 40000, rate = 80, rangemin = 800, rangemax = 2000, target = 3, randomtarget = "0.6,0,1", custom_state1 = "custom_ground", selfhppercent = 40 },
-- 지상 회오리 Attack15_Gust_Hard
   { skill_index = 21335, cooltime = 35000, rate = 70, rangemin = 0, rangemax = 2000, target = 3, custom_state1 = "custom_ground", selfhppercent = 40, next_lua_skill_index = 1 },
}
