--/genmon 503763
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000
g_Lua_NearValue2 = 8000


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 16, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 36206,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 2, },--Attack_VolNEST_4rd_MVFireWall_03_1
   { skill_index = 36207,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 2, },--Attack_VolNEST_4rd_MVFireWall_03_2
   { skill_index = 36208,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 2, },--Attack_VolNEST_4rd_MVFireWall_03_3
   { skill_index = 36209,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 2, },--Attack_VolNEST_4rd_MVFireWall_03_4
   { skill_index = 36210,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 8000, target = 2, },--Attack_VolNEST_4rd_MVFireWall_03_5
}


