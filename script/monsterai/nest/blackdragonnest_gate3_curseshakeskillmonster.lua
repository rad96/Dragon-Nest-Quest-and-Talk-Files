--/genmon 503652

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 750;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1650;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 4, loop = 1, },
}
g_Lua_Near2 =
{ 
   { action_name = "Stand", rate = 4, loop = 1, },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 4, loop = 1, },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand", rate = 4, loop = 1, },
}

g_Lua_Skill = {
   { skill_index = 34763, cooltime =1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },
   { skill_index = 34764, cooltime =1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },
}