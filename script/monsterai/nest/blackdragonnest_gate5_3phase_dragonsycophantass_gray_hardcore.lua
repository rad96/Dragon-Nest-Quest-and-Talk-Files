--AiDragonSycophantAss_Gray_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Attack6_HighKick_BlackDragon", rate = 20, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Attack6_HighKick_BlackDragon", rate = 15, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Attack4_ThrowDagger_BlackDragon", rate = 15, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 34976, cooltime = 15000, rate = 100, rangemin = 0, rangemax = 5000, target = 2, encountertime = 15000 },
}
