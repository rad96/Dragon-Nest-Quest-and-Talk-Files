g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 750;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1650;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 5000

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 3 },
   { action_name = "Assault", rate = 10, loop = 1 },
}

g_Lua_Assault = 
{ 
   { lua_skill_index = 1, rate = 10, approach = 300 },
}

g_Lua_Skill = {
--Attack000_��������     -- Passive --
   { skill_index = 34765, cooltime =1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, notusedskill="34765", }, 
--Attack001_ChopBD.lua(Assault)
   { skill_index = 34766, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },
--Attack004_AssaultBD_Start
   { skill_index = 34770, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },
--Attack005_WhirlWindBD_Start
   { skill_index = 34771, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },
--Attack005_WhirlWindBD_Start
   { skill_index = 34788, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },
--Attack011_MegaBombChopBD(6.5sec)
   { skill_index = 34778, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3 },

--Attack006_NandongBD_Start(8.5sec)
   { skill_index = 34772, cooltime = 25000, rate = 60, rangemin = 0, rangemax = 1200, target = 3, selfhppercent = 96 },

--Attack001_ChopBD.lua
   { skill_index = 34766, cooltime = 8000, rate = 60, rangemin = 0, rangemax = 400, target = 3, encountertime = 5000 },
--Attack003_JumpChopBD
   { skill_index = 34769, cooltime = 8000, rate = 80, rangemin = 800, rangemax = 1200, target = 3, encountertime = 5000 },
--Attack015_ComboBD
   { skill_index = 34782, cooltime = 25000, rate = 60, rangemin = 0, rangemax = 1200, target = 3, encountertime = 5000 },
--Attack012_ComeOnBD
   { skill_index = 34779, cooltime = 20000, rate = 70, rangemin = 0, rangemax = 1200, target = 3, next_lua_skill_index = 2 },
}