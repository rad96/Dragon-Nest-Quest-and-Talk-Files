--AiEnt_Gray_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 350;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1400;
g_Lua_NearValue4 = 1900;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 350
g_Lua_AssaultTime = 6000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 1, loop= 1 },
   { action_name = "Attack01_RightHand_MistNest", rate = 19, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 1, loop= 1 },
   { action_name = "Move_Front", rate = 5, loop= 1 },
   { action_name = "Assault", rate = 22, loop= 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop=1 },
   { action_name = "Move_Front", rate = 5, loop= 2 },
   { action_name = "Assault", rate = 22, loop=1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 1,  },
   { action_name = "Move_Front", rate = 2, loop= 3 },
   { action_name = "Assault", rate = 22, loop= 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack01_RightHand_MistNest", rate = 13, approach = 350 },
}

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Skill = { 
   -- 돌진(보호막 생성 지역)
   { skill_index = 34651, cooltime = 20000, rate = 100, priority = 80, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1" },
   -- 붉은 열매 뿌리기(34662 : 정해진위치, 34663 : 유저 위치로)
   { skill_index = 34662,  cooltime = 20000, globalcooltime = 1, rate = 80, rangemin = 0, rangemax = 5000, target = 3, encountertime = 10000, usedskill = "34651" }, 
   { skill_index = 34663,  cooltime = 20000, globalcooltime = 1, rate = 90, rangemin = 0, rangemax = 5000, target = 3, encountertime = 10000, usedskill = "34651" }, 
   { skill_index = 34664,  cooltime = 15000, rate = 90, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1", encountertime = 10000, usedskill = "34651" }, 
   -- 박쥐
   { skill_index = 34667,  cooltime = 35000, rate = 70, rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6,0,1", encountertime = 10000 },
   -- 세계수 보스용 수그리기
   { skill_index = 34674,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 1 },
}
