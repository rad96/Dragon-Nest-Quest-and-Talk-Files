g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 1400;
g_Lua_NearValue2 = 2000;
g_Lua_NearValue3 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

-- (1)발사 (2)Move_Front
g_Lua_GlobalCoolTime1 = 80000
g_Lua_GlobalCoolTime2 = 8000

-- g_Lua_OnlyPartsDamage = 2

-- 181 : 머리 182 : 몸통
g_Lua_PartsProcessor = {
   { hp = "0,100", ignore="182", nodamage="182" },
   { hp = "0,100", checkblow ="274", active="182" },
--   { hp = "0,100", checkblow ="2", ignore="181", nodamage="181" },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 8, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 8, loop = 1 },
}

g_Lua_Skill = { 
-- 콤보, 처음 시작할 때 무조건 사용
   { skill_index = 32056, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, limitcount = 1 },
-- 기본 이동 +300
   { skill_index = 32067, cooltime = 8000, globalcooltime = 2, rate = 80, rangemin = 1800, rangemax = 2250, target = 3, td = "FL,FR", encountertime = 10000 },
   { skill_index = 32075, cooltime = 8000, globalcooltime = 2, rate = 80, rangemin = 2250, rangemax = 2600, target = 3, td = "FL,FR", encountertime = 10000 },
   { skill_index = 32076, cooltime = 8000, globalcooltime = 2, rate = 80, rangemin = 2600, rangemax = 2950, target = 3, td = "FL,FR", encountertime = 10000 },
   { skill_index = 32077, cooltime = 8000, globalcooltime = 2, rate = 80, rangemin = 2950, rangemax = 5300, target = 3, td = "FL,FR", encountertime = 10000 },
-- 롤링
   { skill_index = 32069, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 5000, target = 1, selfhppercent = 24, next_lua_skill_index = 6 },
   { skill_index = 32063, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },

-- 발사 왼쪽,오른쪽(200f.4)
   { skill_index = 32057, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3},
   { skill_index = 32079, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 1, next_lua_skill_index = 7 },
   { skill_index = 32074, cooltime = 80000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 8, priority = 40, encountertime = 20000, selfhppercentrange = "75,100" },
   { skill_index = 32074, cooltime = 80000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 8, priority = 40, selfhppercentrange = "50,75" },
   { skill_index = 32074, cooltime = 80000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 8, priority = 40, selfhppercentrange = "25,50" },
   { skill_index = 32074, cooltime = 80000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, next_lua_skill_index = 8, priority = 40, selfhppercentrange = "1,25" },

-- 슬래쉬(160f.3)
   { skill_index = 32055, cooltime = 10000, rate = 70, rangemin = 0, rangemax = 1400, target = 3, td = "FL,FR", encountertime = 10000 },
-- 콤보(675f.12)
   { skill_index = 32056, cooltime = 20000, rate = 60, rangemin = 0, rangemax = 1400, target = 3, td = "FL,FR", encountertime = 10000 },
-- 하울(280f.4)
   { skill_index = 32059, cooltime = 50000, rate = 20, rangemin = 0, rangemax = 5000, target = 3, encountertime = 20000, encountertime = 10000 },

-- 지옥
   { skill_index = 32051, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 50, selfhppercent = 75, limitcount = 1, next_lua_skill_index = 18 },
   { skill_index = 32051, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 50, selfhppercent = 50, limitcount = 1, next_lua_skill_index = 18 },
   { skill_index = 32051, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, priority = 50, selfhppercent = 25, limitcount = 1, next_lua_skill_index = 18 },
   { skill_index = 32070, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 1 },

-- 회전(325f.6)
   { skill_index = 32062, cooltime = 30000, rate = 80, rangemin = 1000, rangemax = 2000, target = 3, selfhppercent = 74 },
-- 스톰프(650f.11)
   { skill_index = 32060, cooltime = 50000, rate = 50, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 49 },

-- 머리체력 회복
   { skill_index = 32071, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 1, usedskill = 32073, priority = 60 },

-- 기절(32066:부파기절)(32073:기절회복)
-- { skill_index = 32066, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 1, destroyparts = "181", priority = 60 },
-- { skill_index = 32073, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 1 },
}
