--AiBroo_Blue_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1600;
g_Lua_NearValue3 = 2200;
g_Lua_NearValue4 = 2800;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1, selfhppercentrange = "60,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Move_Back", rate = 4, loop = 1 },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 4, loop = 1, selfhppercentrange = "60,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,60" },
   { action_name = "Move_Front", rate = 8, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 2, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Right", rate = 2, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 16, loop = 1, td = "FL,FR"  },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 16, loop = 1, td = "FL,FR"  },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1, td = "FL,FR" },
   { action_name = "Assault", rate = 16, loop = 1, td = "FL,FR"  },
   { action_name = "Turn_Left", rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
}

g_Lua_Assault = { 
   { action_name = "Attack01_Bash", rate = 8, loop = 1, approach = 500 },
}

-- 심판
g_Lua_GlobalCoolTime1 = 20000
-- 대쉬
g_Lua_GlobalCoolTime2 = 27000
-- 분노
g_Lua_GlobalCoolTime3 = 50000
-- 슬래쉬 계열
g_Lua_GlobalCoolTime4 = 10000
-- Twister
g_Lua_GlobalCoolTime5 = 36000

g_Lua_Skill = { 
-- NextLuaSkill(31437:Spear)(34142:Finish)
   { skill_index = 34137, cooltime = 1000, rate = -1, rangemin = 800, rangemax = 1500, target = 3, td = "FR,FL,LF,LB,RF,RB", randomtarget = "1.6,0,1" },
   { skill_index = 34142, cooltime = 1000, globalcooltime = 1, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, usedskill = 34155 },
-- 34151(step2)2/3/4/ 34152(step3)5/6/ 34153(step4)7/
   { skill_index = 34151, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
   { skill_index = 34151, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, combo1 = "5,100,0" },
   { skill_index = 34151, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, combo1 = "6,100,0" },
   { skill_index = 34152, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
   { skill_index = 34152, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, combo1 = "7,100,0" },
   { skill_index = 34153, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
-- Combo(114/65/80/250) (3sec/4sec/5.5sec/8.5sec)
   { skill_index = 34150, cooltime = 7000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, td = "FL,FR", selfhppercentrange = "80,100" },
   { skill_index = 34150, cooltime = 8000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, combo1 = "2,100,0", td = "FL,FR", selfhppercentrange = "60,79" },
   { skill_index = 34150, cooltime = 9500, rate = 70, rangemin = 0, rangemax = 1000, target = 3, combo1 = "3,100,0", td = "FL,FR", selfhppercentrange = "40,59" },
   { skill_index = 34150, cooltime = 12500, rate = 70, rangemin = 0, rangemax = 1000, target = 3, combo1 = "4,100,0", td = "FL,FR", selfhppercent = 39 },
-- Bash
   { skill_index = 34154, cooltime = 9000, rate = 70, rangemin = 0, rangemax = 600, target = 3, td = "FR,FL", encountertime = 6000 },
-- Slash
   { skill_index = 34133, cooltime = 15000, globalcooltime = 4, rate = 70, rangemin = 600, rangemax = 2000, target = 3, td = "FR,FL", encountertime = 6000 },
-- Left
   { skill_index = 34134, cooltime = 25000, globalcooltime = 4, rate = 70, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "0.6,0,1", td = "LF,LB", encountertime = 6000 },
-- Right
   { skill_index = 34135, cooltime = 25000, globalcooltime = 4, rate = 70, rangemin = 0, rangemax = 2000, target = 3, randomtarget = "0.6,0,1", td = "RF,RB", encountertime = 6000 },
-- Tail
   { skill_index = 34136, cooltime = 20000, rate = 70, rangemin = 0, rangemax = 700, target = 3, randomtarget = "0.6,0,1", td = "BR,BL", encountertime = 6000 },
-- Spear
   { skill_index = 34141, cooltime = 22000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, next_lua_skill_index = 0, encountertime = 6000 },
-- Twister
   { skill_index = 34138, cooltime = 36000, globalcooltime = 5, rate = 60, rangemin = 0, rangemax = 600, target = 3, encountertime = 12000 },
   { skill_index = 34138, cooltime = 36000, globalcooltime = 5, rate = 100, rangemin = 0, rangemax = 5000, target = 3, encountertime = 6000, limitcount = 1 },
-- Dash
   { skill_index = 34120, cooltime = 27000, globalcooltime = 2, rate = 60, rangemin = 0, rangemax = 1000, target = 3, td = "FR,FL,LF,LB,RF,RB", randomtarget = "1.6,0,1", encountertime = 6000 },
   { skill_index = 34139, cooltime = 27000, globalcooltime = 2, rate = 60, rangemin = 1000, rangemax = 2000, target = 3, td = "FR,FL,LF,LB,RF,RB", randomtarget = "1.6,0,1", encountertime = 6000 },
   { skill_index = 34140, cooltime = 27000, globalcooltime = 2, rate = 60, rangemin = 2000, rangemax = 3000, target = 3, td = "FR,FL,LF,LB,RF,RB", randomtarget = "1.6,0,1", encountertime = 6000 },
-- Forest
   { skill_index = 34121, cooltime = 60000, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 38 },
-- Rage
   { skill_index = 34122, cooltime = 50000, globalcooltime = 3, rate = 60, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80 },
   { skill_index = 34123, cooltime = 50000, globalcooltime = 3, rate = 60, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80 },
   { skill_index = 34124, cooltime = 50000, globalcooltime = 3, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80 },
   { skill_index = 34125, cooltime = 50000, globalcooltime = 3, rate = 70, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80 },
   { skill_index = 34126, cooltime = 50000, globalcooltime = 3, rate = 50, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80 },
   { skill_index = 34127, cooltime = 50000, globalcooltime = 3, rate = 50, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 80 },
-- Judgment Start
   { skill_index = 34155, cooltime = 1000, globalcooltime = 1, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 60, limitcount = 1 },
   { skill_index = 34155, cooltime = 1000, globalcooltime = 1, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 40, limitcount = 1 },
   { skill_index = 34155, cooltime = 1000, globalcooltime = 1, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 20, limitcount = 1 },
-- Judgment Front
   { skill_index = 34128, cooltime = 1000, rate = 100, priority = 10, rangemin = 0, rangemax = 5000, target = 3, td = "FR,FL", usedskill = 34155, randomtarget = "1.6,0,1" },
-- Judgment Back
   { skill_index = 34129, cooltime = 1000, rate = 100, priority = 10, rangemin = 0, rangemax = 5000, target = 3, td = "BR,BL", usedskill = 34155, randomtarget = "1.6,0,1" },
-- Judgment Left
   { skill_index = 34130, cooltime = 1000, rate = 100, priority = 10, rangemin = 0, rangemax = 5000, target = 3, td = "LF,LB", usedskill = 34155, randomtarget = "1.6,0,1" },
-- Judgment Right
   { skill_index = 34131, cooltime = 1000, rate = 100, priority = 10, rangemin = 0, rangemax = 5000, target = 3, td = "RF,RB", usedskill = 34155, randomtarget = "1.6,0,1" },
}
