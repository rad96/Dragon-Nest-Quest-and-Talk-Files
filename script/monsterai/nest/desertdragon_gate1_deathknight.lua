--AiDeathKnight_Red_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 20000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Attack1_Combo_DesertDragon", rate = 19, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 13, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 8, loop = 1, approach = 300 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 300 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 300 },
   { action_name = "Attack1_Combo", rate = 16, loop = 1, approach = 300 },
}
g_Lua_Skill = { 
-- 마력엔진 파괴
   { skill_index = 31909,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1, priority = 10 },
-- 리자드맨 소환(자폭병)
   { skill_index = 31921,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1, priority = 10, selfhppercent = 75, globalcooltime = 1 },
-- 부하 소환
   { skill_index = 31922,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1, selfhppercent = 75, priority = 10, globalcooltime = 1 },
   { skill_index = 31922,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1, selfhppercent = 50, priority = 10, globalcooltime = 1 },
   { skill_index = 31922,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, limitcount = 1, selfhppercent = 25, priority = 10, globalcooltime = 1 },
-- 휠타이푼
   { skill_index = 31902,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", selfhppercent = 24, priority = 8, globalcooltime = 1 },
-- 지진
   { skill_index = 31901,  cooltime = 40000, rate = 60, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6", globalcooltime = 1 },
-- 사이클론 슬래쉬
   { skill_index = 31915,  cooltime = 20000, rate = 80,rangemin = 0, rangemax = 1500, target = 3, randomtarget = "1.6", encountertime = 10000 },
-- 라인드라이브
   { skill_index = 30123,  cooltime = 20000, rate = 60, rangemin = 0, rangemax = 1000, target = 3, encountertime = 10000 },
-- 망자의 관
   { skill_index = 31908,  cooltime = 15000, rate = 60, rangemin = 400, rangemax = 1200, target = 3, randomtarget = "1.6", encountertime = 15000 },
}
