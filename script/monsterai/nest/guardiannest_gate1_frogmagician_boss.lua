g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 900;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 500
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 4, loop = 1, selfhppercentrange = "25,100" },
   { action_name = "Stand", rate = 4, loop = 1, selfhppercentrange = "0,24" },
   { action_name = "Move_Left", rate = 6, loop = 2, globalcooltime = 3 },
   { action_name = "Move_Right", rate = 6, loop = 2, globalcooltime = 4 },
   { action_name = "Move_Back", rate = 2, loop = 1 },
   { action_name = "Move_Teleport_Left", rate = 6, loop = 1, td = "RF,RB", globalcooltime = 3 },
   { action_name = "Move_Teleport_Right", rate = 6, loop = 1, td = "LF,LB", globalcooltime = 4 },
   { action_name = "Attack01_YoYo", rate = 20, loop = 1},
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 2, globalcooltime = 3 },
   { action_name = "Move_Right", rate = 2, loop = 2, globalcooltime = 4 },
   { action_name = "Move_Teleport_Left", rate = 6, loop = 1, td = "RF,RB", globalcooltime = 3 },
   { action_name = "Move_Teleport_Right", rate = 6, loop = 1, td = "LF,LB", globalcooltime = 4 },
   { action_name = "Walk_Left", rate = 4, loop = 2 },
   { action_name = "Walk_Right", rate = 4, loop = 2 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Move_Front", rate = 6, loop = 1  },
}

g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
}

g_Lua_SkillProcessor = {
   { skill_index = 34001, changetarget = "100,0" },
   { skill_index = 34004, changetarget = "100,0" },
}

-- 궁
g_Lua_GlobalCoolTime1 = 20000
-- 블라
g_Lua_GlobalCoolTime2 = 35000
-- 텔레
g_Lua_GlobalCoolTime3 = 10000
g_Lua_GlobalCoolTime4 = 10000
-- 창과방패
g_Lua_GlobalCoolTime5 = 10000

g_Lua_Skill = { 
-- 순간이동(아직 ㄴㄴ해)
   { skill_index = 34006, cooltime = 1000, globalcooltime = 1, rate = 100, priority = 90, rangemin = 0, rangemax = 5000, target = 1, limitcount = 1, selfhppercent = 75 },
   { skill_index = 34006, cooltime = 1000, globalcooltime = 1, rate = 100, priority = 80, rangemin = 0, rangemax = 5000, target = 1, limitcount = 1, selfhppercent = 50 },
   { skill_index = 34006, cooltime = 1000, globalcooltime = 1, rate = 100, priority = 70, rangemin = 0, rangemax = 5000, target = 1, limitcount = 1, selfhppercent = 25 },
-- 창병 소환
   { skill_index = 34009, cooltime = 25000, globalcooltime = 1, globalcooltime2 = 5, rate = 100, priority = 30, rangemin = 0, rangemax = 5000, target = 1 },
   { skill_index = 34009, cooltime = 15000, rate = -1, priority = 30, rangemin = 0, rangemax = 5000, target = 1, selfhppercentrange = "0,23" },
-- 마이크로
   { skill_index = 34004, cooltime = 15000, rate = 70, rangemin = 0, rangemax = 600, target = 3 },
-- 버블버블 다크
   { skill_index = 34001, cooltime = 15000, rate = 70, rangemin = 600, rangemax = 1200, target = 3, randomtarget = "1.6,0,1" },
-- 서먼블랙홀
   { skill_index = 34016, cooltime = 35000, globalcooltime = 2, rate = 70, rangemin = 0, rangemax = 600, target = 3, randomtarget = "1.6,0,1", next_lua_skill_index = 8 },
   { skill_index = 34002, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 4500, target = 3 },
   { skill_index = 34002, cooltime = 35000, globalcooltime = 2, rate = 70, rangemin = 500, rangemax = 1000, target = 3, randomtarget = "1.6,0,1" },
-- 버프삭제
   { skill_index = 34003, cooltime = 40000, rate = 90, rangemin = 0, rangemax = 5000, target = 3 },
-- 점프
   { skill_index = 34005, cooltime = 40000, globalcooltime = 5, rate = 70, priority = 20, rangemin = 0, rangemax = 5000, target = 1, selfhppercent = 73 },
-- 방벽 추가
   { skill_index = 34018, cooltime = 1000, globalcooltime = 5, rate = 100, priority = 50, rangemin = 0, rangemax = 5000, target = 1, selfhppercent = 49, limitcount = 1 },
}



