-- Cleric Relic of Lightnig AI

g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 1500.0;
g_Lua_NearValue2 = 3000.0;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 200;
g_Lua_AssaultTime = 5000;

g_Lua_Near1 = 
{
	{ action_name = "Stand",		rate = 5,		loop = 1 }, 
	{ action_name = "Attack_Entrap_Behit",		rate = 5,		loop = 1 }, 
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",		rate = 10,		loop = 1 },
}

g_Lua_BeHitSkill =
	{
		{ lua_skill_index = 4, rate = 100, skill_index = 34645 },
		{ lua_skill_index = 4, rate = 100, skill_index = 34646 },
		{ lua_skill_index = 4, rate = 100, skill_index = 34647 },
		{ lua_skill_index = 4, rate = 100, skill_index = 34648 },
	}
	
g_Lua_GlobalCoolTime1 = 60000

g_Lua_Skill=
{
	{ skill_index = 34645, cooltime = 1000, rangemin = 0, rangemax = 10000, target = 3, rate = 70, globalcooltime = 1, limitcount =1},
	{ skill_index = 34646, cooltime = 1000, rangemin = 0, rangemax = 10000, target = 3, rate = 80, globalcooltime = 1, limitcount =1},
	{ skill_index = 34647, cooltime = 1000, rangemin = 0, rangemax = 10000, target = 3, rate = 90, globalcooltime = 1, limitcount =1},
	{ skill_index = 34648, cooltime = 1000, rangemin = 0, rangemax = 10000, target = 3, rate = 100, globalcooltime = 1, limitcount =1},
	{ skill_index = 34649, cooltime = 1000, rangemin = 0, rangemax = 10000, target = 3, rate = -1,limitcount =1},
}  

