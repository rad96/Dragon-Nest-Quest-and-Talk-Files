-- Celberos Hard
g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 5000
g_Lua_NearValue2 = 6000

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 320;
g_Lua_AssualtTime = 3000;

g_Lua_Near1 = 
{ 
    { action_name = "Stand", rate = 5, loop = 1 },
}

g_Lua_Near2 = 
{ 
    { action_name = "Stand", rate = 5, loop = 1 },
}

g_Lua_Skill=
{
-- 터널(루프30 / 스타트3 / 엔드0.5 = 총 33.5) (33.5+4.5+33=71 + 5 = 76)
    { skill_index = 32123, cooltime = 76000, rate = 100, rangemin = 0, rangemax = 6000, target = 3, next_lua_skill_index = 1 },
-- 이동(루프4 / 스타트0.25 / 엔드0.25 = 총 4.5)
    { skill_index = 32129, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, next_lua_skill_index = 2 },
-- 꼬리나옴(루프30 / 스타트1.5 / 엔드1.5 = 총 33)
    { skill_index = 32124, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, next_lua_skill_index = 3 },
-- 서먼오프
    { skill_index = 32114, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 6000, target = 3, selfhppercent = 50 },
}

