--TypoonKimNest_Bonus_SantaOrc.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 2400;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 3  },
   { action_name = "Walk_Right", rate = 10, loop = 3  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 3  },
   { action_name = "Move_Right", rate = 20, loop = 3  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 3  },
   { action_name = "Walk_Right", rate = 10, loop = 3  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 3  },
   { action_name = "Move_Right", rate = 20, loop = 3  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 3  },
   { action_name = "Walk_Right", rate = 10, loop = 3  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 3  },
   { action_name = "Move_Right", rate = 20, loop = 3  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 3  },
   { action_name = "Walk_Right", rate = 10, loop = 3  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 20, loop = 3  },
   { action_name = "Move_Right", rate = 20, loop = 3  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
}