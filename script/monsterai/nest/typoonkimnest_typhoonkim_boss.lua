--AiMinotauros_Red_Boss_Nest.lua

g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 1200.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3000.0;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 5000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 8000

g_Lua_GlobalCoolTime1 = 50000 -- 트리거
g_Lua_GlobalCoolTime2 = 10000 -- 스트레이트.
g_Lua_GlobalCoolTime3 = 41000 -- Shout
g_Lua_GlobalCoolTime4 = 35000 --휠윈드
g_Lua_GlobalCoolTime5 = 25000 -- 그레이트 봄버!
g_Lua_GlobalCoolTime6 = 15000 --

g_Lua_Near1 = 
	{ 
		{ action_name = "Stand", rate = 18, loop = 1 },
		{ action_name = "Walk_Left", rate = 13, loop = 1 },
		{ action_name = "Walk_Right", rate = 13, loop = 1 },
		-- { action_name = "Attack001_ArmedPush", rate = 10, loop = 1 ,cooltime=8000, notusedskill = "33290", td="FR,FL"},
		{ action_name = "Attack002_Combo", rate = 13, loop = 1 ,cooltime=25000, notusedskill = "33290" ,globalcooltime=6},
		{ action_name = "Attack003_FullSwing", rate = 15, loop = 1, cooltime=8000, notusedskill = "33290" },
		{ action_name = "Attack003_FullSwing_Lightning", rate = 15, loop = 1, usedskill = "33290" },
		{ action_name = "Attack012_Chop_Lightning", rate = 15, loop = 1 ,usedskill = "33290"},
	}
g_Lua_Near2 = 
	{ 
		{ action_name = "Stand", rate = 14, loop = 1 },	
		{ action_name = "Walk_Left", rate = 18, loop = 1 },
		{ action_name = "Walk_Right", rate = 18, loop = 1 },		
		-- { action_name = "Attack001_ArmedPush", rate = 8, loop = 1 ,cooltime=8000, notusedskill = "33290" ,globalcooltime=6},
		{ action_name = "Attack003_FullSwing", rate = 15, loop = 1, notusedskill = "33290" },
		
		{ action_name = "Attack003_FullSwing_Lightning", rate = 20, loop = 1, usedskill = "33290" },
		{ action_name = "Attack012_Chop_Lightning", rate = 15, loop = 1 ,usedskill = "33290"},
	}
g_Lua_Near3 = 
	{ 
		{ action_name = "Stand", rate = 15, loop = 1 },
		{ action_name = "Attack013_JumpChop", rate = 15, loop = 1, notusedskill = "33290" },
		{ action_name = "Attack005_GreatBomb", rate = 13, loop = 1, notusedskill = "33290", cooltime=25000,globalcooltime=5},
		{ action_name = "Move_Front", rate = 13, loop = 1 },
		{ action_name = "Move_Left", rate = 15, loop = 1 },
		{ action_name = "Move_Right", rate = 15, loop = 1 },		
		{ action_name = "Attack013_JumpChop_Lightning", rate = 15, loop = 1 ,usedskill = "33290" },
	}
g_Lua_Near4 = 
	{ 
		{ action_name = "Stand", rate = 10, loop = 1 },
		{ action_name = "Attack005_GreatBomb", rate = 28, loop = 1,notusedskill = "33290" ,cooltime=25000,globalcooltime=5},
		{ action_name = "Move_Left", rate = 20, loop = 1 },
		{ action_name = "Move_Right", rate = 20, loop = 1 },
		{ action_name = "Move_Front", rate = 15, loop = 1 },
		{ action_name = "Walk_Front", rate = 25, loop = 1 },
	}
g_Lua_Near5 = 
	{ 
		{ action_name = "Stand", rate = 2, loop = 1 },
		{ action_name = "Move_Front", rate = 25, loop = 2 },
	}

g_Lua_Skill = 
	{ 
		{skill_index=33298,  cooltime=55000, rate=50,   rangemin=0,  rangemax=500,   target=3, usedskill = "33290", next_lua_skill_index=1 },--Attack007_Stomp_Lightning
		{skill_index=33299,  cooltime=1000, rate=-1,   rangemin=0,  rangemax=5000,   target=3, },--Attack014_WhirlWindLight_Start
		
		{skill_index=33304,  cooltime=90000, rate=100,   rangemin=0,  rangemax=5000,   target=3, selfhppercent=80, },--Attack008_IWillbeBack
		{skill_index=33290,  cooltime=100, rate=100,   rangemin=0,  rangemax=3000,   target=3, selfhppercent=60, limitcount=1  },--Attack011_WeaponChange_Start
		-- {skill_index=33301,  cooltime=100, rate=100,   rangemin=0,  rangemax=5000,   target=1, selfhppercent = 40, limitcount=1},--Attack016_Stare
		
 		{skill_index=33292,  cooltime=70000, rate=30,   rangemin=0,  rangemax=3000,   target=3, encountertime=40000, globalcooltime=1 },--Attack017_Fear
		
		{skill_index=33286,  cooltime=30000, rate=80,   rangemin=0,  rangemax=1200,   target=3,  notusedskill = "33290", globalcooltime=2 },--Attack004_StraightArm
		-- {skill_index=33287,  cooltime=60000, rate=20,   rangemin=0,  rangemax=600,   target=3, notusedskill = "33290", encountertime=25000, globalcooltime=3,},--Attack006_Shout
		{skill_index=33302,  cooltime=20000, rate=45,   rangemin=300,  rangemax=1200,   target=3,  notusedskill="33290", selfhppercentrange = "60,100" },--Attack018_SlideCharge_Start

		{skill_index=33295,  cooltime=25000, rate=30,   rangemin=0,  rangemax=600,   target=3, usedskill = "33290" },--Attack002_Combo_Lightning
		{skill_index=33294,  cooltime=40000, rate=80,   rangemin=0,  rangemax=1200,   target=3, usedskill = "33290", globalcooltime=2 },--Attack004_StraightArm_Lightning
		-- {skill_index=33297,  cooltime=80000, rate=20,   rangemin=0,  rangemax=600,   target=3, usedskill = "33290",globalcooltime=3},--Attack006_Shout_Lightning
		{skill_index=33296,  cooltime=25000, rate=60,   rangemin=600,  rangemax=1500,   target=3,  usedskill="33290" ,globalcooltime=5 },--Attack005_GreatBomb_Lightning
		
		-- {skill_index=33293,  cooltime=60000, rate=80,   rangemin=0,  rangemax=1000,   target=3, usedskill="33290", selfhppercentrange = "0,40"  },--Attack008_ShockBreak
		-- {skill_index=33288,  cooltime=25000, rate=50,   rangemin=0,  rangemax=500,   target=3, encountertime=10000, notusedskill = "33290", next_lua_skill_index=2 },--Attack007_Stomp
		-- {skill_index=33291,  cooltime=1000, rate=-1,   rangemin=0,  rangemax=5000,   target=3, },--Attack014_WhirlWind_Start
	}