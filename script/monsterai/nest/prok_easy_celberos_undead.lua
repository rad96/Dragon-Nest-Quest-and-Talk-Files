
g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 700;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1550;
g_Lua_NearValue4 = 2500;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 400;
g_Lua_AssualtTime = 500;

g_Lua_GlobalCoolTime1 = 13000
g_Lua_GlobalCoolTime2 = 10000

g_Lua_CustomAction =
{
   CustomAction1 = 
   {
	{ "Avoid", 0},
	{ "Attack_Jump", 0},	 		
   },
   CustomAction2 = 
   {
	{ "Attack_Down_Left_SeaDragon", 0},
	{ "Attack_Down_Right_SeaDragon", 0},
 		
   },
   CustomAction3 = 
   {
	{ "Attack_Down_Right_SeaDragon", 0},
	{ "Attack_Down_Left_SeaDragon", 0},
   },
}
g_Lua_Near1 = 
{
   { action_name = "Stand_2", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Turn_Left",	rate = 13, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 13, loop = -1, td = "RF,RB,BR" },
   { action_name = "Attack_Punch", rate = 30, loop = 1, td = "FL,FR,LF" },
   { action_name = "CustomAction2", rate = 30, loop = 1, cooltime = 8000, encountertime = 10000, td = "FL,FR", globalcooltime = 1 },
   { action_name = "CustomAction3", rate = 30, loop = 1, cooltime = 8000, encountertime = 10000, td = "FL,FR", globalcooltime = 1 },
   { action_name = "Attack_Bite", rate = -1, loop = 1, cooltime = 8000, encountertime = 10000, td = "FL,FR" },
   { action_name = "Attack_Cry", rate = -1, loop = 1, cooltime = 60000, selfhppercent = 75 },
   { action_name = "Attack_Back_Left", rate = 22, loop = 1, td = "BL" },
   { action_name = "Attack_Back_Right", rate = 22, loop = 1, td = "BR" },
}
g_Lua_Near2 = 
{
   { action_name = "Stand_2", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 10, loop = 1, td = "FL,FR", cooltime = 13000 },
   { action_name = "Turn_Left",	rate = 15, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 15, loop = -1, td = "RF,RB,BR" },
   { action_name = "CustomAction1", rate = 30, loop = 1, cooltime = 10000, td = "FL,FR", selfhppercent = 50, globalcooltime = 2 },
}
g_Lua_Near3 = 
{
   { action_name = "Stand_2", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 15, loop = 2, td = "FL,FR" },
   { action_name = "Turn_Left",	rate = 25, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 25, loop = -1, td = "RF,RB,BR" },
   { action_name = "Attack_Jump", rate = 15, loop = 1, cooltime = 10000, td = "FL,FR", selfhppercent = 50, globalcooltime = 2, randomtarget = 1.1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand_2", rate = 5, loop = 1, td = "FL,FR" },
   { action_name = "Walk_Front", rate = 25, loop = 3, td = "FL,FR" },
   { action_name = "Walk_Left", rate = 5, loop = 2, td = "LF,FL,FR,RF,RB,BR,BL,LB" },
   { action_name = "Walk_Right", rate = 5, loop = 2, td = "LF,FL,FR,RF,RB,BR,BL,LB" },
   { action_name = "Turn_Left",	rate = 25, loop = -1, td = "LF,LB,BL" },
   { action_name = "Turn_Right", rate = 25, loop = -1, td = "RF,RB,BR" },
}
g_Lua_Assault = { 
   { action_name = "Attack_Punch", rate = 5, loop = 1, approach = 400 },
}
g_Lua_Skill=
{
-- 그림자 함정
   { skill_index = 32420, cooltime = 20000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", multipletarget = 1, limitcount = 1 },
   { skill_index = 32420, cooltime = 20000, rate = 100, rangemin = 0, rangemax = 1900, target = 3, td = "FL,FR", multipletarget = 1, selfhppercent = 75 },
-- 부패의 저주
   { skill_index = 30534, cooltime = 20000, rangemin = 0, rangemax = 2500, target = 3, rate = 100, selfhppercent = 75, td = "FL,FR" },

}