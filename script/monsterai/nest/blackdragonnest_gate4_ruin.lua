g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 1250;
g_Lua_NearValue6 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 7, loop = 1 },
}
g_Lua_Skill = { 
   { skill_index = 34821, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, randomtarget = "1.6,0,1" },
}
