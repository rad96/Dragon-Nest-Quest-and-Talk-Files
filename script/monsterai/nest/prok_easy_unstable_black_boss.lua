--AiUnstable_Black_Elite_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Attack1_Kick", rate = 17, loop = 1 },
   { action_name = "Attack8_Combo", rate = 10, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
   { action_name = "Attack4_Saw", rate = 27, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 14, loop = 1 },
   { action_name = "Attack2_Teleport", rate = 9, loop = 1 },
   { action_name = "Assault", rate = 9, loop = 1 },
   { action_name = "Attack3_Bite", rate = 17, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Walk_Front", rate = 6, loop = 2 },
   { action_name = "Move_Teleport", rate = 6, loop = 1  },
   { action_name = "Assault", rate = 17, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Kick", rate = 12, loop = 1, approach = 200, max_missradian = 10 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 200 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 200 },
   { action_name = "Stand_1", rate = 4, loop = 1, approach = 200 },
}
g_Lua_Skill = { 
   --어스 웨이브
   { skill_index = 33134,  cooltime = 30000, rate = 70, rangemin = 0, rangemax = 1500, target = 3 },
   --어스 붐
   { skill_index = 33135,  cooltime = 30000, rate = 70, rangemin = 0, rangemax = 500, target = 3 },
   --서클 붐
   --{ skill_index = 33137,  cooltime = 45000, rate = 70, rangemin = 0, rangemax = 500, target = 3 }, 
   --강한 접프 어택
   { skill_index = 33136,  cooltime = 52000, rate = 70, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 50, randomtarget=1.1 },
}

