--GreenDragonNest_FinalBoss_GreenDragon_3Phase.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 2000;
g_Lua_NearValue2 = 3000;


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 30000
g_Lua_GlobalCoolTime2 = 50000
g_Lua_GlobalCoolTime3 = 60000


g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, selfhppercent = 100, custom_state1 = "custom_Ground"  },
--   { action_name = "Attack11_Bite&Attack", rate = 20, loop = 2, selfhppercentrange = "0,100", td = "FL,FR,LF,RF",  cooltime = 10000, custom_state1 = "custom_Ground"  },
--   { action_name = "Attack12_Right&LeftAttack", rate = 20, loop = 2, selfhppercentrange = "0,100", td = "FL,FR,LF,RF",  cooltime = 10000, custom_state1 = "custom_Ground"  },
--   { action_name = "Attack14_TailAttack", rate = 20, loop = 2, selfhppercentrange = "0,100", td = "RB,BR,BL,LB",  cooltime = 20000, custom_state1 = "custom_Ground"  },
--   { action_name = "Attack13_Stomp&Tail", rate = 20, loop = 2, selfhppercentrange = "0,100", td = "FL,FR,LF,RF,RB,BR,BL,LB",  cooltime = 30000, custom_state1 = "custom_Ground" },
--   { action_name = "Attack33_CliffAttack", rate = 20, loop = 2, selfhppercentrange = "0,100", td = "FL,FR,LF,RF,RB,BR,BL,LB",  cooltime = 50000, custom_state1 = "custom_UnderGround"  },
   { action_name = "Attack54_FlyLanding", rate = 30, loop = 1, selfhppercentrange = "0,100", td = "FL,FR,LF,RF,RB,BR,BL,LB",  cooltime = 50000, custom_state1 = "custom_Fly"  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 100, loop = 1, selfhppercent = 100, custom_state1 = "custom_Ground"  },
--   { action_name = "Attack11_Bite&Attack", rate = 20, loop = 2, selfhppercentrange = "0,100", td = "FL,FR,LF,RF",  cooltime = 10000, custom_state1 = "custom_Ground"  },
--   { action_name = "Attack12_Right&LeftAttack", rate = 20, loop = 2, selfhppercentrange = "0,100", td = "FL,FR,LF,RF",  cooltime = 10000, custom_state1 = "custom_Ground"  },
--   { action_name = "Attack14_TailAttack", rate = 20, loop = 2, selfhppercentrange = "0,100", td = "RB,BR,BL,LB",  cooltime = 20000, custom_state1 = "custom_Ground"  },
--   { action_name = "Attack13_Stomp&Tail", rate = 20, loop = 2, selfhppercentrange = "0,100", td = "FL,FR,LF,RF,RB,BR,BL,LB",  cooltime = 30000, custom_state1 = "custom_Ground"  },
--   { action_name = "Attack33_CliffAttack", rate = 20, loop = 2, selfhppercentrange = "0,100", td = "FL,FR,LF,RF,RB,BR,BL,LB",  cooltime = 50000, custom_state1 = "custom_UnderGround"  },
   { action_name = "Attack54_FlyLanding", rate = 30, loop = 1, selfhppercentrange = "0,100", td = "FL,FR,LF,RF,RB,BR,BL,LB",  cooltime = 50000, custom_state1 = "custom_Fly"  },
}


g_Lua_Skill = { 
-- 라이트브레스
--   { skill_index = 31553,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "40,100", custom_state1 = "custom_Ground", globalcooltime = 1},
-- 레프트브레스
--   { skill_index = 31554,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "40,100", custom_state1 = "custom_Ground", globalcooltime = 1},
-- 넝쿨 소환
--   { skill_index = 31555,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "20,100", custom_state1 = "custom_Ground", multipletarget = "1,3,exceptcannon"},
-- 정면 포이즌 브레스
--   { skill_index = 31556,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "40,100", custom_state1 = "custom_Ground"},


-- 플레임 도넛
   { skill_index = 31557,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "20,100", custom_state1 = "custom_Ground", globalcooltime = 2},
-- 블리자드
   { skill_index = 31558,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "20,100", custom_state1 = "custom_Ground", globalcooltime = 2},


-- 나무 소환
--   { skill_index = 31559,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "0,20", custom_state1 = "custom_Ground"},
-- 포이즌 미스트
--   { skill_index = 31560,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "0,20", custom_state1 = "custom_Ground"},
-- 포이즌 샤워
--   { skill_index = 31561,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "0,20", custom_state1 = "custom_Ground"},


-- 절벽 브레스
--   { skill_index = 31581,  cooltime = 60000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,80", custom_state1 = "custom_Ground"},
-- 절벽 리턴 브레스
--   { skill_index = 31586,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,80", custom_state1 = "custom_UnderGround"},

-- 절벽 이동
--   { skill_index = 31582,  cooltime = 60000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,60", custom_state1 = "custom_Ground"},
-- 절벽 파이어볼
--   { skill_index = 31583,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,60", custom_state1 = "custom_UnderGround"},
-- 절벽 아이스월
--   { skill_index = 31584,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,60", custom_state1 = "custom_UnderGround"},
-- 절벽 날갯짓
--   { skill_index = 31585,  cooltime = 60000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,60", custom_state1 = "custom_UnderGround"},


-- 비상 몬스터소환
   { skill_index = 31591,  cooltime = 60000, rate = 50, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,100", custom_state1 = "custom_Ground"},
-- 비상 볼케이노
   { skill_index = 31592,  cooltime = 60000, rate = 70, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,100", custom_state1 = "custom_Fly", globalcooltime = 3 },
-- 비상 글레이서
   { skill_index = 31593,  cooltime = 60000, rate = 100, rangemin = 0, rangemax = 40000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,100", custom_state1 = "custom_Fly", globalcooltime = 3 },
}