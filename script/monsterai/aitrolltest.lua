-- �׽�Ʈ ���� AI

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 500.0;
g_Lua_NearValue2 = 1000.0;
g_Lua_NearValue3 = 1500.0;

g_Lua_LookTargetNearState = 3;

g_Lua_WanderingDistance = 1500.0;

g_Lua_PatrolBaseTime = 5000;

g_Lua_PatrolRandTime = 3000;


g_Lua_CustomAction =
{
	CustomAction1 = 
	{
		{ "Move_Front", 1},
		{ "Attack4", 1},
	},
	
	CustomAction2 = 
	{ 
		{ "Move_Right", 1},
		{ "Move_Front", 1},
		{ "Attack4", 1},
	},
		
	CustomAction3 = 
	{ 
		{ "Move_Back",  2},
		{ "Move_Right", 2}, 
		{ "Move_Front", 2},
		{ "Attack4", 1},
	}
}


g_Lua_CustomAttack =
{
	CustomAttack1 = 
	{
		{ "Move_Back",  1},
	 	{ "Attack1", 1},
	 	{ "Attack2",  3},
	 	{ "Attack3",  1},
	}
}

g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 1 },
	{ action_name = "Walk_Back",	rate = 15,		loop = 1 },

}

g_Lua_Near2 = 
{
	{ action_name = "CustomAction1", rate = 10, loop = 1 }, 
	{ action_name = "CustomAction2", rate = 10, loop = 1 },
	{ action_name = "CustomAction3", rate = 10, loop = 1 },
	{ action_name = "CustomAttack1", rate = 50, loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 15,		loop = 1 },
	{ action_name = "Move_Right",	rate = 15,		loop = 1 },
	{ action_name = "Move_Front",	rate = 40,		loop = 1 },
}
