--SeaDragonNest_Gate5_Shadow_Dark.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 50
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Walk_Front_SeaDragon", rate = 15, loop = 2 },
   { action_name = "Move_Front_SeaDragon", rate = 15, loop = 1, cooltime = 8000 },
   { action_name = "Move_Front_SeaDragon", rate = 10, loop = 3, cooltime = 10000, selfhppercent = 30 },
   { action_name = "Walk_Left_SeaDragon", rate = 3, loop = 2  },
   { action_name = "Walk_Right_SeaDragon", rate = 3, loop = 2  },
   { action_name = "Attack1_Claw_SeaDragon", rate = 6, loop = 1, cooltime = 10000, randomtarget = 1.0 },
   { action_name = "Attack4_FireWall", rate = 6, loop = 1, cooltime = 10000 },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Front_SeaDragon", rate = 25, loop = 2 },
   { action_name = "Move_Front_SeaDragon", rate = 25, loop = 1, cooltime = 8000 },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Front_SeaDragon", rate = 15, loop = 2 },
   { action_name = "Move_Front_SeaDragon", rate = 15, loop = 1, cooltime = 8000 },
   { action_name = "Move_Front_SeaDragon", rate = 10, loop = 3, cooltime = 10000, selfhppercent = 30 },
   { action_name = "Walk_Left_SeaDragon", rate = 3, loop = 2 },
   { action_name = "Walk_Right_SeaDragon", rate = 3, loop = 2  },
   { action_name = "Attack1_Claw_SeaDragon", rate = 6, loop = 1, cooltime = 10000, randomtarget = 1.0 },
   { action_name = "Attack4_FireWall", rate = 6, loop = 1, cooltime = 10000 },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front_SeaDragon", rate = 15, loop = 2 },
   { action_name = "Move_Front_SeaDragon", rate = 15, loop = 1, cooltime = 8000 },
   { action_name = "Move_Front_SeaDragon", rate = 10, loop = 3, cooltime = 10000, selfhppercent = 30 },
   { action_name = "Walk_Left_SeaDragon", rate = 3, loop = 2  },
   { action_name = "Walk_Right_SeaDragon", rate = 3, loop = 2  },
   { action_name = "Attack1_Claw_SeaDragon", rate = 6, loop = 1, cooltime = 10000, randomtarget = 1.0 },
   { action_name = "Attack4_FireWall", rate = 6, loop = 1, cooltime = 10000 },
}
g_Lua_Skill = { 
--����
   { skill_index = 30535, cooltime = 46000, rate = 100, rangemin = 0, rangemax = 1500, target = 3, multipletarget = "1,3", encountertime = 8000, selfhppercent = 50 },
--����
   { skill_index = 30528, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 3500, target = 3, announce = "100,10000" },
}