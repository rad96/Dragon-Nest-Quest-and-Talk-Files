-- Goblin Blue Elite Normal AI
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 300.0;
g_Lua_NearValue3 = 500.0;
g_Lua_NearValue4 = 800.0;
g_Lua_NearValue5 = 1200.0;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 100;
g_Lua_AssaultTime = 3000;


g_Lua_CustomAction = {
  CustomAction1 = {
      { "Move_Back", 5 },
      { "Look_Back", 0 },
      { "Move_Back", 0 },
      { "Turn_Left", 0 },
  },
  CustomAction2 = {
      { "Walk_Left", 4 },
      { "Turn_Right", 0 },
  },
  CustomAction3 = {
      { "Walk_Right", 4 },
      { "Turn_Left", 0 },
  },
  CustomAction4 = {
      { "Move_Back", 4 },
      { "Turn_Right", 0 },
  },
  CustomAction5 = {
      { "Move_Back", 3 },
      { "Look_Back", 0 },
      { "Move_Back", 0 },
      { "Turn_Left", 0 },
  },
  CustomAction6 = {
      { "Move_Left", 3 },
      { "Turn_Right", 0 },
  },
  CustomAction7 = {
      { "Move_Right", 3 },
      { "Turn_Left", 0 },
  },
  CustomAction8 = {
      { "Move_Back", 3 },
      { "Turn_Right", 0 },
  },
  CustomAction9= {
      { "Attack1_Bite", 0 },
      { "Move_Back", 3 },
      { "Look_Back", 0 },
      { "Turn_Left", 0 },
  },
  CustomAction10= {
      { "Attack1_Bite", 0 },
      { "Move_Back", 1 },
      { "Look_Back", 0 },
      { "Turn_Right", 0 },
  },
}

g_Lua_Near1 = 
{ 
	{ action_name = "CustomAction5",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction8",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction1",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction4",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction9",	rate = 30,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "CustomAction1",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 15,		loop = 1 },
	{ action_name = "CustomAction3",	rate = 15,		loop = 1 },	
	{ action_name = "CustomAction6",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction7",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction10",	rate = 30,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "CustomAction1",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 15,		loop = 1 },
	{ action_name = "CustomAction3",	rate = 15,		loop = 1 },	
	{ action_name = "CustomAction6",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction7",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction10",	rate = 30,		loop = 1 },
}
g_Lua_Near4 = 
{ 
	{ action_name = "Stand",	rate = 3,		loop = 1 },
	{ action_name = "CustomAction1",	rate = 10,		loop = 1 },
	{ action_name = "CustomAction2",	rate = 15,		loop = 1 },
	{ action_name = "CustomAction3",	rate = 15,		loop = 1 },	
	{ action_name = "CustomAction6",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction7",	rate = 20,		loop = 1 },
	{ action_name = "CustomAction10",	rate = 30,		loop = 1 },
}
g_Lua_Near5 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 2 },
}