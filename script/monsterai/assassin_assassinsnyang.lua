
g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;
g_Lua_NearValue3 = 300.0;
g_Lua_NearValue4 = 600.0;
g_Lua_NearValue5 = 800.0;
g_Lua_NearValue6 = 1200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 1;
g_Lua_WanderingDistance = 10000;
g_Lua_ApproachValue = 100;
g_Lua_AssualtTime = 3000;

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"!Stay|!Move|!Attack|!Stiff|!Down", "Air", "Hit" }, 
}

g_Lua_GlobalCoolTime1 = 2600

g_Lua_CustomAction = {
-- 
  CustomAction1 = 
   {
	   { "Tumble_Back" },
   },
   CustomAction2 = 
   {
	   { "useskill", lua_skill_index = 0, rate = 100 },
   },
 }

g_Lua_Near1 = 
{ 
	 { action_name = "Move_Left",	rate = 2,		loop = 1, td = "FL,FR,LF,RF,LB,BL,BR,RB" },
	 { action_name = "Move_Right",	rate = 2,		loop = 1, td = "FL,FR,LF,RF,LB,BL,BR,RB" },
	 { action_name = "Move_Front",	rate = 2,		loop = 1, td = "FL,FR,LF,RF,LB,BL,BR,RB" },
	--{ action_name = "Attack_AjanaDrop", rate = 100, loop = 1, target_condition = "State3", cooltime = 5000, cancellook = 0 },
     { action_name = "CustomAction2", rate = 30,  loop = 1, td = "FL,FR,LF,RF,LB,BL,BR,RB" },
     { action_name = "CustomAction1", rate = 100,  target_condition = "State3",  loop = 1, td = "FL,FR,LF,RF,LB,BL,BR,RB"},
}

g_Lua_Near2 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 1, td = "FL,FR,LF,RF,LB,BL,BR,RB" },
	{ action_name = "Tumble_Front",	rate = 100,		loop = 1, td = "FL,FR,LF,RF,LB,BL,BR,RB", globalcooltime = 1 },

}

g_Lua_Near3 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3 , td = "FL,FR,LF,RF,LB,BL,BR,RB" },
	{ action_name = "Tumble_Front",	rate = 100,		loop = 1, td = "FL,FR,LF,RF,LB,BL,BR,RB", globalcooltime = 1 },

}

g_Lua_Near4 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3, td = "FL,FR,LF,RF,LB,BL,BR,RB" },
	{ action_name = "Tumble_Front",	rate = 100,		loop = 1, td = "FL,FR,LF,RF,LB,BL,BR,RB", globalcooltime = 1 },

	
}

g_Lua_Near5 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 3, td = "FL,FR,LF,RF,LB,BL,BR,RB" },
	{ action_name = "Tumble_Front",	rate = 100,		loop = 1, td = "FL,FR,LF,RF,LB,BL,BR,RB", globalcooltime = 1 },

}

g_Lua_Near6 = 
{ 
	{ action_name = "Move_Front",	rate = 15,		loop = 5, td = "FL,FR,LF,RF"},
	{ action_name = "Tumble_Front",	rate = 100,		loop = 1, td = "FL,FR,LF,RF,LB,BL,BR,RB", globalcooltime = 1 },
}


g_Lua_Skill =
{
--0아즈나블레이드Lv1
	 {skill_index=261801,  cooltime = 5000, rangemin = 100, rangemax = 1000, target = 3, rate = 30, combo1 = " 4,100, 0"},
--1아즈나드롭Lv1
	 {skill_index=262101,  cooltime = 7000, rangemin = 0, rangemax = 200, target = 3, rate = 20, priority= 3 , combo1 = "5,60,0" },
--2아즈나 평타 
	 {skill_index=262301, cooltime = -1, rangemin = 0, rangemax = 200, target = 3, rate = 50, priority= 2, combo1 = "3,100,0" },
--3아즈나 평타1 
	 {skill_index=262302, cooltime = -1, rangemin = 0, rangemax = 250, target = 3, rate = 50, priority= 2, combo1 = "0,80,0", combo2 = "2,100,0"},
--4덤블링F 
	 {skill_index=262303, cooltime = 5000, rangemin = 0, rangemax = 1200, target = 3, rate = 30, cancellook = 0, combo1 = " 0,60,0", globalcooltime = 1 },	
--5덤블링B 
	 {skill_index=262304, cooltime = 5000, rangemin = 0, rangemax = 200, target = 3, rate = -1, cancellook = 0, combo1 = " 0,100,0" },	 		
} 
  