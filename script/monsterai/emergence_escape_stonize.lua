--AiBroo_Blue_Normal.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 5000;
g_Lua_NearValue2 = 6000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_Skill = { 
-- Fear
   { skill_index = 21214, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, encountertime = 4000, limitcount = 1 },
}
