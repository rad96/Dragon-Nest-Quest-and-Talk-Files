

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 1200;
g_Lua_NearValue2 = 2000;
g_Lua_NearValue3 = 5000;
g_Lua_NearValue4 = 6000;
g_Lua_NearValue5 = 10000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 1000
g_Lua_AssualtTime = 5000

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "1Phase_Attack005_Bite_Center" },
      { "1Phase_Attack005_Bite_Left" },
      { "1Phase_Attack005_Bite_Right" },
  },
}

g_Lua_Near1 = 
{ 
-- 0 ~ 2000
	{ action_name = "Die",			rate = 100,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercent = 90 },
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 2,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "90,100" },
	{ action_name = "1Phase_Attack001_StampWithRH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "90,94", cooltime = 12000 },
	{ action_name = "1Phase_Attack002_StampWithLH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FL,LF", selfhppercentrange = "90,94", cooltime = 12000 },
	{ action_name = "1Phase_Attack003_SingleStampWithRH", rate = 12,custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "95,100", cooltime = 12000 },
	{ action_name = "1Phase_Attack004_SingleStampWithLH", rate = 12,custom_state1 = "custom_ground",	loop = 1,	td = "FL,LF", selfhppercentrange = "95,100", cooltime = 12000 },
--	{ action_name = "1Phase_Attack029_GraptoThrow_Right", rate = 12,custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "90,100", cooltime = 12000 },
--	{ action_name = "1Phase_Attack028_GraptoThrow_Left", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "CustomAction1", rate = 12,			custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "90,95", cooltime = 45000 },
}

g_Lua_Near2 = 
{ 
-- 2000 ~ 5000
	{ action_name = "Die",		rate = 100,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercent = 90 },
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 2,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "90,100" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 20,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8,		custom_state1 = "custom_ground",	loop = 1,	td = "LF", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "1Phase_Attack001_StampWithRH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "90,94", cooltime = 12000 },
	{ action_name = "1Phase_Attack002_StampWithLH",	rate = 10,	custom_state1 = "custom_ground",	loop = 1,	td = "FL,LF", selfhppercentrange = "90,94", cooltime = 12000 },
	{ action_name = "1Phase_Attack003_SingleStampWithRH", rate = 12,custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "1Phase_Attack004_SingleStampWithLH", rate = 12,custom_state1 = "custom_ground",	loop = 1,	td = "FL,LF", selfhppercentrange = "90,100", cooltime = 12000 },
--	{ action_name = "1Phase_Attack029_GraptoThrow_Right", rate = 12,custom_state1 = "custom_ground",	loop = 1,	td = "FR,RF", selfhppercentrange = "90,100", cooltime = 12000 },
--	{ action_name = "1Phase_Attack028_GraptoThrow_Left", rate = 12,	custom_state1 = "custom_ground",	loop = 1,	td = "LF,FL", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "CustomAction1", rate = 12,			custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "90,95", cooltime = 45000 },
}

g_Lua_Near3 = 
{ 
-- 1800 ~ 2500
	{ action_name = "Die",		rate = 100,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercent = 90 },
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 2,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "90,100" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 20,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8,		custom_state1 = "custom_ground",	loop = 1,	td = "LF", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "CustomAction1", rate = 12,			custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "90,95", cooltime = 45000 },
}

g_Lua_Near4 = 
{ 
-- 1800 ~ 2500
	{ action_name = "Die",		rate = 100,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercent = 90 },
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 2,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "90,100" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 20,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8,		custom_state1 = "custom_ground",	loop = 1,	td = "LF", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF", selfhppercentrange = "90,100", cooltime = 12000 },
	{ action_name = "CustomAction1", rate = 12,			custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "90,95", cooltime = 45000 },
}


g_Lua_Near5 = 
{ 
-- 1800 ~ 2500
	{ action_name = "Die",		rate = 100,		custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercent = 90 },
	{ action_name = "Stand",		rate = 20,		custom_state1 = "custom_ground",	loop = 2,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "90,100" },
	{ action_name = "1Phase_Attack005_Bite_Center", rate = 20,	custom_state1 = "custom_ground",	loop = 1,	td = "FR,FL", selfhppercentrange = "90,100", cooltime = 6000 },
	{ action_name = "1Phase_Attack005_Bite_Left", rate = 8,		custom_state1 = "custom_ground",	loop = 1,	td = "LF", selfhppercentrange = "90,100", cooltime = 8000 },
	{ action_name = "1Phase_Attack005_Bite_Right", rate = 8,	custom_state1 = "custom_ground",	loop = 1,	td = "RF", selfhppercentrange = "90,100", cooltime = 8000 },
	{ action_name = "CustomAction1", rate = 12,			custom_state1 = "custom_ground",	loop = 1,	td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "90,95", cooltime = 45000 },
}



g_Lua_Skill = { 
-- 드래곤브레스1
   { skill_index = 30603,  cooltime = 60000, rate = 100, custom_state1 = "custom_ground", rangemin = 0, rangemax = 10000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "90,100" },
-- 아이스볼트1
   { skill_index = 30590,  cooltime = 30000, rate = 40, custom_state1 = "custom_ground", rangemin = 0, rangemax = 10000, target = 3, selfhppercentrange = "90,95", td = "FL,FR,LF,RF,RB,BR,BL,LB"  },
-- 얼음감옥
--   { skill_index = 30591,  cooltime = 40000, rate = 100, custom_state1 = "custom_ground", rangemin = 700, rangemax = 3000, target = 3, selfhppercentrange = "85,90", td = "FL,FR,LF,RF,RB,BR,BL,LB" },

}