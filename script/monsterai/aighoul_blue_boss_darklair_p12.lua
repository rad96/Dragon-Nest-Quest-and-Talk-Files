--AiGhoul_Blue_Boss_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 600;
g_Lua_NearValue5 = 1000;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Move_Front_NotTarget", rate = 10, loop = 2 , max_missradian = 360   },
}
g_Lua_Near2 = { 
   { action_name = "Move_Front_NotTarget", rate = 20, loop = 2 , max_missradian = 360  },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Front_NotTarget", rate = 10, loop = 2 , max_missradian = 360   },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front_NotTarget", rate = 10, loop = 3 , max_missradian = 360   },
   { action_name = "Move_Front_NotTargett", rate = 10, loop = 2 , max_missradian = 360   },
}
g_Lua_Near5 = { 
   { action_name = "Walk_Front_NotTarget", rate = 10, loop = 2 , max_missradian = 360   },
   { action_name = "Move_Front_NotTarget", rate = 15, loop = 2 , max_missradian = 360   },
}
g_Lua_Near6 = { 
   { action_name = "Walk_Front_NotTarget", rate = 5, loop = 2 , max_missradian = 360  },
   { action_name = "Move_Front_NotTarget", rate = 5, loop = 3 , max_missradian = 360  },
}
g_Lua_Skill = { 
   { skill_index = 20026,  cooltime = 3000, rate = 80,rangemin = 0, rangemax = 1000, target = 3, selfhppercent = 100, max_missradian= 360 },
}
