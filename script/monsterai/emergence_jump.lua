--AiBroo_Blue_Normal.lua

g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 8000;
g_Lua_NearValue2 = 10000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
}

g_Lua_Skill = { 
-- 21210: 점프 / 21211: 30프레임 / 21212: 60프레임 / 21213: 서먼오프
--0 시작
   { skill_index = 21210, cooltime = 1000, rate = 100, rangemin = 0, rangemax = 8000, target = 3, combo1 = "18,100,1", combo2 = "1,100,0"},
   { skill_index = 21212, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "2,100,0" },
   { skill_index = 21210, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "18,100,1", combo2 = "3,100,0"},
   { skill_index = 21212, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "4,100,0" },
   { skill_index = 21210, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "18,100,1", combo2 = "5,100,0"},
   { skill_index = 21212, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "6,100,0" },
   { skill_index = 21210, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "18,100,1", combo2 = "7,100,0"},
   { skill_index = 21212, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "8,100,0" },
   { skill_index = 21210, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "18,100,1", combo2 = "9,100,0"},
   { skill_index = 21212, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "10,100,0" },
   { skill_index = 21211, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "11,100,0" },
   { skill_index = 21212, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "12,100,0" },
   { skill_index = 21210, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "18,100,1", combo2 = "13,100,0" },
   { skill_index = 21212, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "14,100,0" },
   { skill_index = 21210, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "18,100,1", combo2 = "15,100,0" },
   { skill_index = 21212, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "16,100,0" },
   { skill_index = 21210, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "18,100,1", combo2 = "17,100,0" },
   { skill_index = 21212, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, combo1 = "18,100,0" },
   { skill_index = 21213, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
}
