-- Academic_Alfredo

g_Lua_NearTableCount = 3;

g_Lua_NearValue1 = 1000;
g_Lua_NearValue2 = 2000;
g_Lua_NearValue3 = 3000;


g_Lua_NoAggroOwnerFollow=1;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_GlobalCoolTime1 = 10000
g_Lua_GlobalCoolTime2 = 20000
g_Lua_GlobalCoolTime3 = 30000
g_Lua_GlobalCoolTime4 = 24000


g_Lua_Near1 = 
{
     { action_name = "Stand", rate = 10, loop = 1 },
	 { action_name = "Move_Front", rate = 15, loop = 1 },
	 { action_name = "Move_Left", rate = 15, loop = 1 },
	 { action_name = "Move_Right", rate = 15, loop = 1 },
	 { action_name = "Move_NonTargetFront", rate = 20, loop = 1 },
}
g_Lua_Near2 = 
{
     { action_name = "Stand", rate = 10, loop = 1 }, 
	 { action_name = "Move_Front", rate = 15, loop = 1 },
	 { action_name = "Move_Left", rate = 15, loop = 1 },
	 { action_name = "Move_Right", rate = 15, loop = 1 },
	 { action_name = "Move_NonTargetFront", rate = 20, loop = 1 },
}
g_Lua_Near3 = 
{
     { action_name = "Stand", rate = 10, loop = 1 }, 
	 { action_name = "Move_Front", rate = 15, loop = 1 },
	 { action_name = "Move_Left", rate = 15, loop = 1 },
	 { action_name = "Move_Right", rate = 15, loop = 1 },
	 { action_name = "Move_NonTargetFront", rate = 20, loop = 1 },
}				

g_Lua_Skill = { 
   { skill_index = 21228, cooltime = 10000, rate = 50, rangemin = 0, rangemax = 5000, target = 1 },
   { skill_index = 21218, cooltime = 15000, rate = 50, rangemin = 0, rangemax = 5000, target = 3, encountertime = 4000, globalcooltime = 1, randomtarget = "1.6,0,1" },
   { skill_index = 21229, cooltime = 15000, rate = 50, rangemin = 0, rangemax = 5000, target = 3, encountertime = 4000, globalcooltime = 1, randomtarget = "1.6,0,1" },
}
