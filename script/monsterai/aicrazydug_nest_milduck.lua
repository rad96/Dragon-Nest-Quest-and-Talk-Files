g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200 --몬스터가 돌격시에 타겟에게 다가가는 최소거리
g_Lua_AssualtTime = 2000 --Assault 명령을 받고 돌격 할 때 타겟을 쫓아가는 시간


g_Lua_Near1 = { 
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Attack1_Bite", rate = 80, loop = 1  }, --돌격 하거나
   { action_name = "Assault", rate = 5, loop = 1  }, --돌격 하거나
}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 50, loop = 1  },
   { action_name = "Assault", rate = 150, loop = 1  }, --돌격 하거나
}

g_Lua_Near3 = { --너무 멀면 대기
   { action_name = "Stand", rate = 80, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
}

g_Lua_Assault = { { lua_skill_index = 0, rate = 10, approach = 1500},} --Attack01_RedDNest_Bash

g_Lua_Skill = --밀덕 캐넌
{ 
   { skill_index = 36803,  cooltime = 5000, rate = 1, rangemin = 300, rangemax = 2000, target = 3 }, 
}

--스킬 파라미터
--priority= <--스킬 타임이 다시 왔을때 이값이 높은 스킬이 우선적으로 실행된다.
--encounter = 몬스터와 조우하고 이 시간이 흐른뒤에 스킬 사용