
g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 0; -- A.i�� _back�� ��Ʈ���ϳ����� ���ܽ�Ų��. �ɸ��Ϳ� �������� �ڷ� �̵����� ���ϰ�..
g_Lua_AssualtTime = 3000;

g_Lua_State =  
{ 
 State1 = {"!Stay|!Move|!Stiff|!Attack|!Air", "Down" },
 State2 = {"!Air", "!Down" },
 State3 = {"Down|Air","!Move"}, 
 State4 = {"Attack"}, 
 State5 = {"!Move","Air"},
}

g_Lua_CustomAction = {
-- �뽬����
  CustomAction1 = {
     { "Skill_Dash" },
     { "Skill_DashSlash" },
  },
-- ��������������
  CustomAction2 = {
     { "Tumble_Left" },
  },
-- ��������������
  CustomAction3 = {
     { "Tumble_Right" },
  },
-- �⺻ ���� 2��Ÿ 145������
  CustomAction4 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
  },
-- �⺻ ���� 3��Ÿ 232������
  CustomAction5 = {
     { "Attack1_Sword" },
     { "Attack2_Sword" },
     { "Attack3_Sword" },
  },
-- ��Ÿ ����
  CustomAction6 = {
     { "useskill", lua_skill_index = 17, rate = 100 },
  },
  -- �������� ���� �ļ�Ÿ
  CustomAction7 = {
     { "useskill", lua_skill_index = 11, rate = 100 },
  },
  CustomAction8 = {
     {  "useskill", lua_skill_index = 10, rate = 100 },
  },
}

g_Lua_GlobalCoolTime1 = 2600
g_Lua_GlobalCoolTime2 = 10000

g_Lua_Near1 = 
{ 
     { action_name = "Attack_Down", rate = -1, loop = 1, cooltime = 23000, target_condition = "State1" },
	 { action_name = "Move_Left", rate = 5, loop = 1, cooltime = 5000 },
     { action_name = "Move_Right", rate = 5, loop = 1, cooltime = 5000 },
	 { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4"},
	 { action_name = "Tumble_Back", rate = 15, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Left", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Right", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 --{ action_name = "CustomAction6", rate = 100, loop = 1, target_condition = "State5" },
     --{ action_name = "CustomAction8", rate = 100, loop = 1, cooltime = 2000, target_condition = "State1" },
--1Ÿ
     --{ action_name = "Attack1_Sword", rate = 10, loop = 1, cooltime = 7000, globalcooltime = 1 },
--2Ÿ
     --{ action_name = "CustomAction4", rate = 7, loop = 1, cooltime = 9000, globalcooltime = 1 },
--3Ÿ
     --{ action_name = "CustomAction5", rate = 30, loop = 1, cooltime = 10000, globalcooltime = 1, target_condition = "State3" },
--4Ÿ
     --{ action_name = "CustomAction6", rate = 2, loop = 1, cooltime = 3000, globalcooltime = 1 },
}
g_Lua_Near2 = 
{ 
{ action_name = "Tumble_Front", rate = 5, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4"},
	 { action_name = "Tumble_Back", rate = 15, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Left", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Right", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 --{ action_name = "CustomAction2", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
	 --{ action_name = "CustomAction3", rate = 2, loop = 1, cooltime = 10000,  target_condition = "State4"  },
}
g_Lua_Near3 = 
{ 
     { action_name = "Move_Left", rate = 5, loop = 3 },
     { action_name = "Move_Right", rate = 5, loop = 3 },
    { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4"},
	 { action_name = "Tumble_Back", rate = 15, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Left", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Right", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
     --{ action_name = "Assault",  rate = 5, loop = 1 },
}
g_Lua_Near4 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
	 { action_name = "Move_Left", rate = 10, loop = 3 },
     { action_name = "Move_Right", rate = 10, loop = 3 },
	 { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4"},
	 { action_name = "Tumble_Back", rate = 15, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Left", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Right", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
     --{ action_name = "Assault",  rate = 5,  loop = 1 },
}
g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 2 },
	 { action_name = "Move_Left", rate = 10, loop = 3 },
     { action_name = "Move_Right", rate = 10, loop = 3 },
	 { action_name = "Tumble_Front", rate = 10, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4"},
	 { action_name = "Tumble_Back", rate = 15, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Left", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
	 { action_name = "Tumble_Right", rate = 20, loop = 1, cooltime = 2600, globalcooltime=1, target_condition = "State4" },
}

g_Lua_Assault = { 
     --{ action_name = "", rate = 30, loop = 1, approach = 300 },
     { action_name = "", rate = 10, loop = 1, approach = 150 },
}

g_Lua_NonDownMeleeDamage = {
     { action_name = "Tumble_Front", rate = 60, loop = 1, cooltime = 2600, globalcooltime=1 },
     --{ action_name = "Move_Back", rate = 10, loop = 1 },
	 { action_name = "Skill_FakeShot", rate = 30, loop = 1, cooltime = 15000},
     { action_name = "CustomAction2", rate = 10, loop = 1 , globalcooltime=1 },
     { action_name = "CustomAction3", rate = 10, loop = 1 , globalcooltime=1},
}

g_Lua_NonDownRangeDamage = {
     --{ action_name = "Assault", rate = 15, loop = 1 },
	 { action_name = "useskill", lua_skill_index = 7, rate = 50 },
	 { action_name = "useskill", lua_skill_index = 16, rate = 50 },
	 { action_name = "Tumble_Left", rate = 10, cooltime = 2600, globalcooltime=1 },
     { action_name = "Tumble_Right", rate = 10, cooltime = 2600, globalcooltime=1},
}

g_Lua_RandomSkill ={
{
{ -- Ʈ���̾ޱۼ� ���� �׽�Ʈ
   { skill_index = 34211, rate= 20 }, 
   { skill_index = 34212, rate= 20 },
   { skill_index = 34213, rate= 15 },
   { skill_index = 34214, rate= 15 },
   { skill_index = 34215, rate= 15 },
   { skill_index = 34216, rate= 15 },
},
}
}

g_Lua_Skill = { 

-- 0Ʈ�� ��
     { skill_index = 82504, cooltime = 4500, rate = 0, rangemin = 200, rangemax = 1000, target = 3, combo1 = "9,50,1" , combo2 = "10,70,0", combo3 = "8,100,1" }, 
-- 1��Ƽ �� 
	 { skill_index = 82505, cooltime = 28000, rate = 50, rangemin = 0, rangemax = 300, target = 3, combo1 = "14,100,1" },
-- 2���ο� ����ű
     { skill_index = 82506, cooltime = 12000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "3,60,1", combo2 = "14,100,1" },
-- 3���� ű
     { skill_index = 82510, cooltime = 0, rate = 100, rangemin = 0, rangemax = 130, target = 3, cancellook = 1, combo1 = "2,50,1", combo2 = "10,70,1", combo3 = "20,100,1" ,combo4 = "4,100,0", target_condition = "State5	"}, 
-- 4������_��
     { skill_index = 82508, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 1000, target = 3, combo1 ="18,90, 0", globalcooltime=1  },
-- 5����Ʈ ű
     { skill_index = 82509, cooltime = 6000, rate = -1, rangemin = 0, rangemax = 300, target = 3, combo1 = "2,100,1"  },
-- 6�ο� ����ű
     { skill_index = 82512, cooltime = 3000, rate = 100, rangemin = 0, rangemax = 150, target = 3, combo1 = "2,50,1", combo2 = "7,100,1",  target_condition = "State1" },
-- 7���� ����ű
     { skill_index = 82518, cooltime = 0, rate = -1, rangemin = 0, rangemax = 600, target = 3, combo1 = "1,60,1" ,combo2 = "9,100,0"},
	 
-- 8�Ǿ�� ��
     { skill_index = 82516, cooltime = 10000, rate = 5, rangemin = 300, rangemax = 1000, target = 3, combo1 = "9,60,1", combo2 = "10,100,1", combo3 = "4,100,0" },
-- 9���� �ַο�
     { skill_index = 82517, cooltime = 13000, rate = 20, rangemin = 0, rangemax = 1000, target = 3, combo1 = "1,100,0",combo2 = "0,100,0", target_condition = "State3"  },
-- 10���� ��
     { skill_index = 82520, cooltime = 15000, rate = 30, rangemin = 0, rangemax = 1000, target = 3, combo1 = "2,50,1", combo2 = "14,100,1", target_condition = "State3"},	 	 

-- 11�ַο� ����
     { skill_index = 82526, cooltime = 45000, rate = 20, rangemin = 700, rangemax = 1000, target = 3, cancellook = 1 ,  priority= 3 },--blowcheck = "70"

-- 12 Ʈ���� �ޱ� ��
     { skill_index = 82548, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, globalcooltime=2},
-- 13 ����Ľ�
     { skill_index = 82535, cooltime = 45000, rate = 10, rangemin = 200, rangemax = 1200, target = 3, cancellook = 1, combo1 = "16,100,0", priority= 2},
-- 14 ��Ŭ ��
     { skill_index = 82536, cooltime = 18000, rate = 20, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "7,50,0",combo3 = "16,70,0",  combo2 = "26,100,1"},
-- 15 ����Ľ�����
     { skill_index = 82545, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3, cancellook = 1, combo1 = "16,100,0" , usedskill = "82535"},
-- 16 ����Ľ�����1
     { skill_index = 82546, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 2000, target = 3, cancellook = 1, combo1 = "15,100,0", usedskill = "82535" },
-- 17 Ʈ���̾ޱۼ�R
     { skill_index = 82534, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "5,50,0", combo2 = "3,100,0" , globalcooltime=2},-- randomskill�ñ׳η� ������ ��ų�� Ȯ������ -1�� �����ؾߵ�
-- 18 Ʈ���̾ޱۼ�L
     { skill_index = 82547, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 900, target = 3, cancellook = 1, combo1 = "5,50,0", combo2 = "3,100,0", globalcooltime=2 },
-- 19 ������R
     { skill_index = 82552, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 400, target = 3 , globalcooltime=1 } ,
-- 20 ������L
     { skill_index = 82553, cooltime = 2600, rate = -1, rangemin = 0, rangemax = 400, target = 3 , globalcooltime=1  },


	 }