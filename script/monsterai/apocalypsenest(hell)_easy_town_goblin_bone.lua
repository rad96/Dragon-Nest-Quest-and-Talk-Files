--AiGoblin_Bone_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 120;
g_Lua_AssualtTime = 5000;

g_Lua_Near1 = { 
   { action_name = "Move_Back", rate = 10, loop = 3  },
   { action_name = "Attack1_Slash", rate = 3, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack7_Nanmu", rate = 4, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Move_Back", rate = 10, loop = 1  },
   { action_name = "Attack4_FastJumpAttack", rate = 4, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Attack8_ThrowStone_Lv2", rate = 3, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 4, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack7_Nanmu", rate = 10, loop = 1, approach = 300.0 },
   { action_name = "Attack1_Slash", rate = 10, loop = 1, approach = 100.0 },
   { action_name = "Move_Back", rate = 10, loop = 3, approach = 200.0 },
}
