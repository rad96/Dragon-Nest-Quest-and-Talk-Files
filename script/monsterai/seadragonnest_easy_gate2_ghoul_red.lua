--AiGhoul_Red_Elite_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 600;
g_Lua_NearValue5 = 2000;
g_Lua_NearValue6 = 5000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Attack1_ArmAttack", rate = 20, loop = 1, selfhppercent = 50  },
   { action_name = "Stand_1", rate = 1, loop = 2  },
   { action_name = "Move_Left", rate = 8, loop = 2, selfhppercentrange = "0,99"   },
   { action_name = "Move_Right", rate = 8, loop = 2, selfhppercentrange = "0,99"  },
   { action_name = "Walk_Back", rate = 15, loop = 1, selfhppercentrange = "99,100"  },
   { action_name = "Walk_Left", rate = 5, loop = 1, selfhppercentrange = "99,100"   },
   { action_name = "Walk_Right", rate = 5, loop = 1, selfhppercentrange = "99,100"  },
   { action_name = "Attack1_ArmAttack", rate = 20, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Attack1_ArmAttack", rate = 20, loop = 1, selfhppercent = 50  },
   { action_name = "Attack2_ArmSlash", rate = 10, loop = 1, selfhppercent = 50 },
   { action_name = "Attack3_DashBite", rate = 12, loop = 1, selfhppercent = 50  },
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Move_Front", rate = 8, loop = 1, selfhppercentrange = "0,99"  },
   { action_name = "Move_Left", rate = 5, loop = 2, selfhppercentrange = "0,99"   },
   { action_name = "Move_Right", rate = 5, loop = 2, selfhppercentrange = "0,99"  },
   { action_name = "Walk_Back", rate = 8, loop = 1, selfhppercentrange = "99,100"  },
   { action_name = "Walk_Front", rate = 8, loop = 1, selfhppercentrange = "99,100"  },
   { action_name = "Walk_Left", rate = 5, loop = 1, selfhppercentrange = "99,100"   },
   { action_name = "Walk_Right", rate = 5, loop = 1, selfhppercentrange = "99,100"  },
   { action_name = "Attack1_ArmAttack", rate = 20, loop = 1  },
   { action_name = "Attack2_ArmSlash", rate = 10, loop = 1  },
   { action_name = "Attack3_DashBite", rate = 12, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1, selfhppercentrange = "0,99"  },
   { action_name = "Move_Left", rate = 8, loop = 2, selfhppercentrange = "0,99"   },
   { action_name = "Move_Right", rate = 8, loop = 2, selfhppercentrange = "0,99"  },
   { action_name = "Walk_Front", rate = 10, loop = 1, selfhppercentrange = "99,100"  },
   { action_name = "Walk_Left", rate = 8, loop = 1, selfhppercentrange = "99,100"   },
   { action_name = "Walk_Right", rate = 8, loop = 1, selfhppercentrange = "99,100"  },
   { action_name = "Attack2_ArmSlash", rate = 20, loop = 1  },
   { action_name = "Attack3_DashBite", rate = 15, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 40, loop = 1, selfhppercentrange = "0,99"  },
   { action_name = "Move_Left", rate = 2, loop = 2, selfhppercentrange = "0,99"   },
   { action_name = "Move_Right", rate = 2, loop = 2, selfhppercentrange = "0,99"  },
   { action_name = "Walk_Front", rate = 15, loop = 1, selfhppercentrange = "99,100"  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 40, loop = 2, selfhppercentrange = "0,99"  },
   { action_name = "Move_Left", rate = 2, loop = 2, selfhppercentrange = "0,99"   },
   { action_name = "Move_Right", rate = 2, loop = 2, selfhppercentrange = "0,99"  },
   { action_name = "Walk_Front", rate = 15, loop = 1, selfhppercentrange = "99,100"  },
}
g_Lua_Near6 = { 
   { action_name = "Move_Front", rate = 40, loop = 3, selfhppercentrange = "0,99"  },
   { action_name = "Move_Left", rate = 2, loop = 2, selfhppercentrange = "0,99"   },
   { action_name = "Move_Right", rate = 2, loop = 2, selfhppercentrange = "0,99"  },
   { action_name = "Walk_Front", rate = 15, loop = 1, selfhppercentrange = "99,100"  },
}

g_Lua_Skill = {
   { skill_index = 20022,  cooltime = 10000, rate = 100,rangemin = 100, rangemax = 800, target = 3, selfhppercent = 20 },
}