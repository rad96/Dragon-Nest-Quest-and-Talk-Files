--AiGoblin_Green_Elite_Easy.lua

g_Lua_NearTableCount = 7

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 200;
g_Lua_NearValue3 = 350;
g_Lua_NearValue4 = 550;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;
g_Lua_NearValue7 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
  State2 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Walk_Back", 2 },
      { "Attack2_Upper", 1 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 45, loop = 2  },
   { action_name = "Move_Back", rate = 45, loop = 2  },
   { action_name = "Attack1_Slash", rate = 3, loop = 1  },
   { action_name = "CustomAction1", rate = -1, loop = 1, target_condition = "State2" },
}
g_Lua_Near2 = { 
   { action_name = "Stand2", rate = 18, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Back", rate = 24, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 2  },
   { action_name = "Attack1_Slash", rate = 3, loop = 1  },
   { action_name = "Attack7_Nanmu", rate = -1, loop = 1, max_missradian = 10  },
   { action_name = "CustomAction1", rate = -1, loop = 1, target_condition = "State2" },
}
g_Lua_Near3 = { 
   { action_name = "Stand2", rate = 24, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Front", rate = 24, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 9, loop = 1  },
   { action_name = "Attack2_Upper", rate = 3, loop = 1  },
   { action_name = "Attack7_Nanmu", rate = -1, loop = 1, max_missradian = 10  },
   { action_name = "CustomAction1", rate = -1, loop = 1, target_condition = "State2" },
}
g_Lua_Near4 = { 
   { action_name = "Stand2", rate = 30, loop = 2  },
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Front", rate = 24, loop = 2  },
   { action_name = "Walk_Back", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 9, loop = 2  },
   { action_name = "Move_Back", rate = 3, loop = 2  },
   { action_name = "Assault", rate = 4, loop = 1  },
   { action_name = "Attack7_Nanmu", rate = -1, loop = 1, max_missradian = 10  },
}
g_Lua_Near5 = { 
   { action_name = "Stand2", rate = 36, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 36, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 2  },
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Stand2", rate = 15, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 3  },
   { action_name = "Walk_Right", rate = 3, loop = 3  },
   { action_name = "Walk_Front", rate = 30, loop = 3  },
   { action_name = "Move_Left", rate = 3, loop = 3  },
   { action_name = "Move_Right", rate = 3, loop = 3  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
   { action_name = "Assault", rate = 3, loop = 1  },
}
g_Lua_Near7 = { 
   { action_name = "Stand2", rate = 15, loop = 2  },
   { action_name = "Walk_Left", rate = 4, loop = 3  },
   { action_name = "Walk_Right", rate = 4, loop = 3  },
   { action_name = "Walk_Front", rate = 30, loop = 3  },
   { action_name = "Move_Left", rate = 4, loop = 3  },
   { action_name = "Move_Right", rate = 4, loop = 3  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Upper", rate = 5, loop = 1 },
   { action_name = "Attack1_Slash", rate = 3, loop = 1 },
}
