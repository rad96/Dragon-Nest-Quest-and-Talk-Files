--AiMinotauros_Red_Boss_Nest.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1300;
g_Lua_NearValue4 = 2000;
g_Lua_NearValue5 = 3500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_SkillProcessor = {
   { skill_index = 30572, changetarget = "1000,0" },
}

g_Lua_ThreatRange = 
{ 
    { ThreatRange = 10000, CognizanceGentleRange = 11000, CognizanceThreatRange = 12000, selfhppercent = 99 },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1, selfhppercent = 99 },
   { action_name = "Walk_Left", rate = 8, loop = 3, selfhppercent = 99 },
   { action_name = "Walk_Right", rate = 8, loop = 3, selfhppercent = 99 },
   { action_name = "Walk_Back", rate = 4, loop = 2, selfhppercent = 99 },
   { action_name = "Attack2_Slash_Red", rate = 4, loop = 1, cooltime = 12000, selfhppercent = 99 },
   { action_name = "Attack1_bash_Red", rate = 4, loop = 1, cooltime = 18000, selfhppercent = 99 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1, selfhppercent = 99 },
   { action_name = "Walk_Left", rate = 8, loop = 3, selfhppercent = 99 },
   { action_name = "Walk_Right", rate = 8, loop = 3, selfhppercent = 99 },
   { action_name = "Walk_Back", rate = 4, loop = 1, selfhppercent = 99 },
   { action_name = "Attack2_Slash_Red", rate = 4, loop = 1, cooltime = 12000, selfhppercent = 99 },
   { action_name = "Attack1_bash_Red", rate = 4, loop = 1, cooltime = 18000, selfhppercent = 99 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1, selfhppercent = 99 },
   { action_name = "Walk_Left", rate = 5, loop = 2, selfhppercent = 99 },
   { action_name = "Walk_Right", rate = 5, loop = 2, selfhppercent = 99 },
   { action_name = "Walk_Front", rate = 10, loop = 2, selfhppercent = 99 },
   { action_name = "Walk_Back", rate = 3, loop = 1, selfhppercent = 99 },
   { action_name = "Move_Left", rate = 10, loop = 1, selfhppercent = 99 },
   { action_name = "Move_Right", rate = 10, loop = 1, selfhppercent = 99 },
   { action_name = "Move_Front", rate = 35, loop = 1, selfhppercent = 99 },
   { action_name = "Assault", rate = 10, loop = 1, selfhppercent = 99 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1, selfhppercent = 99 },
   { action_name = "Walk_Left", rate = 10, loop = 1, selfhppercent = 99 },
   { action_name = "Walk_Right", rate = 10, loop = 1, selfhppercent = 99 },
   { action_name = "Walk_Front", rate = 10, loop = 1, selfhppercent = 99 },
   { action_name = "Walk_Back", rate = 5, loop = 1, selfhppercent = 99 },
   { action_name = "Move_Front", rate = 35, loop = 1, selfhppercent = 99 },
   { action_name = "Assault", rate = 10, loop = 1, selfhppercent = 99 },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 2, loop = 1, selfhppercent = 99 },
   { action_name = "Walk_Left", rate = 3, loop = 1, selfhppercent = 99 },
   { action_name = "Walk_Right", rate = 3, loop = 1, selfhppercent = 99 },
   { action_name = "Walk_Front", rate = 5, loop = 1, selfhppercent = 99 },
   { action_name = "Move_Left", rate = 7, loop = 1, selfhppercent = 99 },
   { action_name = "Move_Right", rate = 7, loop = 1, selfhppercent = 99 },
   { action_name = "Move_Front", rate = 25, loop = 1, selfhppercent = 99 },
   { action_name = "Assault", rate = 50, loop = 1, selfhppercent = 99 },
}
g_Lua_Assault = { 
   { action_name = "Attack4_Pushed_Red", rate = 8, loop = 1, approach = 250.0 },
}

g_Lua_Skill = { 
--조건스킬
   { skill_index = 30571,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,4" },
--Path Find
-- 카우 스트라이크
   { skill_index = 30570, cooltime = 20000, rate = 100, rangemin = 0, rangemax = 20000, target = 3, selfhppercentrange = "99,100", anymultipletarget = 1 },

--1Phase
-- 진동(Force Stun)
   { skill_index = 20333, cooltime = 30000, rate = 80, rangemin = 100, rangemax = 1300, target = 3, selfhppercentrange = "75,99" },
-- 도끼 던지기
   { skill_index = 30572, cooltime = 34000, rate = 80, rangemin = 600, rangemax = 1500, target = 3, selfhppercentrange = "75,99"  },

--2Phase
-- 진동
   { skill_index = 20333, cooltime = 50000, rate = 40, rangemin = 100, rangemax = 1300, target = 3, selfhppercentrange = "50,75" },
-- 도끼 던지기
   { skill_index = 30572, cooltime = 40000, rate = 80, rangemin = 600, rangemax = 1500, target = 3, selfhppercentrange = "50,75"  },
-- 빅배쉬
   { skill_index = 20336, cooltime = 40000, rate = 65, rangemin = 200, rangemax = 2500, target = 3, selfhppercentrange = "50,75" },

--3Phase
-- 진동
   { skill_index = 20333, cooltime = 40000, rate = 40, rangemin = 100, rangemax = 1300, target = 3, selfhppercentrange = "25,50" },
-- 댄스
   { skill_index = 30574,  next_lua_skill_index=0, cooltime = 60000, rate = 80, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "25,50" },
-- 도끼 던지기
   { skill_index = 30572, cooltime = 40000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "25,50"  },

--4Phase
-- 진동
   { skill_index = 20333, cooltime = 70000, rate = 40, rangemin = 100, rangemax = 1300, target = 3, selfhppercentrange = "0,25" },
-- 댄스
   { skill_index = 30574,  next_lua_skill_index=0, cooltime = 70000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,25" },
-- 도끼 던지기
   { skill_index = 30572, cooltime = 40000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "0,25"  },
-- 카우 스트라이크
   { skill_index = 30570, cooltime = 30000, rate = 100, rangemin = 0, rangemax = 8000, target = 3, selfhppercentrange = "0,15", anymultipletarget = "1,4" },
}