

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 120;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}


g_Lua_Near1 = 
{ 
-- 0 ~ 120 :
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Attack_SideKick",	rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Back",		rate = 30,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
}
g_Lua_Near2 = 
{ 
-- 120 ~ 500 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Back",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near3 = 
{ 
-- 500 ~ 800 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Assault", 		rate = 10, 		loop = 1,	selfhppercentrange = "0,70" },
}

g_Lua_Near4 = 
{ 
-- 800 ~ 1200 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Assault", 		rate = 20, 		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near5 = 
{ 
-- 1200 ~ 10000 : 
	{ action_name = "Stand",		rate = 8,		loop = 2,	selfhppercentrange = "0,100" },
	{ action_name = "Assault", 		rate = 30, 		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },

}

g_Lua_Assault = { 
   { action_name = "Skill_DashUpper", rate = 30, loop = 1, approach = 300   },
}

g_Lua_Skill = { 
-- ������ ����
   { skill_index = 31121,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "RB,BR,BL,LB"  },
-- ������ ����
   { skill_index = 31122,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FL,FR,LF,RF"  },
-- ������ ������
   { skill_index = 31123,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FL,LF,RB,BR,BL"  },
-- ������ ����
   { skill_index = 31124,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FR,RF,RB,BR,BL"  },
-- ����Ʈ��ġ
   { skill_index = 31001,  cooltime = 6000, rate = 100, rangemin = 0, rangemax = 200,target = 3  },
-- ��񽽷���
   { skill_index = 31002,  cooltime = 10000, rate = 100, rangemin = 200, rangemax = 600,target = 3  },
-- ��Ŭ�극��ũ
   { skill_index = 31004,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 800,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- ����Ʈ���̺�
   { skill_index = 31003,  cooltime = 8000, rate = 100, rangemin = 300, rangemax = 1000,target = 3, selfhppercentrange = "0,90"  },

-- ������
   { skill_index = 31021,  cooltime = 18000, rate = 100, rangemin = 0, rangemax = 800,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "0,80" },
-- �ö��׽���
   { skill_index = 31022,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 500,target = 3  },
-- ������
   { skill_index = 31023,  cooltime = 40000, rate = 100, rangemin = 0, rangemax = 500,target = 3, selfhppercentrange = "0,70" },
-- �۴Ͻ̽���
   { skill_index = 31024,  cooltime = 14000, rate = 100, rangemin = 300, rangemax = 1000, target = 3  },
-- ���̾�Ų
   { skill_index = 31025,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 1200,target = 1, td = "FL,FR,LF,RF,RB,BR,BL,LB"  },
}