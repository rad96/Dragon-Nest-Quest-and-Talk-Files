--AiGhoul_Gray_Boss_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
}
g_Lua_Skill = { 
  -- 블루 구울 소환
  { skill_index = 31812,  cooltime = 25000, rate = 100, rangemin = 0, rangemax = 3000, target = 3 },
  { skill_index = 31815,  cooltime = 25000, rate = 100, rangemin = 0, rangemax = 3000, target = 3 },
}