
g_Lua_NearTableCount = 5;

g_Lua_NearValue1 = 600.0;
g_Lua_NearValue2 = 1000.0;
g_Lua_NearValue3 = 1500.0;
g_Lua_NearValue4 = 2000.0;
g_Lua_NearValue5 = 3500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 500;
g_Lua_AssualtTime = 5000;

g_Lua_SkillProcessor = {
     { skill_index = 30207,  changetarget = "3000,1"  },
}

g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1, td = "FL,RF" },
     { action_name = "Walk_Left", rate = 20, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Walk_Right", rate = 20, loop = 1, td = "FL,FR,LF,RF" },
     { action_name = "Attack018_Bite", rate = 5, loop = 1, td = "FL,FR" },
     { action_name = "Attack020_Jump", rate = 20, loop = 3, td = "FL,FR", cooltime = 15000 },
}

g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 5, loop = 1, td = "FL,RF", selfhppercentrange = "80,100" },
     { action_name = "Move_Front", rate = 8, loop = 1, td = "FL,FR,LF,RF", selfhppercentrange = "80,100" },
}

g_Lua_Near3 = 
{ 
     { action_name = "Move_Front", rate = 20, loop = 1, td = "FL,FR,LF,RF", selfhppercentrange = "80,100" },
}

g_Lua_Near4 = 
{ 
     { action_name = "Move_Front", rate = 10, loop = 1, td = "FL,FR,LF,RF", selfhppercent = 20 },
}

g_Lua_Near5 = 
{ 
     { action_name = "Move_Front", rate = 10, loop = 1, td = "FL,FR,LF,RF", selfhppercent = 20 },
}