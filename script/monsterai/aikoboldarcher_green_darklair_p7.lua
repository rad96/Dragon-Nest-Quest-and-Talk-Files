--AiKoboldArcher_Green_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000
g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack1", 0 },
      { "Walk_Back", 1 },
  },
  CustomAction2 = {
      { "Attack4_SnareTrap", 0 },
      { "Move_Back", 1 },
  },
  CustomAction3 = {
      { "Attack4_SnareTrap", 0 },
      { "Walk_Front", 1 },
  },
  CustomAction4 = {
      { "Attack4_SnareTrap", 0 },
      { "Move_Front", 1 },  
  },
  CustomAction5 = {
      { "Attack4_SnareTrap", 0 },
      { "Walk_Left", 1 },
  },
  CustomAction6 = {
      { "Attack2_MultiShot", 0 },
      { "Move_Left", 1 },
  },
  CustomAction7 = {
      { "Attack1", 0 },
      { "Walk_Right", 1 },
  },
  CustomAction8 = {
      { "Attack2_MultiShot", 0 },
      { "Move_Right", 1 },
  },

}
g_Lua_Near1 = { 
   { action_name = "CustomAction1", rate = 5, loop = 1  },
   { action_name = "CustomAction3", rate = 5, loop = 1  },
   { action_name = "CustomAction5", rate = 5, loop = 1  },
   { action_name = "CustomAction7", rate = 5, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "CustomAction1", rate = 5, loop = 1  },
   { action_name = "CustomAction3", rate = 5, loop = 1  },
   { action_name = "CustomAction5", rate = 5, loop = 1  },
   { action_name = "CustomAction7", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "CustomAction1", rate = 5, loop = 1  },
   { action_name = "CustomAction3", rate = 5, loop = 1  },
   { action_name = "CustomAction5", rate = 5, loop = 1  },
   { action_name = "CustomAction7", rate = 5, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "CustomAction1", rate = 5, loop = 1  },
   { action_name = "CustomAction3", rate = 5, loop = 1  },
   { action_name = "CustomAction5", rate = 5, loop = 1  },
   { action_name = "CustomAction7", rate = 5, loop = 1  },
}
