--AiLamia_Red_Normal.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1300;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack01_TailAttack", 0 },
      { "Walk_Back", 2 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 16, loop = 2 },
   { action_name = "Attack01_TailAttack", rate = 6, loop = 1 },
   { action_name = "CustomAction1", rate = 4, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Back", rate = 10, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 2 },
   { action_name = "Walk_Right", rate = 6, loop = 2 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 2 },
}
g_Lua_Skill = { 
   { skill_index = 35206,  cooltime = 46000, rate = 60,rangemin = 0, rangemax = 600, target = 3 },
   { skill_index = 35203,  cooltime = 12000, rate = 80, rangemin = 0, rangemax = 600, target = 3 },
   { skill_index = 35204,  cooltime = 38000, rate = 80, rangemin = 600, rangemax = 1200, target = 3 },
   { skill_index = 35205,  cooltime = 46000, rate = 60, rangemin = 400, rangemax = 1000, target = 3, selfhppercent = 50 },
   { skill_index = 35207,  cooltime = 93000, rate = 70, rangemin = 0, rangemax = 3000, target = 1, selfhppercent = 20, limitcount = 1 },
}
