g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 1200;
g_Lua_NearValue3 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200 --몬스터가 돌격시에 타겟에게 다가가는 최소거리
g_Lua_AssualtTime = 2000 --Assault 명령을 받고 돌격 할 때 타겟을 쫓아가는 시간


g_Lua_Near1 = { 
   { action_name = "Stand", rate = 2, loop = 1  },
   { action_name = "Move_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
   { action_name = "Attack1_Bite", rate = 5, loop = 1	},
   { action_name = "Attack14_Summon_MechBomb", rate = 15, loop = 1, cooltime = 30000, selfhppercent = 50},--메카봄덕 소환
}

g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Assault", rate = 10, loop = 1  }, --돌격 하거나
   { action_name = "Move_Back", rate = 5, loop = 3  }, --뒤로 거리 벌리거나
   { action_name = "Move_Left", rate = 2, loop = 3  },
   { action_name = "Move_Right", rate = 2, loop = 3  },
   { action_name = "Attack14_Summon_MechBomb", rate = 10, loop = 1, cooltime = 30000, selfhppercent = 100},--메카봄덕 소환
}

g_Lua_Near3 = { 
   { action_name = "Stand", rate = 50, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 3  },
   { action_name = "Move_Right", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 10, loop = 1  },
   { action_name = "Attack14_Summon_MechBomb", rate = 50, loop = 1, cooltime = 30000, selfhppercent = 100},--메카봄덕 소환
}

--[[ g_Lua_Assault = { 
   { action_name = "Attack1_Bite", rate = 1, loop = 1  },
}
--]]
g_Lua_Assault = { { lua_skill_index = 0, rate = 10, approach = 1500},} --Attack01_RedDNest_Bash

g_Lua_Skill = { 
   { skill_index = 36803,  cooltime = 5000, rate = 1, loop = 1, rangemin = 300, rangemax = 2000, target = 3, selfhppercent = 100 }, 
   { skill_index = 36803,  cooltime = 5000, rate = 1, loop = 5, rangemin = 300, rangemax = 2000, target = 3, selfhppercent = 50 }, 
   { skill_index = 36803,  cooltime = 3000, rate = 1, loop = 10, rangemin = 300, rangemax = 2000, target = 3, selfhppercent = 10 }, 
}

--스킬 파라미터
--priority= <--스킬 타임이 다시 왔을때 이값이 높은 스킬이 우선적으로 실행된다.
--encounter = 몬스터와 조우하고 이 시간이 흐른뒤에 스킬 사용
