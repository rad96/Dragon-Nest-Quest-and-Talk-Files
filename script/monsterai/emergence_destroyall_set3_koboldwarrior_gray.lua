--AiKoboldWarrior_Gray_Normal.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 50;
g_Lua_NearValue2 = 150;
g_Lua_NearValue3 = 200;
g_Lua_NearValue4 = 300;
g_Lua_NearValue5 = 800;
g_Lua_NearValue6 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000


g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 2  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 18, loop = 2  },
   { action_name = "Move_Back", rate = 18, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 4, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 1, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Move_Right", rate = 1, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
}

g_Lua_Skill = { 
   { skill_index = 20031,  cooltime = 15000, rate = 100,rangemin = 0, rangemax = 5000, target = 1, priority= 80, encountertime = 16000 },
   { skill_index = 21246,  cooltime = 15000, rate = 100,rangemin = 0, rangemax = 5000, target = 1,  limitcount = 1, priority= 99 },
}
