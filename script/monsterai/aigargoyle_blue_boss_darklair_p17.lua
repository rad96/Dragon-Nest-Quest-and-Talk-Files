--AiGargoyle_Blue_Boss_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 2  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 16, loop = 2  },
   { action_name = "Move_Left", rate = -1, loop = 1  },
   { action_name = "Move_Right", rate = -1, loop = 1  },
   { action_name = "Move_Back", rate = -1, loop = 1  },
   { action_name = "Attack1_DashAttack_N", rate = 48, loop = 1  },
   { action_name = "Attack3_Combo_N", rate = 13, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 7, loop = 1  },
   { action_name = "Move_Right", rate = 7, loop = 1  },
   { action_name = "Move_Front", rate = 14, loop = 1  },
   { action_name = "Move_Back", rate = 14, loop = 1  },
   { action_name = "Attack1_DashAttack_N", rate = 22, loop = 1  },
   { action_name = "Attack3_Combo_N", rate = 90, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 5, loop = 2  },
   { action_name = "Walk_Left", rate = 5, loop = 3  },
   { action_name = "Walk_Right", rate = 5, loop = 3  },
   { action_name = "Walk_Back", rate = 5, loop = 3  },
   { action_name = "Walk_Front", rate = 5, loop = 3  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Attack1_DashAttack_N", rate = 33, loop = 2  },
   { action_name = "Attack3_Combo_N", rate = 110, loop = 1  },
}
g_Lua_Skill = { 
   { skill_index = 20127,  cooltime = 60000, rate = 100,rangemin = 0, rangemax = 1500, target = 1, selfhppercent = 80 },
   { skill_index = 20120,  cooltime = 10000, rate = 80, rangemin = 0, rangemax = 800, target = 3, selfhppercent = 100 },
   { skill_index = 20121,  cooltime = 12000, rate = 100, rangemin = 0, rangemax = 1000, target = 4, hppercent = 50 },
   { skill_index = 20122,  cooltime = 12000, rate = 100, rangemin = 200, rangemax = 700, target = 3, selfhppercent = 100 },
}

