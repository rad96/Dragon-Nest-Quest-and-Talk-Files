-- /genmon 703279
g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 450;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1600;
g_Lua_NearValue4 = 4000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 450
g_Lua_AssualtTime = 6000
g_Lua_NoAggroStand = 1

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 2, loop=1 },
	{ action_name = "Walk_Left", rate = 4, loop = 1 },
	{ action_name = "Walk_Right", rate = 4, loop = 1 },
	{ action_name = "Attack01_Slash_90lv", cooltime = 15000, rate = 8, loop = 1 },
}

g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 2, loop=1 },
	{ action_name = "Walk_Left", rate = 4, loop = 1 },
	{ action_name = "Walk_Right", rate = 4, loop = 1 },
	{ action_name = "Walk_Front", rate = 6, loop= 1 },
}

g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 2, loop = 1 },
	{ action_name = "Walk_Left", rate = 3, loop = 1 },
	{ action_name = "Walk_Right", rate = 3, loop = 1 },
	{ action_name = "Move_Front", rate = 10, loop= 1 },
}

g_Lua_Near4 = { 
	{ action_name = "Stand", rate = 2, loop = 1 },
	{ action_name = "Walk_Front", rate = 6, loop= 1 },
--	{ action_name = "Assault", rate = 12, loop = 1 },
}

g_Lua_Assault = { 
	{ lua_skill_index = 4, rate = 5, approach = 900 },
}

--GlobalCootime
g_Lua_GlobalCoolTime1 = 50000   -- ���� ��Ÿ��1
g_Lua_GlobalCoolTime2 = 50000   -- ���� �� ���Ľ� ��Ÿ��
g_Lua_GlobalCoolTime3 = 45000   -- ���� �� ĳ�� ��Ÿ��
g_Lua_GlobalCoolTime4 = 35000	-- ���� ��Ÿ��

	
g_Lua_Skill = {

-- next_Lua_skill
	-- 0 : Attack15_GoldLight_Start
	{ skill_index = 500062, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, next_lua_skill_index = 2 },
	-- 1 : Attack15_GoldLight_Start
	{ skill_index = 500062, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 10000, target = 3, next_lua_skill_index = 3 },
	-- 2 : Attack07_Compass_90lv_Start
	{ skill_index = 500057, cooltime = 1000, globalcooltime = 2, rate = -1, rangemin = 0, rangemax = 10000, target = 3 },
	-- 3 : Attack04_Cannon_90lv
	{ skill_index = 500053, cooltime = 1000, globalcooltime = 3, rate = -1, rangemin = 0, rangemax = 10000, target = 3 },
	-- 4 : Attack05_JumpAttack_05M_90lv
	{ skill_index = 500054, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 10000, target = 3 },
	-- 5 : Attack01_Slash_90lv
	{ skill_index = 500050, cooltime = 6000, rate = -1, rangemin = 0, rangemax = 500, target = 3 },
	-- 6 : Attack07_Compass_90lv_Start (not globalcooltime)
	{ skill_index = 500057, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 10000, target = 3 },

-- Ai 
	--Attack02_Shake_90lv
	{ skill_index = 500051, cooltime = 40000, rate = 60, rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "61,100" },
	{ skill_index = 500051, cooltime = 40000, globalcooltime = 2, rate = 160, rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "5,60", next_lua_skill_index = 6 },

	--Attack15_GoldLight_Start
	{ skill_index = 500062, cooltime = 60000, globalcooltime = 1, rate = 120, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR,BR,BL", selfhppercentrange = "61,90", next_lua_skill_index = 2, limitcount = 2 },
	{ skill_index = 500062, cooltime = 60000, globalcooltime = 1, rate = 120, rangemin = 600, rangemax = 2000, target = 3, td = "FL,FR,BR,BL", selfhppercentrange = "61,90", next_lua_skill_index = 3, limitcount = 2 },

	{ skill_index = 500062, cooltime = 60000, globalcooltime = 1, rate = 120, rangemin = 0, rangemax = 800, target = 3, td = "FL,FR,BR,BL", selfhppercentrange = "5,40", next_lua_skill_index = 2, limitcount = 2 },
	{ skill_index = 500062, cooltime = 60000, globalcooltime = 1, rate = 120, rangemin = 600, rangemax = 2000, target = 3, td = "FL,FR,BR,BL", selfhppercentrange = "5,40", next_lua_skill_index = 3, limitcount = 2 },
	
	--Attack04_Cannon_90lv
	{ skill_index = 500053, cooltime = 45000, globalcooltime = 3, rate = 70, rangemin = 600, rangemax = 3000, target = 3, encountertime = 20000 },
	--Attack07_Compass_90lv_Start
	{ skill_index = 500057, cooltime = 50000, globalcooltime = 2, rate = 40, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR", encountertime = 15000 },
	--Attack05_JumpAttack_05M_90lv
	{ skill_index = 500054, cooltime = 35000, globalcooltime = 4, rate = 70, rangemin = 400, rangemax = 800, target = 3, td = "FR,FL", randomtarget = "1.6,0,1"},
	--Attack05_JumpAttack_11M_90lv
	{ skill_index = 500055, cooltime = 35000, globalcooltime = 4, rate = 70, rangemin = 800, rangemax = 1600, target = 3, td = "FR,FL", randomtarget = "1.6,0,1"},
	-- Attack06_Volcano_90lv_Start
	{ skill_index = 500056, cooltime = 55000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,50" },
	--Attack13_Dash_90lv_Start
	{ skill_index = 500059, cooltime = 35000, rate = 70, rangemin = 0, rangemax = 300, target = 3, encountertime = 15000, randomtarget = "1.6,0,1" },

}

