--90lv_LizardSoldier_Antler_Abyss.lua
--리자드맨 솔저 사막 베리

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 1000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 3000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 7, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 2 },
   { action_name = "Walk_Right", rate = 8, loop = 2 },
   { action_name = "Walk_Back", rate = 5, loop = 2 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
   { action_name = "Move_Back", rate = 1, loop = 1 },
   { action_name = "Attack01_Chopping", rate = 3, loop = 1 },
   { action_name = "Attack02_Thrust", rate = 3, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 2, loop = 2 },
   { action_name = "Move_Right", rate = 2, loop = 2 },
   { action_name = "Move_Front", rate = 6, loop = 2 },
   { action_name = "Attack05_Howl", rate = 7, loop = 1, cooltime = 20000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 2 },
   { action_name = "Walk_Right", rate = 3, loop = 2 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
   { action_name = "Assault", rate = 1, loop = 1 },
}

g_Lua_Assault = { 
   { action_name = "Attack01_Chopping", rate = 5, loop = 1, approach = 200 },
}

g_Lua_Skill = { 
   { skill_index = 35168,  cooltime = 30000, rate = 15, rangemin = 50, rangemax = 300, target = 3 }, -- Attack03_Combo
   { skill_index = 35167,  cooltime = 30000, rate = 15, rangemin = 50, rangemax = 300, target = 3 }, -- Attack04_Beating
   { skill_index = 500001,  cooltime = 45000, rate = 25, rangemin = 300, rangemax = 1000, target = 3 }, -- Attack06_Headbutt_Start
}
