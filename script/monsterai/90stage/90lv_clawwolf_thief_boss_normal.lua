--AiClawwolf_Thief_Boss_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 350;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000

g_Lua_Near1 = { 
	{ action_name = "Stand", rate = 10, loop = 1 },
	{ action_name = "Walk_Back", rate = 5, loop = 2 },
	{ action_name = "Walk_Front", rate = 10, loop = 1 },
	{ action_name = "Attack06_Scratch_90lv", cooltime = 13000, rate = 8, loop = 1 },

}
g_Lua_Near2 = { 
	{ action_name = "Stand", rate = 20, loop = 1 },
	{ action_name = "Walk_Back", rate = 5, loop = 2 },
	{ action_name = "Walk_Left", rate = 3, loop = 2 },
	{ action_name = "Walk_Right", rate = 3, loop = 2 },
	{ action_name = "Attack04_Combo_90lv", cooltime = 20000, rate = 8, loop = 1 },	
	{ action_name = "Attack03_TripleSteb_90lv", cooltime = 20000, rate = 8, loop = 1 },	

}
g_Lua_Near3 = { 
	{ action_name = "Stand", rate = 15, loop = 1 },
	{ action_name = "Walk_Left", rate = 3, loop = 2 },
	{ action_name = "Walk_Right", rate = 3, loop = 2 },
	{ action_name = "Walk_Front", rate = 15, loop = 1 },
	{ action_name = "Move_Front", rate = 8, loop = 2 },

}

g_Lua_GlobalCoolTime1 = 40000 -- Ambush Cooltime
g_Lua_GlobalCoolTime2 = 50000 -- Rampage Cooltime
g_Lua_GlobalCoolTime3 = 30000 -- FlameBomb Cooltime
g_Lua_GlobalCoolTime4 = 30000 -- ShiftStep_Back + WindSlash_V2 Cooltime

g_Lua_Skill = { 

-- Next_lua_skill_index
	--0 : Attack12_WindSlash_v2_90lv_Start
	{ skill_index = 500076, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 }, 
	--1 : Attack01_Ambush_90lv
	{ skill_index = 500066, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	--2 : Attack09_Rampage_90lv_Start1
	{ skill_index = 500072, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "0.6,0,1" },
	--3 : Attack13_ShiftStep_Back_90lv
	{ skill_index = 500079, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
	--4 : Attack09_Rampage_90lv_BlueWindPlain_a
	{ skill_index = 500071, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, randomtarget = "0.6,0,1" },
	--5 : Attack09_Rampage_90lv_BlueWindPlain_b
	{ skill_index = 500074, cooltime = 10000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },

-- AI
	-- Attack13_ShiftStep_Left_90lv 
	{ skill_index = 500077, cooltime = 52000, globalcooltime = 2, rate = 30, rangemin = 200, rangemax = 2000, target = 3, selfhppercentrange = "41,60", next_lua_skill_index  = 5 },
	{ skill_index = 500077, cooltime = 65000, globalcooltime = 2, rate = 30, rangemin = 50, rangemax = 2000, target = 3, selfhppercentrange = "0,20", next_lua_skill_index  = 2 },
	-- Attack13_ShiftStep_Right_90lv
	{ skill_index = 500078, cooltime = 52000, globalcooltime = 2, rate = 30, rangemin = 200, rangemax = 2000, target = 3, selfhppercentrange = "41,60", next_lua_skill_index  = 5 },
	{ skill_index = 500078, cooltime = 62000, globalcooltime = 2, rate = 30, rangemin = 50, rangemax = 2000, target = 3, selfhppercentrange = "0,20", rnext_lua_skill_index  = 2 },
	-- Attack13_ShiftStep_Back_90lv
	{ skill_index = 500079, cooltime = 52000, globalcooltime = 4, rate = 30, rangemin = 0, rangemax = 300, target = 3, selfhppercentrange = "0,35", next_lua_skill_index = 0, limitcount = 1 }, 
	{ skill_index = 500079, cooltime = 62000, globalcooltime = 3, rate = 30, rangemin = 0, rangemax = 300, target = 3, selfhppercentrange = "0,30", next_lua_skill_index = 4 }, 
	-- Attack05_DashUpper_90lv_Start
	{ skill_index = 500070, cooltime = 70000, rate = 50, rangemin = 500, rangemax = 2000, encountertime = 60000, target = 3, },
	-- Attack02_FlameBomb_90lv
	{ skill_index = 500067, cooltime = 45000, rate = 50, rangemin = 50, rangemax = 300, target = 3, encountertime = 20000, selfhppercentrange = "71,100" }, 
	{ skill_index = 500067, cooltime = 45000, rate = 50, rangemin = 50, rangemax = 300, target = 3, selfhppercentrange = "0,70", next_lua_skill_index = 3 }, 
	-- Attack09_Rampage_90lv_BlueWindPlain_a
	{ skill_index = 500071, cooltime = 70000, globalcooltime = 3, rate = 30, rangemin = 200, rangemax = 800, target = 3, randomtarget = "0.6,0,1",  encountertime = 30000 },
	--5 : Attack09_Rampage_90lv_BlueWindPlain_b
	{ skill_index = 500074, cooltime = 52000, rate = 40, rangemin = 200, rangemax = 500, selfhppercentrange = "41,90", target = 3 },
	{ skill_index = 500074, cooltime = 52000, rate = 30, rangemin = 200, rangemax = 500, selfhppercentrange = "0,40", target = 3 },

}