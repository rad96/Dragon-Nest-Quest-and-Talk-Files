g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 750;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1650;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssaultTime = 5000
g_Lua_NoAggroStand = 1

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 2 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 2 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Assault", rate = 3, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 2 },
}

g_Lua_Assault = 
{ 
   { lua_skill_index = 1, rate = 10, approach = 300 },
}

g_Lua_GlobalCoolTime1 = 25000
g_Lua_GlobalCoolTime2 = 15000

g_Lua_Skill = {
	--���ǽ�ų--
	{ skill_index = 500083, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 500, target = 3, td = "FR,FL" }, --Attack004_Assault_90lv_Start
	{ skill_index = 500081, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 500, target = 3 }, --Attack002_Bash_90lv
	--�Ϲݽ�ų--
	{ skill_index = 500081, cooltime = 40000, rate = 60, rangemin = 0, rangemax = 450, target = 3 }, --Attack002_Bash_90lv
	{ skill_index = 500082, cooltime = 60000, rate = 80, rangemin = 650, rangemax = 1100, target = 3, }, --Attack003_JumpChop_ChangeTarget_90lv
	{ skill_index = 500083, cooltime = 70000, rate = 80, rangemin = 0, rangemax = 450, target = 3, td = "FR,FL", globalcooltime = 2 }, --Attack004_Assault_90lv_Start
	{ skill_index = 500084, cooltime = 60000, rate = 60, rangemin = 0, rangemax = 1200, target = 3, selfhppercent = 80, globalcooltime = 2, randomtarget= "1.6,0,1" }, --Attack006_Nandong_90lv_Start
	{ skill_index = 500085, cooltime = 80000, rate = 70, rangemin = 0, rangemax = 700, target = 3, combo1 = "1,100,1" }, --ä��_Attack012_Comon_90lv_Start
	{ skill_index = 500086, cooltime = 60000, rate = 70, rangemin = 0, rangemax = 600, target = 3, selfhppercent = 100, globalcooltime = 2, randomtarget= "1.6,0,1" }, --���Ʒ��޺�Attack16_UpDownCombo
	{ skill_index = 500087, cooltime = 90000, rate = 100, rangemin = 0, rangemax = 1200, target = 3, selfhppercent = 80 }, --Į������Attack014_CurseShake_90lv_Start
	{ skill_index = 500088, cooltime = 60000, rate = 60, rangemin = 0, rangemax = 400, target = 3, globalcooltime = 2, selfhppercent = 50}, --3�޺�Attack015_Combo_90lv
	{ skill_index = 500089, cooltime = 85000, rate = 60, rangemin = 0, rangemax = 600, target = 3, globalcooltime = 2, selfhppercent = 50 }, --������ Attack005_Whirlwind_90lv_Start
}