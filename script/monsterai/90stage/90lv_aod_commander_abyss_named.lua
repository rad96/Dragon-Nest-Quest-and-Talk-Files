-- 90lv_AOD_Commander_Abyss.lua

g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 6, loop = 1  },
   { action_name = "Walk_Right", rate = 6, loop = 1  },
   { action_name = "Walk_Back", rate = 6, loop = 1  },
   { action_name = "Move_Back", rate = 6, loop = 1  },
   { action_name = "Attack1_Smash", rate = 40, loop = 1  },
   { action_name = "Attack2_ShieldSlam", rate = 30, loop = 1 },
   { action_name = "Attack4_ShieldCrash", rate = 40, loop = 1 , globalcooltime = 1, selfhppercent = 75  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 1  },
   { action_name = "Attack4_ShieldCrash", rate = 40, loop = 1 , globalcooltime = 1, selfhppercent = 75  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 3  },
}

g_Lua_Assault = { 
  { action_name = "Attack1_Smash", rate = 30, loop = 1, approach = 200  },
}

g_Lua_GlobalCoolTime1 = 30000 
  
g_Lua_Skill = { 
   { skill_index = 500017,  cooltime = 30000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 75 }, -- Attack3_Shockwave_Start
}
