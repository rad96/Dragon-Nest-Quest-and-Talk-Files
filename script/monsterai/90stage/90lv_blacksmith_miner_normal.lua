--90레벨 개방 대장장이 벨린 광부 베리
--90lv_Blacksmith_Miner_Abyss.lua
--몬스터 ID : 603273

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 450;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop=1 },
   { action_name = "Walk_Back", rate = 7, loop=1 },
   { action_name = "Walk_Left", rate = 5, loop=1 },
   { action_name = "Walk_Right", rate = 5, loop=1 },
   { action_name = "Attack02_Bash", rate = 6, loop=1 },
   { action_name = "Attack11_TwiceChop", rate = 5, loop=1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 3, loop=1 },
   { action_name = "Walk_Front", rate = 10, loop=1 },
   { action_name = "Walk_Back", rate = 4, loop=1 },
   { action_name = "Walk_Left", rate = 5, loop=1 },
   { action_name = "Walk_Right", rate = 5, loop=1 },
   { action_name = "Attack10_Nanmu1", rate = 3, loop=1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop=1 },
   { action_name = "Stand_1", rate = 1, loop=1 },
   { action_name = "Stand_2", rate = 1, loop=1 },
   { action_name = "Walk_Front", rate = 8, loop=2 },
   { action_name = "Walk_Left", rate = 4, loop=1 },
   { action_name = "Walk_Right", rate = 4, loop=1 },
   { action_name = "Move_Front", rate = 6, loop=1 },
   { action_name = "Attack9_BigBomb", rate = 2, loop=1 },
}
g_Lua_Near4 = { 
   { action_name = "Move_Front", rate = 15, loop=1 },
}