-- 격전의 황무지 BOSS 아누비스 /genmon 703226

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 750;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1650;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssaultTime = 5000
g_Lua_NoAggroStand = 1

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Walk_Front", rate = 2, loop = 1 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 2 },
   { action_name = "Move_Front", rate = 6, loop = 2 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 1 },
   { action_name = "Move_Right", rate = 3, loop = 1 },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Assault", rate = 10, loop = 1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 2 },
   { action_name = "Assault", rate = 10, loop = 1 },
}

g_Lua_Assault = 
{ 
   { lua_skill_index = 0, rate = 10, approach = 100 },
}

g_Lua_GlobalCoolTime1 = 35000
g_Lua_GlobalCoolTime2 = 20000

g_Lua_Skill = {
	--조건스킬--
	{ skill_index = 500030, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, }, --Attack01_Bash-------------0
	{ skill_index = 500031, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3 }, --Attack02_ShortMagic----------------1
	{ skill_index = 500032, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, globalcooltime = 2 }, --Attack03_ChargeMagic_Start------------2
	{ skill_index = 500033, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, globalcooltime = 2 }, --Attack04_Bangground------------------3
	{ skill_index = 500034, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, globalcooltime = 2}, --Attack05_Mesocyclone-------------------4
	{ skill_index = 500036, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, globalcooltime = 2 }, --Attack03_ChargeMagic_Start------------5
	--일반스킬--
	{ skill_index = 500030, cooltime = 15000, rate = 70, rangemin = 0, rangemax = 400, target = 3, td = "FR,FL,LF,RF" }, --Attack01_Bash
	{ skill_index = 500031, cooltime = 50000, rate = 60, rangemin = 0, rangemax = 2000, target = 3, multipletarget = "2,4,0,1", selfhppercent = 50, }, --Attack02_ShortMagic
	{ skill_index = 500032, cooltime = 65000, rate = 70, rangemin = 0, rangemax = 800, target = 3, globalcooltime = 2, randomtarget = "1.6,0,1", selfhppercent = 80 }, --Attack03_ChargeMagic_Start
	{ skill_index = 500036, cooltime = 65000, rate = 70, rangemin = 0, rangemax = 1000, target = 3, globalcooltime = 2, selfhppercent = 60}, --Attack03_ChargeMagic_Fix_Start
	{ skill_index = 500033, cooltime = 70000, rate = 70, rangemin = 0, rangemax = 700, target = 3, globalcooltime = 2 }, --Attack04_Bangground
	{ skill_index = 500034, cooltime = 70000, rate = 80, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 60, globalcooltime = 2}, --Attack05_Mesocyclone
	--순간이동분기-
	{ skill_index = 500035, cooltime = 50000, rate = 90, rangemin = 0, rangemax = 1500, target = 3, next_lua_skill_index = 0, selfhppercent = 80 }, --Attack06_Teleport_Start > Bash
	{ skill_index = 500035, cooltime = 50000, rate = 90, rangemin = 0, rangemax = 1500, target = 3, next_lua_skill_index = 2, selfhppercent = 80 }, --Attack06_Teleport_Start > ChargeMagic
	{ skill_index = 500035, cooltime = 50000, rate = 90, rangemin = 0, rangemax = 1500, target = 3, next_lua_skill_index = 3, selfhppercent = 80 }, --Attack06_Teleport_Start > Bangground
	{ skill_index = 500035, cooltime = 50000, rate = 90, rangemin = 0, rangemax = 1500, target = 3, next_lua_skill_index = 4, selfhppercent = 60 }, --Attack06_Teleport_Start > Mesocyclone
	{ skill_index = 500035, cooltime = 50000, rate = 90, rangemin = 0, rangemax = 1500, target = 3, next_lua_skill_index = 5, selfhppercent = 60 }, --Attack06_Teleport_Start > ChargeMagic_Fix
	--테스트용
	-- { skill_index = 500086, cooltime = 7000, rate = 80, rangemin = 0, rangemax = 1000, target = 3, td = "FR,FL", next_lua_skill_index = 4 }, 
}