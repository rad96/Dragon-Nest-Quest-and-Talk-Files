--80AbyssEX_Charty_Shadow_Boss.lua
--/genmon 703161

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack05_Swoop", 0 },
      { "Attack06_Thrusting", 0 },
  },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Move_Left", rate = 10, loop = 2  },
   { action_name = "Move_Right", rate = 10, loop = 2  },
   { action_name = "Move_Back", rate = 10, loop = 2  },
   { action_name = "Attack03_Indiscriminate_Gigantes", rate = 12, loop = 1, cooltime = 17000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = -1, loop = 2  },
   { action_name = "Move_Back", rate = 2, loop = 2  },
   { action_name = "CustomAction1", rate = 7, loop = 1  },
   { action_name = "Attack03_Indiscriminate_Gigantes", rate = 12, loop = 1, cooltime = 17000 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 7, loop = 2  },
   { action_name = "Walk_Right", rate = 7, loop = 2  },
   { action_name = "Walk_Front", rate = 9, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 2  },
   { action_name = "Move_Left", rate = 6, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 1  },
   { action_name = "Move_Back", rate = 2, loop = 2  },
   { action_name = "CustomAction1", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 6, loop = 2  },
   { action_name = "Walk_Back", rate = -1, loop = 2  },
   { action_name = "Move_Left", rate = 4, loop = 2  },
   { action_name = "Move_Right", rate = 4, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 2  },
   { action_name = "Move_Back", rate = -1, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Move_Left", rate = 7, loop = 2  },
   { action_name = "Move_Right", rate = 7, loop = 2  },
   { action_name = "Move_Front", rate = 16, loop = 2  },
}

g_Lua_GlobalCoolTime1 = 10000
g_Lua_GlobalCoolTime2 = 15000

g_Lua_Skill = { 
   { skill_index = 20941,  cooltime = 12000, rate = 50, rangemin = 0, rangemax = 300, target = 3, },-- 두번할퀴기 Attack04_DoubleSlash
   { skill_index = 20942,  cooltime = 15000, rate = 50, rangemin = 0, rangemax = 400, target = 3, },-- 가시공격 Attack02_Thorns
   { skill_index = 20944,  cooltime = 30000, rate = 60, rangemin = 0, rangemax = 400, target = 3, selfhppercentrange = "0,100" },-- 바닥가시공격 Attack07_ThornsGround 원래 50~100
   { skill_index = 20945,  cooltime = 25000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 100 },-- 가시발사 Attack08_ThrowThorns
   { skill_index = 31752,  cooltime = 3000, rate = 1, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 50, limitcount = 1, priority=99 },-- 밀어내기 포효 Attack07_Roar
   -- { skill_index = 31754,  cooltime = 50000, rate = 60, rangemin = 500, rangemax = 3000, target = 3, selfhppercent = 50, globalcooltime = 1, },-- 검은 구슬 발사 Attack10_BlowOynx 원래 50
   -- { skill_index = 33060,  cooltime = 40000, rate = 1, rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "0,50", priority=99 },-- 직전 가시 Attack06_ThornsLine  원래 49
   --신규스킬--
   { skill_index = 36415,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 75, randometarget = "1.5,0,1", globalcooltime = 1, }, --Attack12_Rolling_80AbyssEX_Start 원래 75퍼
   -- { skill_index = 36416,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 75, randometarget = "1.5,0,1", globalcooltime = 1, },-- 도굴개시 Attack13_DigGround_80AbyssEX_Start 원래 50퍼
   -- { skill_index = 31752,  cooltime = 30000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,49" },-- 밀어내기 포효 Attack07_Roar
   --테스트용--
   -- { skill_index = 36416,  cooltime = 10000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, priority = 100 },-- 바닥가시공격 Attack07_ThornsGround
   -- { skill_index = 33060,  cooltime = 10000, rate = 60, rangemin = 0, rangemax = 1500, target = 3, },-- 검은가시 포효 Attack07_ThornsRoar
   -- { skill_index = 20945,  cooltime = 10000, rate = 60, rangemin = 0, rangemax = 3000, target = 3, },-- 밀어내기 포효 Attack07_Roar

   
}