--AiFrogFlower_Red_Elite_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 300
g_Lua_AssualtTime = 5000
g_Lua_NoAggroStand = 1

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 6, loop = 1 },
   { action_name = "Move_Right", rate = 6, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Move_Back_3", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 5, loop = 2 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 6, loop = 1 },
   { action_name = "Move_Right", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Move_Back_2", rate = 3, loop = 1 },
   { action_name = "Move_Back_3", rate = 3, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 2 },
   { action_name = "Walk_Right", rate = 6, loop = 2 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
   { action_name = "Move_Back_2", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 4 },
}

g_Lua_GlobalCoolTime1 = 7000

g_Lua_Skill = { 
	{ skill_index = 500003,  cooltime = 40000, rate = 80, rangemin = 800, rangemax = 1500, target = 3, anymultipletarget ="1,2,0,1", globalcooltime = 1 }, --Attack02_Siege_Start
	{ skill_index = 500004,  cooltime = 40000, rate = 80, rangemin = 0, rangemax = 800, target = 3, encountertime = 6000 }, --Attack03_Radar_Start
	{ skill_index = 500005,  cooltime = 20000, rate = 80, rangemin = 600, rangemax = 1000, target = 3, globalcooltime = 1 }, --Attack01_Launch
   
--테스트용--
	-- { skill_index = 500005,  cooltime = 5000, rate = 80, rangemin = 700, rangemax = 2000, target = 3, },
}
