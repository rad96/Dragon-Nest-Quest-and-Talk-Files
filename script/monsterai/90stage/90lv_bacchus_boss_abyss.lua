--90lv_Bacchus_Boss_Abyss.lua
-- /genmon 703251

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000
g_Lua_NoAggroStand = 1

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 7, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 16, loop = 1  },
   { action_name = "Move_Back", rate = 16, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 1  },
   { action_name = "Move_Front", rate = 50, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack4_RocketDash_End", rate = 10, approach = 150 },
   { lua_skill_index = 0, rate = 10, approach = 150 },
}

g_Lua_GlobalCoolTime1 = 15000
g_Lua_GlobalCoolTime2 = 10000

g_Lua_Skill = { 
   --조건스킬
   { skill_index = 500046,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 350, target = 3, },
   { skill_index = 500049,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 350, target = 3, },--Attack4_RocketDash_End 개그용
   --일반스킬
   { skill_index = 500040,  cooltime = 10000, rate = 70, rangemin = 0, rangemax = 250, target = 3, td = "FL,FR", combo1 = "1,50,0", selfhppercentrange = "75, 100"}, -- Attack1_bash 전방 내리치기
   { skill_index = 500046,  cooltime = 10000, rate = 70, rangemin = 0, rangemax = 250, target = 3, td = "FL,FR", combo1 = "1,30,0", selfhppercent = 100, globalcooltime = 2}, -- Attack1_bash_Width 전방내리치기 가로강화
   { skill_index = 500047,  cooltime = 10000, rate = 70, rangemin = 0, rangemax = 500, target = 3, td = "FL,FR", combo1 = "1,30,0", selfhppercent = 100, globalcooltime = 2}, -- Attack1_bash_Vertical 전방 내리치기 세로강화
   { skill_index = 500041,  cooltime = 25000, rate = 80, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 90, td = "FL,FR" }, --Attack2_SwitchCombo 찌르고 넘기기
   { skill_index = 500042,  cooltime = 45000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 90, td = "FL,FR", globalcooltime = 1,  }, --Attack3_SakeBreath_Start 술뿜기 브레스
   { skill_index = 500043,  cooltime = 45000, rate = 80, rangemin = 200, rangemax = 1500, target = 3, selfhppercent = 100, td = "FL,FR", globalcooltime = 1 }, --Attack4_RocketDash_Start_대시공격 
   { skill_index = 500044,  cooltime = 50000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 75, td = "FL,FR", globalcooltime = 1 }, --Attack5_WindSlash_흡입베기
   { skill_index = 500045,  cooltime = 70000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercent = 75, randomtarget = "1,4,0,1", globalcooltime = 1,}, --Attack6_HeliStomp_Start_헬리콥터스톰프
   --테스트용
   -- { skill_index = 36423,  cooltime = 10000, rate = 100, rangemin = 0, rangemax = 800, target = 3, },
}
