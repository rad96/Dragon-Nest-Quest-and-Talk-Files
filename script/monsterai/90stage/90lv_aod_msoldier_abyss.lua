--하급재앙의군대(남성) /genmon 703219 검든애

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 1500

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 15, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
   { action_name = "Move_Back", rate = 1, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 7, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 2 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 8, loop = 1 },
   { action_name = "Walk_Front", rate = 15, loop = 2 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 3, loop = 2 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
}

g_Lua_Skill = {
	{ skill_index = 500018, cooltime = 15000, rate = 60, rangemin = 0, rangemax = 300, target = 3, td = "FR,FL" }, --Attack01_DoubleFang 2연속공격
	{ skill_index = 500019, cooltime = 30000, rate = 60, rangemin = 200, rangemax = 500, target = 3, encountertime = 14000 }, --Attack02_DashFang_Start 대시공격
}