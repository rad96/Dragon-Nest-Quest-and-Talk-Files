--하급재앙의 군대(남성) 원거리 공격형 /genmon 703222

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 3, loop = 1  },
   { action_name = "Move_Back", rate = 1, loop = 1  },
   { action_name = "Attack01_DoubleFang_Noweapon", rate = 7, loop = 1, cooltime = 8000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Walk_Back", rate = 7, loop = 1  },
   { action_name = "Move_Left", rate = 2, loop = 1  },
   { action_name = "Move_Right", rate = 2, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 2  },
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Left", rate = 1, loop = 1  },
   { action_name = "Move_Right", rate = 1, loop = 1  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
}
g_Lua_Skill = {
	{ skill_index = 500020, cooltime = 40000, rate = 80, rangemin = 300, rangemax = 1000, target = 3, encountertime = 10000}, --Attack03_ChargeFang_Start 차지공격
}
