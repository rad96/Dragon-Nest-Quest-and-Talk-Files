--90레벨 개방 크로우맨 어쌔신 주정뱅이 베리
--90lv_CrowAssasin_drunken_Abyss.lua
--몬스터 ID : 703271

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 500;
g_Lua_NearValue4 = 900;
g_Lua_NearValue5 = 1200;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_2", rate = 3, loop = 1  },
   { action_name = "Walk_Back", rate = 7, loop = 2  },
   { action_name = "Move_Back", rate = 15, loop = 2  },
   { action_name = "Attack3_DoubleSwing", rate = 3, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_2", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 2  },
   { action_name = "Walk_Right", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 5, loop = 2  },
   { action_name = "Attack3_DoubleSwing", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop =2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 4, loop = 2  },
   { action_name = "Walk_Back", rate = 4, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 3, loop = 2  },
   { action_name = "Move_Back", rate = 4, loop = 2  },
   { action_name = "Attack1_Throw", rate = 2, loop = 1  },
   { action_name = "Attack2_BodyThrow", rate = 3, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 5, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2 },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Move_Left", rate = 5, loop = 2  },
   { action_name = "Move_Right", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 7, loop = 2  },
   { action_name = "Attack1_Throw", rate = 2, loop = 1  },
   { action_name = "Attack2_BodyThrow", rate = 4, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_2", rate = 3, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 8, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 25, loop = 3  },
   { action_name = "Move_Back", rate = 1, loop = 2  },
   { action_name = "Attack1_Throw", rate = 6, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Stand_2", rate = 3, loop = 2  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 30, loop = 3  },
}

g_Lua_Skill = { 
   { skill_index = 500002,  cooltime = 10000, rate = 6, rangemin = 0, rangemax = 300, target = 3 },
}
