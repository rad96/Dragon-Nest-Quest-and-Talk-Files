--AiDarkElf_Blue_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 700;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Walk_Back", rate = 5, loop = 1 },
   { action_name = "Move_Back", rate = 7, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 1, loop = 1  },
   { action_name = "Move_Left", rate = 7, loop = 1  },
   { action_name = "Move_Right", rate = 7, loop = 1  },
   { action_name = "Move_Back", rate = 3, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 3, loop = 1  },
   { action_name = "Move_Right", rate = 3, loop = 1  },
   { action_name = "Move_Front", rate = 7, loop = 2  },
   { action_name = "Move_Back", rate = 4, loop = 1  },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_2", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 7, loop = 2  },
   { action_name = "Move_Back", rate = 2, loop = 1  },
   { action_name = "Assault", rate = 5, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Walk_Front", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 7, loop = 2  },
}
g_Lua_Assault = { 
   { lua_skill_index = 0, rate = 45, loop = 1, approach = 150.0  },
   { lua_skill_index = 1, rate = 45, loop = 1, approach = 700.0  },
   { action_name = "Move_Back", rate = 10, loop = 2, approach = 150.0,},--lua_skill_index = 1
}
g_Lua_Skill = { 
   --조건스킬--
   { skill_index = 500014,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 400, target = 3,}, --Attack01_Slash
   { skill_index = 500015,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, }, --Attack02_Boomerang
   --일반스킬--
   { skill_index = 500014,  cooltime = 20000, rate = 80, rangemin = 0, rangemax = 250, target = 3, selfhppercent = 100, }, --Attack01_Slash
   { skill_index = 500015,  cooltime = 40000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, }, --Attack02_Boomerang
   { skill_index = 500016,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 1000, target = 3, limitcount = 2, selfhppercent = 75}, --Attack03_MagingActivate
}
