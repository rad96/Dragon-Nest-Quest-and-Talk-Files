--/genmon 703280 90레벨 스테이지 트랩용
g_Lua_NearTableCount = 2

g_Lua_NearValue1 = 3000
g_Lua_NearValue2 = 6000


g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 8000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 16, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
}

g_Lua_Skill = { 
   { skill_index = 500061,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, },--Attack_90lv_Stage3_Drunken 술취하기
   { skill_index = 500048,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, },--Attack_90lv_Stage3_Drunken 술취하기 이벤트용(범위넒음)
   { skill_index = 500012,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, },--Attack_90lv_Stage1_Gravity_Startt 모래지옥 범위좁음
   { skill_index = 500013,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 3000, target = 3, },--Attack_90lv_Stage1_Gravity2_Startt 모래지옥 범위넓음
}