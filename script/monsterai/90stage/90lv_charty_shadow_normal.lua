--AiCharty_Shadow_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1200;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" }, 
}

g_Lua_CustomAction = {
  CustomAction1 = {
      { "Attack05_Swoop", 1 },
      { "Attack06_Thrusting", 1 },
  },
}

g_Lua_GlobalCoolTime1 = 30000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Left", rate = 4, loop = 2  },
   { action_name = "Walk_Right", rate = 4, loop = 2  },
   { action_name = "Walk_Back", rate = 2, loop = 1  },
   { action_name = "Move_Back", rate = 1, loop = 1  },
   { action_name = "Attack04_DoubleSlash", rate = 15, loop = 1  },
   { action_name = "Attack06_Thrusting", rate = 15, loop = 1, target_condition = "State1" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 1  },
   { action_name = "Walk_Right", rate = 3, loop = 1  },
   { action_name = "Walk_Front", rate = 12, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 1  },
   { action_name = "CustomAction1", rate = 12, loop = 1  },
   { action_name = "Assault", rate = 11, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 3, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
   { action_name = "Move_Left", rate = 3, loop = 2  },
   { action_name = "Move_Right", rate = 3, loop = 2  },
   { action_name = "Move_Front", rate = 10, loop = 1  },
   { action_name = "Move_Back", rate = 1, loop = 1  },
   { action_name = "CustomAction1", rate = 6, loop = 1  },
   { action_name = "Assault", rate = 11, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 12, loop = 2  },
   { action_name = "Move_Front", rate = 12, loop = 2  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 14, loop = 2  },
   { action_name = "Move_Front", rate = 16, loop = 2  },
}
g_Lua_Assault = { 
   { action_name = "Attack04_DoubleSlash", rate = 10, loop = 1, approach = 150 },
}
g_Lua_Skill = { 
   { skill_index = 20942,  cooltime = 16000, rate = -1, rangemin = 0, rangemax = 300, target = 3 },
   { skill_index = 20943,  cooltime = 30000, rate = 80, rangemin = 200, rangemax = 600, target = 3, selfhppercent = 75 },
   -- { skill_index = 20944,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 400, target = 3, selfhppercent = 75 }, --바닥 가시공격 삭제
   { skill_index = 20945,  cooltime = 30000, rate = 80, rangemin = 300, rangemax = 1000, target = 3 },
}
