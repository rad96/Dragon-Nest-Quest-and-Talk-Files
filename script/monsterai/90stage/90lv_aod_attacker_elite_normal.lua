g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 300;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1650;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssaultTime = 5000
g_Lua_NoAggroStand = 1

g_Lua_Near1 = 
{ 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 3, loop = 1 },
}
g_Lua_Near2 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Move_Left", rate = 4, loop = 1 },
   { action_name = "Move_Right", rate = 4, loop = 1 },
}
g_Lua_Near3 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Assault", rate = 5, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
}
g_Lua_Near4 = 
{ 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 3 },
}

g_Lua_Assault = 
{ 
   { lua_skill_index = 0, rate = 10, approach = 300 },
}

g_Lua_GlobalCoolTime1 = 10000


g_Lua_Skill = {
	--넥스트스킬--
	{ skill_index = 500010, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 1000, target = 3, },--Attack02_Dempsey_Upper
	--일반스킬--
	{ skill_index = 500007, cooltime = 16000, rate = 60, rangemin = 0, rangemax = 200, target = 3, td = "FR,FL", globalcooltime = 1 }, --Attack01_Punch
	{ skill_index = 500007, cooltime = 16000, rate = 60, rangemin = 0, rangemax = 200, target = 3, td = "FR,FL", next_lua_skill_index = 0, globalcooltime = 1 }, --Attack01_Punch
	{ skill_index = 500008, cooltime = 45000, rate = 60, rangemin = 0, rangemax = 500, target = 3, td = "FR,FL", randomtarget= "1.6,0,1", }, --Attack02_Dempsey_Start
	{ skill_index = 500009, cooltime = 60000, rate = 60, rangemin = 0, rangemax = 400, target = 3,}, --Attack03_Hammer
	{ skill_index = 500010, cooltime = 30000, rate = 60, rangemin = 0, rangemax = 350, target = 3 }, --Attack02_Dempsey_Upper
}