g_Lua_NearTableCount = 3

g_Lua_NearValue1 = 100;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 3000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 3000 --몬스터가 돌격시에 타겟에게 다가가는 최소거리
g_Lua_AssualtTime = 5000 --Assault 명령을 받고 돌격 할 때 타겟을 쫓아가는 시간. 


 g_Lua_Near1 = {  
   { action_name = "Walk_Left", rate = 3, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Attack5_BiteSide", rate = 6, loop = 1	},
}
 
g_Lua_Near2 = { 
   { action_name = "Walk_Left", rate = 2, loop = 2  },
   { action_name = "Walk_Right", rate = 2, loop = 2  },
   { action_name = "Walk_Front", rate = 7, loop = 2  },
   { action_name = "Assault", rate = 5, loop = 1  },
}

g_Lua_Near3 = { 
   { action_name = "Walk_Right", rate = 10, loop = 2  },
   { action_name = "Walk_Left", rate = 10, loop = 3  },
   { action_name = "Assault", rate = 2, loop = 1  },
}

g_Lua_Assault = { 
   { action_name = "Attack4_BiteFull",	rate = 1,	loop = 1	},
}
