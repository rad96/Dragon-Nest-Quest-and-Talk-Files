--SeaDragonNest_Gate7_HalfGolem_Fire_Boss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1800;
g_Lua_NearValue5 = 15000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 16, loop = 1  },
   { action_name = "Move_Back", rate = 8, loop = 1  },
   { action_name = "Attack1_WavePunch", rate = 35, loop = 1  },
   { action_name = "Attack2_Blow", rate = 65, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 10, loop = 1  },
   { action_name = "Walk_Right", rate = 10, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 1  },
   { action_name = "Walk_Back", rate = 15, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 7, loop = 1  },
   { action_name = "Move_Back", rate = 7, loop = 1  },
   { action_name = "Attack1_WavePunch", rate = 80, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 8, loop = 1  },
   { action_name = "Walk_Right", rate = 8, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 7, loop = 1  },
   { action_name = "Move_Back", rate = 5, loop = 1  },
   { action_name = "Attack1_WavePunch", rate = 63, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Move_Left", rate = 5, loop = 1  },
   { action_name = "Move_Right", rate = 5, loop = 1  },
   { action_name = "Move_Front", rate = 30, loop = 2  },
}
g_Lua_Near5 = { 
   { action_name = "Move_Front", rate = 25, loop = 3  },
}


g_Lua_Skill = { 
--1Phase
--파이어볼
   { skill_index = 30539,  cooltime = 10000, rate = 100, rangemin = 300, rangemax = 2000, target = 3, selfhppercentrange = "75,100"  },
--볼케이노
   { skill_index = 30536,  cooltime = 8000, rate = 75, rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "75,100"  },

--2Phase
--프레임버스트
   { skill_index = 30538,  cooltime = 8000, rate = 90, rangemin = 300, rangemax = 1500, target = 3, selfhppercentrange = "50,75", td = "FL,FR,LF,RF,RB,BR,BL,LB"  },
--볼케이노
   { skill_index = 30536,  cooltime = 18000, rate = 75, rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "50,75"  },
--파이어볼
   { skill_index = 30539,  cooltime = 10000, rate = 100, rangemin = 300, rangemax = 2000, target = 3, selfhppercentrange = "50,75" },

--3Phase
--프레임로드
   { skill_index = 30537,  cooltime = 15000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "25,50"  },
--프레임버스트
   { skill_index = 30538,  cooltime = 30000, rate = 60, rangemin = 300, rangemax = 1500, target = 3, selfhppercentrange = "25,50", td = "FL,FR,LF,RF,RB,BR,BL,LB"  },
--볼케이노
   { skill_index = 30536,  cooltime = 30000, rate = 55, rangemin = 0, rangemax = 2000, target = 3, selfhppercentrange = "25,50"  },
--파이어볼
   { skill_index = 30539,  cooltime = 10000, rate = 100, rangemin = 300, rangemax = 2000, target = 3, selfhppercentrange = "25,50"  },

--4Phase
--프레임로드
   { skill_index = 30537,  cooltime = 15000, rate = 90, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "0,25"  },
--이그니션
   { skill_index = 30540,  cooltime = 5000, rate = 100, rangemin = 0, rangemax = 2000, target = 3, selfhppercent = 25, blowcheck = "42" },
--프레임버스트
   { skill_index = 30538,  cooltime = 30000, rate = 75, rangemin = 300, rangemax = 1500, target = 3, selfhppercentrange = "0,25"  },
--파이어볼
   { skill_index = 30539,  cooltime = 10000, rate = 80, rangemin = 300, rangemax = 2000, target = 3, selfhppercentrange = "0,25"  },
}
