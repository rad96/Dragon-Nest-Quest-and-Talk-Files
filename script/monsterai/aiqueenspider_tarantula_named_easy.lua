--AiQueenSpider_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 900;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 4500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 10000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 4, loop = 1  },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack1_Slash_Boss", rate = 2, loop = 1  },
   { action_name = "Attack2_Blow_Boss", rate = 2, loop = 1  },
   { action_name = "Attack8_RollingAttack", rate = 4, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1  },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Attack8_RollingAttack", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Attack8_RollingAttack", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Attack3_JumpAttack", rate = 4, loop = 1, randomtarget = "1.1" },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Attack3_JumpAttack", rate = 4, loop = 1, randomtarget = "1.1" },
}

g_Lua_Skill = { 
   { skill_index = 30201,  cooltime = 32000, rate = 50, rangemin = 0, rangemax = 800, target = 3 },
}