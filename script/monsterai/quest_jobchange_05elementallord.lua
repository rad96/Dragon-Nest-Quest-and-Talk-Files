g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 120;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1200;
g_Lua_NearValue5 = 10000;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down|Stun" },
	State2 = {"!Stay|!Move|!Stiff|!Attack|Down|Stun", "Air" },
}



g_Lua_Near1 = 
{ 
-- 0 ~ 120 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Back",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near2 = 
{ 
-- 120 ~ 500 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Back",		rate = 20,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near3 = 
{ 
-- 500 ~ 800 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near4 = 
{ 
-- 800 ~ 1200 : 
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Near5 = 
{ 
-- 1200 ~ 10000 : Ʈ����, �����ַο�
	{ action_name = "Stand",		rate = 8,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Front",		rate = 10,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Left",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
	{ action_name = "Move_Right",		rate = 15,		loop = 1,	selfhppercentrange = "0,100" },
}

g_Lua_Skill = { 
-- ������ ����
   { skill_index = 31121,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "RB,BR,BL,LB"  },
-- ������ ����
   { skill_index = 31122,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FL,FR,LF,RF"  },
-- ������ ������
   { skill_index = 31123,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FL,LF,RB,BR,BL"  },
-- ������ ����
   { skill_index = 31124,  cooltime = 2600, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FR,RF,RB,BR,BL"  },
-- �÷��ӿ�
   { skill_index = 31061,  cooltime = 7000, rate = 100, rangemin = 0, rangemax = 600,target = 3  },
-- �۷��̼Ƚ�����ũ
   { skill_index = 31062,  cooltime = 6500, rate = 100, rangemin = 0, rangemax = 800,target = 3  },
-- ������̻���
   { skill_index = 31063,  cooltime = 16000, rate = 100, rangemin = 0, rangemax = 600,target = 3, td = "FL,FR,LF,RF" },
-- ��ũ���̺�
   { skill_index = 31064,  cooltime = 23000, rate = 100, rangemin = 0, rangemax = 400,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },

-- ���丣��
   { skill_index = 31071,  cooltime = 24000, rate = 100, rangemin = 0, rangemax = 800,target = 3, td = "FL,FR" },
-- ���̾��
   { skill_index = 31072,  cooltime = 33000, rate = 100, rangemin = 0, rangemax = 500,target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB" },
-- ����¡�ҵ�
   { skill_index = 31073,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 500,target = 3  },
-- ĥ���̽�Ʈ
   { skill_index = 31074,  cooltime = 45000, rate = 100, rangemin = 0, rangemax = 800,target = 3, td = "FL,FR,LF,RF" },
-- ���̾����
   { skill_index = 31075,  cooltime = 20000, rate = 100, rangemin = 0, rangemax = 1200,target = 3, td = "FL,FR" },
}