--AiUnstable_Black_Boss_ShadowOfEvil_Master.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 800;
g_Lua_NearValue4 = 1100;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 7000
g_Lua_GlobalCoolTime2 = 9000
g_Lua_GlobalCoolTime3 = 7000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Move_Back", rate = 4, loop = 1 },
   { action_name = "Attack8_Combo", rate = 9, loop = 1, globalcooltime = 2 },
   { action_name = "Attack1_Kick", rate = 9, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 6, loop = 1 },
   { action_name = "Move_Front", rate = 4, loop = 1 },
   { action_name = "Move_Back", rate = 2, loop = 1 },
   { action_name = "Attack8_Combo", rate = 10, loop = 1, globalcooltime = 2 },
   { action_name = "Attack4_Saw", rate = 19, loop = 1, globalcooltime = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 16, loop = 1 },
   { action_name = "Walk_Right", rate = 16, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
   { action_name = "Attack3_Bite", rate = 19, loop = 1 },
   { action_name = "Attack4_Saw", rate = 10, loop = 1, globalcooltime = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Walk_Left", rate = 16, loop = 1 },
   { action_name = "Walk_Right", rate = 16, loop = 1 },
   { action_name = "Walk_Front", rate = 14, loop = 1 },
   { action_name = "Move_Front", rate = 14, loop = 1 },
   { action_name = "Assault", rate = 9, loop = 1 },
   { action_name = "Move_Teleport", rate = 6, loop = 1 },
}
g_Lua_Near5 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Walk_Front", rate = 6, loop = 2 },
   { action_name = "Walk_Left", rate = 16, loop = 1 },
   { action_name = "Walk_Right", rate = 16, loop = 1 },
   { action_name = "Assault", rate = 9, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Kick", rate = 13, loop = 1, approach = 200 },
   { action_name = "Attack8_Combo", rate = 13, loop = 1, approach = 200 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 200 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 200 },
   { action_name = "Stand_1", rate = 12, loop = 1, approach = 200 },
}
g_Lua_Skill = {  
   { skill_index = 20829,  cooltime = 1000, rate = 100, rangemin = 0, rangemax = 5500, target = 3, encountertime = 20000, next_lua_skill_index = 1 },
   { skill_index = 20831,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5500, target = 3 },
   { skill_index = 20821,  cooltime = 21000, rate = 80, rangemin = 700, rangemax = 1100, target = 3, randomtarget = 1.1 },
   { skill_index = 20825,  cooltime = 37000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 70, multipletarget = "1,3" },
   { skill_index = 20833,  cooltime = 8000, rate = 60, rangemin = 600, rangemax = 800, target = 3, globalcooltime = 3 },
   { skill_index = 20832,  cooltime = 8000, rate = 60, rangemin = 800, rangemax = 1100, target = 3, globalcooltime = 3 },
}