--AiGreatBeatleBig_White_Boss_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 40000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Attack01_Slash", rate = 42, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
   { action_name = "Attack04_Combo", rate = 42, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 27, loop = 1 },
   { action_name = "Attack07_Swoop", rate = 42, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 27, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 8, loop = 1, approach = 300 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 300 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 300 },
   { action_name = "Attack01_Slash", rate = 36, loop = 1, approach = 300 },
}
g_Lua_Skill = { 
   { skill_index = 33140,  cooltime = 30000, rate = 100,rangemin = 0, rangemax = 2500, target = 3, selfhppercent = 90, globalcooltime = 1  },
   { skill_index = 20861,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 90, globalcooltime = 1  },
   { skill_index = 20851,  cooltime = 14000, rate = 70, rangemin = 0, rangemax = 1500, target = 3 },
   { skill_index = 20856,  cooltime = 17000, rate = 70, rangemin = 500, rangemax = 2500, target = 3 },
}
