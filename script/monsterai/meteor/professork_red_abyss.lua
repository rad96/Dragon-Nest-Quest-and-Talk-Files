--AiProfessorK_Red_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 500;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 100
g_Lua_AssualtTime = 5000

g_Lua_CustomAction = {
   CustomAction1 = 
     {
		  { "Fly", 0},
		  { "Attack06_ThrowBomb", 0 }, 
		  { "Attack06_ThrowBomb", 0 }, 
		  { "Attack06_ThrowBomb", 0 }, 
		  { "Fly", 0},
		  { "Attack08_Landing_Stun", 0},
		  { "Stun", 0},
     },
	 CustomAction2 = 
     {
          { "Fly", 0},
          { "Attack07_ThrowBigBomb", 0},
		  { "Fly", 0},
	      { "Attack08_Landing_Stun", 0},
		  { "Stun", 0},
     },
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 7, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Left", rate = 7, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Right", rate = 7, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "CustomAction1", rate = 68, custom_state1 = "custom_fly", selfhppercentrange = "55,100" },
   { action_name = "CustomAction2", rate = 68, custom_state1 = "custom_fly", selfhppercentrange = "0,55" },
   { action_name = "Attack01_ChainSaw", rate = 42, custom_state1 = "custom_ground" ,  cooltime = 15000 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 7, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Front", rate = 7, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Left", rate = 7, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Right", rate = 7, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "CustomAction1", rate = 68, custom_state1 = "custom_fly", selfhppercentrange = "55,100" },
   { action_name = "CustomAction2", rate = 68, custom_state1 = "custom_fly", selfhppercentrange = "0,55" },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 7, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Front", rate = 12, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Assault", rate = 42, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "CustomAction1", rate = 68, custom_state1 = "custom_fly", selfhppercentrange = "55,100" },
   { action_name = "CustomAction2", rate = 68, custom_state1 = "custom_fly", selfhppercentrange = "0,55" },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 7, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Walk_Front", rate = 15, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "Assault", rate = 42, loop = 1, custom_state1 = "custom_ground" },
   { action_name = "CustomAction1", rate = 68, custom_state1 = "custom_fly", selfhppercentrange = "55,100" },
   { action_name = "CustomAction2", rate = 68, custom_state1 = "custom_fly", selfhppercentrange = "0,55" },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 8, loop = 1, approach = 200, custom_state1 = "custom_ground" },
   { action_name = "Walk_Left", rate = 7, loop = 1, approach = 200, custom_state1 = "custom_ground" },
   { action_name = "Walk_Right", rate = 7, loop = 1, approach = 200, custom_state1 = "custom_ground" },
   { action_name = "Attack01_ChainSaw", rate = 42, loop = 1, approach = 200, custom_state1 = "custom_ground" },
}
g_Lua_Skill = { 
   { skill_index = 33193,  cooltime = 42000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercentrange = "0,90", custom_state1 = "custom_ground", encountertime = 5000 },
   { skill_index = 33185,  cooltime = 16000, rate = 50, rangemin = 100, rangemax = 1000, target = 3, randomtarget=1.1, custom_state1 = "custom_ground"  },
   { skill_index = 33194,  cooltime = 21000, rate = 60, rangemin = 500, rangemax = 1000, target = 3, custom_state1 = "custom_ground"  },
   { skill_index = 33195,  cooltime = 45000, rate = 50, rangemin = 0, rangemax = 4000, target = 3, custom_state1 = "custom_ground", selfhppercentrange = "0,60" },
}
