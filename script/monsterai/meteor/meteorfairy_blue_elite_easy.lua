--AiMeteorFairy_Blue_Elite_Easy.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 20, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Walk_Back", rate = 2, loop = 1 },
   { action_name = "Attack01_Swing", rate = 2, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 9, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 4, loop = 1 },
   { action_name = "Walk_Right", rate = 4, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 2, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 2 },
   { action_name = "Move_Front", rate = 12, loop = 3 },
}
g_Lua_Assault = { 
   { action_name = "Attack01_Swing", rate = 6, loop = 1, approach = 100, max_missradian = 30 },
   { action_name = "Stand_1", rate = 12, loop = 1, approach = 100, max_missradian = 30 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 100, max_missradian = 30 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 100, max_missradian = 30 },
}
g_Lua_Skill = { 
   { skill_index = 20842,  cooltime = 31000, rate = 60,rangemin = 0, rangemax = 900, target = 3, max_missradian = 30 },
   { skill_index = 20845,  cooltime = 36000, rate = 40, rangemin = 0, rangemax = 1100, target = 3, max_missradian = 30 },
}
