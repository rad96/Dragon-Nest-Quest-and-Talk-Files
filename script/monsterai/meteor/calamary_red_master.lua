--AiCalamary_Red_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 800;
g_Lua_NearValue3 = 1200;
g_Lua_NearValue4 = 1600;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 200
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Attack1_Slash", rate = 9, loop = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 5, loop = 1 },
   { action_name = "Walk_Right", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 12, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 6, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Stand_1", rate = 5, loop = 1, approach = 300 },
   { action_name = "Walk_Left", rate = 4, loop = 1, approach = 300 },
   { action_name = "Walk_Right", rate = 4, loop = 1, approach = 300 },
   { action_name = "Attack1_Slash", rate = 10, loop = 1, approach = 300 },
}
g_Lua_Skill = { 
   { skill_index = 20881,  cooltime = 19000, rate = 80,rangemin = 400, rangemax = 1000, target = 3 },
   { skill_index = 20874,  cooltime = 23000, rate = 60, rangemin = 0, rangemax = 1000, target = 3 },
   { skill_index = 20873,  cooltime = 28000, rate = 40, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 80 },
}
