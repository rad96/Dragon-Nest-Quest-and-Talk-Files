--AiCarbuncle_Black_Master.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 600;
g_Lua_NearValue3 = 900;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 12, loop = 1 },
   { action_name = "Walk_Left", rate = 6, loop = 1 },
   { action_name = "Walk_Right", rate = 6, loop = 1 },
   { action_name = "Walk_Back", rate = 4, loop = 1 },
   { action_name = "Attack1_TailAttack", rate = 6, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 9, loop = 2 },
   { action_name = "Walk_Front", rate = 5, loop = 1 },
   { action_name = "Attack5_Crash", rate = 6, loop = 1, max_missradian = 30 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Walk_Front", rate = 5, loop = 2 },
   { action_name = "Move_Front", rate = 8, loop = 1 },
   { action_name = "Attack3_Big", rate = -1, loop = 1, max_missradian = 30 },
   { action_name = "Assault", rate = 4, loop = 1 },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 8, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 3 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_TailAttack", rate = 4, loop = 1, approach = 200, max_missradian = 30 },
   { action_name = "Walk_Left", rate = 2, loop = 1, approach = 200 },
   { action_name = "Walk_Right", rate = 2, loop = 1, approach = 200 },
}
