--AiUnstable_Blue_Elite_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 200;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1100;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 16, loop = 1 },
   { action_name = "Walk_Left", rate = 8, loop = 1 },
   { action_name = "Walk_Right", rate = 8, loop = 1 },
   { action_name = "Walk_Back", rate = 3, loop = 1 },
   { action_name = "Attack1_Kick", rate = 9, loop = 1, max_missradian = 30 },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Right", rate = 3, loop = 1 },
   { action_name = "Walk_Left", rate = 3, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 6, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 2, loop = 1 },
   { action_name = "Walk_Front", rate = 8, loop = 2 },
   { action_name = "Move_Front", rate = 14, loop = 1 },
   { action_name = "Attack2_Teleport", rate = 6, loop = 1, max_missradian = 10 },
   { action_name = "Move_Teleport_Blue", rate = 6, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 2 },
   { action_name = "Assault", rate = 6, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack1_Kick", rate = 2, loop = 1, approach = 200, max_missradian = 30 },
   { action_name = "Walk_Left", rate = 6, loop = 1, approach = 200 },
   { action_name = "Walk_Right", rate = 6, loop = 1, approach = 200 },
   { action_name = "Stand_1", rate = 2, loop = 1, approach = 200 },
}
g_Lua_Skill = { 
   { skill_index = 20822,  cooltime = 31000, rate = 60,rangemin = 300, rangemax = 1200, target = 3, max_missradian = 20 },
   { skill_index = 20824,  cooltime = 31000, rate = 40, rangemin = 0, rangemax = 900, target = 3, max_missradian = 20 },
}
