--AiMeteorFairyQueen_Green_Normal.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 300;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssaultTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1, globalcooltime = 3 },
   { action_name = "Move_Right", rate = 5, loop = 1, globalcooltime = 3 },
   { action_name = "Attack09_SideKick", rate = 13, loop = 1, td = "FR,FL", globalcooltime = 1 },
}
g_Lua_Near2 = { 
   { action_name = "Stand", rate = 5, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Near3 = { 
   { action_name = "Stand", rate = 4, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 4, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand", rate = 2, loop = 1 },
   { action_name = "Move_Front", rate = 12, loop = 1 },
   { action_name = "Assault", rate = 3, loop = 1  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Move_Front", rate = 10, loop = 2 },
   { action_name = "Move_Left", rate = 5, loop = 1 },
   { action_name = "Move_Right", rate = 5, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack09_SideKick", rate = 5, loop = 1, approach = 200.0, globalcooltime = 1 },
}

g_Lua_GlobalCoolTime1 = 10000
g_Lua_GlobalCoolTime2 = 25000
g_Lua_GlobalCoolTime3 = 7000

g_Lua_Skill = { 
   { skill_index = 35072,  cooltime = 1000, rate = -1,rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 35079,  cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3 },
   { skill_index = 35071,  cooltime = 125000, rate = 100, rangemin = 0, rangemax = 5000, target = 3, selfhppercent = 75, next_lua_skill_index = 0, limitcount = 6, globalcooltime = 2 },
   { skill_index = 35073,  cooltime = 58000, rate = 40, rangemin = 0, rangemax = 700, target = 3 },
   { skill_index = 35075,  cooltime = 27000, rate = 60, rangemin = 0, rangemax = 200, target = 3, td = "FR,FL" },
   { skill_index = 35077,  cooltime = 38000, rate = 50, rangemin = 200, rangemax = 700, target = 3, td = "FR,FL",randomtarget = "0.2,0,1" },
   { skill_index = 35078,  cooltime = 75000, rate = 40, rangemin = 500, rangemax = 2500, target = 3, selfhppercent = 90, td = "FR,FL,LF,RF" },
   { skill_index = 35080,  cooltime = 125000, rate = 70, rangemin = 0, rangemax = 2500, target = 3, selfhppercent = 50, next_lua_skill_index = 1, globalcooltime = 2 },
}
