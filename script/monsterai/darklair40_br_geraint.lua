-- NPC Geraint Normal AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 600.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 1500.0;



g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;




g_Lua_Near1 = 
{ 
     { action_name = "Stand", rate = 2, loop = 1 },
     { action_name = "Walk_Left", rate = 5, loop = 1 },
	 { action_name = "Walk_Right", rate = 5, loop = 1 },
     { action_name = "Attack1_Upper", rate = 15, loop = 1 },
     { action_name = "Attack3_Slash", rate = 10, loop = 1 },
     { action_name = "Attack5_ImpactAttack", rate = 5, loop = 1 },
}

g_Lua_Near2 = 
{ 
     { action_name = "Stand", rate = 2, loop = 1 },
     { action_name = "Walk_Left", rate = 5, loop = 2 },
     { action_name = "Walk_Right", rate = 5, loop = 2 },
     { action_name = "Walk_Front", rate = 10, loop = 2 },
     { action_name = "Move_Left", rate = 5, loop = 2 },
     { action_name = "Move_Right", rate = 5, loop = 2 },
     { action_name = "Move_Front", rate = 5, loop = 2 },
     { action_name = "Attack3_Slash", rate = 15, loop = 1 },
     { action_name = "Attack5_ImpactAttack", rate = 10, loop = 1 },
     { action_name = "Attack4_Lazer", rate = 20, loop = 1 },
     { action_name = "Assault", rate = 10, loop = 1 },
}


g_Lua_Near3 = 
{ 
     { action_name = "Stand", rate = 1, loop = 1 },
     { action_name = "Walk_Left", rate = 3, loop = 2 },
     { action_name = "Walk_Right", rate = 3, loop = 2 },
     { action_name = "Walk_Front", rate = 10, loop = 2 },
     { action_name = "Move_Left", rate = 3, loop = 2 },
     { action_name = "Move_Right", rate = 3, loop = 2 },
     { action_name = "Move_Front", rate = 25, loop = 1 },
     { action_name = "Attack3_Slash", rate = 20, loop = 1 },
     { action_name = "Attack4_Lazer", rate = 14, loop = 1 },
     { action_name = "Assault", rate = 15, loop = 1 },
}

g_Lua_Near4 = 
{ 
     { action_name = "Stand", rate = 1, loop = 2 },
     { action_name = "Walk_Left", rate = 3, loop = 1 },
     { action_name = "Walk_Right", rate = 3, loop = 1 },
     { action_name = "Walk_Front", rate = 10, loop = 2 },
     { action_name = "Move_Left", rate = 3, loop = 1 },
     { action_name = "Move_Right", rate = 3, loop = 1 },
     { action_name = "Move_Front", rate = 30, loop = 2 },
	 { action_name = "Assault", rate = 35, loop = 1 },
}

g_Lua_Assault =
{ 
     { action_name = "Attack1_Upper", rate = 15, loop = 1, cancellook = 0, approach = 150.0 },
     { action_name = "Attack5_ImpactAttack", rate = 10, loop = 1, cancellook = 0, approach = 200.0 },
}