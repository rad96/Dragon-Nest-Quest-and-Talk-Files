--AiSpittler_Green_Boss_Abyss.lua

g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 500;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, custom_state1 = "custom_ground",  },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground",  },
   { action_name = "Stand_On", rate = 10, loop = 1, custom_state1 = "custom_underground",  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, custom_state1 = "custom_ground",  },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground",  },
   { action_name = "Stand_On", rate = 10, loop = 1, custom_state1 = "custom_underground",  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, custom_state1 = "custom_ground",  },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground",  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 10, loop = 1, custom_state1 = "custom_ground",  },
   { action_name = "Stand", rate = 10, loop = 1, custom_state1 = "custom_underground",  },
}
