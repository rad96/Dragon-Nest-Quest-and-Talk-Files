--AiGhoul_Blue_Boss_Abyss.lua

g_Lua_NearTableCount = 6

g_Lua_NearValue1 = 150;
g_Lua_NearValue2 = 250;
g_Lua_NearValue3 = 400;
g_Lua_NearValue4 = 600;
g_Lua_NearValue5 = 1000;
g_Lua_NearValue6 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 150
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 25000

g_Lua_State = {
  State1 = {"Stay|Move|Stiff|Attack|Air", "!Down" }, 
}

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Attack1_ArmAttack", rate = 22, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 7, loop = 1  },
   { action_name = "Attack1_ArmAttack", rate = 30, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Front_230", rate = 10, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 3, loop = 2  },
   { action_name = "Walk_Front_230", rate = 10, loop = 3  },
   { action_name = "Move_Front", rate = 10, loop = 2  },
   { action_name = "Assault", rate = 38, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 3, loop = 1  },
   { action_name = "Walk_Front_230", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 15, loop = 2  },
   { action_name = "Assault", rate = 42, loop = 1  },
}
g_Lua_Near6 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Walk_Front_230", rate = 5, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 3  },
}
g_Lua_Assault = { 
   { action_name = "Attack1_ArmAttack", rate = -1, loop = 1, cancellook = 1, approach = 150.0  },
}
g_Lua_Skill = { 
  -- ���� ������
   { skill_index = 31781,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 200, target = 3 },
   -- �޼� ������
   { skill_index = 31782,  cooltime = 20000, rate = 100, rangemin = 300, rangemax = 1500, target = 3 },
   -- ����
   { skill_index = 31783,  cooltime = 25000, rate = 100, rangemin = 0, rangemax = 700, target = 3, selfhppercentrange = "0,75" },
   -- ����Ÿ��
   { skill_index = 31784,  cooltime = 25000, rate = 100, rangemin = 0, rangemax = 2500, target = 3, multipletarget = "1,4" },
   -- ���幦�� ��ȯ
   { skill_index = 31786,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 2500, target = 3, selfhppercentrange = "0,75", globalcooltime = 1 },
   -- ���繦�� ��ȯ
   { skill_index = 31787,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 2500, target = 3, selfhppercentrange = "0,50", globalcooltime = 1 },
   -- �� ��ȯ
   { skill_index = 31788,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 2500, target = 3, selfhppercentrange = "0,50" },
}