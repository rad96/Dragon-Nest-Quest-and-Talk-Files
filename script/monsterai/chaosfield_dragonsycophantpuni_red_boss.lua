--DragonSycophantPuni_Red_Boss_Abyss.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 400;
g_Lua_NearValue2 = 700;
g_Lua_NearValue3 = 1000;
g_Lua_NearValue4 = 1500;
g_Lua_NearValue5 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_Chopping", rate = 27, loop = 1  },
   { action_name = "Attack2_Slash", rate = 45, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 10, loop = 1  },
   { action_name = "Attack1_Chopping", rate = 65, loop = 1  },
   { action_name = "Assault", rate = 30, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 15, loop = 2  },
   { action_name = "Move_Front", rate = 5, loop = 2  },
   { action_name = "Assault", rate = 90, loop = 1  },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 15, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 1  },
   { action_name = "Walk_Right", rate = 5, loop = 1  },
   { action_name = "Walk_Front", rate = 20, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
   { action_name = "Assault", rate = 130, loop = 1  },
}
g_Lua_Near5 = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Walk_Left", rate = 5, loop = 2  },
   { action_name = "Walk_Right", rate = 5, loop = 2  },
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Move_Front", rate = 20, loop = 1  },
}
g_Lua_Assault = { 
   { action_name = "Attack2_Slash", rate = 5, loop = 1, approach = 250.0  },
}
g_Lua_Skill = { 
   { skill_index = 20383,  cooltime = 30000, rate = 100,rangemin = 0, rangemax = 2000, target = 1, selfhppercent = 50 },
   { skill_index = 20380,  cooltime = 28000, rate = 80, rangemin = 0, rangemax = 500, target = 3, selfhppercent = 100 },
   { skill_index = 20385,  cooltime = 12000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercent = 70 },
}
