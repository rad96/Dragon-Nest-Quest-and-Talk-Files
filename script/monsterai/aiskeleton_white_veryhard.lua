-- Skeleton White Master AI

g_Lua_NearTableCount = 6;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 300.0;
g_Lua_NearValue3 = 700.0;
g_Lua_NearValue4 = 900.0;
g_Lua_NearValue5 = 1200.0;
g_Lua_NearValue6 = 1500.0;

g_Lua_LookTargetNearState = 5;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;
g_Lua_ApproachValue = 150;
g_Lua_AssualtTime = 5000;

g_Lua_State = 	
{	
	State1 = {"!Stay|!Move|!Stiff|!Attack", "Down" },
}


g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Back",	rate = 20,		loop = 2 },
	{ action_name = "Move_Back",	rate = 20,		loop = 2 },
	{ action_name = "Attack1_Chop",	rate = 10,		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Walk_Left",	rate = 20,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 20,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 20,		loop = 2 },
	{ action_name = "Attack1_Chop",	rate = 15,		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 3,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 20,		loop = 1 },
	{ action_name = "Assault",	rate = 8,		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 3,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 3 },
	{ action_name = "Walk_Back",	rate = 1,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Front",	rate = 15,		loop = 3 },
	{ action_name = "Move_Back",	rate = 1,		loop = 3 },
	{ action_name = "Assault",	rate = 9,		loop = 1 },
}

g_Lua_Near5 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 30,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Front",	rate = 35,		loop = 3 },
	{ action_name = "Assault",	rate = 17,		loop = 1 },
}

g_Lua_Near6 = 
{ 
	{ action_name = "Stand_1",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Left",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Right",	rate = 3,		loop = 3 },
	{ action_name = "Walk_Front",	rate = 30,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Front",	rate = 30,		loop = 3 },
}

g_Lua_Assault =
{ 
	{ action_name = "Attack1_Chop",	rate = 5,	loop = 1, approach = 150.0 },
}

g_Lua_Skill = 
{
	{ skill_index = 20046, sp = 0, cooltime = 7000, rangemin = 000, rangemax = 250, target = 3, selfhppercent = 100, target_condition = "State1" },
 	{ skill_index = 20046, sp = 0, cooltime = 15000, rangemin = 000, rangemax = 250, target = 3, selfhppercent = 100, rate = 100 },
 	{ skill_index = 20045, sp = 0, cooltime = 15000, rangemin = 400, rangemax = 800, target = 3, selfhppercent = 100, rate = -1 },
 	{ skill_index = 20044, sp = 0, cooltime = 18000, rangemin = 100, rangemax = 600, target = 3, selfhppercent = 100, rate = 100 },
}