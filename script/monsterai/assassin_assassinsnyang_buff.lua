

g_Lua_NearTableCount = 2;

g_Lua_NearValue1 = 150.0;
g_Lua_NearValue2 = 200.0;

g_Lua_NoAggroOwnerFollow=1;
g_Lua_LookTargetNearState = 2;
g_Lua_WanderingDistance = 1500.0;
g_Lua_PatrolBaseTime = 5000;
g_Lua_PatrolRandTime = 3000;

g_Lua_State = {
  State1 = {"!Stay|!Move|!Stiff|!Down|!Air", "Attack" }, 
  State2 = {"!Stay|!Move|!Stiff|!Attack", "Hit"}, 
  State3 = {"!Stay|!Move|!Attack|!Stiff|!Down|", "Air", "Hit" }, 
}

g_Lua_CustomAction = {
-- 
  CustomAction1 = {
       { "Attack1" },
	   { "Attack2" },
  },
 }

g_Lua_Near1 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
    { action_name = "Move_Front",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
	{ action_name = "Move_Back",	rate = 5,		loop = 3 },
	--{ action_name = "Attack_AjanaDrop", rate = 100, loop = 1, target_condition = "State3", cooltime = 5000, cancellook = 0 },
        --{ action_name = "Attack1", rate = 0,              loop = 1 },
       --{ action_name = "CustomAction1", rate = 0,              loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand",	rate = 5,		loop = 1 },
	{ action_name = "Move_Front",	rate = 5,		loop = 3 },
	{ action_name = "Move_Left",	rate = 5,		loop = 3 },
	{ action_name = "Move_Right",	rate = 5,		loop = 3 },
    { action_name = "Move_Back",	rate = 5,		loop = 3 },
}
	
g_Lua_Skill =
{
	    --{skill_index=261801, SP = 0, cooltime = 5000, rangemin = 200, rangemax = 1000, target = 3, rate = 100,  priority=3, cancellook = 0 },--아즈나블레이드Lv1
	    {skill_index=261901, SP = 0, cooltime = 5000, rangemin = 0, rangemax = 10000, target = 4, rate = 100, ignoreaggro = 1 },--아즈나블레스Lv1
	    --{skill_index=262101, SP = 0, cooltime = 3000, rangemin = 0, rangemax = 200, target = 3, rate = 100, cancellook = 2, target_condition = "State3"	},--아즈나드롭Lv1
	    --{skill_index=262201, SP = 0, cooltime = 1000, rangemin = 0, rangemax = 10000, target = 3, rate = 100,  priority=3 },--아즈나힐링Lv1		
} 
  