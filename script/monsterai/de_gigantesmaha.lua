--GreenDragonNest_Gate1_Golem_Blue.lua


g_Lua_NearTableCount = 4

g_Lua_NearValue1 = 600;
g_Lua_NearValue2 = 1000;
g_Lua_NearValue3 = 1500;
g_Lua_NearValue4 = 2000;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 400
g_Lua_AssualtTime = 5000

g_Lua_GlobalCoolTime1 = 25000
g_Lua_GlobalCoolTime2 = 30000
g_Lua_GlobalCoolTime3 = 20000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Back", rate = 1, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 15000, globalcoomtime = 3 },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 15000, globalcoomtime = 3 },
   { action_name = "Attack01_Punch", rate = 20, loop = 1, cooltime = 10000 },
   { action_name = "Attack02_CannonPunch", rate = 20, loop = 1, cooltime = 10000 },
   { action_name = "Attack06_RightAttack", rate = 20, loop = 1, cooltime = 12000, td = "FR,RF,RB,BR" },
   { action_name = "Attack07_LeftAttack", rate = 20, loop = 1, cooltime = 12000, td = "FL,LF,BL,LB" },
}
g_Lua_Near2 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 10, loop = 1 },
   { action_name = "Walk_Left", rate = 2, loop = 1, cooltime = 15000, globalcoomtime = 3 },
   { action_name = "Walk_Right", rate = 2, loop = 1, cooltime = 15000, globalcoomtime = 3 },
}
g_Lua_Near3 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Walk_Front", rate = 4, loop = 1 },
   { action_name = "Assault", rate = 20, loop = 1, cooltime = 27000, selfhppercent = 95 },
}
g_Lua_Near4 = { 
   { action_name = "Stand_1", rate = 1, loop = 1 },
   { action_name = "Move_Front", rate = 10, loop = 1 },
}
g_Lua_Assault = { 
   { action_name = "Attack03_PunchHammer", rate = 30, loop = 1, approach = 250.0  },
}

g_Lua_Skill = {
   -- ����
   { skill_index = 31838,  cooltime = 5000, rate = -1, rangemin = 0, rangemax = 3000, target = 3 },
   -- ��߱�����
   { skill_index = 31839,  cooltime = 40000, rate = 100, rangemin = 0, rangemax = 3500, target = 3, selfhppercentrange = "0,40", multipletarget = "1,4" },
   -- ����ġ��
   { skill_index = 31834,  cooltime = 20000, rate = 100, rangemin = 200, rangemax = 600, target = 3, selfhppercentrange = "0,100" },
   -- ��ճ���ġ��
   { skill_index = 31836,  cooltime = 35000, rate = 100, rangemin = 400, rangemax = 1200, target = 3, selfhppercentrange = "0,80" },
   -- ��ź ����
   { skill_index = 31837,  cooltime = 25000, rate = 100, rangemin = 700, rangemax = 1500, target = 3, selfhppercentrange = "60,100" },
   -- ������ġ
   -- { skill_index = 31840,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,80", limitcount = 1 },
   -- { skill_index = 31840,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,60", limitcount = 1 },
   -- { skill_index = 31840,  cooltime = 15000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,20", limitcount = 1 },
   -- Ŭ��������ź
   { skill_index = 31841,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 1700, target = 3, selfhppercentrange = "0,60" },
   -- ����
   { skill_index = 31842,  cooltime = 22000, rate = 100, rangemin = 300, rangemax = 600, target = 3, selfhppercentrange = "20,80" },
   -- ����
   { skill_index = 31843,  cooltime = 30000, rate = 100, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "20,80", next_lua_skill_index = 0 },
   -- �̻��ϳ���
   { skill_index = 31844,  cooltime = 27000, rate = 100, rangemin = 0, rangemax = 2500, target = 3, selfhppercentrange = "40,80" , globalcoomtime = 2 },
   -- �̻��ϳ���(������)
   { skill_index = 31845,  cooltime = 27000, rate = 100, rangemin = 0, rangemax = 2500, target = 3, selfhppercentrange = "0,39" , globalcoomtime = 2 },


   -- ������
   --{ skill_index = 31314, cooltime = 1000, rate = -1, rangemin = 0, rangemax = 5000, target = 3, usedskill ="31319" },-- ���ɾ��� Lv2
   --{ skill_index = 31321, cooltime = 60000, rate = 100, rangemin = 0, rangemax = 4500, target = 3, selfhppercentrange = "0,100", encountertime = 45000 }, -- ������ ����
   --{ skill_index = 31319, cooltime = 110000, rate = 100, rangemin = 0, rangemax = 4500, target = 1, selfhppercentrange = "0,50", notusedskill = "31319" ,next_lua_skill_index = 0},-- "�ݳ�" �ؽ�Ʈ�׼����� ���ɾ��ý���.
   --{ skill_index = 31320, cooltime = 45000, rate = 80, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "0,25" },-- ������
   --{ skill_index = 31316, cooltime = 45000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, selfhppercentrange = "0,25" },-- 3���� ��ġ��
   --{ skill_index = 31311, cooltime = 15000, rate = 30, rangemin = 0, rangemax = 1000, target = 3, selfhppercentrange = "10,50", td = "FL,FR,LF,RF", usedskill ="31319" },-- ���ָ�ġ�� Lv2
   --{ skill_index = 31312, cooltime = 15000, rate = 30, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "10,50", td = "FL,FR,LF,RF", usedskill ="31319" },-- �����ָ�ġ�� Lv2
   --{ skill_index = 31313, cooltime = 20000, rate = 50, rangemin = 0, rangemax = 1500, target = 3, selfhppercentrange = "10,50", usedskill ="31319" },-- ������ ġ�� Lv2
   --{ skill_index = 31315, cooltime = 30000, rate = 50, rangemin = 0, rangemax = 3000, target = 3, td = "FL,FR,LF,RF,RB,BR,BL,LB", selfhppercentrange = "25,50", usedskill ="31319"  },-- ��ġ�� Lv2
   --{ skill_index = 31317, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,4", notusedskill="31319"},-- ����� ��� LV1
   --{ skill_index = 31322, cooltime = 30000, rate = 80, rangemin = 0, rangemax = 3000, target = 3, multipletarget = "1,8", usedskill="31319" }, -- ����� ��� LV2
   --{ skill_index = 31318, cooltime = 30000, rate = 40, rangemin = 0, rangemax = 1000, target = 3, td = "FL,FR,LF,RF", selfhppercentrange = "0,75",usedskill ="31319" },-- �극��
}