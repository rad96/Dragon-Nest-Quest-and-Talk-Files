--DarkLairDefence_Lv10_OgreWhite.lua

g_Lua_NearTableCount = 5

g_Lua_NearValue1 = 250;
g_Lua_NearValue2 = 400;
g_Lua_NearValue3 = 600;
g_Lua_NearValue4 = 800;
g_Lua_NearValue5 = 1500;

g_Lua_PatrolBaseTime = 5000
g_Lua_PatrolRandTime = 3000
g_Lua_ApproachValue = 120
g_Lua_AssualtTime = 5000

g_Lua_Near1 = { 
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Attack1_bash", rate = 15, loop = 1  },
   { action_name = "Attack6_HornAttack", rate = 10, loop = 1  },
   { action_name = "Walk_Back", rate = 1, loop = 1  },
}
g_Lua_Near2 = { 
   { action_name = "Walk_Front", rate = 10, loop = 2  },
   { action_name = "Stand_1", rate = 1, loop = 1  },
   { action_name = "Attack6_HornAttack", rate = 10, loop = 1  },
}
g_Lua_Near3 = { 
   { action_name = "Walk_Front", rate = 10, loop = 2  },
}
g_Lua_Near4 = { 
   { action_name = "Walk_Front", rate = 20, loop = 4  },
}
g_Lua_Near5 = { 
   { action_name = "Walk_Front", rate = 10, loop = 6  },
}
g_Lua_NonDownRangeDamage = { 
   { action_name = "Stand_1", rate = 10, loop = 1  },
   { action_name = "Stand_2", rate = 10, loop = 1  },
   { action_name = "Stand_4", rate = 10, loop = 1  },
}