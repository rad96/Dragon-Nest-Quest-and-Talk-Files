-- KoboldArcher Yellow Master AI

g_Lua_NearTableCount = 4;

g_Lua_NearValue1 = 300.0;
g_Lua_NearValue2 = 700.0;
g_Lua_NearValue3 = 1000.0;
g_Lua_NearValue4 = 1500.0;

g_Lua_LookTargetNearState = 4;

g_Lua_WanderingDistance = 1500.0;

g_Lua_PatrolBaseTime = 5000;

g_Lua_PatrolRandTime = 3000;


g_Lua_Near1 = 
{ 
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 2 },
	{ action_name = "Move_Left",	rate = 5,		loop = 1 },
	{ action_name = "Move_Right",	rate = 5,		loop = 1 },
	{ action_name = "Move_Back",	rate = 25,		loop = 2 },
	{ action_name = "Attack1",	rate = 6, 		loop = 1 },
	{ action_name = "Attack1_Miss1",	rate = 14, 		loop = 1 },
}

g_Lua_Near2 = 
{ 
	{ action_name = "Stand_1",	rate = 15,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 20,		loop = 2 },
	{ action_name = "Move_Left",	rate = 8,		loop = 2 },
	{ action_name = "Move_Right",	rate = 8,		loop = 2 },
	{ action_name = "Move_Back",	rate = 20,		loop = 2 },
	{ action_name = "Attack1",	rate = 12, 		loop = 1 },
	{ action_name = "Attack1_Miss1",	rate = 22, 		loop = 1 },
}

g_Lua_Near3 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 10,		loop = 2 },
	{ action_name = "Walk_Back",	rate = 10,		loop = 1 },
	{ action_name = "Move_Left",	rate = 10,		loop = 2 },
	{ action_name = "Move_Right",	rate = 10,		loop = 2 },
	{ action_name = "Move_Front",	rate = 10,		loop = 2 },
	{ action_name = "Move_Back",	rate = 10,		loop = 1 },
	{ action_name = "Attack1",	rate = 14, 		loop = 1 },
	{ action_name = "Attack1_Miss2",	rate = 25, 		loop = 1 },
}

g_Lua_Near4 = 
{ 
	{ action_name = "Stand_1",	rate = 10,		loop = 1 },
	{ action_name = "Walk_Left",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Right",	rate = 5,		loop = 2 },
	{ action_name = "Walk_Front",	rate = 30,		loop = 2 },
	{ action_name = "Attack1",	rate = 2, 		loop = 1 },
	{ action_name = "Attack1_Miss2",	rate = 15, 		loop = 1 },
}
