<VillageServer>

function sq15_782_skin_food_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 722 then
		sq15_782_skin_food_OnTalk_n722_cat_mewmew(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n722_cat_mewmew--------------------------------------------------------------------------------
function sq15_782_skin_food_OnTalk_n722_cat_mewmew(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n722_cat_mewmew-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n722_cat_mewmew-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n722_cat_mewmew-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n722_cat_mewmew-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7820, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7820, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7820, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7820, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7820, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7820, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7820, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7820, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7820, false);
			 end 

	end
	if npc_talk_index == "n722_cat_mewmew-accepting-acceptted" then
				api_quest_AddQuest(userObjID,782, 1);
				api_quest_SetJournalStep(userObjID,782, 1);
				api_quest_SetQuestStep(userObjID,782, 1);
				npc_talk_index = "n722_cat_mewmew-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1393, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1394, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201393, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201394, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300413, 5);

	end
	if npc_talk_index == "n722_cat_mewmew-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7820, true);
				 api_quest_RewardQuestUser(userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7820, true);
				 api_quest_RewardQuestUser(userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7820, true);
				 api_quest_RewardQuestUser(userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7820, true);
				 api_quest_RewardQuestUser(userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7820, true);
				 api_quest_RewardQuestUser(userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7820, true);
				 api_quest_RewardQuestUser(userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7820, true);
				 api_quest_RewardQuestUser(userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7820, true);
				 api_quest_RewardQuestUser(userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7820, true);
				 api_quest_RewardQuestUser(userObjID, 7820, questID, 1);
			 end 
	end
	if npc_talk_index == "n722_cat_mewmew-2-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300413, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300413, api_quest_HasQuestItem(userObjID, 300413, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_782_skin_food_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 782);
	if qstep == 1 and CountIndex == 1393 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300413, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300413, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 1394 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300413, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300413, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201393 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300413, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300413, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201394 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300413, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300413, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300413 then

	end
end

function sq15_782_skin_food_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 782);
	if qstep == 1 and CountIndex == 1393 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1394 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201393 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201394 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300413 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function sq15_782_skin_food_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 782);
	local questID=782;
end

function sq15_782_skin_food_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1393, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1394, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201393, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201394, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300413, 5);
end

function sq15_782_skin_food_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_782_skin_food_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,782, 1);
				api_quest_SetJournalStep(userObjID,782, 1);
				api_quest_SetQuestStep(userObjID,782, 1);
				npc_talk_index = "n722_cat_mewmew-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1393, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1394, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201393, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201394, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300413, 5);
end

</VillageServer>

<GameServer>
function sq15_782_skin_food_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 722 then
		sq15_782_skin_food_OnTalk_n722_cat_mewmew( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n722_cat_mewmew--------------------------------------------------------------------------------
function sq15_782_skin_food_OnTalk_n722_cat_mewmew( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n722_cat_mewmew-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n722_cat_mewmew-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n722_cat_mewmew-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n722_cat_mewmew-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, false);
			 end 

	end
	if npc_talk_index == "n722_cat_mewmew-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,782, 1);
				api_quest_SetJournalStep( pRoom, userObjID,782, 1);
				api_quest_SetQuestStep( pRoom, userObjID,782, 1);
				npc_talk_index = "n722_cat_mewmew-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1393, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1394, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201393, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201394, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300413, 5);

	end
	if npc_talk_index == "n722_cat_mewmew-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7820, questID, 1);
			 end 
	end
	if npc_talk_index == "n722_cat_mewmew-2-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300413, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300413, api_quest_HasQuestItem( pRoom, userObjID, 300413, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_782_skin_food_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 782);
	if qstep == 1 and CountIndex == 1393 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300413, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300413, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 1394 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300413, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300413, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201393 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300413, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300413, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201394 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300413, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300413, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300413 then

	end
end

function sq15_782_skin_food_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 782);
	if qstep == 1 and CountIndex == 1393 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1394 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201393 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201394 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300413 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function sq15_782_skin_food_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 782);
	local questID=782;
end

function sq15_782_skin_food_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1393, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1394, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201393, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201394, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300413, 5);
end

function sq15_782_skin_food_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_782_skin_food_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,782, 1);
				api_quest_SetJournalStep( pRoom, userObjID,782, 1);
				api_quest_SetQuestStep( pRoom, userObjID,782, 1);
				npc_talk_index = "n722_cat_mewmew-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1393, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1394, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201393, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201394, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300413, 5);
end

</GameServer>