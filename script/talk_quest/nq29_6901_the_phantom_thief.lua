<VillageServer>

function nq29_6901_the_phantom_thief_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 255 then
		nq29_6901_the_phantom_thief_OnTalk_n255_invisible_thief(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n255_invisible_thief--------------------------------------------------------------------------------
function nq29_6901_the_phantom_thief_OnTalk_n255_invisible_thief(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n255_invisible_thief-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n255_invisible_thief-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n255_invisible_thief-accepting-acceppted" then
				api_quest_AddQuest(userObjID,6901, 1);
				api_quest_SetJournalStep(userObjID,6901, 1);
				api_quest_SetQuestStep(userObjID,6901, 1);
				npc_talk_index = "n255_invisible_thief-1";

	end
	if npc_talk_index == "n255_invisible_thief-1-b" then 
	end
	if npc_talk_index == "n255_invisible_thief-1-c" then 
	end
	if npc_talk_index == "n255_invisible_thief-1-d" then 
	end
	if npc_talk_index == "n255_invisible_thief-1-e" then 
	end
	if npc_talk_index == "n255_invisible_thief-1-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 255, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq29_6901_the_phantom_thief_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6901);
end

function nq29_6901_the_phantom_thief_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6901);
end

function nq29_6901_the_phantom_thief_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6901);
	local questID=6901;
end

function nq29_6901_the_phantom_thief_OnRemoteStart( userObjID, questID )
end

function nq29_6901_the_phantom_thief_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq29_6901_the_phantom_thief_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq29_6901_the_phantom_thief_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 255 then
		nq29_6901_the_phantom_thief_OnTalk_n255_invisible_thief( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n255_invisible_thief--------------------------------------------------------------------------------
function nq29_6901_the_phantom_thief_OnTalk_n255_invisible_thief( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n255_invisible_thief-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n255_invisible_thief-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n255_invisible_thief-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,6901, 1);
				api_quest_SetJournalStep( pRoom, userObjID,6901, 1);
				api_quest_SetQuestStep( pRoom, userObjID,6901, 1);
				npc_talk_index = "n255_invisible_thief-1";

	end
	if npc_talk_index == "n255_invisible_thief-1-b" then 
	end
	if npc_talk_index == "n255_invisible_thief-1-c" then 
	end
	if npc_talk_index == "n255_invisible_thief-1-d" then 
	end
	if npc_talk_index == "n255_invisible_thief-1-e" then 
	end
	if npc_talk_index == "n255_invisible_thief-1-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 255, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq29_6901_the_phantom_thief_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6901);
end

function nq29_6901_the_phantom_thief_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6901);
end

function nq29_6901_the_phantom_thief_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6901);
	local questID=6901;
end

function nq29_6901_the_phantom_thief_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq29_6901_the_phantom_thief_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq29_6901_the_phantom_thief_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>