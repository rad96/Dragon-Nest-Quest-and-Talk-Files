<VillageServer>

function eq_2350_thx_orc_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1276 then
		eq_2350_thx_orc_OnTalk_n1276_thx_orc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1276_thx_orc--------------------------------------------------------------------------------
function eq_2350_thx_orc_OnTalk_n1276_thx_orc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1276_thx_orc-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1276_thx_orc-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1276_thx_orc-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23500, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23500, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23500, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23500, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23500, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23500, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23500, false);
			 end 

	end
	if npc_talk_index == "n1276_thx_orc-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2350, 1);
				api_quest_SetJournalStep(userObjID,2350, 1);
				api_quest_SetQuestStep(userObjID,2350, 1);
				npc_talk_index = "n1276_thx_orc-1";

	end
	if npc_talk_index == "n1276_thx_orc-1-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23500, true);
				 api_quest_RewardQuestUser(userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23500, true);
				 api_quest_RewardQuestUser(userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23500, true);
				 api_quest_RewardQuestUser(userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23500, true);
				 api_quest_RewardQuestUser(userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23500, true);
				 api_quest_RewardQuestUser(userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23500, true);
				 api_quest_RewardQuestUser(userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23500, true);
				 api_quest_RewardQuestUser(userObjID, 23500, questID, 1);
			 end 
	end
	if npc_talk_index == "n1276_thx_orc-1-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2350_thx_orc_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2350);
end

function eq_2350_thx_orc_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2350);
end

function eq_2350_thx_orc_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2350);
	local questID=2350;
end

function eq_2350_thx_orc_OnRemoteStart( userObjID, questID )
end

function eq_2350_thx_orc_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function eq_2350_thx_orc_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2350, 1);
				api_quest_SetJournalStep(userObjID,2350, 1);
				api_quest_SetQuestStep(userObjID,2350, 1);
				npc_talk_index = "n1276_thx_orc-1";
end

</VillageServer>

<GameServer>
function eq_2350_thx_orc_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1276 then
		eq_2350_thx_orc_OnTalk_n1276_thx_orc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1276_thx_orc--------------------------------------------------------------------------------
function eq_2350_thx_orc_OnTalk_n1276_thx_orc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1276_thx_orc-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1276_thx_orc-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1276_thx_orc-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, false);
			 end 

	end
	if npc_talk_index == "n1276_thx_orc-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2350, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2350, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2350, 1);
				npc_talk_index = "n1276_thx_orc-1";

	end
	if npc_talk_index == "n1276_thx_orc-1-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23500, questID, 1);
			 end 
	end
	if npc_talk_index == "n1276_thx_orc-1-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2350_thx_orc_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2350);
end

function eq_2350_thx_orc_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2350);
end

function eq_2350_thx_orc_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2350);
	local questID=2350;
end

function eq_2350_thx_orc_OnRemoteStart( pRoom,  userObjID, questID )
end

function eq_2350_thx_orc_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function eq_2350_thx_orc_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2350, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2350, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2350, 1);
				npc_talk_index = "n1276_thx_orc-1";
end

</GameServer>