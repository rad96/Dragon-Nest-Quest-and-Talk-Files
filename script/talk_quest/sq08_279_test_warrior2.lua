<VillageServer>

function sq08_279_test_warrior2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 32 then
		sq08_279_test_warrior2_OnTalk_n032_warrior_master_chandler(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n032_warrior_master_chandler--------------------------------------------------------------------------------
function sq08_279_test_warrior2_OnTalk_n032_warrior_master_chandler(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n032_warrior_master_chandler-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n032_warrior_master_chandler-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n032_warrior_master_chandler-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n032_warrior_master_chandler-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n032_warrior_master_chandler-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2790, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2790, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2790, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2790, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2790, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2790, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2790, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2790, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2790, false);
			 end 

	end
	if npc_talk_index == "n032_warrior_master_chandler-accepting-acceptted" then
				api_quest_AddQuest(userObjID,279, 1);
				api_quest_SetJournalStep(userObjID,279, 1);
				api_quest_SetQuestStep(userObjID,279, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";

	end
	if npc_talk_index == "n032_warrior_master_chandler-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300072, 1);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-a" then 
				api_quest_DelQuestItem(userObjID, 300072, 1);
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-b" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-h" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-d" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-f" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-i" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-e" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-g" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-i" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-change_class1" then 
				api_quest_SetQuestMemo(userObjID, questID, 1, 1);
				npc_talk_index = "n032_warrior_master_chandler-3-j";
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-change_class2" then 
				api_quest_SetQuestMemo(userObjID, questID, 1, 2);
				npc_talk_index = "n032_warrior_master_chandler-3-j";
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-k" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-check_changejob" then 
				if api_quest_GetQuestMemo(userObjID, questID, 1) == 1  then
									npc_talk_index = "n032_warrior_master_chandler-3-l";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2791, true);
				 api_quest_RewardQuestUser(userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2791, true);
				 api_quest_RewardQuestUser(userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2791, true);
				 api_quest_RewardQuestUser(userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2791, true);
				 api_quest_RewardQuestUser(userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2791, true);
				 api_quest_RewardQuestUser(userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2791, true);
				 api_quest_RewardQuestUser(userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2791, true);
				 api_quest_RewardQuestUser(userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2791, true);
				 api_quest_RewardQuestUser(userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2791, true);
				 api_quest_RewardQuestUser(userObjID, 2791, questID, 1);
			 end 

				else
									if api_quest_GetQuestMemo(userObjID, questID, 1) == 2  then
									npc_talk_index = "n032_warrior_master_chandler-3-m";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 end 

				else
									npc_talk_index = "n032_warrior_master_chandler-3-m";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2792, true);
				 api_quest_RewardQuestUser(userObjID, 2792, questID, 1);
			 end 

				end

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-complete_quest1" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 11);
				npc_talk_index = "n032_warrior_master_chandler-3-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-complete_quest2" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 12);
				npc_talk_index = "n032_warrior_master_chandler-3-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-o" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-complete_check" then 
				if api_quest_IsMarkingCompleteQuest(userObjID, 340) == 1  then
									npc_talk_index = "n032_warrior_master_chandler-3-p";

				else
									api_quest_AddQuest(userObjID,340, 1);
				api_quest_SetJournalStep(userObjID,340, 1);
				api_quest_SetQuestStep(userObjID,340, 1);
				api_npc_NextScript(userObjID, npcObjID, "n032_warrior_master_chandler-1", "sq08_340_change_job1.xml");

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_279_test_warrior2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 279);
	if qstep == 2 and CountIndex == 413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300072, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300072, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300072 then

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300072, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300072, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_279_test_warrior2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 279);
	if qstep == 2 and CountIndex == 413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300072 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
end

function sq08_279_test_warrior2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 279);
	local questID=279;
end

function sq08_279_test_warrior2_OnRemoteStart( userObjID, questID )
end

function sq08_279_test_warrior2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_279_test_warrior2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,279, 1);
				api_quest_SetJournalStep(userObjID,279, 1);
				api_quest_SetQuestStep(userObjID,279, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";
end

</VillageServer>

<GameServer>
function sq08_279_test_warrior2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 32 then
		sq08_279_test_warrior2_OnTalk_n032_warrior_master_chandler( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n032_warrior_master_chandler--------------------------------------------------------------------------------
function sq08_279_test_warrior2_OnTalk_n032_warrior_master_chandler( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n032_warrior_master_chandler-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n032_warrior_master_chandler-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n032_warrior_master_chandler-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n032_warrior_master_chandler-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n032_warrior_master_chandler-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2790, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2790, false);
			 end 

	end
	if npc_talk_index == "n032_warrior_master_chandler-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,279, 1);
				api_quest_SetJournalStep( pRoom, userObjID,279, 1);
				api_quest_SetQuestStep( pRoom, userObjID,279, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";

	end
	if npc_talk_index == "n032_warrior_master_chandler-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300072, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-a" then 
				api_quest_DelQuestItem( pRoom, userObjID, 300072, 1);
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-b" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-h" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-d" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-f" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-i" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-e" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-g" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-i" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-change_class1" then 
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 1);
				npc_talk_index = "n032_warrior_master_chandler-3-j";
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-change_class2" then 
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 2);
				npc_talk_index = "n032_warrior_master_chandler-3-j";
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-k" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-check_changejob" then 
				if api_quest_GetQuestMemo( pRoom, userObjID, questID, 1) == 1  then
									npc_talk_index = "n032_warrior_master_chandler-3-l";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2791, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2791, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2791, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2791, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2791, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2791, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2791, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2791, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2791, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2791, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2791, questID, 1);
			 end 

				else
									if api_quest_GetQuestMemo( pRoom, userObjID, questID, 1) == 2  then
									npc_talk_index = "n032_warrior_master_chandler-3-m";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 end 

				else
									npc_talk_index = "n032_warrior_master_chandler-3-m";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2792, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2792, questID, 1);
			 end 

				end

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-complete_quest1" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 11);
				npc_talk_index = "n032_warrior_master_chandler-3-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-complete_quest2" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 12);
				npc_talk_index = "n032_warrior_master_chandler-3-n";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-o" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-complete_check" then 
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 340) == 1  then
									npc_talk_index = "n032_warrior_master_chandler-3-p";

				else
									api_quest_AddQuest( pRoom, userObjID,340, 1);
				api_quest_SetJournalStep( pRoom, userObjID,340, 1);
				api_quest_SetQuestStep( pRoom, userObjID,340, 1);
				api_npc_NextScript( pRoom, userObjID, npcObjID, "n032_warrior_master_chandler-1", "sq08_340_change_job1.xml");

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_279_test_warrior2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 279);
	if qstep == 2 and CountIndex == 413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300072, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300072, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300072 then

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300072, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300072, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_279_test_warrior2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 279);
	if qstep == 2 and CountIndex == 413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300072 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
end

function sq08_279_test_warrior2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 279);
	local questID=279;
end

function sq08_279_test_warrior2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_279_test_warrior2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_279_test_warrior2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,279, 1);
				api_quest_SetJournalStep( pRoom, userObjID,279, 1);
				api_quest_SetQuestStep( pRoom, userObjID,279, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";
end

</GameServer>