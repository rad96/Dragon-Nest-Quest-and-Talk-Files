<VillageServer>

function sq15_4143_ghost_buster2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 41 then
		sq15_4143_ghost_buster2_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 46 then
		sq15_4143_ghost_buster2_OnTalk_n046_cleric_master_enoc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function sq15_4143_ghost_buster2_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n041_duke_stwart-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n041_duke_stwart-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-1-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-1-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-1-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400348, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700554, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700555, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 700556, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 600554, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 600555, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 600556, 30000);
	end
	if npc_talk_index == "n041_duke_stwart-3-a" then 

				if api_quest_HasQuestItem(userObjID, 400348, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400348, api_quest_HasQuestItem(userObjID, 400348, 1));
				end
	end
	if npc_talk_index == "n041_duke_stwart-3-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41430, true);
				 api_quest_RewardQuestUser(userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41430, true);
				 api_quest_RewardQuestUser(userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41430, true);
				 api_quest_RewardQuestUser(userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41430, true);
				 api_quest_RewardQuestUser(userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41430, true);
				 api_quest_RewardQuestUser(userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41430, true);
				 api_quest_RewardQuestUser(userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41430, true);
				 api_quest_RewardQuestUser(userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41430, true);
				 api_quest_RewardQuestUser(userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41430, true);
				 api_quest_RewardQuestUser(userObjID, 41430, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-3-d" then 

				if api_quest_HasQuestItem(userObjID, 400348, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400348, api_quest_HasQuestItem(userObjID, 400348, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n046_cleric_master_enoc--------------------------------------------------------------------------------
function sq15_4143_ghost_buster2_OnTalk_n046_cleric_master_enoc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n046_cleric_master_enoc-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n046_cleric_master_enoc-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41430, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41430, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41430, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41430, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41430, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41430, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41430, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41430, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41430, false);
			 end 

	end
	if npc_talk_index == "n046_cleric_master_enoc-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4143, 1);
				api_quest_SetJournalStep(userObjID,4143, 1);
				api_quest_SetQuestStep(userObjID,4143, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4143_ghost_buster2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4143);
	if qstep == 2 and CountIndex == 700554 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 700555 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 700556 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400348 then

	end
	if qstep == 2 and CountIndex == 600554 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 600555 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 600556 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4143_ghost_buster2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4143);
	if qstep == 2 and CountIndex == 700554 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700555 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700556 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400348 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 600554 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 600555 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 600556 and Count >= TargetCount  then

	end
end

function sq15_4143_ghost_buster2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4143);
	local questID=4143;
end

function sq15_4143_ghost_buster2_OnRemoteStart( userObjID, questID )
end

function sq15_4143_ghost_buster2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4143_ghost_buster2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4143, 1);
				api_quest_SetJournalStep(userObjID,4143, 1);
				api_quest_SetQuestStep(userObjID,4143, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";
end

</VillageServer>

<GameServer>
function sq15_4143_ghost_buster2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 41 then
		sq15_4143_ghost_buster2_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 46 then
		sq15_4143_ghost_buster2_OnTalk_n046_cleric_master_enoc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function sq15_4143_ghost_buster2_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n041_duke_stwart-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n041_duke_stwart-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-1-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-1-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-1-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400348, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700554, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700555, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 700556, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 600554, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 600555, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 600556, 30000);
	end
	if npc_talk_index == "n041_duke_stwart-3-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400348, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400348, api_quest_HasQuestItem( pRoom, userObjID, 400348, 1));
				end
	end
	if npc_talk_index == "n041_duke_stwart-3-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41430, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-3-d" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400348, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400348, api_quest_HasQuestItem( pRoom, userObjID, 400348, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n046_cleric_master_enoc--------------------------------------------------------------------------------
function sq15_4143_ghost_buster2_OnTalk_n046_cleric_master_enoc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n046_cleric_master_enoc-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n046_cleric_master_enoc-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41430, false);
			 end 

	end
	if npc_talk_index == "n046_cleric_master_enoc-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4143, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4143, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4143, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4143_ghost_buster2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4143);
	if qstep == 2 and CountIndex == 700554 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 700555 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 700556 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400348 then

	end
	if qstep == 2 and CountIndex == 600554 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 600555 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 600556 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400348, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400348, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4143_ghost_buster2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4143);
	if qstep == 2 and CountIndex == 700554 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700555 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700556 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400348 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 600554 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 600555 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 600556 and Count >= TargetCount  then

	end
end

function sq15_4143_ghost_buster2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4143);
	local questID=4143;
end

function sq15_4143_ghost_buster2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4143_ghost_buster2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4143_ghost_buster2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4143, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4143, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4143, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";
end

</GameServer>