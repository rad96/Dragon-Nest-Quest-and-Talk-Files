<VillageServer>

function mq11_9329_clue_of_curse_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1147 then
		mq11_9329_clue_of_curse_OnTalk_n1147_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 209 then
		mq11_9329_clue_of_curse_OnTalk_n209_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 519 then
		mq11_9329_clue_of_curse_OnTalk_n519_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 533 then
		mq11_9329_clue_of_curse_OnTalk_n533_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1147_lunaria--------------------------------------------------------------------------------
function mq11_9329_clue_of_curse_OnTalk_n1147_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1147_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1147_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1147_lunaria-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1147_lunaria-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1147_lunaria-4-b" then 
	end
	if npc_talk_index == "n1147_lunaria-4-c" then 
	end
	if npc_talk_index == "n1147_lunaria-4-d" then 
	end
	if npc_talk_index == "n1147_lunaria-4-e" then 
	end
	if npc_talk_index == "n1147_lunaria-4-f" then 
	end
	if npc_talk_index == "n1147_lunaria-4-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93290, true);
				 api_quest_RewardQuestUser(userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93290, true);
				 api_quest_RewardQuestUser(userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93290, true);
				 api_quest_RewardQuestUser(userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93290, true);
				 api_quest_RewardQuestUser(userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93290, true);
				 api_quest_RewardQuestUser(userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93290, true);
				 api_quest_RewardQuestUser(userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93290, true);
				 api_quest_RewardQuestUser(userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93290, true);
				 api_quest_RewardQuestUser(userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93290, true);
				 api_quest_RewardQuestUser(userObjID, 93290, questID, 1);
			 end 
	end
	if npc_talk_index == "n1147_lunaria-4-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9727, 2);
					api_quest_SetQuestStep(userObjID, 9727, 1);
					api_quest_SetJournalStep(userObjID, 9727, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n209_elite_soldier--------------------------------------------------------------------------------
function mq11_9329_clue_of_curse_OnTalk_n209_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n209_elite_soldier-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n209_elite_soldier-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n209_elite_soldier-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9329, 2);
				api_quest_SetJournalStep(userObjID,9329, 1);
				api_quest_SetQuestStep(userObjID,9329, 1);
				npc_talk_index = "n209_elite_soldier-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n519_cian--------------------------------------------------------------------------------
function mq11_9329_clue_of_curse_OnTalk_n519_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n519_cian-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n519_cian-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n519_cian-1-b" then 
	end
	if npc_talk_index == "n519_cian-1-c" then 
	end
	if npc_talk_index == "n519_cian-1-d" then 
	end
	if npc_talk_index == "n519_cian-1-e" then 
	end
	if npc_talk_index == "n519_cian-1-f" then 
	end
	if npc_talk_index == "n519_cian-1-g" then 
	end
	if npc_talk_index == "n519_cian-1-h" then 
	end
	if npc_talk_index == "n519_cian-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1172, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201172, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300250, 1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n533_triana--------------------------------------------------------------------------------
function mq11_9329_clue_of_curse_OnTalk_n533_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n533_triana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n533_triana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n533_triana-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n533_triana-3-b" then 
	end
	if npc_talk_index == "n533_triana-3-c" then 
	end
	if npc_talk_index == "n533_triana-3-c" then 
	end
	if npc_talk_index == "n533_triana-3-d" then 
	end
	if npc_talk_index == "n533_triana-3-e" then 
	end
	if npc_talk_index == "n533_triana-3-f" then 
	end
	if npc_talk_index == "n533_triana-3-f" then 
	end
	if npc_talk_index == "n533_triana-3-f" then 
	end
	if npc_talk_index == "n533_triana-3-g" then 
	end
	if npc_talk_index == "n533_triana-3-h" then 
	end
	if npc_talk_index == "n533_triana-3-i" then 
	end
	if npc_talk_index == "n533_triana-3-j" then 
	end
	if npc_talk_index == "n533_triana-3-k" then 
	end
	if npc_talk_index == "n533_triana-3-l" then 
	end
	if npc_talk_index == "n533_triana-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

				if api_quest_HasQuestItem(userObjID, 300250, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300250, api_quest_HasQuestItem(userObjID, 300250, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9329_clue_of_curse_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9329);
	if qstep == 2 and CountIndex == 1172 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300250, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300250, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201172 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300250, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300250, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300250 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function mq11_9329_clue_of_curse_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9329);
	if qstep == 2 and CountIndex == 1172 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201172 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300250 and Count >= TargetCount  then

	end
end

function mq11_9329_clue_of_curse_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9329);
	local questID=9329;
end

function mq11_9329_clue_of_curse_OnRemoteStart( userObjID, questID )
end

function mq11_9329_clue_of_curse_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_9329_clue_of_curse_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9329, 2);
				api_quest_SetJournalStep(userObjID,9329, 1);
				api_quest_SetQuestStep(userObjID,9329, 1);
				npc_talk_index = "n209_elite_soldier-1";
end

</VillageServer>

<GameServer>
function mq11_9329_clue_of_curse_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1147 then
		mq11_9329_clue_of_curse_OnTalk_n1147_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 209 then
		mq11_9329_clue_of_curse_OnTalk_n209_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 519 then
		mq11_9329_clue_of_curse_OnTalk_n519_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 533 then
		mq11_9329_clue_of_curse_OnTalk_n533_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1147_lunaria--------------------------------------------------------------------------------
function mq11_9329_clue_of_curse_OnTalk_n1147_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1147_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1147_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1147_lunaria-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1147_lunaria-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1147_lunaria-4-b" then 
	end
	if npc_talk_index == "n1147_lunaria-4-c" then 
	end
	if npc_talk_index == "n1147_lunaria-4-d" then 
	end
	if npc_talk_index == "n1147_lunaria-4-e" then 
	end
	if npc_talk_index == "n1147_lunaria-4-f" then 
	end
	if npc_talk_index == "n1147_lunaria-4-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93290, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93290, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93290, questID, 1);
			 end 
	end
	if npc_talk_index == "n1147_lunaria-4-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9727, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9727, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9727, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n209_elite_soldier--------------------------------------------------------------------------------
function mq11_9329_clue_of_curse_OnTalk_n209_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n209_elite_soldier-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n209_elite_soldier-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n209_elite_soldier-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9329, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9329, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9329, 1);
				npc_talk_index = "n209_elite_soldier-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n519_cian--------------------------------------------------------------------------------
function mq11_9329_clue_of_curse_OnTalk_n519_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n519_cian-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n519_cian-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n519_cian-1-b" then 
	end
	if npc_talk_index == "n519_cian-1-c" then 
	end
	if npc_talk_index == "n519_cian-1-d" then 
	end
	if npc_talk_index == "n519_cian-1-e" then 
	end
	if npc_talk_index == "n519_cian-1-f" then 
	end
	if npc_talk_index == "n519_cian-1-g" then 
	end
	if npc_talk_index == "n519_cian-1-h" then 
	end
	if npc_talk_index == "n519_cian-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1172, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201172, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300250, 1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n533_triana--------------------------------------------------------------------------------
function mq11_9329_clue_of_curse_OnTalk_n533_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n533_triana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n533_triana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n533_triana-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n533_triana-3-b" then 
	end
	if npc_talk_index == "n533_triana-3-c" then 
	end
	if npc_talk_index == "n533_triana-3-c" then 
	end
	if npc_talk_index == "n533_triana-3-d" then 
	end
	if npc_talk_index == "n533_triana-3-e" then 
	end
	if npc_talk_index == "n533_triana-3-f" then 
	end
	if npc_talk_index == "n533_triana-3-f" then 
	end
	if npc_talk_index == "n533_triana-3-f" then 
	end
	if npc_talk_index == "n533_triana-3-g" then 
	end
	if npc_talk_index == "n533_triana-3-h" then 
	end
	if npc_talk_index == "n533_triana-3-i" then 
	end
	if npc_talk_index == "n533_triana-3-j" then 
	end
	if npc_talk_index == "n533_triana-3-k" then 
	end
	if npc_talk_index == "n533_triana-3-l" then 
	end
	if npc_talk_index == "n533_triana-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

				if api_quest_HasQuestItem( pRoom, userObjID, 300250, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300250, api_quest_HasQuestItem( pRoom, userObjID, 300250, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9329_clue_of_curse_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9329);
	if qstep == 2 and CountIndex == 1172 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300250, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300250, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201172 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300250, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300250, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300250 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function mq11_9329_clue_of_curse_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9329);
	if qstep == 2 and CountIndex == 1172 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201172 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300250 and Count >= TargetCount  then

	end
end

function mq11_9329_clue_of_curse_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9329);
	local questID=9329;
end

function mq11_9329_clue_of_curse_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_9329_clue_of_curse_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_9329_clue_of_curse_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9329, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9329, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9329, 1);
				npc_talk_index = "n209_elite_soldier-1";
end

</GameServer>