<VillageServer>

function nq27_6801_destruchtion_of_evidence_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 42 then
		nq27_6801_destruchtion_of_evidence_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function nq27_6801_destruchtion_of_evidence_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n042_general_duglars-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 68010, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 68010, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 68010, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 68010, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 68010, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 68010, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 68010, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 68010, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 68010, false);
			 end 

	end
	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest(userObjID,6801, 1);
				api_quest_SetJournalStep(userObjID,6801, 1);
				api_quest_SetQuestStep(userObjID,6801, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n042_general_duglars-4-b" then 
	end
	if npc_talk_index == "n042_general_duglars-4-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-4-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 42, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n042_general_duglars-4-e" then 
	end
	if npc_talk_index == "n042_general_duglars-4-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 68010, true);
				 api_quest_RewardQuestUser(userObjID, 68010, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-4-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 42, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq27_6801_destruchtion_of_evidence_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6701);
end

function nq27_6801_destruchtion_of_evidence_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6701);
end

function nq27_6801_destruchtion_of_evidence_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6701);
	local questID=6701;
end

function nq27_6801_destruchtion_of_evidence_OnRemoteStart( userObjID, questID )
end

function nq27_6801_destruchtion_of_evidence_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq27_6801_destruchtion_of_evidence_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,6801, 1);
				api_quest_SetJournalStep(userObjID,6801, 1);
				api_quest_SetQuestStep(userObjID,6801, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</VillageServer>

<GameServer>
function nq27_6801_destruchtion_of_evidence_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 42 then
		nq27_6801_destruchtion_of_evidence_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function nq27_6801_destruchtion_of_evidence_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n042_general_duglars-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, false);
			 end 

	end
	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,6801, 1);
				api_quest_SetJournalStep( pRoom, userObjID,6801, 1);
				api_quest_SetQuestStep( pRoom, userObjID,6801, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n042_general_duglars-4-b" then 
	end
	if npc_talk_index == "n042_general_duglars-4-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-4-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 42, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n042_general_duglars-4-e" then 
	end
	if npc_talk_index == "n042_general_duglars-4-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 68010, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 68010, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-4-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 42, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq27_6801_destruchtion_of_evidence_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6701);
end

function nq27_6801_destruchtion_of_evidence_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6701);
end

function nq27_6801_destruchtion_of_evidence_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6701);
	local questID=6701;
end

function nq27_6801_destruchtion_of_evidence_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq27_6801_destruchtion_of_evidence_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq27_6801_destruchtion_of_evidence_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,6801, 1);
				api_quest_SetJournalStep( pRoom, userObjID,6801, 1);
				api_quest_SetQuestStep( pRoom, userObjID,6801, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</GameServer>