<VillageServer>

function dq08_1111_tears_basin_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 778 then
		dq08_1111_tears_basin_OnTalk_n778_bluff_dealers_jeff(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n778_bluff_dealers_jeff--------------------------------------------------------------------------------
function dq08_1111_tears_basin_OnTalk_n778_bluff_dealers_jeff(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n778_bluff_dealers_jeff-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n778_bluff_dealers_jeff-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n778_bluff_dealers_jeff-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n778_bluff_dealers_jeff-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 11110, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 11110, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 11110, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 11110, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 11110, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 11110, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 11110, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 11110, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 11110, false);
			 end 

	end
	if npc_talk_index == "n778_bluff_dealers_jeff-accepting-acceptted" then
				api_quest_AddQuest(userObjID,1111, 1);
				api_quest_SetJournalStep(userObjID,1111, 1);
				api_quest_SetQuestStep(userObjID,1111, 1);
				npc_talk_index = "n778_bluff_dealers_jeff-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 244, 1 );

	end
	if npc_talk_index == "n778_bluff_dealers_jeff-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 11110, true);
				 api_quest_RewardQuestUser(userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 11110, true);
				 api_quest_RewardQuestUser(userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 11110, true);
				 api_quest_RewardQuestUser(userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 11110, true);
				 api_quest_RewardQuestUser(userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 11110, true);
				 api_quest_RewardQuestUser(userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 11110, true);
				 api_quest_RewardQuestUser(userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 11110, true);
				 api_quest_RewardQuestUser(userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 11110, true);
				 api_quest_RewardQuestUser(userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 11110, true);
				 api_quest_RewardQuestUser(userObjID, 11110, questID, 1);
			 end 
	end
	if npc_talk_index == "n778_bluff_dealers_jeff-2-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq08_1111_tears_basin_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1111);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( userObjID ) >= 0 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end
		 end 
	 end 
	if qstep == 1 and CountIndex == 300510 then

	end
end

function dq08_1111_tears_basin_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1111);
	if qstep == 1 and CountIndex == 300510 and Count >= TargetCount  then

	end
end

function dq08_1111_tears_basin_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1111);
	local questID=1111;
end

function dq08_1111_tears_basin_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 244, 1 );
end

function dq08_1111_tears_basin_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function dq08_1111_tears_basin_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,1111, 1);
				api_quest_SetJournalStep(userObjID,1111, 1);
				api_quest_SetQuestStep(userObjID,1111, 1);
				npc_talk_index = "n778_bluff_dealers_jeff-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 244, 1 );
end

</VillageServer>

<GameServer>
function dq08_1111_tears_basin_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 778 then
		dq08_1111_tears_basin_OnTalk_n778_bluff_dealers_jeff( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n778_bluff_dealers_jeff--------------------------------------------------------------------------------
function dq08_1111_tears_basin_OnTalk_n778_bluff_dealers_jeff( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n778_bluff_dealers_jeff-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n778_bluff_dealers_jeff-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n778_bluff_dealers_jeff-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n778_bluff_dealers_jeff-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, false);
			 end 

	end
	if npc_talk_index == "n778_bluff_dealers_jeff-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,1111, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1111, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1111, 1);
				npc_talk_index = "n778_bluff_dealers_jeff-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 244, 1 );

	end
	if npc_talk_index == "n778_bluff_dealers_jeff-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 11110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 11110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 11110, questID, 1);
			 end 
	end
	if npc_talk_index == "n778_bluff_dealers_jeff-2-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq08_1111_tears_basin_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1111);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( pRoom,  userObjID ) >= 0 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end
		 end 
	 end 
	if qstep == 1 and CountIndex == 300510 then

	end
end

function dq08_1111_tears_basin_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1111);
	if qstep == 1 and CountIndex == 300510 and Count >= TargetCount  then

	end
end

function dq08_1111_tears_basin_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1111);
	local questID=1111;
end

function dq08_1111_tears_basin_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 244, 1 );
end

function dq08_1111_tears_basin_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function dq08_1111_tears_basin_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,1111, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1111, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1111, 1);
				npc_talk_index = "n778_bluff_dealers_jeff-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 244, 1 );
end

</GameServer>