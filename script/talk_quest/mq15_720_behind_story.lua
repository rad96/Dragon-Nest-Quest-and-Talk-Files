<VillageServer>

function mq15_720_behind_story_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 701 then
		mq15_720_behind_story_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 727 then
		mq15_720_behind_story_OnTalk_n727_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_720_behind_story_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest(userObjID,720, 2);
				api_quest_SetJournalStep(userObjID,720, 1);
				api_quest_SetQuestStep(userObjID,720, 1);
				npc_talk_index = "n701_oldkarakule-1";
				api_quest_SetQuestStep(userObjID, questID,2);

	end
	if npc_talk_index == "n701_oldkarakule-4-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7201, true);
				 api_quest_RewardQuestUser(userObjID, 7201, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7202, true);
				 api_quest_RewardQuestUser(userObjID, 7202, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7203, true);
				 api_quest_RewardQuestUser(userObjID, 7203, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7204, true);
				 api_quest_RewardQuestUser(userObjID, 7204, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7205, true);
				 api_quest_RewardQuestUser(userObjID, 7205, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7206, true);
				 api_quest_RewardQuestUser(userObjID, 7206, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7201, true);
				 api_quest_RewardQuestUser(userObjID, 7201, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7201, true);
				 api_quest_RewardQuestUser(userObjID, 7201, questID, 1);
			 end 
	end
	if npc_talk_index == "n701_oldkarakule-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 721, 2);
					api_quest_SetQuestStep(userObjID, 721, 1);
					api_quest_SetJournalStep(userObjID, 721, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n701_oldkarakule-1", "mq15_721_native_medicine.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n727_velskud--------------------------------------------------------------------------------
function mq15_720_behind_story_OnTalk_n727_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n727_velskud-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n727_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n727_velskud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n727_velskud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n727_velskud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n727_velskud-3-b" then 
	end
	if npc_talk_index == "n727_velskud-3-c" then 
	end
	if npc_talk_index == "n727_velskud-3-d" then 
	end
	if npc_talk_index == "n727_velskud-3-e" then 
	end
	if npc_talk_index == "n727_velskud-3-f" then 
	end
	if npc_talk_index == "n727_velskud-3-g" then 
	end
	if npc_talk_index == "n727_velskud-3-h" then 
	end
	if npc_talk_index == "n727_velskud-3-i" then 
	end
	if npc_talk_index == "n727_velskud-3-j" then 
	end
	if npc_talk_index == "n727_velskud-3-k" then 
	end
	if npc_talk_index == "n727_velskud-3-l" then 
	end
	if npc_talk_index == "n727_velskud-3-m" then 
	end
	if npc_talk_index == "n727_velskud-3-n" then 
	end
	if npc_talk_index == "n727_velskud-3-o" then 
	end
	if npc_talk_index == "n727_velskud-3-p" then 
	end
	if npc_talk_index == "n727_velskud-3-q" then 
	end
	if npc_talk_index == "n727_velskud-3-r" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_720_behind_story_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 720);
end

function mq15_720_behind_story_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 720);
end

function mq15_720_behind_story_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 720);
	local questID=720;
end

function mq15_720_behind_story_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,2);
end

function mq15_720_behind_story_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_720_behind_story_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,720, 2);
				api_quest_SetJournalStep(userObjID,720, 1);
				api_quest_SetQuestStep(userObjID,720, 1);
				npc_talk_index = "n701_oldkarakule-1";
				api_quest_SetQuestStep(userObjID, questID,2);
end

</VillageServer>

<GameServer>
function mq15_720_behind_story_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 701 then
		mq15_720_behind_story_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 727 then
		mq15_720_behind_story_OnTalk_n727_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_720_behind_story_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,720, 2);
				api_quest_SetJournalStep( pRoom, userObjID,720, 1);
				api_quest_SetQuestStep( pRoom, userObjID,720, 1);
				npc_talk_index = "n701_oldkarakule-1";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);

	end
	if npc_talk_index == "n701_oldkarakule-4-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7201, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7201, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7202, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7202, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7203, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7203, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7204, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7204, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7205, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7205, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7206, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7206, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7201, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7201, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7201, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7201, questID, 1);
			 end 
	end
	if npc_talk_index == "n701_oldkarakule-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 721, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 721, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 721, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n701_oldkarakule-1", "mq15_721_native_medicine.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n727_velskud--------------------------------------------------------------------------------
function mq15_720_behind_story_OnTalk_n727_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n727_velskud-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n727_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n727_velskud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n727_velskud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n727_velskud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n727_velskud-3-b" then 
	end
	if npc_talk_index == "n727_velskud-3-c" then 
	end
	if npc_talk_index == "n727_velskud-3-d" then 
	end
	if npc_talk_index == "n727_velskud-3-e" then 
	end
	if npc_talk_index == "n727_velskud-3-f" then 
	end
	if npc_talk_index == "n727_velskud-3-g" then 
	end
	if npc_talk_index == "n727_velskud-3-h" then 
	end
	if npc_talk_index == "n727_velskud-3-i" then 
	end
	if npc_talk_index == "n727_velskud-3-j" then 
	end
	if npc_talk_index == "n727_velskud-3-k" then 
	end
	if npc_talk_index == "n727_velskud-3-l" then 
	end
	if npc_talk_index == "n727_velskud-3-m" then 
	end
	if npc_talk_index == "n727_velskud-3-n" then 
	end
	if npc_talk_index == "n727_velskud-3-o" then 
	end
	if npc_talk_index == "n727_velskud-3-p" then 
	end
	if npc_talk_index == "n727_velskud-3-q" then 
	end
	if npc_talk_index == "n727_velskud-3-r" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_720_behind_story_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 720);
end

function mq15_720_behind_story_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 720);
end

function mq15_720_behind_story_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 720);
	local questID=720;
end

function mq15_720_behind_story_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
end

function mq15_720_behind_story_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_720_behind_story_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,720, 2);
				api_quest_SetJournalStep( pRoom, userObjID,720, 1);
				api_quest_SetQuestStep( pRoom, userObjID,720, 1);
				npc_talk_index = "n701_oldkarakule-1";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
end

</GameServer>