<VillageServer>

function nq32_8608_based_on_grades_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 62 then
		nq32_8608_based_on_grades_OnTalk_n062_trainer_lindsay(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n062_trainer_lindsay--------------------------------------------------------------------------------
function nq32_8608_based_on_grades_OnTalk_n062_trainer_lindsay(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n062_trainer_lindsay-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n062_trainer_lindsay-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n062_trainer_lindsay-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n062_trainer_lindsay-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n062_trainer_lindsay-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 86080, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 86080, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 86080, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 86080, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 86080, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 86080, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 86080, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 86080, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 86080, false);
			 end 

	end
	if npc_talk_index == "n062_trainer_lindsay-accepting-acceppted" then
				api_quest_AddQuest(userObjID,8608, 1);
				api_quest_SetJournalStep(userObjID,8608, 1);
				api_quest_SetQuestStep(userObjID,8608, 1);
				npc_talk_index = "n062_trainer_lindsay-1";

	end
	if npc_talk_index == "n062_trainer_lindsay-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n062_trainer_lindsay-2-c" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-d" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-e" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-f" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-g" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-h" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-i" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-j" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 86080, true);
				 api_quest_RewardQuestUser(userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 86080, true);
				 api_quest_RewardQuestUser(userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 86080, true);
				 api_quest_RewardQuestUser(userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 86080, true);
				 api_quest_RewardQuestUser(userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 86080, true);
				 api_quest_RewardQuestUser(userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 86080, true);
				 api_quest_RewardQuestUser(userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 86080, true);
				 api_quest_RewardQuestUser(userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 86080, true);
				 api_quest_RewardQuestUser(userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 86080, true);
				 api_quest_RewardQuestUser(userObjID, 86080, questID, 1);
			 end 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 62, 600 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq32_8608_based_on_grades_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8608);
end

function nq32_8608_based_on_grades_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8608);
end

function nq32_8608_based_on_grades_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8608);
	local questID=8608;
end

function nq32_8608_based_on_grades_OnRemoteStart( userObjID, questID )
end

function nq32_8608_based_on_grades_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq32_8608_based_on_grades_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq32_8608_based_on_grades_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 62 then
		nq32_8608_based_on_grades_OnTalk_n062_trainer_lindsay( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n062_trainer_lindsay--------------------------------------------------------------------------------
function nq32_8608_based_on_grades_OnTalk_n062_trainer_lindsay( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n062_trainer_lindsay-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n062_trainer_lindsay-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n062_trainer_lindsay-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n062_trainer_lindsay-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n062_trainer_lindsay-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, false);
			 end 

	end
	if npc_talk_index == "n062_trainer_lindsay-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,8608, 1);
				api_quest_SetJournalStep( pRoom, userObjID,8608, 1);
				api_quest_SetQuestStep( pRoom, userObjID,8608, 1);
				npc_talk_index = "n062_trainer_lindsay-1";

	end
	if npc_talk_index == "n062_trainer_lindsay-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n062_trainer_lindsay-2-c" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-d" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-e" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-f" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-g" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-h" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-i" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-j" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n062_trainer_lindsay-2-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86080, questID, 1);
			 end 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 62, 600 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq32_8608_based_on_grades_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8608);
end

function nq32_8608_based_on_grades_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8608);
end

function nq32_8608_based_on_grades_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8608);
	local questID=8608;
end

function nq32_8608_based_on_grades_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq32_8608_based_on_grades_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq32_8608_based_on_grades_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>