<VillageServer>

function sqc14_4584_shock_therapy_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1504 then
		sqc14_4584_shock_therapy_OnTalk_n1504_hound_ghost(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 92 then
		sqc14_4584_shock_therapy_OnTalk_n092_trader_kelly(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1504_hound_ghost--------------------------------------------------------------------------------
function sqc14_4584_shock_therapy_OnTalk_n1504_hound_ghost(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1504_hound_ghost-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1504_hound_ghost-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1504_hound_ghost-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1504_hound_ghost-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45840, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45840, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45840, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45840, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45840, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45840, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45840, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45840, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45840, false);
			 end 

	end
	if npc_talk_index == "n1504_hound_ghost-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4584, 1);
				api_quest_SetJournalStep(userObjID,4584, 1);
				api_quest_SetQuestStep(userObjID,4584, 1);
				npc_talk_index = "n1504_hound_ghost-1";

	end
	if npc_talk_index == "n1504_hound_ghost-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n092_trader_kelly--------------------------------------------------------------------------------
function sqc14_4584_shock_therapy_OnTalk_n092_trader_kelly(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n092_trader_kelly-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n092_trader_kelly-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n092_trader_kelly-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n092_trader_kelly-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n092_trader_kelly-2-b" then 
	end
	if npc_talk_index == "n092_trader_kelly-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 602120, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 702120, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400444, 1);
	end
	if npc_talk_index == "n092_trader_kelly-4-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45840, true);
				 api_quest_RewardQuestUser(userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45840, true);
				 api_quest_RewardQuestUser(userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45840, true);
				 api_quest_RewardQuestUser(userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45840, true);
				 api_quest_RewardQuestUser(userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45840, true);
				 api_quest_RewardQuestUser(userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45840, true);
				 api_quest_RewardQuestUser(userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45840, true);
				 api_quest_RewardQuestUser(userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45840, true);
				 api_quest_RewardQuestUser(userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45840, true);
				 api_quest_RewardQuestUser(userObjID, 45840, questID, 1);
			 end 
	end
	if npc_talk_index == "n092_trader_kelly-4-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4585, 1);
					api_quest_SetQuestStep(userObjID, 4585, 1);
					api_quest_SetJournalStep(userObjID, 4585, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4584_shock_therapy_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4584);
	if qstep == 3 and CountIndex == 602120 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400444, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400444, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 702120 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400444, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400444, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 400444 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

	end
end

function sqc14_4584_shock_therapy_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4584);
	if qstep == 3 and CountIndex == 602120 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 702120 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 400444 and Count >= TargetCount  then

	end
end

function sqc14_4584_shock_therapy_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4584);
	local questID=4584;
end

function sqc14_4584_shock_therapy_OnRemoteStart( userObjID, questID )
end

function sqc14_4584_shock_therapy_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc14_4584_shock_therapy_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4584, 1);
				api_quest_SetJournalStep(userObjID,4584, 1);
				api_quest_SetQuestStep(userObjID,4584, 1);
				npc_talk_index = "n1504_hound_ghost-1";
end

</VillageServer>

<GameServer>
function sqc14_4584_shock_therapy_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1504 then
		sqc14_4584_shock_therapy_OnTalk_n1504_hound_ghost( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 92 then
		sqc14_4584_shock_therapy_OnTalk_n092_trader_kelly( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1504_hound_ghost--------------------------------------------------------------------------------
function sqc14_4584_shock_therapy_OnTalk_n1504_hound_ghost( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1504_hound_ghost-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1504_hound_ghost-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1504_hound_ghost-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1504_hound_ghost-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, false);
			 end 

	end
	if npc_talk_index == "n1504_hound_ghost-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4584, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4584, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4584, 1);
				npc_talk_index = "n1504_hound_ghost-1";

	end
	if npc_talk_index == "n1504_hound_ghost-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n092_trader_kelly--------------------------------------------------------------------------------
function sqc14_4584_shock_therapy_OnTalk_n092_trader_kelly( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n092_trader_kelly-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n092_trader_kelly-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n092_trader_kelly-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n092_trader_kelly-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n092_trader_kelly-2-b" then 
	end
	if npc_talk_index == "n092_trader_kelly-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 602120, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 702120, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400444, 1);
	end
	if npc_talk_index == "n092_trader_kelly-4-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45840, questID, 1);
			 end 
	end
	if npc_talk_index == "n092_trader_kelly-4-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4585, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4585, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4585, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4584_shock_therapy_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4584);
	if qstep == 3 and CountIndex == 602120 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400444, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400444, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 702120 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400444, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400444, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 400444 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

	end
end

function sqc14_4584_shock_therapy_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4584);
	if qstep == 3 and CountIndex == 602120 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 702120 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 400444 and Count >= TargetCount  then

	end
end

function sqc14_4584_shock_therapy_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4584);
	local questID=4584;
end

function sqc14_4584_shock_therapy_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc14_4584_shock_therapy_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc14_4584_shock_therapy_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4584, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4584, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4584, 1);
				npc_talk_index = "n1504_hound_ghost-1";
end

</GameServer>