<VillageServer>

function nq12_7201_blacksmith_family_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 91 then
		nq12_7201_blacksmith_family_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n091_trader_bellin--------------------------------------------------------------------------------
function nq12_7201_blacksmith_family_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,7201, 1);
				api_quest_SetJournalStep(userObjID,7201, 1);
				api_quest_SetQuestStep(userObjID,7201, 1);
				npc_talk_index = "n091_trader_bellin-1";

	end
	if npc_talk_index == "n091_trader_bellin-1-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-c" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-d" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-e" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-f" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-g" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-h" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-i" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-j" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-k" then 
	end
	if npc_talk_index == "n091_trader_bellin-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n091_trader_bellin-2-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-2-c" then 
	end
	if npc_talk_index == "n091_trader_bellin-2-d" then 
	end
	if npc_talk_index == "n091_trader_bellin-2-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 91, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq12_7201_blacksmith_family_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7201);
end

function nq12_7201_blacksmith_family_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7201);
end

function nq12_7201_blacksmith_family_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7201);
	local questID=7201;
end

function nq12_7201_blacksmith_family_OnRemoteStart( userObjID, questID )
end

function nq12_7201_blacksmith_family_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq12_7201_blacksmith_family_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,7201, 1);
				api_quest_SetJournalStep(userObjID,7201, 1);
				api_quest_SetQuestStep(userObjID,7201, 1);
				npc_talk_index = "n091_trader_bellin-1";
end

</VillageServer>

<GameServer>
function nq12_7201_blacksmith_family_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 91 then
		nq12_7201_blacksmith_family_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n091_trader_bellin--------------------------------------------------------------------------------
function nq12_7201_blacksmith_family_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,7201, 1);
				api_quest_SetJournalStep( pRoom, userObjID,7201, 1);
				api_quest_SetQuestStep( pRoom, userObjID,7201, 1);
				npc_talk_index = "n091_trader_bellin-1";

	end
	if npc_talk_index == "n091_trader_bellin-1-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-c" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-d" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-e" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-f" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-g" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-h" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-i" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-j" then 
	end
	if npc_talk_index == "n091_trader_bellin-1-k" then 
	end
	if npc_talk_index == "n091_trader_bellin-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n091_trader_bellin-2-b" then 
	end
	if npc_talk_index == "n091_trader_bellin-2-c" then 
	end
	if npc_talk_index == "n091_trader_bellin-2-d" then 
	end
	if npc_talk_index == "n091_trader_bellin-2-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 91, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq12_7201_blacksmith_family_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7201);
end

function nq12_7201_blacksmith_family_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7201);
end

function nq12_7201_blacksmith_family_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7201);
	local questID=7201;
end

function nq12_7201_blacksmith_family_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq12_7201_blacksmith_family_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq12_7201_blacksmith_family_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,7201, 1);
				api_quest_SetJournalStep( pRoom, userObjID,7201, 1);
				api_quest_SetQuestStep( pRoom, userObjID,7201, 1);
				npc_talk_index = "n091_trader_bellin-1";
end

</GameServer>