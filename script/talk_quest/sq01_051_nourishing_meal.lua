<VillageServer>

function sq01_051_nourishing_meal_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 2 then
		sq01_051_nourishing_meal_OnTalk_n002_trader_dorin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 3 then
		sq01_051_nourishing_meal_OnTalk_n003_trader_dolis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n002_trader_dorin--------------------------------------------------------------------------------
function sq01_051_nourishing_meal_OnTalk_n002_trader_dorin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n002_trader_dorin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n002_trader_dorin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n002_trader_dorin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n002_trader_dorin-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 511, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 512, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 512, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 512, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 511, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 516, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 517, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 511, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 511, false);
			 end 

	end
	if npc_talk_index == "n002_trader_dorin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,51, 1);
				api_quest_SetJournalStep(userObjID,51, 1);
				api_quest_SetQuestStep(userObjID,51, 1);
				npc_talk_index = "n002_trader_dorin-1";

	end
	if npc_talk_index == "n002_trader_dorin-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 511, true);
				 api_quest_RewardQuestUser(userObjID, 511, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 512, true);
				 api_quest_RewardQuestUser(userObjID, 512, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 512, true);
				 api_quest_RewardQuestUser(userObjID, 512, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 512, true);
				 api_quest_RewardQuestUser(userObjID, 512, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 511, true);
				 api_quest_RewardQuestUser(userObjID, 511, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 516, true);
				 api_quest_RewardQuestUser(userObjID, 516, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 517, true);
				 api_quest_RewardQuestUser(userObjID, 517, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 511, true);
				 api_quest_RewardQuestUser(userObjID, 511, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 511, true);
				 api_quest_RewardQuestUser(userObjID, 511, questID, 1);
			 end 
	end
	if npc_talk_index == "n002_trader_dorin-4-c" then 

				if api_quest_HasQuestItem(userObjID, 300066, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300066, api_quest_HasQuestItem(userObjID, 300066, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n003_trader_dolis--------------------------------------------------------------------------------
function sq01_051_nourishing_meal_OnTalk_n003_trader_dolis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n003_trader_dolis-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n003_trader_dolis-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n003_trader_dolis-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n003_trader_dolis-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n003_trader_dolis-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n003_trader_dolis-1-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-c" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-d" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-e" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-f" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-g" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-h" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-i" then 
	end
	if npc_talk_index == "n003_trader_dolis-2" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 105, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300065, 10);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n003_trader_dolis-3-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

				if api_quest_HasQuestItem(userObjID, 300065, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300065, api_quest_HasQuestItem(userObjID, 300065, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300066, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300066, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_051_nourishing_meal_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 51);
	if qstep == 2 and CountIndex == 111 then

	end
	if qstep == 2 and CountIndex == 105 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300065, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300065, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300065 then

	end
end

function sq01_051_nourishing_meal_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 51);
	if qstep == 2 and CountIndex == 111 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 105 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300065 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function sq01_051_nourishing_meal_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 51);
	local questID=51;
end

function sq01_051_nourishing_meal_OnRemoteStart( userObjID, questID )
end

function sq01_051_nourishing_meal_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq01_051_nourishing_meal_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,51, 1);
				api_quest_SetJournalStep(userObjID,51, 1);
				api_quest_SetQuestStep(userObjID,51, 1);
				npc_talk_index = "n002_trader_dorin-1";
end

</VillageServer>

<GameServer>
function sq01_051_nourishing_meal_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 2 then
		sq01_051_nourishing_meal_OnTalk_n002_trader_dorin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 3 then
		sq01_051_nourishing_meal_OnTalk_n003_trader_dolis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n002_trader_dorin--------------------------------------------------------------------------------
function sq01_051_nourishing_meal_OnTalk_n002_trader_dorin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n002_trader_dorin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n002_trader_dorin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n002_trader_dorin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n002_trader_dorin-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 511, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 512, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 512, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 512, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 511, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 516, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 517, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 511, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 511, false);
			 end 

	end
	if npc_talk_index == "n002_trader_dorin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,51, 1);
				api_quest_SetJournalStep( pRoom, userObjID,51, 1);
				api_quest_SetQuestStep( pRoom, userObjID,51, 1);
				npc_talk_index = "n002_trader_dorin-1";

	end
	if npc_talk_index == "n002_trader_dorin-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 511, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 511, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 512, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 512, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 512, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 512, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 512, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 512, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 511, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 511, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 516, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 516, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 517, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 517, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 511, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 511, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 511, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 511, questID, 1);
			 end 
	end
	if npc_talk_index == "n002_trader_dorin-4-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300066, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300066, api_quest_HasQuestItem( pRoom, userObjID, 300066, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n003_trader_dolis--------------------------------------------------------------------------------
function sq01_051_nourishing_meal_OnTalk_n003_trader_dolis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n003_trader_dolis-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n003_trader_dolis-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n003_trader_dolis-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n003_trader_dolis-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n003_trader_dolis-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n003_trader_dolis-1-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-c" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-d" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-e" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-f" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-g" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-h" then 
	end
	if npc_talk_index == "n003_trader_dolis-1-i" then 
	end
	if npc_talk_index == "n003_trader_dolis-2" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 105, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300065, 10);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n003_trader_dolis-3-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

				if api_quest_HasQuestItem( pRoom, userObjID, 300065, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300065, api_quest_HasQuestItem( pRoom, userObjID, 300065, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300066, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300066, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_051_nourishing_meal_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 51);
	if qstep == 2 and CountIndex == 111 then

	end
	if qstep == 2 and CountIndex == 105 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300065, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300065, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300065 then

	end
end

function sq01_051_nourishing_meal_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 51);
	if qstep == 2 and CountIndex == 111 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 105 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300065 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function sq01_051_nourishing_meal_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 51);
	local questID=51;
end

function sq01_051_nourishing_meal_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq01_051_nourishing_meal_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq01_051_nourishing_meal_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,51, 1);
				api_quest_SetJournalStep( pRoom, userObjID,51, 1);
				api_quest_SetQuestStep( pRoom, userObjID,51, 1);
				npc_talk_index = "n002_trader_dorin-1";
end

</GameServer>