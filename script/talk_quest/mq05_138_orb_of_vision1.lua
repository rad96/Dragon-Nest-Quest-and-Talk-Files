<VillageServer>

function mq05_138_orb_of_vision1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 11 then
		mq05_138_orb_of_vision1_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 12 then
		mq05_138_orb_of_vision1_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 156 then
		mq05_138_orb_of_vision1_OnTalk_n156_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 20 then
		mq05_138_orb_of_vision1_OnTalk_n020_sorceress_yullia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function mq05_138_orb_of_vision1_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n011_priest_leonardo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n011_priest_leonardo-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-2-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-c" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-d" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-e" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_138_orb_of_vision1_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n012_sorceress_master_cynthia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n012_sorceress_master_cynthia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n012_sorceress_master_cynthia-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n012_sorceress_master_cynthia-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,138, 2);
				api_quest_SetJournalStep(userObjID,138, 1);
				api_quest_SetQuestStep(userObjID,138, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";

	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-skill_01" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_ui_OpenSkillShop(userObjID);
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n156_cleric_jake--------------------------------------------------------------------------------
function mq05_138_orb_of_vision1_OnTalk_n156_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n156_cleric_jake-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n156_cleric_jake-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n156_cleric_jake-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n156_cleric_jake-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n156_cleric_jake-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n156_cleric_jake-3-b" then 
	end
	if npc_talk_index == "n156_cleric_jake-3-b" then 
	end
	if npc_talk_index == "n156_cleric_jake-3-c" then 
	end
	if npc_talk_index == "n156_cleric_jake-3-d" then 
	end
	if npc_talk_index == "n156_cleric_jake-3-e" then 
	end
	if npc_talk_index == "n156_cleric_jake-5" then 
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n156_cleric_jake-5" then 
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n156_cleric_jake-5-b" then 
	end
	if npc_talk_index == "n156_cleric_jake-5-c" then 
	end
	if npc_talk_index == "n156_cleric_jake-5-d" then 
	end
	if npc_talk_index == "n156_cleric_jake-5-e" then 
	end
	if npc_talk_index == "n156_cleric_jake-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n020_sorceress_yullia--------------------------------------------------------------------------------
function mq05_138_orb_of_vision1_OnTalk_n020_sorceress_yullia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n020_sorceress_yullia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n020_sorceress_yullia-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n020_sorceress_yullia-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n020_sorceress_yullia-6-b" then 
	end
	if npc_talk_index == "n020_sorceress_yullia-6-c" then 
	end
	if npc_talk_index == "n020_sorceress_yullia-6-d" then 
	end
	if npc_talk_index == "n020_sorceress_yullia-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
	end
	if npc_talk_index == "n020_sorceress_yullia-7-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1380, true);
				 api_quest_RewardQuestUser(userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1380, true);
				 api_quest_RewardQuestUser(userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1380, true);
				 api_quest_RewardQuestUser(userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1380, true);
				 api_quest_RewardQuestUser(userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1380, true);
				 api_quest_RewardQuestUser(userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1380, true);
				 api_quest_RewardQuestUser(userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1380, true);
				 api_quest_RewardQuestUser(userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1380, true);
				 api_quest_RewardQuestUser(userObjID, 1380, questID, 1);
			 end 
	end
	if npc_talk_index == "n020_sorceress_yullia-7-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 139, 2);
					api_quest_SetQuestStep(userObjID, 139, 1);
					api_quest_SetJournalStep(userObjID, 139, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n020_sorceress_yullia-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n020_sorceress_yullia-1", "mq05_139_orb_of_vision2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_138_orb_of_vision1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 138);
end

function mq05_138_orb_of_vision1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 138);
end

function mq05_138_orb_of_vision1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 138);
	local questID=138;
end

function mq05_138_orb_of_vision1_OnRemoteStart( userObjID, questID )
end

function mq05_138_orb_of_vision1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq05_138_orb_of_vision1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,138, 2);
				api_quest_SetJournalStep(userObjID,138, 1);
				api_quest_SetQuestStep(userObjID,138, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";
end

</VillageServer>

<GameServer>
function mq05_138_orb_of_vision1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 11 then
		mq05_138_orb_of_vision1_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 12 then
		mq05_138_orb_of_vision1_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 156 then
		mq05_138_orb_of_vision1_OnTalk_n156_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 20 then
		mq05_138_orb_of_vision1_OnTalk_n020_sorceress_yullia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function mq05_138_orb_of_vision1_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n011_priest_leonardo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n011_priest_leonardo-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-2-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-c" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-d" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_138_orb_of_vision1_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n012_sorceress_master_cynthia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n012_sorceress_master_cynthia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n012_sorceress_master_cynthia-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n012_sorceress_master_cynthia-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,138, 2);
				api_quest_SetJournalStep( pRoom, userObjID,138, 1);
				api_quest_SetQuestStep( pRoom, userObjID,138, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";

	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-1-skill_01" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_ui_OpenSkillShop( pRoom, userObjID);
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n156_cleric_jake--------------------------------------------------------------------------------
function mq05_138_orb_of_vision1_OnTalk_n156_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n156_cleric_jake-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n156_cleric_jake-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n156_cleric_jake-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n156_cleric_jake-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n156_cleric_jake-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n156_cleric_jake-3-b" then 
	end
	if npc_talk_index == "n156_cleric_jake-3-b" then 
	end
	if npc_talk_index == "n156_cleric_jake-3-c" then 
	end
	if npc_talk_index == "n156_cleric_jake-3-d" then 
	end
	if npc_talk_index == "n156_cleric_jake-3-e" then 
	end
	if npc_talk_index == "n156_cleric_jake-5" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n156_cleric_jake-5" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n156_cleric_jake-5-b" then 
	end
	if npc_talk_index == "n156_cleric_jake-5-c" then 
	end
	if npc_talk_index == "n156_cleric_jake-5-d" then 
	end
	if npc_talk_index == "n156_cleric_jake-5-e" then 
	end
	if npc_talk_index == "n156_cleric_jake-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n020_sorceress_yullia--------------------------------------------------------------------------------
function mq05_138_orb_of_vision1_OnTalk_n020_sorceress_yullia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n020_sorceress_yullia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n020_sorceress_yullia-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n020_sorceress_yullia-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n020_sorceress_yullia-6-b" then 
	end
	if npc_talk_index == "n020_sorceress_yullia-6-c" then 
	end
	if npc_talk_index == "n020_sorceress_yullia-6-d" then 
	end
	if npc_talk_index == "n020_sorceress_yullia-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
	end
	if npc_talk_index == "n020_sorceress_yullia-7-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1380, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1380, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1380, questID, 1);
			 end 
	end
	if npc_talk_index == "n020_sorceress_yullia-7-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 139, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 139, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 139, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n020_sorceress_yullia-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n020_sorceress_yullia-1", "mq05_139_orb_of_vision2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_138_orb_of_vision1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 138);
end

function mq05_138_orb_of_vision1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 138);
end

function mq05_138_orb_of_vision1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 138);
	local questID=138;
end

function mq05_138_orb_of_vision1_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq05_138_orb_of_vision1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq05_138_orb_of_vision1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,138, 2);
				api_quest_SetJournalStep( pRoom, userObjID,138, 1);
				api_quest_SetQuestStep( pRoom, userObjID,138, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";
end

</GameServer>