<VillageServer>

function nq12_7220_angel_hound_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 193 then
		nq12_7220_angel_hound_OnTalk_n193_santaund(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 91 then
		nq12_7220_angel_hound_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n193_santaund--------------------------------------------------------------------------------
function nq12_7220_angel_hound_OnTalk_n193_santaund(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n193_santaund-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n193_santaund-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n193_santaund-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n193_santaund-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n193_santaund-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n193_santaund-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 72200, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 72200, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 72200, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 72200, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 72200, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 72200, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 72200, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 72200, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 72200, false);
			 end 

	end
	if npc_talk_index == "n193_santaund-accepting-acceptted" then
				api_quest_AddQuest(userObjID,7220, 1);
				api_quest_SetJournalStep(userObjID,7220, 1);
				api_quest_SetQuestStep(userObjID,7220, 1);
				npc_talk_index = "n193_santaund-1";

	end
	if npc_talk_index == "n193_santaund-1-b" then 
	end
	if npc_talk_index == "n193_santaund-1-c" then 
	end
	if npc_talk_index == "n193_santaund-1-d" then 
	end
	if npc_talk_index == "n193_santaund-1-e" then 
	end
	if npc_talk_index == "n193_santaund-1-f" then 
	end
	if npc_talk_index == "n193_santaund-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n193_santaund-1-g" then 
	end
	if npc_talk_index == "n193_santaund-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n193_santaund-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n193_santaund-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);

				if api_quest_HasQuestItem(userObjID, 400198, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400198, api_quest_HasQuestItem(userObjID, 400198, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400199, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400199, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n091_trader_bellin--------------------------------------------------------------------------------
function nq12_7220_angel_hound_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n091_trader_bellin-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n091_trader_bellin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n091_trader_bellin-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-5" then 

				if api_quest_HasQuestItem(userObjID, 400199, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400199, api_quest_HasQuestItem(userObjID, 400199, 1));
				end
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n091_trader_bellin-5-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 72200, true);
				 api_quest_RewardQuestUser(userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 72200, true);
				 api_quest_RewardQuestUser(userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 72200, true);
				 api_quest_RewardQuestUser(userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 72200, true);
				 api_quest_RewardQuestUser(userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 72200, true);
				 api_quest_RewardQuestUser(userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 72200, true);
				 api_quest_RewardQuestUser(userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 72200, true);
				 api_quest_RewardQuestUser(userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 72200, true);
				 api_quest_RewardQuestUser(userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 72200, true);
				 api_quest_RewardQuestUser(userObjID, 72200, questID, 1);
			 end 
	end
	if npc_talk_index == "n091_trader_bellin-5-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 91, 600 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq12_7220_angel_hound_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7220);
end

function nq12_7220_angel_hound_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7220);
end

function nq12_7220_angel_hound_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7220);
	local questID=7220;
end

function nq12_7220_angel_hound_OnRemoteStart( userObjID, questID )
end

function nq12_7220_angel_hound_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq12_7220_angel_hound_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,7220, 1);
				api_quest_SetJournalStep(userObjID,7220, 1);
				api_quest_SetQuestStep(userObjID,7220, 1);
				npc_talk_index = "n193_santaund-1";
end

</VillageServer>

<GameServer>
function nq12_7220_angel_hound_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 193 then
		nq12_7220_angel_hound_OnTalk_n193_santaund( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 91 then
		nq12_7220_angel_hound_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n193_santaund--------------------------------------------------------------------------------
function nq12_7220_angel_hound_OnTalk_n193_santaund( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n193_santaund-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n193_santaund-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n193_santaund-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n193_santaund-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n193_santaund-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n193_santaund-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, false);
			 end 

	end
	if npc_talk_index == "n193_santaund-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,7220, 1);
				api_quest_SetJournalStep( pRoom, userObjID,7220, 1);
				api_quest_SetQuestStep( pRoom, userObjID,7220, 1);
				npc_talk_index = "n193_santaund-1";

	end
	if npc_talk_index == "n193_santaund-1-b" then 
	end
	if npc_talk_index == "n193_santaund-1-c" then 
	end
	if npc_talk_index == "n193_santaund-1-d" then 
	end
	if npc_talk_index == "n193_santaund-1-e" then 
	end
	if npc_talk_index == "n193_santaund-1-f" then 
	end
	if npc_talk_index == "n193_santaund-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n193_santaund-1-g" then 
	end
	if npc_talk_index == "n193_santaund-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n193_santaund-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n193_santaund-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);

				if api_quest_HasQuestItem( pRoom, userObjID, 400198, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400198, api_quest_HasQuestItem( pRoom, userObjID, 400198, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400199, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400199, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n091_trader_bellin--------------------------------------------------------------------------------
function nq12_7220_angel_hound_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n091_trader_bellin-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n091_trader_bellin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n091_trader_bellin-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-5" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400199, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400199, api_quest_HasQuestItem( pRoom, userObjID, 400199, 1));
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n091_trader_bellin-5-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72200, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72200, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72200, questID, 1);
			 end 
	end
	if npc_talk_index == "n091_trader_bellin-5-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 91, 600 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq12_7220_angel_hound_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7220);
end

function nq12_7220_angel_hound_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7220);
end

function nq12_7220_angel_hound_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7220);
	local questID=7220;
end

function nq12_7220_angel_hound_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq12_7220_angel_hound_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq12_7220_angel_hound_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,7220, 1);
				api_quest_SetJournalStep( pRoom, userObjID,7220, 1);
				api_quest_SetQuestStep( pRoom, userObjID,7220, 1);
				npc_talk_index = "n193_santaund-1";
end

</GameServer>