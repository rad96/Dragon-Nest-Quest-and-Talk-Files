<VillageServer>

function sq05_155_remember_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 13 then
		sq05_155_remember_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 159 then
		sq05_155_remember_OnTalk_n159_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 170 then
		sq05_155_remember_OnTalk_n170_cl_saintheaven(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 172 then
		sq05_155_remember_OnTalk_n172_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 173 then
		sq05_155_remember_OnTalk_n173_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 174 then
		sq05_155_remember_OnTalk_n174_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 885 then
		sq05_155_remember_OnTalk_n885_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n013_cleric_tomas--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n013_cleric_tomas-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted" then
				api_quest_AddQuest(userObjID,155, 2);
				api_quest_SetJournalStep(userObjID,155, 1);
				api_quest_SetQuestStep(userObjID,155, 1);
				npc_talk_index = "n013_cleric_tomas-1";

	end
	if npc_talk_index == "n013_cleric_tomas-1-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-d" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-e" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-f" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-g" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-h" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-i" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-j" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n159_light_pole--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n159_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n159_light_pole-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n159_light_pole-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n159_light_pole-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n159_light_pole-2-w_155_2" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_user_ChangeMap(userObjID,13004,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n170_cl_saintheaven--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n170_cl_saintheaven(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n170_cl_saintheaven-1";
				local rcval_1_3 = math.random(1,30);
				if rcval_1_3 >= 1 and rcval_1_3 < 10 then
					npc_talk_index = "n170_cl_saintheaven-1";
				end
				if rcval_1_3 >= 10 and rcval_1_3 < 20 then
					npc_talk_index = "n170_cl_saintheaven-1-a";
				end
				if rcval_1_3 >= 20 and rcval_1_3 < 30 then
					npc_talk_index = "n170_cl_saintheaven-1-b";
				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n172_cleric_tomas--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n172_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n172_cleric_tomas-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n172_cleric_tomas-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n172_cleric_tomas-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n172_cleric_tomas-3-b" then 
	end
	if npc_talk_index == "n172_cleric_tomas-3-c" then 
	end
	if npc_talk_index == "n172_cleric_tomas-3-d" then 
	end
	if npc_talk_index == "n172_cleric_tomas-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n173_cleric_jake--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n173_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n173_cleric_jake-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n173_cleric_jake-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n173_cleric_jake-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n173_cleric_jake-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n174_cleric_jake--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n174_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n174_cleric_jake-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n885_light_pole--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n885_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n885_light_pole-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n885_light_pole-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n885_light_pole-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n885_light_pole-5-b" then 
	end
	if npc_talk_index == "n885_light_pole-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
	end
	if npc_talk_index == "n885_light_pole-6-b" then 
	end
	if npc_talk_index == "n885_light_pole-6-c" then 
	end
	if npc_talk_index == "n885_light_pole-6-d" then 
	end
	if npc_talk_index == "n885_light_pole-6-e" then 
	end
	if npc_talk_index == "n885_light_pole-6-f" then 
	end
	if npc_talk_index == "n885_light_pole-6-g" then 
	end
	if npc_talk_index == "n885_light_pole-6-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1550, true);
				 api_quest_RewardQuestUser(userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1550, true);
				 api_quest_RewardQuestUser(userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1550, true);
				 api_quest_RewardQuestUser(userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1550, true);
				 api_quest_RewardQuestUser(userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1550, true);
				 api_quest_RewardQuestUser(userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1550, true);
				 api_quest_RewardQuestUser(userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1550, true);
				 api_quest_RewardQuestUser(userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1550, true);
				 api_quest_RewardQuestUser(userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1550, true);
				 api_quest_RewardQuestUser(userObjID, 1550, questID, 1);
			 end 
	end
	if npc_talk_index == "n885_light_pole-6-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 156, 2);
					api_quest_SetQuestStep(userObjID, 156, 1);
					api_quest_SetJournalStep(userObjID, 156, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n885_light_pole-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n885_light_pole-1", "sq05_156_path_of_jake.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_155_remember_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 155);
end

function sq05_155_remember_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 155);
end

function sq05_155_remember_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 155);
	local questID=155;
end

function sq05_155_remember_OnRemoteStart( userObjID, questID )
end

function sq05_155_remember_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_155_remember_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,155, 2);
				api_quest_SetJournalStep(userObjID,155, 1);
				api_quest_SetQuestStep(userObjID,155, 1);
				npc_talk_index = "n013_cleric_tomas-1";
end

</VillageServer>

<GameServer>
function sq05_155_remember_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 13 then
		sq05_155_remember_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 159 then
		sq05_155_remember_OnTalk_n159_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 170 then
		sq05_155_remember_OnTalk_n170_cl_saintheaven( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 172 then
		sq05_155_remember_OnTalk_n172_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 173 then
		sq05_155_remember_OnTalk_n173_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 174 then
		sq05_155_remember_OnTalk_n174_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 885 then
		sq05_155_remember_OnTalk_n885_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n013_cleric_tomas--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n013_cleric_tomas-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,155, 2);
				api_quest_SetJournalStep( pRoom, userObjID,155, 1);
				api_quest_SetQuestStep( pRoom, userObjID,155, 1);
				npc_talk_index = "n013_cleric_tomas-1";

	end
	if npc_talk_index == "n013_cleric_tomas-1-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-d" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-e" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-f" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-g" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-h" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-i" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-j" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n159_light_pole--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n159_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n159_light_pole-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n159_light_pole-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n159_light_pole-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n159_light_pole-2-w_155_2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_user_ChangeMap( pRoom, userObjID,13004,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n170_cl_saintheaven--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n170_cl_saintheaven( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n170_cl_saintheaven-1";
				local rcval_1_3 = math.random(1,30);
				if rcval_1_3 >= 1 and rcval_1_3 < 10 then
					npc_talk_index = "n170_cl_saintheaven-1";
				end
				if rcval_1_3 >= 10 and rcval_1_3 < 20 then
					npc_talk_index = "n170_cl_saintheaven-1-a";
				end
				if rcval_1_3 >= 20 and rcval_1_3 < 30 then
					npc_talk_index = "n170_cl_saintheaven-1-b";
				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n172_cleric_tomas--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n172_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n172_cleric_tomas-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n172_cleric_tomas-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n172_cleric_tomas-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n172_cleric_tomas-3-b" then 
	end
	if npc_talk_index == "n172_cleric_tomas-3-c" then 
	end
	if npc_talk_index == "n172_cleric_tomas-3-d" then 
	end
	if npc_talk_index == "n172_cleric_tomas-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n173_cleric_jake--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n173_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n173_cleric_jake-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n173_cleric_jake-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n173_cleric_jake-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n173_cleric_jake-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n174_cleric_jake--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n174_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n174_cleric_jake-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n885_light_pole--------------------------------------------------------------------------------
function sq05_155_remember_OnTalk_n885_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n885_light_pole-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n885_light_pole-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n885_light_pole-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n885_light_pole-5-b" then 
	end
	if npc_talk_index == "n885_light_pole-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
	end
	if npc_talk_index == "n885_light_pole-6-b" then 
	end
	if npc_talk_index == "n885_light_pole-6-c" then 
	end
	if npc_talk_index == "n885_light_pole-6-d" then 
	end
	if npc_talk_index == "n885_light_pole-6-e" then 
	end
	if npc_talk_index == "n885_light_pole-6-f" then 
	end
	if npc_talk_index == "n885_light_pole-6-g" then 
	end
	if npc_talk_index == "n885_light_pole-6-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1550, questID, 1);
			 end 
	end
	if npc_talk_index == "n885_light_pole-6-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 156, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 156, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 156, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n885_light_pole-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n885_light_pole-1", "sq05_156_path_of_jake.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_155_remember_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 155);
end

function sq05_155_remember_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 155);
end

function sq05_155_remember_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 155);
	local questID=155;
end

function sq05_155_remember_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq05_155_remember_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_155_remember_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,155, 2);
				api_quest_SetJournalStep( pRoom, userObjID,155, 1);
				api_quest_SetQuestStep( pRoom, userObjID,155, 1);
				npc_talk_index = "n013_cleric_tomas-1";
end

</GameServer>