<VillageServer>

function sq08_261_argenta_milk_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 164 then
		sq08_261_argenta_milk_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 26 then
		sq08_261_argenta_milk_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 28 then
		sq08_261_argenta_milk_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 29 then
		sq08_261_argenta_milk_OnTalk_n029_crew_kevin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 323 then
		sq08_261_argenta_milk_OnTalk_n323_warehouse_lupert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_261_argenta_milk_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n164_argenta_wounded-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n164_argenta_wounded-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-g" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2611, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2612, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2613, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2614, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2615, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2616, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2617, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2618, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2619, false);
			 end 

	end
	if npc_talk_index == "n164_argenta_wounded-accepting-h" then
				api_quest_AddQuest(userObjID,261, 1);
				api_quest_SetJournalStep(userObjID,261, 1);
				api_quest_SetQuestStep(userObjID,261, 1);
				api_quest_SetQuestStep(userObjID, questID,2);

	end
	if npc_talk_index == "n164_argenta_wounded-1-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n164_argenta_wounded-9-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-9-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-9-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-9-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-9-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2611, true);
				 api_quest_RewardQuestUser(userObjID, 2611, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2612, true);
				 api_quest_RewardQuestUser(userObjID, 2612, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2613, true);
				 api_quest_RewardQuestUser(userObjID, 2613, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2614, true);
				 api_quest_RewardQuestUser(userObjID, 2614, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2615, true);
				 api_quest_RewardQuestUser(userObjID, 2615, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2616, true);
				 api_quest_RewardQuestUser(userObjID, 2616, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2617, true);
				 api_quest_RewardQuestUser(userObjID, 2617, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2618, true);
				 api_quest_RewardQuestUser(userObjID, 2618, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2619, true);
				 api_quest_RewardQuestUser(userObjID, 2619, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-9-g" then 

				if api_quest_HasQuestItem(userObjID, 300137, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300137, api_quest_HasQuestItem(userObjID, 300137, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n026_trader_may--------------------------------------------------------------------------------
function sq08_261_argenta_milk_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n026_trader_may-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n026_trader_may-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n026_trader_may-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n026_trader_may-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n026_trader_may-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n026_trader_may-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n026_trader_may-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-2-b" then 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 
	end
	if npc_talk_index == "n026_trader_may-3" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 444, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200444, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 445, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200445, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 446, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200446, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 3, 400409, 5);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n026_trader_may-4-b" then 
	end
	if npc_talk_index == "n026_trader_may-4-c" then 
	end
	if npc_talk_index == "n026_trader_may-4-d" then 

				if api_quest_HasQuestItem(userObjID, 400409, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400409, api_quest_HasQuestItem(userObjID, 400409, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300137, 3) == 1 then 
					api_quest_AddQuestItem(userObjID, 300137, 3, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n026_trader_may-8-b" then 
	end
	if npc_talk_index == "n026_trader_may-9" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300137, 10) == 1 then 
					api_quest_AddQuestItem(userObjID, 300137, 10, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_261_argenta_milk_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n028_scholar_bailey-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n028_scholar_bailey-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-6-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-6-c" then 
				api_quest_DelQuestItem(userObjID, 300137, 1);
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n029_crew_kevin--------------------------------------------------------------------------------
function sq08_261_argenta_milk_OnTalk_n029_crew_kevin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n029_crew_kevin-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n029_crew_kevin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n029_crew_kevin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n029_crew_kevin-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n029_crew_kevin-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n029_crew_kevin-7-b" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-kevinansw" then 
				if api_quest_IsMarkingCompleteQuest(userObjID, 218 ) == 1  then
									npc_talk_index = "n029_crew_kevin-7-c";

				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 208 ) == 1  then
									npc_talk_index = "n029_crew_kevin-7-f";

				else
									npc_talk_index = "n029_crew_kevin-7-h";

				end

				end
	end
	if npc_talk_index == "n029_crew_kevin-7-d" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-e" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-j" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-g" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-j" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-i" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-j" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-k" then 

				if api_quest_HasQuestItem(userObjID, 300137, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300137, api_quest_HasQuestItem(userObjID, 300137, 1));
				end
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n323_warehouse_lupert--------------------------------------------------------------------------------
function sq08_261_argenta_milk_OnTalk_n323_warehouse_lupert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n323_warehouse_lupert-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n323_warehouse_lupert-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n323_warehouse_lupert-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n323_warehouse_lupert-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n323_warehouse_lupert-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n323_warehouse_lupert-5-b" then 
				api_quest_DelQuestItem(userObjID, 300137, 1);
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_261_argenta_milk_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 261);
	if qstep == 3 and CountIndex == 444 then
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 3 and CountIndex == 400409 then

	end
	if qstep == 3 and CountIndex == 200444 then
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 3 and CountIndex == 445 then
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 3 and CountIndex == 200445 then
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 3 and CountIndex == 446 then
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 3 and CountIndex == 200446 then
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
end

function sq08_261_argenta_milk_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 261);
	if qstep == 3 and CountIndex == 444 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 400409 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200444 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 445 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200445 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 446 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200446 and Count >= TargetCount  then

	end
end

function sq08_261_argenta_milk_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 261);
	local questID=261;
end

function sq08_261_argenta_milk_OnRemoteStart( userObjID, questID )
end

function sq08_261_argenta_milk_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_261_argenta_milk_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq08_261_argenta_milk_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 164 then
		sq08_261_argenta_milk_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 26 then
		sq08_261_argenta_milk_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 28 then
		sq08_261_argenta_milk_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 29 then
		sq08_261_argenta_milk_OnTalk_n029_crew_kevin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 323 then
		sq08_261_argenta_milk_OnTalk_n323_warehouse_lupert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_261_argenta_milk_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n164_argenta_wounded-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n164_argenta_wounded-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-g" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2611, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2612, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2613, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2614, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2615, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2616, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2617, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2618, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2619, false);
			 end 

	end
	if npc_talk_index == "n164_argenta_wounded-accepting-h" then
				api_quest_AddQuest( pRoom, userObjID,261, 1);
				api_quest_SetJournalStep( pRoom, userObjID,261, 1);
				api_quest_SetQuestStep( pRoom, userObjID,261, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);

	end
	if npc_talk_index == "n164_argenta_wounded-1-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n164_argenta_wounded-9-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-9-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-9-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-9-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-9-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2611, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2611, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2612, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2612, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2613, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2613, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2614, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2614, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2615, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2615, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2616, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2616, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2617, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2617, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2618, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2618, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2619, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2619, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-9-g" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300137, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300137, api_quest_HasQuestItem( pRoom, userObjID, 300137, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n026_trader_may--------------------------------------------------------------------------------
function sq08_261_argenta_milk_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n026_trader_may-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n026_trader_may-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n026_trader_may-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n026_trader_may-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n026_trader_may-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n026_trader_may-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n026_trader_may-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-2-b" then 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 
	end
	if npc_talk_index == "n026_trader_may-3" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 444, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200444, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 445, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200445, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 446, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200446, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 3, 400409, 5);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n026_trader_may-4-b" then 
	end
	if npc_talk_index == "n026_trader_may-4-c" then 
	end
	if npc_talk_index == "n026_trader_may-4-d" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400409, api_quest_HasQuestItem( pRoom, userObjID, 400409, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300137, 3) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300137, 3, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n026_trader_may-8-b" then 
	end
	if npc_talk_index == "n026_trader_may-9" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300137, 10) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300137, 10, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_261_argenta_milk_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n028_scholar_bailey-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n028_scholar_bailey-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-6-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-6-c" then 
				api_quest_DelQuestItem( pRoom, userObjID, 300137, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n029_crew_kevin--------------------------------------------------------------------------------
function sq08_261_argenta_milk_OnTalk_n029_crew_kevin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n029_crew_kevin-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n029_crew_kevin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n029_crew_kevin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n029_crew_kevin-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n029_crew_kevin-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n029_crew_kevin-7-b" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-kevinansw" then 
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 218 ) == 1  then
									npc_talk_index = "n029_crew_kevin-7-c";

				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 208 ) == 1  then
									npc_talk_index = "n029_crew_kevin-7-f";

				else
									npc_talk_index = "n029_crew_kevin-7-h";

				end

				end
	end
	if npc_talk_index == "n029_crew_kevin-7-d" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-e" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-j" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-g" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-j" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-i" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-j" then 
	end
	if npc_talk_index == "n029_crew_kevin-7-k" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300137, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300137, api_quest_HasQuestItem( pRoom, userObjID, 300137, 1));
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n323_warehouse_lupert--------------------------------------------------------------------------------
function sq08_261_argenta_milk_OnTalk_n323_warehouse_lupert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n323_warehouse_lupert-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n323_warehouse_lupert-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n323_warehouse_lupert-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n323_warehouse_lupert-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n323_warehouse_lupert-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n323_warehouse_lupert-5-b" then 
				api_quest_DelQuestItem( pRoom, userObjID, 300137, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_261_argenta_milk_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 261);
	if qstep == 3 and CountIndex == 444 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 3 and CountIndex == 400409 then

	end
	if qstep == 3 and CountIndex == 200444 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 3 and CountIndex == 445 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 3 and CountIndex == 200445 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 3 and CountIndex == 446 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 3 and CountIndex == 200446 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400409, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400409, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400409, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
end

function sq08_261_argenta_milk_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 261);
	if qstep == 3 and CountIndex == 444 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 400409 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200444 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 445 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200445 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 446 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200446 and Count >= TargetCount  then

	end
end

function sq08_261_argenta_milk_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 261);
	local questID=261;
end

function sq08_261_argenta_milk_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_261_argenta_milk_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_261_argenta_milk_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>