<VillageServer>

function sq11_572_unforeseen_damage_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 38 then
		sq11_572_unforeseen_damage_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 52 then
		sq11_572_unforeseen_damage_OnTalk_n052_place_of_trail(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function sq11_572_unforeseen_damage_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n038_royal_magician_kalaen-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5721, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5722, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5723, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5724, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5725, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5726, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5727, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5728, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5729, false);
			 end 

	end
	if npc_talk_index == "n038_royal_magician_kalaen-accepting-acceppted" then
				api_quest_AddQuest(userObjID,572, 1);
				api_quest_SetJournalStep(userObjID,572, 1);
				api_quest_SetQuestStep(userObjID,572, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1167, 5);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201167, 30000);
				npc_talk_index = "n038_royal_magician_kalaen-1";

	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n052_place_of_trail--------------------------------------------------------------------------------
function sq11_572_unforeseen_damage_OnTalk_n052_place_of_trail(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n052_place_of_trail-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n052_place_of_trail-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n052_place_of_trail-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n052_place_of_trail-3-b" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-c" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-c" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-d" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-e" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-ck_job1" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n052_place_of_trail-3-j";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n052_place_of_trail-3-j";

				else
									npc_talk_index = "n052_place_of_trail-3-k";

				end

				end
	end
	if npc_talk_index == "n052_place_of_trail-3-g" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-g" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-h" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5721, true);
				 api_quest_RewardQuestUser(userObjID, 5721, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5722, true);
				 api_quest_RewardQuestUser(userObjID, 5722, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5723, true);
				 api_quest_RewardQuestUser(userObjID, 5723, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5724, true);
				 api_quest_RewardQuestUser(userObjID, 5724, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5725, true);
				 api_quest_RewardQuestUser(userObjID, 5725, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5726, true);
				 api_quest_RewardQuestUser(userObjID, 5726, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5727, true);
				 api_quest_RewardQuestUser(userObjID, 5727, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5728, true);
				 api_quest_RewardQuestUser(userObjID, 5728, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5729, true);
				 api_quest_RewardQuestUser(userObjID, 5729, questID, 1);
			 end 
	end
	if npc_talk_index == "n052_place_of_trail-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 573, 1);
					api_quest_SetQuestStep(userObjID, 573, 1);
					api_quest_SetJournalStep(userObjID, 573, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 255, 600 );

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n052_place_of_trail-1", "sq11_573_fearful_fact.xml");
			return;
		end
	end
	if npc_talk_index == "n052_place_of_trail-3-l" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-f" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-g" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_572_unforeseen_damage_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 572);
	if qstep == 1 and CountIndex == 1167 then

	end
	if qstep == 1 and CountIndex == 201167 then
				api_quest_IncCounting(userObjID, 2, 1167);

	end
end

function sq11_572_unforeseen_damage_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 572);
	if qstep == 1 and CountIndex == 1167 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201167 and Count >= TargetCount  then

	end
end

function sq11_572_unforeseen_damage_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 572);
	local questID=572;
end

function sq11_572_unforeseen_damage_OnRemoteStart( userObjID, questID )
end

function sq11_572_unforeseen_damage_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_572_unforeseen_damage_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_572_unforeseen_damage_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 38 then
		sq11_572_unforeseen_damage_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 52 then
		sq11_572_unforeseen_damage_OnTalk_n052_place_of_trail( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function sq11_572_unforeseen_damage_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n038_royal_magician_kalaen-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5721, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5722, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5723, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5724, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5725, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5726, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5727, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5728, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5729, false);
			 end 

	end
	if npc_talk_index == "n038_royal_magician_kalaen-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,572, 1);
				api_quest_SetJournalStep( pRoom, userObjID,572, 1);
				api_quest_SetQuestStep( pRoom, userObjID,572, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1167, 5);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201167, 30000);
				npc_talk_index = "n038_royal_magician_kalaen-1";

	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n052_place_of_trail--------------------------------------------------------------------------------
function sq11_572_unforeseen_damage_OnTalk_n052_place_of_trail( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n052_place_of_trail-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n052_place_of_trail-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n052_place_of_trail-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n052_place_of_trail-3-b" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-c" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-c" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-d" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-e" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-ck_job1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n052_place_of_trail-3-j";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n052_place_of_trail-3-j";

				else
									npc_talk_index = "n052_place_of_trail-3-k";

				end

				end
	end
	if npc_talk_index == "n052_place_of_trail-3-g" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-g" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-h" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5721, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5721, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5722, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5722, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5723, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5723, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5724, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5724, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5725, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5725, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5726, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5726, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5727, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5727, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5728, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5728, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5729, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5729, questID, 1);
			 end 
	end
	if npc_talk_index == "n052_place_of_trail-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 573, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 573, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 573, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 255, 600 );

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n052_place_of_trail-1", "sq11_573_fearful_fact.xml");
			return;
		end
	end
	if npc_talk_index == "n052_place_of_trail-3-l" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-f" then 
	end
	if npc_talk_index == "n052_place_of_trail-3-g" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_572_unforeseen_damage_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 572);
	if qstep == 1 and CountIndex == 1167 then

	end
	if qstep == 1 and CountIndex == 201167 then
				api_quest_IncCounting( pRoom, userObjID, 2, 1167);

	end
end

function sq11_572_unforeseen_damage_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 572);
	if qstep == 1 and CountIndex == 1167 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201167 and Count >= TargetCount  then

	end
end

function sq11_572_unforeseen_damage_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 572);
	local questID=572;
end

function sq11_572_unforeseen_damage_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_572_unforeseen_damage_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_572_unforeseen_damage_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>