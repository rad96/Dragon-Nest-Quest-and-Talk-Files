<VillageServer>

function sq11_461_skill_of_battle_01_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 65 then
		sq11_461_skill_of_battle_01_OnTalk_n065_goblinson04_gosuk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 66 then
		sq11_461_skill_of_battle_01_OnTalk_n066_skill_of_battle01_notice(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n065_goblinson04_gosuk--------------------------------------------------------------------------------
function sq11_461_skill_of_battle_01_OnTalk_n065_goblinson04_gosuk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n065_goblinson04_gosuk-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n065_goblinson04_gosuk-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n065_goblinson04_gosuk-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n065_goblinson04_gosuk-2-b" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-c" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-d" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4611, true);
				 api_quest_RewardQuestUser(userObjID, 4611, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4612, true);
				 api_quest_RewardQuestUser(userObjID, 4612, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4613, true);
				 api_quest_RewardQuestUser(userObjID, 4613, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4614, true);
				 api_quest_RewardQuestUser(userObjID, 4614, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4615, true);
				 api_quest_RewardQuestUser(userObjID, 4615, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4616, true);
				 api_quest_RewardQuestUser(userObjID, 4616, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4617, true);
				 api_quest_RewardQuestUser(userObjID, 4617, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4618, true);
				 api_quest_RewardQuestUser(userObjID, 4618, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4619, true);
				 api_quest_RewardQuestUser(userObjID, 4619, questID, 1);
			 end 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 462, 1);
					api_quest_SetQuestStep(userObjID, 462, 1);
					api_quest_SetJournalStep(userObjID, 462, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n065_goblinson04_gosuk-1", "sq11_462_skill_of_battle_02.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n066_skill_of_battle01_notice--------------------------------------------------------------------------------
function sq11_461_skill_of_battle_01_OnTalk_n066_skill_of_battle01_notice(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n066_skill_of_battle01_notice-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n066_skill_of_battle01_notice-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n066_skill_of_battle01_notice-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4611, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4612, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4613, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4614, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4615, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4616, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4617, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4618, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4619, false);
			 end 

	end
	if npc_talk_index == "n066_skill_of_battle01_notice-accepting-acceptted" then
				api_quest_AddQuest(userObjID,461, 1);
				api_quest_SetJournalStep(userObjID,461, 1);
				api_quest_SetQuestStep(userObjID,461, 1);
				npc_talk_index = "n066_skill_of_battle01_notice-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_461_skill_of_battle_01_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 461);
end

function sq11_461_skill_of_battle_01_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 461);
end

function sq11_461_skill_of_battle_01_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 461);
	local questID=461;
end

function sq11_461_skill_of_battle_01_OnRemoteStart( userObjID, questID )
end

function sq11_461_skill_of_battle_01_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_461_skill_of_battle_01_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,461, 1);
				api_quest_SetJournalStep(userObjID,461, 1);
				api_quest_SetQuestStep(userObjID,461, 1);
				npc_talk_index = "n066_skill_of_battle01_notice-1";
end

</VillageServer>

<GameServer>
function sq11_461_skill_of_battle_01_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 65 then
		sq11_461_skill_of_battle_01_OnTalk_n065_goblinson04_gosuk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 66 then
		sq11_461_skill_of_battle_01_OnTalk_n066_skill_of_battle01_notice( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n065_goblinson04_gosuk--------------------------------------------------------------------------------
function sq11_461_skill_of_battle_01_OnTalk_n065_goblinson04_gosuk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n065_goblinson04_gosuk-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n065_goblinson04_gosuk-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n065_goblinson04_gosuk-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n065_goblinson04_gosuk-2-b" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-c" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-d" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4611, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4611, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4612, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4612, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4613, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4613, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4614, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4614, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4615, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4615, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4616, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4616, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4617, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4617, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4618, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4618, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4619, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4619, questID, 1);
			 end 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 462, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 462, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 462, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n065_goblinson04_gosuk-1", "sq11_462_skill_of_battle_02.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n066_skill_of_battle01_notice--------------------------------------------------------------------------------
function sq11_461_skill_of_battle_01_OnTalk_n066_skill_of_battle01_notice( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n066_skill_of_battle01_notice-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n066_skill_of_battle01_notice-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n066_skill_of_battle01_notice-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4611, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4612, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4613, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4614, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4615, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4616, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4617, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4618, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4619, false);
			 end 

	end
	if npc_talk_index == "n066_skill_of_battle01_notice-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,461, 1);
				api_quest_SetJournalStep( pRoom, userObjID,461, 1);
				api_quest_SetQuestStep( pRoom, userObjID,461, 1);
				npc_talk_index = "n066_skill_of_battle01_notice-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_461_skill_of_battle_01_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 461);
end

function sq11_461_skill_of_battle_01_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 461);
end

function sq11_461_skill_of_battle_01_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 461);
	local questID=461;
end

function sq11_461_skill_of_battle_01_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_461_skill_of_battle_01_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_461_skill_of_battle_01_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,461, 1);
				api_quest_SetJournalStep( pRoom, userObjID,461, 1);
				api_quest_SetQuestStep( pRoom, userObjID,461, 1);
				npc_talk_index = "n066_skill_of_battle01_notice-1";
end

</GameServer>