<VillageServer>

function sq08_264_argenta_book_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 164 then
		sq08_264_argenta_book_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 188 then
		sq08_264_argenta_book_OnTalk_n188_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_264_argenta_book_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n164_argenta_wounded-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2641, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2642, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2643, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2644, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2645, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2646, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2647, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2648, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2649, false);
			 end 

	end
	if npc_talk_index == "n164_argenta_wounded-accepting-acceptted" then
				api_quest_AddQuest(userObjID,264, 1);
				api_quest_SetJournalStep(userObjID,264, 1);
				api_quest_SetQuestStep(userObjID,264, 1);
				npc_talk_index = "n164_argenta_wounded-1";

	end
	if npc_talk_index == "n164_argenta_wounded-7-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-7-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-7-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-7-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-7-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-7-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2641, true);
				 api_quest_RewardQuestUser(userObjID, 2641, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2642, true);
				 api_quest_RewardQuestUser(userObjID, 2642, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2643, true);
				 api_quest_RewardQuestUser(userObjID, 2643, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2644, true);
				 api_quest_RewardQuestUser(userObjID, 2644, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2645, true);
				 api_quest_RewardQuestUser(userObjID, 2645, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2646, true);
				 api_quest_RewardQuestUser(userObjID, 2646, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2647, true);
				 api_quest_RewardQuestUser(userObjID, 2647, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2648, true);
				 api_quest_RewardQuestUser(userObjID, 2648, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2649, true);
				 api_quest_RewardQuestUser(userObjID, 2649, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-7-h" then 

				if api_quest_HasQuestItem(userObjID, 300119, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300119, api_quest_HasQuestItem(userObjID, 300119, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n188_slave_david--------------------------------------------------------------------------------
function sq08_264_argenta_book_OnTalk_n188_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n188_slave_david-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n188_slave_david-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n188_slave_david-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n188_slave_david-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n188_slave_david-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n188_slave_david-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n188_slave_david-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n188_slave_david-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n188_slave_david-1-david123" then 
				if api_quest_IsMarkingCompleteQuest(userObjID, 263 ) == 1  then
									npc_talk_index = "n188_slave_david-1-e";

				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 260 ) == 1  then
									npc_talk_index = "n188_slave_david-1-e";

				else
									npc_talk_index = "n188_slave_david-1-f";

				end

				end
	end
	if npc_talk_index == "n188_slave_david-1-b" then 
	end
	if npc_talk_index == "n188_slave_david-1-c" then 
	end
	if npc_talk_index == "n188_slave_david-1-d" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n188_slave_david-1-a" then 
	end
	if npc_talk_index == "n188_slave_david-1-a" then 
	end
	if npc_talk_index == "n188_slave_david-3-b" then 
	end
	if npc_talk_index == "n188_slave_david-3-c" then 
	end
	if npc_talk_index == "n188_slave_david-3-d" then 
	end
	if npc_talk_index == "n188_slave_david-3-e" then 
	end
	if npc_talk_index == "n188_slave_david-3-f" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n188_slave_david-6-b" then 
	end
	if npc_talk_index == "n188_slave_david-6-c" then 
	end
	if npc_talk_index == "n188_slave_david-6-d" then 
	end
	if npc_talk_index == "n188_slave_david-6-e" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 6);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300119, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300119, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_264_argenta_book_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 264);
end

function sq08_264_argenta_book_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 264);
end

function sq08_264_argenta_book_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 264);
	local questID=264;
end

function sq08_264_argenta_book_OnRemoteStart( userObjID, questID )
end

function sq08_264_argenta_book_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_264_argenta_book_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,264, 1);
				api_quest_SetJournalStep(userObjID,264, 1);
				api_quest_SetQuestStep(userObjID,264, 1);
				npc_talk_index = "n164_argenta_wounded-1";
end

</VillageServer>

<GameServer>
function sq08_264_argenta_book_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 164 then
		sq08_264_argenta_book_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 188 then
		sq08_264_argenta_book_OnTalk_n188_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_264_argenta_book_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n164_argenta_wounded-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2641, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2642, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2643, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2644, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2645, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2646, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2647, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2648, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2649, false);
			 end 

	end
	if npc_talk_index == "n164_argenta_wounded-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,264, 1);
				api_quest_SetJournalStep( pRoom, userObjID,264, 1);
				api_quest_SetQuestStep( pRoom, userObjID,264, 1);
				npc_talk_index = "n164_argenta_wounded-1";

	end
	if npc_talk_index == "n164_argenta_wounded-7-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-7-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-7-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-7-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-7-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-7-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2641, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2641, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2642, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2642, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2643, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2643, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2644, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2644, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2645, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2645, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2646, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2646, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2647, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2647, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2648, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2648, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2649, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2649, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-7-h" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300119, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300119, api_quest_HasQuestItem( pRoom, userObjID, 300119, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n188_slave_david--------------------------------------------------------------------------------
function sq08_264_argenta_book_OnTalk_n188_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n188_slave_david-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n188_slave_david-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n188_slave_david-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n188_slave_david-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n188_slave_david-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n188_slave_david-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n188_slave_david-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n188_slave_david-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n188_slave_david-1-david123" then 
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 263 ) == 1  then
									npc_talk_index = "n188_slave_david-1-e";

				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 260 ) == 1  then
									npc_talk_index = "n188_slave_david-1-e";

				else
									npc_talk_index = "n188_slave_david-1-f";

				end

				end
	end
	if npc_talk_index == "n188_slave_david-1-b" then 
	end
	if npc_talk_index == "n188_slave_david-1-c" then 
	end
	if npc_talk_index == "n188_slave_david-1-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n188_slave_david-1-a" then 
	end
	if npc_talk_index == "n188_slave_david-1-a" then 
	end
	if npc_talk_index == "n188_slave_david-3-b" then 
	end
	if npc_talk_index == "n188_slave_david-3-c" then 
	end
	if npc_talk_index == "n188_slave_david-3-d" then 
	end
	if npc_talk_index == "n188_slave_david-3-e" then 
	end
	if npc_talk_index == "n188_slave_david-3-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n188_slave_david-6-b" then 
	end
	if npc_talk_index == "n188_slave_david-6-c" then 
	end
	if npc_talk_index == "n188_slave_david-6-d" then 
	end
	if npc_talk_index == "n188_slave_david-6-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300119, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300119, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_264_argenta_book_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 264);
end

function sq08_264_argenta_book_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 264);
end

function sq08_264_argenta_book_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 264);
	local questID=264;
end

function sq08_264_argenta_book_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_264_argenta_book_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_264_argenta_book_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,264, 1);
				api_quest_SetJournalStep( pRoom, userObjID,264, 1);
				api_quest_SetQuestStep( pRoom, userObjID,264, 1);
				npc_talk_index = "n164_argenta_wounded-1";
end

</GameServer>