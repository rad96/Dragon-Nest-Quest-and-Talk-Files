<VillageServer>

function sq_2183_halloween_2012_2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 100 then
		sq_2183_halloween_2012_2_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2183_halloween_2012_2_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				if api_user_HasItem(userObjID, 805307245, 1) >= 1 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-1-a";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21830, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21830, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21830, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21830, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21830, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21830, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21830, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21830, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21830, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2183, 1);
				api_quest_SetJournalStep(userObjID,2183, 1);
				api_quest_SetQuestStep(userObjID,2183, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805307245, 1);
				if api_user_HasItem(userObjID, 805307245, 1) >= 1 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-accepting-d";

				end

	end
	if npc_talk_index == "n100_event_ilyn-accepting-se_back" then
				api_npc_NextScript(userObjID, npcObjID, "quest", "n100_event_ilyn.xml");

	end
	if npc_talk_index == "n100_event_ilyn-1-exchange_love" then 
				if api_user_HasItem(userObjID, 805307245, 1) >= 1 then
									npc_talk_index = "n100_event_ilyn-1-c";

				else
									npc_talk_index = "n100_event_ilyn-1-b";

				end
	end
	if npc_talk_index == "n100_event_ilyn-1-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21830, true);
				 api_quest_RewardQuestUser(userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21830, true);
				 api_quest_RewardQuestUser(userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21830, true);
				 api_quest_RewardQuestUser(userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21830, true);
				 api_quest_RewardQuestUser(userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21830, true);
				 api_quest_RewardQuestUser(userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21830, true);
				 api_quest_RewardQuestUser(userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21830, true);
				 api_quest_RewardQuestUser(userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21830, true);
				 api_quest_RewardQuestUser(userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21830, true);
				 api_quest_RewardQuestUser(userObjID, 21830, questID, 1);
			 end 
	end
	if npc_talk_index == "n100_event_ilyn-1-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_user_DelItem(userObjID, 805307245, 1, questID);
	end
	if npc_talk_index == "n100_event_ilyn-1-se_back" then 
				api_npc_NextScript(userObjID, npcObjID, "quest", "n100_event_ilyn.xml");
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2183_halloween_2012_2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2183);
	if qstep == 1 and CountIndex == 805307245 then
				if api_user_HasItem(userObjID, 805307245, 1) >= 1 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-accepting-d";

				end

	end
end

function sq_2183_halloween_2012_2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2183);
	if qstep == 1 and CountIndex == 805307245 and Count >= TargetCount  then

	end
end

function sq_2183_halloween_2012_2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2183);
	local questID=2183;
end

function sq_2183_halloween_2012_2_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805307245, 1);
end

function sq_2183_halloween_2012_2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_2183_halloween_2012_2_ForceAccept( userObjID, npcObjID, questID )
				npc_talk_index = "n100_event_ilyn-1";
				npc_talk_index = "n100_event_ilyn-accepting-d";
				api_quest_AddQuest(userObjID,2183, 1);
				api_quest_SetJournalStep(userObjID,2183, 1);
				api_quest_SetQuestStep(userObjID,2183, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805307245, 1);
				if api_user_HasItem(userObjID, 805307245, 1) >= 1 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-accepting-d";

				end
end

</VillageServer>

<GameServer>
function sq_2183_halloween_2012_2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 100 then
		sq_2183_halloween_2012_2_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2183_halloween_2012_2_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				if api_user_HasItem( pRoom, userObjID, 805307245, 1) >= 1 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-1-a";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2183, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2183, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2183, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805307245, 1);
				if api_user_HasItem( pRoom, userObjID, 805307245, 1) >= 1 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-accepting-d";

				end

	end
	if npc_talk_index == "n100_event_ilyn-accepting-se_back" then
				api_npc_NextScript( pRoom, userObjID, npcObjID, "quest", "n100_event_ilyn.xml");

	end
	if npc_talk_index == "n100_event_ilyn-1-exchange_love" then 
				if api_user_HasItem( pRoom, userObjID, 805307245, 1) >= 1 then
									npc_talk_index = "n100_event_ilyn-1-c";

				else
									npc_talk_index = "n100_event_ilyn-1-b";

				end
	end
	if npc_talk_index == "n100_event_ilyn-1-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21830, questID, 1);
			 end 
	end
	if npc_talk_index == "n100_event_ilyn-1-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_user_DelItem( pRoom, userObjID, 805307245, 1, questID);
	end
	if npc_talk_index == "n100_event_ilyn-1-se_back" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "quest", "n100_event_ilyn.xml");
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2183_halloween_2012_2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2183);
	if qstep == 1 and CountIndex == 805307245 then
				if api_user_HasItem( pRoom, userObjID, 805307245, 1) >= 1 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-accepting-d";

				end

	end
end

function sq_2183_halloween_2012_2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2183);
	if qstep == 1 and CountIndex == 805307245 and Count >= TargetCount  then

	end
end

function sq_2183_halloween_2012_2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2183);
	local questID=2183;
end

function sq_2183_halloween_2012_2_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805307245, 1);
end

function sq_2183_halloween_2012_2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_2183_halloween_2012_2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				npc_talk_index = "n100_event_ilyn-1";
				npc_talk_index = "n100_event_ilyn-accepting-d";
				api_quest_AddQuest( pRoom, userObjID,2183, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2183, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2183, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805307245, 1);
				if api_user_HasItem( pRoom, userObjID, 805307245, 1) >= 1 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-accepting-d";

				end
end

</GameServer>