<VillageServer>

function sq11_1333_cook_skill_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 351 then
		sq11_1333_cook_skill_OnTalk_n351_farm(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n351_farm--------------------------------------------------------------------------------
function sq11_1333_cook_skill_OnTalk_n351_farm(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n351_farm-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n351_farm-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n351_farm-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n351_farm-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n351_farm-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 13330, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 13330, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 13330, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 13330, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 13330, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 13330, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 13330, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 13330, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 13330, false);
			 end 

	end
	if npc_talk_index == "n351_farm-accepting-acceptted" then
				api_quest_AddQuest(userObjID,1333, 1);
				api_quest_SetJournalStep(userObjID,1333, 1);
				api_quest_SetQuestStep(userObjID,1333, 1);
				npc_talk_index = "n351_farm-1";

	end
	if npc_talk_index == "n351_farm-1-b" then 
	end
	if npc_talk_index == "n351_farm-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n351_farm-2-chk_item" then 
				if api_user_HasItem(userObjID, 348351490, 1) >= 1 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n351_farm-3";
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									npc_talk_index = "n351_farm-2-a";

				end
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 348351490, 1);
	end
	if npc_talk_index == "n351_farm-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 13330, true);
				 api_quest_RewardQuestUser(userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 13330, true);
				 api_quest_RewardQuestUser(userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 13330, true);
				 api_quest_RewardQuestUser(userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 13330, true);
				 api_quest_RewardQuestUser(userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 13330, true);
				 api_quest_RewardQuestUser(userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 13330, true);
				 api_quest_RewardQuestUser(userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 13330, true);
				 api_quest_RewardQuestUser(userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 13330, true);
				 api_quest_RewardQuestUser(userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 13330, true);
				 api_quest_RewardQuestUser(userObjID, 13330, questID, 1);
			 end 
	end
	if npc_talk_index == "n351_farm-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_user_DelItem(userObjID, 348351490, 1, questID);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_1333_cook_skill_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1333);
	if qstep == 3 and CountIndex == 348351490 then

	end
end

function sq11_1333_cook_skill_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1333);
	if qstep == 3 and CountIndex == 348351490 and Count >= TargetCount  then

	end
end

function sq11_1333_cook_skill_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1333);
	local questID=1333;
end

function sq11_1333_cook_skill_OnRemoteStart( userObjID, questID )
end

function sq11_1333_cook_skill_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_1333_cook_skill_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,1333, 1);
				api_quest_SetJournalStep(userObjID,1333, 1);
				api_quest_SetQuestStep(userObjID,1333, 1);
				npc_talk_index = "n351_farm-1";
end

</VillageServer>

<GameServer>
function sq11_1333_cook_skill_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 351 then
		sq11_1333_cook_skill_OnTalk_n351_farm( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n351_farm--------------------------------------------------------------------------------
function sq11_1333_cook_skill_OnTalk_n351_farm( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n351_farm-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n351_farm-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n351_farm-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n351_farm-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n351_farm-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, false);
			 end 

	end
	if npc_talk_index == "n351_farm-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,1333, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1333, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1333, 1);
				npc_talk_index = "n351_farm-1";

	end
	if npc_talk_index == "n351_farm-1-b" then 
	end
	if npc_talk_index == "n351_farm-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n351_farm-2-chk_item" then 
				if api_user_HasItem( pRoom, userObjID, 348351490, 1) >= 1 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n351_farm-3";
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									npc_talk_index = "n351_farm-2-a";

				end
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 348351490, 1);
	end
	if npc_talk_index == "n351_farm-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 13330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 13330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 13330, questID, 1);
			 end 
	end
	if npc_talk_index == "n351_farm-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_user_DelItem( pRoom, userObjID, 348351490, 1, questID);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_1333_cook_skill_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1333);
	if qstep == 3 and CountIndex == 348351490 then

	end
end

function sq11_1333_cook_skill_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1333);
	if qstep == 3 and CountIndex == 348351490 and Count >= TargetCount  then

	end
end

function sq11_1333_cook_skill_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1333);
	local questID=1333;
end

function sq11_1333_cook_skill_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_1333_cook_skill_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_1333_cook_skill_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,1333, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1333, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1333, 1);
				npc_talk_index = "n351_farm-1";
end

</GameServer>