<VillageServer>

function mq08_225_get_back_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 28 then
		mq08_225_get_back_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 34 then
		mq08_225_get_back_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 392 then
		mq08_225_get_back_OnTalk_n392_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 543 then
		mq08_225_get_back_OnTalk_n543_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function mq08_225_get_back_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,225, 2);
				api_quest_SetJournalStep(userObjID,225, 1);
				api_quest_SetQuestStep(userObjID,225, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-check_level01" then 
				if api_user_GetUserLevel(userObjID) >= 12 then
									npc_talk_index = "n028_scholar_bailey-1-a";

				else
									npc_talk_index = "n028_scholar_bailey-1-c";

				end
	end
	if npc_talk_index == "n028_scholar_bailey-1-b" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n034_cleric_master_germain--------------------------------------------------------------------------------
function mq08_225_get_back_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n034_cleric_master_germain-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n034_cleric_master_germain-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n034_cleric_master_germain-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-2-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-f" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 433, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200433, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300030, 1);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n392_academic_station--------------------------------------------------------------------------------
function mq08_225_get_back_OnTalk_n392_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n392_academic_station-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n392_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n392_academic_station-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n392_academic_station-5-b" then 
	end
	if npc_talk_index == "n392_academic_station-5-b" then 
	end
	if npc_talk_index == "n392_academic_station-5-b" then 
	end
	if npc_talk_index == "n392_academic_station-5-c" then 
	end
	if npc_talk_index == "n392_academic_station-5-d" then 
	end
	if npc_talk_index == "n392_academic_station-5-e" then 
	end
	if npc_talk_index == "n392_academic_station-5-f" then 
	end
	if npc_talk_index == "n392_academic_station-5-g" then 
	end
	if npc_talk_index == "n392_academic_station-5-h" then 
	end
	if npc_talk_index == "n392_academic_station-5-i" then 
	end
	if npc_talk_index == "n392_academic_station-5-j" then 
	end
	if npc_talk_index == "n392_academic_station-5-k" then 
	end
	if npc_talk_index == "n392_academic_station-5-l" then 
	end
	if npc_talk_index == "n392_academic_station-5-m" then 
	end
	if npc_talk_index == "n392_academic_station-5-n" then 
	end
	if npc_talk_index == "n392_academic_station-5-o" then 
	end
	if npc_talk_index == "n392_academic_station-5-p" then 
	end
	if npc_talk_index == "n392_academic_station-5-q" then 
	end
	if npc_talk_index == "n392_academic_station-5-r" then 
	end
	if npc_talk_index == "n392_academic_station-5-s" then 
	end
	if npc_talk_index == "n392_academic_station-5-t" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2250, true);
				 api_quest_RewardQuestUser(userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2250, true);
				 api_quest_RewardQuestUser(userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2250, true);
				 api_quest_RewardQuestUser(userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2250, true);
				 api_quest_RewardQuestUser(userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2250, true);
				 api_quest_RewardQuestUser(userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2250, true);
				 api_quest_RewardQuestUser(userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2250, true);
				 api_quest_RewardQuestUser(userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2250, true);
				 api_quest_RewardQuestUser(userObjID, 2250, questID, 1);
			 end 
	end
	if npc_talk_index == "n392_academic_station-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 226, 2);
					api_quest_SetQuestStep(userObjID, 226, 1);
					api_quest_SetJournalStep(userObjID, 226, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n392_academic_station-1", "mq08_226_unexpected_situation.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n543_angelica--------------------------------------------------------------------------------
function mq08_225_get_back_OnTalk_n543_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n543_angelica-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n543_angelica-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n543_angelica-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n543_angelica-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n543_angelica-4-b" then 
	end
	if npc_talk_index == "n543_angelica-4-c" then 
	end
	if npc_talk_index == "n543_angelica-4-d" then 
	end
	if npc_talk_index == "n543_angelica-4-e" then 
	end
	if npc_talk_index == "n543_angelica-4-f" then 
	end
	if npc_talk_index == "n543_angelica-4-g" then 
	end
	if npc_talk_index == "n543_angelica-4-h" then 
	end
	if npc_talk_index == "n543_angelica-4-i" then 
	end
	if npc_talk_index == "n543_angelica-4-j" then 
	end
	if npc_talk_index == "n543_angelica-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);

				if api_quest_HasQuestItem(userObjID, 300030, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300030, api_quest_HasQuestItem(userObjID, 300030, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_225_get_back_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 225);
	if qstep == 3 and CountIndex == 433 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300030, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300030, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200433 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300030, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300030, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300030 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

	end
end

function mq08_225_get_back_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 225);
	if qstep == 3 and CountIndex == 433 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200433 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300030 and Count >= TargetCount  then

	end
end

function mq08_225_get_back_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 225);
	local questID=225;
end

function mq08_225_get_back_OnRemoteStart( userObjID, questID )
end

function mq08_225_get_back_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_225_get_back_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,225, 2);
				api_quest_SetJournalStep(userObjID,225, 1);
				api_quest_SetQuestStep(userObjID,225, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</VillageServer>

<GameServer>
function mq08_225_get_back_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 28 then
		mq08_225_get_back_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 34 then
		mq08_225_get_back_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 392 then
		mq08_225_get_back_OnTalk_n392_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 543 then
		mq08_225_get_back_OnTalk_n543_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function mq08_225_get_back_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,225, 2);
				api_quest_SetJournalStep( pRoom, userObjID,225, 1);
				api_quest_SetQuestStep( pRoom, userObjID,225, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-check_level01" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 12 then
									npc_talk_index = "n028_scholar_bailey-1-a";

				else
									npc_talk_index = "n028_scholar_bailey-1-c";

				end
	end
	if npc_talk_index == "n028_scholar_bailey-1-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n034_cleric_master_germain--------------------------------------------------------------------------------
function mq08_225_get_back_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n034_cleric_master_germain-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n034_cleric_master_germain-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n034_cleric_master_germain-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-2-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-f" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 433, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200433, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300030, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n392_academic_station--------------------------------------------------------------------------------
function mq08_225_get_back_OnTalk_n392_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n392_academic_station-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n392_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n392_academic_station-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n392_academic_station-5-b" then 
	end
	if npc_talk_index == "n392_academic_station-5-b" then 
	end
	if npc_talk_index == "n392_academic_station-5-b" then 
	end
	if npc_talk_index == "n392_academic_station-5-c" then 
	end
	if npc_talk_index == "n392_academic_station-5-d" then 
	end
	if npc_talk_index == "n392_academic_station-5-e" then 
	end
	if npc_talk_index == "n392_academic_station-5-f" then 
	end
	if npc_talk_index == "n392_academic_station-5-g" then 
	end
	if npc_talk_index == "n392_academic_station-5-h" then 
	end
	if npc_talk_index == "n392_academic_station-5-i" then 
	end
	if npc_talk_index == "n392_academic_station-5-j" then 
	end
	if npc_talk_index == "n392_academic_station-5-k" then 
	end
	if npc_talk_index == "n392_academic_station-5-l" then 
	end
	if npc_talk_index == "n392_academic_station-5-m" then 
	end
	if npc_talk_index == "n392_academic_station-5-n" then 
	end
	if npc_talk_index == "n392_academic_station-5-o" then 
	end
	if npc_talk_index == "n392_academic_station-5-p" then 
	end
	if npc_talk_index == "n392_academic_station-5-q" then 
	end
	if npc_talk_index == "n392_academic_station-5-r" then 
	end
	if npc_talk_index == "n392_academic_station-5-s" then 
	end
	if npc_talk_index == "n392_academic_station-5-t" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2250, questID, 1);
			 end 
	end
	if npc_talk_index == "n392_academic_station-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 226, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 226, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 226, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n392_academic_station-1", "mq08_226_unexpected_situation.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n543_angelica--------------------------------------------------------------------------------
function mq08_225_get_back_OnTalk_n543_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n543_angelica-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n543_angelica-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n543_angelica-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n543_angelica-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n543_angelica-4-b" then 
	end
	if npc_talk_index == "n543_angelica-4-c" then 
	end
	if npc_talk_index == "n543_angelica-4-d" then 
	end
	if npc_talk_index == "n543_angelica-4-e" then 
	end
	if npc_talk_index == "n543_angelica-4-f" then 
	end
	if npc_talk_index == "n543_angelica-4-g" then 
	end
	if npc_talk_index == "n543_angelica-4-h" then 
	end
	if npc_talk_index == "n543_angelica-4-i" then 
	end
	if npc_talk_index == "n543_angelica-4-j" then 
	end
	if npc_talk_index == "n543_angelica-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

				if api_quest_HasQuestItem( pRoom, userObjID, 300030, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300030, api_quest_HasQuestItem( pRoom, userObjID, 300030, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_225_get_back_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 225);
	if qstep == 3 and CountIndex == 433 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300030, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300030, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200433 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300030, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300030, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300030 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

	end
end

function mq08_225_get_back_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 225);
	if qstep == 3 and CountIndex == 433 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200433 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300030 and Count >= TargetCount  then

	end
end

function mq08_225_get_back_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 225);
	local questID=225;
end

function mq08_225_get_back_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq08_225_get_back_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_225_get_back_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,225, 2);
				api_quest_SetJournalStep( pRoom, userObjID,225, 1);
				api_quest_SetQuestStep( pRoom, userObjID,225, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</GameServer>