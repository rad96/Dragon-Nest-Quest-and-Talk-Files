<VillageServer>

function sq11_536_ringleader_of_the_disaster_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 41 then
		sq11_536_ringleader_of_the_disaster_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function sq11_536_ringleader_of_the_disaster_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n041_duke_stwart-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_quest_SetQuestStep(userObjID, questID,2);
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n041_duke_stwart-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n041_duke_stwart-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n041_duke_stwart-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5361, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5362, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5363, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5364, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5365, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5366, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5367, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5368, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5369, false);
			 end 

	end
	if npc_talk_index == "n041_duke_stwart-accepting-acceptted" then
				api_quest_AddQuest(userObjID,536, 1);
				api_quest_SetJournalStep(userObjID,536, 1);
				api_quest_SetQuestStep(userObjID,536, 1);
				npc_talk_index = "n041_duke_stwart-1";

	end
	if npc_talk_index == "n041_duke_stwart-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 884, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 888, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 889, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200884, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200888, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200889, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 3, 400140, 1);
				api_quest_SetCountingInfo(userObjID, questID, 7, 3, 400141, 1);
				api_quest_SetCountingInfo(userObjID, questID, 8, 3, 400142, 1);
	end
	if npc_talk_index == "n041_duke_stwart-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);

				if api_quest_HasQuestItem(userObjID, 400140, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400140, api_quest_HasQuestItem(userObjID, 400140, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400141, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400141, api_quest_HasQuestItem(userObjID, 400141, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400142, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400142, api_quest_HasQuestItem(userObjID, 400142, 1));
				end
	end
	if npc_talk_index == "n041_duke_stwart-5-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-5-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-5-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-5-e" then 
	end
	if npc_talk_index == "n041_duke_stwart-5-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5361, true);
				 api_quest_RewardQuestUser(userObjID, 5361, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5362, true);
				 api_quest_RewardQuestUser(userObjID, 5362, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5363, true);
				 api_quest_RewardQuestUser(userObjID, 5363, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5364, true);
				 api_quest_RewardQuestUser(userObjID, 5364, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5365, true);
				 api_quest_RewardQuestUser(userObjID, 5365, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5366, true);
				 api_quest_RewardQuestUser(userObjID, 5366, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5367, true);
				 api_quest_RewardQuestUser(userObjID, 5367, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5368, true);
				 api_quest_RewardQuestUser(userObjID, 5368, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5369, true);
				 api_quest_RewardQuestUser(userObjID, 5369, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-5-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_536_ringleader_of_the_disaster_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 536);
	if qstep == 2 and CountIndex == 884 then
				if api_quest_HasQuestItem(userObjID, 400140, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400140, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400140, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 888 then
				if api_quest_HasQuestItem(userObjID, 400141, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400141, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400141, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 889 then
				if api_quest_HasQuestItem(userObjID, 400142, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400142, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400142, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200884 then
				if api_quest_HasQuestItem(userObjID, 400140, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400140, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400140, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200888 then
				if api_quest_HasQuestItem(userObjID, 400141, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400141, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400141, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200889 then
				if api_quest_HasQuestItem(userObjID, 400142, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400142, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400142, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 400140 then

	end
	if qstep == 2 and CountIndex == 400141 then

	end
	if qstep == 2 and CountIndex == 400142 then
				if api_quest_HasQuestItem(userObjID, 400140, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400141, 1) >= 1  and api_quest_HasQuestItem(userObjID, 400142, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

				else
				end

	end
end

function sq11_536_ringleader_of_the_disaster_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 536);
	if qstep == 2 and CountIndex == 884 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 888 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 889 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200884 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200888 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200889 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400140 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400141 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400142 and Count >= TargetCount  then

	end
end

function sq11_536_ringleader_of_the_disaster_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 536);
	local questID=536;
end

function sq11_536_ringleader_of_the_disaster_OnRemoteStart( userObjID, questID )
end

function sq11_536_ringleader_of_the_disaster_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_536_ringleader_of_the_disaster_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,536, 1);
				api_quest_SetJournalStep(userObjID,536, 1);
				api_quest_SetQuestStep(userObjID,536, 1);
				npc_talk_index = "n041_duke_stwart-1";
end

</VillageServer>

<GameServer>
function sq11_536_ringleader_of_the_disaster_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 41 then
		sq11_536_ringleader_of_the_disaster_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function sq11_536_ringleader_of_the_disaster_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n041_duke_stwart-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n041_duke_stwart-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n041_duke_stwart-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n041_duke_stwart-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5361, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5362, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5363, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5364, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5365, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5366, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5367, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5368, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5369, false);
			 end 

	end
	if npc_talk_index == "n041_duke_stwart-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,536, 1);
				api_quest_SetJournalStep( pRoom, userObjID,536, 1);
				api_quest_SetQuestStep( pRoom, userObjID,536, 1);
				npc_talk_index = "n041_duke_stwart-1";

	end
	if npc_talk_index == "n041_duke_stwart-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 884, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 888, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 889, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200884, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200888, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200889, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 3, 400140, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 3, 400141, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 3, 400142, 1);
	end
	if npc_talk_index == "n041_duke_stwart-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);

				if api_quest_HasQuestItem( pRoom, userObjID, 400140, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400140, api_quest_HasQuestItem( pRoom, userObjID, 400140, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400141, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400141, api_quest_HasQuestItem( pRoom, userObjID, 400141, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400142, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400142, api_quest_HasQuestItem( pRoom, userObjID, 400142, 1));
				end
	end
	if npc_talk_index == "n041_duke_stwart-5-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-5-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-5-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-5-e" then 
	end
	if npc_talk_index == "n041_duke_stwart-5-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5361, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5361, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5362, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5362, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5363, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5363, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5364, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5364, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5365, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5365, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5366, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5366, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5367, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5367, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5368, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5368, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5369, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5369, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-5-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_536_ringleader_of_the_disaster_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 536);
	if qstep == 2 and CountIndex == 884 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400140, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400140, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400140, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 888 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400141, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400141, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400141, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 889 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400142, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400142, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400142, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200884 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400140, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400140, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400140, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200888 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400141, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400141, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400141, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200889 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400142, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400142, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400142, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 400140 then

	end
	if qstep == 2 and CountIndex == 400141 then

	end
	if qstep == 2 and CountIndex == 400142 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400140, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400141, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 400142, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

				else
				end

	end
end

function sq11_536_ringleader_of_the_disaster_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 536);
	if qstep == 2 and CountIndex == 884 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 888 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 889 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200884 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200888 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200889 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400140 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400141 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400142 and Count >= TargetCount  then

	end
end

function sq11_536_ringleader_of_the_disaster_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 536);
	local questID=536;
end

function sq11_536_ringleader_of_the_disaster_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_536_ringleader_of_the_disaster_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_536_ringleader_of_the_disaster_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,536, 1);
				api_quest_SetJournalStep( pRoom, userObjID,536, 1);
				api_quest_SetQuestStep( pRoom, userObjID,536, 1);
				npc_talk_index = "n041_duke_stwart-1";
end

</GameServer>