<VillageServer>

function dq_guild_1287_key_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 996 then
		dq_guild_1287_key_OnTalk_n996_guild_quest_helper(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n996_guild_quest_helper--------------------------------------------------------------------------------
function dq_guild_1287_key_OnTalk_n996_guild_quest_helper(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n996_guild_quest_helper-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n996_guild_quest_helper-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n996_guild_quest_helper-accepting-acceptted" then
				api_quest_AddQuest(userObjID,1287, 1);
				api_quest_SetJournalStep(userObjID,1287, 1);
				api_quest_SetQuestStep(userObjID,1287, 1);
				npc_talk_index = "n996_guild_quest_helper-1";

	end
	if npc_talk_index == "n996_guild_quest_helper-1-b" then 
	end
	if npc_talk_index == "n996_guild_quest_helper-1-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 12870, true);
				 api_quest_RewardQuestUser(userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 12870, true);
				 api_quest_RewardQuestUser(userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 12870, true);
				 api_quest_RewardQuestUser(userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 12870, true);
				 api_quest_RewardQuestUser(userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 12870, true);
				 api_quest_RewardQuestUser(userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 12870, true);
				 api_quest_RewardQuestUser(userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 12870, true);
				 api_quest_RewardQuestUser(userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 12870, true);
				 api_quest_RewardQuestUser(userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 12870, true);
				 api_quest_RewardQuestUser(userObjID, 12870, questID, 1);
			 end 
	end
	if npc_talk_index == "n996_guild_quest_helper-1-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n996_guild_quest_helper-1-back" then 
				api_npc_NextScript(userObjID, npcObjID, "talk", "n996_guild_quest_helper.xml");
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq_guild_1287_key_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1287);
end

function dq_guild_1287_key_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1287);
end

function dq_guild_1287_key_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1287);
	local questID=1287;
end

function dq_guild_1287_key_OnRemoteStart( userObjID, questID )
end

function dq_guild_1287_key_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function dq_guild_1287_key_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,1287, 1);
				api_quest_SetJournalStep(userObjID,1287, 1);
				api_quest_SetQuestStep(userObjID,1287, 1);
				npc_talk_index = "n996_guild_quest_helper-1";
end

</VillageServer>

<GameServer>
function dq_guild_1287_key_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 996 then
		dq_guild_1287_key_OnTalk_n996_guild_quest_helper( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n996_guild_quest_helper--------------------------------------------------------------------------------
function dq_guild_1287_key_OnTalk_n996_guild_quest_helper( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n996_guild_quest_helper-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n996_guild_quest_helper-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n996_guild_quest_helper-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,1287, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1287, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1287, 1);
				npc_talk_index = "n996_guild_quest_helper-1";

	end
	if npc_talk_index == "n996_guild_quest_helper-1-b" then 
	end
	if npc_talk_index == "n996_guild_quest_helper-1-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12870, questID, 1);
			 end 
	end
	if npc_talk_index == "n996_guild_quest_helper-1-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n996_guild_quest_helper-1-back" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "talk", "n996_guild_quest_helper.xml");
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq_guild_1287_key_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1287);
end

function dq_guild_1287_key_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1287);
end

function dq_guild_1287_key_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1287);
	local questID=1287;
end

function dq_guild_1287_key_OnRemoteStart( pRoom,  userObjID, questID )
end

function dq_guild_1287_key_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function dq_guild_1287_key_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,1287, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1287, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1287, 1);
				npc_talk_index = "n996_guild_quest_helper-1";
end

</GameServer>