<VillageServer>

function mqc14_9426_the_man_who_open_the_door_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1515 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1545 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1545_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1546 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1546_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1547 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1547_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1505 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1505_elf_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1548 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1548_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1518 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1518_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1537 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1537_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9426, 2);
				api_quest_SetJournalStep(userObjID,9426, 1);
				api_quest_SetQuestStep(userObjID,9426, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n1067_elder_elf-10";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-1-levelchk" then 
				if api_user_GetUserLevel(userObjID) >= 74 then
									npc_talk_index = "n1067_elder_elf-1-b";

				else
									npc_talk_index = "n1067_elder_elf-1-e";

				end
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-10-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94261, true);
				 api_quest_RewardQuestUser(userObjID, 94261, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94262, true);
				 api_quest_RewardQuestUser(userObjID, 94262, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94263, true);
				 api_quest_RewardQuestUser(userObjID, 94263, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94264, true);
				 api_quest_RewardQuestUser(userObjID, 94264, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94265, true);
				 api_quest_RewardQuestUser(userObjID, 94265, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94266, true);
				 api_quest_RewardQuestUser(userObjID, 94266, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94267, true);
				 api_quest_RewardQuestUser(userObjID, 94267, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94268, true);
				 api_quest_RewardQuestUser(userObjID, 94268, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94269, true);
				 api_quest_RewardQuestUser(userObjID, 94269, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-10-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9427, 2);
					api_quest_SetQuestStep(userObjID, 9427, 1);
					api_quest_SetJournalStep(userObjID, 9427, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1067_elder_elf-10-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1067_elder_elf-1", "mqc14_9427_invasion.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1545_pether--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1545_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1545_pether-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1546_lunaria--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1546_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1546_lunaria-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1546_lunaria-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1547_for_memory--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1547_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1547_for_memory-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1547_for_memory-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1547_for_memory-3-b" then 
	end
	if npc_talk_index == "n1547_for_memory-3-c" then 
	end
	if npc_talk_index == "n1547_for_memory-3-d" then 
	end
	if npc_talk_index == "n1547_for_memory-3-e" then 
	end
	if npc_talk_index == "n1547_for_memory-3-f" then 
	end
	if npc_talk_index == "n1547_for_memory-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1505_elf_king--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1505_elf_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1505_elf_king-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1505_elf_king-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1505_elf_king-5-b" then 
	end
	if npc_talk_index == "n1505_elf_king-5-c" then 
	end
	if npc_talk_index == "n1505_elf_king-5-d" then 
	end
	if npc_talk_index == "n1505_elf_king-5-e" then 
	end
	if npc_talk_index == "n1505_elf_king-5-f" then 
	end
	if npc_talk_index == "n1505_elf_king-5-g" then 
	end
	if npc_talk_index == "n1505_elf_king-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1548_lunaria--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1548_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1548_lunaria-6";
				if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1548_lunaria-6-i";

				else
									npc_talk_index = "n1548_lunaria-6";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1548_lunaria-7";
				if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1548_lunaria-7-r";

				else
									npc_talk_index = "n1548_lunaria-7";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1548_lunaria-6-b" then 
	end
	if npc_talk_index == "n1548_lunaria-6-c" then 
	end
	if npc_talk_index == "n1548_lunaria-6-d" then 
	end
	if npc_talk_index == "n1548_lunaria-6-e" then 
	end
	if npc_talk_index == "n1548_lunaria-6-f" then 
	end
	if npc_talk_index == "n1548_lunaria-6-g" then 
	end
	if npc_talk_index == "n1548_lunaria-6-h" then 
	end
	if npc_talk_index == "n1548_lunaria-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
	end
	if npc_talk_index == "n1548_lunaria-6-j" then 
	end
	if npc_talk_index == "n1548_lunaria-6-k" then 
	end
	if npc_talk_index == "n1548_lunaria-6-l" then 
	end
	if npc_talk_index == "n1548_lunaria-6-m" then 
	end
	if npc_talk_index == "n1548_lunaria-6-n" then 
	end
	if npc_talk_index == "n1548_lunaria-6-o" then 
	end
	if npc_talk_index == "n1548_lunaria-6-p" then 
	end
	if npc_talk_index == "n1548_lunaria-6-q" then 
	end
	if npc_talk_index == "n1548_lunaria-6-r" then 
	end
	if npc_talk_index == "n1548_lunaria-6-s" then 
	end
	if npc_talk_index == "n1548_lunaria-6-next_step" then 
				npc_talk_index = "n1548_lunaria-7-r";
				api_quest_SetQuestStep(userObjID, questID,7);
	end
	if npc_talk_index == "n1548_lunaria-7-b" then 
	end
	if npc_talk_index == "n1548_lunaria-7-c" then 
	end
	if npc_talk_index == "n1548_lunaria-7-d" then 
	end
	if npc_talk_index == "n1548_lunaria-7-e" then 
	end
	if npc_talk_index == "n1548_lunaria-7-f" then 
	end
	if npc_talk_index == "n1548_lunaria-7-g" then 
	end
	if npc_talk_index == "n1548_lunaria-7-h" then 
	end
	if npc_talk_index == "n1548_lunaria-7-i" then 
	end
	if npc_talk_index == "n1548_lunaria-7-j" then 
	end
	if npc_talk_index == "n1548_lunaria-7-k" then 
	end
	if npc_talk_index == "n1548_lunaria-7-l" then 
	end
	if npc_talk_index == "n1548_lunaria-7-m" then 
	end
	if npc_talk_index == "n1548_lunaria-7-n" then 
	end
	if npc_talk_index == "n1548_lunaria-7-o" then 
	end
	if npc_talk_index == "n1548_lunaria-7-p" then 
	end
	if npc_talk_index == "n1548_lunaria-7-q" then 
	end
	if npc_talk_index == "n1548_lunaria-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n1548_lunaria-7-s" then 
	end
	if npc_talk_index == "n1548_lunaria-7-t" then 
	end
	if npc_talk_index == "n1548_lunaria-7-u" then 
	end
	if npc_talk_index == "n1548_lunaria-7-v" then 
	end
	if npc_talk_index == "n1548_lunaria-7-w" then 
	end
	if npc_talk_index == "n1548_lunaria-7-x" then 
	end
	if npc_talk_index == "n1548_lunaria-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1518_for_memory--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1518_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1518_for_memory-8";
				if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1518_for_memory-8";

				else
									npc_talk_index = "n1518_for_memory-8-c";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1518_for_memory-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1518_for_memory-8-b" then 
	end
	if npc_talk_index == "n1518_for_memory-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end
	if npc_talk_index == "n1518_for_memory-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1537_gereint_kid--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1537_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1537_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9426_the_man_who_open_the_door_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9426);
end

function mqc14_9426_the_man_who_open_the_door_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9426);
end

function mqc14_9426_the_man_who_open_the_door_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9426);
	local questID=9426;
end

function mqc14_9426_the_man_who_open_the_door_OnRemoteStart( userObjID, questID )
end

function mqc14_9426_the_man_who_open_the_door_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9426_the_man_who_open_the_door_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9426, 2);
				api_quest_SetJournalStep(userObjID,9426, 1);
				api_quest_SetQuestStep(userObjID,9426, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</VillageServer>

<GameServer>
function mqc14_9426_the_man_who_open_the_door_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1515 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1545 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1545_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1546 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1546_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1547 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1547_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1505 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1505_elf_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1548 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1548_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1518 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1518_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1537 then
		mqc14_9426_the_man_who_open_the_door_OnTalk_n1537_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9426, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9426, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9426, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n1067_elder_elf-10";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-1-levelchk" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 74 then
									npc_talk_index = "n1067_elder_elf-1-b";

				else
									npc_talk_index = "n1067_elder_elf-1-e";

				end
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-10-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94261, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94261, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94262, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94262, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94263, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94263, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94264, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94264, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94265, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94265, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94266, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94266, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94267, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94267, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94268, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94268, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94269, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94269, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-10-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9427, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9427, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9427, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1067_elder_elf-10-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1067_elder_elf-1", "mqc14_9427_invasion.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1545_pether--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1545_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1545_pether-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1546_lunaria--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1546_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1546_lunaria-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1546_lunaria-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1547_for_memory--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1547_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1547_for_memory-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1547_for_memory-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1547_for_memory-3-b" then 
	end
	if npc_talk_index == "n1547_for_memory-3-c" then 
	end
	if npc_talk_index == "n1547_for_memory-3-d" then 
	end
	if npc_talk_index == "n1547_for_memory-3-e" then 
	end
	if npc_talk_index == "n1547_for_memory-3-f" then 
	end
	if npc_talk_index == "n1547_for_memory-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1505_elf_king--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1505_elf_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1505_elf_king-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1505_elf_king-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1505_elf_king-5-b" then 
	end
	if npc_talk_index == "n1505_elf_king-5-c" then 
	end
	if npc_talk_index == "n1505_elf_king-5-d" then 
	end
	if npc_talk_index == "n1505_elf_king-5-e" then 
	end
	if npc_talk_index == "n1505_elf_king-5-f" then 
	end
	if npc_talk_index == "n1505_elf_king-5-g" then 
	end
	if npc_talk_index == "n1505_elf_king-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1548_lunaria--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1548_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1548_lunaria-6";
				if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1548_lunaria-6-i";

				else
									npc_talk_index = "n1548_lunaria-6";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1548_lunaria-7";
				if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1548_lunaria-7-r";

				else
									npc_talk_index = "n1548_lunaria-7";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1548_lunaria-6-b" then 
	end
	if npc_talk_index == "n1548_lunaria-6-c" then 
	end
	if npc_talk_index == "n1548_lunaria-6-d" then 
	end
	if npc_talk_index == "n1548_lunaria-6-e" then 
	end
	if npc_talk_index == "n1548_lunaria-6-f" then 
	end
	if npc_talk_index == "n1548_lunaria-6-g" then 
	end
	if npc_talk_index == "n1548_lunaria-6-h" then 
	end
	if npc_talk_index == "n1548_lunaria-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
	end
	if npc_talk_index == "n1548_lunaria-6-j" then 
	end
	if npc_talk_index == "n1548_lunaria-6-k" then 
	end
	if npc_talk_index == "n1548_lunaria-6-l" then 
	end
	if npc_talk_index == "n1548_lunaria-6-m" then 
	end
	if npc_talk_index == "n1548_lunaria-6-n" then 
	end
	if npc_talk_index == "n1548_lunaria-6-o" then 
	end
	if npc_talk_index == "n1548_lunaria-6-p" then 
	end
	if npc_talk_index == "n1548_lunaria-6-q" then 
	end
	if npc_talk_index == "n1548_lunaria-6-r" then 
	end
	if npc_talk_index == "n1548_lunaria-6-s" then 
	end
	if npc_talk_index == "n1548_lunaria-6-next_step" then 
				npc_talk_index = "n1548_lunaria-7-r";
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
	end
	if npc_talk_index == "n1548_lunaria-7-b" then 
	end
	if npc_talk_index == "n1548_lunaria-7-c" then 
	end
	if npc_talk_index == "n1548_lunaria-7-d" then 
	end
	if npc_talk_index == "n1548_lunaria-7-e" then 
	end
	if npc_talk_index == "n1548_lunaria-7-f" then 
	end
	if npc_talk_index == "n1548_lunaria-7-g" then 
	end
	if npc_talk_index == "n1548_lunaria-7-h" then 
	end
	if npc_talk_index == "n1548_lunaria-7-i" then 
	end
	if npc_talk_index == "n1548_lunaria-7-j" then 
	end
	if npc_talk_index == "n1548_lunaria-7-k" then 
	end
	if npc_talk_index == "n1548_lunaria-7-l" then 
	end
	if npc_talk_index == "n1548_lunaria-7-m" then 
	end
	if npc_talk_index == "n1548_lunaria-7-n" then 
	end
	if npc_talk_index == "n1548_lunaria-7-o" then 
	end
	if npc_talk_index == "n1548_lunaria-7-p" then 
	end
	if npc_talk_index == "n1548_lunaria-7-q" then 
	end
	if npc_talk_index == "n1548_lunaria-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n1548_lunaria-7-s" then 
	end
	if npc_talk_index == "n1548_lunaria-7-t" then 
	end
	if npc_talk_index == "n1548_lunaria-7-u" then 
	end
	if npc_talk_index == "n1548_lunaria-7-v" then 
	end
	if npc_talk_index == "n1548_lunaria-7-w" then 
	end
	if npc_talk_index == "n1548_lunaria-7-x" then 
	end
	if npc_talk_index == "n1548_lunaria-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1518_for_memory--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1518_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1518_for_memory-8";
				if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1518_for_memory-8";

				else
									npc_talk_index = "n1518_for_memory-8-c";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1518_for_memory-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1518_for_memory-8-b" then 
	end
	if npc_talk_index == "n1518_for_memory-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end
	if npc_talk_index == "n1518_for_memory-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1537_gereint_kid--------------------------------------------------------------------------------
function mqc14_9426_the_man_who_open_the_door_OnTalk_n1537_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1537_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9426_the_man_who_open_the_door_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9426);
end

function mqc14_9426_the_man_who_open_the_door_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9426);
end

function mqc14_9426_the_man_who_open_the_door_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9426);
	local questID=9426;
end

function mqc14_9426_the_man_who_open_the_door_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9426_the_man_who_open_the_door_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9426_the_man_who_open_the_door_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9426, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9426, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9426, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</GameServer>