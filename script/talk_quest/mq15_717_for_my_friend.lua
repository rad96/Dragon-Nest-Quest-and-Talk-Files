<VillageServer>

function mq15_717_for_my_friend_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 495 then
		mq15_717_for_my_friend_OnTalk_n495_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_717_for_my_friend_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_717_for_my_friend_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 725 then
		mq15_717_for_my_friend_OnTalk_n725_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n495_sidel--------------------------------------------------------------------------------
function mq15_717_for_my_friend_OnTalk_n495_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n495_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n495_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n495_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n495_sidel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n495_sidel-3-b" then 
	end
	if npc_talk_index == "n495_sidel-3-c" then 
	end
	if npc_talk_index == "n495_sidel-3-d" then 
	end
	if npc_talk_index == "n495_sidel-3-e" then 
	end
	if npc_talk_index == "n495_sidel-3-f" then 
	end
	if npc_talk_index == "n495_sidel-3-g" then 
	end
	if npc_talk_index == "n495_sidel-3-h" then 
	end
	if npc_talk_index == "n495_sidel-3-i" then 
	end
	if npc_talk_index == "n495_sidel-3-j" then 
	end
	if npc_talk_index == "n495_sidel-3-k" then 
	end
	if npc_talk_index == "n495_sidel-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_717_for_my_friend_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				if api_quest_IsMarkingCompleteQuest(userObjID, 758) == 1  then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 767) == 1  then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 763) == 1  then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 754) == 1  then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 4073) == 1  then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									npc_talk_index = "n701_oldkarakule-1";

				end

				end

				end

				end

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest(userObjID,717, 2);
				api_quest_SetJournalStep(userObjID,717, 1);
				api_quest_SetQuestStep(userObjID,717, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n701_oldkarakule-4-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7171, true);
				 api_quest_RewardQuestUser(userObjID, 7171, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7172, true);
				 api_quest_RewardQuestUser(userObjID, 7172, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7173, true);
				 api_quest_RewardQuestUser(userObjID, 7173, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7174, true);
				 api_quest_RewardQuestUser(userObjID, 7174, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7175, true);
				 api_quest_RewardQuestUser(userObjID, 7175, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7176, true);
				 api_quest_RewardQuestUser(userObjID, 7176, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7171, true);
				 api_quest_RewardQuestUser(userObjID, 7171, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7171, true);
				 api_quest_RewardQuestUser(userObjID, 7171, questID, 1);
			 end 
	end
	if npc_talk_index == "n701_oldkarakule-4-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 718, 2);
					api_quest_SetQuestStep(userObjID, 718, 1);
					api_quest_SetJournalStep(userObjID, 718, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_717_for_my_friend_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-2-e" then 
	end
	if npc_talk_index == "n707_sidel-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n725_charty--------------------------------------------------------------------------------
function mq15_717_for_my_friend_OnTalk_n725_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_717_for_my_friend_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 717);
end

function mq15_717_for_my_friend_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 717);
end

function mq15_717_for_my_friend_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 717);
	local questID=717;
end

function mq15_717_for_my_friend_OnRemoteStart( userObjID, questID )
end

function mq15_717_for_my_friend_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_717_for_my_friend_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,717, 2);
				api_quest_SetJournalStep(userObjID,717, 1);
				api_quest_SetQuestStep(userObjID,717, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</VillageServer>

<GameServer>
function mq15_717_for_my_friend_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 495 then
		mq15_717_for_my_friend_OnTalk_n495_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_717_for_my_friend_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_717_for_my_friend_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 725 then
		mq15_717_for_my_friend_OnTalk_n725_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n495_sidel--------------------------------------------------------------------------------
function mq15_717_for_my_friend_OnTalk_n495_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n495_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n495_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n495_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n495_sidel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n495_sidel-3-b" then 
	end
	if npc_talk_index == "n495_sidel-3-c" then 
	end
	if npc_talk_index == "n495_sidel-3-d" then 
	end
	if npc_talk_index == "n495_sidel-3-e" then 
	end
	if npc_talk_index == "n495_sidel-3-f" then 
	end
	if npc_talk_index == "n495_sidel-3-g" then 
	end
	if npc_talk_index == "n495_sidel-3-h" then 
	end
	if npc_talk_index == "n495_sidel-3-i" then 
	end
	if npc_talk_index == "n495_sidel-3-j" then 
	end
	if npc_talk_index == "n495_sidel-3-k" then 
	end
	if npc_talk_index == "n495_sidel-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_717_for_my_friend_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 758) == 1  then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 767) == 1  then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 763) == 1  then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 754) == 1  then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 4073) == 1  then
									npc_talk_index = "n701_oldkarakule-1-c";

				else
									npc_talk_index = "n701_oldkarakule-1";

				end

				end

				end

				end

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,717, 2);
				api_quest_SetJournalStep( pRoom, userObjID,717, 1);
				api_quest_SetQuestStep( pRoom, userObjID,717, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n701_oldkarakule-4-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7171, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7171, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7172, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7172, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7173, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7173, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7174, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7174, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7175, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7175, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7176, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7176, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7171, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7171, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7171, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7171, questID, 1);
			 end 
	end
	if npc_talk_index == "n701_oldkarakule-4-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 718, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 718, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 718, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_717_for_my_friend_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-2-b" then 
	end
	if npc_talk_index == "n707_sidel-2-c" then 
	end
	if npc_talk_index == "n707_sidel-2-d" then 
	end
	if npc_talk_index == "n707_sidel-2-e" then 
	end
	if npc_talk_index == "n707_sidel-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n725_charty--------------------------------------------------------------------------------
function mq15_717_for_my_friend_OnTalk_n725_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_717_for_my_friend_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 717);
end

function mq15_717_for_my_friend_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 717);
end

function mq15_717_for_my_friend_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 717);
	local questID=717;
end

function mq15_717_for_my_friend_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_717_for_my_friend_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_717_for_my_friend_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,717, 2);
				api_quest_SetJournalStep( pRoom, userObjID,717, 1);
				api_quest_SetQuestStep( pRoom, userObjID,717, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</GameServer>