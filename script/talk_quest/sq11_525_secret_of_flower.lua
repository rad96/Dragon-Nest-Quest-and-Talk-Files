<VillageServer>

function sq11_525_secret_of_flower_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 45 then
		sq11_525_secret_of_flower_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_525_secret_of_flower_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5251, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5252, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5253, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5254, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5255, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5256, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5257, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5258, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5259, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-accepted" then
				api_quest_AddQuest(userObjID,525, 1);
				api_quest_SetJournalStep(userObjID,525, 1);
				api_quest_SetQuestStep(userObjID,525, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 870, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200870, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400103, 1);

	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5251, true);
				 api_quest_RewardQuestUser(userObjID, 5251, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5252, true);
				 api_quest_RewardQuestUser(userObjID, 5252, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5253, true);
				 api_quest_RewardQuestUser(userObjID, 5253, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5254, true);
				 api_quest_RewardQuestUser(userObjID, 5254, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5255, true);
				 api_quest_RewardQuestUser(userObjID, 5255, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5256, true);
				 api_quest_RewardQuestUser(userObjID, 5256, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5257, true);
				 api_quest_RewardQuestUser(userObjID, 5257, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5258, true);
				 api_quest_RewardQuestUser(userObjID, 5258, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5259, true);
				 api_quest_RewardQuestUser(userObjID, 5259, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-g" then 

				if api_quest_HasQuestItem(userObjID, 400103, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400103, api_quest_HasQuestItem(userObjID, 400103, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_525_secret_of_flower_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 525);
	if qstep == 1 and CountIndex == 870 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400103, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400103, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400103 then

	end
	if qstep == 1 and CountIndex == 200870 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400103, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400103, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_525_secret_of_flower_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 525);
	if qstep == 1 and CountIndex == 870 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400103 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200870 and Count >= TargetCount  then

	end
end

function sq11_525_secret_of_flower_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 525);
	local questID=525;
end

function sq11_525_secret_of_flower_OnRemoteStart( userObjID, questID )
end

function sq11_525_secret_of_flower_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_525_secret_of_flower_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_525_secret_of_flower_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 45 then
		sq11_525_secret_of_flower_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_525_secret_of_flower_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5251, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5252, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5253, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5254, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5255, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5256, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5257, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5258, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5259, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-accepted" then
				api_quest_AddQuest( pRoom, userObjID,525, 1);
				api_quest_SetJournalStep( pRoom, userObjID,525, 1);
				api_quest_SetQuestStep( pRoom, userObjID,525, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 870, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200870, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400103, 1);

	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5251, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5251, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5252, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5252, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5253, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5253, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5254, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5254, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5255, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5255, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5256, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5256, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5257, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5257, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5258, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5258, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5259, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5259, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-g" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400103, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400103, api_quest_HasQuestItem( pRoom, userObjID, 400103, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_525_secret_of_flower_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 525);
	if qstep == 1 and CountIndex == 870 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400103, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400103, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400103 then

	end
	if qstep == 1 and CountIndex == 200870 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400103, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400103, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_525_secret_of_flower_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 525);
	if qstep == 1 and CountIndex == 870 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400103 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200870 and Count >= TargetCount  then

	end
end

function sq11_525_secret_of_flower_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 525);
	local questID=525;
end

function sq11_525_secret_of_flower_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_525_secret_of_flower_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_525_secret_of_flower_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>