<VillageServer>

function sq08_331_challenge_of_catchef1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 26 then
		sq08_331_challenge_of_catchef1_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_331_challenge_of_catchef1_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3311, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3312, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3313, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3314, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3315, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3316, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3317, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3318, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3319, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest(userObjID,331, 1);
				api_quest_SetJournalStep(userObjID,331, 1);
				api_quest_SetQuestStep(userObjID,331, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-1-b" then 
	end
	if npc_talk_index == "n026_trader_may-1-c" then 
	end
	if npc_talk_index == "n026_trader_may-1-d" then 
	end
	if npc_talk_index == "n026_trader_may-1-e" then 
	end
	if npc_talk_index == "n026_trader_may-1-f" then 
	end
	if npc_talk_index == "n026_trader_may-1-g" then 
	end
	if npc_talk_index == "n026_trader_may-1-h" then 
	end
	if npc_talk_index == "n026_trader_may-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400134, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 652, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 659, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 660, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200652, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200659, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200660, 30000);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n026_trader_may-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3311, true);
				 api_quest_RewardQuestUser(userObjID, 3311, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3312, true);
				 api_quest_RewardQuestUser(userObjID, 3312, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3313, true);
				 api_quest_RewardQuestUser(userObjID, 3313, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3314, true);
				 api_quest_RewardQuestUser(userObjID, 3314, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3315, true);
				 api_quest_RewardQuestUser(userObjID, 3315, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3316, true);
				 api_quest_RewardQuestUser(userObjID, 3316, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3317, true);
				 api_quest_RewardQuestUser(userObjID, 3317, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3318, true);
				 api_quest_RewardQuestUser(userObjID, 3318, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3319, true);
				 api_quest_RewardQuestUser(userObjID, 3319, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 332, 1);
					api_quest_SetQuestStep(userObjID, 332, 1);
					api_quest_SetJournalStep(userObjID, 332, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n026_trader_may-3-c" then 
	end
	if npc_talk_index == "n026_trader_may-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n026_trader_may-1", "sq08_332_challenge_of_catchef2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_331_challenge_of_catchef1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 331);
	if qstep == 2 and CountIndex == 400134 then

	end
	if qstep == 2 and CountIndex == 652 then
				if api_quest_HasQuestItem(userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 659 then
				if api_quest_HasQuestItem(userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 660 then
				if api_quest_HasQuestItem(userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200652 then
				if api_quest_HasQuestItem(userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200659 then
				if api_quest_HasQuestItem(userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200660 then
				if api_quest_HasQuestItem(userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
end

function sq08_331_challenge_of_catchef1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 331);
	if qstep == 2 and CountIndex == 400134 and Count >= TargetCount  then
				if api_quest_HasQuestItem(userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 652 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 659 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 660 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200652 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200659 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200660 and Count >= TargetCount  then

	end
end

function sq08_331_challenge_of_catchef1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 331);
	local questID=331;
end

function sq08_331_challenge_of_catchef1_OnRemoteStart( userObjID, questID )
end

function sq08_331_challenge_of_catchef1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_331_challenge_of_catchef1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,331, 1);
				api_quest_SetJournalStep(userObjID,331, 1);
				api_quest_SetQuestStep(userObjID,331, 1);
				npc_talk_index = "n026_trader_may-1";
end

</VillageServer>

<GameServer>
function sq08_331_challenge_of_catchef1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 26 then
		sq08_331_challenge_of_catchef1_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_331_challenge_of_catchef1_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3311, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3312, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3313, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3314, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3315, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3316, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3317, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3318, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3319, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,331, 1);
				api_quest_SetJournalStep( pRoom, userObjID,331, 1);
				api_quest_SetQuestStep( pRoom, userObjID,331, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-1-b" then 
	end
	if npc_talk_index == "n026_trader_may-1-c" then 
	end
	if npc_talk_index == "n026_trader_may-1-d" then 
	end
	if npc_talk_index == "n026_trader_may-1-e" then 
	end
	if npc_talk_index == "n026_trader_may-1-f" then 
	end
	if npc_talk_index == "n026_trader_may-1-g" then 
	end
	if npc_talk_index == "n026_trader_may-1-h" then 
	end
	if npc_talk_index == "n026_trader_may-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400134, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 652, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 659, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 660, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200652, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200659, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200660, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n026_trader_may-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3311, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3311, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3312, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3312, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3313, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3313, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3314, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3314, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3315, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3315, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3316, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3316, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3317, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3317, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3318, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3318, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3319, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3319, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 332, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 332, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 332, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n026_trader_may-3-c" then 
	end
	if npc_talk_index == "n026_trader_may-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n026_trader_may-1", "sq08_332_challenge_of_catchef2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_331_challenge_of_catchef1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 331);
	if qstep == 2 and CountIndex == 400134 then

	end
	if qstep == 2 and CountIndex == 652 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 659 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 660 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200652 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200659 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 200660 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400134, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
end

function sq08_331_challenge_of_catchef1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 331);
	if qstep == 2 and CountIndex == 400134 and Count >= TargetCount  then
				if api_quest_HasQuestItem( pRoom, userObjID, 400134, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 652 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 659 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 660 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200652 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200659 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200660 and Count >= TargetCount  then

	end
end

function sq08_331_challenge_of_catchef1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 331);
	local questID=331;
end

function sq08_331_challenge_of_catchef1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_331_challenge_of_catchef1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_331_challenge_of_catchef1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,331, 1);
				api_quest_SetJournalStep( pRoom, userObjID,331, 1);
				api_quest_SetQuestStep( pRoom, userObjID,331, 1);
				npc_talk_index = "n026_trader_may-1";
end

</GameServer>