<VillageServer>

function mqc03_9589_secret_meeting_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1848 then
		mqc03_9589_secret_meeting_OnTalk_n1848_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1851 then
		mqc03_9589_secret_meeting_OnTalk_n1851_lencea_charlotte(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1859 then
		mqc03_9589_secret_meeting_OnTalk_n1859_juergen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		mqc03_9589_secret_meeting_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 99 then
		mqc03_9589_secret_meeting_OnTalk_n099_engineer_hubert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1848_shaolong--------------------------------------------------------------------------------
function mqc03_9589_secret_meeting_OnTalk_n1848_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1848_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1848_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1848_shaolong-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1848_shaolong-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1848_shaolong-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1848_shaolong-2-b" then 
	end
	if npc_talk_index == "n1848_shaolong-2-c" then 
	end
	if npc_talk_index == "n1848_shaolong-2-d" then 
	end
	if npc_talk_index == "n1848_shaolong-2-e" then 
	end
	if npc_talk_index == "n1848_shaolong-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1848_shaolong-5-b" then 
	end
	if npc_talk_index == "n1848_shaolong-5-c" then 
	end
	if npc_talk_index == "n1848_shaolong-5-d" then 
	end
	if npc_talk_index == "n1848_shaolong-5-e" then 
	end
	if npc_talk_index == "n1848_shaolong-5-f" then 
	end
	if npc_talk_index == "n1848_shaolong-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1851_lencea_charlotte--------------------------------------------------------------------------------
function mqc03_9589_secret_meeting_OnTalk_n1851_lencea_charlotte(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1859_juergen--------------------------------------------------------------------------------
function mqc03_9589_secret_meeting_OnTalk_n1859_juergen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1859_juergen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1859_juergen-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1859_juergen-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1859_juergen-4-b" then 
	end
	if npc_talk_index == "n1859_juergen-4-c" then 
	end
	if npc_talk_index == "n1859_juergen-4-d" then 
	end
	if npc_talk_index == "n1859_juergen-4-e" then 
	end
	if npc_talk_index == "n1859_juergen-4-f" then 
	end
	if npc_talk_index == "n1859_juergen-4-g" then 
	end
	if npc_talk_index == "n1859_juergen-4-h" then 
	end
	if npc_talk_index == "n1859_juergen-4-i" then 
	end
	if npc_talk_index == "n1859_juergen-4-j" then 
	end
	if npc_talk_index == "n1859_juergen-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function mqc03_9589_secret_meeting_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9589, 2);
				api_quest_SetJournalStep(userObjID,9589, 1);
				api_quest_SetQuestStep(userObjID,9589, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n099_engineer_hubert--------------------------------------------------------------------------------
function mqc03_9589_secret_meeting_OnTalk_n099_engineer_hubert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n099_engineer_hubert-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n099_engineer_hubert-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n099_engineer_hubert-6-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-6-c" then 
	end
	if npc_talk_index == "n099_engineer_hubert-6-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95890, true);
				 api_quest_RewardQuestUser(userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95890, true);
				 api_quest_RewardQuestUser(userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95890, true);
				 api_quest_RewardQuestUser(userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95890, true);
				 api_quest_RewardQuestUser(userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95890, true);
				 api_quest_RewardQuestUser(userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95890, true);
				 api_quest_RewardQuestUser(userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95890, true);
				 api_quest_RewardQuestUser(userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95890, true);
				 api_quest_RewardQuestUser(userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95890, true);
				 api_quest_RewardQuestUser(userObjID, 95890, questID, 1);
			 end 
	end
	if npc_talk_index == "n099_engineer_hubert-6-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9590, 2);
					api_quest_SetQuestStep(userObjID, 9590, 1);
					api_quest_SetJournalStep(userObjID, 9590, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n099_engineer_hubert-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n099_engineer_hubert-1", "mqc04_9590_disappointing_palace_entrance.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc03_9589_secret_meeting_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9589);
end

function mqc03_9589_secret_meeting_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9589);
end

function mqc03_9589_secret_meeting_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9589);
	local questID=9589;
end

function mqc03_9589_secret_meeting_OnRemoteStart( userObjID, questID )
end

function mqc03_9589_secret_meeting_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc03_9589_secret_meeting_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9589, 2);
				api_quest_SetJournalStep(userObjID,9589, 1);
				api_quest_SetQuestStep(userObjID,9589, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</VillageServer>

<GameServer>
function mqc03_9589_secret_meeting_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1848 then
		mqc03_9589_secret_meeting_OnTalk_n1848_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1851 then
		mqc03_9589_secret_meeting_OnTalk_n1851_lencea_charlotte( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1859 then
		mqc03_9589_secret_meeting_OnTalk_n1859_juergen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		mqc03_9589_secret_meeting_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 99 then
		mqc03_9589_secret_meeting_OnTalk_n099_engineer_hubert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1848_shaolong--------------------------------------------------------------------------------
function mqc03_9589_secret_meeting_OnTalk_n1848_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1848_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1848_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1848_shaolong-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1848_shaolong-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1848_shaolong-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1848_shaolong-2-b" then 
	end
	if npc_talk_index == "n1848_shaolong-2-c" then 
	end
	if npc_talk_index == "n1848_shaolong-2-d" then 
	end
	if npc_talk_index == "n1848_shaolong-2-e" then 
	end
	if npc_talk_index == "n1848_shaolong-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1848_shaolong-5-b" then 
	end
	if npc_talk_index == "n1848_shaolong-5-c" then 
	end
	if npc_talk_index == "n1848_shaolong-5-d" then 
	end
	if npc_talk_index == "n1848_shaolong-5-e" then 
	end
	if npc_talk_index == "n1848_shaolong-5-f" then 
	end
	if npc_talk_index == "n1848_shaolong-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1851_lencea_charlotte--------------------------------------------------------------------------------
function mqc03_9589_secret_meeting_OnTalk_n1851_lencea_charlotte( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1859_juergen--------------------------------------------------------------------------------
function mqc03_9589_secret_meeting_OnTalk_n1859_juergen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1859_juergen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1859_juergen-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1859_juergen-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1859_juergen-4-b" then 
	end
	if npc_talk_index == "n1859_juergen-4-c" then 
	end
	if npc_talk_index == "n1859_juergen-4-d" then 
	end
	if npc_talk_index == "n1859_juergen-4-e" then 
	end
	if npc_talk_index == "n1859_juergen-4-f" then 
	end
	if npc_talk_index == "n1859_juergen-4-g" then 
	end
	if npc_talk_index == "n1859_juergen-4-h" then 
	end
	if npc_talk_index == "n1859_juergen-4-i" then 
	end
	if npc_talk_index == "n1859_juergen-4-j" then 
	end
	if npc_talk_index == "n1859_juergen-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function mqc03_9589_secret_meeting_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9589, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9589, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9589, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n099_engineer_hubert--------------------------------------------------------------------------------
function mqc03_9589_secret_meeting_OnTalk_n099_engineer_hubert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n099_engineer_hubert-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n099_engineer_hubert-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n099_engineer_hubert-6-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-6-c" then 
	end
	if npc_talk_index == "n099_engineer_hubert-6-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95890, questID, 1);
			 end 
	end
	if npc_talk_index == "n099_engineer_hubert-6-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9590, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9590, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9590, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n099_engineer_hubert-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n099_engineer_hubert-1", "mqc04_9590_disappointing_palace_entrance.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc03_9589_secret_meeting_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9589);
end

function mqc03_9589_secret_meeting_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9589);
end

function mqc03_9589_secret_meeting_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9589);
	local questID=9589;
end

function mqc03_9589_secret_meeting_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc03_9589_secret_meeting_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc03_9589_secret_meeting_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9589, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9589, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9589, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</GameServer>