<VillageServer>

function sq15_4291_the_duck_in_timewarp_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1039 then
		sq15_4291_the_duck_in_timewarp_OnTalk_n1039_mechaduck(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1041 then
		sq15_4291_the_duck_in_timewarp_OnTalk_n1041_academic_xd_29(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1049 then
		sq15_4291_the_duck_in_timewarp_OnTalk_n1049_jasmin_bo_ok(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq15_4291_the_duck_in_timewarp_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1039_mechaduck--------------------------------------------------------------------------------
function sq15_4291_the_duck_in_timewarp_OnTalk_n1039_mechaduck(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1039_mechaduck-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1039_mechaduck-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1039_mechaduck-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1039_mechaduck-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1039_mechaduck-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1039_mechaduck-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n1039_mechaduck-3-b" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-c" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-c" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-d" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-e" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-e" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-f" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-f" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-g" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-h" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-class_check1" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1039_mechaduck-3-j";

				else
									npc_talk_index = "n1039_mechaduck-3-i";

				end
	end
	if npc_talk_index == "n1039_mechaduck-3-k" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-k" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-k" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-l" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-m" then 
	end
	if npc_talk_index == "n1039_mechaduck-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1041_academic_xd_29--------------------------------------------------------------------------------
function sq15_4291_the_duck_in_timewarp_OnTalk_n1041_academic_xd_29(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1041_academic_xd_29-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1041_academic_xd_29-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1041_academic_xd_29-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1041_academic_xd_29-5-b" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-c" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-d" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-e" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-f" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-g" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-g" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-h" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1049_jasmin_bo_ok--------------------------------------------------------------------------------
function sq15_4291_the_duck_in_timewarp_OnTalk_n1049_jasmin_bo_ok(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1049_jasmin_bo_ok-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1049_jasmin_bo_ok-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1049_jasmin_bo_ok-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1049_jasmin_bo_ok-7-class_check" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1049_jasmin_bo_ok-7-f";

				else
									npc_talk_index = "n1049_jasmin_bo_ok-7-a";

				end
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-b" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-c" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-d" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-e" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-g" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-h" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-i" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-j" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-k" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-l" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-m" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq15_4291_the_duck_in_timewarp_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n098_warrior_master_lodrigo-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 42910, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 42910, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 42910, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 42910, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 42910, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 42910, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 42910, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 42910, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 42910, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4291, 1);
				api_quest_SetJournalStep(userObjID,4291, 1);
				api_quest_SetQuestStep(userObjID,4291, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-8-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 42910, true);
				 api_quest_RewardQuestUser(userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 42910, true);
				 api_quest_RewardQuestUser(userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 42910, true);
				 api_quest_RewardQuestUser(userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 42910, true);
				 api_quest_RewardQuestUser(userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 42910, true);
				 api_quest_RewardQuestUser(userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 42910, true);
				 api_quest_RewardQuestUser(userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 42910, true);
				 api_quest_RewardQuestUser(userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 42910, true);
				 api_quest_RewardQuestUser(userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 42910, true);
				 api_quest_RewardQuestUser(userObjID, 42910, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-8-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4291_the_duck_in_timewarp_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4291);
end

function sq15_4291_the_duck_in_timewarp_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4291);
end

function sq15_4291_the_duck_in_timewarp_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4291);
	local questID=4291;
end

function sq15_4291_the_duck_in_timewarp_OnRemoteStart( userObjID, questID )
end

function sq15_4291_the_duck_in_timewarp_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4291_the_duck_in_timewarp_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4291, 1);
				api_quest_SetJournalStep(userObjID,4291, 1);
				api_quest_SetQuestStep(userObjID,4291, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</VillageServer>

<GameServer>
function sq15_4291_the_duck_in_timewarp_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1039 then
		sq15_4291_the_duck_in_timewarp_OnTalk_n1039_mechaduck( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1041 then
		sq15_4291_the_duck_in_timewarp_OnTalk_n1041_academic_xd_29( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1049 then
		sq15_4291_the_duck_in_timewarp_OnTalk_n1049_jasmin_bo_ok( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq15_4291_the_duck_in_timewarp_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1039_mechaduck--------------------------------------------------------------------------------
function sq15_4291_the_duck_in_timewarp_OnTalk_n1039_mechaduck( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1039_mechaduck-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1039_mechaduck-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1039_mechaduck-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1039_mechaduck-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1039_mechaduck-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1039_mechaduck-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n1039_mechaduck-3-b" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-c" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-c" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-d" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-e" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-e" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-f" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-f" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-g" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-h" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-class_check1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1039_mechaduck-3-j";

				else
									npc_talk_index = "n1039_mechaduck-3-i";

				end
	end
	if npc_talk_index == "n1039_mechaduck-3-k" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-k" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-k" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-l" then 
	end
	if npc_talk_index == "n1039_mechaduck-3-m" then 
	end
	if npc_talk_index == "n1039_mechaduck-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1041_academic_xd_29--------------------------------------------------------------------------------
function sq15_4291_the_duck_in_timewarp_OnTalk_n1041_academic_xd_29( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1041_academic_xd_29-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1041_academic_xd_29-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1041_academic_xd_29-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1041_academic_xd_29-5-b" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-c" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-d" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-e" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-f" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-g" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-g" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-5-h" then 
	end
	if npc_talk_index == "n1041_academic_xd_29-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1049_jasmin_bo_ok--------------------------------------------------------------------------------
function sq15_4291_the_duck_in_timewarp_OnTalk_n1049_jasmin_bo_ok( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1049_jasmin_bo_ok-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1049_jasmin_bo_ok-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1049_jasmin_bo_ok-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1049_jasmin_bo_ok-7-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1049_jasmin_bo_ok-7-f";

				else
									npc_talk_index = "n1049_jasmin_bo_ok-7-a";

				end
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-b" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-c" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-d" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-e" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-g" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-h" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-i" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-j" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-k" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-l" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-7-m" then 
	end
	if npc_talk_index == "n1049_jasmin_bo_ok-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq15_4291_the_duck_in_timewarp_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n098_warrior_master_lodrigo-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4291, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4291, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4291, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-8-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42910, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42910, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42910, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-8-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4291_the_duck_in_timewarp_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4291);
end

function sq15_4291_the_duck_in_timewarp_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4291);
end

function sq15_4291_the_duck_in_timewarp_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4291);
	local questID=4291;
end

function sq15_4291_the_duck_in_timewarp_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4291_the_duck_in_timewarp_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4291_the_duck_in_timewarp_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4291, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4291, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4291, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</GameServer>