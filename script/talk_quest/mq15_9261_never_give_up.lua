<VillageServer>

function mq15_9261_never_give_up_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 393 then
		mq15_9261_never_give_up_OnTalk_n393_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		mq15_9261_never_give_up_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 836 then
		mq15_9261_never_give_up_OnTalk_n836_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n393_academic_station--------------------------------------------------------------------------------
function mq15_9261_never_give_up_OnTalk_n393_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n393_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n393_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n393_academic_station-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n393_academic_station-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n393_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9261, 2);
				api_quest_SetJournalStep(userObjID,9261, 1);
				api_quest_SetQuestStep(userObjID,9261, 1);
				npc_talk_index = "n393_academic_station-1";

	end
	if npc_talk_index == "n393_academic_station-1-b" then 
	end
	if npc_talk_index == "n393_academic_station-1-c" then 
	end
	if npc_talk_index == "n393_academic_station-3-b" then 
	end
	if npc_talk_index == "n393_academic_station-3-c" then 
	end
	if npc_talk_index == "n393_academic_station-3-d" then 
	end
	if npc_talk_index == "n393_academic_station-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n393_academic_station-4-b" then 
	end
	if npc_talk_index == "n393_academic_station-4-c" then 
	end
	if npc_talk_index == "n393_academic_station-4-d" then 
	end
	if npc_talk_index == "n393_academic_station-4-e" then 
	end
	if npc_talk_index == "n393_academic_station-4-f" then 
	end
	if npc_talk_index == "n393_academic_station-4-g" then 
	end
	if npc_talk_index == "n393_academic_station-4-h" then 
	end
	if npc_talk_index == "n393_academic_station-4-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92610, true);
				 api_quest_RewardQuestUser(userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92610, true);
				 api_quest_RewardQuestUser(userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92610, true);
				 api_quest_RewardQuestUser(userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92610, true);
				 api_quest_RewardQuestUser(userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92610, true);
				 api_quest_RewardQuestUser(userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92610, true);
				 api_quest_RewardQuestUser(userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92610, true);
				 api_quest_RewardQuestUser(userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92610, true);
				 api_quest_RewardQuestUser(userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92610, true);
				 api_quest_RewardQuestUser(userObjID, 92610, questID, 1);
			 end 
	end
	if npc_talk_index == "n393_academic_station-4-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9262, 2);
					api_quest_SetQuestStep(userObjID, 9262, 1);
					api_quest_SetJournalStep(userObjID, 9262, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n393_academic_station-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n393_academic_station-1", "mq15_9262_moment_of_turning_point.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function mq15_9261_never_give_up_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-j" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n836_velskud--------------------------------------------------------------------------------
function mq15_9261_never_give_up_OnTalk_n836_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9261_never_give_up_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9261);
end

function mq15_9261_never_give_up_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9261);
end

function mq15_9261_never_give_up_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9261);
	local questID=9261;
end

function mq15_9261_never_give_up_OnRemoteStart( userObjID, questID )
end

function mq15_9261_never_give_up_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9261_never_give_up_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9261, 2);
				api_quest_SetJournalStep(userObjID,9261, 1);
				api_quest_SetQuestStep(userObjID,9261, 1);
				npc_talk_index = "n393_academic_station-1";
end

</VillageServer>

<GameServer>
function mq15_9261_never_give_up_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 393 then
		mq15_9261_never_give_up_OnTalk_n393_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		mq15_9261_never_give_up_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 836 then
		mq15_9261_never_give_up_OnTalk_n836_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n393_academic_station--------------------------------------------------------------------------------
function mq15_9261_never_give_up_OnTalk_n393_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n393_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n393_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n393_academic_station-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n393_academic_station-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n393_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9261, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9261, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9261, 1);
				npc_talk_index = "n393_academic_station-1";

	end
	if npc_talk_index == "n393_academic_station-1-b" then 
	end
	if npc_talk_index == "n393_academic_station-1-c" then 
	end
	if npc_talk_index == "n393_academic_station-3-b" then 
	end
	if npc_talk_index == "n393_academic_station-3-c" then 
	end
	if npc_talk_index == "n393_academic_station-3-d" then 
	end
	if npc_talk_index == "n393_academic_station-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n393_academic_station-4-b" then 
	end
	if npc_talk_index == "n393_academic_station-4-c" then 
	end
	if npc_talk_index == "n393_academic_station-4-d" then 
	end
	if npc_talk_index == "n393_academic_station-4-e" then 
	end
	if npc_talk_index == "n393_academic_station-4-f" then 
	end
	if npc_talk_index == "n393_academic_station-4-g" then 
	end
	if npc_talk_index == "n393_academic_station-4-h" then 
	end
	if npc_talk_index == "n393_academic_station-4-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92610, questID, 1);
			 end 
	end
	if npc_talk_index == "n393_academic_station-4-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9262, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9262, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9262, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n393_academic_station-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n393_academic_station-1", "mq15_9262_moment_of_turning_point.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function mq15_9261_never_give_up_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-j" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n836_velskud--------------------------------------------------------------------------------
function mq15_9261_never_give_up_OnTalk_n836_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9261_never_give_up_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9261);
end

function mq15_9261_never_give_up_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9261);
end

function mq15_9261_never_give_up_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9261);
	local questID=9261;
end

function mq15_9261_never_give_up_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9261_never_give_up_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9261_never_give_up_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9261, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9261, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9261, 1);
				npc_talk_index = "n393_academic_station-1";
end

</GameServer>