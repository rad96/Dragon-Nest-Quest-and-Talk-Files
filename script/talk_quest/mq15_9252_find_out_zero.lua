<VillageServer>

function mq15_9252_find_out_zero_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1029 then
		mq15_9252_find_out_zero_OnTalk_n1029_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 393 then
		mq15_9252_find_out_zero_OnTalk_n393_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1029_academic--------------------------------------------------------------------------------
function mq15_9252_find_out_zero_OnTalk_n1029_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1029_academic-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1029_academic-1-b" then 
	end
	if npc_talk_index == "n1029_academic-1-c" then 
	end
	if npc_talk_index == "n1029_academic-1-d" then 
	end
	if npc_talk_index == "n1029_academic-1-e" then 
	end
	if npc_talk_index == "n1029_academic-1-f" then 
	end
	if npc_talk_index == "n1029_academic-1-g" then 
	end
	if npc_talk_index == "n1029_academic-1-h" then 
	end
	if npc_talk_index == "n1029_academic-1-i" then 
	end
	if npc_talk_index == "n1029_academic-1-j" then 
	end
	if npc_talk_index == "n1029_academic-1-k" then 
	end
	if npc_talk_index == "n1029_academic-1-l" then 
	end
	if npc_talk_index == "n1029_academic-1-m" then 
	end
	if npc_talk_index == "n1029_academic-1-n" then 
	end
	if npc_talk_index == "n1029_academic-1-o" then 
	end
	if npc_talk_index == "n1029_academic-1-p" then 
	end
	if npc_talk_index == "n1029_academic-1-q" then 
	end
	if npc_talk_index == "n1029_academic-1-r" then 
	end
	if npc_talk_index == "n1029_academic-1-s" then 
	end
	if npc_talk_index == "n1029_academic-1-t" then 
	end
	if npc_talk_index == "n1029_academic-1-u" then 
	end
	if npc_talk_index == "n1029_academic-1-v" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92520, true);
				 api_quest_RewardQuestUser(userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92520, true);
				 api_quest_RewardQuestUser(userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92520, true);
				 api_quest_RewardQuestUser(userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92520, true);
				 api_quest_RewardQuestUser(userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92520, true);
				 api_quest_RewardQuestUser(userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92520, true);
				 api_quest_RewardQuestUser(userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92520, true);
				 api_quest_RewardQuestUser(userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92520, true);
				 api_quest_RewardQuestUser(userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92520, true);
				 api_quest_RewardQuestUser(userObjID, 92520, questID, 1);
			 end 
	end
	if npc_talk_index == "n1029_academic-1-w" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9253, 2);
					api_quest_SetQuestStep(userObjID, 9253, 1);
					api_quest_SetJournalStep(userObjID, 9253, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n393_academic_station--------------------------------------------------------------------------------
function mq15_9252_find_out_zero_OnTalk_n393_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n393_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n393_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n393_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9252, 2);
				api_quest_SetJournalStep(userObjID,9252, 1);
				api_quest_SetQuestStep(userObjID,9252, 1);
				npc_talk_index = "n393_academic_station-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9252_find_out_zero_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9252);
end

function mq15_9252_find_out_zero_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9252);
end

function mq15_9252_find_out_zero_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9252);
	local questID=9252;
end

function mq15_9252_find_out_zero_OnRemoteStart( userObjID, questID )
end

function mq15_9252_find_out_zero_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9252_find_out_zero_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9252, 2);
				api_quest_SetJournalStep(userObjID,9252, 1);
				api_quest_SetQuestStep(userObjID,9252, 1);
				npc_talk_index = "n393_academic_station-1";
end

</VillageServer>

<GameServer>
function mq15_9252_find_out_zero_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1029 then
		mq15_9252_find_out_zero_OnTalk_n1029_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 393 then
		mq15_9252_find_out_zero_OnTalk_n393_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1029_academic--------------------------------------------------------------------------------
function mq15_9252_find_out_zero_OnTalk_n1029_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1029_academic-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1029_academic-1-b" then 
	end
	if npc_talk_index == "n1029_academic-1-c" then 
	end
	if npc_talk_index == "n1029_academic-1-d" then 
	end
	if npc_talk_index == "n1029_academic-1-e" then 
	end
	if npc_talk_index == "n1029_academic-1-f" then 
	end
	if npc_talk_index == "n1029_academic-1-g" then 
	end
	if npc_talk_index == "n1029_academic-1-h" then 
	end
	if npc_talk_index == "n1029_academic-1-i" then 
	end
	if npc_talk_index == "n1029_academic-1-j" then 
	end
	if npc_talk_index == "n1029_academic-1-k" then 
	end
	if npc_talk_index == "n1029_academic-1-l" then 
	end
	if npc_talk_index == "n1029_academic-1-m" then 
	end
	if npc_talk_index == "n1029_academic-1-n" then 
	end
	if npc_talk_index == "n1029_academic-1-o" then 
	end
	if npc_talk_index == "n1029_academic-1-p" then 
	end
	if npc_talk_index == "n1029_academic-1-q" then 
	end
	if npc_talk_index == "n1029_academic-1-r" then 
	end
	if npc_talk_index == "n1029_academic-1-s" then 
	end
	if npc_talk_index == "n1029_academic-1-t" then 
	end
	if npc_talk_index == "n1029_academic-1-u" then 
	end
	if npc_talk_index == "n1029_academic-1-v" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92520, questID, 1);
			 end 
	end
	if npc_talk_index == "n1029_academic-1-w" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9253, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9253, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9253, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n393_academic_station--------------------------------------------------------------------------------
function mq15_9252_find_out_zero_OnTalk_n393_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n393_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n393_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n393_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9252, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9252, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9252, 1);
				npc_talk_index = "n393_academic_station-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9252_find_out_zero_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9252);
end

function mq15_9252_find_out_zero_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9252);
end

function mq15_9252_find_out_zero_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9252);
	local questID=9252;
end

function mq15_9252_find_out_zero_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9252_find_out_zero_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9252_find_out_zero_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9252, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9252, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9252, 1);
				npc_talk_index = "n393_academic_station-1";
end

</GameServer>