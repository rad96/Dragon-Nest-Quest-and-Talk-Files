<VillageServer>

function sqc16_4651_fragrant_moon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1138 then
		sqc16_4651_fragrant_moon_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function sqc16_4651_fragrant_moon_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1138_elf_guard_sitredel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46510, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46510, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46510, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46510, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46510, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46510, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46510, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46510, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46510, false);
			 end 

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4651, 1);
				api_quest_SetJournalStep(userObjID,4651, 1);
				api_quest_SetQuestStep(userObjID,4651, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400473, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 603172, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 703172, 30000);

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-f" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-h" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-i" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-j" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-k" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-k" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-l" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-m" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 end 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-m" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 end 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-m" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46510, true);
				 api_quest_RewardQuestUser(userObjID, 46510, questID, 1);
			 end 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-n" then 

				if api_quest_HasQuestItem(userObjID, 400473, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400473, api_quest_HasQuestItem(userObjID, 400473, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4651_fragrant_moon_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4651);
	if qstep == 1 and CountIndex == 603172 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400473, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400473, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 703172 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400473, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400473, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400473 then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function sqc16_4651_fragrant_moon_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4651);
	if qstep == 1 and CountIndex == 603172 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 703172 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400473 and Count >= TargetCount  then

	end
end

function sqc16_4651_fragrant_moon_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4651);
	local questID=4651;
end

function sqc16_4651_fragrant_moon_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400473, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 603172, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 703172, 30000);
end

function sqc16_4651_fragrant_moon_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc16_4651_fragrant_moon_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4651, 1);
				api_quest_SetJournalStep(userObjID,4651, 1);
				api_quest_SetQuestStep(userObjID,4651, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400473, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 603172, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 703172, 30000);
end

</VillageServer>

<GameServer>
function sqc16_4651_fragrant_moon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1138 then
		sqc16_4651_fragrant_moon_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function sqc16_4651_fragrant_moon_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1138_elf_guard_sitredel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, false);
			 end 

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4651, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4651, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4651, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400473, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 603172, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 703172, 30000);

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-f" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-h" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-i" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-j" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-k" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-k" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-l" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-m" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 end 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-m" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 end 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-m" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46510, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46510, questID, 1);
			 end 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-n" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400473, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400473, api_quest_HasQuestItem( pRoom, userObjID, 400473, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4651_fragrant_moon_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4651);
	if qstep == 1 and CountIndex == 603172 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400473, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400473, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 703172 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400473, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400473, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400473 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function sqc16_4651_fragrant_moon_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4651);
	if qstep == 1 and CountIndex == 603172 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 703172 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400473 and Count >= TargetCount  then

	end
end

function sqc16_4651_fragrant_moon_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4651);
	local questID=4651;
end

function sqc16_4651_fragrant_moon_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400473, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 603172, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 703172, 30000);
end

function sqc16_4651_fragrant_moon_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc16_4651_fragrant_moon_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4651, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4651, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4651, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400473, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 603172, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 703172, 30000);
end

</GameServer>