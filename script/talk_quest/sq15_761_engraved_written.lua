<VillageServer>

function sq15_761_engraved_written_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 703 then
		sq15_761_engraved_written_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n703_book_doctor--------------------------------------------------------------------------------
function sq15_761_engraved_written_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n703_book_doctor-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n703_book_doctor-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7610, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7610, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7610, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7610, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7610, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7610, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7610, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7610, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7610, false);
			 end 

	end
	if npc_talk_index == "n703_book_doctor-accepting-acceptted" then
				api_quest_AddQuest(userObjID,761, 1);
				api_quest_SetJournalStep(userObjID,761, 1);
				api_quest_SetQuestStep(userObjID,761, 1);
				npc_talk_index = "n703_book_doctor-1";

	end
	if npc_talk_index == "n703_book_doctor-1-b" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-1-d" then 
	end
	if npc_talk_index == "n703_book_doctor-1-e" then 
	end
	if npc_talk_index == "n703_book_doctor-1-e" then 
	end
	if npc_talk_index == "n703_book_doctor-1-f" then 
	end
	if npc_talk_index == "n703_book_doctor-1-g" then 
	end
	if npc_talk_index == "n703_book_doctor-1-h" then 
	end
	if npc_talk_index == "n703_book_doctor-1-i" then 
	end
	if npc_talk_index == "n703_book_doctor-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1417, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201417, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300422, 1);
	end
	if npc_talk_index == "n703_book_doctor-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7610, true);
				 api_quest_RewardQuestUser(userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7610, true);
				 api_quest_RewardQuestUser(userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7610, true);
				 api_quest_RewardQuestUser(userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7610, true);
				 api_quest_RewardQuestUser(userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7610, true);
				 api_quest_RewardQuestUser(userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7610, true);
				 api_quest_RewardQuestUser(userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7610, true);
				 api_quest_RewardQuestUser(userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7610, true);
				 api_quest_RewardQuestUser(userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7610, true);
				 api_quest_RewardQuestUser(userObjID, 7610, questID, 1);
			 end 
	end
	if npc_talk_index == "n703_book_doctor-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 762, 1);
					api_quest_SetQuestStep(userObjID, 762, 1);
					api_quest_SetJournalStep(userObjID, 762, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300421, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300421, api_quest_HasQuestItem(userObjID, 300421, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300422, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300422, api_quest_HasQuestItem(userObjID, 300422, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300423, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300423, api_quest_HasQuestItem(userObjID, 300423, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300424, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300424, api_quest_HasQuestItem(userObjID, 300424, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300425, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300425, api_quest_HasQuestItem(userObjID, 300425, 1));
				end
	end
	if npc_talk_index == "n703_book_doctor-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n703_book_doctor-1", "sq15_762_knowledge_of_sorceress.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_761_engraved_written_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 761);
	if qstep == 2 and CountIndex == 1417 then
				api_quest_ClearCountingInfo(userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300422, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 201417 then
				api_quest_ClearCountingInfo(userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300422, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300422 then

	end
end

function sq15_761_engraved_written_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 761);
	if qstep == 2 and CountIndex == 1417 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201417 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300422 and Count >= TargetCount  then

	end
end

function sq15_761_engraved_written_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 761);
	local questID=761;
end

function sq15_761_engraved_written_OnRemoteStart( userObjID, questID )
end

function sq15_761_engraved_written_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_761_engraved_written_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,761, 1);
				api_quest_SetJournalStep(userObjID,761, 1);
				api_quest_SetQuestStep(userObjID,761, 1);
				npc_talk_index = "n703_book_doctor-1";
end

</VillageServer>

<GameServer>
function sq15_761_engraved_written_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 703 then
		sq15_761_engraved_written_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n703_book_doctor--------------------------------------------------------------------------------
function sq15_761_engraved_written_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n703_book_doctor-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n703_book_doctor-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, false);
			 end 

	end
	if npc_talk_index == "n703_book_doctor-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,761, 1);
				api_quest_SetJournalStep( pRoom, userObjID,761, 1);
				api_quest_SetQuestStep( pRoom, userObjID,761, 1);
				npc_talk_index = "n703_book_doctor-1";

	end
	if npc_talk_index == "n703_book_doctor-1-b" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-1-d" then 
	end
	if npc_talk_index == "n703_book_doctor-1-e" then 
	end
	if npc_talk_index == "n703_book_doctor-1-e" then 
	end
	if npc_talk_index == "n703_book_doctor-1-f" then 
	end
	if npc_talk_index == "n703_book_doctor-1-g" then 
	end
	if npc_talk_index == "n703_book_doctor-1-h" then 
	end
	if npc_talk_index == "n703_book_doctor-1-i" then 
	end
	if npc_talk_index == "n703_book_doctor-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1417, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201417, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300422, 1);
	end
	if npc_talk_index == "n703_book_doctor-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7610, questID, 1);
			 end 
	end
	if npc_talk_index == "n703_book_doctor-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 762, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 762, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 762, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300421, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300421, api_quest_HasQuestItem( pRoom, userObjID, 300421, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300422, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300422, api_quest_HasQuestItem( pRoom, userObjID, 300422, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300423, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300423, api_quest_HasQuestItem( pRoom, userObjID, 300423, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300424, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300424, api_quest_HasQuestItem( pRoom, userObjID, 300424, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300425, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300425, api_quest_HasQuestItem( pRoom, userObjID, 300425, 1));
				end
	end
	if npc_talk_index == "n703_book_doctor-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n703_book_doctor-1", "sq15_762_knowledge_of_sorceress.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_761_engraved_written_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 761);
	if qstep == 2 and CountIndex == 1417 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300422, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 201417 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300422, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300422, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300422 then

	end
end

function sq15_761_engraved_written_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 761);
	if qstep == 2 and CountIndex == 1417 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201417 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300422 and Count >= TargetCount  then

	end
end

function sq15_761_engraved_written_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 761);
	local questID=761;
end

function sq15_761_engraved_written_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_761_engraved_written_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_761_engraved_written_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,761, 1);
				api_quest_SetJournalStep( pRoom, userObjID,761, 1);
				api_quest_SetQuestStep( pRoom, userObjID,761, 1);
				npc_talk_index = "n703_book_doctor-1";
end

</GameServer>