<VillageServer>

function mqc05_9606_people_left_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 216 then
		mqc05_9606_people_left_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 261 then
		mqc05_9606_people_left_OnTalk_n261_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 287 then
		mqc05_9606_people_left_OnTalk_n287_blackdragons_wing(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mqc05_9606_people_left_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n216_geraint_wounded--------------------------------------------------------------------------------
function mqc05_9606_people_left_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n216_geraint_wounded-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n216_geraint_wounded-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n216_geraint_wounded-6-b" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-c" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-d" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-e" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-f" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-g" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-h" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-i" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-j" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-k" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-l" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-m" then 
	end
	if npc_talk_index == "n216_geraint_wounded-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);

				if api_quest_HasQuestItem(userObjID, 300249, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300249, api_quest_HasQuestItem(userObjID, 300249, 1));
				end
	end
	if npc_talk_index == "n216_geraint_wounded-7-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 96060, true);
				 api_quest_RewardQuestUser(userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 96060, true);
				 api_quest_RewardQuestUser(userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 96060, true);
				 api_quest_RewardQuestUser(userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 96060, true);
				 api_quest_RewardQuestUser(userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 96060, true);
				 api_quest_RewardQuestUser(userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4176, true);
				 api_quest_RewardQuestUser(userObjID, 4176, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 96060, true);
				 api_quest_RewardQuestUser(userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 96060, true);
				 api_quest_RewardQuestUser(userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 96060, true);
				 api_quest_RewardQuestUser(userObjID, 96060, questID, 1);
			 end 
	end
	if npc_talk_index == "n216_geraint_wounded-7-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9607, 2);
					api_quest_SetQuestStep(userObjID, 9607, 1);
					api_quest_SetJournalStep(userObjID, 9607, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n261_slave_david--------------------------------------------------------------------------------
function mqc05_9606_people_left_OnTalk_n261_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n261_slave_david-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n261_slave_david-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n261_slave_david-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n261_slave_david-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n261_slave_david-3-b" then 
	end
	if npc_talk_index == "n261_slave_david-3-c" then 
	end
	if npc_talk_index == "n261_slave_david-3-d" then 
	end
	if npc_talk_index == "n261_slave_david-3-e" then 
	end
	if npc_talk_index == "n261_slave_david-3-f" then 
	end
	if npc_talk_index == "n261_slave_david-3-g" then 
	end
	if npc_talk_index == "n261_slave_david-3-h" then 
	end
	if npc_talk_index == "n261_slave_david-3-i" then 
	end
	if npc_talk_index == "n261_slave_david-3-j" then 
	end
	if npc_talk_index == "n261_slave_david-3-k" then 
	end
	if npc_talk_index == "n261_slave_david-3-l" then 
	end
	if npc_talk_index == "n261_slave_david-3-l" then 
	end
	if npc_talk_index == "n261_slave_david-3-m" then 
	end
	if npc_talk_index == "n261_slave_david-3-n" then 
	end
	if npc_talk_index == "n261_slave_david-3-o" then 
	end
	if npc_talk_index == "n261_slave_david-3-p" then 
	end
	if npc_talk_index == "n261_slave_david-3-q" then 
	end
	if npc_talk_index == "n261_slave_david-3-q" then 
	end
	if npc_talk_index == "n261_slave_david-3-r" then 
	end
	if npc_talk_index == "n261_slave_david-3-s" then 
	end
	if npc_talk_index == "n261_slave_david-3-t" then 
	end
	if npc_talk_index == "n261_slave_david-3-u" then 
	end
	if npc_talk_index == "n261_slave_david-3-v" then 
	end
	if npc_talk_index == "n261_slave_david-3-v" then 
	end
	if npc_talk_index == "n261_slave_david-3-v" then 
	end
	if npc_talk_index == "n261_slave_david-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300249, 1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n287_blackdragons_wing--------------------------------------------------------------------------------
function mqc05_9606_people_left_OnTalk_n287_blackdragons_wing(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n287_blackdragons_wing-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n287_blackdragons_wing-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n287_blackdragons_wing-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n287_blackdragons_wing-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n287_blackdragons_wing-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300249, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300249, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9606_people_left_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n039_bishop_ignasio-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n039_bishop_ignasio-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9606, 2);
				api_quest_SetJournalStep(userObjID,9606, 1);
				api_quest_SetQuestStep(userObjID,9606, 1);

				if api_quest_HasQuestItem(userObjID, 300025, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300025, api_quest_HasQuestItem(userObjID, 300025, 1));
				end
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-1-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-d" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n039_bishop_ignasio-5-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9606_people_left_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9606);
	if qstep == 4 and CountIndex == 300249 then

	end
end

function mqc05_9606_people_left_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9606);
	if qstep == 4 and CountIndex == 300249 and Count >= TargetCount  then

	end
end

function mqc05_9606_people_left_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9606);
	local questID=9606;
end

function mqc05_9606_people_left_OnRemoteStart( userObjID, questID )
end

function mqc05_9606_people_left_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc05_9606_people_left_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9606, 2);
				api_quest_SetJournalStep(userObjID,9606, 1);
				api_quest_SetQuestStep(userObjID,9606, 1);

				if api_quest_HasQuestItem(userObjID, 300025, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300025, api_quest_HasQuestItem(userObjID, 300025, 1));
				end
				npc_talk_index = "n039_bishop_ignasio-1";
end

</VillageServer>

<GameServer>
function mqc05_9606_people_left_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 216 then
		mqc05_9606_people_left_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 261 then
		mqc05_9606_people_left_OnTalk_n261_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 287 then
		mqc05_9606_people_left_OnTalk_n287_blackdragons_wing( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mqc05_9606_people_left_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n216_geraint_wounded--------------------------------------------------------------------------------
function mqc05_9606_people_left_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n216_geraint_wounded-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n216_geraint_wounded-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n216_geraint_wounded-6-b" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-c" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-d" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-e" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-f" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-g" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-h" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-i" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-j" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-k" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-l" then 
	end
	if npc_talk_index == "n216_geraint_wounded-6-m" then 
	end
	if npc_talk_index == "n216_geraint_wounded-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);

				if api_quest_HasQuestItem( pRoom, userObjID, 300249, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300249, api_quest_HasQuestItem( pRoom, userObjID, 300249, 1));
				end
	end
	if npc_talk_index == "n216_geraint_wounded-7-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4176, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4176, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96060, questID, 1);
			 end 
	end
	if npc_talk_index == "n216_geraint_wounded-7-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9607, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9607, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9607, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n261_slave_david--------------------------------------------------------------------------------
function mqc05_9606_people_left_OnTalk_n261_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n261_slave_david-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n261_slave_david-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n261_slave_david-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n261_slave_david-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n261_slave_david-3-b" then 
	end
	if npc_talk_index == "n261_slave_david-3-c" then 
	end
	if npc_talk_index == "n261_slave_david-3-d" then 
	end
	if npc_talk_index == "n261_slave_david-3-e" then 
	end
	if npc_talk_index == "n261_slave_david-3-f" then 
	end
	if npc_talk_index == "n261_slave_david-3-g" then 
	end
	if npc_talk_index == "n261_slave_david-3-h" then 
	end
	if npc_talk_index == "n261_slave_david-3-i" then 
	end
	if npc_talk_index == "n261_slave_david-3-j" then 
	end
	if npc_talk_index == "n261_slave_david-3-k" then 
	end
	if npc_talk_index == "n261_slave_david-3-l" then 
	end
	if npc_talk_index == "n261_slave_david-3-l" then 
	end
	if npc_talk_index == "n261_slave_david-3-m" then 
	end
	if npc_talk_index == "n261_slave_david-3-n" then 
	end
	if npc_talk_index == "n261_slave_david-3-o" then 
	end
	if npc_talk_index == "n261_slave_david-3-p" then 
	end
	if npc_talk_index == "n261_slave_david-3-q" then 
	end
	if npc_talk_index == "n261_slave_david-3-q" then 
	end
	if npc_talk_index == "n261_slave_david-3-r" then 
	end
	if npc_talk_index == "n261_slave_david-3-s" then 
	end
	if npc_talk_index == "n261_slave_david-3-t" then 
	end
	if npc_talk_index == "n261_slave_david-3-u" then 
	end
	if npc_talk_index == "n261_slave_david-3-v" then 
	end
	if npc_talk_index == "n261_slave_david-3-v" then 
	end
	if npc_talk_index == "n261_slave_david-3-v" then 
	end
	if npc_talk_index == "n261_slave_david-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300249, 1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n287_blackdragons_wing--------------------------------------------------------------------------------
function mqc05_9606_people_left_OnTalk_n287_blackdragons_wing( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n287_blackdragons_wing-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n287_blackdragons_wing-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n287_blackdragons_wing-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n287_blackdragons_wing-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n287_blackdragons_wing-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300249, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300249, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9606_people_left_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n039_bishop_ignasio-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n039_bishop_ignasio-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9606, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9606, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9606, 1);

				if api_quest_HasQuestItem( pRoom, userObjID, 300025, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300025, api_quest_HasQuestItem( pRoom, userObjID, 300025, 1));
				end
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-1-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n039_bishop_ignasio-5-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9606_people_left_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9606);
	if qstep == 4 and CountIndex == 300249 then

	end
end

function mqc05_9606_people_left_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9606);
	if qstep == 4 and CountIndex == 300249 and Count >= TargetCount  then

	end
end

function mqc05_9606_people_left_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9606);
	local questID=9606;
end

function mqc05_9606_people_left_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc05_9606_people_left_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc05_9606_people_left_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9606, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9606, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9606, 1);

				if api_quest_HasQuestItem( pRoom, userObjID, 300025, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300025, api_quest_HasQuestItem( pRoom, userObjID, 300025, 1));
				end
				npc_talk_index = "n039_bishop_ignasio-1";
end

</GameServer>