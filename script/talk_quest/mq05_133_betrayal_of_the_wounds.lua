<VillageServer>

function mq05_133_betrayal_of_the_wounds_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 12 then
		mq05_133_betrayal_of_the_wounds_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 13 then
		mq05_133_betrayal_of_the_wounds_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 165 then
		mq05_133_betrayal_of_the_wounds_OnTalk_n165_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_133_betrayal_of_the_wounds_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n012_sorceress_master_cynthia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,133, 2);
				api_quest_SetJournalStep(userObjID,133, 1);
				api_quest_SetQuestStep(userObjID,133, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n013_cleric_tomas--------------------------------------------------------------------------------
function mq05_133_betrayal_of_the_wounds_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n013_cleric_tomas-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-3-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-d" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-e" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-f" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-g" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-h" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1330, true);
				 api_quest_RewardQuestUser(userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1330, true);
				 api_quest_RewardQuestUser(userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1330, true);
				 api_quest_RewardQuestUser(userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1330, true);
				 api_quest_RewardQuestUser(userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1330, true);
				 api_quest_RewardQuestUser(userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1330, true);
				 api_quest_RewardQuestUser(userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1330, true);
				 api_quest_RewardQuestUser(userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1330, true);
				 api_quest_RewardQuestUser(userObjID, 1330, questID, 1);
			 end 
	end
	if npc_talk_index == "n013_cleric_tomas-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 134, 2);
					api_quest_SetQuestStep(userObjID, 134, 1);
					api_quest_SetJournalStep(userObjID, 134, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n013_cleric_tomas-1", "mq05_134_girl_of_destiny.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n165_cleric_jake--------------------------------------------------------------------------------
function mq05_133_betrayal_of_the_wounds_OnTalk_n165_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n165_cleric_jake-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n165_cleric_jake-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n165_cleric_jake-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n165_cleric_jake-2-b" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-c" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-d" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-e" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-e" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-f" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-g" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-h" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-h" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-i" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-j" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-k" then 
	end
	if npc_talk_index == "n165_cleric_jake-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300028, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300028, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300059, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300059, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_133_betrayal_of_the_wounds_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 133);
end

function mq05_133_betrayal_of_the_wounds_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 133);
end

function mq05_133_betrayal_of_the_wounds_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 133);
	local questID=133;
end

function mq05_133_betrayal_of_the_wounds_OnRemoteStart( userObjID, questID )
end

function mq05_133_betrayal_of_the_wounds_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq05_133_betrayal_of_the_wounds_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,133, 2);
				api_quest_SetJournalStep(userObjID,133, 1);
				api_quest_SetQuestStep(userObjID,133, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";
end

</VillageServer>

<GameServer>
function mq05_133_betrayal_of_the_wounds_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 12 then
		mq05_133_betrayal_of_the_wounds_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 13 then
		mq05_133_betrayal_of_the_wounds_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 165 then
		mq05_133_betrayal_of_the_wounds_OnTalk_n165_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_133_betrayal_of_the_wounds_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n012_sorceress_master_cynthia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,133, 2);
				api_quest_SetJournalStep( pRoom, userObjID,133, 1);
				api_quest_SetQuestStep( pRoom, userObjID,133, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n013_cleric_tomas--------------------------------------------------------------------------------
function mq05_133_betrayal_of_the_wounds_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n013_cleric_tomas-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-3-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-d" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-e" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-f" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-g" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-h" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1330, questID, 1);
			 end 
	end
	if npc_talk_index == "n013_cleric_tomas-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 134, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 134, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 134, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n013_cleric_tomas-1", "mq05_134_girl_of_destiny.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n165_cleric_jake--------------------------------------------------------------------------------
function mq05_133_betrayal_of_the_wounds_OnTalk_n165_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n165_cleric_jake-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n165_cleric_jake-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n165_cleric_jake-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n165_cleric_jake-2-b" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-c" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-d" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-e" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-e" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-f" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-g" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-h" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-h" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-i" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-j" then 
	end
	if npc_talk_index == "n165_cleric_jake-2-k" then 
	end
	if npc_talk_index == "n165_cleric_jake-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300028, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300028, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300059, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300059, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_133_betrayal_of_the_wounds_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 133);
end

function mq05_133_betrayal_of_the_wounds_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 133);
end

function mq05_133_betrayal_of_the_wounds_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 133);
	local questID=133;
end

function mq05_133_betrayal_of_the_wounds_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq05_133_betrayal_of_the_wounds_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq05_133_betrayal_of_the_wounds_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,133, 2);
				api_quest_SetJournalStep( pRoom, userObjID,133, 1);
				api_quest_SetQuestStep( pRoom, userObjID,133, 1);
				npc_talk_index = "n012_sorceress_master_cynthia-1";
end

</GameServer>