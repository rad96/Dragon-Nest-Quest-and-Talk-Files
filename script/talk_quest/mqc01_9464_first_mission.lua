<VillageServer>

function mqc01_9464_first_mission_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 11 then
		mqc01_9464_first_mission_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 12 then
		mqc01_9464_first_mission_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1686 then
		mqc01_9464_first_mission_OnTalk_n1686_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function mqc01_9464_first_mission_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n011_priest_leonardo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n011_priest_leonardo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9464, 2);
				api_quest_SetJournalStep(userObjID,9464, 1);
				api_quest_SetQuestStep(userObjID,9464, 1);
				npc_talk_index = "n011_priest_leonardo-1";

	end
	if npc_talk_index == "n011_priest_leonardo-1-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mqc01_9464_first_mission_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n012_sorceress_master_cynthia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n012_sorceress_master_cynthia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-2-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-c" then 

				if api_quest_HasQuestItem(userObjID, 300059, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300059, api_quest_HasQuestItem(userObjID, 300059, 1));
				end
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-g" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-h" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1686_eltia--------------------------------------------------------------------------------
function mqc01_9464_first_mission_OnTalk_n1686_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1686_eltia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1686_eltia-3-b" then 
	end
	if npc_talk_index == "n1686_eltia-3-c" then 
	end
	if npc_talk_index == "n1686_eltia-3-d" then 
	end
	if npc_talk_index == "n1686_eltia-3-e" then 
	end
	if npc_talk_index == "n1686_eltia-3-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94640, true);
				 api_quest_RewardQuestUser(userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94640, true);
				 api_quest_RewardQuestUser(userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94640, true);
				 api_quest_RewardQuestUser(userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94640, true);
				 api_quest_RewardQuestUser(userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94640, true);
				 api_quest_RewardQuestUser(userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94640, true);
				 api_quest_RewardQuestUser(userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94640, true);
				 api_quest_RewardQuestUser(userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94640, true);
				 api_quest_RewardQuestUser(userObjID, 94640, questID, 1);
			 end 
	end
	if npc_talk_index == "n1686_eltia-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9465, 2);
					api_quest_SetQuestStep(userObjID, 9465, 1);
					api_quest_SetJournalStep(userObjID, 9465, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1686_eltia-1", "mqc01_9465_where_is_the_wand.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9464_first_mission_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9464);
end

function mqc01_9464_first_mission_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9464);
end

function mqc01_9464_first_mission_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9464);
	local questID=9464;
end

function mqc01_9464_first_mission_OnRemoteStart( userObjID, questID )
end

function mqc01_9464_first_mission_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc01_9464_first_mission_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9464, 2);
				api_quest_SetJournalStep(userObjID,9464, 1);
				api_quest_SetQuestStep(userObjID,9464, 1);
				npc_talk_index = "n011_priest_leonardo-1";
end

</VillageServer>

<GameServer>
function mqc01_9464_first_mission_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 11 then
		mqc01_9464_first_mission_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 12 then
		mqc01_9464_first_mission_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1686 then
		mqc01_9464_first_mission_OnTalk_n1686_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function mqc01_9464_first_mission_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n011_priest_leonardo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n011_priest_leonardo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9464, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9464, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9464, 1);
				npc_talk_index = "n011_priest_leonardo-1";

	end
	if npc_talk_index == "n011_priest_leonardo-1-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mqc01_9464_first_mission_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n012_sorceress_master_cynthia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n012_sorceress_master_cynthia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-2-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300059, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300059, api_quest_HasQuestItem( pRoom, userObjID, 300059, 1));
				end
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-g" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-h" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1686_eltia--------------------------------------------------------------------------------
function mqc01_9464_first_mission_OnTalk_n1686_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1686_eltia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1686_eltia-3-b" then 
	end
	if npc_talk_index == "n1686_eltia-3-c" then 
	end
	if npc_talk_index == "n1686_eltia-3-d" then 
	end
	if npc_talk_index == "n1686_eltia-3-e" then 
	end
	if npc_talk_index == "n1686_eltia-3-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94640, questID, 1);
			 end 
	end
	if npc_talk_index == "n1686_eltia-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9465, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9465, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9465, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1686_eltia-1", "mqc01_9465_where_is_the_wand.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9464_first_mission_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9464);
end

function mqc01_9464_first_mission_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9464);
end

function mqc01_9464_first_mission_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9464);
	local questID=9464;
end

function mqc01_9464_first_mission_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc01_9464_first_mission_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc01_9464_first_mission_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9464, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9464, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9464, 1);
				npc_talk_index = "n011_priest_leonardo-1";
end

</GameServer>