<VillageServer>

function sq08_4603_battle_strength_new_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 32 then
		sq08_4603_battle_strength_new_OnTalk_n032_warrior_master_chandler(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n032_warrior_master_chandler--------------------------------------------------------------------------------
function sq08_4603_battle_strength_new_OnTalk_n032_warrior_master_chandler(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n032_warrior_master_chandler-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n032_warrior_master_chandler-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n032_warrior_master_chandler-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n032_warrior_master_chandler-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n032_warrior_master_chandler-accepting-j" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46030, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46030, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46030, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46030, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46030, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46030, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46030, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46030, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46030, false);
			 end 

	end
	if npc_talk_index == "n032_warrior_master_chandler-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4603, 1);
				api_quest_SetJournalStep(userObjID,4603, 1);
				api_quest_SetQuestStep(userObjID,4603, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";

	end
	if npc_talk_index == "n032_warrior_master_chandler-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 114, 1 );
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-b" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46030, true);
				 api_quest_RewardQuestUser(userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46030, true);
				 api_quest_RewardQuestUser(userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46030, true);
				 api_quest_RewardQuestUser(userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46030, true);
				 api_quest_RewardQuestUser(userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46030, true);
				 api_quest_RewardQuestUser(userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46030, true);
				 api_quest_RewardQuestUser(userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46030, true);
				 api_quest_RewardQuestUser(userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46030, true);
				 api_quest_RewardQuestUser(userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46030, true);
				 api_quest_RewardQuestUser(userObjID, 46030, questID, 1);
			 end 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_4603_battle_strength_new_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4603);
	 local qstep= 1; 
	 if qstep == 1 then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
		 end 
	 end 
end

function sq08_4603_battle_strength_new_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4603);
end

function sq08_4603_battle_strength_new_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4603);
	local questID=4603;
end

function sq08_4603_battle_strength_new_OnRemoteStart( userObjID, questID )
end

function sq08_4603_battle_strength_new_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_4603_battle_strength_new_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4603, 1);
				api_quest_SetJournalStep(userObjID,4603, 1);
				api_quest_SetQuestStep(userObjID,4603, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";
end

</VillageServer>

<GameServer>
function sq08_4603_battle_strength_new_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 32 then
		sq08_4603_battle_strength_new_OnTalk_n032_warrior_master_chandler( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n032_warrior_master_chandler--------------------------------------------------------------------------------
function sq08_4603_battle_strength_new_OnTalk_n032_warrior_master_chandler( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n032_warrior_master_chandler-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n032_warrior_master_chandler-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n032_warrior_master_chandler-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n032_warrior_master_chandler-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n032_warrior_master_chandler-accepting-j" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, false);
			 end 

	end
	if npc_talk_index == "n032_warrior_master_chandler-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4603, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4603, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4603, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";

	end
	if npc_talk_index == "n032_warrior_master_chandler-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 114, 1 );
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-b" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-c" then 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46030, questID, 1);
			 end 
	end
	if npc_talk_index == "n032_warrior_master_chandler-3-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_4603_battle_strength_new_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4603);
	 local qstep= 1; 
	 if qstep == 1 then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
		 end 
	 end 
end

function sq08_4603_battle_strength_new_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4603);
end

function sq08_4603_battle_strength_new_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4603);
	local questID=4603;
end

function sq08_4603_battle_strength_new_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_4603_battle_strength_new_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_4603_battle_strength_new_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4603, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4603, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4603, 1);
				npc_talk_index = "n032_warrior_master_chandler-1";
end

</GameServer>