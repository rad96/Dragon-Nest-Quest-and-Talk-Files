<VillageServer>

function sq08_378_test_academic2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 392 then
		sq08_378_test_academic2_OnTalk_n392_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 780 then
		sq08_378_test_academic2_OnTalk_n780_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n392_academic_station--------------------------------------------------------------------------------
function sq08_378_test_academic2_OnTalk_n392_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n392_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n392_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n392_academic_station-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n392_academic_station-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n392_academic_station-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3780, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3780, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3780, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3780, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3780, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3780, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3780, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3780, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3780, false);
			 end 

	end
	if npc_talk_index == "n392_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,378, 1);
				api_quest_SetJournalStep(userObjID,378, 1);
				api_quest_SetQuestStep(userObjID,378, 1);
				npc_talk_index = "n392_academic_station-1";

	end
	if npc_talk_index == "n392_academic_station-1-b" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300018, 1);
	end
	if npc_talk_index == "n392_academic_station-3-b" then 
	end
	if npc_talk_index == "n392_academic_station-3-c" then 
	end
	if npc_talk_index == "n392_academic_station-3-d" then 
	end
	if npc_talk_index == "n392_academic_station-3-b" then 
	end
	if npc_talk_index == "n392_academic_station-3-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3781, true);
				 api_quest_RewardQuestUser(userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3781, true);
				 api_quest_RewardQuestUser(userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3781, true);
				 api_quest_RewardQuestUser(userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3781, true);
				 api_quest_RewardQuestUser(userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3781, true);
				 api_quest_RewardQuestUser(userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3781, true);
				 api_quest_RewardQuestUser(userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3781, true);
				 api_quest_RewardQuestUser(userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3781, true);
				 api_quest_RewardQuestUser(userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3781, true);
				 api_quest_RewardQuestUser(userObjID, 3781, questID, 1);
			 end 
	end
	if npc_talk_index == "n392_academic_station-3-b" then 
	end
	if npc_talk_index == "n392_academic_station-3-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3782, true);
				 api_quest_RewardQuestUser(userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3782, true);
				 api_quest_RewardQuestUser(userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3782, true);
				 api_quest_RewardQuestUser(userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3782, true);
				 api_quest_RewardQuestUser(userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3782, true);
				 api_quest_RewardQuestUser(userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3782, true);
				 api_quest_RewardQuestUser(userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3782, true);
				 api_quest_RewardQuestUser(userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3782, true);
				 api_quest_RewardQuestUser(userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3782, true);
				 api_quest_RewardQuestUser(userObjID, 3782, questID, 1);
			 end 
	end
	if npc_talk_index == "n392_academic_station-3-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 379, 1);
					api_quest_SetQuestStep(userObjID, 379, 1);
					api_quest_SetJournalStep(userObjID, 379, 1);

									api_user_SetUserJobID(userObjID, 46);

				if api_quest_HasQuestItem(userObjID, 300018, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300018, api_quest_HasQuestItem(userObjID, 300018, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n392_academic_station-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n392_academic_station-1", "sq08_379_change_job5.xml");
			return;
		end
	end
	if npc_talk_index == "n392_academic_station-3-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 379, 1);
					api_quest_SetQuestStep(userObjID, 379, 1);
					api_quest_SetJournalStep(userObjID, 379, 1);

									api_user_SetUserJobID(userObjID, 49);

				if api_quest_HasQuestItem(userObjID, 300018, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300018, api_quest_HasQuestItem(userObjID, 300018, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n780_velskud--------------------------------------------------------------------------------
function sq08_378_test_academic2_OnTalk_n780_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n780_velskud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n780_velskud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n780_velskud-2-b" then 
	end
	if npc_talk_index == "n780_velskud-2-c" then 
	end
	if npc_talk_index == "n780_velskud-2-d" then 
	end
	if npc_talk_index == "n780_velskud-2-e" then 
	end
	if npc_talk_index == "n780_velskud-2-f" then 
	end
	if npc_talk_index == "n780_velskud-2-g" then 
	end
	if npc_talk_index == "n780_velskud-2-h" then 
	end
	if npc_talk_index == "n780_velskud-2-i" then 
	end
	if npc_talk_index == "n780_velskud-2-j" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300018, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300018, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_378_test_academic2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 378);
	if qstep == 2 and CountIndex == 300018 then

	end
end

function sq08_378_test_academic2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 378);
	if qstep == 2 and CountIndex == 300018 and Count >= TargetCount  then

	end
end

function sq08_378_test_academic2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 378);
	local questID=378;
end

function sq08_378_test_academic2_OnRemoteStart( userObjID, questID )
end

function sq08_378_test_academic2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_378_test_academic2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,378, 1);
				api_quest_SetJournalStep(userObjID,378, 1);
				api_quest_SetQuestStep(userObjID,378, 1);
				npc_talk_index = "n392_academic_station-1";
end

</VillageServer>

<GameServer>
function sq08_378_test_academic2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 392 then
		sq08_378_test_academic2_OnTalk_n392_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 780 then
		sq08_378_test_academic2_OnTalk_n780_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n392_academic_station--------------------------------------------------------------------------------
function sq08_378_test_academic2_OnTalk_n392_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n392_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n392_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n392_academic_station-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n392_academic_station-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n392_academic_station-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3780, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3780, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3780, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3780, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3780, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3780, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3780, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3780, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3780, false);
			 end 

	end
	if npc_talk_index == "n392_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,378, 1);
				api_quest_SetJournalStep( pRoom, userObjID,378, 1);
				api_quest_SetQuestStep( pRoom, userObjID,378, 1);
				npc_talk_index = "n392_academic_station-1";

	end
	if npc_talk_index == "n392_academic_station-1-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300018, 1);
	end
	if npc_talk_index == "n392_academic_station-3-b" then 
	end
	if npc_talk_index == "n392_academic_station-3-c" then 
	end
	if npc_talk_index == "n392_academic_station-3-d" then 
	end
	if npc_talk_index == "n392_academic_station-3-b" then 
	end
	if npc_talk_index == "n392_academic_station-3-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3781, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3781, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3781, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3781, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3781, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3781, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3781, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3781, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3781, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3781, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3781, questID, 1);
			 end 
	end
	if npc_talk_index == "n392_academic_station-3-b" then 
	end
	if npc_talk_index == "n392_academic_station-3-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3782, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3782, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3782, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3782, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3782, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3782, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3782, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3782, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3782, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3782, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3782, questID, 1);
			 end 
	end
	if npc_talk_index == "n392_academic_station-3-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 379, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 379, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 379, 1);

									api_user_SetUserJobID( pRoom, userObjID, 46);

				if api_quest_HasQuestItem( pRoom, userObjID, 300018, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300018, api_quest_HasQuestItem( pRoom, userObjID, 300018, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n392_academic_station-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n392_academic_station-1", "sq08_379_change_job5.xml");
			return;
		end
	end
	if npc_talk_index == "n392_academic_station-3-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 379, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 379, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 379, 1);

									api_user_SetUserJobID( pRoom, userObjID, 49);

				if api_quest_HasQuestItem( pRoom, userObjID, 300018, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300018, api_quest_HasQuestItem( pRoom, userObjID, 300018, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n780_velskud--------------------------------------------------------------------------------
function sq08_378_test_academic2_OnTalk_n780_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n780_velskud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n780_velskud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n780_velskud-2-b" then 
	end
	if npc_talk_index == "n780_velskud-2-c" then 
	end
	if npc_talk_index == "n780_velskud-2-d" then 
	end
	if npc_talk_index == "n780_velskud-2-e" then 
	end
	if npc_talk_index == "n780_velskud-2-f" then 
	end
	if npc_talk_index == "n780_velskud-2-g" then 
	end
	if npc_talk_index == "n780_velskud-2-h" then 
	end
	if npc_talk_index == "n780_velskud-2-i" then 
	end
	if npc_talk_index == "n780_velskud-2-j" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300018, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300018, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_378_test_academic2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 378);
	if qstep == 2 and CountIndex == 300018 then

	end
end

function sq08_378_test_academic2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 378);
	if qstep == 2 and CountIndex == 300018 and Count >= TargetCount  then

	end
end

function sq08_378_test_academic2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 378);
	local questID=378;
end

function sq08_378_test_academic2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_378_test_academic2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_378_test_academic2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,378, 1);
				api_quest_SetJournalStep( pRoom, userObjID,378, 1);
				api_quest_SetQuestStep( pRoom, userObjID,378, 1);
				npc_talk_index = "n392_academic_station-1";
end

</GameServer>