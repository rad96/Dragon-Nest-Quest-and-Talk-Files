<VillageServer>

function sq08_299_fear_of_abyss2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 28 then
		sq08_299_fear_of_abyss2_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_299_fear_of_abyss2_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n028_scholar_bailey-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,299, 1);
				api_quest_SetJournalStep(userObjID,299, 1);
				api_quest_SetQuestStep(userObjID,299, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2991, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2992, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2993, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2994, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2995, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2996, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2997, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2997, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2997, false);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-1-e" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300132, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300133, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300134, 1);
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 300135, 1);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200612, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200664, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200712, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 200762, 30000);
	end
	if npc_talk_index == "n028_scholar_bailey-1-f" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2991, true);
				 api_quest_RewardQuestUser(userObjID, 2991, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2992, true);
				 api_quest_RewardQuestUser(userObjID, 2992, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2993, true);
				 api_quest_RewardQuestUser(userObjID, 2993, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2994, true);
				 api_quest_RewardQuestUser(userObjID, 2994, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2995, true);
				 api_quest_RewardQuestUser(userObjID, 2995, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2996, true);
				 api_quest_RewardQuestUser(userObjID, 2996, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2997, true);
				 api_quest_RewardQuestUser(userObjID, 2997, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2997, true);
				 api_quest_RewardQuestUser(userObjID, 2997, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2997, true);
				 api_quest_RewardQuestUser(userObjID, 2997, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-3-f" then 

				if api_quest_HasQuestItem(userObjID, 300132, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300132, api_quest_HasQuestItem(userObjID, 300132, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300133, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300133, api_quest_HasQuestItem(userObjID, 300133, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300134, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300134, api_quest_HasQuestItem(userObjID, 300134, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300135, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300135, api_quest_HasQuestItem(userObjID, 300135, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_299_fear_of_abyss2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 299);
	if qstep == 2 and CountIndex == 200612 then
				if api_quest_HasQuestItem(userObjID, 300132, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300132, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300132, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200664 then
				if api_quest_HasQuestItem(userObjID, 300133, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300133, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300133, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200712 then
				if api_quest_HasQuestItem(userObjID, 300134, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300134, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200762 then
				if api_quest_HasQuestItem(userObjID, 300135, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300135, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300135, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 300132 then

	end
	if qstep == 2 and CountIndex == 300133 then

	end
	if qstep == 2 and CountIndex == 300134 then

	end
	if qstep == 2 and CountIndex == 300135 then

	end
end

function sq08_299_fear_of_abyss2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 299);
	if qstep == 2 and CountIndex == 200612 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200664 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200712 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200762 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300132 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300133 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300134 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300135 and Count >= TargetCount  then

	end
end

function sq08_299_fear_of_abyss2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 299);
	local questID=299;
end

function sq08_299_fear_of_abyss2_OnRemoteStart( userObjID, questID )
end

function sq08_299_fear_of_abyss2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_299_fear_of_abyss2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,299, 1);
				api_quest_SetJournalStep(userObjID,299, 1);
				api_quest_SetQuestStep(userObjID,299, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</VillageServer>

<GameServer>
function sq08_299_fear_of_abyss2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 28 then
		sq08_299_fear_of_abyss2_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_299_fear_of_abyss2_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n028_scholar_bailey-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,299, 1);
				api_quest_SetJournalStep( pRoom, userObjID,299, 1);
				api_quest_SetQuestStep( pRoom, userObjID,299, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2991, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2992, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2993, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2994, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2995, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2996, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2997, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2997, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2997, false);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-1-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300132, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300133, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300134, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 300135, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200612, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200664, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200712, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 200762, 30000);
	end
	if npc_talk_index == "n028_scholar_bailey-1-f" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2991, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2991, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2992, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2992, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2993, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2993, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2994, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2994, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2995, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2995, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2996, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2996, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2997, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2997, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2997, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2997, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2997, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2997, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-3-f" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300132, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300132, api_quest_HasQuestItem( pRoom, userObjID, 300132, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300133, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300133, api_quest_HasQuestItem( pRoom, userObjID, 300133, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300134, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300134, api_quest_HasQuestItem( pRoom, userObjID, 300134, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300135, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300135, api_quest_HasQuestItem( pRoom, userObjID, 300135, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_299_fear_of_abyss2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 299);
	if qstep == 2 and CountIndex == 200612 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300132, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300132, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300132, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200664 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300133, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300133, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300133, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200712 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300134, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300134, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300134, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200762 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300135, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300135, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300135, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300132, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300133, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300134, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300135, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 300132 then

	end
	if qstep == 2 and CountIndex == 300133 then

	end
	if qstep == 2 and CountIndex == 300134 then

	end
	if qstep == 2 and CountIndex == 300135 then

	end
end

function sq08_299_fear_of_abyss2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 299);
	if qstep == 2 and CountIndex == 200612 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200664 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200712 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200762 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300132 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300133 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300134 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300135 and Count >= TargetCount  then

	end
end

function sq08_299_fear_of_abyss2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 299);
	local questID=299;
end

function sq08_299_fear_of_abyss2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_299_fear_of_abyss2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_299_fear_of_abyss2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,299, 1);
				api_quest_SetJournalStep( pRoom, userObjID,299, 1);
				api_quest_SetQuestStep( pRoom, userObjID,299, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</GameServer>