<VillageServer>

function mq15_9204_warrior_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 707 then
		mq15_9204_warrior_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		mq15_9204_warrior_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mq15_9204_warrior_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function mq15_9204_warrior_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n711_warrior_fedro-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n711_warrior_fedro-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9204, 2);
				api_quest_SetJournalStep(userObjID,9204, 1);
				api_quest_SetQuestStep(userObjID,9204, 1);
				npc_talk_index = "n711_warrior_fedro-1";

	end
	if npc_talk_index == "n711_warrior_fedro-1-class_check" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n711_warrior_fedro-1-b";

				else
									npc_talk_index = "n711_warrior_fedro-1-d";

				end
	end
	if npc_talk_index == "n711_warrior_fedro-1-c" then 
	end
	if npc_talk_index == "n711_warrior_fedro-1-f" then 
	end
	if npc_talk_index == "n711_warrior_fedro-1-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-1-f" then 
	end
	if npc_talk_index == "n711_warrior_fedro-1-g" then 
	end
	if npc_talk_index == "n711_warrior_fedro-1-h" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n711_warrior_fedro-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92040, true);
				 api_quest_RewardQuestUser(userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92040, true);
				 api_quest_RewardQuestUser(userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92040, true);
				 api_quest_RewardQuestUser(userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92040, true);
				 api_quest_RewardQuestUser(userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92040, true);
				 api_quest_RewardQuestUser(userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92040, true);
				 api_quest_RewardQuestUser(userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92040, true);
				 api_quest_RewardQuestUser(userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92040, true);
				 api_quest_RewardQuestUser(userObjID, 92040, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9205, 2);
					api_quest_SetQuestStep(userObjID, 9205, 1);
					api_quest_SetJournalStep(userObjID, 9205, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n711_warrior_fedro-1", "mq15_9205_shadow.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9204_warrior_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9204);
end

function mq15_9204_warrior_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9204);
end

function mq15_9204_warrior_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9204);
	local questID=9204;
end

function mq15_9204_warrior_OnRemoteStart( userObjID, questID )
end

function mq15_9204_warrior_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9204_warrior_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9204, 2);
				api_quest_SetJournalStep(userObjID,9204, 1);
				api_quest_SetQuestStep(userObjID,9204, 1);
				npc_talk_index = "n711_warrior_fedro-1";
end

</VillageServer>

<GameServer>
function mq15_9204_warrior_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 707 then
		mq15_9204_warrior_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		mq15_9204_warrior_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mq15_9204_warrior_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function mq15_9204_warrior_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n711_warrior_fedro-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n711_warrior_fedro-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9204, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9204, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9204, 1);
				npc_talk_index = "n711_warrior_fedro-1";

	end
	if npc_talk_index == "n711_warrior_fedro-1-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n711_warrior_fedro-1-b";

				else
									npc_talk_index = "n711_warrior_fedro-1-d";

				end
	end
	if npc_talk_index == "n711_warrior_fedro-1-c" then 
	end
	if npc_talk_index == "n711_warrior_fedro-1-f" then 
	end
	if npc_talk_index == "n711_warrior_fedro-1-e" then 
	end
	if npc_talk_index == "n711_warrior_fedro-1-f" then 
	end
	if npc_talk_index == "n711_warrior_fedro-1-g" then 
	end
	if npc_talk_index == "n711_warrior_fedro-1-h" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n711_warrior_fedro-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92040, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92040, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92040, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9205, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9205, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9205, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n711_warrior_fedro-1", "mq15_9205_shadow.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9204_warrior_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9204);
end

function mq15_9204_warrior_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9204);
end

function mq15_9204_warrior_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9204);
	local questID=9204;
end

function mq15_9204_warrior_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9204_warrior_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9204_warrior_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9204, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9204, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9204, 1);
				npc_talk_index = "n711_warrior_fedro-1";
end

</GameServer>