<VillageServer>

function sq11_468_saint_challenger1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 242 then
		sq11_468_saint_challenger1_OnTalk_n242_orc_oneeye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq11_468_saint_challenger1_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n242_orc_oneeye--------------------------------------------------------------------------------
function sq11_468_saint_challenger1_OnTalk_n242_orc_oneeye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n242_orc_oneeye-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n242_orc_oneeye-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n242_orc_oneeye-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n242_orc_oneeye-2-b" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-c" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-d" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-e" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-f" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-g" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-h" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_468_saint_challenger1_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n098_warrior_master_lodrigo-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-n" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4681, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4682, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4683, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4684, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4685, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4686, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4687, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4688, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4689, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest(userObjID,468, 1);
				api_quest_SetJournalStep(userObjID,468, 1);
				api_quest_SetQuestStep(userObjID,468, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4681, true);
				 api_quest_RewardQuestUser(userObjID, 4681, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4682, true);
				 api_quest_RewardQuestUser(userObjID, 4682, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4683, true);
				 api_quest_RewardQuestUser(userObjID, 4683, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4684, true);
				 api_quest_RewardQuestUser(userObjID, 4684, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4685, true);
				 api_quest_RewardQuestUser(userObjID, 4685, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4686, true);
				 api_quest_RewardQuestUser(userObjID, 4686, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4687, true);
				 api_quest_RewardQuestUser(userObjID, 4687, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4688, true);
				 api_quest_RewardQuestUser(userObjID, 4688, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4689, true);
				 api_quest_RewardQuestUser(userObjID, 4689, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 469, 1);
					api_quest_SetQuestStep(userObjID, 469, 1);
					api_quest_SetJournalStep(userObjID, 469, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-d" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-e" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-f" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-g" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-h" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-i" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-j" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n098_warrior_master_lodrigo-1", "sq11_469_saint_challenger2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_468_saint_challenger1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 468);
end

function sq11_468_saint_challenger1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 468);
end

function sq11_468_saint_challenger1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 468);
	local questID=468;
end

function sq11_468_saint_challenger1_OnRemoteStart( userObjID, questID )
end

function sq11_468_saint_challenger1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_468_saint_challenger1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,468, 1);
				api_quest_SetJournalStep(userObjID,468, 1);
				api_quest_SetQuestStep(userObjID,468, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</VillageServer>

<GameServer>
function sq11_468_saint_challenger1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 242 then
		sq11_468_saint_challenger1_OnTalk_n242_orc_oneeye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq11_468_saint_challenger1_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n242_orc_oneeye--------------------------------------------------------------------------------
function sq11_468_saint_challenger1_OnTalk_n242_orc_oneeye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n242_orc_oneeye-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n242_orc_oneeye-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n242_orc_oneeye-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n242_orc_oneeye-2-b" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-c" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-d" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-e" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-f" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-g" then 
	end
	if npc_talk_index == "n242_orc_oneeye-2-h" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_468_saint_challenger1_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n098_warrior_master_lodrigo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n098_warrior_master_lodrigo-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-n" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4681, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4682, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4683, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4684, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4685, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4686, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4687, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4688, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4689, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,468, 1);
				api_quest_SetJournalStep( pRoom, userObjID,468, 1);
				api_quest_SetQuestStep( pRoom, userObjID,468, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4681, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4681, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4682, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4682, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4683, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4683, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4684, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4684, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4685, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4685, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4686, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4686, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4687, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4687, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4688, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4688, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4689, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4689, questID, 1);
			 end 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 469, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 469, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 469, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-d" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-e" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-f" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-g" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-h" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-i" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-j" then 
	end
	if npc_talk_index == "n098_warrior_master_lodrigo-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n098_warrior_master_lodrigo-1", "sq11_469_saint_challenger2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_468_saint_challenger1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 468);
end

function sq11_468_saint_challenger1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 468);
end

function sq11_468_saint_challenger1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 468);
	local questID=468;
end

function sq11_468_saint_challenger1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_468_saint_challenger1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_468_saint_challenger1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,468, 1);
				api_quest_SetJournalStep( pRoom, userObjID,468, 1);
				api_quest_SetQuestStep( pRoom, userObjID,468, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</GameServer>