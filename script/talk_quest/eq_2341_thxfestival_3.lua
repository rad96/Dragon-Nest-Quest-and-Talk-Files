<VillageServer>

function eq_2341_thxfestival_3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 100 then
		eq_2341_thxfestival_3_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function eq_2341_thxfestival_3_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				if api_user_HasItem(userObjID, 805308697, 7) >= 7 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-1-d";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23410, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23410, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23410, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23410, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23410, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23410, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23410, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2341, 1);
				api_quest_SetJournalStep(userObjID,2341, 1);
				api_quest_SetQuestStep(userObjID,2341, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805308697, 30002);
				if api_user_HasItem(userObjID, 805308697, 7) >= 7 then
									api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-accepting-d";

				end

	end
	if npc_talk_index == "n100_event_ilyn-1-change_event" then 
				if api_user_HasItem(userObjID, 805308697, 7) >= 7 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23410, true);
				 api_quest_RewardQuestUser(userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23410, true);
				 api_quest_RewardQuestUser(userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23410, true);
				 api_quest_RewardQuestUser(userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23410, true);
				 api_quest_RewardQuestUser(userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23410, true);
				 api_quest_RewardQuestUser(userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23410, true);
				 api_quest_RewardQuestUser(userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23410, true);
				 api_quest_RewardQuestUser(userObjID, 23410, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-c";

				else
									npc_talk_index = "n100_event_ilyn-1-b";

				end
	end
	if npc_talk_index == "n100_event_ilyn-1-exit_event" then 
				api_npc_NextScript(userObjID, npcObjID, "quest", "n100_event_ilyn.xml");
	end
	if npc_talk_index == "n100_event_ilyn-1-a" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_DelItem(userObjID, 805308697, 7, questID);


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2341_thxfestival_3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2341);
	if qstep == 1 and CountIndex == 805308697 then
				if api_user_HasItem(userObjID, 805308697, 7) >= 7 then
									api_quest_SetJournalStep(userObjID, questID, 2);

				else
									api_quest_SetJournalStep(userObjID, questID, 1);

				end

	end
end

function eq_2341_thxfestival_3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2341);
	if qstep == 1 and CountIndex == 805308697 and Count >= TargetCount  then

	end
end

function eq_2341_thxfestival_3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2341);
	local questID=2341;
end

function eq_2341_thxfestival_3_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805308697, 30002);
				api_quest_SetJournalStep(userObjID, questID, 2);
end

function eq_2341_thxfestival_3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function eq_2341_thxfestival_3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n100_event_ilyn-1";
				npc_talk_index = "n100_event_ilyn-accepting-d";
				api_quest_AddQuest(userObjID,2341, 1);
				api_quest_SetJournalStep(userObjID,2341, 1);
				api_quest_SetQuestStep(userObjID,2341, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805308697, 30002);
				if api_user_HasItem(userObjID, 805308697, 7) >= 7 then
									api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-accepting-d";

				end
end

</VillageServer>

<GameServer>
function eq_2341_thxfestival_3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 100 then
		eq_2341_thxfestival_3_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function eq_2341_thxfestival_3_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				if api_user_HasItem( pRoom, userObjID, 805308697, 7) >= 7 then
									npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-1-d";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2341, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2341, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2341, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805308697, 30002);
				if api_user_HasItem( pRoom, userObjID, 805308697, 7) >= 7 then
									api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-accepting-d";

				end

	end
	if npc_talk_index == "n100_event_ilyn-1-change_event" then 
				if api_user_HasItem( pRoom, userObjID, 805308697, 7) >= 7 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23410, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23410, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-c";

				else
									npc_talk_index = "n100_event_ilyn-1-b";

				end
	end
	if npc_talk_index == "n100_event_ilyn-1-exit_event" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "quest", "n100_event_ilyn.xml");
	end
	if npc_talk_index == "n100_event_ilyn-1-a" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_DelItem( pRoom, userObjID, 805308697, 7, questID);


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2341_thxfestival_3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2341);
	if qstep == 1 and CountIndex == 805308697 then
				if api_user_HasItem( pRoom, userObjID, 805308697, 7) >= 7 then
									api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
									api_quest_SetJournalStep( pRoom, userObjID, questID, 1);

				end

	end
end

function eq_2341_thxfestival_3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2341);
	if qstep == 1 and CountIndex == 805308697 and Count >= TargetCount  then

	end
end

function eq_2341_thxfestival_3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2341);
	local questID=2341;
end

function eq_2341_thxfestival_3_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805308697, 30002);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
end

function eq_2341_thxfestival_3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function eq_2341_thxfestival_3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n100_event_ilyn-1";
				npc_talk_index = "n100_event_ilyn-accepting-d";
				api_quest_AddQuest( pRoom, userObjID,2341, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2341, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2341, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805308697, 30002);
				if api_user_HasItem( pRoom, userObjID, 805308697, 7) >= 7 then
									api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n100_event_ilyn-1";

				else
									npc_talk_index = "n100_event_ilyn-accepting-d";

				end
end

</GameServer>