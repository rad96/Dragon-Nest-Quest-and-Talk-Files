<VillageServer>

function mqc01_9574_antagonism_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 129 then
		mqc01_9574_antagonism_OnTalk_n129_scholar_bunstain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 569 then
		mqc01_9574_antagonism_OnTalk_n569_guard_timose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1847 then
		mqc01_9574_antagonism_OnTalk_n1847_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1841 then
		mqc01_9574_antagonism_OnTalk_n1841_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n129_scholar_bunstain--------------------------------------------------------------------------------
function mqc01_9574_antagonism_OnTalk_n129_scholar_bunstain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n129_scholar_bunstain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n129_scholar_bunstain-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n129_scholar_bunstain-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n129_scholar_bunstain-accepting-1" then
				api_quest_AddQuest(userObjID,9574, 2);
				api_quest_SetJournalStep(userObjID,9574, 1);
				api_quest_SetQuestStep(userObjID,9574, 1);
				npc_talk_index = "n129_scholar_bunstain-1";

	end
	if npc_talk_index == "n129_scholar_bunstain-1-b" then 
	end
	if npc_talk_index == "n129_scholar_bunstain-1-c" then 
	end
	if npc_talk_index == "n129_scholar_bunstain-1-d" then 
	end
	if npc_talk_index == "n129_scholar_bunstain-1-e" then 
	end
	if npc_talk_index == "n129_scholar_bunstain-1-f" then 
	end
	if npc_talk_index == "n129_scholar_bunstain-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				if api_quest_HasQuestItem(userObjID, 300009, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300009, api_quest_HasQuestItem(userObjID, 300009, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300010, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300010, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n569_guard_timose--------------------------------------------------------------------------------
function mqc01_9574_antagonism_OnTalk_n569_guard_timose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n569_guard_timose-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n569_guard_timose-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n569_guard_timose-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n569_guard_timose-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1847_suriya--------------------------------------------------------------------------------
function mqc01_9574_antagonism_OnTalk_n1847_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1847_suriya-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1847_suriya-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1847_suriya-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1847_suriya-4-b" then 
	end
	if npc_talk_index == "n1847_suriya-4-c" then 
	end
	if npc_talk_index == "n1847_suriya-4-d" then 
	end
	if npc_talk_index == "n1847_suriya-4-e" then 
	end
	if npc_talk_index == "n1847_suriya-4-f" then 
	end
	if npc_talk_index == "n1847_suriya-4-g" then 
	end
	if npc_talk_index == "n1847_suriya-4-h" then 
	end
	if npc_talk_index == "n1847_suriya-4-i" then 
	end
	if npc_talk_index == "n1847_suriya-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1841_shaolong--------------------------------------------------------------------------------
function mqc01_9574_antagonism_OnTalk_n1841_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1841_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1841_shaolong-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1841_shaolong-5-b" then 
	end
	if npc_talk_index == "n1841_shaolong-5-c" then 
	end
	if npc_talk_index == "n1841_shaolong-5-d" then 
	end
	if npc_talk_index == "n1841_shaolong-5-e" then 
	end
	if npc_talk_index == "n1841_shaolong-5-f" then 
	end
	if npc_talk_index == "n1841_shaolong-5-g" then 
	end
	if npc_talk_index == "n1841_shaolong-5-h" then 
	end
	if npc_talk_index == "n1841_shaolong-5-i" then 
	end
	if npc_talk_index == "n1841_shaolong-5-j" then 
	end
	if npc_talk_index == "n1841_shaolong-5-k" then 
	end
	if npc_talk_index == "n1841_shaolong-5-l" then 
	end
	if npc_talk_index == "n1841_shaolong-5-m" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95740, true);
				 api_quest_RewardQuestUser(userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95740, true);
				 api_quest_RewardQuestUser(userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95740, true);
				 api_quest_RewardQuestUser(userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95740, true);
				 api_quest_RewardQuestUser(userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95740, true);
				 api_quest_RewardQuestUser(userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95740, true);
				 api_quest_RewardQuestUser(userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95740, true);
				 api_quest_RewardQuestUser(userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95740, true);
				 api_quest_RewardQuestUser(userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95740, true);
				 api_quest_RewardQuestUser(userObjID, 95740, questID, 1);
			 end 
	end
	if npc_talk_index == "n1841_shaolong-5-n" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9575, 2);
					api_quest_SetQuestStep(userObjID, 9575, 1);
					api_quest_SetJournalStep(userObjID, 9575, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1841_shaolong-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1841_shaolong-1", "mqc01_9575_botched_work_processing.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9574_antagonism_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9574);
end

function mqc01_9574_antagonism_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9574);
end

function mqc01_9574_antagonism_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9574);
	local questID=9574;
end

function mqc01_9574_antagonism_OnRemoteStart( userObjID, questID )
end

function mqc01_9574_antagonism_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc01_9574_antagonism_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mqc01_9574_antagonism_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 129 then
		mqc01_9574_antagonism_OnTalk_n129_scholar_bunstain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 569 then
		mqc01_9574_antagonism_OnTalk_n569_guard_timose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1847 then
		mqc01_9574_antagonism_OnTalk_n1847_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1841 then
		mqc01_9574_antagonism_OnTalk_n1841_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n129_scholar_bunstain--------------------------------------------------------------------------------
function mqc01_9574_antagonism_OnTalk_n129_scholar_bunstain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n129_scholar_bunstain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n129_scholar_bunstain-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n129_scholar_bunstain-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n129_scholar_bunstain-accepting-1" then
				api_quest_AddQuest( pRoom, userObjID,9574, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9574, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9574, 1);
				npc_talk_index = "n129_scholar_bunstain-1";

	end
	if npc_talk_index == "n129_scholar_bunstain-1-b" then 
	end
	if npc_talk_index == "n129_scholar_bunstain-1-c" then 
	end
	if npc_talk_index == "n129_scholar_bunstain-1-d" then 
	end
	if npc_talk_index == "n129_scholar_bunstain-1-e" then 
	end
	if npc_talk_index == "n129_scholar_bunstain-1-f" then 
	end
	if npc_talk_index == "n129_scholar_bunstain-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				if api_quest_HasQuestItem( pRoom, userObjID, 300009, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300009, api_quest_HasQuestItem( pRoom, userObjID, 300009, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300010, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300010, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n569_guard_timose--------------------------------------------------------------------------------
function mqc01_9574_antagonism_OnTalk_n569_guard_timose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n569_guard_timose-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n569_guard_timose-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n569_guard_timose-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n569_guard_timose-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1847_suriya--------------------------------------------------------------------------------
function mqc01_9574_antagonism_OnTalk_n1847_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1847_suriya-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1847_suriya-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1847_suriya-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1847_suriya-4-b" then 
	end
	if npc_talk_index == "n1847_suriya-4-c" then 
	end
	if npc_talk_index == "n1847_suriya-4-d" then 
	end
	if npc_talk_index == "n1847_suriya-4-e" then 
	end
	if npc_talk_index == "n1847_suriya-4-f" then 
	end
	if npc_talk_index == "n1847_suriya-4-g" then 
	end
	if npc_talk_index == "n1847_suriya-4-h" then 
	end
	if npc_talk_index == "n1847_suriya-4-i" then 
	end
	if npc_talk_index == "n1847_suriya-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1841_shaolong--------------------------------------------------------------------------------
function mqc01_9574_antagonism_OnTalk_n1841_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1841_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1841_shaolong-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1841_shaolong-5-b" then 
	end
	if npc_talk_index == "n1841_shaolong-5-c" then 
	end
	if npc_talk_index == "n1841_shaolong-5-d" then 
	end
	if npc_talk_index == "n1841_shaolong-5-e" then 
	end
	if npc_talk_index == "n1841_shaolong-5-f" then 
	end
	if npc_talk_index == "n1841_shaolong-5-g" then 
	end
	if npc_talk_index == "n1841_shaolong-5-h" then 
	end
	if npc_talk_index == "n1841_shaolong-5-i" then 
	end
	if npc_talk_index == "n1841_shaolong-5-j" then 
	end
	if npc_talk_index == "n1841_shaolong-5-k" then 
	end
	if npc_talk_index == "n1841_shaolong-5-l" then 
	end
	if npc_talk_index == "n1841_shaolong-5-m" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95740, questID, 1);
			 end 
	end
	if npc_talk_index == "n1841_shaolong-5-n" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9575, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9575, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9575, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1841_shaolong-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1841_shaolong-1", "mqc01_9575_botched_work_processing.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9574_antagonism_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9574);
end

function mqc01_9574_antagonism_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9574);
end

function mqc01_9574_antagonism_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9574);
	local questID=9574;
end

function mqc01_9574_antagonism_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc01_9574_antagonism_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc01_9574_antagonism_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>