<VillageServer>

function sq15_4217_part_with_pal_2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 739 then
		sq15_4217_part_with_pal_2_OnTalk_n739_darklair_grissini(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 741 then
		sq15_4217_part_with_pal_2_OnTalk_n741_darklair_grissini(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n739_darklair_grissini--------------------------------------------------------------------------------
function sq15_4217_part_with_pal_2_OnTalk_n739_darklair_grissini(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n739_darklair_grissini-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n739_darklair_grissini-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n739_darklair_grissini-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n739_darklair_grissini-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 42170, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 42170, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 42170, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 42170, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 42170, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 42170, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 42170, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 42170, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 42170, false);
			 end 

	end
	if npc_talk_index == "n739_darklair_grissini-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4217, 1);
				api_quest_SetJournalStep(userObjID,4217, 1);
				api_quest_SetQuestStep(userObjID,4217, 1);
				npc_talk_index = "n739_darklair_grissini-1";

	end
	if npc_talk_index == "n739_darklair_grissini-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 900234, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300567, 1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n741_darklair_grissini--------------------------------------------------------------------------------
function sq15_4217_part_with_pal_2_OnTalk_n741_darklair_grissini(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n741_darklair_grissini-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n741_darklair_grissini-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n741_darklair_grissini-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 42170, true);
				 api_quest_RewardQuestUser(userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 42170, true);
				 api_quest_RewardQuestUser(userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 42170, true);
				 api_quest_RewardQuestUser(userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 42170, true);
				 api_quest_RewardQuestUser(userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 42170, true);
				 api_quest_RewardQuestUser(userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 42170, true);
				 api_quest_RewardQuestUser(userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 42170, true);
				 api_quest_RewardQuestUser(userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 42170, true);
				 api_quest_RewardQuestUser(userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 42170, true);
				 api_quest_RewardQuestUser(userObjID, 42170, questID, 1);
			 end 
	end
	if npc_talk_index == "n741_darklair_grissini-3-c" then 

				if api_quest_HasQuestItem(userObjID, 300567, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300567, api_quest_HasQuestItem(userObjID, 300567, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4217_part_with_pal_2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4217);
	if qstep == 2 and CountIndex == 900234 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300567, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300567, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300567 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function sq15_4217_part_with_pal_2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4217);
	if qstep == 2 and CountIndex == 900234 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300567 and Count >= TargetCount  then

	end
end

function sq15_4217_part_with_pal_2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4217);
	local questID=4217;
end

function sq15_4217_part_with_pal_2_OnRemoteStart( userObjID, questID )
end

function sq15_4217_part_with_pal_2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4217_part_with_pal_2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4217, 1);
				api_quest_SetJournalStep(userObjID,4217, 1);
				api_quest_SetQuestStep(userObjID,4217, 1);
				npc_talk_index = "n739_darklair_grissini-1";
end

</VillageServer>

<GameServer>
function sq15_4217_part_with_pal_2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 739 then
		sq15_4217_part_with_pal_2_OnTalk_n739_darklair_grissini( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 741 then
		sq15_4217_part_with_pal_2_OnTalk_n741_darklair_grissini( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n739_darklair_grissini--------------------------------------------------------------------------------
function sq15_4217_part_with_pal_2_OnTalk_n739_darklair_grissini( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n739_darklair_grissini-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n739_darklair_grissini-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n739_darklair_grissini-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n739_darklair_grissini-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, false);
			 end 

	end
	if npc_talk_index == "n739_darklair_grissini-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4217, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4217, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4217, 1);
				npc_talk_index = "n739_darklair_grissini-1";

	end
	if npc_talk_index == "n739_darklair_grissini-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 900234, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300567, 1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n741_darklair_grissini--------------------------------------------------------------------------------
function sq15_4217_part_with_pal_2_OnTalk_n741_darklair_grissini( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n741_darklair_grissini-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n741_darklair_grissini-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n741_darklair_grissini-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 42170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 42170, questID, 1);
			 end 
	end
	if npc_talk_index == "n741_darklair_grissini-3-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300567, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300567, api_quest_HasQuestItem( pRoom, userObjID, 300567, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4217_part_with_pal_2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4217);
	if qstep == 2 and CountIndex == 900234 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300567, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300567, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300567 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function sq15_4217_part_with_pal_2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4217);
	if qstep == 2 and CountIndex == 900234 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300567 and Count >= TargetCount  then

	end
end

function sq15_4217_part_with_pal_2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4217);
	local questID=4217;
end

function sq15_4217_part_with_pal_2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4217_part_with_pal_2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4217_part_with_pal_2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4217, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4217, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4217, 1);
				npc_talk_index = "n739_darklair_grissini-1";
end

</GameServer>