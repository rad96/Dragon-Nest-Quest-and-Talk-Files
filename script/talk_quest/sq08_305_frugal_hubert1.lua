<VillageServer>

function sq08_305_frugal_hubert1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 99 then
		sq08_305_frugal_hubert1_OnTalk_n099_engineer_hubert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n099_engineer_hubert--------------------------------------------------------------------------------
function sq08_305_frugal_hubert1_OnTalk_n099_engineer_hubert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n099_engineer_hubert-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n099_engineer_hubert-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n099_engineer_hubert-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n099_engineer_hubert-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3051, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3052, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3053, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3054, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3055, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3056, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3057, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3058, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3059, false);
			 end 

	end
	if npc_talk_index == "n099_engineer_hubert-accepting-acceptted" then
				api_quest_AddQuest(userObjID,305, 1);
				api_quest_SetJournalStep(userObjID,305, 1);
				api_quest_SetQuestStep(userObjID,305, 1);
				npc_talk_index = "n099_engineer_hubert-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300241, 1);

	end
	if npc_talk_index == "n099_engineer_hubert-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3051, true);
				 api_quest_RewardQuestUser(userObjID, 3051, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3052, true);
				 api_quest_RewardQuestUser(userObjID, 3052, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3053, true);
				 api_quest_RewardQuestUser(userObjID, 3053, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3054, true);
				 api_quest_RewardQuestUser(userObjID, 3054, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3055, true);
				 api_quest_RewardQuestUser(userObjID, 3055, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3056, true);
				 api_quest_RewardQuestUser(userObjID, 3056, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3057, true);
				 api_quest_RewardQuestUser(userObjID, 3057, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3058, true);
				 api_quest_RewardQuestUser(userObjID, 3058, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3059, true);
				 api_quest_RewardQuestUser(userObjID, 3059, questID, 1);
			 end 
	end
	if npc_talk_index == "n099_engineer_hubert-2-c" then 

				if api_quest_HasQuestItem(userObjID, 300241, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300241, api_quest_HasQuestItem(userObjID, 300241, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 306, 1);
					api_quest_SetQuestStep(userObjID, 306, 1);
					api_quest_SetJournalStep(userObjID, 306, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n099_engineer_hubert-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n099_engineer_hubert-1", "sq08_306_frugal_hubert2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_305_frugal_hubert1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 305);
	if qstep == 1 and CountIndex == 300241 then

	end
end

function sq08_305_frugal_hubert1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 305);
	if qstep == 1 and CountIndex == 300241 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function sq08_305_frugal_hubert1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 305);
	local questID=305;
end

function sq08_305_frugal_hubert1_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300241, 1);
end

function sq08_305_frugal_hubert1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_305_frugal_hubert1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,305, 1);
				api_quest_SetJournalStep(userObjID,305, 1);
				api_quest_SetQuestStep(userObjID,305, 1);
				npc_talk_index = "n099_engineer_hubert-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300241, 1);
end

</VillageServer>

<GameServer>
function sq08_305_frugal_hubert1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 99 then
		sq08_305_frugal_hubert1_OnTalk_n099_engineer_hubert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n099_engineer_hubert--------------------------------------------------------------------------------
function sq08_305_frugal_hubert1_OnTalk_n099_engineer_hubert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n099_engineer_hubert-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n099_engineer_hubert-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n099_engineer_hubert-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n099_engineer_hubert-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3051, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3052, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3053, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3054, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3055, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3056, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3057, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3058, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3059, false);
			 end 

	end
	if npc_talk_index == "n099_engineer_hubert-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,305, 1);
				api_quest_SetJournalStep( pRoom, userObjID,305, 1);
				api_quest_SetQuestStep( pRoom, userObjID,305, 1);
				npc_talk_index = "n099_engineer_hubert-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300241, 1);

	end
	if npc_talk_index == "n099_engineer_hubert-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3051, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3051, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3052, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3052, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3053, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3053, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3054, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3054, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3055, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3055, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3056, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3056, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3057, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3057, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3058, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3058, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3059, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3059, questID, 1);
			 end 
	end
	if npc_talk_index == "n099_engineer_hubert-2-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300241, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300241, api_quest_HasQuestItem( pRoom, userObjID, 300241, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 306, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 306, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 306, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n099_engineer_hubert-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n099_engineer_hubert-1", "sq08_306_frugal_hubert2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_305_frugal_hubert1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 305);
	if qstep == 1 and CountIndex == 300241 then

	end
end

function sq08_305_frugal_hubert1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 305);
	if qstep == 1 and CountIndex == 300241 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function sq08_305_frugal_hubert1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 305);
	local questID=305;
end

function sq08_305_frugal_hubert1_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300241, 1);
end

function sq08_305_frugal_hubert1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_305_frugal_hubert1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,305, 1);
				api_quest_SetJournalStep( pRoom, userObjID,305, 1);
				api_quest_SetQuestStep( pRoom, userObjID,305, 1);
				npc_talk_index = "n099_engineer_hubert-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300241, 1);
end

</GameServer>