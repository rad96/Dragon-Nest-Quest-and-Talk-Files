<VillageServer>

function mq34_9281_queens_seat_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1061 then
		mq34_9281_queens_seat_OnTalk_n1061_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mq34_9281_queens_seat_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1079 then
		mq34_9281_queens_seat_OnTalk_n1079_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1080 then
		mq34_9281_queens_seat_OnTalk_n1080_elf_queen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1081 then
		mq34_9281_queens_seat_OnTalk_n1081_elf_queen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1082 then
		mq34_9281_queens_seat_OnTalk_n1082_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1061_yuvenciel--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1061_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1061_yuvenciel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1061_yuvenciel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1061_yuvenciel-1-b" then 
	end
	if npc_talk_index == "n1061_yuvenciel-1-b" then 
	end
	if npc_talk_index == "n1061_yuvenciel-1-c" then 
	end
	if npc_talk_index == "n1061_yuvenciel-1-d" then 
	end
	if npc_talk_index == "n1061_yuvenciel-1-e" then 
	end
	if npc_talk_index == "n1061_yuvenciel-1-f" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1067_elder_elf-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-party_check" then
				if api_user_IsPartymember(userObjID) == 0  then
									api_quest_AddQuest(userObjID,9281, 2);
				api_quest_SetJournalStep(userObjID,9281, 1);
				api_quest_SetQuestStep(userObjID,9281, 1);
				npc_talk_index = "n1067_elder_elf-1";

				else
				end

	end
	if npc_talk_index == "n1067_elder_elf-7-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92810, true);
				 api_quest_RewardQuestUser(userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92810, true);
				 api_quest_RewardQuestUser(userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92810, true);
				 api_quest_RewardQuestUser(userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92810, true);
				 api_quest_RewardQuestUser(userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92810, true);
				 api_quest_RewardQuestUser(userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92810, true);
				 api_quest_RewardQuestUser(userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92810, true);
				 api_quest_RewardQuestUser(userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92810, true);
				 api_quest_RewardQuestUser(userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92810, true);
				 api_quest_RewardQuestUser(userObjID, 92810, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-7-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1079_yuvenciel--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1079_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1079_yuvenciel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1079_yuvenciel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1079_yuvenciel-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1079_yuvenciel-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1079_yuvenciel-2-b" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-b" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-b" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-c" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-d" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-e" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-f" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-g" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-h" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-i" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-j" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-k" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1080_elf_queen--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1080_elf_queen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1080_elf_queen-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1080_elf_queen-3-b" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-c" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-d" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-e" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-e" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-f" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-g" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-h" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1081_elf_queen--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1081_elf_queen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1081_elf_queen-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1081_elf_queen-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1081_elf_queen-4-b" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-c" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-d" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-e" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-f" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-g" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-h" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-i" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-j" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-k" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-l" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n1081_elf_queen-5-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1082_cian--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1082_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1082_cian-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1082_cian-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1082_cian-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1082_cian-6-b" then 
	end
	if npc_talk_index == "n1082_cian-6-c" then 
	end
	if npc_talk_index == "n1082_cian-6-d" then 
	end
	if npc_talk_index == "n1082_cian-6-e" then 
	end
	if npc_talk_index == "n1082_cian-6-f" then 
	end
	if npc_talk_index == "n1082_cian-6-g" then 
	end
	if npc_talk_index == "n1082_cian-6-h" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9281_queens_seat_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9281);
end

function mq34_9281_queens_seat_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9281);
end

function mq34_9281_queens_seat_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9281);
	local questID=9281;
end

function mq34_9281_queens_seat_OnRemoteStart( userObjID, questID )
end

function mq34_9281_queens_seat_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9281_queens_seat_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mq34_9281_queens_seat_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1061 then
		mq34_9281_queens_seat_OnTalk_n1061_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mq34_9281_queens_seat_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1079 then
		mq34_9281_queens_seat_OnTalk_n1079_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1080 then
		mq34_9281_queens_seat_OnTalk_n1080_elf_queen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1081 then
		mq34_9281_queens_seat_OnTalk_n1081_elf_queen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1082 then
		mq34_9281_queens_seat_OnTalk_n1082_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1061_yuvenciel--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1061_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1061_yuvenciel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1061_yuvenciel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1061_yuvenciel-1-b" then 
	end
	if npc_talk_index == "n1061_yuvenciel-1-b" then 
	end
	if npc_talk_index == "n1061_yuvenciel-1-c" then 
	end
	if npc_talk_index == "n1061_yuvenciel-1-d" then 
	end
	if npc_talk_index == "n1061_yuvenciel-1-e" then 
	end
	if npc_talk_index == "n1061_yuvenciel-1-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1067_elder_elf-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-party_check" then
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_quest_AddQuest( pRoom, userObjID,9281, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9281, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9281, 1);
				npc_talk_index = "n1067_elder_elf-1";

				else
				end

	end
	if npc_talk_index == "n1067_elder_elf-7-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92810, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92810, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92810, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-7-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1079_yuvenciel--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1079_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1079_yuvenciel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1079_yuvenciel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1079_yuvenciel-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1079_yuvenciel-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1079_yuvenciel-2-b" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-b" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-b" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-c" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-d" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-e" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-f" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-g" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-h" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-i" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-j" then 
	end
	if npc_talk_index == "n1079_yuvenciel-5-k" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1080_elf_queen--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1080_elf_queen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1080_elf_queen-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1080_elf_queen-3-b" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-c" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-d" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-e" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-e" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-f" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-g" then 
	end
	if npc_talk_index == "n1080_elf_queen-3-h" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1081_elf_queen--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1081_elf_queen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1081_elf_queen-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1081_elf_queen-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1081_elf_queen-4-b" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-c" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-d" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-e" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-f" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-g" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-h" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-i" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-j" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-k" then 
	end
	if npc_talk_index == "n1081_elf_queen-4-l" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n1081_elf_queen-5-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1082_cian--------------------------------------------------------------------------------
function mq34_9281_queens_seat_OnTalk_n1082_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1082_cian-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1082_cian-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1082_cian-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1082_cian-6-b" then 
	end
	if npc_talk_index == "n1082_cian-6-c" then 
	end
	if npc_talk_index == "n1082_cian-6-d" then 
	end
	if npc_talk_index == "n1082_cian-6-e" then 
	end
	if npc_talk_index == "n1082_cian-6-f" then 
	end
	if npc_talk_index == "n1082_cian-6-g" then 
	end
	if npc_talk_index == "n1082_cian-6-h" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9281_queens_seat_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9281);
end

function mq34_9281_queens_seat_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9281);
end

function mq34_9281_queens_seat_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9281);
	local questID=9281;
end

function mq34_9281_queens_seat_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9281_queens_seat_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9281_queens_seat_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>