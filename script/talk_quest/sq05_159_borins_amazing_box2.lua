<VillageServer>

function sq05_159_borins_amazing_box2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 16 then
		sq05_159_borins_amazing_box2_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 17 then
		sq05_159_borins_amazing_box2_OnTalk_n017_trader_jeny(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function sq05_159_borins_amazing_box2_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n016_trader_borin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n016_trader_borin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n016_trader_borin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1590, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1590, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1590, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1597, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1597, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1590, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1590, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1590, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1590, false);
			 end 

	end
	if npc_talk_index == "n016_trader_borin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,159, 1);
				api_quest_SetJournalStep(userObjID,159, 1);
				api_quest_SetQuestStep(userObjID,159, 1);
				npc_talk_index = "n016_trader_borin-1";

	end
	if npc_talk_index == "n016_trader_borin-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				if api_user_CheckInvenForAddItem(userObjID, 300152, 1) == 1 then
					api_user_AddItem(userObjID, 300152, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n016_trader_borin-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n016_trader_borin-4-b" then 
	end
	if npc_talk_index == "n016_trader_borin-4-c" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									if api_user_HasItem(userObjID, 543754241, 1) >= 1 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 end 

				else
									if api_user_HasItem(userObjID, 544278529, 1) >= 1 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 end 

				else
									if api_user_HasItem(userObjID, 544802817, 1) >= 1 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1590, true);
				 api_quest_RewardQuestUser(userObjID, 1590, questID, 1);
			 end 

				end

				end

				end

				else
									if api_user_HasItem(userObjID, 541132801, 1) >= 1 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 end 

				else
									if api_user_HasItem(userObjID, 541657089, 1) >= 1 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 end 

				else
									if api_user_HasItem(userObjID, 542181377, 1) >= 1 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1597, true);
				 api_quest_RewardQuestUser(userObjID, 1597, questID, 1);
			 end 

				end

				end

				end

				end
	end
	if npc_talk_index == "n016_trader_borin-4-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_user_HasItem(userObjID, 300159, 1) > 0 then 
					api_user_DelItem(userObjID, 300159, api_user_HasItem(userObjID, 300159, 1, questID));
				end

				if api_user_HasItem(userObjID, 300160, 1) > 0 then 
					api_user_DelItem(userObjID, 300160, api_user_HasItem(userObjID, 300160, 1, questID));
				end

				if api_user_HasItem(userObjID, 300161, 1) > 0 then 
					api_user_DelItem(userObjID, 300161, api_user_HasItem(userObjID, 300161, 1, questID));
				end

				if api_user_HasItem(userObjID, 300162, 1) > 0 then 
					api_user_DelItem(userObjID, 300162, api_user_HasItem(userObjID, 300162, 1, questID));
				end

				if api_user_HasItem(userObjID, 300163, 1) > 0 then 
					api_user_DelItem(userObjID, 300163, api_user_HasItem(userObjID, 300163, 1, questID));
				end

				if api_user_HasItem(userObjID, 300164, 1) > 0 then 
					api_user_DelItem(userObjID, 300164, api_user_HasItem(userObjID, 300164, 1, questID));
				end

				if api_user_HasItem(userObjID, 543754241, 1) > 0 then 
					api_user_DelItem(userObjID, 543754241, api_user_HasItem(userObjID, 543754241, 1, questID));
				end

				if api_user_HasItem(userObjID, 544278529, 1) > 0 then 
					api_user_DelItem(userObjID, 544278529, api_user_HasItem(userObjID, 544278529, 1, questID));
				end

				if api_user_HasItem(userObjID, 544802817, 1) > 0 then 
					api_user_DelItem(userObjID, 544802817, api_user_HasItem(userObjID, 544802817, 1, questID));
				end

				if api_user_HasItem(userObjID, 541132801, 1) > 0 then 
					api_user_DelItem(userObjID, 541132801, api_user_HasItem(userObjID, 541132801, 1, questID));
				end

				if api_user_HasItem(userObjID, 541657089, 1) > 0 then 
					api_user_DelItem(userObjID, 541657089, api_user_HasItem(userObjID, 541657089, 1, questID));
				end

				if api_user_HasItem(userObjID, 542181377, 1) > 0 then 
					api_user_DelItem(userObjID, 542181377, api_user_HasItem(userObjID, 542181377, 1, questID));
				end

				if api_user_HasItem(userObjID, 300152 , 1) > 0 then 
					api_user_DelItem(userObjID, 300152 , api_user_HasItem(userObjID, 300152 , 1, questID));
				end
	end
	if npc_talk_index == "n016_trader_borin-4-opencompounditem2" then 
				api_ui_OpenShop(userObjID,25,100);
	end
	if npc_talk_index == "n016_trader_borin-4-a" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n017_trader_jeny--------------------------------------------------------------------------------
function sq05_159_borins_amazing_box2_OnTalk_n017_trader_jeny(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n017_trader_jeny-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n017_trader_jeny-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n017_trader_jeny-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n017_trader_jeny-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n017_trader_jeny-2-b" then 
	end
	if npc_talk_index == "n017_trader_jeny-2-c" then 
	end
	if npc_talk_index == "n017_trader_jeny-3" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									 local TableItem = { 				 { 300159, 1 }  , 
				 { 300160, 1 }  , 
				 { 300161, 1 }  
 				} 
				if api_user_CheckInvenForAddItemList(userObjID, TableItem) == 1 then
					api_user_AddItem(userObjID, 300159, 1, questID);
					api_user_AddItem(userObjID, 300160, 1, questID);
					api_user_AddItem(userObjID, 300161, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									 local TableItem = { 				 { 300162, 1 }  , 
				 { 300163, 1 }  , 
				 { 300164, 1 }  
 				} 
				if api_user_CheckInvenForAddItemList(userObjID, TableItem) == 1 then
					api_user_AddItem(userObjID, 300162, 1, questID);
					api_user_AddItem(userObjID, 300163, 1, questID);
					api_user_AddItem(userObjID, 300164, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_159_borins_amazing_box2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 159);
end

function sq05_159_borins_amazing_box2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 159);
end

function sq05_159_borins_amazing_box2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 159);
	local questID=159;
end

function sq05_159_borins_amazing_box2_OnRemoteStart( userObjID, questID )
end

function sq05_159_borins_amazing_box2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_159_borins_amazing_box2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,159, 1);
				api_quest_SetJournalStep(userObjID,159, 1);
				api_quest_SetQuestStep(userObjID,159, 1);
				npc_talk_index = "n016_trader_borin-1";
end

</VillageServer>

<GameServer>
function sq05_159_borins_amazing_box2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 16 then
		sq05_159_borins_amazing_box2_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 17 then
		sq05_159_borins_amazing_box2_OnTalk_n017_trader_jeny( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function sq05_159_borins_amazing_box2_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n016_trader_borin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n016_trader_borin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n016_trader_borin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, false);
			 end 

	end
	if npc_talk_index == "n016_trader_borin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,159, 1);
				api_quest_SetJournalStep( pRoom, userObjID,159, 1);
				api_quest_SetQuestStep( pRoom, userObjID,159, 1);
				npc_talk_index = "n016_trader_borin-1";

	end
	if npc_talk_index == "n016_trader_borin-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				if api_user_CheckInvenForAddItem( pRoom, userObjID, 300152, 1) == 1 then
					api_user_AddItem( pRoom, userObjID, 300152, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n016_trader_borin-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n016_trader_borin-4-b" then 
	end
	if npc_talk_index == "n016_trader_borin-4-c" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									if api_user_HasItem( pRoom, userObjID, 543754241, 1) >= 1 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 end 

				else
									if api_user_HasItem( pRoom, userObjID, 544278529, 1) >= 1 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 end 

				else
									if api_user_HasItem( pRoom, userObjID, 544802817, 1) >= 1 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1590, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1590, questID, 1);
			 end 

				end

				end

				end

				else
									if api_user_HasItem( pRoom, userObjID, 541132801, 1) >= 1 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 end 

				else
									if api_user_HasItem( pRoom, userObjID, 541657089, 1) >= 1 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 end 

				else
									if api_user_HasItem( pRoom, userObjID, 542181377, 1) >= 1 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1597, questID, 1);
			 end 

				end

				end

				end

				end
	end
	if npc_talk_index == "n016_trader_borin-4-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_user_HasItem( pRoom, userObjID, 300159, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300159, api_user_HasItem( pRoom, userObjID, 300159, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300160, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300160, api_user_HasItem( pRoom, userObjID, 300160, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300161, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300161, api_user_HasItem( pRoom, userObjID, 300161, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300162, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300162, api_user_HasItem( pRoom, userObjID, 300162, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300163, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300163, api_user_HasItem( pRoom, userObjID, 300163, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300164, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300164, api_user_HasItem( pRoom, userObjID, 300164, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 543754241, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 543754241, api_user_HasItem( pRoom, userObjID, 543754241, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 544278529, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 544278529, api_user_HasItem( pRoom, userObjID, 544278529, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 544802817, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 544802817, api_user_HasItem( pRoom, userObjID, 544802817, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 541132801, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 541132801, api_user_HasItem( pRoom, userObjID, 541132801, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 541657089, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 541657089, api_user_HasItem( pRoom, userObjID, 541657089, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 542181377, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 542181377, api_user_HasItem( pRoom, userObjID, 542181377, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300152 , 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300152 , api_user_HasItem( pRoom, userObjID, 300152 , 1, questID));
				end
	end
	if npc_talk_index == "n016_trader_borin-4-opencompounditem2" then 
				api_ui_OpenShop( pRoom, userObjID,25,100);
	end
	if npc_talk_index == "n016_trader_borin-4-a" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n017_trader_jeny--------------------------------------------------------------------------------
function sq05_159_borins_amazing_box2_OnTalk_n017_trader_jeny( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n017_trader_jeny-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n017_trader_jeny-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n017_trader_jeny-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n017_trader_jeny-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n017_trader_jeny-2-b" then 
	end
	if npc_talk_index == "n017_trader_jeny-2-c" then 
	end
	if npc_talk_index == "n017_trader_jeny-3" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									 local TableItem = { 				 { 300159, 1 }  , 
				 { 300160, 1 }  , 
				 { 300161, 1 }  
 				} 
				if api_user_CheckInvenForAddItemList( pRoom, userObjID, TableItem) == 1 then
					api_user_AddItem( pRoom, userObjID, 300159, 1, questID);
					api_user_AddItem( pRoom, userObjID, 300160, 1, questID);
					api_user_AddItem( pRoom, userObjID, 300161, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									 local TableItem = { 				 { 300162, 1 }  , 
				 { 300163, 1 }  , 
				 { 300164, 1 }  
 				} 
				if api_user_CheckInvenForAddItemList( pRoom, userObjID, TableItem) == 1 then
					api_user_AddItem( pRoom, userObjID, 300162, 1, questID);
					api_user_AddItem( pRoom, userObjID, 300163, 1, questID);
					api_user_AddItem( pRoom, userObjID, 300164, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_159_borins_amazing_box2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 159);
end

function sq05_159_borins_amazing_box2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 159);
end

function sq05_159_borins_amazing_box2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 159);
	local questID=159;
end

function sq05_159_borins_amazing_box2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq05_159_borins_amazing_box2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_159_borins_amazing_box2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,159, 1);
				api_quest_SetJournalStep( pRoom, userObjID,159, 1);
				api_quest_SetQuestStep( pRoom, userObjID,159, 1);
				npc_talk_index = "n016_trader_borin-1";
end

</GameServer>