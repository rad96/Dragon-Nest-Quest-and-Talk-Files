<VillageServer>

function sqc16_4695_red_dragon_nest_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 999 then
		sqc16_4695_red_dragon_nest_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		sqc16_4695_red_dragon_nest_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1838 then
		sqc16_4695_red_dragon_nest_OnTalk_n1838_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1839 then
		sqc16_4695_red_dragon_nest_OnTalk_n1839_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1840 then
		sqc16_4695_red_dragon_nest_OnTalk_n1840_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n999_remote--------------------------------------------------------------------------------
function sqc16_4695_red_dragon_nest_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n999_remote-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n999_remote-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4695, 1);
				api_quest_SetJournalStep(userObjID,4695, 1);
				api_quest_SetQuestStep(userObjID,4695, 1);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function sqc16_4695_red_dragon_nest_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n776_darklair_mardlen-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-1-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				npc_talk_index = "n776_darklair_mardlen-5";
	end
	if npc_talk_index == "n776_darklair_mardlen-1-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-g" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-h" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-i" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-j" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-j" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-k" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-l" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-m" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-n" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-n" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-o" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-o" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-p" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-q" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-r" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n776_darklair_mardlen-5-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-5-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-5-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-5-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-5-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46950, true);
				 api_quest_RewardQuestUser(userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46950, true);
				 api_quest_RewardQuestUser(userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46950, true);
				 api_quest_RewardQuestUser(userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46950, true);
				 api_quest_RewardQuestUser(userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46950, true);
				 api_quest_RewardQuestUser(userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46950, true);
				 api_quest_RewardQuestUser(userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46950, true);
				 api_quest_RewardQuestUser(userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46950, true);
				 api_quest_RewardQuestUser(userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46950, true);
				 api_quest_RewardQuestUser(userObjID, 46950, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-5-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-5-h" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1838_rubinat--------------------------------------------------------------------------------
function sqc16_4695_red_dragon_nest_OnTalk_n1838_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1838_rubinat-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1838_rubinat-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1838_rubinat-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1838_rubinat-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1838_rubinat-4-b" then 
	end
	if npc_talk_index == "n1838_rubinat-4-c" then 
	end
	if npc_talk_index == "n1838_rubinat-4-d" then 
	end
	if npc_talk_index == "n1838_rubinat-4-e" then 
	end
	if npc_talk_index == "n1838_rubinat-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1839_gereint_kid--------------------------------------------------------------------------------
function sqc16_4695_red_dragon_nest_OnTalk_n1839_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1839_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1839_gereint_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1839_gereint_kid-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1839_gereint_kid-2-b" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-c" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-d" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-f" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-f" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-g" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-h" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-i" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-j" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-j" then 
	end
	if npc_talk_index == "n1839_gereint_kid-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1840_velskud--------------------------------------------------------------------------------
function sqc16_4695_red_dragon_nest_OnTalk_n1840_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1840_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1840_velskud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1840_velskud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1840_velskud-3-b" then 
	end
	if npc_talk_index == "n1840_velskud-3-c" then 
	end
	if npc_talk_index == "n1840_velskud-3-c" then 
	end
	if npc_talk_index == "n1840_velskud-3-d" then 
	end
	if npc_talk_index == "n1840_velskud-3-e" then 
	end
	if npc_talk_index == "n1840_velskud-3-f" then 
	end
	if npc_talk_index == "n1840_velskud-3-g" then 
	end
	if npc_talk_index == "n1840_velskud-3-h" then 
	end
	if npc_talk_index == "n1840_velskud-3-h" then 
	end
	if npc_talk_index == "n1840_velskud-3-h" then 
	end
	if npc_talk_index == "n1840_velskud-3-i" then 
	end
	if npc_talk_index == "n1840_velskud-3-j" then 
	end
	if npc_talk_index == "n1840_velskud-3-k" then 
	end
	if npc_talk_index == "n1840_velskud-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4695_red_dragon_nest_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4695);
end

function sqc16_4695_red_dragon_nest_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4695);
end

function sqc16_4695_red_dragon_nest_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4695);
	local questID=4695;
end

function sqc16_4695_red_dragon_nest_OnRemoteStart( userObjID, questID )
end

function sqc16_4695_red_dragon_nest_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc16_4695_red_dragon_nest_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4695, 1);
				api_quest_SetJournalStep(userObjID,4695, 1);
				api_quest_SetQuestStep(userObjID,4695, 1);
end

</VillageServer>

<GameServer>
function sqc16_4695_red_dragon_nest_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 999 then
		sqc16_4695_red_dragon_nest_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		sqc16_4695_red_dragon_nest_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1838 then
		sqc16_4695_red_dragon_nest_OnTalk_n1838_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1839 then
		sqc16_4695_red_dragon_nest_OnTalk_n1839_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1840 then
		sqc16_4695_red_dragon_nest_OnTalk_n1840_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n999_remote--------------------------------------------------------------------------------
function sqc16_4695_red_dragon_nest_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n999_remote-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n999_remote-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4695, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4695, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4695, 1);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function sqc16_4695_red_dragon_nest_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n776_darklair_mardlen-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-1-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				npc_talk_index = "n776_darklair_mardlen-5";
	end
	if npc_talk_index == "n776_darklair_mardlen-1-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-g" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-h" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-i" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-j" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-j" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-k" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-l" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-m" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-n" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-n" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-o" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-o" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-p" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-q" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-r" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n776_darklair_mardlen-5-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-5-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-5-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-5-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-5-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46950, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-5-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-5-h" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1838_rubinat--------------------------------------------------------------------------------
function sqc16_4695_red_dragon_nest_OnTalk_n1838_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1838_rubinat-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1838_rubinat-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1838_rubinat-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1838_rubinat-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1838_rubinat-4-b" then 
	end
	if npc_talk_index == "n1838_rubinat-4-c" then 
	end
	if npc_talk_index == "n1838_rubinat-4-d" then 
	end
	if npc_talk_index == "n1838_rubinat-4-e" then 
	end
	if npc_talk_index == "n1838_rubinat-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1839_gereint_kid--------------------------------------------------------------------------------
function sqc16_4695_red_dragon_nest_OnTalk_n1839_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1839_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1839_gereint_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1839_gereint_kid-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1839_gereint_kid-2-b" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-c" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-d" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-f" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-f" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-g" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-h" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-i" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-j" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-j" then 
	end
	if npc_talk_index == "n1839_gereint_kid-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1840_velskud--------------------------------------------------------------------------------
function sqc16_4695_red_dragon_nest_OnTalk_n1840_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1840_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1840_velskud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1840_velskud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1840_velskud-3-b" then 
	end
	if npc_talk_index == "n1840_velskud-3-c" then 
	end
	if npc_talk_index == "n1840_velskud-3-c" then 
	end
	if npc_talk_index == "n1840_velskud-3-d" then 
	end
	if npc_talk_index == "n1840_velskud-3-e" then 
	end
	if npc_talk_index == "n1840_velskud-3-f" then 
	end
	if npc_talk_index == "n1840_velskud-3-g" then 
	end
	if npc_talk_index == "n1840_velskud-3-h" then 
	end
	if npc_talk_index == "n1840_velskud-3-h" then 
	end
	if npc_talk_index == "n1840_velskud-3-h" then 
	end
	if npc_talk_index == "n1840_velskud-3-i" then 
	end
	if npc_talk_index == "n1840_velskud-3-j" then 
	end
	if npc_talk_index == "n1840_velskud-3-k" then 
	end
	if npc_talk_index == "n1840_velskud-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4695_red_dragon_nest_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4695);
end

function sqc16_4695_red_dragon_nest_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4695);
end

function sqc16_4695_red_dragon_nest_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4695);
	local questID=4695;
end

function sqc16_4695_red_dragon_nest_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc16_4695_red_dragon_nest_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc16_4695_red_dragon_nest_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4695, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4695, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4695, 1);
end

</GameServer>