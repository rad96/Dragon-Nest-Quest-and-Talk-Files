<VillageServer>

function sq15_882_tears_of_goddess_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 791 then
		sq15_882_tears_of_goddess_OnTalk_n791_bluff_dealers_chief(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n791_bluff_dealers_chief--------------------------------------------------------------------------------
function sq15_882_tears_of_goddess_OnTalk_n791_bluff_dealers_chief(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n791_bluff_dealers_chief-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n791_bluff_dealers_chief-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n791_bluff_dealers_chief-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n791_bluff_dealers_chief-accepting-g" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8820, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8820, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8820, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8820, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8820, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8820, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8820, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8820, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8820, false);
			 end 

	end
	if npc_talk_index == "n791_bluff_dealers_chief-accepting-acceptted" then
				api_quest_AddQuest(userObjID,882, 1);
				api_quest_SetJournalStep(userObjID,882, 1);
				api_quest_SetQuestStep(userObjID,882, 1);
				npc_talk_index = "n791_bluff_dealers_chief-1";

	end
	if npc_talk_index == "n791_bluff_dealers_chief-1-b" then 
	end
	if npc_talk_index == "n791_bluff_dealers_chief-1-c" then 
	end
	if npc_talk_index == "n791_bluff_dealers_chief-1-d" then 
	end
	if npc_talk_index == "n791_bluff_dealers_chief-1-open_shop" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				if api_user_GetUserClassID(userObjID) == 1 then
									api_ui_OpenShop(userObjID,14051,100);

				else
									if api_user_GetUserClassID(userObjID) == 2 then
									api_ui_OpenShop(userObjID,14052,100);

				else
									if api_user_GetUserClassID(userObjID) == 3 then
									api_ui_OpenShop(userObjID,14053,100);

				else
									if api_user_GetUserClassID(userObjID) == 4 then
									api_ui_OpenShop(userObjID,14054,100);

				else
									if api_user_GetUserClassID(userObjID) == 5 then
									api_ui_OpenShop(userObjID,14055,100);

				else
									if api_user_GetUserClassID(userObjID) == 6 then
									api_ui_OpenShop(userObjID,14056,100);

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									api_ui_OpenShop(userObjID,14057,100);

				else
									if api_user_GetUserClassID(userObjID) == 8 then
									api_ui_OpenShop(userObjID,14058,100);

				else
									api_ui_OpenShop(userObjID,14059,100);

				end

				end

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n791_bluff_dealers_chief-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8820, true);
				 api_quest_RewardQuestUser(userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8820, true);
				 api_quest_RewardQuestUser(userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8820, true);
				 api_quest_RewardQuestUser(userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8820, true);
				 api_quest_RewardQuestUser(userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8820, true);
				 api_quest_RewardQuestUser(userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8820, true);
				 api_quest_RewardQuestUser(userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8820, true);
				 api_quest_RewardQuestUser(userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8820, true);
				 api_quest_RewardQuestUser(userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8820, true);
				 api_quest_RewardQuestUser(userObjID, 8820, questID, 1);
			 end 
	end
	if npc_talk_index == "n791_bluff_dealers_chief-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_882_tears_of_goddess_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 882);
end

function sq15_882_tears_of_goddess_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 882);
end

function sq15_882_tears_of_goddess_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 882);
	local questID=882;
end

function sq15_882_tears_of_goddess_OnRemoteStart( userObjID, questID )
end

function sq15_882_tears_of_goddess_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_882_tears_of_goddess_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,882, 1);
				api_quest_SetJournalStep(userObjID,882, 1);
				api_quest_SetQuestStep(userObjID,882, 1);
				npc_talk_index = "n791_bluff_dealers_chief-1";
end

</VillageServer>

<GameServer>
function sq15_882_tears_of_goddess_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 791 then
		sq15_882_tears_of_goddess_OnTalk_n791_bluff_dealers_chief( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n791_bluff_dealers_chief--------------------------------------------------------------------------------
function sq15_882_tears_of_goddess_OnTalk_n791_bluff_dealers_chief( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n791_bluff_dealers_chief-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n791_bluff_dealers_chief-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n791_bluff_dealers_chief-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n791_bluff_dealers_chief-accepting-g" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, false);
			 end 

	end
	if npc_talk_index == "n791_bluff_dealers_chief-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,882, 1);
				api_quest_SetJournalStep( pRoom, userObjID,882, 1);
				api_quest_SetQuestStep( pRoom, userObjID,882, 1);
				npc_talk_index = "n791_bluff_dealers_chief-1";

	end
	if npc_talk_index == "n791_bluff_dealers_chief-1-b" then 
	end
	if npc_talk_index == "n791_bluff_dealers_chief-1-c" then 
	end
	if npc_talk_index == "n791_bluff_dealers_chief-1-d" then 
	end
	if npc_talk_index == "n791_bluff_dealers_chief-1-open_shop" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									api_ui_OpenShop( pRoom, userObjID,14051,100);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									api_ui_OpenShop( pRoom, userObjID,14052,100);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									api_ui_OpenShop( pRoom, userObjID,14053,100);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									api_ui_OpenShop( pRoom, userObjID,14054,100);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									api_ui_OpenShop( pRoom, userObjID,14055,100);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									api_ui_OpenShop( pRoom, userObjID,14056,100);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									api_ui_OpenShop( pRoom, userObjID,14057,100);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 8 then
									api_ui_OpenShop( pRoom, userObjID,14058,100);

				else
									api_ui_OpenShop( pRoom, userObjID,14059,100);

				end

				end

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n791_bluff_dealers_chief-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8820, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8820, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8820, questID, 1);
			 end 
	end
	if npc_talk_index == "n791_bluff_dealers_chief-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_882_tears_of_goddess_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 882);
end

function sq15_882_tears_of_goddess_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 882);
end

function sq15_882_tears_of_goddess_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 882);
	local questID=882;
end

function sq15_882_tears_of_goddess_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_882_tears_of_goddess_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_882_tears_of_goddess_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,882, 1);
				api_quest_SetJournalStep( pRoom, userObjID,882, 1);
				api_quest_SetQuestStep( pRoom, userObjID,882, 1);
				npc_talk_index = "n791_bluff_dealers_chief-1";
end

</GameServer>