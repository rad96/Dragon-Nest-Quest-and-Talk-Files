<VillageServer>

function sq15_763_truly_power_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 475 then
		sq15_763_truly_power_OnTalk_n475_great_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 476 then
		sq15_763_truly_power_OnTalk_n476_the_light(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		sq15_763_truly_power_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 713 then
		sq15_763_truly_power_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n475_great_book--------------------------------------------------------------------------------
function sq15_763_truly_power_OnTalk_n475_great_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n475_great_book-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n475_great_book-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n475_great_book-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n475_great_book-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n475_great_book-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n475_great_book-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n475_great_book-1-b" then 
	end
	if npc_talk_index == "n475_great_book-1-check_job" then 
				if api_user_GetUserJobID(userObjID) == 17  then
									npc_talk_index = "n475_great_book-1-c";

				else
									npc_talk_index = "n475_great_book-1-g";

				end
	end
	if npc_talk_index == "n475_great_book-1-d" then 
	end
	if npc_talk_index == "n475_great_book-1-e" then 
	end
	if npc_talk_index == "n475_great_book-1-f" then 
	end
	if npc_talk_index == "n475_great_book-1-f" then 
	end
	if npc_talk_index == "n475_great_book-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n475_great_book-1-c" then 
	end
	if npc_talk_index == "n475_great_book-1-h" then 
	end
	if npc_talk_index == "n475_great_book-1-i" then 
	end
	if npc_talk_index == "n475_great_book-1-j" then 
	end
	if npc_talk_index == "n475_great_book-1-j" then 
	end
	if npc_talk_index == "n475_great_book-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n475_great_book-1-g" then 
	end
	if npc_talk_index == "n475_great_book-2-open_ui2" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n475_great_book-2-set_ba1" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n475_great_book-3";
				api_user_SetSecondJobSkill( userObjID, 35 );
	end
	if npc_talk_index == "n475_great_book-2-set_ba2" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n475_great_book-3";
				api_user_SetSecondJobSkill( userObjID, 36 );
	end
	if npc_talk_index == "n475_great_book-2-set_battle_3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n475_great_book-3";
				api_user_SetSecondJobSkill( userObjID, 37 );
	end
	if npc_talk_index == "n475_great_book-2-set_battle_4" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n475_great_book-3";
				api_user_SetSecondJobSkill( userObjID, 38 );
	end
	if npc_talk_index == "n475_great_book-3-open_ui3" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n475_great_book-4-open_ui4" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n475_great_book-4-open_ui4_1" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n475_great_book-4-b" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n475_great_book-5-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n476_the_light--------------------------------------------------------------------------------
function sq15_763_truly_power_OnTalk_n476_the_light(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n476_the_light-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n476_the_light-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n476_the_light-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n476_the_light-1-check1" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n476_the_light-1-b";

				else
									npc_talk_index = "n476_the_light-1-a";

				end
	end
	if npc_talk_index == "n476_the_light-1-enter_room1" then 
				api_user_ChangeMap(userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function sq15_763_truly_power_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n701_oldkarakule-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n713_soceress_tamara--------------------------------------------------------------------------------
function sq15_763_truly_power_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n713_soceress_tamara-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n713_soceress_tamara-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-a" then
				if api_user_GetUserJobID(userObjID) == 35   or api_user_GetUserJobID(userObjID) == 36   or api_user_GetUserJobID(userObjID) == 37   or api_user_GetUserJobID(userObjID) == 38  then
									npc_talk_index = "n713_soceress_tamara-accepting-d";

				else
									npc_talk_index = "n713_soceress_tamara-accepting-a";

				end

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 end 

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted" then
				api_quest_AddQuest(userObjID,763, 1);
				api_quest_SetJournalStep(userObjID,763, 1);
				api_quest_SetQuestStep(userObjID,763, 1);
				npc_talk_index = "n713_soceress_tamara-1";

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7630, false);
			 end 

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,763, 1);
				api_quest_SetJournalStep(userObjID,763, 1);
				api_quest_SetQuestStep(userObjID,763, 1);
				npc_talk_index = "n713_soceress_tamara-5";
				api_quest_SetJournalStep(userObjID, questID, 5);
				api_quest_SetQuestStep(userObjID, questID,5);

	end
	if npc_talk_index == "n713_soceress_tamara-5-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-5-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-5-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-5-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-5-a" then 
	end
	if npc_talk_index == "n713_soceress_tamara-5-set_job1" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n713_soceress_tamara-5-g";
				api_user_SetUserJobID(userObjID, 35);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n713_soceress_tamara-5-a" then 
	end
	if npc_talk_index == "n713_soceress_tamara-5-set_job2" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n713_soceress_tamara-5-g";
				api_user_SetUserJobID(userObjID, 36);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n713_soceress_tamara-5-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-5-set_job3" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n713_soceress_tamara-5-g";
				api_user_SetUserJobID(userObjID, 37);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n713_soceress_tamara-5-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-5-set_job4" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n713_soceress_tamara-5-g";
				api_user_SetUserJobID(userObjID, 38);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n713_soceress_tamara-5-h" then 
	end
	if npc_talk_index == "n713_soceress_tamara-5-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7630, true);
				 api_quest_RewardQuestUser(userObjID, 7630, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-5-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_763_truly_power_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 763);
end

function sq15_763_truly_power_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 763);
end

function sq15_763_truly_power_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 763);
	local questID=763;
end

function sq15_763_truly_power_OnRemoteStart( userObjID, questID )
end

function sq15_763_truly_power_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_763_truly_power_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,763, 1);
				api_quest_SetJournalStep(userObjID,763, 1);
				api_quest_SetQuestStep(userObjID,763, 1);
				npc_talk_index = "n713_soceress_tamara-1";
end

</VillageServer>

<GameServer>
function sq15_763_truly_power_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 475 then
		sq15_763_truly_power_OnTalk_n475_great_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 476 then
		sq15_763_truly_power_OnTalk_n476_the_light( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		sq15_763_truly_power_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 713 then
		sq15_763_truly_power_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n475_great_book--------------------------------------------------------------------------------
function sq15_763_truly_power_OnTalk_n475_great_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n475_great_book-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n475_great_book-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n475_great_book-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n475_great_book-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n475_great_book-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n475_great_book-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n475_great_book-1-b" then 
	end
	if npc_talk_index == "n475_great_book-1-check_job" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 17  then
									npc_talk_index = "n475_great_book-1-c";

				else
									npc_talk_index = "n475_great_book-1-g";

				end
	end
	if npc_talk_index == "n475_great_book-1-d" then 
	end
	if npc_talk_index == "n475_great_book-1-e" then 
	end
	if npc_talk_index == "n475_great_book-1-f" then 
	end
	if npc_talk_index == "n475_great_book-1-f" then 
	end
	if npc_talk_index == "n475_great_book-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n475_great_book-1-c" then 
	end
	if npc_talk_index == "n475_great_book-1-h" then 
	end
	if npc_talk_index == "n475_great_book-1-i" then 
	end
	if npc_talk_index == "n475_great_book-1-j" then 
	end
	if npc_talk_index == "n475_great_book-1-j" then 
	end
	if npc_talk_index == "n475_great_book-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n475_great_book-1-g" then 
	end
	if npc_talk_index == "n475_great_book-2-open_ui2" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n475_great_book-2-set_ba1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n475_great_book-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 35 );
	end
	if npc_talk_index == "n475_great_book-2-set_ba2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n475_great_book-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 36 );
	end
	if npc_talk_index == "n475_great_book-2-set_battle_3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n475_great_book-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 37 );
	end
	if npc_talk_index == "n475_great_book-2-set_battle_4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n475_great_book-3";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 38 );
	end
	if npc_talk_index == "n475_great_book-3-open_ui3" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n475_great_book-4-open_ui4" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n475_great_book-4-open_ui4_1" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n475_great_book-4-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n475_great_book-5-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n476_the_light--------------------------------------------------------------------------------
function sq15_763_truly_power_OnTalk_n476_the_light( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n476_the_light-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n476_the_light-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n476_the_light-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n476_the_light-1-check1" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n476_the_light-1-b";

				else
									npc_talk_index = "n476_the_light-1-a";

				end
	end
	if npc_talk_index == "n476_the_light-1-enter_room1" then 
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function sq15_763_truly_power_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n701_oldkarakule-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n713_soceress_tamara--------------------------------------------------------------------------------
function sq15_763_truly_power_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n713_soceress_tamara-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n713_soceress_tamara-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-a" then
				if api_user_GetUserJobID( pRoom, userObjID) == 35   or api_user_GetUserJobID( pRoom, userObjID) == 36   or api_user_GetUserJobID( pRoom, userObjID) == 37   or api_user_GetUserJobID( pRoom, userObjID) == 38  then
									npc_talk_index = "n713_soceress_tamara-accepting-d";

				else
									npc_talk_index = "n713_soceress_tamara-accepting-a";

				end

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 end 

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,763, 1);
				api_quest_SetJournalStep( pRoom, userObjID,763, 1);
				api_quest_SetQuestStep( pRoom, userObjID,763, 1);
				npc_talk_index = "n713_soceress_tamara-1";

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, false);
			 end 

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,763, 1);
				api_quest_SetJournalStep( pRoom, userObjID,763, 1);
				api_quest_SetQuestStep( pRoom, userObjID,763, 1);
				npc_talk_index = "n713_soceress_tamara-5";
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);

	end
	if npc_talk_index == "n713_soceress_tamara-5-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-5-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-5-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-5-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-5-a" then 
	end
	if npc_talk_index == "n713_soceress_tamara-5-set_job1" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n713_soceress_tamara-5-g";
				api_user_SetUserJobID( pRoom, userObjID, 35);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n713_soceress_tamara-5-a" then 
	end
	if npc_talk_index == "n713_soceress_tamara-5-set_job2" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n713_soceress_tamara-5-g";
				api_user_SetUserJobID( pRoom, userObjID, 36);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n713_soceress_tamara-5-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-5-set_job3" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n713_soceress_tamara-5-g";
				api_user_SetUserJobID( pRoom, userObjID, 37);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n713_soceress_tamara-5-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-5-set_job4" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n713_soceress_tamara-5-g";
				api_user_SetUserJobID( pRoom, userObjID, 38);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n713_soceress_tamara-5-h" then 
	end
	if npc_talk_index == "n713_soceress_tamara-5-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7630, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7630, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-5-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_763_truly_power_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 763);
end

function sq15_763_truly_power_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 763);
end

function sq15_763_truly_power_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 763);
	local questID=763;
end

function sq15_763_truly_power_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_763_truly_power_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_763_truly_power_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,763, 1);
				api_quest_SetJournalStep( pRoom, userObjID,763, 1);
				api_quest_SetQuestStep( pRoom, userObjID,763, 1);
				npc_talk_index = "n713_soceress_tamara-1";
end

</GameServer>