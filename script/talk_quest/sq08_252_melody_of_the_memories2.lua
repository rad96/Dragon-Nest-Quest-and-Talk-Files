<VillageServer>

function sq08_252_melody_of_the_memories2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 26 then
		sq08_252_melody_of_the_memories2_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 28 then
		sq08_252_melody_of_the_memories2_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 33 then
		sq08_252_melody_of_the_memories2_OnTalk_n033_archer_master_adellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_252_melody_of_the_memories2_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2521, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2522, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2523, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2524, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2525, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2526, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2527, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2528, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2529, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest(userObjID,252, 1);
				api_quest_SetJournalStep(userObjID,252, 1);
				api_quest_SetQuestStep(userObjID,252, 1);
				api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-1-a", npc_talk_target);

	end
	if npc_talk_index == "n026_trader_may-1-b" then 
	end
	if npc_talk_index == "n026_trader_may-1-c" then 
	end
	if npc_talk_index == "n026_trader_may-1-d" then 
	end
	if npc_talk_index == "n026_trader_may-1-e" then 
	end
	if npc_talk_index == "n026_trader_may-1-f" then 
	end
	if npc_talk_index == "n026_trader_may-1-g" then 
	end
	if npc_talk_index == "n026_trader_may-1-h" then 
	end
	if npc_talk_index == "n026_trader_may-1-i" then 
	end
	if npc_talk_index == "n026_trader_may-1-j" then 
	end
	if npc_talk_index == "n026_trader_may-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 371, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 372, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200371, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200372, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300166, 1);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n026_trader_may-3-a" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_252_melody_of_the_memories2_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n033_archer_master_adellin--------------------------------------------------------------------------------
function sq08_252_melody_of_the_memories2_OnTalk_n033_archer_master_adellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n033_archer_master_adellin-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n033_archer_master_adellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n033_archer_master_adellin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n033_archer_master_adellin-4-a" then 

				if api_quest_HasQuestItem(userObjID, 300165, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300165, api_quest_HasQuestItem(userObjID, 300165, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300166, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300166, api_quest_HasQuestItem(userObjID, 300166, 1));
				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4-b" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-c" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-d" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-e" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2521, true);
				 api_quest_RewardQuestUser(userObjID, 2521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2522, true);
				 api_quest_RewardQuestUser(userObjID, 2522, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2523, true);
				 api_quest_RewardQuestUser(userObjID, 2523, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2524, true);
				 api_quest_RewardQuestUser(userObjID, 2524, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2525, true);
				 api_quest_RewardQuestUser(userObjID, 2525, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2526, true);
				 api_quest_RewardQuestUser(userObjID, 2526, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2527, true);
				 api_quest_RewardQuestUser(userObjID, 2527, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2528, true);
				 api_quest_RewardQuestUser(userObjID, 2528, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2529, true);
				 api_quest_RewardQuestUser(userObjID, 2529, questID, 1);
			 end 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_252_melody_of_the_memories2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 252);
	if qstep == 2 and CountIndex == 371 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300166, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300166 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 372 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300166, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200371 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300166, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200372 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300166, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_252_melody_of_the_memories2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 252);
	if qstep == 2 and CountIndex == 371 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300166 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 372 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200371 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200372 and Count >= TargetCount  then

	end
end

function sq08_252_melody_of_the_memories2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 252);
	local questID=252;
end

function sq08_252_melody_of_the_memories2_OnRemoteStart( userObjID, questID )
end

function sq08_252_melody_of_the_memories2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_252_melody_of_the_memories2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,252, 1);
				api_quest_SetJournalStep(userObjID,252, 1);
				api_quest_SetQuestStep(userObjID,252, 1);
				api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-1-a", npc_talk_target);
end

</VillageServer>

<GameServer>
function sq08_252_melody_of_the_memories2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 26 then
		sq08_252_melody_of_the_memories2_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 28 then
		sq08_252_melody_of_the_memories2_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 33 then
		sq08_252_melody_of_the_memories2_OnTalk_n033_archer_master_adellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_252_melody_of_the_memories2_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2521, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2522, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2523, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2524, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2525, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2526, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2527, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2528, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2529, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,252, 1);
				api_quest_SetJournalStep( pRoom, userObjID,252, 1);
				api_quest_SetQuestStep( pRoom, userObjID,252, 1);
				api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-1-a", npc_talk_target);

	end
	if npc_talk_index == "n026_trader_may-1-b" then 
	end
	if npc_talk_index == "n026_trader_may-1-c" then 
	end
	if npc_talk_index == "n026_trader_may-1-d" then 
	end
	if npc_talk_index == "n026_trader_may-1-e" then 
	end
	if npc_talk_index == "n026_trader_may-1-f" then 
	end
	if npc_talk_index == "n026_trader_may-1-g" then 
	end
	if npc_talk_index == "n026_trader_may-1-h" then 
	end
	if npc_talk_index == "n026_trader_may-1-i" then 
	end
	if npc_talk_index == "n026_trader_may-1-j" then 
	end
	if npc_talk_index == "n026_trader_may-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 371, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 372, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200371, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200372, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300166, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n026_trader_may-3-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_252_melody_of_the_memories2_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n033_archer_master_adellin--------------------------------------------------------------------------------
function sq08_252_melody_of_the_memories2_OnTalk_n033_archer_master_adellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n033_archer_master_adellin-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n033_archer_master_adellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n033_archer_master_adellin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n033_archer_master_adellin-4-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300165, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300165, api_quest_HasQuestItem( pRoom, userObjID, 300165, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300166, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300166, api_quest_HasQuestItem( pRoom, userObjID, 300166, 1));
				end
	end
	if npc_talk_index == "n033_archer_master_adellin-4-b" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-c" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-d" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-e" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2522, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2522, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2523, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2523, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2524, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2524, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2525, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2525, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2526, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2526, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2527, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2527, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2528, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2528, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2529, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2529, questID, 1);
			 end 
	end
	if npc_talk_index == "n033_archer_master_adellin-4-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_252_melody_of_the_memories2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 252);
	if qstep == 2 and CountIndex == 371 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300166, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300166 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 372 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300166, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200371 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300166, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200372 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300166, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_252_melody_of_the_memories2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 252);
	if qstep == 2 and CountIndex == 371 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300166 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 372 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200371 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200372 and Count >= TargetCount  then

	end
end

function sq08_252_melody_of_the_memories2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 252);
	local questID=252;
end

function sq08_252_melody_of_the_memories2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_252_melody_of_the_memories2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_252_melody_of_the_memories2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,252, 1);
				api_quest_SetJournalStep( pRoom, userObjID,252, 1);
				api_quest_SetQuestStep( pRoom, userObjID,252, 1);
				api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-1-a", npc_talk_target);
end

</GameServer>