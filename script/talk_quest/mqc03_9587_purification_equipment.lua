<VillageServer>

function mqc03_9587_purification_equipment_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1227 then
		mqc03_9587_purification_equipment_OnTalk_n1227_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1848 then
		mqc03_9587_purification_equipment_OnTalk_n1848_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		mqc03_9587_purification_equipment_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1227_argenta--------------------------------------------------------------------------------
function mqc03_9587_purification_equipment_OnTalk_n1227_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1227_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1227_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1227_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1227_argenta-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1227_argenta-3-b" then 
	end
	if npc_talk_index == "n1227_argenta-3-c" then 
	end
	if npc_talk_index == "n1227_argenta-3-cutscene" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 664, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200664, 30000);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_user_PlayCutScene(userObjID,npcObjID,72);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1848_shaolong--------------------------------------------------------------------------------
function mqc03_9587_purification_equipment_OnTalk_n1848_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1848_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1848_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1848_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1848_shaolong-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1848_shaolong-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9587, 2);
				api_quest_SetJournalStep(userObjID,9587, 1);
				api_quest_SetQuestStep(userObjID,9587, 1);
				npc_talk_index = "n1848_shaolong-1";

	end
	if npc_talk_index == "n1848_shaolong-1-b" then 
	end
	if npc_talk_index == "n1848_shaolong-1-c" then 
	end
	if npc_talk_index == "n1848_shaolong-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function mqc03_9587_purification_equipment_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n035_soceress_master_tiana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n035_soceress_master_tiana-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-2-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-h" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-i" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-j" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95870, true);
				 api_quest_RewardQuestUser(userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95870, true);
				 api_quest_RewardQuestUser(userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95870, true);
				 api_quest_RewardQuestUser(userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95870, true);
				 api_quest_RewardQuestUser(userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95870, true);
				 api_quest_RewardQuestUser(userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95870, true);
				 api_quest_RewardQuestUser(userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95870, true);
				 api_quest_RewardQuestUser(userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95870, true);
				 api_quest_RewardQuestUser(userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95870, true);
				 api_quest_RewardQuestUser(userObjID, 95870, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9588, 2);
					api_quest_SetQuestStep(userObjID, 9588, 1);
					api_quest_SetJournalStep(userObjID, 9588, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n035_soceress_master_tiana-1", "mqc03_9588_the_request.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc03_9587_purification_equipment_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9587);
	if qstep == 4 and CountIndex == 664 then
				api_quest_ClearCountingInfo(userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300061, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300061, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);

	end
	if qstep == 4 and CountIndex == 200664 then
				api_quest_IncCounting(userObjID, 2, 664);

	end
end

function mqc03_9587_purification_equipment_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9587);
	if qstep == 4 and CountIndex == 664 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 200664 and Count >= TargetCount  then

	end
end

function mqc03_9587_purification_equipment_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9587);
	local questID=9587;
end

function mqc03_9587_purification_equipment_OnRemoteStart( userObjID, questID )
end

function mqc03_9587_purification_equipment_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc03_9587_purification_equipment_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9587, 2);
				api_quest_SetJournalStep(userObjID,9587, 1);
				api_quest_SetQuestStep(userObjID,9587, 1);
				npc_talk_index = "n1848_shaolong-1";
end

</VillageServer>

<GameServer>
function mqc03_9587_purification_equipment_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1227 then
		mqc03_9587_purification_equipment_OnTalk_n1227_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1848 then
		mqc03_9587_purification_equipment_OnTalk_n1848_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		mqc03_9587_purification_equipment_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1227_argenta--------------------------------------------------------------------------------
function mqc03_9587_purification_equipment_OnTalk_n1227_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1227_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1227_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1227_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1227_argenta-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1227_argenta-3-b" then 
	end
	if npc_talk_index == "n1227_argenta-3-c" then 
	end
	if npc_talk_index == "n1227_argenta-3-cutscene" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 664, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200664, 30000);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_user_PlayCutScene( pRoom, userObjID,npcObjID,72);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1848_shaolong--------------------------------------------------------------------------------
function mqc03_9587_purification_equipment_OnTalk_n1848_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1848_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1848_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1848_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1848_shaolong-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1848_shaolong-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9587, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9587, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9587, 1);
				npc_talk_index = "n1848_shaolong-1";

	end
	if npc_talk_index == "n1848_shaolong-1-b" then 
	end
	if npc_talk_index == "n1848_shaolong-1-c" then 
	end
	if npc_talk_index == "n1848_shaolong-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function mqc03_9587_purification_equipment_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n035_soceress_master_tiana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n035_soceress_master_tiana-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-2-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-h" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-i" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-j" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95870, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9588, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9588, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9588, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-1", "mqc03_9588_the_request.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc03_9587_purification_equipment_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9587);
	if qstep == 4 and CountIndex == 664 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300061, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300061, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

	end
	if qstep == 4 and CountIndex == 200664 then
				api_quest_IncCounting( pRoom, userObjID, 2, 664);

	end
end

function mqc03_9587_purification_equipment_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9587);
	if qstep == 4 and CountIndex == 664 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 200664 and Count >= TargetCount  then

	end
end

function mqc03_9587_purification_equipment_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9587);
	local questID=9587;
end

function mqc03_9587_purification_equipment_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc03_9587_purification_equipment_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc03_9587_purification_equipment_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9587, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9587, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9587, 1);
				npc_talk_index = "n1848_shaolong-1";
end

</GameServer>