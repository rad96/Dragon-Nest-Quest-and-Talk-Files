<VillageServer>

function sq08_377_test_academic1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 392 then
		sq08_377_test_academic1_OnTalk_n392_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 779 then
		sq08_377_test_academic1_OnTalk_n779_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n392_academic_station--------------------------------------------------------------------------------
function sq08_377_test_academic1_OnTalk_n392_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n392_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n392_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n392_academic_station-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n392_academic_station-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3770, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3770, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3770, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3770, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3770, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3770, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3770, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3770, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3770, false);
			 end 

	end
	if npc_talk_index == "n392_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,377, 1);
				api_quest_SetJournalStep(userObjID,377, 1);
				api_quest_SetQuestStep(userObjID,377, 1);
				npc_talk_index = "n392_academic_station-1";

	end
	if npc_talk_index == "n392_academic_station-1-b" then 
	end
	if npc_talk_index == "n392_academic_station-1-c" then 
	end
	if npc_talk_index == "n392_academic_station-1-d" then 
	end
	if npc_talk_index == "n392_academic_station-1-party_check_1" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n392_academic_station-1-e";

				else
									npc_talk_index = "n392_academic_station-1-f";

				end
	end
	if npc_talk_index == "n392_academic_station-1-changemap" then 
				api_user_ChangeMap(userObjID,13023,1);
	end
	if npc_talk_index == "n392_academic_station-3-b" then 
	end
	if npc_talk_index == "n392_academic_station-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3770, true);
				 api_quest_RewardQuestUser(userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3770, true);
				 api_quest_RewardQuestUser(userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3770, true);
				 api_quest_RewardQuestUser(userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3770, true);
				 api_quest_RewardQuestUser(userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3770, true);
				 api_quest_RewardQuestUser(userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3770, true);
				 api_quest_RewardQuestUser(userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3770, true);
				 api_quest_RewardQuestUser(userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3770, true);
				 api_quest_RewardQuestUser(userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3770, true);
				 api_quest_RewardQuestUser(userObjID, 3770, questID, 1);
			 end 
	end
	if npc_talk_index == "n392_academic_station-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 378, 1);
					api_quest_SetQuestStep(userObjID, 378, 1);
					api_quest_SetJournalStep(userObjID, 378, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n392_academic_station-1", "sq08_378_test_academic2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n779_jasmin--------------------------------------------------------------------------------
function sq08_377_test_academic1_OnTalk_n779_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n779_jasmin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n779_jasmin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n779_jasmin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n779_jasmin-2-b" then 
	end
	if npc_talk_index == "n779_jasmin-2-c" then 
	end
	if npc_talk_index == "n779_jasmin-2-d" then 
	end
	if npc_talk_index == "n779_jasmin-2-e" then 
	end
	if npc_talk_index == "n779_jasmin-2-f" then 
	end
	if npc_talk_index == "n779_jasmin-2-g" then 
	end
	if npc_talk_index == "n779_jasmin-2-h" then 
	end
	if npc_talk_index == "n779_jasmin-2-i" then 
	end
	if npc_talk_index == "n779_jasmin-2-j" then 
	end
	if npc_talk_index == "n779_jasmin-2-k" then 
	end
	if npc_talk_index == "n779_jasmin-2-l" then 
	end
	if npc_talk_index == "n779_jasmin-2-m" then 
	end
	if npc_talk_index == "n779_jasmin-2-n" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_377_test_academic1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 377);
end

function sq08_377_test_academic1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 377);
end

function sq08_377_test_academic1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 377);
	local questID=377;
end

function sq08_377_test_academic1_OnRemoteStart( userObjID, questID )
end

function sq08_377_test_academic1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_377_test_academic1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,377, 1);
				api_quest_SetJournalStep(userObjID,377, 1);
				api_quest_SetQuestStep(userObjID,377, 1);
				npc_talk_index = "n392_academic_station-1";
end

</VillageServer>

<GameServer>
function sq08_377_test_academic1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 392 then
		sq08_377_test_academic1_OnTalk_n392_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 779 then
		sq08_377_test_academic1_OnTalk_n779_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n392_academic_station--------------------------------------------------------------------------------
function sq08_377_test_academic1_OnTalk_n392_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n392_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n392_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n392_academic_station-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n392_academic_station-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, false);
			 end 

	end
	if npc_talk_index == "n392_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,377, 1);
				api_quest_SetJournalStep( pRoom, userObjID,377, 1);
				api_quest_SetQuestStep( pRoom, userObjID,377, 1);
				npc_talk_index = "n392_academic_station-1";

	end
	if npc_talk_index == "n392_academic_station-1-b" then 
	end
	if npc_talk_index == "n392_academic_station-1-c" then 
	end
	if npc_talk_index == "n392_academic_station-1-d" then 
	end
	if npc_talk_index == "n392_academic_station-1-party_check_1" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n392_academic_station-1-e";

				else
									npc_talk_index = "n392_academic_station-1-f";

				end
	end
	if npc_talk_index == "n392_academic_station-1-changemap" then 
				api_user_ChangeMap( pRoom, userObjID,13023,1);
	end
	if npc_talk_index == "n392_academic_station-3-b" then 
	end
	if npc_talk_index == "n392_academic_station-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3770, questID, 1);
			 end 
	end
	if npc_talk_index == "n392_academic_station-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 378, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 378, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 378, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n392_academic_station-1", "sq08_378_test_academic2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n779_jasmin--------------------------------------------------------------------------------
function sq08_377_test_academic1_OnTalk_n779_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n779_jasmin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n779_jasmin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n779_jasmin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n779_jasmin-2-b" then 
	end
	if npc_talk_index == "n779_jasmin-2-c" then 
	end
	if npc_talk_index == "n779_jasmin-2-d" then 
	end
	if npc_talk_index == "n779_jasmin-2-e" then 
	end
	if npc_talk_index == "n779_jasmin-2-f" then 
	end
	if npc_talk_index == "n779_jasmin-2-g" then 
	end
	if npc_talk_index == "n779_jasmin-2-h" then 
	end
	if npc_talk_index == "n779_jasmin-2-i" then 
	end
	if npc_talk_index == "n779_jasmin-2-j" then 
	end
	if npc_talk_index == "n779_jasmin-2-k" then 
	end
	if npc_talk_index == "n779_jasmin-2-l" then 
	end
	if npc_talk_index == "n779_jasmin-2-m" then 
	end
	if npc_talk_index == "n779_jasmin-2-n" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_377_test_academic1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 377);
end

function sq08_377_test_academic1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 377);
end

function sq08_377_test_academic1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 377);
	local questID=377;
end

function sq08_377_test_academic1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_377_test_academic1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_377_test_academic1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,377, 1);
				api_quest_SetJournalStep( pRoom, userObjID,377, 1);
				api_quest_SetQuestStep( pRoom, userObjID,377, 1);
				npc_talk_index = "n392_academic_station-1";
end

</GameServer>