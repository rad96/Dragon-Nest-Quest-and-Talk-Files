<VillageServer>

function sq15_767_whistling_of_the_breeze_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 712 then
		sq15_767_whistling_of_the_breeze_OnTalk_n712_archer_zenya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 764 then
		sq15_767_whistling_of_the_breeze_OnTalk_n764_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 765 then
		sq15_767_whistling_of_the_breeze_OnTalk_n765_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n712_archer_zenya--------------------------------------------------------------------------------
function sq15_767_whistling_of_the_breeze_OnTalk_n712_archer_zenya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n712_archer_zenya-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n712_archer_zenya-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n712_archer_zenya-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n712_archer_zenya-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n712_archer_zenya-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7670, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7670, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7670, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7670, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7670, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7670, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7670, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7670, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7670, false);
			 end 

	end
	if npc_talk_index == "n712_archer_zenya-accepting-acceptted" then
				api_quest_AddQuest(userObjID,767, 1);
				api_quest_SetJournalStep(userObjID,767, 1);
				api_quest_SetQuestStep(userObjID,767, 1);
				npc_talk_index = "n712_archer_zenya-1";

	end
	if npc_talk_index == "n712_archer_zenya-1-a" then 
				if api_user_GetUserJobID(userObjID) == 29   or api_user_GetUserJobID(userObjID) == 30   or api_user_GetUserJobID(userObjID) == 31   or api_user_GetUserJobID(userObjID) == 32  then
									npc_talk_index = "n712_archer_zenya-6";
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);

				else
									npc_talk_index = "n712_archer_zenya-1-a";

				end
	end
	if npc_talk_index == "n712_archer_zenya-1-level1" then 
				if api_user_GetUserJobID(userObjID) == 14   or api_user_GetUserJobID(userObjID) == 15  then
									if api_user_GetUserLevel(userObjID) >= 45 then
									npc_talk_index = "n712_archer_zenya-1-b";

				else
									npc_talk_index = "n712_archer_zenya-1-h";

				end

				else
									npc_talk_index = "n712_archer_zenya-1-h";

				end
	end
	if npc_talk_index == "n712_archer_zenya-1-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-d" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-e" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-f" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-g" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-d" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-e" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-f" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-g" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-6-completeh" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 29);
				npc_talk_index = "n712_archer_zenya-6-l";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n712_archer_zenya-6-completei" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 30);
				npc_talk_index = "n712_archer_zenya-6-l";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n712_archer_zenya-6-completej" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 31);
				npc_talk_index = "n712_archer_zenya-6-l";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n712_archer_zenya-6-completek" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 32);
				npc_talk_index = "n712_archer_zenya-6-l";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n712_archer_zenya-6-n" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-o" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-p" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7670, true);
				 api_quest_RewardQuestUser(userObjID, 7670, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-6-q" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n764_yuvenciel--------------------------------------------------------------------------------
function sq15_767_whistling_of_the_breeze_OnTalk_n764_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n764_yuvenciel-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n764_yuvenciel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n764_yuvenciel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n764_yuvenciel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n764_yuvenciel-2-party1" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n764_yuvenciel-2-a";

				else
									npc_talk_index = "n764_yuvenciel-2-h";

				end
	end
	if npc_talk_index == "n764_yuvenciel-2-b" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-c" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-c" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-d" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-e" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-f" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-g" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-move1" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_user_ChangeMap(userObjID,13512,1);
	end
	if npc_talk_index == "n764_yuvenciel-3-move2" then 
				api_user_ChangeMap(userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n765_yuvenciel--------------------------------------------------------------------------------
function sq15_767_whistling_of_the_breeze_OnTalk_n765_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n765_yuvenciel-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n765_yuvenciel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n765_yuvenciel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n765_yuvenciel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n765_yuvenciel-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n765_yuvenciel-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n765_yuvenciel-3-b" then 
	end
	if npc_talk_index == "n765_yuvenciel-3-check3" then 
				if api_user_GetUserJobID(userObjID) == 14  then
									npc_talk_index = "n765_yuvenciel-3-c";

				else
									if api_user_GetUserJobID(userObjID) == 15  then
									npc_talk_index = "n765_yuvenciel-3-g";

				else
									npc_talk_index = "n765_yuvenciel-3-g";

				end

				end
	end
	if npc_talk_index == "n765_yuvenciel-3-d" then 
	end
	if npc_talk_index == "n765_yuvenciel-3-j" then 
	end
	if npc_talk_index == "n765_yuvenciel-3-sni1" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-3-f";
				api_user_SetSecondJobSkill( userObjID, 29 );
	end
	if npc_talk_index == "n765_yuvenciel-3-ati1" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-3-f";
				api_user_SetSecondJobSkill( userObjID, 30 );
	end
	if npc_talk_index == "n765_yuvenciel-3-h" then 
	end
	if npc_talk_index == "n765_yuvenciel-3-j" then 
	end
	if npc_talk_index == "n765_yuvenciel-3-temp1" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-3-f";
				api_user_SetSecondJobSkill( userObjID, 31 );
	end
	if npc_talk_index == "n765_yuvenciel-3-wind1" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-3-f";
				api_user_SetSecondJobSkill( userObjID, 32 );
	end
	if npc_talk_index == "n765_yuvenciel-3-open_ui3" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n765_yuvenciel-5-f" then 
	end
	if npc_talk_index == "n765_yuvenciel-5-sni5" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-4";
				api_user_SetSecondJobSkill( userObjID, 29 );
	end
	if npc_talk_index == "n765_yuvenciel-5-atil5" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-4";
				api_user_SetSecondJobSkill( userObjID, 30 );
	end
	if npc_talk_index == "n765_yuvenciel-5-temp5" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-4";
				api_user_SetSecondJobSkill( userObjID, 31 );
	end
	if npc_talk_index == "n765_yuvenciel-5-wind5" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-4";
				api_user_SetSecondJobSkill( userObjID, 32 );
	end
	if npc_talk_index == "n765_yuvenciel-5-e" then 
	end
	if npc_talk_index == "n765_yuvenciel-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n765_yuvenciel-5-open_ui5" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n765_yuvenciel-6-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_767_whistling_of_the_breeze_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 767);
end

function sq15_767_whistling_of_the_breeze_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 767);
end

function sq15_767_whistling_of_the_breeze_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 767);
	local questID=767;
end

function sq15_767_whistling_of_the_breeze_OnRemoteStart( userObjID, questID )
end

function sq15_767_whistling_of_the_breeze_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_767_whistling_of_the_breeze_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,767, 1);
				api_quest_SetJournalStep(userObjID,767, 1);
				api_quest_SetQuestStep(userObjID,767, 1);
				npc_talk_index = "n712_archer_zenya-1";
end

</VillageServer>

<GameServer>
function sq15_767_whistling_of_the_breeze_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 712 then
		sq15_767_whistling_of_the_breeze_OnTalk_n712_archer_zenya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 764 then
		sq15_767_whistling_of_the_breeze_OnTalk_n764_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 765 then
		sq15_767_whistling_of_the_breeze_OnTalk_n765_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n712_archer_zenya--------------------------------------------------------------------------------
function sq15_767_whistling_of_the_breeze_OnTalk_n712_archer_zenya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n712_archer_zenya-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n712_archer_zenya-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n712_archer_zenya-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n712_archer_zenya-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n712_archer_zenya-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, false);
			 end 

	end
	if npc_talk_index == "n712_archer_zenya-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,767, 1);
				api_quest_SetJournalStep( pRoom, userObjID,767, 1);
				api_quest_SetQuestStep( pRoom, userObjID,767, 1);
				npc_talk_index = "n712_archer_zenya-1";

	end
	if npc_talk_index == "n712_archer_zenya-1-a" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 29   or api_user_GetUserJobID( pRoom, userObjID) == 30   or api_user_GetUserJobID( pRoom, userObjID) == 31   or api_user_GetUserJobID( pRoom, userObjID) == 32  then
									npc_talk_index = "n712_archer_zenya-6";
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);

				else
									npc_talk_index = "n712_archer_zenya-1-a";

				end
	end
	if npc_talk_index == "n712_archer_zenya-1-level1" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 14   or api_user_GetUserJobID( pRoom, userObjID) == 15  then
									if api_user_GetUserLevel( pRoom, userObjID) >= 45 then
									npc_talk_index = "n712_archer_zenya-1-b";

				else
									npc_talk_index = "n712_archer_zenya-1-h";

				end

				else
									npc_talk_index = "n712_archer_zenya-1-h";

				end
	end
	if npc_talk_index == "n712_archer_zenya-1-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-d" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-e" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-f" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-g" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-d" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-e" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-f" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-g" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-6-completeh" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 29);
				npc_talk_index = "n712_archer_zenya-6-l";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n712_archer_zenya-6-completei" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 30);
				npc_talk_index = "n712_archer_zenya-6-l";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n712_archer_zenya-6-completej" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 31);
				npc_talk_index = "n712_archer_zenya-6-l";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n712_archer_zenya-6-completek" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 32);
				npc_talk_index = "n712_archer_zenya-6-l";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n712_archer_zenya-6-n" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-o" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-p" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7670, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-6-q" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n764_yuvenciel--------------------------------------------------------------------------------
function sq15_767_whistling_of_the_breeze_OnTalk_n764_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n764_yuvenciel-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n764_yuvenciel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n764_yuvenciel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n764_yuvenciel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n764_yuvenciel-2-party1" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n764_yuvenciel-2-a";

				else
									npc_talk_index = "n764_yuvenciel-2-h";

				end
	end
	if npc_talk_index == "n764_yuvenciel-2-b" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-c" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-c" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-d" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-e" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-f" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-g" then 
	end
	if npc_talk_index == "n764_yuvenciel-2-move1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end
	if npc_talk_index == "n764_yuvenciel-3-move2" then 
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n765_yuvenciel--------------------------------------------------------------------------------
function sq15_767_whistling_of_the_breeze_OnTalk_n765_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n765_yuvenciel-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n765_yuvenciel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n765_yuvenciel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n765_yuvenciel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n765_yuvenciel-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n765_yuvenciel-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n765_yuvenciel-3-b" then 
	end
	if npc_talk_index == "n765_yuvenciel-3-check3" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 14  then
									npc_talk_index = "n765_yuvenciel-3-c";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 15  then
									npc_talk_index = "n765_yuvenciel-3-g";

				else
									npc_talk_index = "n765_yuvenciel-3-g";

				end

				end
	end
	if npc_talk_index == "n765_yuvenciel-3-d" then 
	end
	if npc_talk_index == "n765_yuvenciel-3-j" then 
	end
	if npc_talk_index == "n765_yuvenciel-3-sni1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-3-f";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 29 );
	end
	if npc_talk_index == "n765_yuvenciel-3-ati1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-3-f";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 30 );
	end
	if npc_talk_index == "n765_yuvenciel-3-h" then 
	end
	if npc_talk_index == "n765_yuvenciel-3-j" then 
	end
	if npc_talk_index == "n765_yuvenciel-3-temp1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-3-f";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 31 );
	end
	if npc_talk_index == "n765_yuvenciel-3-wind1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-3-f";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 32 );
	end
	if npc_talk_index == "n765_yuvenciel-3-open_ui3" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n765_yuvenciel-5-f" then 
	end
	if npc_talk_index == "n765_yuvenciel-5-sni5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 29 );
	end
	if npc_talk_index == "n765_yuvenciel-5-atil5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 30 );
	end
	if npc_talk_index == "n765_yuvenciel-5-temp5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 31 );
	end
	if npc_talk_index == "n765_yuvenciel-5-wind5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n765_yuvenciel-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 32 );
	end
	if npc_talk_index == "n765_yuvenciel-5-e" then 
	end
	if npc_talk_index == "n765_yuvenciel-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n765_yuvenciel-5-open_ui5" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n765_yuvenciel-6-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_767_whistling_of_the_breeze_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 767);
end

function sq15_767_whistling_of_the_breeze_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 767);
end

function sq15_767_whistling_of_the_breeze_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 767);
	local questID=767;
end

function sq15_767_whistling_of_the_breeze_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_767_whistling_of_the_breeze_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_767_whistling_of_the_breeze_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,767, 1);
				api_quest_SetJournalStep( pRoom, userObjID,767, 1);
				api_quest_SetQuestStep( pRoom, userObjID,767, 1);
				npc_talk_index = "n712_archer_zenya-1";
end

</GameServer>