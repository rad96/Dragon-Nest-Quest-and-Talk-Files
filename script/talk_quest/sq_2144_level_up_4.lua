<VillageServer>

function sq_2144_level_up_4_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 100 then
		sq_2144_level_up_4_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2144_level_up_4_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21441, true);
				 api_quest_RewardQuestUser(userObjID, 21441, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21442, true);
				 api_quest_RewardQuestUser(userObjID, 21442, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21443, true);
				 api_quest_RewardQuestUser(userObjID, 21443, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21444, true);
				 api_quest_RewardQuestUser(userObjID, 21444, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21445, true);
				 api_quest_RewardQuestUser(userObjID, 21445, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21446, true);
				 api_quest_RewardQuestUser(userObjID, 21446, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21447, true);
				 api_quest_RewardQuestUser(userObjID, 21447, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21448, true);
				 api_quest_RewardQuestUser(userObjID, 21448, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21440, true);
				 api_quest_RewardQuestUser(userObjID, 21440, questID, 1);
			 end 
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2144, 1);
				api_quest_SetJournalStep(userObjID,2144, 1);
				api_quest_SetQuestStep(userObjID,2144, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21441, true);
				 api_quest_RewardQuestUser(userObjID, 21441, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21442, true);
				 api_quest_RewardQuestUser(userObjID, 21442, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21443, true);
				 api_quest_RewardQuestUser(userObjID, 21443, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21444, true);
				 api_quest_RewardQuestUser(userObjID, 21444, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21445, true);
				 api_quest_RewardQuestUser(userObjID, 21445, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21446, true);
				 api_quest_RewardQuestUser(userObjID, 21446, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21447, true);
				 api_quest_RewardQuestUser(userObjID, 21447, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21448, true);
				 api_quest_RewardQuestUser(userObjID, 21448, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21440, true);
				 api_quest_RewardQuestUser(userObjID, 21440, questID, 1);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-1-a" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_user_HasItem(userObjID, 805307082, 1) > 0 then 
					api_user_DelItem(userObjID, 805307082, api_user_HasItem(userObjID, 805307082, 1, questID));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2144_level_up_4_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2144);
end

function sq_2144_level_up_4_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2144);
end

function sq_2144_level_up_4_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2144);
	local questID=2144;
end

function sq_2144_level_up_4_OnRemoteStart( userObjID, questID )
end

function sq_2144_level_up_4_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_2144_level_up_4_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2144, 1);
				api_quest_SetJournalStep(userObjID,2144, 1);
				api_quest_SetQuestStep(userObjID,2144, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21441, true);
				 api_quest_RewardQuestUser(userObjID, 21441, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21442, true);
				 api_quest_RewardQuestUser(userObjID, 21442, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21443, true);
				 api_quest_RewardQuestUser(userObjID, 21443, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21444, true);
				 api_quest_RewardQuestUser(userObjID, 21444, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21445, true);
				 api_quest_RewardQuestUser(userObjID, 21445, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21446, true);
				 api_quest_RewardQuestUser(userObjID, 21446, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21447, true);
				 api_quest_RewardQuestUser(userObjID, 21447, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21448, true);
				 api_quest_RewardQuestUser(userObjID, 21448, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21440, true);
				 api_quest_RewardQuestUser(userObjID, 21440, questID, 1);
			 end 
end

</VillageServer>

<GameServer>
function sq_2144_level_up_4_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 100 then
		sq_2144_level_up_4_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2144_level_up_4_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21441, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21442, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21443, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21443, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21444, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21444, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21445, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21445, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21446, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21446, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21447, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21447, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21448, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21448, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21440, questID, 1);
			 end 
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2144, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2144, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2144, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21441, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21442, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21443, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21443, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21444, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21444, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21445, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21445, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21446, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21446, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21447, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21447, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21448, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21448, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21440, questID, 1);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-1-a" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_user_HasItem( pRoom, userObjID, 805307082, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 805307082, api_user_HasItem( pRoom, userObjID, 805307082, 1, questID));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2144_level_up_4_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2144);
end

function sq_2144_level_up_4_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2144);
end

function sq_2144_level_up_4_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2144);
	local questID=2144;
end

function sq_2144_level_up_4_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_2144_level_up_4_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_2144_level_up_4_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2144, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2144, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2144, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21441, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21442, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21443, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21443, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21444, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21444, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21445, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21445, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21446, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21446, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21447, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21447, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21448, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21448, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21440, questID, 1);
			 end 
end

</GameServer>