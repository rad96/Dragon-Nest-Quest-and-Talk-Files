<VillageServer>

function mq34_9394_efforts_for_legend_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		mq34_9394_efforts_for_legend_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1232 then
		mq34_9394_efforts_for_legend_OnTalk_n1232_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1235 then
		mq34_9394_efforts_for_legend_OnTalk_n1235_wanderer_elf_riana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1237 then
		mq34_9394_efforts_for_legend_OnTalk_n1237_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1238 then
		mq34_9394_efforts_for_legend_OnTalk_n1238_warrior_barnak(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1239 then
		mq34_9394_efforts_for_legend_OnTalk_n1239_archer_nerwin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1240 then
		mq34_9394_efforts_for_legend_OnTalk_n1240_sorceress_karakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1241 then
		mq34_9394_efforts_for_legend_OnTalk_n1241_cleric_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1242 then
		mq34_9394_efforts_for_legend_OnTalk_n1242_knight_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1243 then
		mq34_9394_efforts_for_legend_OnTalk_n1243_gereint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1244 then
		mq34_9394_efforts_for_legend_OnTalk_n1244_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		mq34_9394_efforts_for_legend_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mq34_9394_efforts_for_legend_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1232_white_dragon--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1232_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1232_white_dragon-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1232_white_dragon-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1232_white_dragon-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1232_white_dragon-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1232_white_dragon-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n1232_white_dragon-6-b" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-d" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-e" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-f" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-g" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-g" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93940, true);
				 api_quest_RewardQuestUser(userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93940, true);
				 api_quest_RewardQuestUser(userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93940, true);
				 api_quest_RewardQuestUser(userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93940, true);
				 api_quest_RewardQuestUser(userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93940, true);
				 api_quest_RewardQuestUser(userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93940, true);
				 api_quest_RewardQuestUser(userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93940, true);
				 api_quest_RewardQuestUser(userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93940, true);
				 api_quest_RewardQuestUser(userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93940, true);
				 api_quest_RewardQuestUser(userObjID, 93940, questID, 1);
			 end 
	end
	if npc_talk_index == "n1232_white_dragon-6-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9395, 2);
					api_quest_SetQuestStep(userObjID, 9395, 1);
					api_quest_SetJournalStep(userObjID, 9395, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1232_white_dragon-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1232_white_dragon-1", "mq34_9395_end_of_legend.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1235_wanderer_elf_riana--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1235_wanderer_elf_riana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1235_wanderer_elf_riana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1235_wanderer_elf_riana-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1235_wanderer_elf_riana-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1235_wanderer_elf_riana-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1235_wanderer_elf_riana-3-b" then 
	end
	if npc_talk_index == "n1235_wanderer_elf_riana-3-c" then 
	end
	if npc_talk_index == "n1235_wanderer_elf_riana-3-c" then 
	end
	if npc_talk_index == "n1235_wanderer_elf_riana-3-d" then 
	end
	if npc_talk_index == "n1235_wanderer_elf_riana-3-e" then 
	end
	if npc_talk_index == "n1235_wanderer_elf_riana-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1237_for_memory--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1237_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1237_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1237_for_memory-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1237_for_memory-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1237_for_memory-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1237_for_memory-4-b" then 
	end
	if npc_talk_index == "n1237_for_memory-4-c" then 
	end
	if npc_talk_index == "n1237_for_memory-4-d" then 
	end
	if npc_talk_index == "n1237_for_memory-4-e" then 
	end
	if npc_talk_index == "n1237_for_memory-4-f" then 
	end
	if npc_talk_index == "n1237_for_memory-4-g" then 
	end
	if npc_talk_index == "n1237_for_memory-4-h" then 
	end
	if npc_talk_index == "n1237_for_memory-4-i" then 
	end
	if npc_talk_index == "n1237_for_memory-4-j" then 
	end
	if npc_talk_index == "n1237_for_memory-4-k" then 
	end
	if npc_talk_index == "n1237_for_memory-4-l" then 
	end
	if npc_talk_index == "n1237_for_memory-4-m" then 
	end
	if npc_talk_index == "n1237_for_memory-4-n" then 
	end
	if npc_talk_index == "n1237_for_memory-4-o" then 
	end
	if npc_talk_index == "n1237_for_memory-4-p" then 
	end
	if npc_talk_index == "n1237_for_memory-4-q" then 
	end
	if npc_talk_index == "n1237_for_memory-4-r" then 
	end
	if npc_talk_index == "n1237_for_memory-4-s" then 
	end
	if npc_talk_index == "n1237_for_memory-4-t" then 
	end
	if npc_talk_index == "n1237_for_memory-4-u" then 
	end
	if npc_talk_index == "n1237_for_memory-4-v" then 
	end
	if npc_talk_index == "n1237_for_memory-4-w" then 
	end
	if npc_talk_index == "n1237_for_memory-4-x" then 
	end
	if npc_talk_index == "n1237_for_memory-4-y" then 
	end
	if npc_talk_index == "n1237_for_memory-4-z" then 
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n1237_for_memory-5-b" then 
	end
	if npc_talk_index == "n1237_for_memory-5-c" then 
	end
	if npc_talk_index == "n1237_for_memory-5-d" then 
	end
	if npc_talk_index == "n1237_for_memory-5-e" then 
	end
	if npc_talk_index == "n1237_for_memory-5-f" then 
	end
	if npc_talk_index == "n1237_for_memory-5-g" then 
	end
	if npc_talk_index == "n1237_for_memory-5-h" then 
	end
	if npc_talk_index == "n1237_for_memory-5-i" then 
	end
	if npc_talk_index == "n1237_for_memory-5-j" then 
	end
	if npc_talk_index == "n1237_for_memory-5-k" then 
	end
	if npc_talk_index == "n1237_for_memory-5-l" then 
	end
	if npc_talk_index == "n1237_for_memory-5-m" then 
	end
	if npc_talk_index == "n1237_for_memory-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1238_warrior_barnak--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1238_warrior_barnak(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1238_warrior_barnak-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1239_archer_nerwin--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1239_archer_nerwin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1239_archer_nerwin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1240_sorceress_karakule--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1240_sorceress_karakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1240_sorceress_karakule-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1241_cleric_teramai--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1241_cleric_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1241_cleric_teramai-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1242_knight_velskud--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1242_knight_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1242_knight_velskud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1242_knight_velskud-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1242_knight_velskud-5-b" then 
	end
	if npc_talk_index == "n1242_knight_velskud-5-c" then 
	end
	if npc_talk_index == "n1242_knight_velskud-5-d" then 
	end
	if npc_talk_index == "n1242_knight_velskud-5-e" then 
	end
	if npc_talk_index == "n1242_knight_velskud-5-f" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1243_gereint--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1243_gereint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1243_gereint-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1243_gereint-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1243_gereint-5-b" then 
	end
	if npc_talk_index == "n1243_gereint-5-c" then 
	end
	if npc_talk_index == "n1243_gereint-5-d" then 
	end
	if npc_talk_index == "n1243_gereint-5-e" then 
	end
	if npc_talk_index == "n1243_gereint-5-f" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1244_for_memory--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1244_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1244_for_memory-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1244_for_memory-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1244_for_memory-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n044_archer_master_ishilien-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9394, 2);
				api_quest_SetJournalStep(userObjID,9394, 1);
				api_quest_SetQuestStep(userObjID,9394, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";

	end
	if npc_talk_index == "n044_archer_master_ishilien-1-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9394_efforts_for_legend_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9394);
end

function mq34_9394_efforts_for_legend_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9394);
end

function mq34_9394_efforts_for_legend_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9394);
	local questID=9394;
end

function mq34_9394_efforts_for_legend_OnRemoteStart( userObjID, questID )
end

function mq34_9394_efforts_for_legend_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9394_efforts_for_legend_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9394, 2);
				api_quest_SetJournalStep(userObjID,9394, 1);
				api_quest_SetQuestStep(userObjID,9394, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
end

</VillageServer>

<GameServer>
function mq34_9394_efforts_for_legend_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		mq34_9394_efforts_for_legend_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1232 then
		mq34_9394_efforts_for_legend_OnTalk_n1232_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1235 then
		mq34_9394_efforts_for_legend_OnTalk_n1235_wanderer_elf_riana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1237 then
		mq34_9394_efforts_for_legend_OnTalk_n1237_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1238 then
		mq34_9394_efforts_for_legend_OnTalk_n1238_warrior_barnak( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1239 then
		mq34_9394_efforts_for_legend_OnTalk_n1239_archer_nerwin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1240 then
		mq34_9394_efforts_for_legend_OnTalk_n1240_sorceress_karakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1241 then
		mq34_9394_efforts_for_legend_OnTalk_n1241_cleric_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1242 then
		mq34_9394_efforts_for_legend_OnTalk_n1242_knight_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1243 then
		mq34_9394_efforts_for_legend_OnTalk_n1243_gereint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1244 then
		mq34_9394_efforts_for_legend_OnTalk_n1244_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		mq34_9394_efforts_for_legend_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mq34_9394_efforts_for_legend_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1232_white_dragon--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1232_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1232_white_dragon-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1232_white_dragon-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1232_white_dragon-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1232_white_dragon-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1232_white_dragon-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n1232_white_dragon-6-b" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-d" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-e" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-f" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-g" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-g" then 
	end
	if npc_talk_index == "n1232_white_dragon-6-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93940, questID, 1);
			 end 
	end
	if npc_talk_index == "n1232_white_dragon-6-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9395, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9395, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9395, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1232_white_dragon-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1232_white_dragon-1", "mq34_9395_end_of_legend.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1235_wanderer_elf_riana--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1235_wanderer_elf_riana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1235_wanderer_elf_riana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1235_wanderer_elf_riana-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1235_wanderer_elf_riana-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1235_wanderer_elf_riana-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1235_wanderer_elf_riana-3-b" then 
	end
	if npc_talk_index == "n1235_wanderer_elf_riana-3-c" then 
	end
	if npc_talk_index == "n1235_wanderer_elf_riana-3-c" then 
	end
	if npc_talk_index == "n1235_wanderer_elf_riana-3-d" then 
	end
	if npc_talk_index == "n1235_wanderer_elf_riana-3-e" then 
	end
	if npc_talk_index == "n1235_wanderer_elf_riana-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1237_for_memory--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1237_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1237_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1237_for_memory-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1237_for_memory-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1237_for_memory-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1237_for_memory-4-b" then 
	end
	if npc_talk_index == "n1237_for_memory-4-c" then 
	end
	if npc_talk_index == "n1237_for_memory-4-d" then 
	end
	if npc_talk_index == "n1237_for_memory-4-e" then 
	end
	if npc_talk_index == "n1237_for_memory-4-f" then 
	end
	if npc_talk_index == "n1237_for_memory-4-g" then 
	end
	if npc_talk_index == "n1237_for_memory-4-h" then 
	end
	if npc_talk_index == "n1237_for_memory-4-i" then 
	end
	if npc_talk_index == "n1237_for_memory-4-j" then 
	end
	if npc_talk_index == "n1237_for_memory-4-k" then 
	end
	if npc_talk_index == "n1237_for_memory-4-l" then 
	end
	if npc_talk_index == "n1237_for_memory-4-m" then 
	end
	if npc_talk_index == "n1237_for_memory-4-n" then 
	end
	if npc_talk_index == "n1237_for_memory-4-o" then 
	end
	if npc_talk_index == "n1237_for_memory-4-p" then 
	end
	if npc_talk_index == "n1237_for_memory-4-q" then 
	end
	if npc_talk_index == "n1237_for_memory-4-r" then 
	end
	if npc_talk_index == "n1237_for_memory-4-s" then 
	end
	if npc_talk_index == "n1237_for_memory-4-t" then 
	end
	if npc_talk_index == "n1237_for_memory-4-u" then 
	end
	if npc_talk_index == "n1237_for_memory-4-v" then 
	end
	if npc_talk_index == "n1237_for_memory-4-w" then 
	end
	if npc_talk_index == "n1237_for_memory-4-x" then 
	end
	if npc_talk_index == "n1237_for_memory-4-y" then 
	end
	if npc_talk_index == "n1237_for_memory-4-z" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n1237_for_memory-5-b" then 
	end
	if npc_talk_index == "n1237_for_memory-5-c" then 
	end
	if npc_talk_index == "n1237_for_memory-5-d" then 
	end
	if npc_talk_index == "n1237_for_memory-5-e" then 
	end
	if npc_talk_index == "n1237_for_memory-5-f" then 
	end
	if npc_talk_index == "n1237_for_memory-5-g" then 
	end
	if npc_talk_index == "n1237_for_memory-5-h" then 
	end
	if npc_talk_index == "n1237_for_memory-5-i" then 
	end
	if npc_talk_index == "n1237_for_memory-5-j" then 
	end
	if npc_talk_index == "n1237_for_memory-5-k" then 
	end
	if npc_talk_index == "n1237_for_memory-5-l" then 
	end
	if npc_talk_index == "n1237_for_memory-5-m" then 
	end
	if npc_talk_index == "n1237_for_memory-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1238_warrior_barnak--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1238_warrior_barnak( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1238_warrior_barnak-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1239_archer_nerwin--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1239_archer_nerwin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1239_archer_nerwin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1240_sorceress_karakule--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1240_sorceress_karakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1240_sorceress_karakule-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1241_cleric_teramai--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1241_cleric_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1241_cleric_teramai-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1242_knight_velskud--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1242_knight_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1242_knight_velskud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1242_knight_velskud-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1242_knight_velskud-5-b" then 
	end
	if npc_talk_index == "n1242_knight_velskud-5-c" then 
	end
	if npc_talk_index == "n1242_knight_velskud-5-d" then 
	end
	if npc_talk_index == "n1242_knight_velskud-5-e" then 
	end
	if npc_talk_index == "n1242_knight_velskud-5-f" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1243_gereint--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1243_gereint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1243_gereint-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1243_gereint-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1243_gereint-5-b" then 
	end
	if npc_talk_index == "n1243_gereint-5-c" then 
	end
	if npc_talk_index == "n1243_gereint-5-d" then 
	end
	if npc_talk_index == "n1243_gereint-5-e" then 
	end
	if npc_talk_index == "n1243_gereint-5-f" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1244_for_memory--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n1244_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1244_for_memory-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1244_for_memory-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1244_for_memory-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n044_archer_master_ishilien-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9394, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9394, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9394, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";

	end
	if npc_talk_index == "n044_archer_master_ishilien-1-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mq34_9394_efforts_for_legend_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9394_efforts_for_legend_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9394);
end

function mq34_9394_efforts_for_legend_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9394);
end

function mq34_9394_efforts_for_legend_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9394);
	local questID=9394;
end

function mq34_9394_efforts_for_legend_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9394_efforts_for_legend_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9394_efforts_for_legend_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9394, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9394, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9394, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
end

</GameServer>