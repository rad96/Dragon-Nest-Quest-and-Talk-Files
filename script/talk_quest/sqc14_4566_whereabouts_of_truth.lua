<VillageServer>

function sqc14_4566_whereabouts_of_truth_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1609 then
		sqc14_4566_whereabouts_of_truth_OnTalk_n1609_sorceress_pochetel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1609_sorceress_pochetel--------------------------------------------------------------------------------
function sqc14_4566_whereabouts_of_truth_OnTalk_n1609_sorceress_pochetel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1609_sorceress_pochetel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1609_sorceress_pochetel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1609_sorceress_pochetel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1609_sorceress_pochetel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1609_sorceress_pochetel-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45660, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45660, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45660, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45660, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45660, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45660, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45660, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45660, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45660, false);
			 end 

	end
	if npc_talk_index == "n1609_sorceress_pochetel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4566, 1);
				api_quest_SetJournalStep(userObjID,4566, 1);
				api_quest_SetQuestStep(userObjID,4566, 1);

	end
	if npc_talk_index == "n1609_sorceress_pochetel-1-b" then 
	end
	if npc_talk_index == "n1609_sorceress_pochetel-1-c" then 
	end
	if npc_talk_index == "n1609_sorceress_pochetel-1-d" then 
	end
	if npc_talk_index == "n1609_sorceress_pochetel-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 601991, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 701991, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400435, 1);
	end
	if npc_talk_index == "n1609_sorceress_pochetel-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45660, true);
				 api_quest_RewardQuestUser(userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45660, true);
				 api_quest_RewardQuestUser(userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45660, true);
				 api_quest_RewardQuestUser(userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45660, true);
				 api_quest_RewardQuestUser(userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45660, true);
				 api_quest_RewardQuestUser(userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45660, true);
				 api_quest_RewardQuestUser(userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45660, true);
				 api_quest_RewardQuestUser(userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45660, true);
				 api_quest_RewardQuestUser(userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45660, true);
				 api_quest_RewardQuestUser(userObjID, 45660, questID, 1);
			 end 

				if api_quest_HasQuestItem(userObjID, 400435, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400435, api_quest_HasQuestItem(userObjID, 400435, 1));
				end
	end
	if npc_talk_index == "n1609_sorceress_pochetel-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4567, 1);
					api_quest_SetQuestStep(userObjID, 4567, 1);
					api_quest_SetJournalStep(userObjID, 4567, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1609_sorceress_pochetel-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1609_sorceress_pochetel-1", "sqc14_4567_truth_of_karakule.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4566_whereabouts_of_truth_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4566);
	if qstep == 2 and CountIndex == 601991 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400435, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400435, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 701991 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400435, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400435, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400435 then

	end
end

function sqc14_4566_whereabouts_of_truth_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4566);
	if qstep == 2 and CountIndex == 601991 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701991 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400435 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function sqc14_4566_whereabouts_of_truth_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4566);
	local questID=4566;
end

function sqc14_4566_whereabouts_of_truth_OnRemoteStart( userObjID, questID )
end

function sqc14_4566_whereabouts_of_truth_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc14_4566_whereabouts_of_truth_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4566, 1);
				api_quest_SetJournalStep(userObjID,4566, 1);
				api_quest_SetQuestStep(userObjID,4566, 1);
end

</VillageServer>

<GameServer>
function sqc14_4566_whereabouts_of_truth_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1609 then
		sqc14_4566_whereabouts_of_truth_OnTalk_n1609_sorceress_pochetel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1609_sorceress_pochetel--------------------------------------------------------------------------------
function sqc14_4566_whereabouts_of_truth_OnTalk_n1609_sorceress_pochetel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1609_sorceress_pochetel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1609_sorceress_pochetel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1609_sorceress_pochetel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1609_sorceress_pochetel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1609_sorceress_pochetel-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, false);
			 end 

	end
	if npc_talk_index == "n1609_sorceress_pochetel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4566, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4566, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4566, 1);

	end
	if npc_talk_index == "n1609_sorceress_pochetel-1-b" then 
	end
	if npc_talk_index == "n1609_sorceress_pochetel-1-c" then 
	end
	if npc_talk_index == "n1609_sorceress_pochetel-1-d" then 
	end
	if npc_talk_index == "n1609_sorceress_pochetel-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 601991, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 701991, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400435, 1);
	end
	if npc_talk_index == "n1609_sorceress_pochetel-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45660, questID, 1);
			 end 

				if api_quest_HasQuestItem( pRoom, userObjID, 400435, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400435, api_quest_HasQuestItem( pRoom, userObjID, 400435, 1));
				end
	end
	if npc_talk_index == "n1609_sorceress_pochetel-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4567, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4567, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4567, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1609_sorceress_pochetel-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1609_sorceress_pochetel-1", "sqc14_4567_truth_of_karakule.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4566_whereabouts_of_truth_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4566);
	if qstep == 2 and CountIndex == 601991 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400435, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400435, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 701991 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400435, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400435, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400435 then

	end
end

function sqc14_4566_whereabouts_of_truth_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4566);
	if qstep == 2 and CountIndex == 601991 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701991 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400435 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function sqc14_4566_whereabouts_of_truth_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4566);
	local questID=4566;
end

function sqc14_4566_whereabouts_of_truth_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc14_4566_whereabouts_of_truth_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc14_4566_whereabouts_of_truth_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4566, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4566, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4566, 1);
end

</GameServer>