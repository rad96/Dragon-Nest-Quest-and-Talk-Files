<VillageServer>

function sqc06_4724_the_meaning_of_strength_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1897 then
		sqc06_4724_the_meaning_of_strength_OnTalk_n1897_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1897_shaolong--------------------------------------------------------------------------------
function sqc06_4724_the_meaning_of_strength_OnTalk_n1897_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1897_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1897_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1897_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1897_shaolong-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1897_shaolong-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47240, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47240, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47240, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47240, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47240, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47240, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47240, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47240, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47240, false);
			 end 

	end
	if npc_talk_index == "n1897_shaolong-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4724, 1);
				api_quest_SetJournalStep(userObjID,4724, 1);
				api_quest_SetQuestStep(userObjID,4724, 1);
				npc_talk_index = "n1897_shaolong-1";

	end
	if npc_talk_index == "n1897_shaolong-1-b" then 
	end
	if npc_talk_index == "n1897_shaolong-1-c" then 
	end
	if npc_talk_index == "n1897_shaolong-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 511, 1 );
	end
	if npc_talk_index == "n1897_shaolong-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47240, true);
				 api_quest_RewardQuestUser(userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47240, true);
				 api_quest_RewardQuestUser(userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47240, true);
				 api_quest_RewardQuestUser(userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47240, true);
				 api_quest_RewardQuestUser(userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47240, true);
				 api_quest_RewardQuestUser(userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47240, true);
				 api_quest_RewardQuestUser(userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47240, true);
				 api_quest_RewardQuestUser(userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47240, true);
				 api_quest_RewardQuestUser(userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47240, true);
				 api_quest_RewardQuestUser(userObjID, 47240, questID, 1);
			 end 
	end
	if npc_talk_index == "n1897_shaolong-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4725, 1);
					api_quest_SetQuestStep(userObjID, 4725, 1);
					api_quest_SetJournalStep(userObjID, 4725, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1897_shaolong-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1897_shaolong-1", "sqc06_4725_basis_of_martial_arts.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc06_4724_the_meaning_of_strength_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4724);
	 local qstep= 1; 
	 if qstep == 1 then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);
		 end 
	 end 
end

function sqc06_4724_the_meaning_of_strength_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4724);
end

function sqc06_4724_the_meaning_of_strength_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4724);
	local questID=4724;
end

function sqc06_4724_the_meaning_of_strength_OnRemoteStart( userObjID, questID )
end

function sqc06_4724_the_meaning_of_strength_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc06_4724_the_meaning_of_strength_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4724, 1);
				api_quest_SetJournalStep(userObjID,4724, 1);
				api_quest_SetQuestStep(userObjID,4724, 1);
				npc_talk_index = "n1897_shaolong-1";
end

</VillageServer>

<GameServer>
function sqc06_4724_the_meaning_of_strength_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1897 then
		sqc06_4724_the_meaning_of_strength_OnTalk_n1897_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1897_shaolong--------------------------------------------------------------------------------
function sqc06_4724_the_meaning_of_strength_OnTalk_n1897_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1897_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1897_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1897_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1897_shaolong-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1897_shaolong-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, false);
			 end 

	end
	if npc_talk_index == "n1897_shaolong-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4724, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4724, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4724, 1);
				npc_talk_index = "n1897_shaolong-1";

	end
	if npc_talk_index == "n1897_shaolong-1-b" then 
	end
	if npc_talk_index == "n1897_shaolong-1-c" then 
	end
	if npc_talk_index == "n1897_shaolong-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 511, 1 );
	end
	if npc_talk_index == "n1897_shaolong-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47240, questID, 1);
			 end 
	end
	if npc_talk_index == "n1897_shaolong-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4725, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4725, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4725, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1897_shaolong-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1897_shaolong-1", "sqc06_4725_basis_of_martial_arts.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc06_4724_the_meaning_of_strength_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4724);
	 local qstep= 1; 
	 if qstep == 1 then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
		 end 
	 end 
end

function sqc06_4724_the_meaning_of_strength_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4724);
end

function sqc06_4724_the_meaning_of_strength_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4724);
	local questID=4724;
end

function sqc06_4724_the_meaning_of_strength_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc06_4724_the_meaning_of_strength_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc06_4724_the_meaning_of_strength_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4724, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4724, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4724, 1);
				npc_talk_index = "n1897_shaolong-1";
end

</GameServer>