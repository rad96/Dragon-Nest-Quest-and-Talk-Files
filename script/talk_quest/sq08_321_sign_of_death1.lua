<VillageServer>

function sq08_321_sign_of_death1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 28 then
		sq08_321_sign_of_death1_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 34 then
		sq08_321_sign_of_death1_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_321_sign_of_death1_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3211, true);
				 api_quest_RewardQuestUser(userObjID, 3211, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3212, true);
				 api_quest_RewardQuestUser(userObjID, 3212, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3213, true);
				 api_quest_RewardQuestUser(userObjID, 3213, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3214, true);
				 api_quest_RewardQuestUser(userObjID, 3214, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3215, true);
				 api_quest_RewardQuestUser(userObjID, 3215, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3216, true);
				 api_quest_RewardQuestUser(userObjID, 3216, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3217, true);
				 api_quest_RewardQuestUser(userObjID, 3217, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3218, true);
				 api_quest_RewardQuestUser(userObjID, 3218, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3219, true);
				 api_quest_RewardQuestUser(userObjID, 3219, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400121, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400121, api_quest_HasQuestItem(userObjID, 400121, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n034_cleric_master_germain--------------------------------------------------------------------------------
function sq08_321_sign_of_death1_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n034_cleric_master_germain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3211, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3212, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3213, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3214, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3215, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3216, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3217, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3218, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3219, false);
			 end 

	end
	if npc_talk_index == "n034_cleric_master_germain-accepting-acceptted" then
				api_quest_AddQuest(userObjID,321, 1);
				api_quest_SetJournalStep(userObjID,321, 1);
				api_quest_SetQuestStep(userObjID,321, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 615, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200615, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400121, 1);
				npc_talk_index = "n034_cleric_master_germain-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_321_sign_of_death1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 321);
	if qstep == 1 and CountIndex == 615 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400121, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400121, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400121 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200615 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400121, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400121, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_321_sign_of_death1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 321);
	if qstep == 1 and CountIndex == 615 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400121 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200615 and Count >= TargetCount  then

	end
end

function sq08_321_sign_of_death1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 321);
	local questID=321;
end

function sq08_321_sign_of_death1_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 615, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200615, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400121, 1);
end

function sq08_321_sign_of_death1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_321_sign_of_death1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,321, 1);
				api_quest_SetJournalStep(userObjID,321, 1);
				api_quest_SetQuestStep(userObjID,321, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 615, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200615, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400121, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
end

</VillageServer>

<GameServer>
function sq08_321_sign_of_death1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 28 then
		sq08_321_sign_of_death1_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 34 then
		sq08_321_sign_of_death1_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_321_sign_of_death1_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3211, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3211, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3212, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3212, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3213, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3213, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3214, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3214, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3215, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3215, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3216, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3216, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3217, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3217, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3218, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3218, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3219, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3219, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400121, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400121, api_quest_HasQuestItem( pRoom, userObjID, 400121, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n034_cleric_master_germain--------------------------------------------------------------------------------
function sq08_321_sign_of_death1_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n034_cleric_master_germain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3211, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3212, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3213, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3214, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3215, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3216, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3217, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3218, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3219, false);
			 end 

	end
	if npc_talk_index == "n034_cleric_master_germain-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,321, 1);
				api_quest_SetJournalStep( pRoom, userObjID,321, 1);
				api_quest_SetQuestStep( pRoom, userObjID,321, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 615, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200615, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400121, 1);
				npc_talk_index = "n034_cleric_master_germain-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_321_sign_of_death1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 321);
	if qstep == 1 and CountIndex == 615 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400121, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400121, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400121 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200615 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400121, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400121, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_321_sign_of_death1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 321);
	if qstep == 1 and CountIndex == 615 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400121 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200615 and Count >= TargetCount  then

	end
end

function sq08_321_sign_of_death1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 321);
	local questID=321;
end

function sq08_321_sign_of_death1_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 615, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200615, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400121, 1);
end

function sq08_321_sign_of_death1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_321_sign_of_death1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,321, 1);
				api_quest_SetJournalStep( pRoom, userObjID,321, 1);
				api_quest_SetQuestStep( pRoom, userObjID,321, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 615, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200615, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400121, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
end

</GameServer>