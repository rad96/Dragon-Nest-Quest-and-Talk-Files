<VillageServer>

function nq41_8302_yohan_talk_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 714 then
		nq41_8302_yohan_talk_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n714_cleric_yohan--------------------------------------------------------------------------------
function nq41_8302_yohan_talk_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n714_cleric_yohan-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-accepting-b" then
				api_quest_ForceCompleteQuest(userObjID,8302, 0, 1, 1, 0);
				api_npc_AddFavorPoint( userObjID, 714, 500 );

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-c" then
				api_quest_ForceCompleteQuest(userObjID,8302, 0, 1, 1, 0);
				api_npc_AddFavorPoint( userObjID, 714, 300 );

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-d" then
				api_quest_ForceCompleteQuest(userObjID,8302, 0, 1, 1, 0);
				api_npc_AddFavorPoint( userObjID, 714, 300 );

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-e" then
				api_quest_ForceCompleteQuest(userObjID,8302, 0, 1, 1, 0);
				api_npc_AddFavorPoint( userObjID, 714, 50 );

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq41_8302_yohan_talk_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8302);
end

function nq41_8302_yohan_talk_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8302);
end

function nq41_8302_yohan_talk_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8302);
	local questID=8302;
end

function nq41_8302_yohan_talk_OnRemoteStart( userObjID, questID )
end

function nq41_8302_yohan_talk_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq41_8302_yohan_talk_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq41_8302_yohan_talk_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 714 then
		nq41_8302_yohan_talk_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n714_cleric_yohan--------------------------------------------------------------------------------
function nq41_8302_yohan_talk_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n714_cleric_yohan-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-accepting-b" then
				api_quest_ForceCompleteQuest( pRoom, userObjID,8302, 0, 1, 1, 0);
				api_npc_AddFavorPoint( pRoom,  userObjID, 714, 500 );

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-c" then
				api_quest_ForceCompleteQuest( pRoom, userObjID,8302, 0, 1, 1, 0);
				api_npc_AddFavorPoint( pRoom,  userObjID, 714, 300 );

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-d" then
				api_quest_ForceCompleteQuest( pRoom, userObjID,8302, 0, 1, 1, 0);
				api_npc_AddFavorPoint( pRoom,  userObjID, 714, 300 );

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-e" then
				api_quest_ForceCompleteQuest( pRoom, userObjID,8302, 0, 1, 1, 0);
				api_npc_AddFavorPoint( pRoom,  userObjID, 714, 50 );

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq41_8302_yohan_talk_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8302);
end

function nq41_8302_yohan_talk_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8302);
end

function nq41_8302_yohan_talk_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8302);
	local questID=8302;
end

function nq41_8302_yohan_talk_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq41_8302_yohan_talk_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq41_8302_yohan_talk_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>