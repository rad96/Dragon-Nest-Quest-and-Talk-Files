<VillageServer>

function sq11_557_spread_of_abyss1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 88 then
		sq11_557_spread_of_abyss1_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_557_spread_of_abyss1_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5571, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5572, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5573, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5574, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5575, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5576, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5577, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5577, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5577, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceppted" then
				api_quest_AddQuest(userObjID,557, 1);
				api_quest_SetJournalStep(userObjID,557, 1);
				api_quest_SetQuestStep(userObjID,557, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 200931, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201007, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201028, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201197, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 201078, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 3, 300263, 1);
				api_quest_SetCountingInfo(userObjID, questID, 6, 3, 300264, 1);
				api_quest_SetCountingInfo(userObjID, questID, 7, 3, 300265, 1);
				api_quest_SetCountingInfo(userObjID, questID, 8, 3, 300266, 1);
				api_quest_SetCountingInfo(userObjID, questID, 9, 3, 300267, 1);
				npc_talk_index = "n088_scholar_starshy-1";

	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5571, true);
				 api_quest_RewardQuestUser(userObjID, 5571, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5572, true);
				 api_quest_RewardQuestUser(userObjID, 5572, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5573, true);
				 api_quest_RewardQuestUser(userObjID, 5573, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5574, true);
				 api_quest_RewardQuestUser(userObjID, 5574, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5575, true);
				 api_quest_RewardQuestUser(userObjID, 5575, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5576, true);
				 api_quest_RewardQuestUser(userObjID, 5576, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5577, true);
				 api_quest_RewardQuestUser(userObjID, 5577, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5577, true);
				 api_quest_RewardQuestUser(userObjID, 5577, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5577, true);
				 api_quest_RewardQuestUser(userObjID, 5577, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-2-!next" then 
		local cqresult = 1
				api_quest_DelQuestItem(userObjID, 300263, 1);
				api_quest_DelQuestItem(userObjID, 300264, 1);
				api_quest_DelQuestItem(userObjID, 300265, 1);
				api_quest_DelQuestItem(userObjID, 300266, 1);
				api_quest_DelQuestItem(userObjID, 300267, 1);

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 558, 1);
					api_quest_SetQuestStep(userObjID, 558, 1);
					api_quest_SetJournalStep(userObjID, 558, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n088_scholar_starshy-1", "sq11_558_spread_of_abyss2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_557_spread_of_abyss1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 557);
	if qstep == 1 and CountIndex == 200931 then
				if api_quest_HasQuestItem(userObjID, 300263, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300263, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300263, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 201007 then
				if api_quest_HasQuestItem(userObjID, 300264, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300264, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300264, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 201028 then
				if api_quest_HasQuestItem(userObjID, 300265, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300265, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300265, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 201197 then
				if api_quest_HasQuestItem(userObjID, 300266, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300266, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300266, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 201078 then
				if api_quest_HasQuestItem(userObjID, 300267, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300267, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300267, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 300263 then

	end
	if qstep == 1 and CountIndex == 300264 then

	end
	if qstep == 1 and CountIndex == 300265 then

	end
	if qstep == 1 and CountIndex == 300266 then

	end
	if qstep == 1 and CountIndex == 300267 then

	end
end

function sq11_557_spread_of_abyss1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 557);
	if qstep == 1 and CountIndex == 200931 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201007 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201028 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201197 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201078 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300263 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300264 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300265 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300266 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300267 and Count >= TargetCount  then

	end
end

function sq11_557_spread_of_abyss1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 557);
	local questID=557;
end

function sq11_557_spread_of_abyss1_OnRemoteStart( userObjID, questID )
end

function sq11_557_spread_of_abyss1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_557_spread_of_abyss1_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_557_spread_of_abyss1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 88 then
		sq11_557_spread_of_abyss1_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_557_spread_of_abyss1_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5571, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5572, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5573, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5574, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5575, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5576, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5577, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5577, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5577, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,557, 1);
				api_quest_SetJournalStep( pRoom, userObjID,557, 1);
				api_quest_SetQuestStep( pRoom, userObjID,557, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 200931, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201007, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201028, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201197, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 201078, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 3, 300263, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 3, 300264, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 3, 300265, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 3, 300266, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 9, 3, 300267, 1);
				npc_talk_index = "n088_scholar_starshy-1";

	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5571, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5571, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5572, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5572, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5573, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5573, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5574, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5574, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5575, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5575, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5576, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5576, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5577, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5577, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5577, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5577, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5577, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5577, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-2-!next" then 
		local cqresult = 1
				api_quest_DelQuestItem( pRoom, userObjID, 300263, 1);
				api_quest_DelQuestItem( pRoom, userObjID, 300264, 1);
				api_quest_DelQuestItem( pRoom, userObjID, 300265, 1);
				api_quest_DelQuestItem( pRoom, userObjID, 300266, 1);
				api_quest_DelQuestItem( pRoom, userObjID, 300267, 1);

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 558, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 558, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 558, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n088_scholar_starshy-1", "sq11_558_spread_of_abyss2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_557_spread_of_abyss1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 557);
	if qstep == 1 and CountIndex == 200931 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300263, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300263, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300263, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 201007 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300264, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300264, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300264, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 201028 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300265, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300265, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300265, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 201197 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300266, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300266, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300266, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 201078 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300267, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300267, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300267, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300263, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300264, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300265, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300266, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300267, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 300263 then

	end
	if qstep == 1 and CountIndex == 300264 then

	end
	if qstep == 1 and CountIndex == 300265 then

	end
	if qstep == 1 and CountIndex == 300266 then

	end
	if qstep == 1 and CountIndex == 300267 then

	end
end

function sq11_557_spread_of_abyss1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 557);
	if qstep == 1 and CountIndex == 200931 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201007 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201028 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201197 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201078 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300263 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300264 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300265 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300266 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300267 and Count >= TargetCount  then

	end
end

function sq11_557_spread_of_abyss1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 557);
	local questID=557;
end

function sq11_557_spread_of_abyss1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_557_spread_of_abyss1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_557_spread_of_abyss1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>