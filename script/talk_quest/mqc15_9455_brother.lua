<VillageServer>

function mqc15_9455_brother_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9455_brother_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1598 then
		mqc15_9455_brother_OnTalk_n1598_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1501 then
		mqc15_9455_brother_OnTalk_n1501_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1503 then
		mqc15_9455_brother_OnTalk_n1503_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9455_brother_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9455, 2);
				api_quest_SetJournalStep(userObjID,9455, 1);
				api_quest_SetQuestStep(userObjID,9455, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1598_velskud--------------------------------------------------------------------------------
function mqc15_9455_brother_OnTalk_n1598_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1598_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1598_velskud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1501_argenta--------------------------------------------------------------------------------
function mqc15_9455_brother_OnTalk_n1501_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1501_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1501_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1501_argenta-2-b" then 
	end
	if npc_talk_index == "n1501_argenta-2-c" then 
	end
	if npc_talk_index == "n1501_argenta-2-d" then 
	end
	if npc_talk_index == "n1501_argenta-2-e" then 
	end
	if npc_talk_index == "n1501_argenta-2-f" then 
	end
	if npc_talk_index == "n1501_argenta-2-g" then 
	end
	if npc_talk_index == "n1501_argenta-2-h" then 
	end
	if npc_talk_index == "n1501_argenta-2-i" then 
	end
	if npc_talk_index == "n1501_argenta-2-j" then 
	end
	if npc_talk_index == "n1501_argenta-2-k" then 
	end
	if npc_talk_index == "n1501_argenta-2-l" then 
	end
	if npc_talk_index == "n1501_argenta-2-m" then 
	end
	if npc_talk_index == "n1501_argenta-2-n" then 
	end
	if npc_talk_index == "n1501_argenta-2-o" then 
	end
	if npc_talk_index == "n1501_argenta-2-p" then 
	end
	if npc_talk_index == "n1501_argenta-2-q" then 
	end
	if npc_talk_index == "n1501_argenta-2-r" then 
	end
	if npc_talk_index == "n1501_argenta-2-s" then 
	end
	if npc_talk_index == "n1501_argenta-2-t" then 
	end
	if npc_talk_index == "n1501_argenta-2-u" then 
	end
	if npc_talk_index == "n1501_argenta-2-v" then 
	end
	if npc_talk_index == "n1501_argenta-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1503_gereint_kid--------------------------------------------------------------------------------
function mqc15_9455_brother_OnTalk_n1503_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1503_gereint_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1503_gereint_kid-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1503_gereint_kid-2-b" then 
	end
	if npc_talk_index == "n1503_gereint_kid-2-c" then 
	end
	if npc_talk_index == "n1503_gereint_kid-2-d" then 
	end
	if npc_talk_index == "n1503_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1503_gereint_kid-2-f" then 
	end
	if npc_talk_index == "n1503_gereint_kid-4-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94550, true);
				 api_quest_RewardQuestUser(userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94550, true);
				 api_quest_RewardQuestUser(userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94550, true);
				 api_quest_RewardQuestUser(userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94550, true);
				 api_quest_RewardQuestUser(userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94550, true);
				 api_quest_RewardQuestUser(userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94550, true);
				 api_quest_RewardQuestUser(userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94550, true);
				 api_quest_RewardQuestUser(userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94550, true);
				 api_quest_RewardQuestUser(userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94550, true);
				 api_quest_RewardQuestUser(userObjID, 94550, questID, 1);
			 end 
	end
	if npc_talk_index == "n1503_gereint_kid-4-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9456, 2);
					api_quest_SetQuestStep(userObjID, 9456, 1);
					api_quest_SetJournalStep(userObjID, 9456, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1503_gereint_kid-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1503_gereint_kid-1", "mqc15_9456_great_silver_dragon.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9455_brother_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9455);
end

function mqc15_9455_brother_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9455);
end

function mqc15_9455_brother_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9455);
	local questID=9455;
end

function mqc15_9455_brother_OnRemoteStart( userObjID, questID )
end

function mqc15_9455_brother_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9455_brother_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9455, 2);
				api_quest_SetJournalStep(userObjID,9455, 1);
				api_quest_SetQuestStep(userObjID,9455, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</VillageServer>

<GameServer>
function mqc15_9455_brother_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9455_brother_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1598 then
		mqc15_9455_brother_OnTalk_n1598_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1501 then
		mqc15_9455_brother_OnTalk_n1501_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1503 then
		mqc15_9455_brother_OnTalk_n1503_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9455_brother_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9455, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9455, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9455, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1598_velskud--------------------------------------------------------------------------------
function mqc15_9455_brother_OnTalk_n1598_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1598_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1598_velskud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1501_argenta--------------------------------------------------------------------------------
function mqc15_9455_brother_OnTalk_n1501_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1501_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1501_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1501_argenta-2-b" then 
	end
	if npc_talk_index == "n1501_argenta-2-c" then 
	end
	if npc_talk_index == "n1501_argenta-2-d" then 
	end
	if npc_talk_index == "n1501_argenta-2-e" then 
	end
	if npc_talk_index == "n1501_argenta-2-f" then 
	end
	if npc_talk_index == "n1501_argenta-2-g" then 
	end
	if npc_talk_index == "n1501_argenta-2-h" then 
	end
	if npc_talk_index == "n1501_argenta-2-i" then 
	end
	if npc_talk_index == "n1501_argenta-2-j" then 
	end
	if npc_talk_index == "n1501_argenta-2-k" then 
	end
	if npc_talk_index == "n1501_argenta-2-l" then 
	end
	if npc_talk_index == "n1501_argenta-2-m" then 
	end
	if npc_talk_index == "n1501_argenta-2-n" then 
	end
	if npc_talk_index == "n1501_argenta-2-o" then 
	end
	if npc_talk_index == "n1501_argenta-2-p" then 
	end
	if npc_talk_index == "n1501_argenta-2-q" then 
	end
	if npc_talk_index == "n1501_argenta-2-r" then 
	end
	if npc_talk_index == "n1501_argenta-2-s" then 
	end
	if npc_talk_index == "n1501_argenta-2-t" then 
	end
	if npc_talk_index == "n1501_argenta-2-u" then 
	end
	if npc_talk_index == "n1501_argenta-2-v" then 
	end
	if npc_talk_index == "n1501_argenta-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1503_gereint_kid--------------------------------------------------------------------------------
function mqc15_9455_brother_OnTalk_n1503_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1503_gereint_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1503_gereint_kid-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1503_gereint_kid-2-b" then 
	end
	if npc_talk_index == "n1503_gereint_kid-2-c" then 
	end
	if npc_talk_index == "n1503_gereint_kid-2-d" then 
	end
	if npc_talk_index == "n1503_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1503_gereint_kid-2-f" then 
	end
	if npc_talk_index == "n1503_gereint_kid-4-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94550, questID, 1);
			 end 
	end
	if npc_talk_index == "n1503_gereint_kid-4-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9456, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9456, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9456, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1503_gereint_kid-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1503_gereint_kid-1", "mqc15_9456_great_silver_dragon.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9455_brother_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9455);
end

function mqc15_9455_brother_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9455);
end

function mqc15_9455_brother_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9455);
	local questID=9455;
end

function mqc15_9455_brother_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9455_brother_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9455_brother_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9455, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9455, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9455, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</GameServer>