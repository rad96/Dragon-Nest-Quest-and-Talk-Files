<VillageServer>

function sq11_581_search_an_absconder_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 36 then
		sq11_581_search_an_absconder_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_581_search_an_absconder_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n036_adventurer_guildmaster_gunter-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5811, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5812, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5813, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5814, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5815, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5816, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5817, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5817, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5817, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest(userObjID,581, 1);
				api_quest_SetJournalStep(userObjID,581, 1);
				api_quest_SetQuestStep(userObjID,581, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1003, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201003, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400185, 1);
				api_quest_SetJournalStep(userObjID, questID, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-3" then 

				if api_quest_HasQuestItem(userObjID, 400185, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400185, api_quest_HasQuestItem(userObjID, 400185, 1));
				end
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-3-b" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-3-c" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-3-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5811, true);
				 api_quest_RewardQuestUser(userObjID, 5811, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5812, true);
				 api_quest_RewardQuestUser(userObjID, 5812, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5813, true);
				 api_quest_RewardQuestUser(userObjID, 5813, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5814, true);
				 api_quest_RewardQuestUser(userObjID, 5814, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5815, true);
				 api_quest_RewardQuestUser(userObjID, 5815, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5816, true);
				 api_quest_RewardQuestUser(userObjID, 5816, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5817, true);
				 api_quest_RewardQuestUser(userObjID, 5817, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5817, true);
				 api_quest_RewardQuestUser(userObjID, 5817, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5817, true);
				 api_quest_RewardQuestUser(userObjID, 5817, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-3-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 36, 480 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_581_search_an_absconder_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 581);
	if qstep == 1 and CountIndex == 1003 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400185, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400185, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400185 then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 201003 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400185, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400185, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_581_search_an_absconder_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 581);
	if qstep == 1 and CountIndex == 1003 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400185 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201003 and Count >= TargetCount  then

	end
end

function sq11_581_search_an_absconder_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 581);
	local questID=581;
end

function sq11_581_search_an_absconder_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1003, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201003, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400185, 1);
				api_quest_SetJournalStep(userObjID, questID, 1);
end

function sq11_581_search_an_absconder_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_581_search_an_absconder_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,581, 1);
				api_quest_SetJournalStep(userObjID,581, 1);
				api_quest_SetQuestStep(userObjID,581, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1003, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201003, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400185, 1);
				api_quest_SetJournalStep(userObjID, questID, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
end

</VillageServer>

<GameServer>
function sq11_581_search_an_absconder_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 36 then
		sq11_581_search_an_absconder_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_581_search_an_absconder_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n036_adventurer_guildmaster_gunter-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5811, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5812, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5813, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5814, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5815, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5816, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5817, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5817, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5817, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,581, 1);
				api_quest_SetJournalStep( pRoom, userObjID,581, 1);
				api_quest_SetQuestStep( pRoom, userObjID,581, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1003, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201003, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400185, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-3" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400185, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400185, api_quest_HasQuestItem( pRoom, userObjID, 400185, 1));
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-3-b" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-3-c" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-3-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5811, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5811, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5812, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5812, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5813, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5813, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5814, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5814, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5815, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5815, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5816, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5816, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5817, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5817, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5817, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5817, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5817, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5817, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-3-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 36, 480 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_581_search_an_absconder_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 581);
	if qstep == 1 and CountIndex == 1003 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400185, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400185, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400185 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 201003 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400185, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400185, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_581_search_an_absconder_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 581);
	if qstep == 1 and CountIndex == 1003 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400185 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201003 and Count >= TargetCount  then

	end
end

function sq11_581_search_an_absconder_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 581);
	local questID=581;
end

function sq11_581_search_an_absconder_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1003, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201003, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400185, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
end

function sq11_581_search_an_absconder_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_581_search_an_absconder_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,581, 1);
				api_quest_SetJournalStep( pRoom, userObjID,581, 1);
				api_quest_SetQuestStep( pRoom, userObjID,581, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1003, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201003, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400185, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
end

</GameServer>