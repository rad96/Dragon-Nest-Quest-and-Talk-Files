<VillageServer>

function sq15_4360_shadow_in_mirror_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1169 then
		sq15_4360_shadow_in_mirror_OnTalk_n1169_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1203 then
		sq15_4360_shadow_in_mirror_OnTalk_n1203_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 575 then
		sq15_4360_shadow_in_mirror_OnTalk_n575_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1169_lunaria--------------------------------------------------------------------------------
function sq15_4360_shadow_in_mirror_OnTalk_n1169_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1169_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1169_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1169_lunaria-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1169_lunaria-1-level_ck_1" then 
				if api_user_GetUserLevel(userObjID) >= 45 then
									if api_user_GetUserJobID(userObjID) == 62  then
									npc_talk_index = "n1169_lunaria-1-b";

				else
									if api_user_GetUserJobID(userObjID) == 67  then
									npc_talk_index = "n1169_lunaria-1-b";

				else
									npc_talk_index = "n1169_lunaria-1-a";

				end

				end

				else
									npc_talk_index = "n1169_lunaria-1-a";

				end
	end
	if npc_talk_index == "n1169_lunaria-1-c" then 
	end
	if npc_talk_index == "n1169_lunaria-1-d" then 
	end
	if npc_talk_index == "n1169_lunaria-1-e" then 
	end
	if npc_talk_index == "n1169_lunaria-1-ck_party_01" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n1169_lunaria-1-f";

				else
									npc_talk_index = "n1169_lunaria-1-g";

				end
	end
	if npc_talk_index == "n1169_lunaria-1-move_01" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13512,1);
	end
	if npc_talk_index == "n1169_lunaria-2-ck_party_02" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n1169_lunaria-2-b";

				else
									npc_talk_index = "n1169_lunaria-2-a";

				end
	end
	if npc_talk_index == "n1169_lunaria-2-move_02" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1203_lunaria--------------------------------------------------------------------------------
function sq15_4360_shadow_in_mirror_OnTalk_n1203_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1203_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1203_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1203_lunaria-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1203_lunaria-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1203_lunaria-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1203_lunaria-2-b" then 
	end
	if npc_talk_index == "n1203_lunaria-2-c" then 
	end
	if npc_talk_index == "n1203_lunaria-2-ck_job02_01" then 
				if api_user_GetUserJobID(userObjID) == 62  then
									npc_talk_index = "n1203_lunaria-2-d";

				else
									npc_talk_index = "n1203_lunaria-2-j";

				end
	end
	if npc_talk_index == "n1203_lunaria-2-e" then 
	end
	if npc_talk_index == "n1203_lunaria-2-f" then 
	end
	if npc_talk_index == "n1203_lunaria-2-i" then 
	end
	if npc_talk_index == "n1203_lunaria-2-ripper_1" then 
				npc_talk_index = "n1203_lunaria-2-h";
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_user_SetSecondJobSkill( userObjID, 63 );
	end
	if npc_talk_index == "n1203_lunaria-2-raven_1" then 
				npc_talk_index = "n1203_lunaria-2-h";
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_user_SetSecondJobSkill( userObjID, 64 );
	end
	if npc_talk_index == "n1203_lunaria-2-open_ui2" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1203_lunaria-2-k" then 
	end
	if npc_talk_index == "n1203_lunaria-2-l" then 
	end
	if npc_talk_index == "n1203_lunaria-2-i" then 
	end
	if npc_talk_index == "n1203_lunaria-4-b" then 
	end
	if npc_talk_index == "n1203_lunaria-4-h" then 
	end
	if npc_talk_index == "n1203_lunaria-4-f" then 
	end
	if npc_talk_index == "n1203_lunaria-4-ripper_2" then 
				npc_talk_index = "n1203_lunaria-3";
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_user_SetSecondJobSkill( userObjID, 63 );
	end
	if npc_talk_index == "n1203_lunaria-4-raven_2" then 
				npc_talk_index = "n1203_lunaria-3";
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_user_SetSecondJobSkill( userObjID, 64 );
	end
	if npc_talk_index == "n1203_lunaria-4-e" then 
	end
	if npc_talk_index == "n1203_lunaria-4-b" then 
	end
	if npc_talk_index == "n1203_lunaria-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);

				if api_quest_HasQuestItem(userObjID, 300577, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300577, api_quest_HasQuestItem(userObjID, 300577, 1));
				end
	end
	if npc_talk_index == "n1203_lunaria-4-g" then 
	end
	if npc_talk_index == "n1203_lunaria-4-open_ui4" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1203_lunaria-4-g" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n575_shadow_meow--------------------------------------------------------------------------------
function sq15_4360_shadow_in_mirror_OnTalk_n575_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n575_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n575_shadow_meow-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n575_shadow_meow-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n575_shadow_meow-accepting-a" then
				if api_user_GetUserJobID(userObjID) == 63   or api_user_GetUserJobID(userObjID) == 64   or api_user_GetUserJobID(userObjID) == 68   or api_user_GetUserJobID(userObjID) == 69  then
									npc_talk_index = "n575_shadow_meow-accepting-b";

				else
									npc_talk_index = "n575_shadow_meow-accepting-a";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 end 

				end

	end
	if npc_talk_index == "n575_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4360, 1);
				api_quest_SetJournalStep(userObjID,4360, 1);
				api_quest_SetQuestStep(userObjID,4360, 1);
				npc_talk_index = "n575_shadow_meow-1";

	end
	if npc_talk_index == "n575_shadow_meow-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43600, false);
			 end 

	end
	if npc_talk_index == "n575_shadow_meow-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,4360, 1);
				api_quest_SetJournalStep(userObjID,4360, 1);
				api_quest_SetQuestStep(userObjID,4360, 1);
				npc_talk_index = "n575_shadow_meow-5";
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);

	end
	if npc_talk_index == "n575_shadow_meow-5-ck_j_last11" then 
				if api_user_GetUserJobID(userObjID) == 62  then
									npc_talk_index = "n575_shadow_meow-5-c";

				else
									npc_talk_index = "n575_shadow_meow-5-c";

				end
	end
	if npc_talk_index == "n575_shadow_meow-5-b" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-d" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-e" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-b" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 end 
	end
	if npc_talk_index == "n575_shadow_meow-5-b" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 end 
	end
	if npc_talk_index == "n575_shadow_meow-5-b" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-chg_ripper" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 63);
				npc_talk_index = "n575_shadow_meow-5-h";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n575_shadow_meow-5-chg_raven" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 64);
				npc_talk_index = "n575_shadow_meow-5-h";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n575_shadow_meow-5-j" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43600, true);
				 api_quest_RewardQuestUser(userObjID, 43600, questID, 1);
			 end 
	end
	if npc_talk_index == "n575_shadow_meow-5-l" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n575_shadow_meow-5-h" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4360_shadow_in_mirror_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4360);
end

function sq15_4360_shadow_in_mirror_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4360);
end

function sq15_4360_shadow_in_mirror_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4360);
	local questID=4360;
end

function sq15_4360_shadow_in_mirror_OnRemoteStart( userObjID, questID )
end

function sq15_4360_shadow_in_mirror_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4360_shadow_in_mirror_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4360, 1);
				api_quest_SetJournalStep(userObjID,4360, 1);
				api_quest_SetQuestStep(userObjID,4360, 1);
				npc_talk_index = "n575_shadow_meow-1";
end

</VillageServer>

<GameServer>
function sq15_4360_shadow_in_mirror_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1169 then
		sq15_4360_shadow_in_mirror_OnTalk_n1169_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1203 then
		sq15_4360_shadow_in_mirror_OnTalk_n1203_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 575 then
		sq15_4360_shadow_in_mirror_OnTalk_n575_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1169_lunaria--------------------------------------------------------------------------------
function sq15_4360_shadow_in_mirror_OnTalk_n1169_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1169_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1169_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1169_lunaria-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1169_lunaria-1-level_ck_1" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 45 then
									if api_user_GetUserJobID( pRoom, userObjID) == 62  then
									npc_talk_index = "n1169_lunaria-1-b";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 67  then
									npc_talk_index = "n1169_lunaria-1-b";

				else
									npc_talk_index = "n1169_lunaria-1-a";

				end

				end

				else
									npc_talk_index = "n1169_lunaria-1-a";

				end
	end
	if npc_talk_index == "n1169_lunaria-1-c" then 
	end
	if npc_talk_index == "n1169_lunaria-1-d" then 
	end
	if npc_talk_index == "n1169_lunaria-1-e" then 
	end
	if npc_talk_index == "n1169_lunaria-1-ck_party_01" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n1169_lunaria-1-f";

				else
									npc_talk_index = "n1169_lunaria-1-g";

				end
	end
	if npc_talk_index == "n1169_lunaria-1-move_01" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end
	if npc_talk_index == "n1169_lunaria-2-ck_party_02" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n1169_lunaria-2-b";

				else
									npc_talk_index = "n1169_lunaria-2-a";

				end
	end
	if npc_talk_index == "n1169_lunaria-2-move_02" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1203_lunaria--------------------------------------------------------------------------------
function sq15_4360_shadow_in_mirror_OnTalk_n1203_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1203_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1203_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1203_lunaria-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1203_lunaria-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1203_lunaria-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1203_lunaria-2-b" then 
	end
	if npc_talk_index == "n1203_lunaria-2-c" then 
	end
	if npc_talk_index == "n1203_lunaria-2-ck_job02_01" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 62  then
									npc_talk_index = "n1203_lunaria-2-d";

				else
									npc_talk_index = "n1203_lunaria-2-j";

				end
	end
	if npc_talk_index == "n1203_lunaria-2-e" then 
	end
	if npc_talk_index == "n1203_lunaria-2-f" then 
	end
	if npc_talk_index == "n1203_lunaria-2-i" then 
	end
	if npc_talk_index == "n1203_lunaria-2-ripper_1" then 
				npc_talk_index = "n1203_lunaria-2-h";
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_user_SetSecondJobSkill( pRoom,  userObjID, 63 );
	end
	if npc_talk_index == "n1203_lunaria-2-raven_1" then 
				npc_talk_index = "n1203_lunaria-2-h";
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_user_SetSecondJobSkill( pRoom,  userObjID, 64 );
	end
	if npc_talk_index == "n1203_lunaria-2-open_ui2" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1203_lunaria-2-k" then 
	end
	if npc_talk_index == "n1203_lunaria-2-l" then 
	end
	if npc_talk_index == "n1203_lunaria-2-i" then 
	end
	if npc_talk_index == "n1203_lunaria-4-b" then 
	end
	if npc_talk_index == "n1203_lunaria-4-h" then 
	end
	if npc_talk_index == "n1203_lunaria-4-f" then 
	end
	if npc_talk_index == "n1203_lunaria-4-ripper_2" then 
				npc_talk_index = "n1203_lunaria-3";
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_user_SetSecondJobSkill( pRoom,  userObjID, 63 );
	end
	if npc_talk_index == "n1203_lunaria-4-raven_2" then 
				npc_talk_index = "n1203_lunaria-3";
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_user_SetSecondJobSkill( pRoom,  userObjID, 64 );
	end
	if npc_talk_index == "n1203_lunaria-4-e" then 
	end
	if npc_talk_index == "n1203_lunaria-4-b" then 
	end
	if npc_talk_index == "n1203_lunaria-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

				if api_quest_HasQuestItem( pRoom, userObjID, 300577, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300577, api_quest_HasQuestItem( pRoom, userObjID, 300577, 1));
				end
	end
	if npc_talk_index == "n1203_lunaria-4-g" then 
	end
	if npc_talk_index == "n1203_lunaria-4-open_ui4" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1203_lunaria-4-g" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n575_shadow_meow--------------------------------------------------------------------------------
function sq15_4360_shadow_in_mirror_OnTalk_n575_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n575_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n575_shadow_meow-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n575_shadow_meow-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n575_shadow_meow-accepting-a" then
				if api_user_GetUserJobID( pRoom, userObjID) == 63   or api_user_GetUserJobID( pRoom, userObjID) == 64   or api_user_GetUserJobID( pRoom, userObjID) == 68   or api_user_GetUserJobID( pRoom, userObjID) == 69  then
									npc_talk_index = "n575_shadow_meow-accepting-b";

				else
									npc_talk_index = "n575_shadow_meow-accepting-a";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 end 

				end

	end
	if npc_talk_index == "n575_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4360, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4360, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4360, 1);
				npc_talk_index = "n575_shadow_meow-1";

	end
	if npc_talk_index == "n575_shadow_meow-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, false);
			 end 

	end
	if npc_talk_index == "n575_shadow_meow-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,4360, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4360, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4360, 1);
				npc_talk_index = "n575_shadow_meow-5";
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

	end
	if npc_talk_index == "n575_shadow_meow-5-ck_j_last11" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 62  then
									npc_talk_index = "n575_shadow_meow-5-c";

				else
									npc_talk_index = "n575_shadow_meow-5-c";

				end
	end
	if npc_talk_index == "n575_shadow_meow-5-b" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-d" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-e" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-b" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 end 
	end
	if npc_talk_index == "n575_shadow_meow-5-b" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 end 
	end
	if npc_talk_index == "n575_shadow_meow-5-b" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-chg_ripper" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 63);
				npc_talk_index = "n575_shadow_meow-5-h";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n575_shadow_meow-5-chg_raven" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 64);
				npc_talk_index = "n575_shadow_meow-5-h";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n575_shadow_meow-5-j" then 
	end
	if npc_talk_index == "n575_shadow_meow-5-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43600, questID, 1);
			 end 
	end
	if npc_talk_index == "n575_shadow_meow-5-l" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n575_shadow_meow-5-h" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4360_shadow_in_mirror_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4360);
end

function sq15_4360_shadow_in_mirror_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4360);
end

function sq15_4360_shadow_in_mirror_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4360);
	local questID=4360;
end

function sq15_4360_shadow_in_mirror_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4360_shadow_in_mirror_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4360_shadow_in_mirror_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4360, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4360, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4360, 1);
				npc_talk_index = "n575_shadow_meow-1";
end

</GameServer>