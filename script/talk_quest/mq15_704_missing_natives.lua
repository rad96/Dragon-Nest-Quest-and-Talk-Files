<VillageServer>

function mq15_704_missing_natives_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 468 then
		mq15_704_missing_natives_OnTalk_n468_empty_place(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_704_missing_natives_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 705 then
		mq15_704_missing_natives_OnTalk_n705_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_704_missing_natives_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 720 then
		mq15_704_missing_natives_OnTalk_n720_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n468_empty_place--------------------------------------------------------------------------------
function mq15_704_missing_natives_OnTalk_n468_empty_place(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n468_empty_place-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n468_empty_place-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n468_empty_place-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n468_empty_place-3-b" then 
	end
	if npc_talk_index == "n468_empty_place-3-c" then 
	end
	if npc_talk_index == "n468_empty_place-3-d" then 
	end
	if npc_talk_index == "n468_empty_place-3-e" then 
	end
	if npc_talk_index == "n468_empty_place-3-f" then 
	end
	if npc_talk_index == "n468_empty_place-3-g" then 
	end
	if npc_talk_index == "n468_empty_place-3-h" then 
	end
	if npc_talk_index == "n468_empty_place-3-i" then 
	end
	if npc_talk_index == "n468_empty_place-3-j" then 
	end
	if npc_talk_index == "n468_empty_place-3-k" then 
	end
	if npc_talk_index == "n468_empty_place-3-l" then 
	end
	if npc_talk_index == "n468_empty_place-3-m" then 
	end
	if npc_talk_index == "n468_empty_place-3-n" then 
	end
	if npc_talk_index == "n468_empty_place-3-o" then 
	end
	if npc_talk_index == "n468_empty_place-3-p" then 
	end
	if npc_talk_index == "n468_empty_place-3-q" then 
	end
	if npc_talk_index == "n468_empty_place-3-r" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7041, true);
				 api_quest_RewardQuestUser(userObjID, 7041, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7042, true);
				 api_quest_RewardQuestUser(userObjID, 7042, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7043, true);
				 api_quest_RewardQuestUser(userObjID, 7043, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7044, true);
				 api_quest_RewardQuestUser(userObjID, 7044, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7045, true);
				 api_quest_RewardQuestUser(userObjID, 7045, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7046, true);
				 api_quest_RewardQuestUser(userObjID, 7046, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7041, true);
				 api_quest_RewardQuestUser(userObjID, 7041, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7041, true);
				 api_quest_RewardQuestUser(userObjID, 7041, questID, 1);
			 end 
	end
	if npc_talk_index == "n468_empty_place-3-s" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 705, 2);
					api_quest_SetQuestStep(userObjID, 705, 1);
					api_quest_SetJournalStep(userObjID, 705, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n468_empty_place-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "-1", "mq15_705_doubtful_proposal.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_704_missing_natives_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n701_oldkarakule-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-2-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-o" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-p" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-q" then 
	end
	if npc_talk_index == "n701_oldkarakule-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n705_sidel--------------------------------------------------------------------------------
function mq15_704_missing_natives_OnTalk_n705_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n705_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n705_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n705_sidel-1-b" then 
	end
	if npc_talk_index == "n705_sidel-1-c" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_704_missing_natives_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,704, 2);
				api_quest_SetJournalStep(userObjID,704, 1);
				api_quest_SetQuestStep(userObjID,704, 1);
				npc_talk_index = "n707_sidel-1";

	end
	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n720_charty--------------------------------------------------------------------------------
function mq15_704_missing_natives_OnTalk_n720_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n720_charty-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_704_missing_natives_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 704);
end

function mq15_704_missing_natives_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 704);
end

function mq15_704_missing_natives_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 704);
	local questID=704;
end

function mq15_704_missing_natives_OnRemoteStart( userObjID, questID )
end

function mq15_704_missing_natives_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_704_missing_natives_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,704, 2);
				api_quest_SetJournalStep(userObjID,704, 1);
				api_quest_SetQuestStep(userObjID,704, 1);
				npc_talk_index = "n707_sidel-1";
end

</VillageServer>

<GameServer>
function mq15_704_missing_natives_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 468 then
		mq15_704_missing_natives_OnTalk_n468_empty_place( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_704_missing_natives_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 705 then
		mq15_704_missing_natives_OnTalk_n705_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_704_missing_natives_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 720 then
		mq15_704_missing_natives_OnTalk_n720_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n468_empty_place--------------------------------------------------------------------------------
function mq15_704_missing_natives_OnTalk_n468_empty_place( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n468_empty_place-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n468_empty_place-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n468_empty_place-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n468_empty_place-3-b" then 
	end
	if npc_talk_index == "n468_empty_place-3-c" then 
	end
	if npc_talk_index == "n468_empty_place-3-d" then 
	end
	if npc_talk_index == "n468_empty_place-3-e" then 
	end
	if npc_talk_index == "n468_empty_place-3-f" then 
	end
	if npc_talk_index == "n468_empty_place-3-g" then 
	end
	if npc_talk_index == "n468_empty_place-3-h" then 
	end
	if npc_talk_index == "n468_empty_place-3-i" then 
	end
	if npc_talk_index == "n468_empty_place-3-j" then 
	end
	if npc_talk_index == "n468_empty_place-3-k" then 
	end
	if npc_talk_index == "n468_empty_place-3-l" then 
	end
	if npc_talk_index == "n468_empty_place-3-m" then 
	end
	if npc_talk_index == "n468_empty_place-3-n" then 
	end
	if npc_talk_index == "n468_empty_place-3-o" then 
	end
	if npc_talk_index == "n468_empty_place-3-p" then 
	end
	if npc_talk_index == "n468_empty_place-3-q" then 
	end
	if npc_talk_index == "n468_empty_place-3-r" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7041, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7041, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7042, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7042, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7043, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7043, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7044, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7044, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7045, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7045, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7046, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7046, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7041, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7041, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7041, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7041, questID, 1);
			 end 
	end
	if npc_talk_index == "n468_empty_place-3-s" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 705, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 705, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 705, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n468_empty_place-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "-1", "mq15_705_doubtful_proposal.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_704_missing_natives_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n701_oldkarakule-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-2-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-o" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-p" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-q" then 
	end
	if npc_talk_index == "n701_oldkarakule-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n705_sidel--------------------------------------------------------------------------------
function mq15_704_missing_natives_OnTalk_n705_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n705_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n705_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n705_sidel-1-b" then 
	end
	if npc_talk_index == "n705_sidel-1-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_704_missing_natives_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,704, 2);
				api_quest_SetJournalStep( pRoom, userObjID,704, 1);
				api_quest_SetQuestStep( pRoom, userObjID,704, 1);
				npc_talk_index = "n707_sidel-1";

	end
	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n720_charty--------------------------------------------------------------------------------
function mq15_704_missing_natives_OnTalk_n720_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n720_charty-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_704_missing_natives_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 704);
end

function mq15_704_missing_natives_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 704);
end

function mq15_704_missing_natives_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 704);
	local questID=704;
end

function mq15_704_missing_natives_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_704_missing_natives_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_704_missing_natives_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,704, 2);
				api_quest_SetJournalStep( pRoom, userObjID,704, 1);
				api_quest_SetQuestStep( pRoom, userObjID,704, 1);
				npc_talk_index = "n707_sidel-1";
end

</GameServer>