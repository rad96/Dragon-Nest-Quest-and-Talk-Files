<VillageServer>

function sq15_770_ancient_recipe_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 713 then
		sq15_770_ancient_recipe_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n713_soceress_tamara--------------------------------------------------------------------------------
function sq15_770_ancient_recipe_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n713_soceress_tamara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7700, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7700, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7700, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7700, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7700, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7700, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7700, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7700, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7700, false);
			 end 

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-acceppted" then
				api_quest_AddQuest(userObjID,770, 1);
				api_quest_SetJournalStep(userObjID,770, 1);
				api_quest_SetQuestStep(userObjID,770, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1408, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1409, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201408, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201409, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300401, 1);
				npc_talk_index = "n713_soceress_tamara-1";

	end
	if npc_talk_index == "n713_soceress_tamara-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7700, true);
				 api_quest_RewardQuestUser(userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7700, true);
				 api_quest_RewardQuestUser(userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7700, true);
				 api_quest_RewardQuestUser(userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7700, true);
				 api_quest_RewardQuestUser(userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7700, true);
				 api_quest_RewardQuestUser(userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7700, true);
				 api_quest_RewardQuestUser(userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7700, true);
				 api_quest_RewardQuestUser(userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7700, true);
				 api_quest_RewardQuestUser(userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7700, true);
				 api_quest_RewardQuestUser(userObjID, 7700, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-2-c" then 

				if api_quest_HasQuestItem(userObjID, 300401, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300401, api_quest_HasQuestItem(userObjID, 300401, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_770_ancient_recipe_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 770);
	if qstep == 1 and CountIndex == 1408 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300401, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300401, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300401 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 1409 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300401, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300401, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201408 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300401, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300401, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201409 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300401, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300401, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_770_ancient_recipe_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 770);
	if qstep == 1 and CountIndex == 1408 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300401 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1409 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201408 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201409 and Count >= TargetCount  then

	end
end

function sq15_770_ancient_recipe_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 770);
	local questID=770;
end

function sq15_770_ancient_recipe_OnRemoteStart( userObjID, questID )
end

function sq15_770_ancient_recipe_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_770_ancient_recipe_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq15_770_ancient_recipe_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 713 then
		sq15_770_ancient_recipe_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n713_soceress_tamara--------------------------------------------------------------------------------
function sq15_770_ancient_recipe_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n713_soceress_tamara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, false);
			 end 

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,770, 1);
				api_quest_SetJournalStep( pRoom, userObjID,770, 1);
				api_quest_SetQuestStep( pRoom, userObjID,770, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1408, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1409, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201408, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201409, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300401, 1);
				npc_talk_index = "n713_soceress_tamara-1";

	end
	if npc_talk_index == "n713_soceress_tamara-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7700, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-2-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300401, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300401, api_quest_HasQuestItem( pRoom, userObjID, 300401, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_770_ancient_recipe_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 770);
	if qstep == 1 and CountIndex == 1408 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300401, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300401, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300401 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 1409 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300401, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300401, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201408 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300401, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300401, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201409 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300401, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300401, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_770_ancient_recipe_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 770);
	if qstep == 1 and CountIndex == 1408 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300401 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1409 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201408 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201409 and Count >= TargetCount  then

	end
end

function sq15_770_ancient_recipe_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 770);
	local questID=770;
end

function sq15_770_ancient_recipe_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_770_ancient_recipe_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_770_ancient_recipe_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>