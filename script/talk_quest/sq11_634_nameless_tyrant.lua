<VillageServer>

function sq11_634_nameless_tyrant_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 38 then
		sq11_634_nameless_tyrant_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function sq11_634_nameless_tyrant_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-g" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6340, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6340, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6340, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6340, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6340, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6340, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6340, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6340, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6340, false);
			 end 

	end
	if npc_talk_index == "n038_royal_magician_kalaen-accepting-acceptted" then
				api_quest_AddQuest(userObjID,634, 1);
				api_quest_SetJournalStep(userObjID,634, 1);
				api_quest_SetQuestStep(userObjID,634, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 201159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300349, 1);

	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6340, true);
				 api_quest_RewardQuestUser(userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6340, true);
				 api_quest_RewardQuestUser(userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6340, true);
				 api_quest_RewardQuestUser(userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6340, true);
				 api_quest_RewardQuestUser(userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6340, true);
				 api_quest_RewardQuestUser(userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6340, true);
				 api_quest_RewardQuestUser(userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6340, true);
				 api_quest_RewardQuestUser(userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6340, true);
				 api_quest_RewardQuestUser(userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6340, true);
				 api_quest_RewardQuestUser(userObjID, 6340, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 

				if api_quest_HasQuestItem(userObjID, 300349, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300349, api_quest_HasQuestItem(userObjID, 300349, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 635, 1);
					api_quest_SetQuestStep(userObjID, 635, 1);
					api_quest_SetJournalStep(userObjID, 635, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n038_royal_magician_kalaen-1", "sq11_635_lucifina_diary.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_634_nameless_tyrant_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 634);
	if qstep == 1 and CountIndex == 201159 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300349, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300349, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 1159 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300349, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300349, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300349 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function sq11_634_nameless_tyrant_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 634);
	if qstep == 1 and CountIndex == 201159 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1159 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300349 and Count >= TargetCount  then

	end
end

function sq11_634_nameless_tyrant_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 634);
	local questID=634;
end

function sq11_634_nameless_tyrant_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 201159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300349, 1);
end

function sq11_634_nameless_tyrant_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_634_nameless_tyrant_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,634, 1);
				api_quest_SetJournalStep(userObjID,634, 1);
				api_quest_SetQuestStep(userObjID,634, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 201159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300349, 1);
end

</VillageServer>

<GameServer>
function sq11_634_nameless_tyrant_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 38 then
		sq11_634_nameless_tyrant_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function sq11_634_nameless_tyrant_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-g" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, false);
			 end 

	end
	if npc_talk_index == "n038_royal_magician_kalaen-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,634, 1);
				api_quest_SetJournalStep( pRoom, userObjID,634, 1);
				api_quest_SetQuestStep( pRoom, userObjID,634, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 201159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300349, 1);

	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6340, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300349, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300349, api_quest_HasQuestItem( pRoom, userObjID, 300349, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 635, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 635, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 635, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n038_royal_magician_kalaen-1", "sq11_635_lucifina_diary.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_634_nameless_tyrant_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 634);
	if qstep == 1 and CountIndex == 201159 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300349, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300349, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 1159 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300349, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300349, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300349 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function sq11_634_nameless_tyrant_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 634);
	if qstep == 1 and CountIndex == 201159 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1159 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300349 and Count >= TargetCount  then

	end
end

function sq11_634_nameless_tyrant_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 634);
	local questID=634;
end

function sq11_634_nameless_tyrant_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 201159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300349, 1);
end

function sq11_634_nameless_tyrant_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_634_nameless_tyrant_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,634, 1);
				api_quest_SetJournalStep( pRoom, userObjID,634, 1);
				api_quest_SetQuestStep( pRoom, userObjID,634, 1);
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 201159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300349, 1);
end

</GameServer>