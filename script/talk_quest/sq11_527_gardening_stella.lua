<VillageServer>

function sq11_527_gardening_stella_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 45 then
		sq11_527_gardening_stella_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_527_gardening_stella_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5271, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5272, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5273, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5274, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5275, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5276, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5277, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5278, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5279, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-accepted" then
				api_quest_AddQuest(userObjID,527, 1);
				api_quest_SetJournalStep(userObjID,527, 1);
				api_quest_SetQuestStep(userObjID,527, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 910, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200910, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400105, 3);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5271, true);
				 api_quest_RewardQuestUser(userObjID, 5271, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5272, true);
				 api_quest_RewardQuestUser(userObjID, 5272, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5273, true);
				 api_quest_RewardQuestUser(userObjID, 5273, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5274, true);
				 api_quest_RewardQuestUser(userObjID, 5274, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5275, true);
				 api_quest_RewardQuestUser(userObjID, 5275, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5276, true);
				 api_quest_RewardQuestUser(userObjID, 5276, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5277, true);
				 api_quest_RewardQuestUser(userObjID, 5277, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5278, true);
				 api_quest_RewardQuestUser(userObjID, 5278, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5279, true);
				 api_quest_RewardQuestUser(userObjID, 5279, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 

				if api_quest_HasQuestItem(userObjID, 400105, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400105, api_quest_HasQuestItem(userObjID, 400105, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 528, 1);
					api_quest_SetQuestStep(userObjID, 528, 1);
					api_quest_SetJournalStep(userObjID, 528, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n045_soceress_master_stella-1", "sq11_528_valid_conclusion.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_527_gardening_stella_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 527);
	if qstep == 1 and CountIndex == 910 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400105, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400105, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400105 then

	end
	if qstep == 1 and CountIndex == 200910 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400105, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400105, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_527_gardening_stella_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 527);
	if qstep == 1 and CountIndex == 910 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400105 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200910 and Count >= TargetCount  then

	end
end

function sq11_527_gardening_stella_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 527);
	local questID=527;
end

function sq11_527_gardening_stella_OnRemoteStart( userObjID, questID )
end

function sq11_527_gardening_stella_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_527_gardening_stella_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_527_gardening_stella_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 45 then
		sq11_527_gardening_stella_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_527_gardening_stella_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5271, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5272, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5273, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5274, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5275, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5276, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5277, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5278, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5279, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-accepted" then
				api_quest_AddQuest( pRoom, userObjID,527, 1);
				api_quest_SetJournalStep( pRoom, userObjID,527, 1);
				api_quest_SetQuestStep( pRoom, userObjID,527, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 910, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200910, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400105, 3);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5271, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5271, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5272, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5272, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5273, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5273, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5274, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5274, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5275, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5275, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5276, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5276, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5277, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5277, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5278, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5278, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5279, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5279, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400105, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400105, api_quest_HasQuestItem( pRoom, userObjID, 400105, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 528, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 528, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 528, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-1", "sq11_528_valid_conclusion.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_527_gardening_stella_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 527);
	if qstep == 1 and CountIndex == 910 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400105, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400105, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400105 then

	end
	if qstep == 1 and CountIndex == 200910 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400105, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400105, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_527_gardening_stella_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 527);
	if qstep == 1 and CountIndex == 910 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400105 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200910 and Count >= TargetCount  then

	end
end

function sq11_527_gardening_stella_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 527);
	local questID=527;
end

function sq11_527_gardening_stella_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_527_gardening_stella_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_527_gardening_stella_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>