<VillageServer>

function mqc16_9566_twisted_fate_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1065 then
		mqc16_9566_twisted_fate_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1774 then
		mqc16_9566_twisted_fate_OnTalk_n1774_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1779 then
		mqc16_9566_twisted_fate_OnTalk_n1779_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1780 then
		mqc16_9566_twisted_fate_OnTalk_n1780_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1782 then
		mqc16_9566_twisted_fate_OnTalk_n1782_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc16_9566_twisted_fate_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1065_geraint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-accepting-acceptted_1" then
				api_quest_AddQuest(userObjID,9566, 2);
				api_quest_SetJournalStep(userObjID,9566, 1);
				api_quest_SetQuestStep(userObjID,9566, 1);
				npc_talk_index = "n1065_geraint_kid-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1774_gereint_kid--------------------------------------------------------------------------------
function mqc16_9566_twisted_fate_OnTalk_n1774_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1774_gereint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1774_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1774_gereint_kid-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,9566, 2);
				api_quest_SetJournalStep(userObjID,9566, 1);
				api_quest_SetQuestStep(userObjID,9566, 1);
				npc_talk_index = "n1774_gereint_kid-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1779_gereint_kid--------------------------------------------------------------------------------
function mqc16_9566_twisted_fate_OnTalk_n1779_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1779_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1780_book--------------------------------------------------------------------------------
function mqc16_9566_twisted_fate_OnTalk_n1780_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1780_book-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1780_book-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1782_rubinat--------------------------------------------------------------------------------
function mqc16_9566_twisted_fate_OnTalk_n1782_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1782_rubinat-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1782_rubinat-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1782_rubinat-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1782_rubinat-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1782_rubinat-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1782_rubinat-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1782_rubinat-3-b" then 
	end
	if npc_talk_index == "n1782_rubinat-3-c" then 
	end
	if npc_talk_index == "n1782_rubinat-3-d" then 
	end
	if npc_talk_index == "n1782_rubinat-3-e" then 
	end
	if npc_talk_index == "n1782_rubinat-3-f" then 
	end
	if npc_talk_index == "n1782_rubinat-3-g" then 
	end
	if npc_talk_index == "n1782_rubinat-3-h" then 
	end
	if npc_talk_index == "n1782_rubinat-3-i" then 
	end
	if npc_talk_index == "n1782_rubinat-3-j" then 
	end
	if npc_talk_index == "n1782_rubinat-3-k" then 
	end
	if npc_talk_index == "n1782_rubinat-3-l" then 
	end
	if npc_talk_index == "n1782_rubinat-3-m" then 
	end
	if npc_talk_index == "n1782_rubinat-3-n" then 
	end
	if npc_talk_index == "n1782_rubinat-3-o" then 
	end
	if npc_talk_index == "n1782_rubinat-3-p" then 
	end
	if npc_talk_index == "n1782_rubinat-3-q" then 
	end
	if npc_talk_index == "n1782_rubinat-3-r" then 
	end
	if npc_talk_index == "n1782_rubinat-3-s" then 
	end
	if npc_talk_index == "n1782_rubinat-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1782_rubinat-4-b" then 
	end
	if npc_talk_index == "n1782_rubinat-4-b" then 
	end
	if npc_talk_index == "n1782_rubinat-4-b" then 
	end
	if npc_talk_index == "n1782_rubinat-4-c" then 
	end
	if npc_talk_index == "n1782_rubinat-4-d" then 
	end
	if npc_talk_index == "n1782_rubinat-4-e" then 
	end
	if npc_talk_index == "n1782_rubinat-4-f" then 
	end
	if npc_talk_index == "n1782_rubinat-4-g" then 
	end
	if npc_talk_index == "n1782_rubinat-4-h" then 
	end
	if npc_talk_index == "n1782_rubinat-4-i" then 
	end
	if npc_talk_index == "n1782_rubinat-4-j" then 
	end
	if npc_talk_index == "n1782_rubinat-4-k" then 
	end
	if npc_talk_index == "n1782_rubinat-4-l" then 
	end
	if npc_talk_index == "n1782_rubinat-4-m" then 
	end
	if npc_talk_index == "n1782_rubinat-4-n" then 
	end
	if npc_talk_index == "n1782_rubinat-4-o" then 
	end
	if npc_talk_index == "n1782_rubinat-4-p" then 
	end
	if npc_talk_index == "n1782_rubinat-4-q" then 
	end
	if npc_talk_index == "n1782_rubinat-4-r" then 
	end
	if npc_talk_index == "n1782_rubinat-4-s" then 
	end
	if npc_talk_index == "n1782_rubinat-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n1782_rubinat-6-b" then 
	end
	if npc_talk_index == "n1782_rubinat-6-c" then 
	end
	if npc_talk_index == "n1782_rubinat-6-d" then 
	end
	if npc_talk_index == "n1782_rubinat-6-e" then 
	end
	if npc_talk_index == "n1782_rubinat-6-f" then 
	end
	if npc_talk_index == "n1782_rubinat-6-g" then 
	end
	if npc_talk_index == "n1782_rubinat-6-h" then 
	end
	if npc_talk_index == "n1782_rubinat-6-i" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-k" then 
	end
	if npc_talk_index == "n1782_rubinat-6-l" then 
	end
	if npc_talk_index == "n1782_rubinat-6-m" then 
	end
	if npc_talk_index == "n1782_rubinat-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
	end
	if npc_talk_index == "n1782_rubinat-7-b" then 
	end
	if npc_talk_index == "n1782_rubinat-7-b" then 
	end
	if npc_talk_index == "n1782_rubinat-7-c" then 
	end
	if npc_talk_index == "n1782_rubinat-7-d" then 
	end
	if npc_talk_index == "n1782_rubinat-7-e" then 
	end
	if npc_talk_index == "n1782_rubinat-7-f" then 
	end
	if npc_talk_index == "n1782_rubinat-7-g" then 
	end
	if npc_talk_index == "n1782_rubinat-7-h" then 
	end
	if npc_talk_index == "n1782_rubinat-7-i" then 
	end
	if npc_talk_index == "n1782_rubinat-7-i" then 
	end
	if npc_talk_index == "n1782_rubinat-7-j" then 
	end
	if npc_talk_index == "n1782_rubinat-7-k" then 
	end
	if npc_talk_index == "n1782_rubinat-7-l" then 
	end
	if npc_talk_index == "n1782_rubinat-7-l" then 
	end
	if npc_talk_index == "n1782_rubinat-7-m" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95660, true);
				 api_quest_RewardQuestUser(userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95660, true);
				 api_quest_RewardQuestUser(userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95660, true);
				 api_quest_RewardQuestUser(userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95660, true);
				 api_quest_RewardQuestUser(userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95660, true);
				 api_quest_RewardQuestUser(userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95660, true);
				 api_quest_RewardQuestUser(userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95660, true);
				 api_quest_RewardQuestUser(userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95660, true);
				 api_quest_RewardQuestUser(userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95660, true);
				 api_quest_RewardQuestUser(userObjID, 95660, questID, 1);
			 end 
	end
	if npc_talk_index == "n1782_rubinat-7-n" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9567, 2);
					api_quest_SetQuestStep(userObjID, 9567, 1);
					api_quest_SetJournalStep(userObjID, 9567, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1782_rubinat-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1782_rubinat-1", "mqc16_9567_mind_of_dragon.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9566_twisted_fate_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9566);
end

function mqc16_9566_twisted_fate_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9566);
end

function mqc16_9566_twisted_fate_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9566);
	local questID=9566;
end

function mqc16_9566_twisted_fate_OnRemoteStart( userObjID, questID )
end

function mqc16_9566_twisted_fate_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc16_9566_twisted_fate_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mqc16_9566_twisted_fate_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1065 then
		mqc16_9566_twisted_fate_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1774 then
		mqc16_9566_twisted_fate_OnTalk_n1774_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1779 then
		mqc16_9566_twisted_fate_OnTalk_n1779_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1780 then
		mqc16_9566_twisted_fate_OnTalk_n1780_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1782 then
		mqc16_9566_twisted_fate_OnTalk_n1782_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc16_9566_twisted_fate_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1065_geraint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-accepting-acceptted_1" then
				api_quest_AddQuest( pRoom, userObjID,9566, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9566, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9566, 1);
				npc_talk_index = "n1065_geraint_kid-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1774_gereint_kid--------------------------------------------------------------------------------
function mqc16_9566_twisted_fate_OnTalk_n1774_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1774_gereint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1774_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1774_gereint_kid-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,9566, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9566, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9566, 1);
				npc_talk_index = "n1774_gereint_kid-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1779_gereint_kid--------------------------------------------------------------------------------
function mqc16_9566_twisted_fate_OnTalk_n1779_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1779_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1780_book--------------------------------------------------------------------------------
function mqc16_9566_twisted_fate_OnTalk_n1780_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1780_book-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1780_book-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1782_rubinat--------------------------------------------------------------------------------
function mqc16_9566_twisted_fate_OnTalk_n1782_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1782_rubinat-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1782_rubinat-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1782_rubinat-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1782_rubinat-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1782_rubinat-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1782_rubinat-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1782_rubinat-3-b" then 
	end
	if npc_talk_index == "n1782_rubinat-3-c" then 
	end
	if npc_talk_index == "n1782_rubinat-3-d" then 
	end
	if npc_talk_index == "n1782_rubinat-3-e" then 
	end
	if npc_talk_index == "n1782_rubinat-3-f" then 
	end
	if npc_talk_index == "n1782_rubinat-3-g" then 
	end
	if npc_talk_index == "n1782_rubinat-3-h" then 
	end
	if npc_talk_index == "n1782_rubinat-3-i" then 
	end
	if npc_talk_index == "n1782_rubinat-3-j" then 
	end
	if npc_talk_index == "n1782_rubinat-3-k" then 
	end
	if npc_talk_index == "n1782_rubinat-3-l" then 
	end
	if npc_talk_index == "n1782_rubinat-3-m" then 
	end
	if npc_talk_index == "n1782_rubinat-3-n" then 
	end
	if npc_talk_index == "n1782_rubinat-3-o" then 
	end
	if npc_talk_index == "n1782_rubinat-3-p" then 
	end
	if npc_talk_index == "n1782_rubinat-3-q" then 
	end
	if npc_talk_index == "n1782_rubinat-3-r" then 
	end
	if npc_talk_index == "n1782_rubinat-3-s" then 
	end
	if npc_talk_index == "n1782_rubinat-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1782_rubinat-4-b" then 
	end
	if npc_talk_index == "n1782_rubinat-4-b" then 
	end
	if npc_talk_index == "n1782_rubinat-4-b" then 
	end
	if npc_talk_index == "n1782_rubinat-4-c" then 
	end
	if npc_talk_index == "n1782_rubinat-4-d" then 
	end
	if npc_talk_index == "n1782_rubinat-4-e" then 
	end
	if npc_talk_index == "n1782_rubinat-4-f" then 
	end
	if npc_talk_index == "n1782_rubinat-4-g" then 
	end
	if npc_talk_index == "n1782_rubinat-4-h" then 
	end
	if npc_talk_index == "n1782_rubinat-4-i" then 
	end
	if npc_talk_index == "n1782_rubinat-4-j" then 
	end
	if npc_talk_index == "n1782_rubinat-4-k" then 
	end
	if npc_talk_index == "n1782_rubinat-4-l" then 
	end
	if npc_talk_index == "n1782_rubinat-4-m" then 
	end
	if npc_talk_index == "n1782_rubinat-4-n" then 
	end
	if npc_talk_index == "n1782_rubinat-4-o" then 
	end
	if npc_talk_index == "n1782_rubinat-4-p" then 
	end
	if npc_talk_index == "n1782_rubinat-4-q" then 
	end
	if npc_talk_index == "n1782_rubinat-4-r" then 
	end
	if npc_talk_index == "n1782_rubinat-4-s" then 
	end
	if npc_talk_index == "n1782_rubinat-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n1782_rubinat-6-b" then 
	end
	if npc_talk_index == "n1782_rubinat-6-c" then 
	end
	if npc_talk_index == "n1782_rubinat-6-d" then 
	end
	if npc_talk_index == "n1782_rubinat-6-e" then 
	end
	if npc_talk_index == "n1782_rubinat-6-f" then 
	end
	if npc_talk_index == "n1782_rubinat-6-g" then 
	end
	if npc_talk_index == "n1782_rubinat-6-h" then 
	end
	if npc_talk_index == "n1782_rubinat-6-i" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-j" then 
	end
	if npc_talk_index == "n1782_rubinat-6-k" then 
	end
	if npc_talk_index == "n1782_rubinat-6-l" then 
	end
	if npc_talk_index == "n1782_rubinat-6-m" then 
	end
	if npc_talk_index == "n1782_rubinat-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
	end
	if npc_talk_index == "n1782_rubinat-7-b" then 
	end
	if npc_talk_index == "n1782_rubinat-7-b" then 
	end
	if npc_talk_index == "n1782_rubinat-7-c" then 
	end
	if npc_talk_index == "n1782_rubinat-7-d" then 
	end
	if npc_talk_index == "n1782_rubinat-7-e" then 
	end
	if npc_talk_index == "n1782_rubinat-7-f" then 
	end
	if npc_talk_index == "n1782_rubinat-7-g" then 
	end
	if npc_talk_index == "n1782_rubinat-7-h" then 
	end
	if npc_talk_index == "n1782_rubinat-7-i" then 
	end
	if npc_talk_index == "n1782_rubinat-7-i" then 
	end
	if npc_talk_index == "n1782_rubinat-7-j" then 
	end
	if npc_talk_index == "n1782_rubinat-7-k" then 
	end
	if npc_talk_index == "n1782_rubinat-7-l" then 
	end
	if npc_talk_index == "n1782_rubinat-7-l" then 
	end
	if npc_talk_index == "n1782_rubinat-7-m" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95660, questID, 1);
			 end 
	end
	if npc_talk_index == "n1782_rubinat-7-n" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9567, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9567, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9567, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1782_rubinat-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1782_rubinat-1", "mqc16_9567_mind_of_dragon.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9566_twisted_fate_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9566);
end

function mqc16_9566_twisted_fate_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9566);
end

function mqc16_9566_twisted_fate_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9566);
	local questID=9566;
end

function mqc16_9566_twisted_fate_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc16_9566_twisted_fate_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc16_9566_twisted_fate_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>