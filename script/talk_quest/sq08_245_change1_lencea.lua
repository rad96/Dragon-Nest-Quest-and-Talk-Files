<VillageServer>

function sq08_245_change1_lencea_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1690 then
		sq08_245_change1_lencea_OnTalk_n1690_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1745 then
		sq08_245_change1_lencea_OnTalk_n1745_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1690_eltia--------------------------------------------------------------------------------
function sq08_245_change1_lencea_OnTalk_n1690_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1690_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1690_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1690_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1690_eltia-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1690_eltia-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2450, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2450, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2450, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2450, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2450, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2450, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2450, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2450, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2450, false);
			 end 

	end
	if npc_talk_index == "n1690_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,245, 1);
				api_quest_SetJournalStep(userObjID,245, 1);
				api_quest_SetQuestStep(userObjID,245, 1);
				npc_talk_index = "n1690_eltia-1";

	end
	if npc_talk_index == "n1690_eltia-1-b" then 
	end
	if npc_talk_index == "n1690_eltia-1-c" then 
	end
	if npc_talk_index == "n1690_eltia-1-d" then 
	end
	if npc_talk_index == "n1690_eltia-1-e" then 
	end
	if npc_talk_index == "n1690_eltia-1-f" then 
	end
	if npc_talk_index == "n1690_eltia-1-party_ch01" then 
				if api_user_GetUserJobID(userObjID) == 72  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);

				else
									if api_user_GetUserJobID(userObjID) == 73  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);

				else
									if api_user_GetUserJobID(userObjID) == 74  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);

				else
									if api_user_IsPartymember(userObjID) == 0  then
									api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13520,5);

				else
									npc_talk_index = "n1690_eltia-1-g";

				end

				end

				end

				end
	end
	if npc_talk_index == "n1690_eltia-1-party_ch02" then 
				if api_user_GetUserJobID(userObjID) == 72  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);

				else
									if api_user_GetUserJobID(userObjID) == 73  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);

				else
									if api_user_GetUserJobID(userObjID) == 74  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);

				else
									if api_user_IsPartymember(userObjID) == 0  then
									api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13520,5);

				else
									npc_talk_index = "n1690_eltia-1-g";

				end

				end

				end

				end
	end
	if npc_talk_index == "n1690_eltia-2-party_check2" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_user_ChangeMap(userObjID,13520,5);

				else
									npc_talk_index = "n1745_eltia-2-a";

				end
	end
	if npc_talk_index == "n1690_eltia-4-b" then 
	end
	if npc_talk_index == "n1690_eltia-4-c" then 
	end
	if npc_talk_index == "n1690_eltia-4-d" then 
	end
	if npc_talk_index == "n1690_eltia-4-e" then 
	end
	if npc_talk_index == "n1690_eltia-4-f" then 
	end
	if npc_talk_index == "n1690_eltia-4-g" then 
				if api_user_GetUserJobID(userObjID) == 11  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2451, true);
				 api_quest_RewardQuestUser(userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2451, true);
				 api_quest_RewardQuestUser(userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2451, true);
				 api_quest_RewardQuestUser(userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2451, true);
				 api_quest_RewardQuestUser(userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2451, true);
				 api_quest_RewardQuestUser(userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2451, true);
				 api_quest_RewardQuestUser(userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2451, true);
				 api_quest_RewardQuestUser(userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2451, true);
				 api_quest_RewardQuestUser(userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2451, true);
				 api_quest_RewardQuestUser(userObjID, 2451, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2452, true);
				 api_quest_RewardQuestUser(userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2452, true);
				 api_quest_RewardQuestUser(userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2452, true);
				 api_quest_RewardQuestUser(userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2452, true);
				 api_quest_RewardQuestUser(userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2452, true);
				 api_quest_RewardQuestUser(userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2452, true);
				 api_quest_RewardQuestUser(userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2452, true);
				 api_quest_RewardQuestUser(userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2452, true);
				 api_quest_RewardQuestUser(userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2452, true);
				 api_quest_RewardQuestUser(userObjID, 2452, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n1690_eltia-4-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4608, 1);
					api_quest_SetQuestStep(userObjID, 4608, 1);
					api_quest_SetJournalStep(userObjID, 4608, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1690_eltia-4-i" then 
	end
	if npc_talk_index == "n1690_eltia-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1690_eltia-1", "sq08__change_job8.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1745_eltia--------------------------------------------------------------------------------
function sq08_245_change1_lencea_OnTalk_n1745_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1745_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1745_eltia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1745_eltia-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1745_eltia-2-b" then 
	end
	if npc_talk_index == "n1745_eltia-2-open_ui2" then 
				api_ui_OpenJobChange(userObjID)
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1745_eltia-3-open_ui3" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1745_eltia-4-go_home" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_245_change1_lencea_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 245);
end

function sq08_245_change1_lencea_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 245);
end

function sq08_245_change1_lencea_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 245);
	local questID=245;
end

function sq08_245_change1_lencea_OnRemoteStart( userObjID, questID )
end

function sq08_245_change1_lencea_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_245_change1_lencea_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,245, 1);
				api_quest_SetJournalStep(userObjID,245, 1);
				api_quest_SetQuestStep(userObjID,245, 1);
				npc_talk_index = "n1690_eltia-1";
end

</VillageServer>

<GameServer>
function sq08_245_change1_lencea_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1690 then
		sq08_245_change1_lencea_OnTalk_n1690_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1745 then
		sq08_245_change1_lencea_OnTalk_n1745_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1690_eltia--------------------------------------------------------------------------------
function sq08_245_change1_lencea_OnTalk_n1690_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1690_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1690_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1690_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1690_eltia-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1690_eltia-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2450, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2450, false);
			 end 

	end
	if npc_talk_index == "n1690_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,245, 1);
				api_quest_SetJournalStep( pRoom, userObjID,245, 1);
				api_quest_SetQuestStep( pRoom, userObjID,245, 1);
				npc_talk_index = "n1690_eltia-1";

	end
	if npc_talk_index == "n1690_eltia-1-b" then 
	end
	if npc_talk_index == "n1690_eltia-1-c" then 
	end
	if npc_talk_index == "n1690_eltia-1-d" then 
	end
	if npc_talk_index == "n1690_eltia-1-e" then 
	end
	if npc_talk_index == "n1690_eltia-1-f" then 
	end
	if npc_talk_index == "n1690_eltia-1-party_ch01" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 72  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 73  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 74  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

				else
									if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13520,5);

				else
									npc_talk_index = "n1690_eltia-1-g";

				end

				end

				end

				end
	end
	if npc_talk_index == "n1690_eltia-1-party_ch02" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 72  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 73  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 74  then
									npc_talk_index = "n1690_eltia-4";
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

				else
									if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13520,5);

				else
									npc_talk_index = "n1690_eltia-1-g";

				end

				end

				end

				end
	end
	if npc_talk_index == "n1690_eltia-2-party_check2" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_user_ChangeMap( pRoom, userObjID,13520,5);

				else
									npc_talk_index = "n1745_eltia-2-a";

				end
	end
	if npc_talk_index == "n1690_eltia-4-b" then 
	end
	if npc_talk_index == "n1690_eltia-4-c" then 
	end
	if npc_talk_index == "n1690_eltia-4-d" then 
	end
	if npc_talk_index == "n1690_eltia-4-e" then 
	end
	if npc_talk_index == "n1690_eltia-4-f" then 
	end
	if npc_talk_index == "n1690_eltia-4-g" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 11  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2451, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2451, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2451, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2451, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2451, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2451, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2451, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2451, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2451, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2451, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2451, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2452, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2452, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2452, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2452, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2452, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2452, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2452, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2452, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2452, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2452, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2452, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n1690_eltia-4-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4608, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4608, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4608, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1690_eltia-4-i" then 
	end
	if npc_talk_index == "n1690_eltia-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1690_eltia-1", "sq08__change_job8.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1745_eltia--------------------------------------------------------------------------------
function sq08_245_change1_lencea_OnTalk_n1745_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1745_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1745_eltia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1745_eltia-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1745_eltia-2-b" then 
	end
	if npc_talk_index == "n1745_eltia-2-open_ui2" then 
				api_ui_OpenJobChange( pRoom, userObjID)
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1745_eltia-3-open_ui3" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1745_eltia-4-go_home" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_245_change1_lencea_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 245);
end

function sq08_245_change1_lencea_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 245);
end

function sq08_245_change1_lencea_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 245);
	local questID=245;
end

function sq08_245_change1_lencea_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_245_change1_lencea_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_245_change1_lencea_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,245, 1);
				api_quest_SetJournalStep( pRoom, userObjID,245, 1);
				api_quest_SetQuestStep( pRoom, userObjID,245, 1);
				npc_talk_index = "n1690_eltia-1";
end

</GameServer>