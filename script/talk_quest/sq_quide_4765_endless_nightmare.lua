<VillageServer>

function sq_quide_4765_endless_nightmare_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 2016 then
		sq_quide_4765_endless_nightmare_OnTalk_n2016_darklair_praline(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 735 then
		sq_quide_4765_endless_nightmare_OnTalk_n735_darklair_mocha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n2016_darklair_praline--------------------------------------------------------------------------------
function sq_quide_4765_endless_nightmare_OnTalk_n2016_darklair_praline(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2016_darklair_praline-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n2016_darklair_praline-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2016_darklair_praline-2-b" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-c" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-d" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-e" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-f" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-g" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-h" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47650, true);
				 api_quest_RewardQuestUser(userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47650, true);
				 api_quest_RewardQuestUser(userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47650, true);
				 api_quest_RewardQuestUser(userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47650, true);
				 api_quest_RewardQuestUser(userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47650, true);
				 api_quest_RewardQuestUser(userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47650, true);
				 api_quest_RewardQuestUser(userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47650, true);
				 api_quest_RewardQuestUser(userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47650, true);
				 api_quest_RewardQuestUser(userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47650, true);
				 api_quest_RewardQuestUser(userObjID, 47650, questID, 1);
			 end 
	end
	if npc_talk_index == "n2016_darklair_praline-2-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n735_darklair_mocha--------------------------------------------------------------------------------
function sq_quide_4765_endless_nightmare_OnTalk_n735_darklair_mocha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n735_darklair_mocha-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n735_darklair_mocha-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n735_darklair_mocha-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n735_darklair_mocha-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47650, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47650, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47650, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47650, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47650, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47650, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47650, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47650, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47650, false);
			 end 

	end
	if npc_talk_index == "n735_darklair_mocha-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4765, 1);
				api_quest_SetJournalStep(userObjID,4765, 1);
				api_quest_SetQuestStep(userObjID,4765, 1);
				npc_talk_index = "n735_darklair_mocha-1";

	end
	if npc_talk_index == "n735_darklair_mocha-1-b" then 
	end
	if npc_talk_index == "n735_darklair_mocha-1-c" then 
	end
	if npc_talk_index == "n735_darklair_mocha-1-d" then 
	end
	if npc_talk_index == "n735_darklair_mocha-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_quide_4765_endless_nightmare_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4765);
end

function sq_quide_4765_endless_nightmare_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4765);
end

function sq_quide_4765_endless_nightmare_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4765);
	local questID=4765;
end

function sq_quide_4765_endless_nightmare_OnRemoteStart( userObjID, questID )
end

function sq_quide_4765_endless_nightmare_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_quide_4765_endless_nightmare_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4765, 1);
				api_quest_SetJournalStep(userObjID,4765, 1);
				api_quest_SetQuestStep(userObjID,4765, 1);
				npc_talk_index = "n735_darklair_mocha-1";
end

</VillageServer>

<GameServer>
function sq_quide_4765_endless_nightmare_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 2016 then
		sq_quide_4765_endless_nightmare_OnTalk_n2016_darklair_praline( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 735 then
		sq_quide_4765_endless_nightmare_OnTalk_n735_darklair_mocha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n2016_darklair_praline--------------------------------------------------------------------------------
function sq_quide_4765_endless_nightmare_OnTalk_n2016_darklair_praline( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2016_darklair_praline-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n2016_darklair_praline-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2016_darklair_praline-2-b" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-c" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-d" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-e" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-f" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-g" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-h" then 
	end
	if npc_talk_index == "n2016_darklair_praline-2-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47650, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47650, questID, 1);
			 end 
	end
	if npc_talk_index == "n2016_darklair_praline-2-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n735_darklair_mocha--------------------------------------------------------------------------------
function sq_quide_4765_endless_nightmare_OnTalk_n735_darklair_mocha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n735_darklair_mocha-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n735_darklair_mocha-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n735_darklair_mocha-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n735_darklair_mocha-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47650, false);
			 end 

	end
	if npc_talk_index == "n735_darklair_mocha-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4765, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4765, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4765, 1);
				npc_talk_index = "n735_darklair_mocha-1";

	end
	if npc_talk_index == "n735_darklair_mocha-1-b" then 
	end
	if npc_talk_index == "n735_darklair_mocha-1-c" then 
	end
	if npc_talk_index == "n735_darklair_mocha-1-d" then 
	end
	if npc_talk_index == "n735_darklair_mocha-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_quide_4765_endless_nightmare_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4765);
end

function sq_quide_4765_endless_nightmare_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4765);
end

function sq_quide_4765_endless_nightmare_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4765);
	local questID=4765;
end

function sq_quide_4765_endless_nightmare_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_quide_4765_endless_nightmare_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_quide_4765_endless_nightmare_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4765, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4765, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4765, 1);
				npc_talk_index = "n735_darklair_mocha-1";
end

</GameServer>