<VillageServer>

function sq15_4103_picky_about_food1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 277 then
		sq15_4103_picky_about_food1_OnTalk_n277_bluff_dealers_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_pope--------------------------------------------------------------------------------
function sq15_4103_picky_about_food1_OnTalk_n277_bluff_dealers_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n277_bluff_dealers_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n277_bluff_dealers_pope-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n277_bluff_dealers_pope-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_pope-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41030, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41030, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41030, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41030, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41030, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41030, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41030, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41030, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41030, false);
			 end 

	end
	if npc_talk_index == "n277_bluff_dealers_pope-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4103, 1);
				api_quest_SetJournalStep(userObjID,4103, 1);
				api_quest_SetQuestStep(userObjID,4103, 1);
				npc_talk_index = "n277_bluff_dealers_pope-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 700101, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700102, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 600101, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 600102, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300561, 2);

	end
	if npc_talk_index == "n277_bluff_dealers_pope-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41030, true);
				 api_quest_RewardQuestUser(userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41030, true);
				 api_quest_RewardQuestUser(userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41030, true);
				 api_quest_RewardQuestUser(userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41030, true);
				 api_quest_RewardQuestUser(userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41030, true);
				 api_quest_RewardQuestUser(userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41030, true);
				 api_quest_RewardQuestUser(userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41030, true);
				 api_quest_RewardQuestUser(userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41030, true);
				 api_quest_RewardQuestUser(userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41030, true);
				 api_quest_RewardQuestUser(userObjID, 41030, questID, 1);
			 end 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-2-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4104, 1);
					api_quest_SetQuestStep(userObjID, 4104, 1);
					api_quest_SetJournalStep(userObjID, 4104, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300561, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300561, api_quest_HasQuestItem(userObjID, 300561, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n277_bluff_dealers_pope-1", "sq15_4104_picky_about_food2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4103_picky_about_food1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4103);
	if qstep == 1 and CountIndex == 700101 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300561, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300561, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700102 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300561, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300561, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300561 then

	end
	if qstep == 1 and CountIndex == 600101 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300561, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300561, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600102 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300561, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300561, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4103_picky_about_food1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4103);
	if qstep == 1 and CountIndex == 700101 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700102 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300561 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 600101 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600102 and Count >= TargetCount  then

	end
end

function sq15_4103_picky_about_food1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4103);
	local questID=4103;
end

function sq15_4103_picky_about_food1_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 700101, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700102, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 600101, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 600102, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300561, 2);
end

function sq15_4103_picky_about_food1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4103_picky_about_food1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4103, 1);
				api_quest_SetJournalStep(userObjID,4103, 1);
				api_quest_SetQuestStep(userObjID,4103, 1);
				npc_talk_index = "n277_bluff_dealers_pope-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 700101, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700102, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 600101, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 600102, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300561, 2);
end

</VillageServer>

<GameServer>
function sq15_4103_picky_about_food1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 277 then
		sq15_4103_picky_about_food1_OnTalk_n277_bluff_dealers_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_pope--------------------------------------------------------------------------------
function sq15_4103_picky_about_food1_OnTalk_n277_bluff_dealers_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n277_bluff_dealers_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n277_bluff_dealers_pope-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n277_bluff_dealers_pope-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_pope-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, false);
			 end 

	end
	if npc_talk_index == "n277_bluff_dealers_pope-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4103, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4103, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4103, 1);
				npc_talk_index = "n277_bluff_dealers_pope-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 700101, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700102, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 600101, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 600102, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300561, 2);

	end
	if npc_talk_index == "n277_bluff_dealers_pope-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41030, questID, 1);
			 end 
	end
	if npc_talk_index == "n277_bluff_dealers_pope-2-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4104, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4104, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4104, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300561, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300561, api_quest_HasQuestItem( pRoom, userObjID, 300561, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n277_bluff_dealers_pope-1", "sq15_4104_picky_about_food2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4103_picky_about_food1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4103);
	if qstep == 1 and CountIndex == 700101 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300561, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300561, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700102 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300561, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300561, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300561 then

	end
	if qstep == 1 and CountIndex == 600101 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300561, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300561, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600102 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300561, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300561, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4103_picky_about_food1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4103);
	if qstep == 1 and CountIndex == 700101 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700102 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300561 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 600101 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600102 and Count >= TargetCount  then

	end
end

function sq15_4103_picky_about_food1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4103);
	local questID=4103;
end

function sq15_4103_picky_about_food1_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 700101, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700102, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 600101, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 600102, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300561, 2);
end

function sq15_4103_picky_about_food1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4103_picky_about_food1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4103, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4103, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4103, 1);
				npc_talk_index = "n277_bluff_dealers_pope-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 700101, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700102, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 600101, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 600102, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300561, 2);
end

</GameServer>