<VillageServer>

function sq11_462_skill_of_battle_02_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 65 then
		sq11_462_skill_of_battle_02_OnTalk_n065_goblinson04_gosuk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n065_goblinson04_gosuk--------------------------------------------------------------------------------
function sq11_462_skill_of_battle_02_OnTalk_n065_goblinson04_gosuk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n065_goblinson04_gosuk-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n065_goblinson04_gosuk-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n065_goblinson04_gosuk-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n065_goblinson04_gosuk-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n065_goblinson04_gosuk-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4621, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4622, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4623, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4624, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4625, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4626, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4627, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4628, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4629, false);
			 end 

	end
	if npc_talk_index == "n065_goblinson04_gosuk-accepting-acceptted" then
				api_quest_AddQuest(userObjID,462, 1);
				api_quest_SetJournalStep(userObjID,462, 1);
				api_quest_SetQuestStep(userObjID,462, 1);
				npc_talk_index = "n065_goblinson04_gosuk-1";

	end
	if npc_talk_index == "n065_goblinson04_gosuk-1-b" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n065_goblinson04_gosuk-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4621, true);
				 api_quest_RewardQuestUser(userObjID, 4621, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4622, true);
				 api_quest_RewardQuestUser(userObjID, 4622, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4623, true);
				 api_quest_RewardQuestUser(userObjID, 4623, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4624, true);
				 api_quest_RewardQuestUser(userObjID, 4624, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4625, true);
				 api_quest_RewardQuestUser(userObjID, 4625, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4626, true);
				 api_quest_RewardQuestUser(userObjID, 4626, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4627, true);
				 api_quest_RewardQuestUser(userObjID, 4627, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4628, true);
				 api_quest_RewardQuestUser(userObjID, 4628, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4629, true);
				 api_quest_RewardQuestUser(userObjID, 4629, questID, 1);
			 end 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 463, 1);
					api_quest_SetQuestStep(userObjID, 463, 1);
					api_quest_SetJournalStep(userObjID, 463, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				api_quest_AddQuest(userObjID,463, 1);
				api_quest_SetJournalStep(userObjID,463, 1);
				api_quest_SetQuestStep(userObjID,463, 1);

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n065_goblinson04_gosuk-1", "sq11_463_skill_of_battle_03.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_462_skill_of_battle_02_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 462);
end

function sq11_462_skill_of_battle_02_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 462);
end

function sq11_462_skill_of_battle_02_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 462);
	local questID=462;
end

function sq11_462_skill_of_battle_02_OnRemoteStart( userObjID, questID )
end

function sq11_462_skill_of_battle_02_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_462_skill_of_battle_02_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,462, 1);
				api_quest_SetJournalStep(userObjID,462, 1);
				api_quest_SetQuestStep(userObjID,462, 1);
				npc_talk_index = "n065_goblinson04_gosuk-1";
end

</VillageServer>

<GameServer>
function sq11_462_skill_of_battle_02_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 65 then
		sq11_462_skill_of_battle_02_OnTalk_n065_goblinson04_gosuk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n065_goblinson04_gosuk--------------------------------------------------------------------------------
function sq11_462_skill_of_battle_02_OnTalk_n065_goblinson04_gosuk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n065_goblinson04_gosuk-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n065_goblinson04_gosuk-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n065_goblinson04_gosuk-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n065_goblinson04_gosuk-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n065_goblinson04_gosuk-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4621, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4622, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4623, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4624, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4625, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4626, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4627, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4628, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4629, false);
			 end 

	end
	if npc_talk_index == "n065_goblinson04_gosuk-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,462, 1);
				api_quest_SetJournalStep( pRoom, userObjID,462, 1);
				api_quest_SetQuestStep( pRoom, userObjID,462, 1);
				npc_talk_index = "n065_goblinson04_gosuk-1";

	end
	if npc_talk_index == "n065_goblinson04_gosuk-1-b" then 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n065_goblinson04_gosuk-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4621, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4621, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4622, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4622, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4623, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4623, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4624, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4624, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4625, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4625, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4626, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4626, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4627, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4627, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4628, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4628, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4629, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4629, questID, 1);
			 end 
	end
	if npc_talk_index == "n065_goblinson04_gosuk-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 463, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 463, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 463, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				api_quest_AddQuest( pRoom, userObjID,463, 1);
				api_quest_SetJournalStep( pRoom, userObjID,463, 1);
				api_quest_SetQuestStep( pRoom, userObjID,463, 1);

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n065_goblinson04_gosuk-1", "sq11_463_skill_of_battle_03.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_462_skill_of_battle_02_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 462);
end

function sq11_462_skill_of_battle_02_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 462);
end

function sq11_462_skill_of_battle_02_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 462);
	local questID=462;
end

function sq11_462_skill_of_battle_02_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_462_skill_of_battle_02_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_462_skill_of_battle_02_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,462, 1);
				api_quest_SetJournalStep( pRoom, userObjID,462, 1);
				api_quest_SetQuestStep( pRoom, userObjID,462, 1);
				npc_talk_index = "n065_goblinson04_gosuk-1";
end

</GameServer>