<VillageServer>

function sq15_811_questionable_letter_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 710 then
		sq15_811_questionable_letter_OnTalk_n710_scholar_hancock(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 714 then
		sq15_811_questionable_letter_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n710_scholar_hancock--------------------------------------------------------------------------------
function sq15_811_questionable_letter_OnTalk_n710_scholar_hancock(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n710_scholar_hancock-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n710_scholar_hancock-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n710_scholar_hancock-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n710_scholar_hancock-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n710_scholar_hancock-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n710_scholar_hancock-1-b" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-c" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-d" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-e" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2" then 

				if api_quest_HasQuestItem(userObjID, 300443, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300443, api_quest_HasQuestItem(userObjID, 300443, 1));
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1555, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201555, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300442, 2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n710_scholar_hancock-3-b" then 
	end
	if npc_talk_index == "n710_scholar_hancock-3-c" then 
	end
	if npc_talk_index == "n710_scholar_hancock-4" then 

				if api_quest_HasQuestItem(userObjID, 300442, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300442, api_quest_HasQuestItem(userObjID, 300442, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300444, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300444, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n714_cleric_yohan--------------------------------------------------------------------------------
function sq15_811_questionable_letter_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n714_cleric_yohan-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n714_cleric_yohan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n714_cleric_yohan-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8110, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8110, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8110, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8110, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8110, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8110, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8110, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8110, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8110, false);
			 end 

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-acceptted" then
				api_quest_AddQuest(userObjID,811, 1);
				api_quest_SetJournalStep(userObjID,811, 1);
				api_quest_SetQuestStep(userObjID,811, 1);
				npc_talk_index = "n714_cleric_yohan-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300443, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300443, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n714_cleric_yohan-4-a" then 

				if api_quest_HasQuestItem(userObjID, 300444, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300444, api_quest_HasQuestItem(userObjID, 300444, 1));
				end
	end
	if npc_talk_index == "n714_cleric_yohan-4-b" then 
	end
	if npc_talk_index == "n714_cleric_yohan-4-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8110, true);
				 api_quest_RewardQuestUser(userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8110, true);
				 api_quest_RewardQuestUser(userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8110, true);
				 api_quest_RewardQuestUser(userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8110, true);
				 api_quest_RewardQuestUser(userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8110, true);
				 api_quest_RewardQuestUser(userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8110, true);
				 api_quest_RewardQuestUser(userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8110, true);
				 api_quest_RewardQuestUser(userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8110, true);
				 api_quest_RewardQuestUser(userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8110, true);
				 api_quest_RewardQuestUser(userObjID, 8110, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 812, 1);
					api_quest_SetQuestStep(userObjID, 812, 1);
					api_quest_SetJournalStep(userObjID, 812, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n714_cleric_yohan-1", "sq15_812_unbreakable_vow.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_811_questionable_letter_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 811);
	if qstep == 2 and CountIndex == 1555 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300442, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300442, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201555 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300442, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300442, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300442 then

	end
end

function sq15_811_questionable_letter_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 811);
	if qstep == 2 and CountIndex == 1555 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201555 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300442 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function sq15_811_questionable_letter_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 811);
	local questID=811;
end

function sq15_811_questionable_letter_OnRemoteStart( userObjID, questID )
end

function sq15_811_questionable_letter_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_811_questionable_letter_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,811, 1);
				api_quest_SetJournalStep(userObjID,811, 1);
				api_quest_SetQuestStep(userObjID,811, 1);
				npc_talk_index = "n714_cleric_yohan-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300443, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300443, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
end

</VillageServer>

<GameServer>
function sq15_811_questionable_letter_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 710 then
		sq15_811_questionable_letter_OnTalk_n710_scholar_hancock( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 714 then
		sq15_811_questionable_letter_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n710_scholar_hancock--------------------------------------------------------------------------------
function sq15_811_questionable_letter_OnTalk_n710_scholar_hancock( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n710_scholar_hancock-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n710_scholar_hancock-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n710_scholar_hancock-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n710_scholar_hancock-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n710_scholar_hancock-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n710_scholar_hancock-1-b" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-c" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-d" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-e" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300443, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300443, api_quest_HasQuestItem( pRoom, userObjID, 300443, 1));
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1555, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201555, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300442, 2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n710_scholar_hancock-3-b" then 
	end
	if npc_talk_index == "n710_scholar_hancock-3-c" then 
	end
	if npc_talk_index == "n710_scholar_hancock-4" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300442, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300442, api_quest_HasQuestItem( pRoom, userObjID, 300442, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300444, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300444, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n714_cleric_yohan--------------------------------------------------------------------------------
function sq15_811_questionable_letter_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n714_cleric_yohan-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n714_cleric_yohan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n714_cleric_yohan-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, false);
			 end 

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,811, 1);
				api_quest_SetJournalStep( pRoom, userObjID,811, 1);
				api_quest_SetQuestStep( pRoom, userObjID,811, 1);
				npc_talk_index = "n714_cleric_yohan-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300443, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300443, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n714_cleric_yohan-4-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300444, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300444, api_quest_HasQuestItem( pRoom, userObjID, 300444, 1));
				end
	end
	if npc_talk_index == "n714_cleric_yohan-4-b" then 
	end
	if npc_talk_index == "n714_cleric_yohan-4-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8110, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 812, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 812, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 812, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n714_cleric_yohan-1", "sq15_812_unbreakable_vow.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_811_questionable_letter_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 811);
	if qstep == 2 and CountIndex == 1555 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300442, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300442, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201555 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300442, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300442, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300442 then

	end
end

function sq15_811_questionable_letter_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 811);
	if qstep == 2 and CountIndex == 1555 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201555 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300442 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function sq15_811_questionable_letter_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 811);
	local questID=811;
end

function sq15_811_questionable_letter_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_811_questionable_letter_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_811_questionable_letter_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,811, 1);
				api_quest_SetJournalStep( pRoom, userObjID,811, 1);
				api_quest_SetQuestStep( pRoom, userObjID,811, 1);
				npc_talk_index = "n714_cleric_yohan-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300443, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300443, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
end

</GameServer>