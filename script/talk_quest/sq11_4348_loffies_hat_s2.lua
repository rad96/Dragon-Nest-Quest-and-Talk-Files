<VillageServer>

function sq11_4348_loffies_hat_s2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 87 then
		sq11_4348_loffies_hat_s2_OnTalk_n087_beggar(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n087_beggar--------------------------------------------------------------------------------
function sq11_4348_loffies_hat_s2_OnTalk_n087_beggar(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n087_beggar-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n087_beggar-1";
				if api_user_HasItem(userObjID, 805307725, 20) >= 20 then
									npc_talk_index = "n087_beggar-1";

				else
									npc_talk_index = "n087_beggar-1-d";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n087_beggar-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43480, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43480, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43480, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43480, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43480, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43480, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43480, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43480, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43480, false);
			 end 

	end
	if npc_talk_index == "n087_beggar-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4348, 1);
				api_quest_SetJournalStep(userObjID,4348, 1);
				api_quest_SetQuestStep(userObjID,4348, 1);
				api_quest_SetQuestMemo(userObjID, 4348, 1, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805307725, 20);
				if api_user_HasItem(userObjID, 805307725, 20) >= 20 then
									npc_talk_index = "n087_beggar-1";
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestMemo(userObjID, 4348, 1, 2);

				else
									npc_talk_index = "n087_beggar-accepting-i";

				end

	end
	if npc_talk_index == "n087_beggar-1-chk" then 
				if api_user_HasItem(userObjID, 805307725, 20) >= 20 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43480, true);
				 api_quest_RewardQuestUser(userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43480, true);
				 api_quest_RewardQuestUser(userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43480, true);
				 api_quest_RewardQuestUser(userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43480, true);
				 api_quest_RewardQuestUser(userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43480, true);
				 api_quest_RewardQuestUser(userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43480, true);
				 api_quest_RewardQuestUser(userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43480, true);
				 api_quest_RewardQuestUser(userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43480, true);
				 api_quest_RewardQuestUser(userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43480, true);
				 api_quest_RewardQuestUser(userObjID, 43480, questID, 1);
			 end 
				npc_talk_index = "n087_beggar-1-c";

				else
									npc_talk_index = "n087_beggar-1-b";

				end
	end
	if npc_talk_index == "n087_beggar-1-back" then 
				api_npc_NextScript(userObjID, npcObjID, "quest", "n087_beggar.xml");
	end
	if npc_talk_index == "n087_beggar-1-back_2" then 
				api_npc_NextScript(userObjID, npcObjID, "quest", "n087_beggar.xml");
	end
	if npc_talk_index == "n087_beggar-1-a" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_DelItem(userObjID, 805307725, 20, questID);


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_4348_loffies_hat_s2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4348);
	if qstep == 1 and CountIndex == 805307725 then
				if api_user_HasItem(userObjID, 805307725, 20) >= 20  and api_quest_GetQuestMemo(userObjID, 4348, 1) == 1  then
									api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestMemo(userObjID, 4348, 1, 2);

				else
									if api_user_HasItem(userObjID, 805307725, 20) == -3  and api_quest_GetQuestMemo(userObjID, 4348, 1) == 2  then
									api_quest_SetJournalStep(userObjID, questID, 1);
				api_quest_SetQuestMemo(userObjID, 4348, 1, 1);

				else
				end

				end

	end
end

function sq11_4348_loffies_hat_s2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4348);
	if qstep == 1 and CountIndex == 805307725 and Count >= TargetCount  then

	end
end

function sq11_4348_loffies_hat_s2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4348);
	local questID=4348;
end

function sq11_4348_loffies_hat_s2_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805307725, 20);
				api_quest_SetJournalStep(userObjID, questID, 2);
end

function sq11_4348_loffies_hat_s2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_4348_loffies_hat_s2_ForceAccept( userObjID, npcObjID, questID )
				npc_talk_index = "n087_beggar-1";
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestMemo(userObjID, 4348, 1, 2);
				npc_talk_index = "n087_beggar-accepting-i";
				api_quest_AddQuest(userObjID,4348, 1);
				api_quest_SetJournalStep(userObjID,4348, 1);
				api_quest_SetQuestStep(userObjID,4348, 1);
				api_quest_SetQuestMemo(userObjID, 4348, 1, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 805307725, 20);
				if api_user_HasItem(userObjID, 805307725, 20) >= 20 then
									npc_talk_index = "n087_beggar-1";
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestMemo(userObjID, 4348, 1, 2);

				else
									npc_talk_index = "n087_beggar-accepting-i";

				end
end

</VillageServer>

<GameServer>
function sq11_4348_loffies_hat_s2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 87 then
		sq11_4348_loffies_hat_s2_OnTalk_n087_beggar( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n087_beggar--------------------------------------------------------------------------------
function sq11_4348_loffies_hat_s2_OnTalk_n087_beggar( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n087_beggar-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n087_beggar-1";
				if api_user_HasItem( pRoom, userObjID, 805307725, 20) >= 20 then
									npc_talk_index = "n087_beggar-1";

				else
									npc_talk_index = "n087_beggar-1-d";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n087_beggar-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, false);
			 end 

	end
	if npc_talk_index == "n087_beggar-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4348, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4348, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4348, 1);
				api_quest_SetQuestMemo( pRoom, userObjID, 4348, 1, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805307725, 20);
				if api_user_HasItem( pRoom, userObjID, 805307725, 20) >= 20 then
									npc_talk_index = "n087_beggar-1";
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestMemo( pRoom, userObjID, 4348, 1, 2);

				else
									npc_talk_index = "n087_beggar-accepting-i";

				end

	end
	if npc_talk_index == "n087_beggar-1-chk" then 
				if api_user_HasItem( pRoom, userObjID, 805307725, 20) >= 20 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43480, questID, 1);
			 end 
				npc_talk_index = "n087_beggar-1-c";

				else
									npc_talk_index = "n087_beggar-1-b";

				end
	end
	if npc_talk_index == "n087_beggar-1-back" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "quest", "n087_beggar.xml");
	end
	if npc_talk_index == "n087_beggar-1-back_2" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "quest", "n087_beggar.xml");
	end
	if npc_talk_index == "n087_beggar-1-a" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_DelItem( pRoom, userObjID, 805307725, 20, questID);


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_4348_loffies_hat_s2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4348);
	if qstep == 1 and CountIndex == 805307725 then
				if api_user_HasItem( pRoom, userObjID, 805307725, 20) >= 20  and api_quest_GetQuestMemo( pRoom, userObjID, 4348, 1) == 1  then
									api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestMemo( pRoom, userObjID, 4348, 1, 2);

				else
									if api_user_HasItem( pRoom, userObjID, 805307725, 20) == -3  and api_quest_GetQuestMemo( pRoom, userObjID, 4348, 1) == 2  then
									api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				api_quest_SetQuestMemo( pRoom, userObjID, 4348, 1, 1);

				else
				end

				end

	end
end

function sq11_4348_loffies_hat_s2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4348);
	if qstep == 1 and CountIndex == 805307725 and Count >= TargetCount  then

	end
end

function sq11_4348_loffies_hat_s2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4348);
	local questID=4348;
end

function sq11_4348_loffies_hat_s2_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805307725, 20);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
end

function sq11_4348_loffies_hat_s2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_4348_loffies_hat_s2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				npc_talk_index = "n087_beggar-1";
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestMemo( pRoom, userObjID, 4348, 1, 2);
				npc_talk_index = "n087_beggar-accepting-i";
				api_quest_AddQuest( pRoom, userObjID,4348, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4348, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4348, 1);
				api_quest_SetQuestMemo( pRoom, userObjID, 4348, 1, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 805307725, 20);
				if api_user_HasItem( pRoom, userObjID, 805307725, 20) >= 20 then
									npc_talk_index = "n087_beggar-1";
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestMemo( pRoom, userObjID, 4348, 1, 2);

				else
									npc_talk_index = "n087_beggar-accepting-i";

				end
end

</GameServer>