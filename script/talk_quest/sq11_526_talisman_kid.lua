<VillageServer>

function sq11_526_talisman_kid_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 49 then
		sq11_526_talisman_kid_OnTalk_n049_sainthaven_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n049_sainthaven_kid--------------------------------------------------------------------------------
function sq11_526_talisman_kid_OnTalk_n049_sainthaven_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n049_sainthaven_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n049_sainthaven_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n049_sainthaven_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n049_sainthaven_kid-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5261, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5262, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5263, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5264, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5265, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5266, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5267, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5268, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5269, false);
			 end 

	end
	if npc_talk_index == "n049_sainthaven_kid-accepting-accepted" then
				api_quest_AddQuest(userObjID,526, 1);
				api_quest_SetJournalStep(userObjID,526, 1);
				api_quest_SetQuestStep(userObjID,526, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 889, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200889, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400420, 1);
				npc_talk_index = "n049_sainthaven_kid-1";

	end
	if npc_talk_index == "n049_sainthaven_kid-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5261, true);
				 api_quest_RewardQuestUser(userObjID, 5261, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5262, true);
				 api_quest_RewardQuestUser(userObjID, 5262, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5263, true);
				 api_quest_RewardQuestUser(userObjID, 5263, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5264, true);
				 api_quest_RewardQuestUser(userObjID, 5264, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5265, true);
				 api_quest_RewardQuestUser(userObjID, 5265, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5266, true);
				 api_quest_RewardQuestUser(userObjID, 5266, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5267, true);
				 api_quest_RewardQuestUser(userObjID, 5267, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5268, true);
				 api_quest_RewardQuestUser(userObjID, 5268, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5269, true);
				 api_quest_RewardQuestUser(userObjID, 5269, questID, 1);
			 end 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-c" then 

				if api_quest_HasQuestItem(userObjID, 400420, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400420, api_quest_HasQuestItem(userObjID, 400420, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_526_talisman_kid_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 526);
	if qstep == 1 and CountIndex == 889 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400420, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400420, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400420 then

	end
	if qstep == 1 and CountIndex == 200889 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400420, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400420, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_526_talisman_kid_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 526);
	if qstep == 1 and CountIndex == 889 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400420 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200889 and Count >= TargetCount  then

	end
end

function sq11_526_talisman_kid_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 526);
	local questID=526;
end

function sq11_526_talisman_kid_OnRemoteStart( userObjID, questID )
end

function sq11_526_talisman_kid_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_526_talisman_kid_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_526_talisman_kid_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 49 then
		sq11_526_talisman_kid_OnTalk_n049_sainthaven_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n049_sainthaven_kid--------------------------------------------------------------------------------
function sq11_526_talisman_kid_OnTalk_n049_sainthaven_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n049_sainthaven_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n049_sainthaven_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n049_sainthaven_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n049_sainthaven_kid-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5261, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5262, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5263, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5264, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5265, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5266, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5267, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5268, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5269, false);
			 end 

	end
	if npc_talk_index == "n049_sainthaven_kid-accepting-accepted" then
				api_quest_AddQuest( pRoom, userObjID,526, 1);
				api_quest_SetJournalStep( pRoom, userObjID,526, 1);
				api_quest_SetQuestStep( pRoom, userObjID,526, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 889, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200889, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400420, 1);
				npc_talk_index = "n049_sainthaven_kid-1";

	end
	if npc_talk_index == "n049_sainthaven_kid-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5261, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5261, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5262, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5262, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5263, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5263, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5264, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5264, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5265, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5265, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5266, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5266, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5267, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5267, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5268, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5268, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5269, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5269, questID, 1);
			 end 
	end
	if npc_talk_index == "n049_sainthaven_kid-2-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400420, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400420, api_quest_HasQuestItem( pRoom, userObjID, 400420, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_526_talisman_kid_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 526);
	if qstep == 1 and CountIndex == 889 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400420, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400420, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400420 then

	end
	if qstep == 1 and CountIndex == 200889 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400420, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400420, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_526_talisman_kid_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 526);
	if qstep == 1 and CountIndex == 889 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400420 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200889 and Count >= TargetCount  then

	end
end

function sq11_526_talisman_kid_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 526);
	local questID=526;
end

function sq11_526_talisman_kid_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_526_talisman_kid_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_526_talisman_kid_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>