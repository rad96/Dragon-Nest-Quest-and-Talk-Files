<VillageServer>

function mqc05_9614_homecoming_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 38 then
		mqc05_9614_homecoming_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mqc05_9614_homecoming_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		mqc05_9614_homecoming_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc05_9614_homecoming_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		mqc05_9614_homecoming_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1861 then
		mqc05_9614_homecoming_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 213 then
		mqc05_9614_homecoming_OnTalk_n213_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-c" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9614, 2);
				api_quest_SetJournalStep(userObjID,9614, 1);
				api_quest_SetQuestStep(userObjID,9614, 1);
				npc_talk_index = "n042_general_duglars-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n044_archer_master_ishilien-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n044_archer_master_ishilien-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-1-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1172, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201172, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300250, 1);
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1861_shaolong--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1861_shaolong-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1861_shaolong-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-4-b" then 
	end
	if npc_talk_index == "n1861_shaolong-4-c" then 
	end
	if npc_talk_index == "n1861_shaolong-4-d" then 
	end
	if npc_talk_index == "n1861_shaolong-4-e" then 
	end
	if npc_talk_index == "n1861_shaolong-4-f" then 
	end
	if npc_talk_index == "n1861_shaolong-4-g" then 
	end
	if npc_talk_index == "n1861_shaolong-4-h" then 
	end
	if npc_talk_index == "n1861_shaolong-4-i" then 
	end
	if npc_talk_index == "n1861_shaolong-4-j" then 
	end
	if npc_talk_index == "n1861_shaolong-4-k" then 
	end
	if npc_talk_index == "n1861_shaolong-4-l" then 
	end
	if npc_talk_index == "n1861_shaolong-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n213_slave_david--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n213_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n213_slave_david-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n213_slave_david-5-b" then 
	end
	if npc_talk_index == "n213_slave_david-5-c" then 
	end
	if npc_talk_index == "n213_slave_david-5-d" then 
	end
	if npc_talk_index == "n213_slave_david-5-e" then 
	end
	if npc_talk_index == "n213_slave_david-5-f" then 
	end
	if npc_talk_index == "n213_slave_david-5-g" then 
	end
	if npc_talk_index == "n213_slave_david-5-h" then 
	end
	if npc_talk_index == "n213_slave_david-5-i" then 
	end
	if npc_talk_index == "n213_slave_david-5-j" then 
	end
	if npc_talk_index == "n213_slave_david-5-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 96140, true);
				 api_quest_RewardQuestUser(userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 96140, true);
				 api_quest_RewardQuestUser(userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 96140, true);
				 api_quest_RewardQuestUser(userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 96140, true);
				 api_quest_RewardQuestUser(userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 96140, true);
				 api_quest_RewardQuestUser(userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 96140, true);
				 api_quest_RewardQuestUser(userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 96140, true);
				 api_quest_RewardQuestUser(userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 96140, true);
				 api_quest_RewardQuestUser(userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 96140, true);
				 api_quest_RewardQuestUser(userObjID, 96140, questID, 1);
			 end 
	end
	if npc_talk_index == "n213_slave_david-5-l" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9727, 2);
					api_quest_SetQuestStep(userObjID, 9727, 1);
					api_quest_SetJournalStep(userObjID, 9727, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n213_slave_david-5-m" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9614_homecoming_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9614);
	if qstep == 2 and CountIndex == 1172 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300250, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300250, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201172 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300250, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300250, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300250 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function mqc05_9614_homecoming_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9614);
	if qstep == 2 and CountIndex == 1172 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201172 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300250 and Count >= TargetCount  then

	end
end

function mqc05_9614_homecoming_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9614);
	local questID=9614;
end

function mqc05_9614_homecoming_OnRemoteStart( userObjID, questID )
end

function mqc05_9614_homecoming_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc05_9614_homecoming_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9614, 2);
				api_quest_SetJournalStep(userObjID,9614, 1);
				api_quest_SetQuestStep(userObjID,9614, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</VillageServer>

<GameServer>
function mqc05_9614_homecoming_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 38 then
		mqc05_9614_homecoming_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mqc05_9614_homecoming_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		mqc05_9614_homecoming_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc05_9614_homecoming_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		mqc05_9614_homecoming_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1861 then
		mqc05_9614_homecoming_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 213 then
		mqc05_9614_homecoming_OnTalk_n213_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-c" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9614, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9614, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9614, 1);
				npc_talk_index = "n042_general_duglars-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n044_archer_master_ishilien-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n044_archer_master_ishilien-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-1-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1172, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201172, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300250, 1);
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1861_shaolong--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1861_shaolong-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1861_shaolong-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-4-b" then 
	end
	if npc_talk_index == "n1861_shaolong-4-c" then 
	end
	if npc_talk_index == "n1861_shaolong-4-d" then 
	end
	if npc_talk_index == "n1861_shaolong-4-e" then 
	end
	if npc_talk_index == "n1861_shaolong-4-f" then 
	end
	if npc_talk_index == "n1861_shaolong-4-g" then 
	end
	if npc_talk_index == "n1861_shaolong-4-h" then 
	end
	if npc_talk_index == "n1861_shaolong-4-i" then 
	end
	if npc_talk_index == "n1861_shaolong-4-j" then 
	end
	if npc_talk_index == "n1861_shaolong-4-k" then 
	end
	if npc_talk_index == "n1861_shaolong-4-l" then 
	end
	if npc_talk_index == "n1861_shaolong-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n213_slave_david--------------------------------------------------------------------------------
function mqc05_9614_homecoming_OnTalk_n213_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n213_slave_david-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n213_slave_david-5-b" then 
	end
	if npc_talk_index == "n213_slave_david-5-c" then 
	end
	if npc_talk_index == "n213_slave_david-5-d" then 
	end
	if npc_talk_index == "n213_slave_david-5-e" then 
	end
	if npc_talk_index == "n213_slave_david-5-f" then 
	end
	if npc_talk_index == "n213_slave_david-5-g" then 
	end
	if npc_talk_index == "n213_slave_david-5-h" then 
	end
	if npc_talk_index == "n213_slave_david-5-i" then 
	end
	if npc_talk_index == "n213_slave_david-5-j" then 
	end
	if npc_talk_index == "n213_slave_david-5-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96140, questID, 1);
			 end 
	end
	if npc_talk_index == "n213_slave_david-5-l" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9727, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9727, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9727, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n213_slave_david-5-m" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9614_homecoming_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9614);
	if qstep == 2 and CountIndex == 1172 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300250, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300250, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201172 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300250, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300250, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300250 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function mqc05_9614_homecoming_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9614);
	if qstep == 2 and CountIndex == 1172 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201172 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300250 and Count >= TargetCount  then

	end
end

function mqc05_9614_homecoming_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9614);
	local questID=9614;
end

function mqc05_9614_homecoming_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc05_9614_homecoming_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc05_9614_homecoming_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9614, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9614, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9614, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</GameServer>