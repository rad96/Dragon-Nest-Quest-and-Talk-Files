<VillageServer>

function sq_2141_level_up_1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 100 then
		sq_2141_level_up_1_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2141_level_up_1_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21411, true);
				 api_quest_RewardQuestUser(userObjID, 21411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21412, true);
				 api_quest_RewardQuestUser(userObjID, 21412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21413, true);
				 api_quest_RewardQuestUser(userObjID, 21413, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21414, true);
				 api_quest_RewardQuestUser(userObjID, 21414, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21415, true);
				 api_quest_RewardQuestUser(userObjID, 21415, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21416, true);
				 api_quest_RewardQuestUser(userObjID, 21416, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21417, true);
				 api_quest_RewardQuestUser(userObjID, 21417, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21418, true);
				 api_quest_RewardQuestUser(userObjID, 21418, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21410, true);
				 api_quest_RewardQuestUser(userObjID, 21410, questID, 1);
			 end 
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2141, 1);
				api_quest_SetJournalStep(userObjID,2141, 1);
				api_quest_SetQuestStep(userObjID,2141, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21411, true);
				 api_quest_RewardQuestUser(userObjID, 21411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21412, true);
				 api_quest_RewardQuestUser(userObjID, 21412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21413, true);
				 api_quest_RewardQuestUser(userObjID, 21413, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21414, true);
				 api_quest_RewardQuestUser(userObjID, 21414, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21415, true);
				 api_quest_RewardQuestUser(userObjID, 21415, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21416, true);
				 api_quest_RewardQuestUser(userObjID, 21416, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21417, true);
				 api_quest_RewardQuestUser(userObjID, 21417, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21418, true);
				 api_quest_RewardQuestUser(userObjID, 21418, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21410, true);
				 api_quest_RewardQuestUser(userObjID, 21410, questID, 1);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-1-a" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_user_HasItem(userObjID, 805307079, 1) > 0 then 
					api_user_DelItem(userObjID, 805307079, api_user_HasItem(userObjID, 805307079, 1, questID));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2141_level_up_1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2141);
end

function sq_2141_level_up_1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2141);
end

function sq_2141_level_up_1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2141);
	local questID=2141;
end

function sq_2141_level_up_1_OnRemoteStart( userObjID, questID )
end

function sq_2141_level_up_1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_2141_level_up_1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2141, 1);
				api_quest_SetJournalStep(userObjID,2141, 1);
				api_quest_SetQuestStep(userObjID,2141, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21411, true);
				 api_quest_RewardQuestUser(userObjID, 21411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21412, true);
				 api_quest_RewardQuestUser(userObjID, 21412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21413, true);
				 api_quest_RewardQuestUser(userObjID, 21413, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21414, true);
				 api_quest_RewardQuestUser(userObjID, 21414, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21415, true);
				 api_quest_RewardQuestUser(userObjID, 21415, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21416, true);
				 api_quest_RewardQuestUser(userObjID, 21416, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21417, true);
				 api_quest_RewardQuestUser(userObjID, 21417, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21418, true);
				 api_quest_RewardQuestUser(userObjID, 21418, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21410, true);
				 api_quest_RewardQuestUser(userObjID, 21410, questID, 1);
			 end 
end

</VillageServer>

<GameServer>
function sq_2141_level_up_1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 100 then
		sq_2141_level_up_1_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq_2141_level_up_1_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21413, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21413, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21414, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21414, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21415, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21415, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21416, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21416, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21417, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21417, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21418, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21418, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21410, questID, 1);
			 end 
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2141, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2141, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2141, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21413, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21413, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21414, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21414, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21415, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21415, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21416, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21416, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21417, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21417, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21418, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21418, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21410, questID, 1);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-1-a" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_user_HasItem( pRoom, userObjID, 805307079, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 805307079, api_user_HasItem( pRoom, userObjID, 805307079, 1, questID));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2141_level_up_1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2141);
end

function sq_2141_level_up_1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2141);
end

function sq_2141_level_up_1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2141);
	local questID=2141;
end

function sq_2141_level_up_1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_2141_level_up_1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_2141_level_up_1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2141, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2141, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2141, 1);
				npc_talk_index = "n100_event_ilyn-1";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21413, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21413, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21414, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21414, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21415, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21415, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21416, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21416, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21417, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21417, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21418, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21418, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21410, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21410, questID, 1);
			 end 
end

</GameServer>