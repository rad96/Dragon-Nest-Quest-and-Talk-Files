<VillageServer>

function mq15_9219_mask_other_world_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 703 then
		mq15_9219_mask_other_world_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n703_book_doctor--------------------------------------------------------------------------------
function mq15_9219_mask_other_world_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n703_book_doctor-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n703_book_doctor-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9219, 2);
				api_quest_SetJournalStep(userObjID,9219, 1);
				api_quest_SetQuestStep(userObjID,9219, 1);
				npc_talk_index = "n703_book_doctor-1";

	end
	if npc_talk_index == "n703_book_doctor-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 600061, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 400319, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700061, 30000);
	end
	if npc_talk_index == "n703_book_doctor-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92191, true);
				 api_quest_RewardQuestUser(userObjID, 92191, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92192, true);
				 api_quest_RewardQuestUser(userObjID, 92192, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92193, true);
				 api_quest_RewardQuestUser(userObjID, 92193, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92194, true);
				 api_quest_RewardQuestUser(userObjID, 92194, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92195, true);
				 api_quest_RewardQuestUser(userObjID, 92195, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92196, true);
				 api_quest_RewardQuestUser(userObjID, 92196, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92191, true);
				 api_quest_RewardQuestUser(userObjID, 92191, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92191, true);
				 api_quest_RewardQuestUser(userObjID, 92191, questID, 1);
			 end 
	end
	if npc_talk_index == "n703_book_doctor-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9220, 2);
					api_quest_SetQuestStep(userObjID, 9220, 1);
					api_quest_SetJournalStep(userObjID, 9220, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9219_mask_other_world_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9219);
	if qstep == 2 and CountIndex == 600061 then
				api_quest_ClearCountingInfo(userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400319, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400319, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 400319 then

	end
	if qstep == 2 and CountIndex == 700061 then
				api_quest_ClearCountingInfo(userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400319, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400319, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function mq15_9219_mask_other_world_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9219);
	if qstep == 2 and CountIndex == 600061 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400319 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700061 and Count >= TargetCount  then

	end
end

function mq15_9219_mask_other_world_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9219);
	local questID=9219;
end

function mq15_9219_mask_other_world_OnRemoteStart( userObjID, questID )
end

function mq15_9219_mask_other_world_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9219_mask_other_world_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9219, 2);
				api_quest_SetJournalStep(userObjID,9219, 1);
				api_quest_SetQuestStep(userObjID,9219, 1);
				npc_talk_index = "n703_book_doctor-1";
end

</VillageServer>

<GameServer>
function mq15_9219_mask_other_world_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 703 then
		mq15_9219_mask_other_world_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n703_book_doctor--------------------------------------------------------------------------------
function mq15_9219_mask_other_world_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n703_book_doctor-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n703_book_doctor-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9219, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9219, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9219, 1);
				npc_talk_index = "n703_book_doctor-1";

	end
	if npc_talk_index == "n703_book_doctor-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 600061, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 400319, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700061, 30000);
	end
	if npc_talk_index == "n703_book_doctor-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92191, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92191, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92192, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92192, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92193, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92193, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92194, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92194, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92195, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92195, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92196, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92196, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92191, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92191, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92191, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92191, questID, 1);
			 end 
	end
	if npc_talk_index == "n703_book_doctor-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9220, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9220, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9220, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9219_mask_other_world_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9219);
	if qstep == 2 and CountIndex == 600061 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400319, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400319, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 400319 then

	end
	if qstep == 2 and CountIndex == 700061 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400319, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400319, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function mq15_9219_mask_other_world_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9219);
	if qstep == 2 and CountIndex == 600061 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400319 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700061 and Count >= TargetCount  then

	end
end

function mq15_9219_mask_other_world_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9219);
	local questID=9219;
end

function mq15_9219_mask_other_world_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9219_mask_other_world_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9219_mask_other_world_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9219, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9219, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9219, 1);
				npc_talk_index = "n703_book_doctor-1";
end

</GameServer>