<VillageServer>

function gqc14_3586_n1408_goddess_eye_time_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1408 then
		gqc14_3586_n1408_goddess_eye_time_OnTalk_n1408_guild_board(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1408_guild_board--------------------------------------------------------------------------------
function gqc14_3586_n1408_goddess_eye_time_OnTalk_n1408_guild_board(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1408_guild_board-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1408_guild_board-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1408_guild_board-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1408_guild_board-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1408_guild_board-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1408_guild_board-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 35860, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 35860, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 35860, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 35860, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 35860, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 35860, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 35860, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 35860, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 35860, false);
			 end 

	end
	if npc_talk_index == "n1408_guild_board-accepting-acceptted" then
				api_quest_AddQuest(userObjID,3586, 1);
				api_quest_SetJournalStep(userObjID,3586, 1);
				api_quest_SetQuestStep(userObjID,3586, 1);
				npc_talk_index = "n1408_guild_board-1";

	end
	if npc_talk_index == "n1408_guild_board-1-a" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 6, 1, 900);
				api_quest_SetCountingInfo(userObjID, questID, 1, 5, 1061, 1 );
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n1408_guild_board-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 35860, true);
				 api_quest_RewardQuestUser(userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 35860, true);
				 api_quest_RewardQuestUser(userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 35860, true);
				 api_quest_RewardQuestUser(userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 35860, true);
				 api_quest_RewardQuestUser(userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 35860, true);
				 api_quest_RewardQuestUser(userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 35860, true);
				 api_quest_RewardQuestUser(userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 35860, true);
				 api_quest_RewardQuestUser(userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 35860, true);
				 api_quest_RewardQuestUser(userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 35860, true);
				 api_quest_RewardQuestUser(userObjID, 35860, questID, 1);
			 end 
	end
	if npc_talk_index == "n1408_guild_board-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1408_guild_board-1" then 
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_ClearCountingInfo(userObjID, questID);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function gqc14_3586_n1408_goddess_eye_time_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3586);
	 local qstep= 1; 
	 if qstep == 1 then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( userObjID ) >= 0 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end
		 end 
	 end 
end

function gqc14_3586_n1408_goddess_eye_time_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3586);
	if qstep == 2 and CountingType == 6 and CountIndex == 1 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_user_UserMessage(userObjID, 6, 8848, Ts);
				api_quest_SetCountingInfo(userObjID, questID, 2, 6, 0, 0);
	end
end

function gqc14_3586_n1408_goddess_eye_time_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3586);
	local questID=3586;
end

function gqc14_3586_n1408_goddess_eye_time_OnRemoteStart( userObjID, questID )
end

function gqc14_3586_n1408_goddess_eye_time_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function gqc14_3586_n1408_goddess_eye_time_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,3586, 1);
				api_quest_SetJournalStep(userObjID,3586, 1);
				api_quest_SetQuestStep(userObjID,3586, 1);
				npc_talk_index = "n1408_guild_board-1";
end

</VillageServer>

<GameServer>
function gqc14_3586_n1408_goddess_eye_time_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1408 then
		gqc14_3586_n1408_goddess_eye_time_OnTalk_n1408_guild_board( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1408_guild_board--------------------------------------------------------------------------------
function gqc14_3586_n1408_goddess_eye_time_OnTalk_n1408_guild_board( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1408_guild_board-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1408_guild_board-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1408_guild_board-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1408_guild_board-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1408_guild_board-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1408_guild_board-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, false);
			 end 

	end
	if npc_talk_index == "n1408_guild_board-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,3586, 1);
				api_quest_SetJournalStep( pRoom, userObjID,3586, 1);
				api_quest_SetQuestStep( pRoom, userObjID,3586, 1);
				npc_talk_index = "n1408_guild_board-1";

	end
	if npc_talk_index == "n1408_guild_board-1-a" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 6, 1, 900);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 5, 1061, 1 );
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n1408_guild_board-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 35860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 35860, questID, 1);
			 end 
	end
	if npc_talk_index == "n1408_guild_board-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1408_guild_board-1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function gqc14_3586_n1408_goddess_eye_time_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3586);
	 local qstep= 1; 
	 if qstep == 1 then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( pRoom,  userObjID ) >= 0 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end
		 end 
	 end 
end

function gqc14_3586_n1408_goddess_eye_time_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3586);
	if qstep == 2 and CountingType == 6 and CountIndex == 1 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_user_UserMessage( pRoom, userObjID, 6, 8848, Ts);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 6, 0, 0);
	end
end

function gqc14_3586_n1408_goddess_eye_time_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3586);
	local questID=3586;
end

function gqc14_3586_n1408_goddess_eye_time_OnRemoteStart( pRoom,  userObjID, questID )
end

function gqc14_3586_n1408_goddess_eye_time_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function gqc14_3586_n1408_goddess_eye_time_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,3586, 1);
				api_quest_SetJournalStep( pRoom, userObjID,3586, 1);
				api_quest_SetQuestStep( pRoom, userObjID,3586, 1);
				npc_talk_index = "n1408_guild_board-1";
end

</GameServer>