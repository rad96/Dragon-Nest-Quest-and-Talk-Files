<VillageServer>

function mqc18_9745_take_care_of_sister_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1816 then
		mqc18_9745_take_care_of_sister_OnTalk_n1816_vernicka(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2035 then
		mqc18_9745_take_care_of_sister_OnTalk_n2035_cityhall_grele(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2036 then
		mqc18_9745_take_care_of_sister_OnTalk_n2036_merca_iris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1816_vernicka--------------------------------------------------------------------------------
function mqc18_9745_take_care_of_sister_OnTalk_n1816_vernicka(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1816_vernicka-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1816_vernicka-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1816_vernicka-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1816_vernicka-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1816_vernicka-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9745, 2);
				api_quest_SetJournalStep(userObjID,9745, 1);
				api_quest_SetQuestStep(userObjID,9745, 1);
				npc_talk_index = "n1816_vernicka-1";

	end
	if npc_talk_index == "n1816_vernicka-2-b" then 
	end
	if npc_talk_index == "n1816_vernicka-2-c" then 
	end
	if npc_talk_index == "n1816_vernicka-2-d" then 
	end
	if npc_talk_index == "n1816_vernicka-2-e" then 
	end
	if npc_talk_index == "n1816_vernicka-2-f" then 
	end
	if npc_talk_index == "n1816_vernicka-2-g" then 
	end
	if npc_talk_index == "n1816_vernicka-2-h" then 
	end
	if npc_talk_index == "n1816_vernicka-2-i" then 
	end
	if npc_talk_index == "n1816_vernicka-2-j" then 
	end
	if npc_talk_index == "n1816_vernicka-2-k" then 
	end
	if npc_talk_index == "n1816_vernicka-2-l" then 
	end
	if npc_talk_index == "n1816_vernicka-2-m" then 
	end
	if npc_talk_index == "n1816_vernicka-2-n" then 
	end
	if npc_talk_index == "n1816_vernicka-2-o" then 
	end
	if npc_talk_index == "n1816_vernicka-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2035_cityhall_grele--------------------------------------------------------------------------------
function mqc18_9745_take_care_of_sister_OnTalk_n2035_cityhall_grele(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2035_cityhall_grele-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n2035_cityhall_grele-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2035_cityhall_grele-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2035_cityhall_grele-1-b" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-c" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-d" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-e" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-f" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-g" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-h" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-i" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-j" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-k" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-l" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2036_merca_iris--------------------------------------------------------------------------------
function mqc18_9745_take_care_of_sister_OnTalk_n2036_merca_iris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2036_merca_iris-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2036_merca_iris-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2036_merca_iris-3-b" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-c" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-d" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-e" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-f" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-g" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-h" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-i" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-j" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-k" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-l" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-m" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-n" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-o" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-p" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-q" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-r" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97450, true);
				 api_quest_RewardQuestUser(userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97450, true);
				 api_quest_RewardQuestUser(userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97450, true);
				 api_quest_RewardQuestUser(userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97450, true);
				 api_quest_RewardQuestUser(userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97450, true);
				 api_quest_RewardQuestUser(userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97450, true);
				 api_quest_RewardQuestUser(userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97450, true);
				 api_quest_RewardQuestUser(userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97450, true);
				 api_quest_RewardQuestUser(userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97450, true);
				 api_quest_RewardQuestUser(userObjID, 97450, questID, 1);
			 end 
	end
	if npc_talk_index == "n2036_merca_iris-3-s" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc18_9745_take_care_of_sister_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9745);
end

function mqc18_9745_take_care_of_sister_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9745);
end

function mqc18_9745_take_care_of_sister_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9745);
	local questID=9745;
end

function mqc18_9745_take_care_of_sister_OnRemoteStart( userObjID, questID )
end

function mqc18_9745_take_care_of_sister_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc18_9745_take_care_of_sister_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9745, 2);
				api_quest_SetJournalStep(userObjID,9745, 1);
				api_quest_SetQuestStep(userObjID,9745, 1);
				npc_talk_index = "n1816_vernicka-1";
end

</VillageServer>

<GameServer>
function mqc18_9745_take_care_of_sister_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1816 then
		mqc18_9745_take_care_of_sister_OnTalk_n1816_vernicka( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2035 then
		mqc18_9745_take_care_of_sister_OnTalk_n2035_cityhall_grele( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2036 then
		mqc18_9745_take_care_of_sister_OnTalk_n2036_merca_iris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1816_vernicka--------------------------------------------------------------------------------
function mqc18_9745_take_care_of_sister_OnTalk_n1816_vernicka( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1816_vernicka-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1816_vernicka-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1816_vernicka-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1816_vernicka-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1816_vernicka-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9745, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9745, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9745, 1);
				npc_talk_index = "n1816_vernicka-1";

	end
	if npc_talk_index == "n1816_vernicka-2-b" then 
	end
	if npc_talk_index == "n1816_vernicka-2-c" then 
	end
	if npc_talk_index == "n1816_vernicka-2-d" then 
	end
	if npc_talk_index == "n1816_vernicka-2-e" then 
	end
	if npc_talk_index == "n1816_vernicka-2-f" then 
	end
	if npc_talk_index == "n1816_vernicka-2-g" then 
	end
	if npc_talk_index == "n1816_vernicka-2-h" then 
	end
	if npc_talk_index == "n1816_vernicka-2-i" then 
	end
	if npc_talk_index == "n1816_vernicka-2-j" then 
	end
	if npc_talk_index == "n1816_vernicka-2-k" then 
	end
	if npc_talk_index == "n1816_vernicka-2-l" then 
	end
	if npc_talk_index == "n1816_vernicka-2-m" then 
	end
	if npc_talk_index == "n1816_vernicka-2-n" then 
	end
	if npc_talk_index == "n1816_vernicka-2-o" then 
	end
	if npc_talk_index == "n1816_vernicka-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2035_cityhall_grele--------------------------------------------------------------------------------
function mqc18_9745_take_care_of_sister_OnTalk_n2035_cityhall_grele( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2035_cityhall_grele-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n2035_cityhall_grele-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2035_cityhall_grele-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2035_cityhall_grele-1-b" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-c" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-d" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-e" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-f" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-g" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-h" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-i" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-j" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-k" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-1-l" then 
	end
	if npc_talk_index == "n2035_cityhall_grele-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2036_merca_iris--------------------------------------------------------------------------------
function mqc18_9745_take_care_of_sister_OnTalk_n2036_merca_iris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2036_merca_iris-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2036_merca_iris-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2036_merca_iris-3-b" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-c" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-d" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-e" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-f" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-g" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-h" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-i" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-j" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-k" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-l" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-m" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-n" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-o" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-p" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-q" then 
	end
	if npc_talk_index == "n2036_merca_iris-3-r" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97450, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97450, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97450, questID, 1);
			 end 
	end
	if npc_talk_index == "n2036_merca_iris-3-s" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc18_9745_take_care_of_sister_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9745);
end

function mqc18_9745_take_care_of_sister_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9745);
end

function mqc18_9745_take_care_of_sister_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9745);
	local questID=9745;
end

function mqc18_9745_take_care_of_sister_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc18_9745_take_care_of_sister_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc18_9745_take_care_of_sister_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9745, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9745, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9745, 1);
				npc_talk_index = "n1816_vernicka-1";
end

</GameServer>