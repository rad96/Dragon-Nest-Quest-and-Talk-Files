<VillageServer>

function sq05_152_hound_chaser_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 17 then
		sq05_152_hound_chaser_OnTalk_n017_trader_jeny(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n017_trader_jeny--------------------------------------------------------------------------------
function sq05_152_hound_chaser_OnTalk_n017_trader_jeny(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n017_trader_jeny-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n017_trader_jeny-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n017_trader_jeny-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n017_trader_jeny-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n017_trader_jeny-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n017_trader_jeny-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1521, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1521, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1522, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1522, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1523, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1521, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1521, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1521, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1521, false);
			 end 

	end
	if npc_talk_index == "n017_trader_jeny-accepting-acceptted" then
				api_quest_AddQuest(userObjID,152, 1);
				api_quest_SetJournalStep(userObjID,152, 1);
				api_quest_SetQuestStep(userObjID,152, 1);
				npc_talk_index = "n017_trader_jeny-1";

	end
	if npc_talk_index == "n017_trader_jeny-2" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 270, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300090, 1);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n017_trader_jeny-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1521, true);
				 api_quest_RewardQuestUser(userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1521, true);
				 api_quest_RewardQuestUser(userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1521, true);
				 api_quest_RewardQuestUser(userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1522, true);
				 api_quest_RewardQuestUser(userObjID, 1522, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1523, true);
				 api_quest_RewardQuestUser(userObjID, 1523, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1521, true);
				 api_quest_RewardQuestUser(userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1521, true);
				 api_quest_RewardQuestUser(userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1521, true);
				 api_quest_RewardQuestUser(userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1521, true);
				 api_quest_RewardQuestUser(userObjID, 1521, questID, 1);
			 end 
	end
	if npc_talk_index == "n017_trader_jeny-4-c" then 

				if api_quest_HasQuestItem(userObjID, 300090, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300090, api_quest_HasQuestItem(userObjID, 300090, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_152_hound_chaser_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 152);
	if qstep == 2 and CountIndex == 270 then
				api_quest_SetQuestStep(userObjID, questID,3);

				local TP_1_4_5 = {};
				api_user_UserMessage(userObjID,6, 1240020, TP_1_4_5);

	end
	if qstep == 2 and CountIndex == 300090 then

	end
end

function sq05_152_hound_chaser_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 152);
	if qstep == 2 and CountIndex == 270 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300090 and Count >= TargetCount  then

	end
end

function sq05_152_hound_chaser_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 152);
	local questID=152;
end

function sq05_152_hound_chaser_OnRemoteStart( userObjID, questID )
end

function sq05_152_hound_chaser_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_152_hound_chaser_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,152, 1);
				api_quest_SetJournalStep(userObjID,152, 1);
				api_quest_SetQuestStep(userObjID,152, 1);
				npc_talk_index = "n017_trader_jeny-1";
end

</VillageServer>

<GameServer>
function sq05_152_hound_chaser_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 17 then
		sq05_152_hound_chaser_OnTalk_n017_trader_jeny( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n017_trader_jeny--------------------------------------------------------------------------------
function sq05_152_hound_chaser_OnTalk_n017_trader_jeny( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n017_trader_jeny-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n017_trader_jeny-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n017_trader_jeny-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n017_trader_jeny-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n017_trader_jeny-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n017_trader_jeny-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1522, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1522, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1523, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, false);
			 end 

	end
	if npc_talk_index == "n017_trader_jeny-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,152, 1);
				api_quest_SetJournalStep( pRoom, userObjID,152, 1);
				api_quest_SetQuestStep( pRoom, userObjID,152, 1);
				npc_talk_index = "n017_trader_jeny-1";

	end
	if npc_talk_index == "n017_trader_jeny-2" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 270, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300090, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n017_trader_jeny-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1522, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1522, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1523, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1523, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1521, questID, 1);
			 end 
	end
	if npc_talk_index == "n017_trader_jeny-4-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300090, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300090, api_quest_HasQuestItem( pRoom, userObjID, 300090, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_152_hound_chaser_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 152);
	if qstep == 2 and CountIndex == 270 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);

				local TP_1_4_5 = {};
				api_user_UserMessage( pRoom, userObjID,6, 1240020, TP_1_4_5);

	end
	if qstep == 2 and CountIndex == 300090 then

	end
end

function sq05_152_hound_chaser_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 152);
	if qstep == 2 and CountIndex == 270 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300090 and Count >= TargetCount  then

	end
end

function sq05_152_hound_chaser_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 152);
	local questID=152;
end

function sq05_152_hound_chaser_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq05_152_hound_chaser_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_152_hound_chaser_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,152, 1);
				api_quest_SetJournalStep( pRoom, userObjID,152, 1);
				api_quest_SetQuestStep( pRoom, userObjID,152, 1);
				npc_talk_index = "n017_trader_jeny-1";
end

</GameServer>