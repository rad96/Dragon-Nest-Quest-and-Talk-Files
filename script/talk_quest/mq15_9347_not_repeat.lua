<VillageServer>

function mq15_9347_not_repeat_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1015 then
		mq15_9347_not_repeat_OnTalk_n1015_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1171 then
		mq15_9347_not_repeat_OnTalk_n1171_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1186 then
		mq15_9347_not_repeat_OnTalk_n1186_karahan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1187 then
		mq15_9347_not_repeat_OnTalk_n1187_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 575 then
		mq15_9347_not_repeat_OnTalk_n575_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		mq15_9347_not_repeat_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1015_academic--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n1015_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1015_academic-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1015_academic-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1015_academic-5-b" then 
	end
	if npc_talk_index == "n1015_academic-5-c" then 
	end
	if npc_talk_index == "n1015_academic-5-c" then 
	end
	if npc_talk_index == "n1015_academic-5-d" then 
	end
	if npc_talk_index == "n1015_academic-5-e" then 
	end
	if npc_talk_index == "n1015_academic-5-f" then 
	end
	if npc_talk_index == "n1015_academic-5-g" then 
	end
	if npc_talk_index == "n1015_academic-5-h" then 
	end
	if npc_talk_index == "n1015_academic-5-i" then 
	end
	if npc_talk_index == "n1015_academic-5-j" then 
	end
	if npc_talk_index == "n1015_academic-5-k" then 
	end
	if npc_talk_index == "n1015_academic-5-l" then 
	end
	if npc_talk_index == "n1015_academic-5-m" then 
	end
	if npc_talk_index == "n1015_academic-5-n" then 
	end
	if npc_talk_index == "n1015_academic-5-o" then 
	end
	if npc_talk_index == "n1015_academic-5-p" then 
	end
	if npc_talk_index == "n1015_academic-5-q" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93470, true);
				 api_quest_RewardQuestUser(userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93470, true);
				 api_quest_RewardQuestUser(userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93470, true);
				 api_quest_RewardQuestUser(userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93470, true);
				 api_quest_RewardQuestUser(userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93470, true);
				 api_quest_RewardQuestUser(userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93470, true);
				 api_quest_RewardQuestUser(userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93470, true);
				 api_quest_RewardQuestUser(userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93470, true);
				 api_quest_RewardQuestUser(userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93470, true);
				 api_quest_RewardQuestUser(userObjID, 93470, questID, 1);
			 end 
	end
	if npc_talk_index == "n1015_academic-5-r" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9348, 2);
					api_quest_SetQuestStep(userObjID, 9348, 1);
					api_quest_SetJournalStep(userObjID, 9348, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1015_academic-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1015_academic-1", "mq15_9348_extinction_of_illusion.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1171_illusion--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n1171_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1186_karahan--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n1186_karahan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1186_karahan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1186_karahan-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1186_karahan-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1186_karahan-2-b" then 
	end
	if npc_talk_index == "n1186_karahan-2-c" then 
	end
	if npc_talk_index == "n1186_karahan-2-d" then 
	end
	if npc_talk_index == "n1186_karahan-2-e" then 
	end
	if npc_talk_index == "n1186_karahan-2-f" then 
	end
	if npc_talk_index == "n1186_karahan-2-g" then 
	end
	if npc_talk_index == "n1186_karahan-2-h" then 
	end
	if npc_talk_index == "n1186_karahan-2-i" then 
	end
	if npc_talk_index == "n1186_karahan-2-j" then 
	end
	if npc_talk_index == "n1186_karahan-2-go_cutscene" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1187_lunaria--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n1187_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1187_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1187_lunaria-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1187_lunaria-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1187_lunaria-4-b" then 
	end
	if npc_talk_index == "n1187_lunaria-4-c" then 
	end
	if npc_talk_index == "n1187_lunaria-4-d" then 
	end
	if npc_talk_index == "n1187_lunaria-4-e" then 
	end
	if npc_talk_index == "n1187_lunaria-4-f" then 
	end
	if npc_talk_index == "n1187_lunaria-4-g" then 
	end
	if npc_talk_index == "n1187_lunaria-4-h" then 
	end
	if npc_talk_index == "n1187_lunaria-4-i" then 
	end
	if npc_talk_index == "n1187_lunaria-4-j" then 
	end
	if npc_talk_index == "n1187_lunaria-4-k" then 
	end
	if npc_talk_index == "n1187_lunaria-4-l" then 
	end
	if npc_talk_index == "n1187_lunaria-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n575_shadow_meow--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n575_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n575_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n575_shadow_meow-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n575_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9347, 2);
				api_quest_SetJournalStep(userObjID,9347, 1);
				api_quest_SetQuestStep(userObjID,9347, 1);
				npc_talk_index = "n575_shadow_meow-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n754_cian-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-1-partychk" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n754_cian-1-c";

				else
									npc_talk_index = "n754_cian-1-b";

				end
	end
	if npc_talk_index == "n754_cian-1-d" then 
	end
	if npc_talk_index == "n754_cian-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9347_not_repeat_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9347);
end

function mq15_9347_not_repeat_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9347);
end

function mq15_9347_not_repeat_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9347);
	local questID=9347;
end

function mq15_9347_not_repeat_OnRemoteStart( userObjID, questID )
end

function mq15_9347_not_repeat_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9347_not_repeat_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9347, 2);
				api_quest_SetJournalStep(userObjID,9347, 1);
				api_quest_SetQuestStep(userObjID,9347, 1);
				npc_talk_index = "n575_shadow_meow-1";
end

</VillageServer>

<GameServer>
function mq15_9347_not_repeat_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1015 then
		mq15_9347_not_repeat_OnTalk_n1015_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1171 then
		mq15_9347_not_repeat_OnTalk_n1171_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1186 then
		mq15_9347_not_repeat_OnTalk_n1186_karahan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1187 then
		mq15_9347_not_repeat_OnTalk_n1187_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 575 then
		mq15_9347_not_repeat_OnTalk_n575_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		mq15_9347_not_repeat_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1015_academic--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n1015_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1015_academic-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1015_academic-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1015_academic-5-b" then 
	end
	if npc_talk_index == "n1015_academic-5-c" then 
	end
	if npc_talk_index == "n1015_academic-5-c" then 
	end
	if npc_talk_index == "n1015_academic-5-d" then 
	end
	if npc_talk_index == "n1015_academic-5-e" then 
	end
	if npc_talk_index == "n1015_academic-5-f" then 
	end
	if npc_talk_index == "n1015_academic-5-g" then 
	end
	if npc_talk_index == "n1015_academic-5-h" then 
	end
	if npc_talk_index == "n1015_academic-5-i" then 
	end
	if npc_talk_index == "n1015_academic-5-j" then 
	end
	if npc_talk_index == "n1015_academic-5-k" then 
	end
	if npc_talk_index == "n1015_academic-5-l" then 
	end
	if npc_talk_index == "n1015_academic-5-m" then 
	end
	if npc_talk_index == "n1015_academic-5-n" then 
	end
	if npc_talk_index == "n1015_academic-5-o" then 
	end
	if npc_talk_index == "n1015_academic-5-p" then 
	end
	if npc_talk_index == "n1015_academic-5-q" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93470, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93470, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93470, questID, 1);
			 end 
	end
	if npc_talk_index == "n1015_academic-5-r" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9348, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9348, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9348, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1015_academic-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1015_academic-1", "mq15_9348_extinction_of_illusion.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1171_illusion--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n1171_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1186_karahan--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n1186_karahan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1186_karahan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1186_karahan-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1186_karahan-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1186_karahan-2-b" then 
	end
	if npc_talk_index == "n1186_karahan-2-c" then 
	end
	if npc_talk_index == "n1186_karahan-2-d" then 
	end
	if npc_talk_index == "n1186_karahan-2-e" then 
	end
	if npc_talk_index == "n1186_karahan-2-f" then 
	end
	if npc_talk_index == "n1186_karahan-2-g" then 
	end
	if npc_talk_index == "n1186_karahan-2-h" then 
	end
	if npc_talk_index == "n1186_karahan-2-i" then 
	end
	if npc_talk_index == "n1186_karahan-2-j" then 
	end
	if npc_talk_index == "n1186_karahan-2-go_cutscene" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1187_lunaria--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n1187_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1187_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1187_lunaria-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1187_lunaria-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1187_lunaria-4-b" then 
	end
	if npc_talk_index == "n1187_lunaria-4-c" then 
	end
	if npc_talk_index == "n1187_lunaria-4-d" then 
	end
	if npc_talk_index == "n1187_lunaria-4-e" then 
	end
	if npc_talk_index == "n1187_lunaria-4-f" then 
	end
	if npc_talk_index == "n1187_lunaria-4-g" then 
	end
	if npc_talk_index == "n1187_lunaria-4-h" then 
	end
	if npc_talk_index == "n1187_lunaria-4-i" then 
	end
	if npc_talk_index == "n1187_lunaria-4-j" then 
	end
	if npc_talk_index == "n1187_lunaria-4-k" then 
	end
	if npc_talk_index == "n1187_lunaria-4-l" then 
	end
	if npc_talk_index == "n1187_lunaria-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n575_shadow_meow--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n575_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n575_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n575_shadow_meow-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n575_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9347, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9347, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9347, 1);
				npc_talk_index = "n575_shadow_meow-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function mq15_9347_not_repeat_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n754_cian-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-1-partychk" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n754_cian-1-c";

				else
									npc_talk_index = "n754_cian-1-b";

				end
	end
	if npc_talk_index == "n754_cian-1-d" then 
	end
	if npc_talk_index == "n754_cian-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9347_not_repeat_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9347);
end

function mq15_9347_not_repeat_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9347);
end

function mq15_9347_not_repeat_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9347);
	local questID=9347;
end

function mq15_9347_not_repeat_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9347_not_repeat_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9347_not_repeat_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9347, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9347, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9347, 1);
				npc_talk_index = "n575_shadow_meow-1";
end

</GameServer>