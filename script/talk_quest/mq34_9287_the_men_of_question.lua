<VillageServer>

function mq34_9287_the_men_of_question_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9287_the_men_of_question_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1057 then
		mq34_9287_the_men_of_question_OnTalk_n1057_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mq34_9287_the_men_of_question_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1083 then
		mq34_9287_the_men_of_question_OnTalk_n1083_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq34_9287_the_men_of_question_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9287_the_men_of_question_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1047_argenta-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-5-b" then 
	end
	if npc_talk_index == "n1047_argenta-5-c" then 
	end
	if npc_talk_index == "n1047_argenta-5-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92871, true);
				 api_quest_RewardQuestUser(userObjID, 92871, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92871, true);
				 api_quest_RewardQuestUser(userObjID, 92871, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92873, true);
				 api_quest_RewardQuestUser(userObjID, 92873, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92874, true);
				 api_quest_RewardQuestUser(userObjID, 92874, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92875, true);
				 api_quest_RewardQuestUser(userObjID, 92875, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92876, true);
				 api_quest_RewardQuestUser(userObjID, 92876, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92877, true);
				 api_quest_RewardQuestUser(userObjID, 92877, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92878, true);
				 api_quest_RewardQuestUser(userObjID, 92878, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92879, true);
				 api_quest_RewardQuestUser(userObjID, 92879, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-5-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9288, 2);
					api_quest_SetQuestStep(userObjID, 9288, 1);
					api_quest_SetJournalStep(userObjID, 9288, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1057_yuvenciel--------------------------------------------------------------------------------
function mq34_9287_the_men_of_question_OnTalk_n1057_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1057_yuvenciel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1057_yuvenciel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1057_yuvenciel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1057_yuvenciel-3-b" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-job3_2" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1057_yuvenciel-3-c";

				else
									npc_talk_index = "n1057_yuvenciel-3-d";

				end
	end
	if npc_talk_index == "n1057_yuvenciel-3-e" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-e" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-job3_3" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1057_yuvenciel-3-f";

				else
									npc_talk_index = "n1057_yuvenciel-3-g";

				end
	end
	if npc_talk_index == "n1057_yuvenciel-3-h" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-h" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-i" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-i" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-k" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-k" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-k" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-l" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mq34_9287_the_men_of_question_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1083_triana--------------------------------------------------------------------------------
function mq34_9287_the_men_of_question_OnTalk_n1083_triana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1083_triana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1083_triana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1083_triana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1083_triana-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1083_triana-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1083_triana-2-job2_2" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1083_triana-2-b";

				else
									npc_talk_index = "n1083_triana-2-c";

				end
	end
	if npc_talk_index == "n1083_triana-2-d" then 
	end
	if npc_talk_index == "n1083_triana-2-d" then 
	end
	if npc_talk_index == "n1083_triana-2-job2_3" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1083_triana-2-e";

				else
									npc_talk_index = "n1083_triana-2-f";

				end
	end
	if npc_talk_index == "n1083_triana-2-g" then 
	end
	if npc_talk_index == "n1083_triana-2-g" then 
	end
	if npc_talk_index == "n1083_triana-2-job2_4" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1083_triana-2-h";

				else
									npc_talk_index = "n1083_triana-2-i";

				end
	end
	if npc_talk_index == "n1083_triana-2-j" then 
	end
	if npc_talk_index == "n1083_triana-2-j" then 
	end
	if npc_talk_index == "n1083_triana-2-k" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1083_triana-4-job4_2" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1083_triana-4-a";

				else
									npc_talk_index = "n1083_triana-4-b";

				end
	end
	if npc_talk_index == "n1083_triana-4-c" then 
	end
	if npc_talk_index == "n1083_triana-4-c" then 
	end
	if npc_talk_index == "n1083_triana-4-d" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq34_9287_the_men_of_question_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9287, 2);
				api_quest_SetJournalStep(userObjID,9287, 1);
				api_quest_SetQuestStep(userObjID,9287, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-1-b" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9287_the_men_of_question_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9287);
end

function mq34_9287_the_men_of_question_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9287);
end

function mq34_9287_the_men_of_question_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9287);
	local questID=9287;
end

function mq34_9287_the_men_of_question_OnRemoteStart( userObjID, questID )
end

function mq34_9287_the_men_of_question_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9287_the_men_of_question_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9287, 2);
				api_quest_SetJournalStep(userObjID,9287, 1);
				api_quest_SetQuestStep(userObjID,9287, 1);
				npc_talk_index = "n822_teramai-1";
end

</VillageServer>

<GameServer>
function mq34_9287_the_men_of_question_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9287_the_men_of_question_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1057 then
		mq34_9287_the_men_of_question_OnTalk_n1057_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mq34_9287_the_men_of_question_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1083 then
		mq34_9287_the_men_of_question_OnTalk_n1083_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq34_9287_the_men_of_question_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9287_the_men_of_question_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1047_argenta-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-5-b" then 
	end
	if npc_talk_index == "n1047_argenta-5-c" then 
	end
	if npc_talk_index == "n1047_argenta-5-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92871, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92871, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92871, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92871, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92873, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92873, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92874, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92874, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92875, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92875, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92876, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92876, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92877, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92877, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92878, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92878, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92879, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92879, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-5-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9288, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9288, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9288, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1057_yuvenciel--------------------------------------------------------------------------------
function mq34_9287_the_men_of_question_OnTalk_n1057_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1057_yuvenciel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1057_yuvenciel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1057_yuvenciel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1057_yuvenciel-3-b" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-job3_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1057_yuvenciel-3-c";

				else
									npc_talk_index = "n1057_yuvenciel-3-d";

				end
	end
	if npc_talk_index == "n1057_yuvenciel-3-e" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-e" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-job3_3" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1057_yuvenciel-3-f";

				else
									npc_talk_index = "n1057_yuvenciel-3-g";

				end
	end
	if npc_talk_index == "n1057_yuvenciel-3-h" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-h" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-i" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-i" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-k" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-k" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-k" then 
	end
	if npc_talk_index == "n1057_yuvenciel-3-l" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mq34_9287_the_men_of_question_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1083_triana--------------------------------------------------------------------------------
function mq34_9287_the_men_of_question_OnTalk_n1083_triana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1083_triana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1083_triana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1083_triana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1083_triana-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1083_triana-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1083_triana-2-job2_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1083_triana-2-b";

				else
									npc_talk_index = "n1083_triana-2-c";

				end
	end
	if npc_talk_index == "n1083_triana-2-d" then 
	end
	if npc_talk_index == "n1083_triana-2-d" then 
	end
	if npc_talk_index == "n1083_triana-2-job2_3" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1083_triana-2-e";

				else
									npc_talk_index = "n1083_triana-2-f";

				end
	end
	if npc_talk_index == "n1083_triana-2-g" then 
	end
	if npc_talk_index == "n1083_triana-2-g" then 
	end
	if npc_talk_index == "n1083_triana-2-job2_4" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1083_triana-2-h";

				else
									npc_talk_index = "n1083_triana-2-i";

				end
	end
	if npc_talk_index == "n1083_triana-2-j" then 
	end
	if npc_talk_index == "n1083_triana-2-j" then 
	end
	if npc_talk_index == "n1083_triana-2-k" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1083_triana-4-job4_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1083_triana-4-a";

				else
									npc_talk_index = "n1083_triana-4-b";

				end
	end
	if npc_talk_index == "n1083_triana-4-c" then 
	end
	if npc_talk_index == "n1083_triana-4-c" then 
	end
	if npc_talk_index == "n1083_triana-4-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq34_9287_the_men_of_question_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9287, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9287, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9287, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-1-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9287_the_men_of_question_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9287);
end

function mq34_9287_the_men_of_question_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9287);
end

function mq34_9287_the_men_of_question_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9287);
	local questID=9287;
end

function mq34_9287_the_men_of_question_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9287_the_men_of_question_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9287_the_men_of_question_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9287, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9287, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9287, 1);
				npc_talk_index = "n822_teramai-1";
end

</GameServer>