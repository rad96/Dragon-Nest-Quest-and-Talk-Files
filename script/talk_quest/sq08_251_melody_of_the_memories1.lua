<VillageServer>

function sq08_251_melody_of_the_memories1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 26 then
		sq08_251_melody_of_the_memories1_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 28 then
		sq08_251_melody_of_the_memories1_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 33 then
		sq08_251_melody_of_the_memories1_OnTalk_n033_archer_master_adellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_251_melody_of_the_memories1_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n026_trader_may-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n026_trader_may-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-2-b" then 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 
	end
	if npc_talk_index == "n026_trader_may-2-e" then 
	end
	if npc_talk_index == "n026_trader_may-2-f" then 
	end
	if npc_talk_index == "n026_trader_may-2-g" then 
	end
	if npc_talk_index == "n026_trader_may-2-h" then 
	end
	if npc_talk_index == "n026_trader_may-2-i" then 
	end
	if npc_talk_index == "n026_trader_may-3" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 350, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200350, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300165, 1);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n026_trader_may-4-level_check" then 
				if api_user_GetUserLevel(userObjID) >= 10 then
									npc_talk_index = "n026_trader_may-4-b";

				else
									npc_talk_index = "n026_trader_may-4-a";

				end
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2511, true);
				 api_quest_RewardQuestUser(userObjID, 2511, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2512, true);
				 api_quest_RewardQuestUser(userObjID, 2512, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2513, true);
				 api_quest_RewardQuestUser(userObjID, 2513, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2514, true);
				 api_quest_RewardQuestUser(userObjID, 2514, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2515, true);
				 api_quest_RewardQuestUser(userObjID, 2515, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2516, true);
				 api_quest_RewardQuestUser(userObjID, 2516, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2517, true);
				 api_quest_RewardQuestUser(userObjID, 2517, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2518, true);
				 api_quest_RewardQuestUser(userObjID, 2518, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2519, true);
				 api_quest_RewardQuestUser(userObjID, 2519, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-4-c" then 
				api_quest_MarkingCompleteQuest(userObjID, questID);
				api_quest_CompleteQuest(userObjID, questID, true);

				if api_quest_HasQuestItem(userObjID, 300165, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300165, api_quest_HasQuestItem(userObjID, 300165, 1));
				end
	end
	if npc_talk_index == "n026_trader_may-4-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 252, 1);
					api_quest_SetQuestStep(userObjID, 252, 1);
					api_quest_SetJournalStep(userObjID, 252, 1);

					
				if api_quest_HasQuestItem(userObjID, 300165, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300165, api_quest_HasQuestItem(userObjID, 300165, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n026_trader_may-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n026_trader_may-1", "sq08_252_melody_of_the_memories2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_251_melody_of_the_memories1_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-e" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-f" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-g" then 
	end
	if npc_talk_index == "n028_scholar_bailey-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n033_archer_master_adellin--------------------------------------------------------------------------------
function sq08_251_melody_of_the_memories1_OnTalk_n033_archer_master_adellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n033_archer_master_adellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n033_archer_master_adellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n033_archer_master_adellin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n033_archer_master_adellin-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2511, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2512, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2513, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2514, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2515, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2516, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2517, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2518, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2519, false);
			 end 

	end
	if npc_talk_index == "n033_archer_master_adellin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,251, 1);
				api_quest_SetJournalStep(userObjID,251, 1);
				api_quest_SetQuestStep(userObjID,251, 1);
				npc_talk_index = "n033_archer_master_adellin-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_251_melody_of_the_memories1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 251);
	if qstep == 3 and CountIndex == 350 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300165, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300165, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300165 then
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 3 and CountIndex == 200350 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300165, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300165, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_251_melody_of_the_memories1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 251);
	if qstep == 3 and CountIndex == 350 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300165 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200350 and Count >= TargetCount  then

	end
end

function sq08_251_melody_of_the_memories1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 251);
	local questID=251;
end

function sq08_251_melody_of_the_memories1_OnRemoteStart( userObjID, questID )
end

function sq08_251_melody_of_the_memories1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_251_melody_of_the_memories1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,251, 1);
				api_quest_SetJournalStep(userObjID,251, 1);
				api_quest_SetQuestStep(userObjID,251, 1);
				npc_talk_index = "n033_archer_master_adellin-1";
end

</VillageServer>

<GameServer>
function sq08_251_melody_of_the_memories1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 26 then
		sq08_251_melody_of_the_memories1_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 28 then
		sq08_251_melody_of_the_memories1_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 33 then
		sq08_251_melody_of_the_memories1_OnTalk_n033_archer_master_adellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_251_melody_of_the_memories1_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n026_trader_may-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n026_trader_may-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-2-b" then 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 
	end
	if npc_talk_index == "n026_trader_may-2-e" then 
	end
	if npc_talk_index == "n026_trader_may-2-f" then 
	end
	if npc_talk_index == "n026_trader_may-2-g" then 
	end
	if npc_talk_index == "n026_trader_may-2-h" then 
	end
	if npc_talk_index == "n026_trader_may-2-i" then 
	end
	if npc_talk_index == "n026_trader_may-3" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 350, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200350, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300165, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n026_trader_may-4-level_check" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 10 then
									npc_talk_index = "n026_trader_may-4-b";

				else
									npc_talk_index = "n026_trader_may-4-a";

				end
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2511, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2511, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2512, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2512, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2513, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2513, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2514, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2514, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2515, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2515, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2516, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2516, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2517, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2517, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2518, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2518, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2519, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2519, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-4-c" then 
				api_quest_MarkingCompleteQuest( pRoom, userObjID, questID);
				api_quest_CompleteQuest( pRoom, userObjID, questID, true);

				if api_quest_HasQuestItem( pRoom, userObjID, 300165, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300165, api_quest_HasQuestItem( pRoom, userObjID, 300165, 1));
				end
	end
	if npc_talk_index == "n026_trader_may-4-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 252, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 252, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 252, 1);

					
				if api_quest_HasQuestItem( pRoom, userObjID, 300165, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300165, api_quest_HasQuestItem( pRoom, userObjID, 300165, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n026_trader_may-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n026_trader_may-1", "sq08_252_melody_of_the_memories2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_251_melody_of_the_memories1_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-e" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-f" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-g" then 
	end
	if npc_talk_index == "n028_scholar_bailey-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n033_archer_master_adellin--------------------------------------------------------------------------------
function sq08_251_melody_of_the_memories1_OnTalk_n033_archer_master_adellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n033_archer_master_adellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n033_archer_master_adellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n033_archer_master_adellin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n033_archer_master_adellin-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2511, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2512, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2513, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2514, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2515, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2516, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2517, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2518, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2519, false);
			 end 

	end
	if npc_talk_index == "n033_archer_master_adellin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,251, 1);
				api_quest_SetJournalStep( pRoom, userObjID,251, 1);
				api_quest_SetQuestStep( pRoom, userObjID,251, 1);
				npc_talk_index = "n033_archer_master_adellin-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_251_melody_of_the_memories1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 251);
	if qstep == 3 and CountIndex == 350 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300165, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300165, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300165 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 3 and CountIndex == 200350 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300165, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300165, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_251_melody_of_the_memories1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 251);
	if qstep == 3 and CountIndex == 350 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300165 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200350 and Count >= TargetCount  then

	end
end

function sq08_251_melody_of_the_memories1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 251);
	local questID=251;
end

function sq08_251_melody_of_the_memories1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_251_melody_of_the_memories1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_251_melody_of_the_memories1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,251, 1);
				api_quest_SetJournalStep( pRoom, userObjID,251, 1);
				api_quest_SetQuestStep( pRoom, userObjID,251, 1);
				npc_talk_index = "n033_archer_master_adellin-1";
end

</GameServer>