<VillageServer>

function mq15_737_truth_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 394 then
		mq15_737_truth_OnTalk_n394_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_737_truth_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 709 then
		mq15_737_truth_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 794 then
		mq15_737_truth_OnTalk_n794_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n394_academic_station--------------------------------------------------------------------------------
function mq15_737_truth_OnTalk_n394_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n394_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n394_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n394_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,737, 2);
				api_quest_SetJournalStep(userObjID,737, 1);
				api_quest_SetQuestStep(userObjID,737, 1);
				npc_talk_index = "n394_academic_station-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_737_truth_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n701_oldkarakule-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-1-b" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n709_trader_lucita--------------------------------------------------------------------------------
function mq15_737_truth_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n794_angelica--------------------------------------------------------------------------------
function mq15_737_truth_OnTalk_n794_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n794_angelica-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n794_angelica-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n794_angelica-2-b" then 
	end
	if npc_talk_index == "n794_angelica-2-c" then 
	end
	if npc_talk_index == "n794_angelica-2-d" then 
	end
	if npc_talk_index == "n794_angelica-2-e" then 
	end
	if npc_talk_index == "n794_angelica-2-f" then 
	end
	if npc_talk_index == "n794_angelica-2-f" then 
	end
	if npc_talk_index == "n794_angelica-2-g" then 
	end
	if npc_talk_index == "n794_angelica-2-h" then 
	end
	if npc_talk_index == "n794_angelica-2-i" then 
	end
	if npc_talk_index == "n794_angelica-2-j" then 
	end
	if npc_talk_index == "n794_angelica-2-k" then 
	end
	if npc_talk_index == "n794_angelica-2-l" then 
	end
	if npc_talk_index == "n794_angelica-2-m" then 
	end
	if npc_talk_index == "n794_angelica-2-n" then 
	end
	if npc_talk_index == "n794_angelica-2-o" then 
	end
	if npc_talk_index == "n794_angelica-2-p" then 
	end
	if npc_talk_index == "n794_angelica-2-q" then 
	end
	if npc_talk_index == "n794_angelica-2-r" then 
	end
	if npc_talk_index == "n794_angelica-2-s" then 
	end
	if npc_talk_index == "n794_angelica-2-t" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7370, true);
				 api_quest_RewardQuestUser(userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7370, true);
				 api_quest_RewardQuestUser(userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7370, true);
				 api_quest_RewardQuestUser(userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7370, true);
				 api_quest_RewardQuestUser(userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7370, true);
				 api_quest_RewardQuestUser(userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7370, true);
				 api_quest_RewardQuestUser(userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7370, true);
				 api_quest_RewardQuestUser(userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7370, true);
				 api_quest_RewardQuestUser(userObjID, 7370, questID, 1);
			 end 
	end
	if npc_talk_index == "n794_angelica-2-clear_check" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 738, 2);
					api_quest_SetQuestStep(userObjID, 738, 1);
					api_quest_SetJournalStep(userObjID, 738, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				if api_quest_IsMarkingCompleteQuest(userObjID, 747) == 1  then
									if api_quest_IsMarkingCompleteQuest(userObjID, 750) == 1  then
									npc_talk_index = "n794_angelica-2-w";
				api_quest_SetQuestStep(userObjID, 738, 2);
				api_quest_SetJournalStep(userObjID, 738, 2);

				else
									npc_talk_index = "n794_angelica-2-v";

				end

				else
									if api_user_GetUserLevel(userObjID) >= 44 then
									if api_quest_IsMarkingCompleteQuest(userObjID, 378) == 1  then
									if api_quest_IsPlayingQuestMaximum(userObjID) == 1  then
									npc_talk_index = "n794_angelica-2-z";

				else
									api_quest_AddQuest(userObjID, 747, 1);
				api_quest_SetQuestStep(userObjID, 747, 1);
				api_quest_SetJournalStep(userObjID, 747, 1);
				npc_talk_index = "n794_angelica-2-u";

				end

				else
									npc_talk_index = "n794_angelica-2-x";

				end

				else
									npc_talk_index = "n794_angelica-2-y";

				end

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_737_truth_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 737);
end

function mq15_737_truth_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 737);
end

function mq15_737_truth_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 737);
	local questID=737;
end

function mq15_737_truth_OnRemoteStart( userObjID, questID )
end

function mq15_737_truth_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_737_truth_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,737, 2);
				api_quest_SetJournalStep(userObjID,737, 1);
				api_quest_SetQuestStep(userObjID,737, 1);
				npc_talk_index = "n394_academic_station-1";
end

</VillageServer>

<GameServer>
function mq15_737_truth_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 394 then
		mq15_737_truth_OnTalk_n394_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_737_truth_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 709 then
		mq15_737_truth_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 794 then
		mq15_737_truth_OnTalk_n794_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n394_academic_station--------------------------------------------------------------------------------
function mq15_737_truth_OnTalk_n394_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n394_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n394_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n394_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,737, 2);
				api_quest_SetJournalStep( pRoom, userObjID,737, 1);
				api_quest_SetQuestStep( pRoom, userObjID,737, 1);
				npc_talk_index = "n394_academic_station-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_737_truth_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n701_oldkarakule-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-1-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n709_trader_lucita--------------------------------------------------------------------------------
function mq15_737_truth_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n794_angelica--------------------------------------------------------------------------------
function mq15_737_truth_OnTalk_n794_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n794_angelica-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n794_angelica-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n794_angelica-2-b" then 
	end
	if npc_talk_index == "n794_angelica-2-c" then 
	end
	if npc_talk_index == "n794_angelica-2-d" then 
	end
	if npc_talk_index == "n794_angelica-2-e" then 
	end
	if npc_talk_index == "n794_angelica-2-f" then 
	end
	if npc_talk_index == "n794_angelica-2-f" then 
	end
	if npc_talk_index == "n794_angelica-2-g" then 
	end
	if npc_talk_index == "n794_angelica-2-h" then 
	end
	if npc_talk_index == "n794_angelica-2-i" then 
	end
	if npc_talk_index == "n794_angelica-2-j" then 
	end
	if npc_talk_index == "n794_angelica-2-k" then 
	end
	if npc_talk_index == "n794_angelica-2-l" then 
	end
	if npc_talk_index == "n794_angelica-2-m" then 
	end
	if npc_talk_index == "n794_angelica-2-n" then 
	end
	if npc_talk_index == "n794_angelica-2-o" then 
	end
	if npc_talk_index == "n794_angelica-2-p" then 
	end
	if npc_talk_index == "n794_angelica-2-q" then 
	end
	if npc_talk_index == "n794_angelica-2-r" then 
	end
	if npc_talk_index == "n794_angelica-2-s" then 
	end
	if npc_talk_index == "n794_angelica-2-t" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7370, questID, 1);
			 end 
	end
	if npc_talk_index == "n794_angelica-2-clear_check" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 738, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 738, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 738, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 747) == 1  then
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 750) == 1  then
									npc_talk_index = "n794_angelica-2-w";
				api_quest_SetQuestStep( pRoom, userObjID, 738, 2);
				api_quest_SetJournalStep( pRoom, userObjID, 738, 2);

				else
									npc_talk_index = "n794_angelica-2-v";

				end

				else
									if api_user_GetUserLevel( pRoom, userObjID) >= 44 then
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 378) == 1  then
									if api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1  then
									npc_talk_index = "n794_angelica-2-z";

				else
									api_quest_AddQuest( pRoom, userObjID, 747, 1);
				api_quest_SetQuestStep( pRoom, userObjID, 747, 1);
				api_quest_SetJournalStep( pRoom, userObjID, 747, 1);
				npc_talk_index = "n794_angelica-2-u";

				end

				else
									npc_talk_index = "n794_angelica-2-x";

				end

				else
									npc_talk_index = "n794_angelica-2-y";

				end

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_737_truth_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 737);
end

function mq15_737_truth_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 737);
end

function mq15_737_truth_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 737);
	local questID=737;
end

function mq15_737_truth_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_737_truth_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_737_truth_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,737, 2);
				api_quest_SetJournalStep( pRoom, userObjID,737, 1);
				api_quest_SetQuestStep( pRoom, userObjID,737, 1);
				npc_talk_index = "n394_academic_station-1";
end

</GameServer>