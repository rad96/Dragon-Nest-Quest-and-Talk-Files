<VillageServer>

function nq08_6301_thanx_visitor_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 201 then
		nq08_6301_thanx_visitor_OnTalk_n201_darklair_millefeuille(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n201_darklair_millefeuille--------------------------------------------------------------------------------
function nq08_6301_thanx_visitor_OnTalk_n201_darklair_millefeuille(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n201_darklair_millefeuille-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n201_darklair_millefeuille-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n201_darklair_millefeuille-accepting-acceppted" then
				api_quest_AddQuest(userObjID,6301, 1);
				api_quest_SetJournalStep(userObjID,6301, 1);
				api_quest_SetQuestStep(userObjID,6301, 1);
				npc_talk_index = "n201_darklair_millefeuille-1";

	end
	if npc_talk_index == "n201_darklair_millefeuille-1-b" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-c" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-d" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-d" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-e" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 201, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-g" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq08_6301_thanx_visitor_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6301);
end

function nq08_6301_thanx_visitor_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6301);
end

function nq08_6301_thanx_visitor_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 6301);
	local questID=6301;
end

function nq08_6301_thanx_visitor_OnRemoteStart( userObjID, questID )
end

function nq08_6301_thanx_visitor_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq08_6301_thanx_visitor_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq08_6301_thanx_visitor_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 201 then
		nq08_6301_thanx_visitor_OnTalk_n201_darklair_millefeuille( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n201_darklair_millefeuille--------------------------------------------------------------------------------
function nq08_6301_thanx_visitor_OnTalk_n201_darklair_millefeuille( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n201_darklair_millefeuille-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n201_darklair_millefeuille-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n201_darklair_millefeuille-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,6301, 1);
				api_quest_SetJournalStep( pRoom, userObjID,6301, 1);
				api_quest_SetQuestStep( pRoom, userObjID,6301, 1);
				npc_talk_index = "n201_darklair_millefeuille-1";

	end
	if npc_talk_index == "n201_darklair_millefeuille-1-b" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-c" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-d" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-d" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-e" then 
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 201, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n201_darklair_millefeuille-1-g" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq08_6301_thanx_visitor_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6301);
end

function nq08_6301_thanx_visitor_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6301);
end

function nq08_6301_thanx_visitor_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 6301);
	local questID=6301;
end

function nq08_6301_thanx_visitor_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq08_6301_thanx_visitor_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq08_6301_thanx_visitor_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>