<VillageServer>

function sq11_547_jewelry_red_tears_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 277 then
		sq11_547_jewelry_red_tears_OnTalk_n277_bluff_dealers_popo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_popo--------------------------------------------------------------------------------
function sq11_547_jewelry_red_tears_OnTalk_n277_bluff_dealers_popo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n277_bluff_dealers_popo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n277_bluff_dealers_popo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n277_bluff_dealers_popo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_popo-accepting-i" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5471, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5472, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5473, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5474, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5475, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5476, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5477, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5478, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5479, false);
			 end 

	end
	if npc_talk_index == "n277_bluff_dealers_popo-accepting-acceptted" then
				api_quest_AddQuest(userObjID,547, 1);
				api_quest_SetJournalStep(userObjID,547, 1);
				api_quest_SetQuestStep(userObjID,547, 1);
				npc_talk_index = "n277_bluff_dealers_popo-1";

	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-b" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-c" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-d" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-e" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-f" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1081, 30001);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201081, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400153, 30001);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestMemo(userObjID, questID, 1, 1);
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-havecheck1" then 
				if api_quest_HasQuestItem(userObjID, 400153, 1) >= 1 then
									npc_talk_index = "n277_bluff_dealers_popo-2-a";

				else
									npc_talk_index = "n277_bluff_dealers_popo-2-f";

				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-b" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-c" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-g" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-havecheck1" then 
				if api_quest_HasQuestItem(userObjID, 400153, 1) >= 1 then
									npc_talk_index = "n277_bluff_dealers_popo-2-a";

				else
									npc_talk_index = "n277_bluff_dealers_popo-2-f";

				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-e" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-h" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-i" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5471, true);
				 api_quest_RewardQuestUser(userObjID, 5471, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5472, true);
				 api_quest_RewardQuestUser(userObjID, 5472, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5473, true);
				 api_quest_RewardQuestUser(userObjID, 5473, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5474, true);
				 api_quest_RewardQuestUser(userObjID, 5474, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5475, true);
				 api_quest_RewardQuestUser(userObjID, 5475, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5476, true);
				 api_quest_RewardQuestUser(userObjID, 5476, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5477, true);
				 api_quest_RewardQuestUser(userObjID, 5477, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5478, true);
				 api_quest_RewardQuestUser(userObjID, 5478, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5479, true);
				 api_quest_RewardQuestUser(userObjID, 5479, questID, 1);
			 end 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-k" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem(userObjID, 400153, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400153, api_quest_HasQuestItem(userObjID, 400153, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-l" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-m" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_547_jewelry_red_tears_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 547);
	if qstep == 2 and CountIndex == 1081 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400153, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400153, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400153 then
				if api_quest_HasQuestItem(userObjID, 400153, 1) >= 1 then
									api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 201081 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400153, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400153, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_547_jewelry_red_tears_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 547);
	if qstep == 2 and CountIndex == 1081 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400153 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201081 and Count >= TargetCount  then

	end
end

function sq11_547_jewelry_red_tears_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 547);
	local questID=547;
end

function sq11_547_jewelry_red_tears_OnRemoteStart( userObjID, questID )
end

function sq11_547_jewelry_red_tears_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_547_jewelry_red_tears_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,547, 1);
				api_quest_SetJournalStep(userObjID,547, 1);
				api_quest_SetQuestStep(userObjID,547, 1);
				npc_talk_index = "n277_bluff_dealers_popo-1";
end

</VillageServer>

<GameServer>
function sq11_547_jewelry_red_tears_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 277 then
		sq11_547_jewelry_red_tears_OnTalk_n277_bluff_dealers_popo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_popo--------------------------------------------------------------------------------
function sq11_547_jewelry_red_tears_OnTalk_n277_bluff_dealers_popo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n277_bluff_dealers_popo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n277_bluff_dealers_popo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n277_bluff_dealers_popo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_popo-accepting-i" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5471, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5472, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5473, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5474, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5475, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5476, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5477, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5478, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5479, false);
			 end 

	end
	if npc_talk_index == "n277_bluff_dealers_popo-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,547, 1);
				api_quest_SetJournalStep( pRoom, userObjID,547, 1);
				api_quest_SetQuestStep( pRoom, userObjID,547, 1);
				npc_talk_index = "n277_bluff_dealers_popo-1";

	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-b" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-c" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-d" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-e" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-1-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1081, 30001);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201081, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400153, 30001);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 1);
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-havecheck1" then 
				if api_quest_HasQuestItem( pRoom, userObjID, 400153, 1) >= 1 then
									npc_talk_index = "n277_bluff_dealers_popo-2-a";

				else
									npc_talk_index = "n277_bluff_dealers_popo-2-f";

				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-b" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-c" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-g" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-havecheck1" then 
				if api_quest_HasQuestItem( pRoom, userObjID, 400153, 1) >= 1 then
									npc_talk_index = "n277_bluff_dealers_popo-2-a";

				else
									npc_talk_index = "n277_bluff_dealers_popo-2-f";

				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-e" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-h" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-i" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5471, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5471, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5472, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5472, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5473, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5473, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5474, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5474, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5475, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5475, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5476, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5476, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5477, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5477, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5478, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5478, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5479, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5479, questID, 1);
			 end 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-k" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem( pRoom, userObjID, 400153, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400153, api_quest_HasQuestItem( pRoom, userObjID, 400153, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-l" then 
	end
	if npc_talk_index == "n277_bluff_dealers_popo-2-m" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_547_jewelry_red_tears_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 547);
	if qstep == 2 and CountIndex == 1081 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400153, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400153, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400153 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400153, 1) >= 1 then
									api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 201081 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400153, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400153, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_547_jewelry_red_tears_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 547);
	if qstep == 2 and CountIndex == 1081 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400153 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201081 and Count >= TargetCount  then

	end
end

function sq11_547_jewelry_red_tears_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 547);
	local questID=547;
end

function sq11_547_jewelry_red_tears_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_547_jewelry_red_tears_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_547_jewelry_red_tears_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,547, 1);
				api_quest_SetJournalStep( pRoom, userObjID,547, 1);
				api_quest_SetQuestStep( pRoom, userObjID,547, 1);
				npc_talk_index = "n277_bluff_dealers_popo-1";
end

</GameServer>