<VillageServer>

function sq11_542_revenge_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 252 then
		sq11_542_revenge_OnTalk_n252_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n252_saint_guard--------------------------------------------------------------------------------
function sq11_542_revenge_OnTalk_n252_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n252_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n252_saint_guard-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n252_saint_guard-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n252_saint_guard-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5421, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5422, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5423, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5424, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5425, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5426, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5427, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5428, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5429, false);
			 end 

	end
	if npc_talk_index == "n252_saint_guard-accepting-acceptted" then
				api_quest_AddQuest(userObjID,542, 1);
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 970, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200970, 30000);
				api_quest_SetJournalStep(userObjID, questID, 1);
				npc_talk_index = "n252_saint_guard-1";

	end
	if npc_talk_index == "n252_saint_guard-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5421, true);
				 api_quest_RewardQuestUser(userObjID, 5421, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5422, true);
				 api_quest_RewardQuestUser(userObjID, 5422, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5423, true);
				 api_quest_RewardQuestUser(userObjID, 5423, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5424, true);
				 api_quest_RewardQuestUser(userObjID, 5424, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5425, true);
				 api_quest_RewardQuestUser(userObjID, 5425, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5426, true);
				 api_quest_RewardQuestUser(userObjID, 5426, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5427, true);
				 api_quest_RewardQuestUser(userObjID, 5427, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5428, true);
				 api_quest_RewardQuestUser(userObjID, 5428, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5429, true);
				 api_quest_RewardQuestUser(userObjID, 5429, questID, 1);
			 end 
	end
	if npc_talk_index == "n252_saint_guard-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_542_revenge_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 542);
	if qstep == 1 and CountIndex == 970 then

	end
	if qstep == 1 and CountIndex == 200970 then
				api_quest_IncCounting(userObjID, 2, 970);

	end
end

function sq11_542_revenge_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 542);
	if qstep == 1 and CountIndex == 970 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 200970 and Count >= TargetCount  then

	end
end

function sq11_542_revenge_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 542);
	local questID=542;
end

function sq11_542_revenge_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 970, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200970, 30000);
				api_quest_SetJournalStep(userObjID, questID, 1);
end

function sq11_542_revenge_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_542_revenge_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,542, 1);
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 970, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200970, 30000);
				api_quest_SetJournalStep(userObjID, questID, 1);
				npc_talk_index = "n252_saint_guard-1";
end

</VillageServer>

<GameServer>
function sq11_542_revenge_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 252 then
		sq11_542_revenge_OnTalk_n252_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n252_saint_guard--------------------------------------------------------------------------------
function sq11_542_revenge_OnTalk_n252_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n252_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n252_saint_guard-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n252_saint_guard-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n252_saint_guard-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5421, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5422, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5423, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5424, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5425, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5426, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5427, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5428, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5429, false);
			 end 

	end
	if npc_talk_index == "n252_saint_guard-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,542, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 970, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200970, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				npc_talk_index = "n252_saint_guard-1";

	end
	if npc_talk_index == "n252_saint_guard-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5421, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5421, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5422, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5422, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5423, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5423, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5424, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5424, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5425, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5425, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5426, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5426, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5427, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5427, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5428, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5428, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5429, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5429, questID, 1);
			 end 
	end
	if npc_talk_index == "n252_saint_guard-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_542_revenge_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 542);
	if qstep == 1 and CountIndex == 970 then

	end
	if qstep == 1 and CountIndex == 200970 then
				api_quest_IncCounting( pRoom, userObjID, 2, 970);

	end
end

function sq11_542_revenge_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 542);
	if qstep == 1 and CountIndex == 970 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 200970 and Count >= TargetCount  then

	end
end

function sq11_542_revenge_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 542);
	local questID=542;
end

function sq11_542_revenge_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 970, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200970, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
end

function sq11_542_revenge_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_542_revenge_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,542, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 970, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200970, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				npc_talk_index = "n252_saint_guard-1";
end

</GameServer>