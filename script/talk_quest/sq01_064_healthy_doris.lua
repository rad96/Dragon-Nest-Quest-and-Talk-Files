<VillageServer>

function sq01_064_healthy_doris_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 3 then
		sq01_064_healthy_doris_OnTalk_n003_trader_dolis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n003_trader_dolis--------------------------------------------------------------------------------
function sq01_064_healthy_doris_OnTalk_n003_trader_dolis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n003_trader_dolis-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n003_trader_dolis-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n003_trader_dolis-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n003_trader_dolis-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 641, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 642, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 642, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 642, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 641, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 646, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 647, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 641, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 641, false);
			 end 

	end
	if npc_talk_index == "n003_trader_dolis-accepting-acceptted" then
				api_quest_AddQuest(userObjID,64, 1);
				api_quest_SetJournalStep(userObjID,64, 1);
				api_quest_SetQuestStep(userObjID,64, 1);
				npc_talk_index = "n003_trader_dolis-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 143, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300094, 5);

	end
	if npc_talk_index == "n003_trader_dolis-2-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 641, true);
				 api_quest_RewardQuestUser(userObjID, 641, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 642, true);
				 api_quest_RewardQuestUser(userObjID, 642, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 642, true);
				 api_quest_RewardQuestUser(userObjID, 642, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 642, true);
				 api_quest_RewardQuestUser(userObjID, 642, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 641, true);
				 api_quest_RewardQuestUser(userObjID, 641, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 646, true);
				 api_quest_RewardQuestUser(userObjID, 646, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 647, true);
				 api_quest_RewardQuestUser(userObjID, 647, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 641, true);
				 api_quest_RewardQuestUser(userObjID, 641, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 641, true);
				 api_quest_RewardQuestUser(userObjID, 641, questID, 1);
			 end 
	end
	if npc_talk_index == "n003_trader_dolis-2-d" then 

				if api_quest_HasQuestItem(userObjID, 300094, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300094, api_quest_HasQuestItem(userObjID, 300094, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_064_healthy_doris_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 64);
	if qstep == 1 and CountIndex == 143 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300094, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300094, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300094 then

	end
end

function sq01_064_healthy_doris_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 64);
	if qstep == 1 and CountIndex == 143 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300094 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function sq01_064_healthy_doris_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 64);
	local questID=64;
end

function sq01_064_healthy_doris_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 143, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300094, 5);
end

function sq01_064_healthy_doris_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq01_064_healthy_doris_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,64, 1);
				api_quest_SetJournalStep(userObjID,64, 1);
				api_quest_SetQuestStep(userObjID,64, 1);
				npc_talk_index = "n003_trader_dolis-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 143, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300094, 5);
end

</VillageServer>

<GameServer>
function sq01_064_healthy_doris_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 3 then
		sq01_064_healthy_doris_OnTalk_n003_trader_dolis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n003_trader_dolis--------------------------------------------------------------------------------
function sq01_064_healthy_doris_OnTalk_n003_trader_dolis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n003_trader_dolis-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n003_trader_dolis-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n003_trader_dolis-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n003_trader_dolis-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 641, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 642, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 642, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 642, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 641, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 646, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 647, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 641, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 641, false);
			 end 

	end
	if npc_talk_index == "n003_trader_dolis-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,64, 1);
				api_quest_SetJournalStep( pRoom, userObjID,64, 1);
				api_quest_SetQuestStep( pRoom, userObjID,64, 1);
				npc_talk_index = "n003_trader_dolis-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 143, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300094, 5);

	end
	if npc_talk_index == "n003_trader_dolis-2-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 641, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 641, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 642, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 642, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 642, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 642, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 642, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 642, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 641, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 641, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 646, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 646, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 647, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 647, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 641, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 641, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 641, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 641, questID, 1);
			 end 
	end
	if npc_talk_index == "n003_trader_dolis-2-d" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300094, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300094, api_quest_HasQuestItem( pRoom, userObjID, 300094, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_064_healthy_doris_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 64);
	if qstep == 1 and CountIndex == 143 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300094, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300094, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300094 then

	end
end

function sq01_064_healthy_doris_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 64);
	if qstep == 1 and CountIndex == 143 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300094 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function sq01_064_healthy_doris_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 64);
	local questID=64;
end

function sq01_064_healthy_doris_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 143, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300094, 5);
end

function sq01_064_healthy_doris_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq01_064_healthy_doris_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,64, 1);
				api_quest_SetJournalStep( pRoom, userObjID,64, 1);
				api_quest_SetQuestStep( pRoom, userObjID,64, 1);
				npc_talk_index = "n003_trader_dolis-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 143, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300094, 5);
end

</GameServer>