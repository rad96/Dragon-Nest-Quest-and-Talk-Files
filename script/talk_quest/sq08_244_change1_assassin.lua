<VillageServer>

function sq08_244_change1_assassin_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1193 then
		sq08_244_change1_assassin_OnTalk_n1193_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1194 then
		sq08_244_change1_assassin_OnTalk_n1194_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1221 then
		sq08_244_change1_assassin_OnTalk_n1221_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 573 then
		sq08_244_change1_assassin_OnTalk_n573_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1193_illusion--------------------------------------------------------------------------------
function sq08_244_change1_assassin_OnTalk_n1193_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1193_illusion-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1193_illusion-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1193_illusion-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1193_illusion-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1193_illusion-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1193_illusion-3-b" then 
	end
	if npc_talk_index == "n1193_illusion-3-c" then 
	end
	if npc_talk_index == "n1193_illusion-3-d" then 
	end
	if npc_talk_index == "n1193_illusion-3-e" then 
	end
	if npc_talk_index == "n1193_illusion-3-f" then 
	end
	if npc_talk_index == "n1193_illusion-3-g" then 
	end
	if npc_talk_index == "n1193_illusion-3-h" then 
	end
	if npc_talk_index == "n1193_illusion-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-c" then 
	end
	if npc_talk_index == "n1193_illusion-5-d" then 
	end
	if npc_talk_index == "n1193_illusion-5-e" then 
	end
	if npc_talk_index == "n1193_illusion-5-e" then 
	end
	if npc_talk_index == "n1193_illusion-5-f" then 
	end
	if npc_talk_index == "n1193_illusion-5-g" then 
	end
	if npc_talk_index == "n1193_illusion-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1194_lunaria--------------------------------------------------------------------------------
function sq08_244_change1_assassin_OnTalk_n1194_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1194_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1194_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1194_lunaria-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1194_lunaria-2-b" then 
	end
	if npc_talk_index == "n1194_lunaria-2-c" then 
	end
	if npc_talk_index == "n1194_lunaria-2-d" then 
	end
	if npc_talk_index == "n1194_lunaria-2-e" then 
	end
	if npc_talk_index == "n1194_lunaria-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1221_lunaria--------------------------------------------------------------------------------
function sq08_244_change1_assassin_OnTalk_n1221_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1221_lunaria-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1221_lunaria-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1221_lunaria-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1221_lunaria-6-b" then 
	end
	if npc_talk_index == "n1221_lunaria-6-c" then 
	end
	if npc_talk_index == "n1221_lunaria-6-d" then 
	end
	if npc_talk_index == "n1221_lunaria-6-open_ui6" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 7);
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1221_lunaria-7-open_ui7" then 
				api_ui_OpenJobChange(userObjID)
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n573_shadow_meow--------------------------------------------------------------------------------
function sq08_244_change1_assassin_OnTalk_n573_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n573_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n573_shadow_meow-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n573_shadow_meow-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n573_shadow_meow-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n573_shadow_meow-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n573_shadow_meow-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2440, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2440, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2440, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2440, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2440, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2440, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2440, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2440, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2440, false);
			 end 

	end
	if npc_talk_index == "n573_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest(userObjID,244, 1);
				api_quest_SetJournalStep(userObjID,244, 1);
				api_quest_SetQuestStep(userObjID,244, 1);
				npc_talk_index = "n573_shadow_meow-1";

	end
	if npc_talk_index == "n573_shadow_meow-1-comp_check1" then 
				if api_user_GetUserJobID(userObjID) == 62  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									if api_user_GetUserJobID(userObjID) == 67  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									if api_user_GetUserJobID(userObjID) == 63  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									if api_user_GetUserJobID(userObjID) == 64  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									if api_user_GetUserJobID(userObjID) == 68  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									if api_user_GetUserJobID(userObjID) == 69  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									npc_talk_index = "n573_shadow_meow-1-b";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n573_shadow_meow-1-c" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-d" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-e" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-f" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-menber1" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n573_shadow_meow-1-g";

				else
									npc_talk_index = "n573_shadow_meow-1-h";

				end
	end
	if npc_talk_index == "n573_shadow_meow-1-changemap1" then 
				api_user_ChangeMap(userObjID,13025,1);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n573_shadow_meow-1-j" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-k" then 
	end
	if npc_talk_index == "n573_shadow_meow-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 9);
	end
	if npc_talk_index == "n573_shadow_meow-2-b" then 
	end
	if npc_talk_index == "n573_shadow_meow-2-partycheck2" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n573_shadow_meow-2-c";

				else
									npc_talk_index = "n573_shadow_meow-2-d";

				end
	end
	if npc_talk_index == "n573_shadow_meow-2-changemap2" then 
				api_user_ChangeMap(userObjID,13025,1);
	end
	if npc_talk_index == "n573_shadow_meow-6-menber6" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n573_shadow_meow-6-b";

				else
									npc_talk_index = "n573_shadow_meow-6-c";

				end
	end
	if npc_talk_index == "n573_shadow_meow-6-changemap6" then 
				api_user_ChangeMap(userObjID,13520,3);
	end
	if npc_talk_index == "n573_shadow_meow-8-b" then 
	end
	if npc_talk_index == "n573_shadow_meow-8-c" then 
	end
	if npc_talk_index == "n573_shadow_meow-8-d" then 
				if api_user_GetUserJobID(userObjID) == 62  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2441, true);
				 api_quest_RewardQuestUser(userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2441, true);
				 api_quest_RewardQuestUser(userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2441, true);
				 api_quest_RewardQuestUser(userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2441, true);
				 api_quest_RewardQuestUser(userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2441, true);
				 api_quest_RewardQuestUser(userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2441, true);
				 api_quest_RewardQuestUser(userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2441, true);
				 api_quest_RewardQuestUser(userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2441, true);
				 api_quest_RewardQuestUser(userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2441, true);
				 api_quest_RewardQuestUser(userObjID, 2441, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2442, true);
				 api_quest_RewardQuestUser(userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2442, true);
				 api_quest_RewardQuestUser(userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2442, true);
				 api_quest_RewardQuestUser(userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2442, true);
				 api_quest_RewardQuestUser(userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2442, true);
				 api_quest_RewardQuestUser(userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2442, true);
				 api_quest_RewardQuestUser(userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2442, true);
				 api_quest_RewardQuestUser(userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2442, true);
				 api_quest_RewardQuestUser(userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2442, true);
				 api_quest_RewardQuestUser(userObjID, 2442, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n573_shadow_meow-8-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 248, 1);
					api_quest_SetQuestStep(userObjID, 248, 1);
					api_quest_SetJournalStep(userObjID, 248, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n573_shadow_meow-8-f" then 
	end
	if npc_talk_index == "n573_shadow_meow-8-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n573_shadow_meow-1", "sq08_248_change_job7.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_244_change1_assassin_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 244);
end

function sq08_244_change1_assassin_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 244);
end

function sq08_244_change1_assassin_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 244);
	local questID=244;
end

function sq08_244_change1_assassin_OnRemoteStart( userObjID, questID )
end

function sq08_244_change1_assassin_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_244_change1_assassin_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,244, 1);
				api_quest_SetJournalStep(userObjID,244, 1);
				api_quest_SetQuestStep(userObjID,244, 1);
				npc_talk_index = "n573_shadow_meow-1";
end

</VillageServer>

<GameServer>
function sq08_244_change1_assassin_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1193 then
		sq08_244_change1_assassin_OnTalk_n1193_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1194 then
		sq08_244_change1_assassin_OnTalk_n1194_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1221 then
		sq08_244_change1_assassin_OnTalk_n1221_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 573 then
		sq08_244_change1_assassin_OnTalk_n573_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1193_illusion--------------------------------------------------------------------------------
function sq08_244_change1_assassin_OnTalk_n1193_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1193_illusion-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1193_illusion-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1193_illusion-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1193_illusion-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1193_illusion-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1193_illusion-3-b" then 
	end
	if npc_talk_index == "n1193_illusion-3-c" then 
	end
	if npc_talk_index == "n1193_illusion-3-d" then 
	end
	if npc_talk_index == "n1193_illusion-3-e" then 
	end
	if npc_talk_index == "n1193_illusion-3-f" then 
	end
	if npc_talk_index == "n1193_illusion-3-g" then 
	end
	if npc_talk_index == "n1193_illusion-3-h" then 
	end
	if npc_talk_index == "n1193_illusion-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-c" then 
	end
	if npc_talk_index == "n1193_illusion-5-d" then 
	end
	if npc_talk_index == "n1193_illusion-5-e" then 
	end
	if npc_talk_index == "n1193_illusion-5-e" then 
	end
	if npc_talk_index == "n1193_illusion-5-f" then 
	end
	if npc_talk_index == "n1193_illusion-5-g" then 
	end
	if npc_talk_index == "n1193_illusion-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1194_lunaria--------------------------------------------------------------------------------
function sq08_244_change1_assassin_OnTalk_n1194_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1194_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1194_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1194_lunaria-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1194_lunaria-2-b" then 
	end
	if npc_talk_index == "n1194_lunaria-2-c" then 
	end
	if npc_talk_index == "n1194_lunaria-2-d" then 
	end
	if npc_talk_index == "n1194_lunaria-2-e" then 
	end
	if npc_talk_index == "n1194_lunaria-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1221_lunaria--------------------------------------------------------------------------------
function sq08_244_change1_assassin_OnTalk_n1221_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1221_lunaria-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1221_lunaria-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1221_lunaria-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1221_lunaria-6-b" then 
	end
	if npc_talk_index == "n1221_lunaria-6-c" then 
	end
	if npc_talk_index == "n1221_lunaria-6-d" then 
	end
	if npc_talk_index == "n1221_lunaria-6-open_ui6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1221_lunaria-7-open_ui7" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n573_shadow_meow--------------------------------------------------------------------------------
function sq08_244_change1_assassin_OnTalk_n573_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n573_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n573_shadow_meow-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n573_shadow_meow-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n573_shadow_meow-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n573_shadow_meow-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n573_shadow_meow-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2440, false);
			 end 

	end
	if npc_talk_index == "n573_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,244, 1);
				api_quest_SetJournalStep( pRoom, userObjID,244, 1);
				api_quest_SetQuestStep( pRoom, userObjID,244, 1);
				npc_talk_index = "n573_shadow_meow-1";

	end
	if npc_talk_index == "n573_shadow_meow-1-comp_check1" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 62  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 67  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 63  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 64  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 68  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 69  then
									npc_talk_index = "n573_shadow_meow-1-i";

				else
									npc_talk_index = "n573_shadow_meow-1-b";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n573_shadow_meow-1-c" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-d" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-e" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-f" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-menber1" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n573_shadow_meow-1-g";

				else
									npc_talk_index = "n573_shadow_meow-1-h";

				end
	end
	if npc_talk_index == "n573_shadow_meow-1-changemap1" then 
				api_user_ChangeMap( pRoom, userObjID,13025,1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n573_shadow_meow-1-j" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-k" then 
	end
	if npc_talk_index == "n573_shadow_meow-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 9);
	end
	if npc_talk_index == "n573_shadow_meow-2-b" then 
	end
	if npc_talk_index == "n573_shadow_meow-2-partycheck2" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n573_shadow_meow-2-c";

				else
									npc_talk_index = "n573_shadow_meow-2-d";

				end
	end
	if npc_talk_index == "n573_shadow_meow-2-changemap2" then 
				api_user_ChangeMap( pRoom, userObjID,13025,1);
	end
	if npc_talk_index == "n573_shadow_meow-6-menber6" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n573_shadow_meow-6-b";

				else
									npc_talk_index = "n573_shadow_meow-6-c";

				end
	end
	if npc_talk_index == "n573_shadow_meow-6-changemap6" then 
				api_user_ChangeMap( pRoom, userObjID,13520,3);
	end
	if npc_talk_index == "n573_shadow_meow-8-b" then 
	end
	if npc_talk_index == "n573_shadow_meow-8-c" then 
	end
	if npc_talk_index == "n573_shadow_meow-8-d" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 62  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2441, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2441, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2441, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2442, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2442, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2442, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n573_shadow_meow-8-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 248, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 248, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 248, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n573_shadow_meow-8-f" then 
	end
	if npc_talk_index == "n573_shadow_meow-8-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n573_shadow_meow-1", "sq08_248_change_job7.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_244_change1_assassin_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 244);
end

function sq08_244_change1_assassin_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 244);
end

function sq08_244_change1_assassin_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 244);
	local questID=244;
end

function sq08_244_change1_assassin_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_244_change1_assassin_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_244_change1_assassin_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,244, 1);
				api_quest_SetJournalStep( pRoom, userObjID,244, 1);
				api_quest_SetQuestStep( pRoom, userObjID,244, 1);
				npc_talk_index = "n573_shadow_meow-1";
end

</GameServer>