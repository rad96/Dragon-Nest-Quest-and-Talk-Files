<VillageServer>

function sq15_766_at_the_crossroads_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 33 then
		sq15_766_at_the_crossroads_OnTalk_n033_archer_master_adellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		sq15_766_at_the_crossroads_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 5 then
		sq15_766_at_the_crossroads_OnTalk_n005_archer_master_diana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 712 then
		sq15_766_at_the_crossroads_OnTalk_n712_archer_zenya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n033_archer_master_adellin--------------------------------------------------------------------------------
function sq15_766_at_the_crossroads_OnTalk_n033_archer_master_adellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n033_archer_master_adellin-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n033_archer_master_adellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n033_archer_master_adellin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n033_archer_master_adellin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n033_archer_master_adellin-3-check2" then 
				if api_user_GetUserJobID(userObjID) == 14  then
									npc_talk_index = "n033_archer_master_adellin-3-b";

				else
									if api_user_GetUserJobID(userObjID) == 15  then
									npc_talk_index = "n033_archer_master_adellin-3-d";

				else
									npc_talk_index = "n033_archer_master_adellin-3-d";

				end

				end
	end
	if npc_talk_index == "n033_archer_master_adellin-3-c" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-3-d" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-3-e" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-3-f" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq15_766_at_the_crossroads_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n044_archer_master_ishilien-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n044_archer_master_ishilien-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n044_archer_master_ishilien-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n044_archer_master_ishilien-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-4-check3" then 
				if api_user_GetUserJobID(userObjID) == 15  then
									npc_talk_index = "n044_archer_master_ishilien-4-d";

				else
									if api_user_GetUserJobID(userObjID) == 14  then
									npc_talk_index = "n044_archer_master_ishilien-4-b";

				else
									npc_talk_index = "n044_archer_master_ishilien-4-b";

				end

				end
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n005_archer_master_diana--------------------------------------------------------------------------------
function sq15_766_at_the_crossroads_OnTalk_n005_archer_master_diana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n005_archer_master_diana-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n005_archer_master_diana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n005_archer_master_diana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n005_archer_master_diana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n005_archer_master_diana-2-check1" then 
				if api_user_GetUserJobID(userObjID) == 15  then
									npc_talk_index = "n005_archer_master_diana-2-b";

				else
									if api_user_GetUserJobID(userObjID) == 14  then
									npc_talk_index = "n005_archer_master_diana-2-d";

				else
									npc_talk_index = "n005_archer_master_diana-2-d";

				end

				end
	end
	if npc_talk_index == "n005_archer_master_diana-2-c" then 
	end
	if npc_talk_index == "n005_archer_master_diana-2-d" then 
	end
	if npc_talk_index == "n005_archer_master_diana-2-e" then 
	end
	if npc_talk_index == "n005_archer_master_diana-2-f" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n712_archer_zenya--------------------------------------------------------------------------------
function sq15_766_at_the_crossroads_OnTalk_n712_archer_zenya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n712_archer_zenya-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n712_archer_zenya-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n712_archer_zenya-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n712_archer_zenya-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n712_archer_zenya-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n712_archer_zenya-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n712_archer_zenya-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7660, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7660, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7660, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7660, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7660, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7660, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7660, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7660, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7660, false);
			 end 

	end
	if npc_talk_index == "n712_archer_zenya-accepting-acceptted" then
				api_quest_AddQuest(userObjID,766, 1);
				api_quest_SetJournalStep(userObjID,766, 1);
				api_quest_SetQuestStep(userObjID,766, 1);
				npc_talk_index = "n712_archer_zenya-1";

	end
	if npc_talk_index == "n712_archer_zenya-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n712_archer_zenya-6-checklast" then 
				if api_user_GetUserJobID(userObjID) == 15  then
									npc_talk_index = "n712_archer_zenya-6-b";

				else
									if api_user_GetUserJobID(userObjID) == 14  then
									npc_talk_index = "n712_archer_zenya-6-d";

				else
									npc_talk_index = "n712_archer_zenya-6-d";

				end

				end
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-d" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-e" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-f" then 
	end
	if npc_talk_index == "n712_archer_zenya-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 7);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1455, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201455, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300423, 1);
	end
	if npc_talk_index == "n712_archer_zenya-8-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7660, true);
				 api_quest_RewardQuestUser(userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7660, true);
				 api_quest_RewardQuestUser(userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7660, true);
				 api_quest_RewardQuestUser(userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7660, true);
				 api_quest_RewardQuestUser(userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7660, true);
				 api_quest_RewardQuestUser(userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7660, true);
				 api_quest_RewardQuestUser(userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7660, true);
				 api_quest_RewardQuestUser(userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7660, true);
				 api_quest_RewardQuestUser(userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7660, true);
				 api_quest_RewardQuestUser(userObjID, 7660, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-8-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 767, 1);
					api_quest_SetQuestStep(userObjID, 767, 1);
					api_quest_SetJournalStep(userObjID, 767, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300421, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300421, api_quest_HasQuestItem(userObjID, 300421, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300422, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300422, api_quest_HasQuestItem(userObjID, 300422, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300423, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300423, api_quest_HasQuestItem(userObjID, 300423, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300424, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300424, api_quest_HasQuestItem(userObjID, 300424, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300425, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300425, api_quest_HasQuestItem(userObjID, 300425, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n712_archer_zenya-1", "sq15_767_whistling_of_the_breeze.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_766_at_the_crossroads_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 766);
	if qstep == 7 and CountIndex == 1455 then
				api_quest_ClearCountingInfo(userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300423, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300423, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 8);

	end
	if qstep == 7 and CountIndex == 201455 then
				api_quest_ClearCountingInfo(userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300423, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300423, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 8);

	end
	if qstep == 7 and CountIndex == 300423 then

	end
end

function sq15_766_at_the_crossroads_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 766);
	if qstep == 7 and CountIndex == 1455 and Count >= TargetCount  then

	end
	if qstep == 7 and CountIndex == 201455 and Count >= TargetCount  then

	end
	if qstep == 7 and CountIndex == 300423 and Count >= TargetCount  then

	end
end

function sq15_766_at_the_crossroads_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 766);
	local questID=766;
end

function sq15_766_at_the_crossroads_OnRemoteStart( userObjID, questID )
end

function sq15_766_at_the_crossroads_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_766_at_the_crossroads_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,766, 1);
				api_quest_SetJournalStep(userObjID,766, 1);
				api_quest_SetQuestStep(userObjID,766, 1);
				npc_talk_index = "n712_archer_zenya-1";
end

</VillageServer>

<GameServer>
function sq15_766_at_the_crossroads_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 33 then
		sq15_766_at_the_crossroads_OnTalk_n033_archer_master_adellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		sq15_766_at_the_crossroads_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 5 then
		sq15_766_at_the_crossroads_OnTalk_n005_archer_master_diana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 712 then
		sq15_766_at_the_crossroads_OnTalk_n712_archer_zenya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n033_archer_master_adellin--------------------------------------------------------------------------------
function sq15_766_at_the_crossroads_OnTalk_n033_archer_master_adellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n033_archer_master_adellin-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n033_archer_master_adellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n033_archer_master_adellin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n033_archer_master_adellin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n033_archer_master_adellin-3-check2" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 14  then
									npc_talk_index = "n033_archer_master_adellin-3-b";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 15  then
									npc_talk_index = "n033_archer_master_adellin-3-d";

				else
									npc_talk_index = "n033_archer_master_adellin-3-d";

				end

				end
	end
	if npc_talk_index == "n033_archer_master_adellin-3-c" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-3-d" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-3-e" then 
	end
	if npc_talk_index == "n033_archer_master_adellin-3-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq15_766_at_the_crossroads_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n044_archer_master_ishilien-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n044_archer_master_ishilien-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n044_archer_master_ishilien-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n044_archer_master_ishilien-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-4-check3" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 15  then
									npc_talk_index = "n044_archer_master_ishilien-4-d";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 14  then
									npc_talk_index = "n044_archer_master_ishilien-4-b";

				else
									npc_talk_index = "n044_archer_master_ishilien-4-b";

				end

				end
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n005_archer_master_diana--------------------------------------------------------------------------------
function sq15_766_at_the_crossroads_OnTalk_n005_archer_master_diana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n005_archer_master_diana-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n005_archer_master_diana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n005_archer_master_diana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n005_archer_master_diana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n005_archer_master_diana-2-check1" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 15  then
									npc_talk_index = "n005_archer_master_diana-2-b";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 14  then
									npc_talk_index = "n005_archer_master_diana-2-d";

				else
									npc_talk_index = "n005_archer_master_diana-2-d";

				end

				end
	end
	if npc_talk_index == "n005_archer_master_diana-2-c" then 
	end
	if npc_talk_index == "n005_archer_master_diana-2-d" then 
	end
	if npc_talk_index == "n005_archer_master_diana-2-e" then 
	end
	if npc_talk_index == "n005_archer_master_diana-2-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n712_archer_zenya--------------------------------------------------------------------------------
function sq15_766_at_the_crossroads_OnTalk_n712_archer_zenya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n712_archer_zenya-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n712_archer_zenya-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n712_archer_zenya-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n712_archer_zenya-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n712_archer_zenya-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n712_archer_zenya-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n712_archer_zenya-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, false);
			 end 

	end
	if npc_talk_index == "n712_archer_zenya-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,766, 1);
				api_quest_SetJournalStep( pRoom, userObjID,766, 1);
				api_quest_SetQuestStep( pRoom, userObjID,766, 1);
				npc_talk_index = "n712_archer_zenya-1";

	end
	if npc_talk_index == "n712_archer_zenya-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n712_archer_zenya-6-checklast" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 15  then
									npc_talk_index = "n712_archer_zenya-6-b";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 14  then
									npc_talk_index = "n712_archer_zenya-6-d";

				else
									npc_talk_index = "n712_archer_zenya-6-d";

				end

				end
	end
	if npc_talk_index == "n712_archer_zenya-6-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-d" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-e" then 
	end
	if npc_talk_index == "n712_archer_zenya-6-f" then 
	end
	if npc_talk_index == "n712_archer_zenya-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1455, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201455, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300423, 1);
	end
	if npc_talk_index == "n712_archer_zenya-8-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7660, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7660, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7660, questID, 1);
			 end 
	end
	if npc_talk_index == "n712_archer_zenya-8-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 767, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 767, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 767, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300421, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300421, api_quest_HasQuestItem( pRoom, userObjID, 300421, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300422, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300422, api_quest_HasQuestItem( pRoom, userObjID, 300422, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300423, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300423, api_quest_HasQuestItem( pRoom, userObjID, 300423, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300424, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300424, api_quest_HasQuestItem( pRoom, userObjID, 300424, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300425, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300425, api_quest_HasQuestItem( pRoom, userObjID, 300425, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n712_archer_zenya-1", "sq15_767_whistling_of_the_breeze.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_766_at_the_crossroads_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 766);
	if qstep == 7 and CountIndex == 1455 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300423, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300423, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);

	end
	if qstep == 7 and CountIndex == 201455 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300423, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300423, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);

	end
	if qstep == 7 and CountIndex == 300423 then

	end
end

function sq15_766_at_the_crossroads_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 766);
	if qstep == 7 and CountIndex == 1455 and Count >= TargetCount  then

	end
	if qstep == 7 and CountIndex == 201455 and Count >= TargetCount  then

	end
	if qstep == 7 and CountIndex == 300423 and Count >= TargetCount  then

	end
end

function sq15_766_at_the_crossroads_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 766);
	local questID=766;
end

function sq15_766_at_the_crossroads_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_766_at_the_crossroads_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_766_at_the_crossroads_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,766, 1);
				api_quest_SetJournalStep( pRoom, userObjID,766, 1);
				api_quest_SetQuestStep( pRoom, userObjID,766, 1);
				npc_talk_index = "n712_archer_zenya-1";
end

</GameServer>