<VillageServer>

function mq15_9385_promise_of_start_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1023 then
		mq15_9385_promise_of_start_OnTalk_n1023_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 574 then
		mq15_9385_promise_of_start_OnTalk_n574_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1023_academic--------------------------------------------------------------------------------
function mq15_9385_promise_of_start_OnTalk_n1023_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1023_academic-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1023_academic-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1023_academic-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1023_academic-2-b" then 
	end
	if npc_talk_index == "n1023_academic-2-c" then 
	end
	if npc_talk_index == "n1023_academic-2-d" then 
	end
	if npc_talk_index == "n1023_academic-2-e" then 
	end
	if npc_talk_index == "n1023_academic-2-f" then 
	end
	if npc_talk_index == "n1023_academic-2-f" then 
	end
	if npc_talk_index == "n1023_academic-2-g" then 
	end
	if npc_talk_index == "n1023_academic-2-h" then 
	end
	if npc_talk_index == "n1023_academic-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n574_shadow_meow--------------------------------------------------------------------------------
function mq15_9385_promise_of_start_OnTalk_n574_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n574_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n574_shadow_meow-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n574_shadow_meow-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n574_shadow_meow-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n574_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9385, 2);
				api_quest_SetJournalStep(userObjID,9385, 1);
				api_quest_SetQuestStep(userObjID,9385, 1);
				npc_talk_index = "n574_shadow_meow-1";

	end
	if npc_talk_index == "n574_shadow_meow-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n574_shadow_meow-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n574_shadow_meow-3-b" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-c" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-d" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-e" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-f" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93850, true);
				 api_quest_RewardQuestUser(userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93850, true);
				 api_quest_RewardQuestUser(userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93850, true);
				 api_quest_RewardQuestUser(userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93850, true);
				 api_quest_RewardQuestUser(userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93850, true);
				 api_quest_RewardQuestUser(userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93850, true);
				 api_quest_RewardQuestUser(userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93850, true);
				 api_quest_RewardQuestUser(userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93850, true);
				 api_quest_RewardQuestUser(userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93850, true);
				 api_quest_RewardQuestUser(userObjID, 93850, questID, 1);
			 end 
	end
	if npc_talk_index == "n574_shadow_meow-3-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n574_shadow_meow-3-i" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-j" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-next" then 
				api_npc_NextScript(userObjID, npcObjID, "quest", "n574_shadow_meow.xml");
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9385_promise_of_start_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9385);
end

function mq15_9385_promise_of_start_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9385);
end

function mq15_9385_promise_of_start_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9385);
	local questID=9385;
end

function mq15_9385_promise_of_start_OnRemoteStart( userObjID, questID )
end

function mq15_9385_promise_of_start_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9385_promise_of_start_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9385, 2);
				api_quest_SetJournalStep(userObjID,9385, 1);
				api_quest_SetQuestStep(userObjID,9385, 1);
				npc_talk_index = "n574_shadow_meow-1";
end

</VillageServer>

<GameServer>
function mq15_9385_promise_of_start_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1023 then
		mq15_9385_promise_of_start_OnTalk_n1023_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 574 then
		mq15_9385_promise_of_start_OnTalk_n574_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1023_academic--------------------------------------------------------------------------------
function mq15_9385_promise_of_start_OnTalk_n1023_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1023_academic-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1023_academic-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1023_academic-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1023_academic-2-b" then 
	end
	if npc_talk_index == "n1023_academic-2-c" then 
	end
	if npc_talk_index == "n1023_academic-2-d" then 
	end
	if npc_talk_index == "n1023_academic-2-e" then 
	end
	if npc_talk_index == "n1023_academic-2-f" then 
	end
	if npc_talk_index == "n1023_academic-2-f" then 
	end
	if npc_talk_index == "n1023_academic-2-g" then 
	end
	if npc_talk_index == "n1023_academic-2-h" then 
	end
	if npc_talk_index == "n1023_academic-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n574_shadow_meow--------------------------------------------------------------------------------
function mq15_9385_promise_of_start_OnTalk_n574_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n574_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n574_shadow_meow-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n574_shadow_meow-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n574_shadow_meow-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n574_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9385, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9385, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9385, 1);
				npc_talk_index = "n574_shadow_meow-1";

	end
	if npc_talk_index == "n574_shadow_meow-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n574_shadow_meow-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n574_shadow_meow-3-b" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-c" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-d" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-e" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-f" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93850, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93850, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93850, questID, 1);
			 end 
	end
	if npc_talk_index == "n574_shadow_meow-3-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n574_shadow_meow-3-i" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-j" then 
	end
	if npc_talk_index == "n574_shadow_meow-3-next" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "quest", "n574_shadow_meow.xml");
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9385_promise_of_start_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9385);
end

function mq15_9385_promise_of_start_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9385);
end

function mq15_9385_promise_of_start_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9385);
	local questID=9385;
end

function mq15_9385_promise_of_start_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9385_promise_of_start_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9385_promise_of_start_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9385, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9385, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9385, 1);
				npc_talk_index = "n574_shadow_meow-1";
end

</GameServer>