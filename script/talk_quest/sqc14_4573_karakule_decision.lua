<VillageServer>

function sqc14_4573_karakule_decision_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1610 then
		sqc14_4573_karakule_decision_OnTalk_n1610_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1623 then
		sqc14_4573_karakule_decision_OnTalk_n1623_sorceress_yutz(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1610_royal_magician_kalaen--------------------------------------------------------------------------------
function sqc14_4573_karakule_decision_OnTalk_n1610_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1610_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1610_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1610_royal_magician_kalaen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1610_royal_magician_kalaen-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45730, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45730, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45730, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45730, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45730, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45730, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45730, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45730, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45730, false);
			 end 

	end
	if npc_talk_index == "n1610_royal_magician_kalaen-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4573, 1);
				api_quest_SetJournalStep(userObjID,4573, 1);
				api_quest_SetQuestStep(userObjID,4573, 1);
				npc_talk_index = "n1610_royal_magician_kalaen-1";

	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-b" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-c" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-d" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-e" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400438, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400438, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1623_sorceress_yutz--------------------------------------------------------------------------------
function sqc14_4573_karakule_decision_OnTalk_n1623_sorceress_yutz(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1623_sorceress_yutz-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1623_sorceress_yutz-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1623_sorceress_yutz-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1623_sorceress_yutz-2-a" then 

				if api_user_HasItem(userObjID, 400438, 1) > 0 then 
					api_user_DelItem(userObjID, 400438, api_user_HasItem(userObjID, 400438, 1, questID));
				end
	end
	if npc_talk_index == "n1623_sorceress_yutz-2-b" then 
	end
	if npc_talk_index == "n1623_sorceress_yutz-2-c" then 
	end
	if npc_talk_index == "n1623_sorceress_yutz-2-d" then 
	end
	if npc_talk_index == "n1623_sorceress_yutz-2-e" then 
	end
	if npc_talk_index == "n1623_sorceress_yutz-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 602096, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 702096, 30000);
	end
	if npc_talk_index == "n1623_sorceress_yutz-4-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45730, true);
				 api_quest_RewardQuestUser(userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45730, true);
				 api_quest_RewardQuestUser(userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45730, true);
				 api_quest_RewardQuestUser(userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45730, true);
				 api_quest_RewardQuestUser(userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45730, true);
				 api_quest_RewardQuestUser(userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45730, true);
				 api_quest_RewardQuestUser(userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45730, true);
				 api_quest_RewardQuestUser(userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45730, true);
				 api_quest_RewardQuestUser(userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45730, true);
				 api_quest_RewardQuestUser(userObjID, 45730, questID, 1);
			 end 
	end
	if npc_talk_index == "n1623_sorceress_yutz-4-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4574, 1);
					api_quest_SetQuestStep(userObjID, 4574, 1);
					api_quest_SetJournalStep(userObjID, 4574, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1623_sorceress_yutz-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1623_sorceress_yutz-1", "sqc14_4574_conflict1.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4573_karakule_decision_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4573);
	if qstep == 3 and CountIndex == 602096 then
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 3 and CountIndex == 702096 then
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function sqc14_4573_karakule_decision_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4573);
	if qstep == 3 and CountIndex == 602096 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 702096 and Count >= TargetCount  then

	end
end

function sqc14_4573_karakule_decision_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4573);
	local questID=4573;
end

function sqc14_4573_karakule_decision_OnRemoteStart( userObjID, questID )
end

function sqc14_4573_karakule_decision_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc14_4573_karakule_decision_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4573, 1);
				api_quest_SetJournalStep(userObjID,4573, 1);
				api_quest_SetQuestStep(userObjID,4573, 1);
				npc_talk_index = "n1610_royal_magician_kalaen-1";
end

</VillageServer>

<GameServer>
function sqc14_4573_karakule_decision_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1610 then
		sqc14_4573_karakule_decision_OnTalk_n1610_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1623 then
		sqc14_4573_karakule_decision_OnTalk_n1623_sorceress_yutz( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1610_royal_magician_kalaen--------------------------------------------------------------------------------
function sqc14_4573_karakule_decision_OnTalk_n1610_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1610_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1610_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1610_royal_magician_kalaen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1610_royal_magician_kalaen-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, false);
			 end 

	end
	if npc_talk_index == "n1610_royal_magician_kalaen-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4573, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4573, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4573, 1);
				npc_talk_index = "n1610_royal_magician_kalaen-1";

	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-b" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-c" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-d" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-e" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400438, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400438, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1623_sorceress_yutz--------------------------------------------------------------------------------
function sqc14_4573_karakule_decision_OnTalk_n1623_sorceress_yutz( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1623_sorceress_yutz-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1623_sorceress_yutz-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1623_sorceress_yutz-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1623_sorceress_yutz-2-a" then 

				if api_user_HasItem( pRoom, userObjID, 400438, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 400438, api_user_HasItem( pRoom, userObjID, 400438, 1, questID));
				end
	end
	if npc_talk_index == "n1623_sorceress_yutz-2-b" then 
	end
	if npc_talk_index == "n1623_sorceress_yutz-2-c" then 
	end
	if npc_talk_index == "n1623_sorceress_yutz-2-d" then 
	end
	if npc_talk_index == "n1623_sorceress_yutz-2-e" then 
	end
	if npc_talk_index == "n1623_sorceress_yutz-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 602096, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 702096, 30000);
	end
	if npc_talk_index == "n1623_sorceress_yutz-4-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45730, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45730, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45730, questID, 1);
			 end 
	end
	if npc_talk_index == "n1623_sorceress_yutz-4-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4574, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4574, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4574, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1623_sorceress_yutz-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1623_sorceress_yutz-1", "sqc14_4574_conflict1.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4573_karakule_decision_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4573);
	if qstep == 3 and CountIndex == 602096 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 3 and CountIndex == 702096 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function sqc14_4573_karakule_decision_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4573);
	if qstep == 3 and CountIndex == 602096 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 702096 and Count >= TargetCount  then

	end
end

function sqc14_4573_karakule_decision_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4573);
	local questID=4573;
end

function sqc14_4573_karakule_decision_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc14_4573_karakule_decision_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc14_4573_karakule_decision_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4573, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4573, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4573, 1);
				npc_talk_index = "n1610_royal_magician_kalaen-1";
end

</GameServer>