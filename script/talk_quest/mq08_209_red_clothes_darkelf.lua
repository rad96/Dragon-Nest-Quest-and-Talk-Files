<VillageServer>

function mq08_209_red_clothes_darkelf_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 164 then
		mq08_209_red_clothes_darkelf_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 167 then
		mq08_209_red_clothes_darkelf_OnTalk_n167_charging_point(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function mq08_209_red_clothes_darkelf_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-acceptted" then
				api_quest_AddQuest(userObjID,209, 2);
				api_quest_SetJournalStep(userObjID,209, 1);
				api_quest_SetQuestStep(userObjID,209, 1);
				npc_talk_index = "n164_argenta_wounded-1";

	end
	if npc_talk_index == "n164_argenta_wounded-1-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2091, true);
				 api_quest_RewardQuestUser(userObjID, 2091, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2092, true);
				 api_quest_RewardQuestUser(userObjID, 2092, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2093, true);
				 api_quest_RewardQuestUser(userObjID, 2093, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2094, true);
				 api_quest_RewardQuestUser(userObjID, 2094, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2095, true);
				 api_quest_RewardQuestUser(userObjID, 2095, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2096, true);
				 api_quest_RewardQuestUser(userObjID, 2096, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2091, true);
				 api_quest_RewardQuestUser(userObjID, 2091, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2091, true);
				 api_quest_RewardQuestUser(userObjID, 2091, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-1-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 210, 2);
					api_quest_SetQuestStep(userObjID, 210, 1);
					api_quest_SetJournalStep(userObjID, 210, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n164_argenta_wounded-1", "mq08_210_destruction_of_manufactory.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n167_charging_point--------------------------------------------------------------------------------
function mq08_209_red_clothes_darkelf_OnTalk_n167_charging_point(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n167_charging_point-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n167_charging_point-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_209_red_clothes_darkelf_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 209);
end

function mq08_209_red_clothes_darkelf_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 209);
end

function mq08_209_red_clothes_darkelf_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 209);
	local questID=209;
end

function mq08_209_red_clothes_darkelf_OnRemoteStart( userObjID, questID )
end

function mq08_209_red_clothes_darkelf_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_209_red_clothes_darkelf_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,209, 2);
				api_quest_SetJournalStep(userObjID,209, 1);
				api_quest_SetQuestStep(userObjID,209, 1);
				npc_talk_index = "n164_argenta_wounded-1";
end

</VillageServer>

<GameServer>
function mq08_209_red_clothes_darkelf_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 164 then
		mq08_209_red_clothes_darkelf_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 167 then
		mq08_209_red_clothes_darkelf_OnTalk_n167_charging_point( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function mq08_209_red_clothes_darkelf_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,209, 2);
				api_quest_SetJournalStep( pRoom, userObjID,209, 1);
				api_quest_SetQuestStep( pRoom, userObjID,209, 1);
				npc_talk_index = "n164_argenta_wounded-1";

	end
	if npc_talk_index == "n164_argenta_wounded-1-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2091, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2091, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2092, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2092, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2093, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2093, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2094, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2094, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2095, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2095, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2096, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2096, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2091, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2091, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2091, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2091, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-1-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 210, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 210, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 210, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n164_argenta_wounded-1", "mq08_210_destruction_of_manufactory.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n167_charging_point--------------------------------------------------------------------------------
function mq08_209_red_clothes_darkelf_OnTalk_n167_charging_point( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n167_charging_point-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n167_charging_point-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_209_red_clothes_darkelf_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 209);
end

function mq08_209_red_clothes_darkelf_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 209);
end

function mq08_209_red_clothes_darkelf_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 209);
	local questID=209;
end

function mq08_209_red_clothes_darkelf_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq08_209_red_clothes_darkelf_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_209_red_clothes_darkelf_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,209, 2);
				api_quest_SetJournalStep( pRoom, userObjID,209, 1);
				api_quest_SetQuestStep( pRoom, userObjID,209, 1);
				npc_talk_index = "n164_argenta_wounded-1";
end

</GameServer>