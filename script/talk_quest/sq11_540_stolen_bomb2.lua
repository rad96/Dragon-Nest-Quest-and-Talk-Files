<VillageServer>

function sq11_540_stolen_bomb2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 42 then
		sq11_540_stolen_bomb2_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function sq11_540_stolen_bomb2_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5401, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5402, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5403, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5404, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5405, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5406, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5407, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5408, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5409, false);
			 end 

	end
	if npc_talk_index == "n042_general_duglars-accepting-b" then
				api_quest_AddQuest(userObjID,540, 1);
				api_quest_SetJournalStep(userObjID,540, 1);
				api_quest_SetQuestStep(userObjID,540, 1);

	end
	if npc_talk_index == "n042_general_duglars-accepting-accpetted" then
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 884, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 883, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200884, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200883, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 400149, 10);
	end
	if npc_talk_index == "n042_general_duglars-3-b" then 
	end
	if npc_talk_index == "n042_general_duglars-3-c" then 
	end
	if npc_talk_index == "n042_general_duglars-3-d" then 
	end
	if npc_talk_index == "n042_general_duglars-3-e" then 
	end
	if npc_talk_index == "n042_general_duglars-3-f" then 
	end
	if npc_talk_index == "n042_general_duglars-3-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5401, true);
				 api_quest_RewardQuestUser(userObjID, 5401, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5402, true);
				 api_quest_RewardQuestUser(userObjID, 5402, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5403, true);
				 api_quest_RewardQuestUser(userObjID, 5403, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5404, true);
				 api_quest_RewardQuestUser(userObjID, 5404, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5405, true);
				 api_quest_RewardQuestUser(userObjID, 5405, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5406, true);
				 api_quest_RewardQuestUser(userObjID, 5406, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5407, true);
				 api_quest_RewardQuestUser(userObjID, 5407, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5408, true);
				 api_quest_RewardQuestUser(userObjID, 5408, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5409, true);
				 api_quest_RewardQuestUser(userObjID, 5409, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-3-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_540_stolen_bomb2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 540);
	if qstep == 2 and CountIndex == 884 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400149, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400149, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 883 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400149, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400149, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400149 then

	end
	if qstep == 2 and CountIndex == 200884 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400149, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400149, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200883 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400149, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400149, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_540_stolen_bomb2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 540);
	if qstep == 2 and CountIndex == 884 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 883 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400149 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200884 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200883 and Count >= TargetCount  then

	end
end

function sq11_540_stolen_bomb2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 540);
	local questID=540;
end

function sq11_540_stolen_bomb2_OnRemoteStart( userObjID, questID )
end

function sq11_540_stolen_bomb2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_540_stolen_bomb2_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_540_stolen_bomb2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 42 then
		sq11_540_stolen_bomb2_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function sq11_540_stolen_bomb2_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5401, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5402, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5403, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5404, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5405, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5406, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5407, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5408, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5409, false);
			 end 

	end
	if npc_talk_index == "n042_general_duglars-accepting-b" then
				api_quest_AddQuest( pRoom, userObjID,540, 1);
				api_quest_SetJournalStep( pRoom, userObjID,540, 1);
				api_quest_SetQuestStep( pRoom, userObjID,540, 1);

	end
	if npc_talk_index == "n042_general_duglars-accepting-accpetted" then
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 884, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 883, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200884, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200883, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 400149, 10);
	end
	if npc_talk_index == "n042_general_duglars-3-b" then 
	end
	if npc_talk_index == "n042_general_duglars-3-c" then 
	end
	if npc_talk_index == "n042_general_duglars-3-d" then 
	end
	if npc_talk_index == "n042_general_duglars-3-e" then 
	end
	if npc_talk_index == "n042_general_duglars-3-f" then 
	end
	if npc_talk_index == "n042_general_duglars-3-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5401, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5401, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5402, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5402, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5403, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5403, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5404, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5404, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5405, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5405, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5406, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5406, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5407, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5407, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5408, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5408, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5409, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5409, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-3-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_540_stolen_bomb2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 540);
	if qstep == 2 and CountIndex == 884 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400149, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400149, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 883 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400149, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400149, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400149 then

	end
	if qstep == 2 and CountIndex == 200884 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400149, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400149, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200883 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400149, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400149, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_540_stolen_bomb2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 540);
	if qstep == 2 and CountIndex == 884 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 883 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400149 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200884 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200883 and Count >= TargetCount  then

	end
end

function sq11_540_stolen_bomb2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 540);
	local questID=540;
end

function sq11_540_stolen_bomb2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_540_stolen_bomb2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_540_stolen_bomb2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>