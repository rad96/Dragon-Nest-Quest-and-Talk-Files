<VillageServer>

function sq34_4433_dragons_memory_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1233 then
		sq34_4433_dragons_memory_OnTalk_n1233_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1249 then
		sq34_4433_dragons_memory_OnTalk_n1249_desert_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1103 then
		sq34_4433_dragons_memory_OnTalk_n1103_trader_phara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1250 then
		sq34_4433_dragons_memory_OnTalk_n1250_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1233_white_dragon--------------------------------------------------------------------------------
function sq34_4433_dragons_memory_OnTalk_n1233_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1233_white_dragon-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1249_desert_dragon--------------------------------------------------------------------------------
function sq34_4433_dragons_memory_OnTalk_n1249_desert_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1249_desert_dragon-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1103_trader_phara--------------------------------------------------------------------------------
function sq34_4433_dragons_memory_OnTalk_n1103_trader_phara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1103_trader_phara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1103_trader_phara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1103_trader_phara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1103_trader_phara-accepting-c" then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400413, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400413, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n1103_trader_phara-accepting-g" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 44330, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 44330, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 44330, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 44330, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 44330, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 44330, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 44330, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 44330, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 44330, false);
			 end 

	end
	if npc_talk_index == "n1103_trader_phara-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4433, 1);
				api_quest_SetJournalStep(userObjID,4433, 1);
				api_quest_SetQuestStep(userObjID,4433, 1);
				npc_talk_index = "n1103_trader_phara-1";

	end
	if npc_talk_index == "n1103_trader_phara-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 44330, true);
				 api_quest_RewardQuestUser(userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 44330, true);
				 api_quest_RewardQuestUser(userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 44330, true);
				 api_quest_RewardQuestUser(userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 44330, true);
				 api_quest_RewardQuestUser(userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 44330, true);
				 api_quest_RewardQuestUser(userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 44330, true);
				 api_quest_RewardQuestUser(userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 44330, true);
				 api_quest_RewardQuestUser(userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 44330, true);
				 api_quest_RewardQuestUser(userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 44330, true);
				 api_quest_RewardQuestUser(userObjID, 44330, questID, 1);
			 end 
	end
	if npc_talk_index == "n1103_trader_phara-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem(userObjID, 400413, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400413, api_quest_HasQuestItem(userObjID, 400413, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1103_trader_phara-2-c" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1250_for_memory--------------------------------------------------------------------------------
function sq34_4433_dragons_memory_OnTalk_n1250_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1250_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1250_for_memory-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1250_for_memory-1-b" then 
	end
	if npc_talk_index == "n1250_for_memory-1-c" then 
	end
	if npc_talk_index == "n1250_for_memory-1-d" then 
	end
	if npc_talk_index == "n1250_for_memory-1-e" then 
	end
	if npc_talk_index == "n1250_for_memory-1-f" then 
	end
	if npc_talk_index == "n1250_for_memory-1-g" then 
	end
	if npc_talk_index == "n1250_for_memory-1-h" then 
	end
	if npc_talk_index == "n1250_for_memory-1-i" then 
	end
	if npc_talk_index == "n1250_for_memory-1-j" then 
	end
	if npc_talk_index == "n1250_for_memory-1-k" then 
	end
	if npc_talk_index == "n1250_for_memory-1-l" then 
	end
	if npc_talk_index == "n1250_for_memory-1-m" then 
	end
	if npc_talk_index == "n1250_for_memory-1-n" then 
	end
	if npc_talk_index == "n1250_for_memory-1-o" then 
	end
	if npc_talk_index == "n1250_for_memory-1-p" then 
	end
	if npc_talk_index == "n1250_for_memory-1-q" then 
	end
	if npc_talk_index == "n1250_for_memory-1-r" then 
	end
	if npc_talk_index == "n1250_for_memory-1-s" then 
	end
	if npc_talk_index == "n1250_for_memory-1-t" then 
	end
	if npc_talk_index == "n1250_for_memory-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4433_dragons_memory_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4433);
end

function sq34_4433_dragons_memory_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4433);
end

function sq34_4433_dragons_memory_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4433);
	local questID=4433;
end

function sq34_4433_dragons_memory_OnRemoteStart( userObjID, questID )
end

function sq34_4433_dragons_memory_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq34_4433_dragons_memory_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4433, 1);
				api_quest_SetJournalStep(userObjID,4433, 1);
				api_quest_SetQuestStep(userObjID,4433, 1);
				npc_talk_index = "n1103_trader_phara-1";
end

</VillageServer>

<GameServer>
function sq34_4433_dragons_memory_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1233 then
		sq34_4433_dragons_memory_OnTalk_n1233_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1249 then
		sq34_4433_dragons_memory_OnTalk_n1249_desert_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1103 then
		sq34_4433_dragons_memory_OnTalk_n1103_trader_phara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1250 then
		sq34_4433_dragons_memory_OnTalk_n1250_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1233_white_dragon--------------------------------------------------------------------------------
function sq34_4433_dragons_memory_OnTalk_n1233_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1233_white_dragon-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1249_desert_dragon--------------------------------------------------------------------------------
function sq34_4433_dragons_memory_OnTalk_n1249_desert_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1249_desert_dragon-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1103_trader_phara--------------------------------------------------------------------------------
function sq34_4433_dragons_memory_OnTalk_n1103_trader_phara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1103_trader_phara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1103_trader_phara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1103_trader_phara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1103_trader_phara-accepting-c" then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400413, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400413, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n1103_trader_phara-accepting-g" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, false);
			 end 

	end
	if npc_talk_index == "n1103_trader_phara-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4433, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4433, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4433, 1);
				npc_talk_index = "n1103_trader_phara-1";

	end
	if npc_talk_index == "n1103_trader_phara-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44330, questID, 1);
			 end 
	end
	if npc_talk_index == "n1103_trader_phara-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem( pRoom, userObjID, 400413, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400413, api_quest_HasQuestItem( pRoom, userObjID, 400413, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1103_trader_phara-2-c" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1250_for_memory--------------------------------------------------------------------------------
function sq34_4433_dragons_memory_OnTalk_n1250_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1250_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1250_for_memory-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1250_for_memory-1-b" then 
	end
	if npc_talk_index == "n1250_for_memory-1-c" then 
	end
	if npc_talk_index == "n1250_for_memory-1-d" then 
	end
	if npc_talk_index == "n1250_for_memory-1-e" then 
	end
	if npc_talk_index == "n1250_for_memory-1-f" then 
	end
	if npc_talk_index == "n1250_for_memory-1-g" then 
	end
	if npc_talk_index == "n1250_for_memory-1-h" then 
	end
	if npc_talk_index == "n1250_for_memory-1-i" then 
	end
	if npc_talk_index == "n1250_for_memory-1-j" then 
	end
	if npc_talk_index == "n1250_for_memory-1-k" then 
	end
	if npc_talk_index == "n1250_for_memory-1-l" then 
	end
	if npc_talk_index == "n1250_for_memory-1-m" then 
	end
	if npc_talk_index == "n1250_for_memory-1-n" then 
	end
	if npc_talk_index == "n1250_for_memory-1-o" then 
	end
	if npc_talk_index == "n1250_for_memory-1-p" then 
	end
	if npc_talk_index == "n1250_for_memory-1-q" then 
	end
	if npc_talk_index == "n1250_for_memory-1-r" then 
	end
	if npc_talk_index == "n1250_for_memory-1-s" then 
	end
	if npc_talk_index == "n1250_for_memory-1-t" then 
	end
	if npc_talk_index == "n1250_for_memory-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4433_dragons_memory_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4433);
end

function sq34_4433_dragons_memory_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4433);
end

function sq34_4433_dragons_memory_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4433);
	local questID=4433;
end

function sq34_4433_dragons_memory_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq34_4433_dragons_memory_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq34_4433_dragons_memory_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4433, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4433, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4433, 1);
				npc_talk_index = "n1103_trader_phara-1";
end

</GameServer>