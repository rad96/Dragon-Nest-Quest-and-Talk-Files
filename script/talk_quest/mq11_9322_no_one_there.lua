<VillageServer>

function mq11_9322_no_one_there_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1157 then
		mq11_9322_no_one_there_OnTalk_n1157_sorceress_karakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1158 then
		mq11_9322_no_one_there_OnTalk_n1158_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		mq11_9322_no_one_there_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1157_sorceress_karakule--------------------------------------------------------------------------------
function mq11_9322_no_one_there_OnTalk_n1157_sorceress_karakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1157_sorceress_karakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1157_sorceress_karakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1157_sorceress_karakule-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1157_sorceress_karakule-2-b" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-c" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-d" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-e" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-f" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-g" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-h" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-i" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-j" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-k" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-l" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-m" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-n" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-o" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-p" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-q" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-q" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-r" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-s" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-t" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-u" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-v" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-w" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-x" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-y" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1158_lunaria--------------------------------------------------------------------------------
function mq11_9322_no_one_there_OnTalk_n1158_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1158_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1158_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1158_lunaria-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1158_lunaria-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93220, true);
				 api_quest_RewardQuestUser(userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93220, true);
				 api_quest_RewardQuestUser(userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93220, true);
				 api_quest_RewardQuestUser(userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93220, true);
				 api_quest_RewardQuestUser(userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93220, true);
				 api_quest_RewardQuestUser(userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93220, true);
				 api_quest_RewardQuestUser(userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93220, true);
				 api_quest_RewardQuestUser(userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93220, true);
				 api_quest_RewardQuestUser(userObjID, 93220, questID, 1);
			 end 
	end
	if npc_talk_index == "n1158_lunaria-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9323, 2);
					api_quest_SetQuestStep(userObjID, 9323, 1);
					api_quest_SetJournalStep(userObjID, 9323, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1158_lunaria-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1158_lunaria-1", "mq11_9323_multi_layer_time.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function mq11_9322_no_one_there_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n776_darklair_mardlen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9322, 2);
				api_quest_SetJournalStep(userObjID,9322, 1);
				api_quest_SetQuestStep(userObjID,9322, 1);
				npc_talk_index = "n776_darklair_mardlen-1";

	end
	if npc_talk_index == "n776_darklair_mardlen-1-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9322_no_one_there_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9322);
end

function mq11_9322_no_one_there_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9322);
end

function mq11_9322_no_one_there_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9322);
	local questID=9322;
end

function mq11_9322_no_one_there_OnRemoteStart( userObjID, questID )
end

function mq11_9322_no_one_there_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_9322_no_one_there_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9322, 2);
				api_quest_SetJournalStep(userObjID,9322, 1);
				api_quest_SetQuestStep(userObjID,9322, 1);
				npc_talk_index = "n776_darklair_mardlen-1";
end

</VillageServer>

<GameServer>
function mq11_9322_no_one_there_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1157 then
		mq11_9322_no_one_there_OnTalk_n1157_sorceress_karakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1158 then
		mq11_9322_no_one_there_OnTalk_n1158_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		mq11_9322_no_one_there_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1157_sorceress_karakule--------------------------------------------------------------------------------
function mq11_9322_no_one_there_OnTalk_n1157_sorceress_karakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1157_sorceress_karakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1157_sorceress_karakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1157_sorceress_karakule-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1157_sorceress_karakule-2-b" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-c" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-d" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-e" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-f" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-g" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-h" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-i" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-j" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-k" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-l" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-m" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-n" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-o" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-p" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-q" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-q" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-r" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-s" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-t" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-u" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-v" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-w" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-x" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-2-y" then 
	end
	if npc_talk_index == "n1157_sorceress_karakule-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1158_lunaria--------------------------------------------------------------------------------
function mq11_9322_no_one_there_OnTalk_n1158_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1158_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1158_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1158_lunaria-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1158_lunaria-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93220, questID, 1);
			 end 
	end
	if npc_talk_index == "n1158_lunaria-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9323, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9323, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9323, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1158_lunaria-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1158_lunaria-1", "mq11_9323_multi_layer_time.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function mq11_9322_no_one_there_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n776_darklair_mardlen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9322, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9322, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9322, 1);
				npc_talk_index = "n776_darklair_mardlen-1";

	end
	if npc_talk_index == "n776_darklair_mardlen-1-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-1-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9322_no_one_there_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9322);
end

function mq11_9322_no_one_there_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9322);
end

function mq11_9322_no_one_there_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9322);
	local questID=9322;
end

function mq11_9322_no_one_there_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_9322_no_one_there_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_9322_no_one_there_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9322, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9322, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9322, 1);
				npc_talk_index = "n776_darklair_mardlen-1";
end

</GameServer>