<VillageServer>

function sq08_288_iron_scale1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 25 then
		sq08_288_iron_scale1_OnTalk_n025_trader_corin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n025_trader_corin--------------------------------------------------------------------------------
function sq08_288_iron_scale1_OnTalk_n025_trader_corin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n025_trader_corin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n025_trader_corin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n025_trader_corin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n025_trader_corin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n025_trader_corin-accepting-i" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2880, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2880, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2880, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2880, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2880, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2880, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2880, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2880, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2880, false);
			 end 

	end
	if npc_talk_index == "n025_trader_corin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,288, 1);
				api_quest_SetJournalStep(userObjID,288, 1);
				api_quest_SetQuestStep(userObjID,288, 1);
				npc_talk_index = "n025_trader_corin-1";

	end
	if npc_talk_index == "n025_trader_corin-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 371, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 372, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200371, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200372, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300077, 1);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n025_trader_corin-3-b" then 
	end
	if npc_talk_index == "n025_trader_corin-3-c" then 
	end
	if npc_talk_index == "n025_trader_corin-3-d" then 
	end
	if npc_talk_index == "n025_trader_corin-3-e" then 
	end
	if npc_talk_index == "n025_trader_corin-3-f" then 
	end
	if npc_talk_index == "n025_trader_corin-3-g" then 
	end
	if npc_talk_index == "n025_trader_corin-3-h" then 
	end
	if npc_talk_index == "n025_trader_corin-3-i" then 
	end
	if npc_talk_index == "n025_trader_corin-3-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2880, true);
				 api_quest_RewardQuestUser(userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2880, true);
				 api_quest_RewardQuestUser(userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2880, true);
				 api_quest_RewardQuestUser(userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2880, true);
				 api_quest_RewardQuestUser(userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2880, true);
				 api_quest_RewardQuestUser(userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2880, true);
				 api_quest_RewardQuestUser(userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2880, true);
				 api_quest_RewardQuestUser(userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2880, true);
				 api_quest_RewardQuestUser(userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2880, true);
				 api_quest_RewardQuestUser(userObjID, 2880, questID, 1);
			 end 
	end
	if npc_talk_index == "n025_trader_corin-3-k" then 
				api_quest_DelQuestItem(userObjID, 300077, 1);

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 289, 1);
					api_quest_SetQuestStep(userObjID, 289, 1);
					api_quest_SetJournalStep(userObjID, 289, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n025_trader_corin-3-l" then 
	end
	if npc_talk_index == "n025_trader_corin-3-m" then 
	end
	if npc_talk_index == "n025_trader_corin-3-n" then 
	end
	if npc_talk_index == "n025_trader_corin-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n025_trader_corin-1", "sq08_289_iron_scale2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_288_iron_scale1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 288);
	if qstep == 2 and CountIndex == 371 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300077, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300077, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 372 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300077, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300077, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300077 then

	end
	if qstep == 2 and CountIndex == 200371 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300077, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300077, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200372 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300077, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300077, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_288_iron_scale1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 288);
	if qstep == 2 and CountIndex == 371 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 372 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300077 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200371 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200372 and Count >= TargetCount  then

	end
end

function sq08_288_iron_scale1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 288);
	local questID=288;
end

function sq08_288_iron_scale1_OnRemoteStart( userObjID, questID )
end

function sq08_288_iron_scale1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_288_iron_scale1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,288, 1);
				api_quest_SetJournalStep(userObjID,288, 1);
				api_quest_SetQuestStep(userObjID,288, 1);
				npc_talk_index = "n025_trader_corin-1";
end

</VillageServer>

<GameServer>
function sq08_288_iron_scale1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 25 then
		sq08_288_iron_scale1_OnTalk_n025_trader_corin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n025_trader_corin--------------------------------------------------------------------------------
function sq08_288_iron_scale1_OnTalk_n025_trader_corin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n025_trader_corin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n025_trader_corin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n025_trader_corin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n025_trader_corin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n025_trader_corin-accepting-i" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, false);
			 end 

	end
	if npc_talk_index == "n025_trader_corin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,288, 1);
				api_quest_SetJournalStep( pRoom, userObjID,288, 1);
				api_quest_SetQuestStep( pRoom, userObjID,288, 1);
				npc_talk_index = "n025_trader_corin-1";

	end
	if npc_talk_index == "n025_trader_corin-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 371, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 372, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200371, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200372, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300077, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n025_trader_corin-3-b" then 
	end
	if npc_talk_index == "n025_trader_corin-3-c" then 
	end
	if npc_talk_index == "n025_trader_corin-3-d" then 
	end
	if npc_talk_index == "n025_trader_corin-3-e" then 
	end
	if npc_talk_index == "n025_trader_corin-3-f" then 
	end
	if npc_talk_index == "n025_trader_corin-3-g" then 
	end
	if npc_talk_index == "n025_trader_corin-3-h" then 
	end
	if npc_talk_index == "n025_trader_corin-3-i" then 
	end
	if npc_talk_index == "n025_trader_corin-3-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2880, questID, 1);
			 end 
	end
	if npc_talk_index == "n025_trader_corin-3-k" then 
				api_quest_DelQuestItem( pRoom, userObjID, 300077, 1);

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 289, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 289, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 289, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n025_trader_corin-3-l" then 
	end
	if npc_talk_index == "n025_trader_corin-3-m" then 
	end
	if npc_talk_index == "n025_trader_corin-3-n" then 
	end
	if npc_talk_index == "n025_trader_corin-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n025_trader_corin-1", "sq08_289_iron_scale2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_288_iron_scale1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 288);
	if qstep == 2 and CountIndex == 371 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300077, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300077, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 372 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300077, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300077, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300077 then

	end
	if qstep == 2 and CountIndex == 200371 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300077, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300077, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200372 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300077, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300077, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_288_iron_scale1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 288);
	if qstep == 2 and CountIndex == 371 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 372 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300077 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200371 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200372 and Count >= TargetCount  then

	end
end

function sq08_288_iron_scale1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 288);
	local questID=288;
end

function sq08_288_iron_scale1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_288_iron_scale1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_288_iron_scale1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,288, 1);
				api_quest_SetJournalStep( pRoom, userObjID,288, 1);
				api_quest_SetQuestStep( pRoom, userObjID,288, 1);
				npc_talk_index = "n025_trader_corin-1";
end

</GameServer>