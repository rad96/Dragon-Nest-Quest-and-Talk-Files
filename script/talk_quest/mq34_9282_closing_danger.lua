<VillageServer>

function mq34_9282_closing_danger_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 101 then
		mq34_9282_closing_danger_OnTalk_n101_event_tracey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1046 then
		mq34_9282_closing_danger_OnTalk_n1046_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1047 then
		mq34_9282_closing_danger_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq34_9282_closing_danger_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n101_event_tracey--------------------------------------------------------------------------------
function mq34_9282_closing_danger_OnTalk_n101_event_tracey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n101_event_tracey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n101_event_tracey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n101_event_tracey-accepting-h" then
				if api_quest_HasQuestItem(userObjID, 400381, 1) == -3 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400381, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400381, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if npc_talk_index == "n101_event_tracey-accepting-i" then
				api_quest_AddQuest(userObjID,9282, 2);
				api_quest_SetJournalStep(userObjID,9282, 1);
				api_quest_SetQuestStep(userObjID,9282, 1);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1046_slave_david--------------------------------------------------------------------------------
function mq34_9282_closing_danger_OnTalk_n1046_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1046_slave_david-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1046_slave_david-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1046_slave_david-1-b" then 
	end
	if npc_talk_index == "n1046_slave_david-1-c" then 
	end
	if npc_talk_index == "n1046_slave_david-1-d" then 
	end
	if npc_talk_index == "n1046_slave_david-1-e" then 
	end
	if npc_talk_index == "n1046_slave_david-1-f" then 
	end
	if npc_talk_index == "n1046_slave_david-1-g" then 
	end
	if npc_talk_index == "n1046_slave_david-1-h" then 
	end
	if npc_talk_index == "n1046_slave_david-1-i" then 
	end
	if npc_talk_index == "n1046_slave_david-1-j" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1047_argenta--------------------------------------------------------------------------------
function mq34_9282_closing_danger_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-2-b" then 
	end
	if npc_talk_index == "n1047_argenta-2-c" then 
	end
	if npc_talk_index == "n1047_argenta-2-d" then 
	end
	if npc_talk_index == "n1047_argenta-2-e" then 
	end
	if npc_talk_index == "n1047_argenta-2-f" then 
	end
	if npc_talk_index == "n1047_argenta-2-g" then 
	end
	if npc_talk_index == "n1047_argenta-2-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92821, true);
				 api_quest_RewardQuestUser(userObjID, 92821, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92821, true);
				 api_quest_RewardQuestUser(userObjID, 92821, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92823, true);
				 api_quest_RewardQuestUser(userObjID, 92823, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92824, true);
				 api_quest_RewardQuestUser(userObjID, 92824, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92825, true);
				 api_quest_RewardQuestUser(userObjID, 92825, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92826, true);
				 api_quest_RewardQuestUser(userObjID, 92826, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92827, true);
				 api_quest_RewardQuestUser(userObjID, 92827, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92828, true);
				 api_quest_RewardQuestUser(userObjID, 92828, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92829, true);
				 api_quest_RewardQuestUser(userObjID, 92829, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-2-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9283, 2);
					api_quest_SetQuestStep(userObjID, 9283, 1);
					api_quest_SetJournalStep(userObjID, 9283, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1047_argenta-1", "mq34_9283_encounter_the_enemy.xml");
			return;
		end
	end
	if npc_talk_index == "n1047_argenta-2-a" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq34_9282_closing_danger_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-f" then
				if api_quest_HasQuestItem(userObjID, 400381, 1) == -3 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400381, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400381, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9282, 2);
				api_quest_SetJournalStep(userObjID,9282, 1);
				api_quest_SetQuestStep(userObjID,9282, 1);
				npc_talk_index = "n822_teramai-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9282_closing_danger_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9282);
end

function mq34_9282_closing_danger_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9282);
end

function mq34_9282_closing_danger_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9282);
	local questID=9282;
end

function mq34_9282_closing_danger_OnRemoteStart( userObjID, questID )
end

function mq34_9282_closing_danger_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9282_closing_danger_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9282, 2);
				api_quest_SetJournalStep(userObjID,9282, 1);
				api_quest_SetQuestStep(userObjID,9282, 1);
				npc_talk_index = "n822_teramai-1";
end

</VillageServer>

<GameServer>
function mq34_9282_closing_danger_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 101 then
		mq34_9282_closing_danger_OnTalk_n101_event_tracey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1046 then
		mq34_9282_closing_danger_OnTalk_n1046_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1047 then
		mq34_9282_closing_danger_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq34_9282_closing_danger_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n101_event_tracey--------------------------------------------------------------------------------
function mq34_9282_closing_danger_OnTalk_n101_event_tracey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n101_event_tracey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n101_event_tracey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n101_event_tracey-accepting-h" then
				if api_quest_HasQuestItem( pRoom, userObjID, 400381, 1) == -3 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400381, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400381, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if npc_talk_index == "n101_event_tracey-accepting-i" then
				api_quest_AddQuest( pRoom, userObjID,9282, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9282, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9282, 1);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1046_slave_david--------------------------------------------------------------------------------
function mq34_9282_closing_danger_OnTalk_n1046_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1046_slave_david-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1046_slave_david-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1046_slave_david-1-b" then 
	end
	if npc_talk_index == "n1046_slave_david-1-c" then 
	end
	if npc_talk_index == "n1046_slave_david-1-d" then 
	end
	if npc_talk_index == "n1046_slave_david-1-e" then 
	end
	if npc_talk_index == "n1046_slave_david-1-f" then 
	end
	if npc_talk_index == "n1046_slave_david-1-g" then 
	end
	if npc_talk_index == "n1046_slave_david-1-h" then 
	end
	if npc_talk_index == "n1046_slave_david-1-i" then 
	end
	if npc_talk_index == "n1046_slave_david-1-j" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1047_argenta--------------------------------------------------------------------------------
function mq34_9282_closing_danger_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-2-b" then 
	end
	if npc_talk_index == "n1047_argenta-2-c" then 
	end
	if npc_talk_index == "n1047_argenta-2-d" then 
	end
	if npc_talk_index == "n1047_argenta-2-e" then 
	end
	if npc_talk_index == "n1047_argenta-2-f" then 
	end
	if npc_talk_index == "n1047_argenta-2-g" then 
	end
	if npc_talk_index == "n1047_argenta-2-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92821, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92821, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92821, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92821, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92823, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92823, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92824, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92824, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92825, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92825, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92826, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92826, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92827, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92827, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92828, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92828, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92829, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92829, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-2-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9283, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9283, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9283, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1047_argenta-1", "mq34_9283_encounter_the_enemy.xml");
			return;
		end
	end
	if npc_talk_index == "n1047_argenta-2-a" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq34_9282_closing_danger_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-f" then
				if api_quest_HasQuestItem( pRoom, userObjID, 400381, 1) == -3 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400381, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400381, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9282, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9282, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9282, 1);
				npc_talk_index = "n822_teramai-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9282_closing_danger_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9282);
end

function mq34_9282_closing_danger_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9282);
end

function mq34_9282_closing_danger_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9282);
	local questID=9282;
end

function mq34_9282_closing_danger_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9282_closing_danger_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9282_closing_danger_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9282, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9282, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9282, 1);
				npc_talk_index = "n822_teramai-1";
end

</GameServer>