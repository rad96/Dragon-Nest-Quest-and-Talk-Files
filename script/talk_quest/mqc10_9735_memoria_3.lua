<VillageServer>

function mqc10_9735_memoria_3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 735 then
		mqc10_9735_memoria_3_OnTalk_n735_darklair_mocha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 749 then
		mqc10_9735_memoria_3_OnTalk_n749_night_kasius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 750 then
		mqc10_9735_memoria_3_OnTalk_n750_night_kasius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mqc10_9735_memoria_3_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n735_darklair_mocha--------------------------------------------------------------------------------
function mqc10_9735_memoria_3_OnTalk_n735_darklair_mocha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n735_darklair_mocha-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n735_darklair_mocha-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n735_darklair_mocha-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n735_darklair_mocha-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n735_darklair_mocha-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9735, 2);
				api_quest_SetJournalStep(userObjID,9735, 1);
				api_quest_SetQuestStep(userObjID,9735, 1);
				npc_talk_index = "n735_darklair_mocha-1";

	end
	if npc_talk_index == "n735_darklair_mocha-4-b" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-b" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-b" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-c" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-d" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-e" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-f" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-g" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-h" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-i" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-j" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-k" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-l" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-m" then 
	end
	if npc_talk_index == "n735_darklair_mocha-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n749_night_kasius--------------------------------------------------------------------------------
function mqc10_9735_memoria_3_OnTalk_n749_night_kasius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n749_night_kasius-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n750_night_kasius--------------------------------------------------------------------------------
function mqc10_9735_memoria_3_OnTalk_n750_night_kasius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n750_night_kasius-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n750_night_kasius-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n750_night_kasius-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n750_night_kasius-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n750_night_kasius-2-b" then 
	end
	if npc_talk_index == "n750_night_kasius-2-c" then 
	end
	if npc_talk_index == "n750_night_kasius-2-d" then 
	end
	if npc_talk_index == "n750_night_kasius-2-e" then 
	end
	if npc_talk_index == "n750_night_kasius-2-f" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n750_night_kasius-3-b" then 
	end
	if npc_talk_index == "n750_night_kasius-3-c" then 
	end
	if npc_talk_index == "n750_night_kasius-3-d" then 
	end
	if npc_talk_index == "n750_night_kasius-3-e" then 
	end
	if npc_talk_index == "n750_night_kasius-3-f" then 
	end
	if npc_talk_index == "n750_night_kasius-3-g" then 
	end
	if npc_talk_index == "n750_night_kasius-3-h" then 
	end
	if npc_talk_index == "n750_night_kasius-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mqc10_9735_memoria_3_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n822_teramai-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-5-b" then 
	end
	if npc_talk_index == "n822_teramai-5-c" then 
	end
	if npc_talk_index == "n822_teramai-5-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97350, true);
				 api_quest_RewardQuestUser(userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97350, true);
				 api_quest_RewardQuestUser(userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97350, true);
				 api_quest_RewardQuestUser(userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97350, true);
				 api_quest_RewardQuestUser(userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97350, true);
				 api_quest_RewardQuestUser(userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97350, true);
				 api_quest_RewardQuestUser(userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97350, true);
				 api_quest_RewardQuestUser(userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97350, true);
				 api_quest_RewardQuestUser(userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97350, true);
				 api_quest_RewardQuestUser(userObjID, 97350, questID, 1);
			 end 
	end
	if npc_talk_index == "n822_teramai-5-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									if api_quest_IsMarkingCompleteQuest(userObjID, 9282) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 9264) == 1 then
				else
									if api_user_GetUserClassID(userObjID) == 2 then
									api_quest_AddQuest(userObjID,9264, 2);
				api_quest_SetJournalStep(userObjID,9264, 1);
				api_quest_SetQuestStep(userObjID,9264, 1);

				else
									api_quest_AddQuest(userObjID,9282, 2);
				api_quest_SetJournalStep(userObjID,9282, 1);
				api_quest_SetQuestStep(userObjID,9282, 1);

				end

				end

				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc10_9735_memoria_3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9735);
end

function mqc10_9735_memoria_3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9735);
end

function mqc10_9735_memoria_3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9735);
	local questID=9735;
end

function mqc10_9735_memoria_3_OnRemoteStart( userObjID, questID )
end

function mqc10_9735_memoria_3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc10_9735_memoria_3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9735, 2);
				api_quest_SetJournalStep(userObjID,9735, 1);
				api_quest_SetQuestStep(userObjID,9735, 1);
				npc_talk_index = "n735_darklair_mocha-1";
end

</VillageServer>

<GameServer>
function mqc10_9735_memoria_3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 735 then
		mqc10_9735_memoria_3_OnTalk_n735_darklair_mocha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 749 then
		mqc10_9735_memoria_3_OnTalk_n749_night_kasius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 750 then
		mqc10_9735_memoria_3_OnTalk_n750_night_kasius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mqc10_9735_memoria_3_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n735_darklair_mocha--------------------------------------------------------------------------------
function mqc10_9735_memoria_3_OnTalk_n735_darklair_mocha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n735_darklair_mocha-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n735_darklair_mocha-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n735_darklair_mocha-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n735_darklair_mocha-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n735_darklair_mocha-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9735, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9735, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9735, 1);
				npc_talk_index = "n735_darklair_mocha-1";

	end
	if npc_talk_index == "n735_darklair_mocha-4-b" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-b" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-b" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-c" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-d" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-e" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-f" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-g" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-h" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-i" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-j" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-k" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-l" then 
	end
	if npc_talk_index == "n735_darklair_mocha-4-m" then 
	end
	if npc_talk_index == "n735_darklair_mocha-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n749_night_kasius--------------------------------------------------------------------------------
function mqc10_9735_memoria_3_OnTalk_n749_night_kasius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n749_night_kasius-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n750_night_kasius--------------------------------------------------------------------------------
function mqc10_9735_memoria_3_OnTalk_n750_night_kasius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n750_night_kasius-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n750_night_kasius-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n750_night_kasius-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n750_night_kasius-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n750_night_kasius-2-b" then 
	end
	if npc_talk_index == "n750_night_kasius-2-c" then 
	end
	if npc_talk_index == "n750_night_kasius-2-d" then 
	end
	if npc_talk_index == "n750_night_kasius-2-e" then 
	end
	if npc_talk_index == "n750_night_kasius-2-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n750_night_kasius-3-b" then 
	end
	if npc_talk_index == "n750_night_kasius-3-c" then 
	end
	if npc_talk_index == "n750_night_kasius-3-d" then 
	end
	if npc_talk_index == "n750_night_kasius-3-e" then 
	end
	if npc_talk_index == "n750_night_kasius-3-f" then 
	end
	if npc_talk_index == "n750_night_kasius-3-g" then 
	end
	if npc_talk_index == "n750_night_kasius-3-h" then 
	end
	if npc_talk_index == "n750_night_kasius-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mqc10_9735_memoria_3_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n822_teramai-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-5-b" then 
	end
	if npc_talk_index == "n822_teramai-5-c" then 
	end
	if npc_talk_index == "n822_teramai-5-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97350, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97350, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97350, questID, 1);
			 end 
	end
	if npc_talk_index == "n822_teramai-5-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9282) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9264) == 1 then
				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									api_quest_AddQuest( pRoom, userObjID,9264, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9264, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9264, 1);

				else
									api_quest_AddQuest( pRoom, userObjID,9282, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9282, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9282, 1);

				end

				end

				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc10_9735_memoria_3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9735);
end

function mqc10_9735_memoria_3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9735);
end

function mqc10_9735_memoria_3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9735);
	local questID=9735;
end

function mqc10_9735_memoria_3_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc10_9735_memoria_3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc10_9735_memoria_3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9735, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9735, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9735, 1);
				npc_talk_index = "n735_darklair_mocha-1";
end

</GameServer>