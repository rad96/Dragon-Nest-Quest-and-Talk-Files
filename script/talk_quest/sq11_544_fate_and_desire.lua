<VillageServer>

function sq11_544_fate_and_desire_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 44 then
		sq11_544_fate_and_desire_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 70 then
		sq11_544_fate_and_desire_OnTalk_n070_plane_ticketer_sorene(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq11_544_fate_and_desire_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n044_archer_master_ishilien-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-1-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-i" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-j" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-k" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-l" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5440, true);
				 api_quest_RewardQuestUser(userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5440, true);
				 api_quest_RewardQuestUser(userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5440, true);
				 api_quest_RewardQuestUser(userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5440, true);
				 api_quest_RewardQuestUser(userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5440, true);
				 api_quest_RewardQuestUser(userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5440, true);
				 api_quest_RewardQuestUser(userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5440, true);
				 api_quest_RewardQuestUser(userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5440, true);
				 api_quest_RewardQuestUser(userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5440, true);
				 api_quest_RewardQuestUser(userObjID, 5440, questID, 1);
			 end 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n070_plane_ticketer_sorene--------------------------------------------------------------------------------
function sq11_544_fate_and_desire_OnTalk_n070_plane_ticketer_sorene(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n070_plane_ticketer_sorene-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n070_plane_ticketer_sorene-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n070_plane_ticketer_sorene-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n070_plane_ticketer_sorene-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5440, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5440, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5440, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5440, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5440, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5440, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5440, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5440, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5440, false);
			 end 

	end
	if npc_talk_index == "n070_plane_ticketer_sorene-accepting-acceptted" then
				api_quest_AddQuest(userObjID,544, 1);
				api_quest_SetJournalStep(userObjID,544, 1);
				api_quest_SetQuestStep(userObjID,544, 1);
				npc_talk_index = "n070_plane_ticketer_sorene-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_544_fate_and_desire_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 544);
end

function sq11_544_fate_and_desire_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 544);
end

function sq11_544_fate_and_desire_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 544);
	local questID=544;
end

function sq11_544_fate_and_desire_OnRemoteStart( userObjID, questID )
end

function sq11_544_fate_and_desire_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_544_fate_and_desire_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,544, 1);
				api_quest_SetJournalStep(userObjID,544, 1);
				api_quest_SetQuestStep(userObjID,544, 1);
				npc_talk_index = "n070_plane_ticketer_sorene-1";
end

</VillageServer>

<GameServer>
function sq11_544_fate_and_desire_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 44 then
		sq11_544_fate_and_desire_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 70 then
		sq11_544_fate_and_desire_OnTalk_n070_plane_ticketer_sorene( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq11_544_fate_and_desire_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n044_archer_master_ishilien-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-1-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-i" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-j" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-k" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-1-l" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5440, questID, 1);
			 end 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n070_plane_ticketer_sorene--------------------------------------------------------------------------------
function sq11_544_fate_and_desire_OnTalk_n070_plane_ticketer_sorene( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n070_plane_ticketer_sorene-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n070_plane_ticketer_sorene-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n070_plane_ticketer_sorene-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n070_plane_ticketer_sorene-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5440, false);
			 end 

	end
	if npc_talk_index == "n070_plane_ticketer_sorene-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,544, 1);
				api_quest_SetJournalStep( pRoom, userObjID,544, 1);
				api_quest_SetQuestStep( pRoom, userObjID,544, 1);
				npc_talk_index = "n070_plane_ticketer_sorene-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_544_fate_and_desire_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 544);
end

function sq11_544_fate_and_desire_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 544);
end

function sq11_544_fate_and_desire_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 544);
	local questID=544;
end

function sq11_544_fate_and_desire_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_544_fate_and_desire_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_544_fate_and_desire_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,544, 1);
				api_quest_SetJournalStep( pRoom, userObjID,544, 1);
				api_quest_SetQuestStep( pRoom, userObjID,544, 1);
				npc_talk_index = "n070_plane_ticketer_sorene-1";
end

</GameServer>