<VillageServer>

function mqc02_9579_mysterious_silver_hair_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 23 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1848 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n1848_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 27 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n027_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 31 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n031_adventurer_wilber(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 53 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n053_adventurer_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n023_ranger_fugus--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n023_ranger_fugus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n023_ranger_fugus-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n023_ranger_fugus-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n023_ranger_fugus-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n023_ranger_fugus-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9579, 2);
				api_quest_SetJournalStep(userObjID,9579, 1);
				api_quest_SetQuestStep(userObjID,9579, 1);
				npc_talk_index = "n023_ranger_fugus-1";

	end
	if npc_talk_index == "n023_ranger_fugus-1-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1848_shaolong--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n1848_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1848_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1848_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1848_shaolong-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1848_shaolong-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1848_shaolong-2-b" then 
	end
	if npc_talk_index == "n1848_shaolong-2-lvchk" then 
				if api_user_GetUserLevel(userObjID) >= 12 then
									npc_talk_index = "n1848_shaolong-2-c";

				else
									npc_talk_index = "n1848_shaolong-2-j";

				end
	end
	if npc_talk_index == "n1848_shaolong-2-d" then 
	end
	if npc_talk_index == "n1848_shaolong-2-e" then 
	end
	if npc_talk_index == "n1848_shaolong-2-f" then 
	end
	if npc_talk_index == "n1848_shaolong-2-g" then 
	end
	if npc_talk_index == "n1848_shaolong-2-h" then 
	end
	if npc_talk_index == "n1848_shaolong-2-i" then 
	end
	if npc_talk_index == "n1848_shaolong-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1848_shaolong-9-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95790, true);
				 api_quest_RewardQuestUser(userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95790, true);
				 api_quest_RewardQuestUser(userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95790, true);
				 api_quest_RewardQuestUser(userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95790, true);
				 api_quest_RewardQuestUser(userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95790, true);
				 api_quest_RewardQuestUser(userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95790, true);
				 api_quest_RewardQuestUser(userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95790, true);
				 api_quest_RewardQuestUser(userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95790, true);
				 api_quest_RewardQuestUser(userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95790, true);
				 api_quest_RewardQuestUser(userObjID, 95790, questID, 1);
			 end 
	end
	if npc_talk_index == "n1848_shaolong-9-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9580, 2);
					api_quest_SetQuestStep(userObjID, 9580, 1);
					api_quest_SetJournalStep(userObjID, 9580, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1848_shaolong-9-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1848_shaolong-1", "mqc02_9580_unexpected_encounter.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-8-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-8-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-8-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-8-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-8-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 9);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n027_argenta--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n027_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n027_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n027_argenta-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n027_argenta-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n027_argenta-7-b" then 
	end
	if npc_talk_index == "n027_argenta-7-c" then 
	end
	if npc_talk_index == "n027_argenta-7-d" then 
	end
	if npc_talk_index == "n027_argenta-7-e" then 
	end
	if npc_talk_index == "n027_argenta-7-f" then 
	end
	if npc_talk_index == "n027_argenta-7-g" then 
	end
	if npc_talk_index == "n027_argenta-7-h" then 
	end
	if npc_talk_index == "n027_argenta-7-i" then 
	end
	if npc_talk_index == "n027_argenta-7-j" then 
	end
	if npc_talk_index == "n027_argenta-7-k" then 
	end
	if npc_talk_index == "n027_argenta-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n031_adventurer_wilber--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n031_adventurer_wilber(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n031_adventurer_wilber-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n031_adventurer_wilber-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n031_adventurer_wilber-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n031_adventurer_wilber-5-b" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-5-c" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-5-d" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-5-e" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n053_adventurer_wounded--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n053_adventurer_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n053_adventurer_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n053_adventurer_wounded-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc02_9579_mysterious_silver_hair_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9579);
end

function mqc02_9579_mysterious_silver_hair_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9579);
end

function mqc02_9579_mysterious_silver_hair_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9579);
	local questID=9579;
end

function mqc02_9579_mysterious_silver_hair_OnRemoteStart( userObjID, questID )
end

function mqc02_9579_mysterious_silver_hair_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc02_9579_mysterious_silver_hair_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9579, 2);
				api_quest_SetJournalStep(userObjID,9579, 1);
				api_quest_SetQuestStep(userObjID,9579, 1);
				npc_talk_index = "n023_ranger_fugus-1";
end

</VillageServer>

<GameServer>
function mqc02_9579_mysterious_silver_hair_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 23 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1848 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n1848_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 27 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n027_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 31 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n031_adventurer_wilber( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 53 then
		mqc02_9579_mysterious_silver_hair_OnTalk_n053_adventurer_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n023_ranger_fugus--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n023_ranger_fugus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n023_ranger_fugus-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n023_ranger_fugus-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n023_ranger_fugus-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n023_ranger_fugus-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9579, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9579, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9579, 1);
				npc_talk_index = "n023_ranger_fugus-1";

	end
	if npc_talk_index == "n023_ranger_fugus-1-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1848_shaolong--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n1848_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1848_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1848_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1848_shaolong-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1848_shaolong-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1848_shaolong-2-b" then 
	end
	if npc_talk_index == "n1848_shaolong-2-lvchk" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 12 then
									npc_talk_index = "n1848_shaolong-2-c";

				else
									npc_talk_index = "n1848_shaolong-2-j";

				end
	end
	if npc_talk_index == "n1848_shaolong-2-d" then 
	end
	if npc_talk_index == "n1848_shaolong-2-e" then 
	end
	if npc_talk_index == "n1848_shaolong-2-f" then 
	end
	if npc_talk_index == "n1848_shaolong-2-g" then 
	end
	if npc_talk_index == "n1848_shaolong-2-h" then 
	end
	if npc_talk_index == "n1848_shaolong-2-i" then 
	end
	if npc_talk_index == "n1848_shaolong-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1848_shaolong-9-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95790, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95790, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95790, questID, 1);
			 end 
	end
	if npc_talk_index == "n1848_shaolong-9-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9580, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9580, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9580, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1848_shaolong-9-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1848_shaolong-1", "mqc02_9580_unexpected_encounter.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-8-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-8-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-8-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-8-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-8-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 9);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n027_argenta--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n027_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n027_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n027_argenta-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n027_argenta-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n027_argenta-7-b" then 
	end
	if npc_talk_index == "n027_argenta-7-c" then 
	end
	if npc_talk_index == "n027_argenta-7-d" then 
	end
	if npc_talk_index == "n027_argenta-7-e" then 
	end
	if npc_talk_index == "n027_argenta-7-f" then 
	end
	if npc_talk_index == "n027_argenta-7-g" then 
	end
	if npc_talk_index == "n027_argenta-7-h" then 
	end
	if npc_talk_index == "n027_argenta-7-i" then 
	end
	if npc_talk_index == "n027_argenta-7-j" then 
	end
	if npc_talk_index == "n027_argenta-7-k" then 
	end
	if npc_talk_index == "n027_argenta-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n031_adventurer_wilber--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n031_adventurer_wilber( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n031_adventurer_wilber-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n031_adventurer_wilber-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n031_adventurer_wilber-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n031_adventurer_wilber-5-b" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-5-c" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-5-d" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-5-e" then 
	end
	if npc_talk_index == "n031_adventurer_wilber-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n053_adventurer_wounded--------------------------------------------------------------------------------
function mqc02_9579_mysterious_silver_hair_OnTalk_n053_adventurer_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n053_adventurer_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n053_adventurer_wounded-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc02_9579_mysterious_silver_hair_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9579);
end

function mqc02_9579_mysterious_silver_hair_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9579);
end

function mqc02_9579_mysterious_silver_hair_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9579);
	local questID=9579;
end

function mqc02_9579_mysterious_silver_hair_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc02_9579_mysterious_silver_hair_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc02_9579_mysterious_silver_hair_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9579, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9579, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9579, 1);
				npc_talk_index = "n023_ranger_fugus-1";
end

</GameServer>