<VillageServer>

function mq34_9292_meaning_of_the_origin_for_forest_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9292_meaning_of_the_origin_for_forest_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1074 then
		mq34_9292_meaning_of_the_origin_for_forest_OnTalk_n1074_azelia_dead(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9292_meaning_of_the_origin_for_forest_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9292, 2);
				api_quest_SetJournalStep(userObjID,9292, 1);
				api_quest_SetQuestStep(userObjID,9292, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-3-b" then 
	end
	if npc_talk_index == "n1047_argenta-3-c" then 
	end
	if npc_talk_index == "n1047_argenta-3-d" then 
	end
	if npc_talk_index == "n1047_argenta-3-e" then 
	end
	if npc_talk_index == "n1047_argenta-3-f" then 
	end
	if npc_talk_index == "n1047_argenta-3-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92921, true);
				 api_quest_RewardQuestUser(userObjID, 92921, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92921, true);
				 api_quest_RewardQuestUser(userObjID, 92921, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92923, true);
				 api_quest_RewardQuestUser(userObjID, 92923, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92924, true);
				 api_quest_RewardQuestUser(userObjID, 92924, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92925, true);
				 api_quest_RewardQuestUser(userObjID, 92925, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92926, true);
				 api_quest_RewardQuestUser(userObjID, 92926, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92927, true);
				 api_quest_RewardQuestUser(userObjID, 92927, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92928, true);
				 api_quest_RewardQuestUser(userObjID, 92928, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92929, true);
				 api_quest_RewardQuestUser(userObjID, 92929, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9293, 2);
					api_quest_SetQuestStep(userObjID, 9293, 1);
					api_quest_SetJournalStep(userObjID, 9293, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400395, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400395, api_quest_HasQuestItem(userObjID, 400395, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1047_argenta-1", "mq34_9293_who_is_the_traitor.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1074_azelia_dead--------------------------------------------------------------------------------
function mq34_9292_meaning_of_the_origin_for_forest_OnTalk_n1074_azelia_dead(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1074_azelia_dead-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1074_azelia_dead-2";
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1074_azelia_dead-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1074_azelia_dead-2-a" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400395, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400395, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9292_meaning_of_the_origin_for_forest_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9292);
end

function mq34_9292_meaning_of_the_origin_for_forest_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9292);
end

function mq34_9292_meaning_of_the_origin_for_forest_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9292);
	local questID=9292;
end

function mq34_9292_meaning_of_the_origin_for_forest_OnRemoteStart( userObjID, questID )
end

function mq34_9292_meaning_of_the_origin_for_forest_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9292_meaning_of_the_origin_for_forest_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9292, 2);
				api_quest_SetJournalStep(userObjID,9292, 1);
				api_quest_SetQuestStep(userObjID,9292, 1);
				npc_talk_index = "n1047_argenta-1";
end

</VillageServer>

<GameServer>
function mq34_9292_meaning_of_the_origin_for_forest_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9292_meaning_of_the_origin_for_forest_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1074 then
		mq34_9292_meaning_of_the_origin_for_forest_OnTalk_n1074_azelia_dead( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9292_meaning_of_the_origin_for_forest_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9292, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9292, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9292, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-3-b" then 
	end
	if npc_talk_index == "n1047_argenta-3-c" then 
	end
	if npc_talk_index == "n1047_argenta-3-d" then 
	end
	if npc_talk_index == "n1047_argenta-3-e" then 
	end
	if npc_talk_index == "n1047_argenta-3-f" then 
	end
	if npc_talk_index == "n1047_argenta-3-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92921, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92921, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92921, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92921, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92923, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92923, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92924, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92924, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92925, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92925, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92926, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92926, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92927, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92927, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92928, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92928, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92929, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92929, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9293, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9293, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9293, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400395, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400395, api_quest_HasQuestItem( pRoom, userObjID, 400395, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1047_argenta-1", "mq34_9293_who_is_the_traitor.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1074_azelia_dead--------------------------------------------------------------------------------
function mq34_9292_meaning_of_the_origin_for_forest_OnTalk_n1074_azelia_dead( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1074_azelia_dead-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1074_azelia_dead-2";
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1074_azelia_dead-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1074_azelia_dead-2-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400395, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400395, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9292_meaning_of_the_origin_for_forest_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9292);
end

function mq34_9292_meaning_of_the_origin_for_forest_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9292);
end

function mq34_9292_meaning_of_the_origin_for_forest_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9292);
	local questID=9292;
end

function mq34_9292_meaning_of_the_origin_for_forest_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9292_meaning_of_the_origin_for_forest_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9292_meaning_of_the_origin_for_forest_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9292, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9292, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9292, 1);
				npc_talk_index = "n1047_argenta-1";
end

</GameServer>