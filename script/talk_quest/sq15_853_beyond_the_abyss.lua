<VillageServer>

function sq15_853_beyond_the_abyss_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 707 then
		sq15_853_beyond_the_abyss_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function sq15_853_beyond_the_abyss_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8530, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8530, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8530, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8530, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8530, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8530, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8530, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8530, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8530, false);
			 end 

	end
	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,853, 1);
				api_quest_SetJournalStep(userObjID,853, 1);
				api_quest_SetQuestStep(userObjID,853, 1);
				npc_talk_index = "n707_sidel-1";

	end
	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 201722, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201769, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 1722, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 1769, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300496, 1);
				api_quest_SetCountingInfo(userObjID, questID, 5, 3, 300497, 1);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8530, true);
				 api_quest_RewardQuestUser(userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8530, true);
				 api_quest_RewardQuestUser(userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8530, true);
				 api_quest_RewardQuestUser(userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8530, true);
				 api_quest_RewardQuestUser(userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8530, true);
				 api_quest_RewardQuestUser(userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8530, true);
				 api_quest_RewardQuestUser(userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8530, true);
				 api_quest_RewardQuestUser(userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8530, true);
				 api_quest_RewardQuestUser(userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8530, true);
				 api_quest_RewardQuestUser(userObjID, 8530, questID, 1);
			 end 
	end
	if npc_talk_index == "n707_sidel-3-e" then 

				if api_quest_HasQuestItem(userObjID, 300496, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300496, api_quest_HasQuestItem(userObjID, 300496, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300497, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300497, api_quest_HasQuestItem(userObjID, 300497, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_853_beyond_the_abyss_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 853);
	if qstep == 2 and CountIndex == 201722 then
				if api_quest_HasQuestItem(userObjID, 300496, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300496, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300496, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 201769 then
				if api_quest_HasQuestItem(userObjID, 300497, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300497, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300497, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 300496 then
				if api_quest_HasQuestItem(userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 300497 then
				if api_quest_HasQuestItem(userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 1722 then
				if api_quest_HasQuestItem(userObjID, 300496, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300496, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300496, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 1769 then
				if api_quest_HasQuestItem(userObjID, 300497, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300497, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300497, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

	end
end

function sq15_853_beyond_the_abyss_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 853);
	if qstep == 2 and CountIndex == 201722 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201769 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300496 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300497 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1722 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1769 and Count >= TargetCount  then

	end
end

function sq15_853_beyond_the_abyss_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 853);
	local questID=853;
end

function sq15_853_beyond_the_abyss_OnRemoteStart( userObjID, questID )
end

function sq15_853_beyond_the_abyss_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_853_beyond_the_abyss_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,853, 1);
				api_quest_SetJournalStep(userObjID,853, 1);
				api_quest_SetQuestStep(userObjID,853, 1);
				npc_talk_index = "n707_sidel-1";
end

</VillageServer>

<GameServer>
function sq15_853_beyond_the_abyss_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 707 then
		sq15_853_beyond_the_abyss_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function sq15_853_beyond_the_abyss_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, false);
			 end 

	end
	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,853, 1);
				api_quest_SetJournalStep( pRoom, userObjID,853, 1);
				api_quest_SetQuestStep( pRoom, userObjID,853, 1);
				npc_talk_index = "n707_sidel-1";

	end
	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 201722, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201769, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 1722, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 1769, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300496, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 3, 300497, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8530, questID, 1);
			 end 
	end
	if npc_talk_index == "n707_sidel-3-e" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300496, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300496, api_quest_HasQuestItem( pRoom, userObjID, 300496, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300497, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300497, api_quest_HasQuestItem( pRoom, userObjID, 300497, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_853_beyond_the_abyss_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 853);
	if qstep == 2 and CountIndex == 201722 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300496, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300496, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300496, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 201769 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300497, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300497, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300497, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 300496 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 300497 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 1722 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300496, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300496, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300496, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
	if qstep == 2 and CountIndex == 1769 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300497, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300496, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300497, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300497, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300497, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

	end
end

function sq15_853_beyond_the_abyss_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 853);
	if qstep == 2 and CountIndex == 201722 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201769 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300496 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300497 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1722 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1769 and Count >= TargetCount  then

	end
end

function sq15_853_beyond_the_abyss_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 853);
	local questID=853;
end

function sq15_853_beyond_the_abyss_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_853_beyond_the_abyss_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_853_beyond_the_abyss_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,853, 1);
				api_quest_SetJournalStep( pRoom, userObjID,853, 1);
				api_quest_SetQuestStep( pRoom, userObjID,853, 1);
				npc_talk_index = "n707_sidel-1";
end

</GameServer>