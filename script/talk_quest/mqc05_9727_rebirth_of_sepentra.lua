<VillageServer>

function mqc05_9727_rebirth_of_sepentra_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1892 then
		mqc05_9727_rebirth_of_sepentra_OnTalk_n1892_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		mqc05_9727_rebirth_of_sepentra_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc05_9727_rebirth_of_sepentra_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1892_saint_guard--------------------------------------------------------------------------------
function mqc05_9727_rebirth_of_sepentra_OnTalk_n1892_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1892_saint_guard-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1892_saint_guard-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1892_saint_guard-1-b" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-c" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-d" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-e" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-f" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-g" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-h" then 
	end
	if npc_talk_index == "n1892_saint_guard-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function mqc05_9727_rebirth_of_sepentra_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n044_archer_master_ishilien-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n044_archer_master_ishilien-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9727, 2);
				api_quest_SetJournalStep(userObjID,9727, 1);
				api_quest_SetQuestStep(userObjID,9727, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";

	end
	if npc_talk_index == "n044_archer_master_ishilien-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-i" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc05_9727_rebirth_of_sepentra_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n042_general_duglars-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n042_general_duglars-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-3-b" then 
	end
	if npc_talk_index == "n042_general_duglars-3-c" then 
	end
	if npc_talk_index == "n042_general_duglars-3-d" then 
	end
	if npc_talk_index == "n042_general_duglars-3-e" then 
	end
	if npc_talk_index == "n042_general_duglars-3-f" then 
	end
	if npc_talk_index == "n042_general_duglars-3-g" then 
	end
	if npc_talk_index == "n042_general_duglars-3-h" then 
	end
	if npc_talk_index == "n042_general_duglars-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1215, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300337, 1);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n042_general_duglars-5-b" then 
	end
	if npc_talk_index == "n042_general_duglars-5-c" then 
	end
	if npc_talk_index == "n042_general_duglars-5-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97270, true);
				 api_quest_RewardQuestUser(userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97270, true);
				 api_quest_RewardQuestUser(userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97270, true);
				 api_quest_RewardQuestUser(userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97270, true);
				 api_quest_RewardQuestUser(userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97270, true);
				 api_quest_RewardQuestUser(userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97270, true);
				 api_quest_RewardQuestUser(userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97270, true);
				 api_quest_RewardQuestUser(userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97270, true);
				 api_quest_RewardQuestUser(userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97270, true);
				 api_quest_RewardQuestUser(userObjID, 97270, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-5-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9798, 2);
					api_quest_SetQuestStep(userObjID, 9798, 1);
					api_quest_SetJournalStep(userObjID, 9798, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n042_general_duglars-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n042_general_duglars-1", "mqc05_9728_overlooking_cape.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9727_rebirth_of_sepentra_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9727);
	if qstep == 4 and CountIndex == 1215 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300337, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300337, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);

	end
	if qstep == 4 and CountIndex == 300337 then

	end
end

function mqc05_9727_rebirth_of_sepentra_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9727);
	if qstep == 4 and CountIndex == 1215 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 300337 and Count >= TargetCount  then

	end
end

function mqc05_9727_rebirth_of_sepentra_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9727);
	local questID=9727;
end

function mqc05_9727_rebirth_of_sepentra_OnRemoteStart( userObjID, questID )
end

function mqc05_9727_rebirth_of_sepentra_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc05_9727_rebirth_of_sepentra_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9727, 2);
				api_quest_SetJournalStep(userObjID,9727, 1);
				api_quest_SetQuestStep(userObjID,9727, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
end

</VillageServer>

<GameServer>
function mqc05_9727_rebirth_of_sepentra_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1892 then
		mqc05_9727_rebirth_of_sepentra_OnTalk_n1892_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		mqc05_9727_rebirth_of_sepentra_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc05_9727_rebirth_of_sepentra_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1892_saint_guard--------------------------------------------------------------------------------
function mqc05_9727_rebirth_of_sepentra_OnTalk_n1892_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1892_saint_guard-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1892_saint_guard-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1892_saint_guard-1-b" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-c" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-d" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-e" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-f" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-g" then 
	end
	if npc_talk_index == "n1892_saint_guard-1-h" then 
	end
	if npc_talk_index == "n1892_saint_guard-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function mqc05_9727_rebirth_of_sepentra_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n044_archer_master_ishilien-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n044_archer_master_ishilien-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9727, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9727, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9727, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";

	end
	if npc_talk_index == "n044_archer_master_ishilien-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-i" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc05_9727_rebirth_of_sepentra_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n042_general_duglars-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n042_general_duglars-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-3-b" then 
	end
	if npc_talk_index == "n042_general_duglars-3-c" then 
	end
	if npc_talk_index == "n042_general_duglars-3-d" then 
	end
	if npc_talk_index == "n042_general_duglars-3-e" then 
	end
	if npc_talk_index == "n042_general_duglars-3-f" then 
	end
	if npc_talk_index == "n042_general_duglars-3-g" then 
	end
	if npc_talk_index == "n042_general_duglars-3-h" then 
	end
	if npc_talk_index == "n042_general_duglars-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1215, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300337, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n042_general_duglars-5-b" then 
	end
	if npc_talk_index == "n042_general_duglars-5-c" then 
	end
	if npc_talk_index == "n042_general_duglars-5-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97270, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97270, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97270, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97270, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97270, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97270, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97270, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97270, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97270, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97270, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97270, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-5-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9798, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9798, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9798, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n042_general_duglars-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n042_general_duglars-1", "mqc05_9728_overlooking_cape.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9727_rebirth_of_sepentra_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9727);
	if qstep == 4 and CountIndex == 1215 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300337, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300337, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

	end
	if qstep == 4 and CountIndex == 300337 then

	end
end

function mqc05_9727_rebirth_of_sepentra_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9727);
	if qstep == 4 and CountIndex == 1215 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 300337 and Count >= TargetCount  then

	end
end

function mqc05_9727_rebirth_of_sepentra_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9727);
	local questID=9727;
end

function mqc05_9727_rebirth_of_sepentra_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc05_9727_rebirth_of_sepentra_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc05_9727_rebirth_of_sepentra_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9727, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9727, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9727, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
end

</GameServer>