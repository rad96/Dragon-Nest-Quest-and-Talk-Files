<VillageServer>

function mqc15_9449_meaning_of_revenge_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9449_meaning_of_revenge_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1590 then
		mqc15_9449_meaning_of_revenge_OnTalk_n1590_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1591 then
		mqc15_9449_meaning_of_revenge_OnTalk_n1591_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9449_meaning_of_revenge_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9449, 2);
				api_quest_SetJournalStep(userObjID,9449, 1);
				api_quest_SetQuestStep(userObjID,9449, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-m" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-p" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1590_rambert--------------------------------------------------------------------------------
function mqc15_9449_meaning_of_revenge_OnTalk_n1590_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1590_rambert-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1590_rambert-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1590_rambert-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1590_rambert-3-b" then 
	end
	if npc_talk_index == "n1590_rambert-3-c" then 
	end
	if npc_talk_index == "n1590_rambert-3-d" then 
	end
	if npc_talk_index == "n1590_rambert-3-e" then 
	end
	if npc_talk_index == "n1590_rambert-3-f" then 
	end
	if npc_talk_index == "n1590_rambert-3-g" then 
	end
	if npc_talk_index == "n1590_rambert-3-h" then 
	end
	if npc_talk_index == "n1590_rambert-3-i" then 
	end
	if npc_talk_index == "n1590_rambert-3-j" then 
	end
	if npc_talk_index == "n1590_rambert-3-k" then 
	end
	if npc_talk_index == "n1590_rambert-3-l" then 
	end
	if npc_talk_index == "n1590_rambert-3-m" then 
	end
	if npc_talk_index == "n1590_rambert-3-n" then 
	end
	if npc_talk_index == "n1590_rambert-3-o" then 
	end
	if npc_talk_index == "n1590_rambert-3-p" then 
	end
	if npc_talk_index == "n1590_rambert-3-q" then 
	end
	if npc_talk_index == "n1590_rambert-3-r" then 
	end
	if npc_talk_index == "n1590_rambert-3-s" then 
	end
	if npc_talk_index == "n1590_rambert-3-t" then 
	end
	if npc_talk_index == "n1590_rambert-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1590_rambert-4-b" then 
	end
	if npc_talk_index == "n1590_rambert-4-c" then 
	end
	if npc_talk_index == "n1590_rambert-4-d" then 
	end
	if npc_talk_index == "n1590_rambert-4-e" then 
	end
	if npc_talk_index == "n1590_rambert-4-f" then 
	end
	if npc_talk_index == "n1590_rambert-4-g" then 
	end
	if npc_talk_index == "n1590_rambert-4-h" then 
	end
	if npc_talk_index == "n1590_rambert-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n1590_rambert-4-j" then 
	end
	if npc_talk_index == "n1590_rambert-4-k" then 
	end
	if npc_talk_index == "n1590_rambert-4-l" then 
	end
	if npc_talk_index == "n1590_rambert-4-m" then 
	end
	if npc_talk_index == "n1590_rambert-4-n" then 
	end
	if npc_talk_index == "n1590_rambert-4-o" then 
	end
	if npc_talk_index == "n1590_rambert-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n1590_rambert-4-q" then 
	end
	if npc_talk_index == "n1590_rambert-4-r" then 
	end
	if npc_talk_index == "n1590_rambert-4-s" then 
	end
	if npc_talk_index == "n1590_rambert-4-t" then 
	end
	if npc_talk_index == "n1590_rambert-4-u" then 
	end
	if npc_talk_index == "n1590_rambert-4-v" then 
	end
	if npc_talk_index == "n1590_rambert-4-w" then 
	end
	if npc_talk_index == "n1590_rambert-4-x" then 
	end
	if npc_talk_index == "n1590_rambert-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n1590_rambert-4-z" then 
	end
	if npc_talk_index == "n1590_rambert-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1591_for_memory--------------------------------------------------------------------------------
function mqc15_9449_meaning_of_revenge_OnTalk_n1591_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1591_for_memory-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1591_for_memory-6-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94490, true);
				 api_quest_RewardQuestUser(userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94490, true);
				 api_quest_RewardQuestUser(userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94490, true);
				 api_quest_RewardQuestUser(userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94490, true);
				 api_quest_RewardQuestUser(userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94490, true);
				 api_quest_RewardQuestUser(userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94490, true);
				 api_quest_RewardQuestUser(userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94490, true);
				 api_quest_RewardQuestUser(userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94490, true);
				 api_quest_RewardQuestUser(userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94490, true);
				 api_quest_RewardQuestUser(userObjID, 94490, questID, 1);
			 end 
	end
	if npc_talk_index == "n1591_for_memory-6-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9451, 2);
					api_quest_SetQuestStep(userObjID, 9451, 1);
					api_quest_SetJournalStep(userObjID, 9451, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1591_for_memory-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1067_elder_elf-1", "mqc15_9451_result_of_someone.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9449_meaning_of_revenge_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9449);
end

function mqc15_9449_meaning_of_revenge_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9449);
end

function mqc15_9449_meaning_of_revenge_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9449);
	local questID=9449;
end

function mqc15_9449_meaning_of_revenge_OnRemoteStart( userObjID, questID )
end

function mqc15_9449_meaning_of_revenge_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9449_meaning_of_revenge_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9449, 2);
				api_quest_SetJournalStep(userObjID,9449, 1);
				api_quest_SetQuestStep(userObjID,9449, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</VillageServer>

<GameServer>
function mqc15_9449_meaning_of_revenge_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		mqc15_9449_meaning_of_revenge_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1590 then
		mqc15_9449_meaning_of_revenge_OnTalk_n1590_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1591 then
		mqc15_9449_meaning_of_revenge_OnTalk_n1591_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9449_meaning_of_revenge_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9449, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9449, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9449, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-m" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-p" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1590_rambert--------------------------------------------------------------------------------
function mqc15_9449_meaning_of_revenge_OnTalk_n1590_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1590_rambert-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1590_rambert-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1590_rambert-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1590_rambert-3-b" then 
	end
	if npc_talk_index == "n1590_rambert-3-c" then 
	end
	if npc_talk_index == "n1590_rambert-3-d" then 
	end
	if npc_talk_index == "n1590_rambert-3-e" then 
	end
	if npc_talk_index == "n1590_rambert-3-f" then 
	end
	if npc_talk_index == "n1590_rambert-3-g" then 
	end
	if npc_talk_index == "n1590_rambert-3-h" then 
	end
	if npc_talk_index == "n1590_rambert-3-i" then 
	end
	if npc_talk_index == "n1590_rambert-3-j" then 
	end
	if npc_talk_index == "n1590_rambert-3-k" then 
	end
	if npc_talk_index == "n1590_rambert-3-l" then 
	end
	if npc_talk_index == "n1590_rambert-3-m" then 
	end
	if npc_talk_index == "n1590_rambert-3-n" then 
	end
	if npc_talk_index == "n1590_rambert-3-o" then 
	end
	if npc_talk_index == "n1590_rambert-3-p" then 
	end
	if npc_talk_index == "n1590_rambert-3-q" then 
	end
	if npc_talk_index == "n1590_rambert-3-r" then 
	end
	if npc_talk_index == "n1590_rambert-3-s" then 
	end
	if npc_talk_index == "n1590_rambert-3-t" then 
	end
	if npc_talk_index == "n1590_rambert-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1590_rambert-4-b" then 
	end
	if npc_talk_index == "n1590_rambert-4-c" then 
	end
	if npc_talk_index == "n1590_rambert-4-d" then 
	end
	if npc_talk_index == "n1590_rambert-4-e" then 
	end
	if npc_talk_index == "n1590_rambert-4-f" then 
	end
	if npc_talk_index == "n1590_rambert-4-g" then 
	end
	if npc_talk_index == "n1590_rambert-4-h" then 
	end
	if npc_talk_index == "n1590_rambert-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n1590_rambert-4-j" then 
	end
	if npc_talk_index == "n1590_rambert-4-k" then 
	end
	if npc_talk_index == "n1590_rambert-4-l" then 
	end
	if npc_talk_index == "n1590_rambert-4-m" then 
	end
	if npc_talk_index == "n1590_rambert-4-n" then 
	end
	if npc_talk_index == "n1590_rambert-4-o" then 
	end
	if npc_talk_index == "n1590_rambert-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n1590_rambert-4-q" then 
	end
	if npc_talk_index == "n1590_rambert-4-r" then 
	end
	if npc_talk_index == "n1590_rambert-4-s" then 
	end
	if npc_talk_index == "n1590_rambert-4-t" then 
	end
	if npc_talk_index == "n1590_rambert-4-u" then 
	end
	if npc_talk_index == "n1590_rambert-4-v" then 
	end
	if npc_talk_index == "n1590_rambert-4-w" then 
	end
	if npc_talk_index == "n1590_rambert-4-x" then 
	end
	if npc_talk_index == "n1590_rambert-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n1590_rambert-4-z" then 
	end
	if npc_talk_index == "n1590_rambert-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1591_for_memory--------------------------------------------------------------------------------
function mqc15_9449_meaning_of_revenge_OnTalk_n1591_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1591_for_memory-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1591_for_memory-6-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94490, questID, 1);
			 end 
	end
	if npc_talk_index == "n1591_for_memory-6-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9451, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9451, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9451, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1591_for_memory-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1067_elder_elf-1", "mqc15_9451_result_of_someone.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9449_meaning_of_revenge_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9449);
end

function mqc15_9449_meaning_of_revenge_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9449);
end

function mqc15_9449_meaning_of_revenge_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9449);
	local questID=9449;
end

function mqc15_9449_meaning_of_revenge_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9449_meaning_of_revenge_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9449_meaning_of_revenge_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9449, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9449, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9449, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</GameServer>