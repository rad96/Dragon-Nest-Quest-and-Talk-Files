<VillageServer>

function mqc17_9716_target_of_disaster_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1823 then
		mqc17_9716_target_of_disaster_OnTalk_n1823_merca_fargeau(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1827 then
		mqc17_9716_target_of_disaster_OnTalk_n1827_tryan_lapis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1817 then
		mqc17_9716_target_of_disaster_OnTalk_n1817_merca_hiver(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1823_merca_fargeau--------------------------------------------------------------------------------
function mqc17_9716_target_of_disaster_OnTalk_n1823_merca_fargeau(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1823_merca_fargeau-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1823_merca_fargeau-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1823_merca_fargeau-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1823_merca_fargeau-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9716, 2);
				api_quest_SetJournalStep(userObjID,9716, 1);
				api_quest_SetQuestStep(userObjID,9716, 1);
				npc_talk_index = "n1823_merca_fargeau-1";

	end
	if npc_talk_index == "n1823_merca_fargeau-1-b" then 
	end
	if npc_talk_index == "n1823_merca_fargeau-1-c" then 
	end
	if npc_talk_index == "n1823_merca_fargeau-1-d" then 
	end
	if npc_talk_index == "n1823_merca_fargeau-1-e" then 
	end
	if npc_talk_index == "n1823_merca_fargeau-1-f" then 
	end
	if npc_talk_index == "n1823_merca_fargeau-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1827_tryan_lapis--------------------------------------------------------------------------------
function mqc17_9716_target_of_disaster_OnTalk_n1827_tryan_lapis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1827_tryan_lapis-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1827_tryan_lapis-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1827_tryan_lapis-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1827_tryan_lapis-2-b" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-c" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-d" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-e" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-f" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-g" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-h" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-i" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-j" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-k" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-l" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-m" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-n" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-o" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-p" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1817_merca_hiver--------------------------------------------------------------------------------
function mqc17_9716_target_of_disaster_OnTalk_n1817_merca_hiver(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1817_merca_hiver-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1817_merca_hiver-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1817_merca_hiver-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97160, true);
				 api_quest_RewardQuestUser(userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97160, true);
				 api_quest_RewardQuestUser(userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97160, true);
				 api_quest_RewardQuestUser(userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97160, true);
				 api_quest_RewardQuestUser(userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97160, true);
				 api_quest_RewardQuestUser(userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97160, true);
				 api_quest_RewardQuestUser(userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97160, true);
				 api_quest_RewardQuestUser(userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97160, true);
				 api_quest_RewardQuestUser(userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97160, true);
				 api_quest_RewardQuestUser(userObjID, 97160, questID, 1);
			 end 
	end
	if npc_talk_index == "n1817_merca_hiver-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9717, 2);
					api_quest_SetQuestStep(userObjID, 9717, 1);
					api_quest_SetJournalStep(userObjID, 9717, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1817_merca_hiver-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1817_merca_hiver-1", "mqc17_9717_resolution_of_girl.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9716_target_of_disaster_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9716);
end

function mqc17_9716_target_of_disaster_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9716);
end

function mqc17_9716_target_of_disaster_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9716);
	local questID=9716;
end

function mqc17_9716_target_of_disaster_OnRemoteStart( userObjID, questID )
end

function mqc17_9716_target_of_disaster_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc17_9716_target_of_disaster_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9716, 2);
				api_quest_SetJournalStep(userObjID,9716, 1);
				api_quest_SetQuestStep(userObjID,9716, 1);
				npc_talk_index = "n1823_merca_fargeau-1";
end

</VillageServer>

<GameServer>
function mqc17_9716_target_of_disaster_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1823 then
		mqc17_9716_target_of_disaster_OnTalk_n1823_merca_fargeau( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1827 then
		mqc17_9716_target_of_disaster_OnTalk_n1827_tryan_lapis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1817 then
		mqc17_9716_target_of_disaster_OnTalk_n1817_merca_hiver( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1823_merca_fargeau--------------------------------------------------------------------------------
function mqc17_9716_target_of_disaster_OnTalk_n1823_merca_fargeau( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1823_merca_fargeau-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1823_merca_fargeau-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1823_merca_fargeau-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1823_merca_fargeau-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9716, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9716, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9716, 1);
				npc_talk_index = "n1823_merca_fargeau-1";

	end
	if npc_talk_index == "n1823_merca_fargeau-1-b" then 
	end
	if npc_talk_index == "n1823_merca_fargeau-1-c" then 
	end
	if npc_talk_index == "n1823_merca_fargeau-1-d" then 
	end
	if npc_talk_index == "n1823_merca_fargeau-1-e" then 
	end
	if npc_talk_index == "n1823_merca_fargeau-1-f" then 
	end
	if npc_talk_index == "n1823_merca_fargeau-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1827_tryan_lapis--------------------------------------------------------------------------------
function mqc17_9716_target_of_disaster_OnTalk_n1827_tryan_lapis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1827_tryan_lapis-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1827_tryan_lapis-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1827_tryan_lapis-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1827_tryan_lapis-2-b" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-c" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-d" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-e" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-f" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-g" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-h" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-i" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-j" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-k" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-l" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-m" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-n" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-o" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-2-p" then 
	end
	if npc_talk_index == "n1827_tryan_lapis-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1817_merca_hiver--------------------------------------------------------------------------------
function mqc17_9716_target_of_disaster_OnTalk_n1817_merca_hiver( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1817_merca_hiver-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1817_merca_hiver-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1817_merca_hiver-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97160, questID, 1);
			 end 
	end
	if npc_talk_index == "n1817_merca_hiver-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9717, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9717, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9717, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1817_merca_hiver-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1817_merca_hiver-1", "mqc17_9717_resolution_of_girl.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9716_target_of_disaster_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9716);
end

function mqc17_9716_target_of_disaster_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9716);
end

function mqc17_9716_target_of_disaster_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9716);
	local questID=9716;
end

function mqc17_9716_target_of_disaster_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc17_9716_target_of_disaster_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc17_9716_target_of_disaster_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9716, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9716, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9716, 1);
				npc_talk_index = "n1823_merca_fargeau-1";
end

</GameServer>