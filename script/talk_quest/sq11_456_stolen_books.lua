<VillageServer>

function sq11_456_stolen_books_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 42 then
		sq11_456_stolen_books_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function sq11_456_stolen_books_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4561, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4562, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4563, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4564, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4565, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4566, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4567, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4568, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4569, false);
			 end 

	end
	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest(userObjID,456, 1);
				api_quest_SetJournalStep(userObjID,456, 1);
				api_quest_SetQuestStep(userObjID,456, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 871, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200871, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 863, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200863, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 862, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200862, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 3, 400098, 2);
				api_quest_SetCountingInfo(userObjID, questID, 7, 3, 400099, 2);
				api_quest_SetCountingInfo(userObjID, questID, 8, 3, 400100, 2);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n042_general_duglars-3-b" then 
	end
	if npc_talk_index == "n042_general_duglars-3-c" then 
	end
	if npc_talk_index == "n042_general_duglars-3-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4561, true);
				 api_quest_RewardQuestUser(userObjID, 4561, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4562, true);
				 api_quest_RewardQuestUser(userObjID, 4562, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4563, true);
				 api_quest_RewardQuestUser(userObjID, 4563, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4564, true);
				 api_quest_RewardQuestUser(userObjID, 4564, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4565, true);
				 api_quest_RewardQuestUser(userObjID, 4565, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4566, true);
				 api_quest_RewardQuestUser(userObjID, 4566, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4567, true);
				 api_quest_RewardQuestUser(userObjID, 4567, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4568, true);
				 api_quest_RewardQuestUser(userObjID, 4568, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4569, true);
				 api_quest_RewardQuestUser(userObjID, 4569, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-3-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400098, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400098, api_quest_HasQuestItem(userObjID, 400098, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400099, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400099, api_quest_HasQuestItem(userObjID, 400099, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400100, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400100, api_quest_HasQuestItem(userObjID, 400100, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400101, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400101, api_quest_HasQuestItem(userObjID, 400101, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400102, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400102, api_quest_HasQuestItem(userObjID, 400102, 1));
				end
	end
	if npc_talk_index == "n042_general_duglars-3-f" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_456_stolen_books_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 456);
	if qstep == 2 and CountIndex == 871 then
				if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2 then
									if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400098, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400098, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200871 then
				if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2 then
									if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400098, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400098, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 863 then
				if api_quest_HasQuestItem(userObjID, 400099, 2) >= 2 then
									if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400099, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400099, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200863 then
				if api_quest_HasQuestItem(userObjID, 400099, 2) >= 2 then
									if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400099, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400099, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 862 then
				if api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400100, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400100, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200862 then
				if api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400100, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400100, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem(userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 400098 then

	end
	if qstep == 2 and CountIndex == 400099 then

	end
	if qstep == 2 and CountIndex == 400100 then

	end
end

function sq11_456_stolen_books_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 456);
	if qstep == 2 and CountIndex == 871 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200871 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 863 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200863 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 862 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200862 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400098 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400099 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400100 and Count >= TargetCount  then

	end
end

function sq11_456_stolen_books_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 456);
	local questID=456;
end

function sq11_456_stolen_books_OnRemoteStart( userObjID, questID )
end

function sq11_456_stolen_books_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_456_stolen_books_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,456, 1);
				api_quest_SetJournalStep(userObjID,456, 1);
				api_quest_SetQuestStep(userObjID,456, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</VillageServer>

<GameServer>
function sq11_456_stolen_books_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 42 then
		sq11_456_stolen_books_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function sq11_456_stolen_books_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n042_general_duglars-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4561, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4562, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4563, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4564, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4565, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4566, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4567, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4568, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4569, false);
			 end 

	end
	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,456, 1);
				api_quest_SetJournalStep( pRoom, userObjID,456, 1);
				api_quest_SetQuestStep( pRoom, userObjID,456, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 871, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200871, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 863, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200863, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 862, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200862, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 3, 400098, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 3, 400099, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 3, 400100, 2);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n042_general_duglars-3-b" then 
	end
	if npc_talk_index == "n042_general_duglars-3-c" then 
	end
	if npc_talk_index == "n042_general_duglars-3-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4561, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4561, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4562, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4562, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4563, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4563, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4564, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4564, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4565, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4565, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4566, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4566, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4567, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4567, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4568, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4568, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4569, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4569, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-3-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400098, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400098, api_quest_HasQuestItem( pRoom, userObjID, 400098, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400099, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400099, api_quest_HasQuestItem( pRoom, userObjID, 400099, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400100, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400100, api_quest_HasQuestItem( pRoom, userObjID, 400100, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400101, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400101, api_quest_HasQuestItem( pRoom, userObjID, 400101, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400102, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400102, api_quest_HasQuestItem( pRoom, userObjID, 400102, 1));
				end
	end
	if npc_talk_index == "n042_general_duglars-3-f" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_456_stolen_books_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 456);
	if qstep == 2 and CountIndex == 871 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400098, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400098, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200871 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400098, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400098, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 863 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400099, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400099, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200863 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400099, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400099, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 862 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400100, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400100, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200862 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400100, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400100, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400098, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400099, 2) >= 2  and api_quest_HasQuestItem( pRoom, userObjID, 400100, 2) >= 2 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 400098 then

	end
	if qstep == 2 and CountIndex == 400099 then

	end
	if qstep == 2 and CountIndex == 400100 then

	end
end

function sq11_456_stolen_books_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 456);
	if qstep == 2 and CountIndex == 871 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200871 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 863 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200863 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 862 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200862 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400098 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400099 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400100 and Count >= TargetCount  then

	end
end

function sq11_456_stolen_books_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 456);
	local questID=456;
end

function sq11_456_stolen_books_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_456_stolen_books_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_456_stolen_books_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,456, 1);
				api_quest_SetJournalStep( pRoom, userObjID,456, 1);
				api_quest_SetQuestStep( pRoom, userObjID,456, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</GameServer>