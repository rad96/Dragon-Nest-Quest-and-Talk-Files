<VillageServer>

function mqc05_9611_despair_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1861 then
		mqc05_9611_despair_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mqc05_9611_despair_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1894 then
		mqc05_9611_despair_OnTalk_n1894_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1895 then
		mqc05_9611_despair_OnTalk_n1895_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1861_shaolong--------------------------------------------------------------------------------
function mqc05_9611_despair_OnTalk_n1861_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1861_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1861_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1861_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1861_shaolong-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1861_shaolong-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9611, 2);
				api_quest_SetJournalStep(userObjID,9611, 1);
				api_quest_SetQuestStep(userObjID,9611, 1);
				npc_talk_index = "n1861_shaolong-1";

	end
	if npc_talk_index == "n1861_shaolong-1-b" then 
	end
	if npc_talk_index == "n1861_shaolong-1-c" then 
	end
	if npc_talk_index == "n1861_shaolong-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1861_shaolong-5-b" then 
	end
	if npc_talk_index == "n1861_shaolong-5-c" then 
	end
	if npc_talk_index == "n1861_shaolong-5-d" then 
	end
	if npc_talk_index == "n1861_shaolong-5-e" then 
	end
	if npc_talk_index == "n1861_shaolong-5-f" then 
	end
	if npc_talk_index == "n1861_shaolong-5-g" then 
	end
	if npc_talk_index == "n1861_shaolong-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9611_despair_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n039_bishop_ignasio-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n039_bishop_ignasio-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-2-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n039_bishop_ignasio-6-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 96110, true);
				 api_quest_RewardQuestUser(userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 96110, true);
				 api_quest_RewardQuestUser(userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 96110, true);
				 api_quest_RewardQuestUser(userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 96110, true);
				 api_quest_RewardQuestUser(userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 96110, true);
				 api_quest_RewardQuestUser(userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 96110, true);
				 api_quest_RewardQuestUser(userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 96110, true);
				 api_quest_RewardQuestUser(userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 96110, true);
				 api_quest_RewardQuestUser(userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 96110, true);
				 api_quest_RewardQuestUser(userObjID, 96110, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-6-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9612, 2);
					api_quest_SetQuestStep(userObjID, 9612, 1);
					api_quest_SetJournalStep(userObjID, 9612, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n039_bishop_ignasio-1", "mqc05_9612_crossroads.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1894_light_pole--------------------------------------------------------------------------------
function mqc05_9611_despair_OnTalk_n1894_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1894_light_pole-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1894_light_pole-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1894_light_pole-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1894_light_pole-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1895_suriya--------------------------------------------------------------------------------
function mqc05_9611_despair_OnTalk_n1895_suriya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1895_suriya-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1895_suriya-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1895_suriya-4-b" then 
	end
	if npc_talk_index == "n1895_suriya-4-c" then 
	end
	if npc_talk_index == "n1895_suriya-4-d" then 
	end
	if npc_talk_index == "n1895_suriya-4-e" then 
	end
	if npc_talk_index == "n1895_suriya-4-f" then 
	end
	if npc_talk_index == "n1895_suriya-4-g" then 
	end
	if npc_talk_index == "n1895_suriya-4-h" then 
	end
	if npc_talk_index == "n1895_suriya-4-i" then 
	end
	if npc_talk_index == "n1895_suriya-4-j" then 
	end
	if npc_talk_index == "n1895_suriya-4-k" then 
	end
	if npc_talk_index == "n1895_suriya-4-l" then 
	end
	if npc_talk_index == "n1895_suriya-4-m" then 
	end
	if npc_talk_index == "n1895_suriya-4-n" then 
	end
	if npc_talk_index == "n1895_suriya-4-o" then 
	end
	if npc_talk_index == "n1895_suriya-4-p" then 
	end
	if npc_talk_index == "n1895_suriya-4-q" then 
	end
	if npc_talk_index == "n1895_suriya-4-r" then 
	end
	if npc_talk_index == "n1895_suriya-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9611_despair_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9611);
end

function mqc05_9611_despair_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9611);
end

function mqc05_9611_despair_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9611);
	local questID=9611;
end

function mqc05_9611_despair_OnRemoteStart( userObjID, questID )
end

function mqc05_9611_despair_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc05_9611_despair_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9611, 2);
				api_quest_SetJournalStep(userObjID,9611, 1);
				api_quest_SetQuestStep(userObjID,9611, 1);
				npc_talk_index = "n1861_shaolong-1";
end

</VillageServer>

<GameServer>
function mqc05_9611_despair_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1861 then
		mqc05_9611_despair_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mqc05_9611_despair_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1894 then
		mqc05_9611_despair_OnTalk_n1894_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1895 then
		mqc05_9611_despair_OnTalk_n1895_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1861_shaolong--------------------------------------------------------------------------------
function mqc05_9611_despair_OnTalk_n1861_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1861_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1861_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1861_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1861_shaolong-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1861_shaolong-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1861_shaolong-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9611, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9611, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9611, 1);
				npc_talk_index = "n1861_shaolong-1";

	end
	if npc_talk_index == "n1861_shaolong-1-b" then 
	end
	if npc_talk_index == "n1861_shaolong-1-c" then 
	end
	if npc_talk_index == "n1861_shaolong-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1861_shaolong-5-b" then 
	end
	if npc_talk_index == "n1861_shaolong-5-c" then 
	end
	if npc_talk_index == "n1861_shaolong-5-d" then 
	end
	if npc_talk_index == "n1861_shaolong-5-e" then 
	end
	if npc_talk_index == "n1861_shaolong-5-f" then 
	end
	if npc_talk_index == "n1861_shaolong-5-g" then 
	end
	if npc_talk_index == "n1861_shaolong-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9611_despair_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n039_bishop_ignasio-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n039_bishop_ignasio-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-2-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n039_bishop_ignasio-6-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96110, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-6-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9612, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9612, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9612, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-1", "mqc05_9612_crossroads.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1894_light_pole--------------------------------------------------------------------------------
function mqc05_9611_despair_OnTalk_n1894_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1894_light_pole-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1894_light_pole-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1894_light_pole-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1894_light_pole-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1895_suriya--------------------------------------------------------------------------------
function mqc05_9611_despair_OnTalk_n1895_suriya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1895_suriya-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1895_suriya-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1895_suriya-4-b" then 
	end
	if npc_talk_index == "n1895_suriya-4-c" then 
	end
	if npc_talk_index == "n1895_suriya-4-d" then 
	end
	if npc_talk_index == "n1895_suriya-4-e" then 
	end
	if npc_talk_index == "n1895_suriya-4-f" then 
	end
	if npc_talk_index == "n1895_suriya-4-g" then 
	end
	if npc_talk_index == "n1895_suriya-4-h" then 
	end
	if npc_talk_index == "n1895_suriya-4-i" then 
	end
	if npc_talk_index == "n1895_suriya-4-j" then 
	end
	if npc_talk_index == "n1895_suriya-4-k" then 
	end
	if npc_talk_index == "n1895_suriya-4-l" then 
	end
	if npc_talk_index == "n1895_suriya-4-m" then 
	end
	if npc_talk_index == "n1895_suriya-4-n" then 
	end
	if npc_talk_index == "n1895_suriya-4-o" then 
	end
	if npc_talk_index == "n1895_suriya-4-p" then 
	end
	if npc_talk_index == "n1895_suriya-4-q" then 
	end
	if npc_talk_index == "n1895_suriya-4-r" then 
	end
	if npc_talk_index == "n1895_suriya-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9611_despair_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9611);
end

function mqc05_9611_despair_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9611);
end

function mqc05_9611_despair_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9611);
	local questID=9611;
end

function mqc05_9611_despair_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc05_9611_despair_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc05_9611_despair_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9611, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9611, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9611, 1);
				npc_talk_index = "n1861_shaolong-1";
end

</GameServer>