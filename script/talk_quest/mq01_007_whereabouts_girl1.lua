<VillageServer>

function mq01_007_whereabouts_girl1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 145 then
		mq01_007_whereabouts_girl1_OnTalk_n145_prairie_guard_pesent(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 146 then
		mq01_007_whereabouts_girl1_OnTalk_n146_prairie_guard_pesent(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 4 then
		mq01_007_whereabouts_girl1_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n145_prairie_guard_pesent--------------------------------------------------------------------------------
function mq01_007_whereabouts_girl1_OnTalk_n145_prairie_guard_pesent(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n145_prairie_guard_pesent-1";
				local rcval_1_6 = math.random(1,100);
				if rcval_1_6 >= 1 and rcval_1_6 < 10 then
					npc_talk_index = "n145_prairie_guard_pesent-1";
				end
				if rcval_1_6 >= 10 and rcval_1_6 < 30 then
					npc_talk_index = "n145_prairie_guard_pesent-1-a";
				end
				if rcval_1_6 >= 30 and rcval_1_6 < 60 then
					npc_talk_index = "n145_prairie_guard_pesent-1-b";
				end
				if rcval_1_6 >= 60 and rcval_1_6 < 100 then
					npc_talk_index = "n145_prairie_guard_pesent-1-c";
				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n146_prairie_guard_pesent--------------------------------------------------------------------------------
function mq01_007_whereabouts_girl1_OnTalk_n146_prairie_guard_pesent(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n146_prairie_guard_pesent-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n004_guard_steave--------------------------------------------------------------------------------
function mq01_007_whereabouts_girl1_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n004_guard_steave-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n004_guard_steave-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n004_guard_steave-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n004_guard_steave-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-accepting-acceptted" then
				api_quest_AddQuest(userObjID,7, 2);
				api_quest_SetJournalStep(userObjID,7, 1);
				api_quest_SetQuestStep(userObjID,7, 1);
				npc_talk_index = "n004_guard_steave-1";

	end
	if npc_talk_index == "n004_guard_steave-2" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 148, 30000);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n004_guard_steave-3-b" then 
	end
	if npc_talk_index == "n004_guard_steave-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 71, true);
				 api_quest_RewardQuestUser(userObjID, 71, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 72, true);
				 api_quest_RewardQuestUser(userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 72, true);
				 api_quest_RewardQuestUser(userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 72, true);
				 api_quest_RewardQuestUser(userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 72, true);
				 api_quest_RewardQuestUser(userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 72, true);
				 api_quest_RewardQuestUser(userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 72, true);
				 api_quest_RewardQuestUser(userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 72, true);
				 api_quest_RewardQuestUser(userObjID, 72, questID, 1);
			 end 
	end
	if npc_talk_index == "n004_guard_steave-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 8, 2);
					api_quest_SetQuestStep(userObjID, 8, 1);
					api_quest_SetJournalStep(userObjID, 8, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n004_guard_steave-1", "mq01_008_whereabouts_girl2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_007_whereabouts_girl1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7);
	if qstep == 2 and CountIndex == 148 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function mq01_007_whereabouts_girl1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7);
	if qstep == 2 and CountIndex == 148 and Count >= TargetCount  then

	end
end

function mq01_007_whereabouts_girl1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7);
	local questID=7;
end

function mq01_007_whereabouts_girl1_OnRemoteStart( userObjID, questID )
end

function mq01_007_whereabouts_girl1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq01_007_whereabouts_girl1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,7, 2);
				api_quest_SetJournalStep(userObjID,7, 1);
				api_quest_SetQuestStep(userObjID,7, 1);
				npc_talk_index = "n004_guard_steave-1";
end

</VillageServer>

<GameServer>
function mq01_007_whereabouts_girl1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 145 then
		mq01_007_whereabouts_girl1_OnTalk_n145_prairie_guard_pesent( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 146 then
		mq01_007_whereabouts_girl1_OnTalk_n146_prairie_guard_pesent( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 4 then
		mq01_007_whereabouts_girl1_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n145_prairie_guard_pesent--------------------------------------------------------------------------------
function mq01_007_whereabouts_girl1_OnTalk_n145_prairie_guard_pesent( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n145_prairie_guard_pesent-1";
				local rcval_1_6 = math.random(1,100);
				if rcval_1_6 >= 1 and rcval_1_6 < 10 then
					npc_talk_index = "n145_prairie_guard_pesent-1";
				end
				if rcval_1_6 >= 10 and rcval_1_6 < 30 then
					npc_talk_index = "n145_prairie_guard_pesent-1-a";
				end
				if rcval_1_6 >= 30 and rcval_1_6 < 60 then
					npc_talk_index = "n145_prairie_guard_pesent-1-b";
				end
				if rcval_1_6 >= 60 and rcval_1_6 < 100 then
					npc_talk_index = "n145_prairie_guard_pesent-1-c";
				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n146_prairie_guard_pesent--------------------------------------------------------------------------------
function mq01_007_whereabouts_girl1_OnTalk_n146_prairie_guard_pesent( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n146_prairie_guard_pesent-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n004_guard_steave--------------------------------------------------------------------------------
function mq01_007_whereabouts_girl1_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n004_guard_steave-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n004_guard_steave-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n004_guard_steave-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n004_guard_steave-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,7, 2);
				api_quest_SetJournalStep( pRoom, userObjID,7, 1);
				api_quest_SetQuestStep( pRoom, userObjID,7, 1);
				npc_talk_index = "n004_guard_steave-1";

	end
	if npc_talk_index == "n004_guard_steave-2" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 148, 30000);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n004_guard_steave-3-b" then 
	end
	if npc_talk_index == "n004_guard_steave-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 71, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 71, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 72, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 72, questID, 1);
			 end 
	end
	if npc_talk_index == "n004_guard_steave-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 8, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 8, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 8, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n004_guard_steave-1", "mq01_008_whereabouts_girl2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_007_whereabouts_girl1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7);
	if qstep == 2 and CountIndex == 148 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function mq01_007_whereabouts_girl1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7);
	if qstep == 2 and CountIndex == 148 and Count >= TargetCount  then

	end
end

function mq01_007_whereabouts_girl1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7);
	local questID=7;
end

function mq01_007_whereabouts_girl1_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq01_007_whereabouts_girl1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq01_007_whereabouts_girl1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,7, 2);
				api_quest_SetJournalStep( pRoom, userObjID,7, 1);
				api_quest_SetQuestStep( pRoom, userObjID,7, 1);
				npc_talk_index = "n004_guard_steave-1";
end

</GameServer>