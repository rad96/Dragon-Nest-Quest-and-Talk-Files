<VillageServer>

function sq11_524_effective_attack_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 36 then
		sq11_524_effective_attack_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_524_effective_attack_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5241, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5242, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5243, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5244, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5245, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5246, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5247, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5248, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5249, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest(userObjID,524, 1);
				api_quest_SetJournalStep(userObjID,524, 1);
				api_quest_SetQuestStep(userObjID,524, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1127, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201127, 30000);

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5241, true);
				 api_quest_RewardQuestUser(userObjID, 5241, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5242, true);
				 api_quest_RewardQuestUser(userObjID, 5242, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5243, true);
				 api_quest_RewardQuestUser(userObjID, 5243, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5244, true);
				 api_quest_RewardQuestUser(userObjID, 5244, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5245, true);
				 api_quest_RewardQuestUser(userObjID, 5245, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5246, true);
				 api_quest_RewardQuestUser(userObjID, 5246, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5247, true);
				 api_quest_RewardQuestUser(userObjID, 5247, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5248, true);
				 api_quest_RewardQuestUser(userObjID, 5248, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5249, true);
				 api_quest_RewardQuestUser(userObjID, 5249, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_524_effective_attack_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 524);
	if qstep == 1 and CountIndex == 1127 then

	end
	if qstep == 1 and CountIndex == 201127 then
				api_quest_IncCounting(userObjID, 2, 1127);

	end
end

function sq11_524_effective_attack_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 524);
	if qstep == 1 and CountIndex == 1127 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 201127 and Count >= TargetCount  then

	end
end

function sq11_524_effective_attack_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 524);
	local questID=524;
end

function sq11_524_effective_attack_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1127, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201127, 30000);
end

function sq11_524_effective_attack_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_524_effective_attack_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,524, 1);
				api_quest_SetJournalStep(userObjID,524, 1);
				api_quest_SetQuestStep(userObjID,524, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1127, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201127, 30000);
end

</VillageServer>

<GameServer>
function sq11_524_effective_attack_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 36 then
		sq11_524_effective_attack_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_524_effective_attack_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5241, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5242, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5243, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5244, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5245, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5246, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5247, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5248, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5249, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,524, 1);
				api_quest_SetJournalStep( pRoom, userObjID,524, 1);
				api_quest_SetQuestStep( pRoom, userObjID,524, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1127, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201127, 30000);

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5241, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5241, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5242, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5242, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5243, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5243, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5244, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5244, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5245, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5245, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5246, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5246, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5247, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5247, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5248, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5248, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5249, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5249, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_524_effective_attack_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 524);
	if qstep == 1 and CountIndex == 1127 then

	end
	if qstep == 1 and CountIndex == 201127 then
				api_quest_IncCounting( pRoom, userObjID, 2, 1127);

	end
end

function sq11_524_effective_attack_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 524);
	if qstep == 1 and CountIndex == 1127 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 201127 and Count >= TargetCount  then

	end
end

function sq11_524_effective_attack_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 524);
	local questID=524;
end

function sq11_524_effective_attack_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1127, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201127, 30000);
end

function sq11_524_effective_attack_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_524_effective_attack_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,524, 1);
				api_quest_SetJournalStep( pRoom, userObjID,524, 1);
				api_quest_SetQuestStep( pRoom, userObjID,524, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1127, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201127, 30000);
end

</GameServer>