<VillageServer>

function mq01_032_befogged_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1 then
		mq01_032_befogged_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1092 then
		mq01_032_befogged_OnTalk_n1092_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1093 then
		mq01_032_befogged_OnTalk_n1093_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 572 then
		mq01_032_befogged_OnTalk_n572_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mq01_032_befogged_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n001_elder_harold-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n001_elder_harold-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n001_elder_harold-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n001_elder_harold-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n001_elder_harold-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n001_elder_harold-accepting-acceptted" then
				api_quest_AddQuest(userObjID,32, 2);
				api_quest_SetJournalStep(userObjID,32, 1);
				api_quest_SetQuestStep(userObjID,32, 1);
				npc_talk_index = "n001_elder_harold-1";

	end
	if npc_talk_index == "n001_elder_harold-1-b" then 
	end
	if npc_talk_index == "n001_elder_harold-1-c" then 
	end
	if npc_talk_index == "n001_elder_harold-1-d" then 
	end
	if npc_talk_index == "n001_elder_harold-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n001_elder_harold-3-b" then 
	end
	if npc_talk_index == "n001_elder_harold-3-b" then 
	end
	if npc_talk_index == "n001_elder_harold-3-c" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n001_elder_harold-7-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 320, true);
				 api_quest_RewardQuestUser(userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 320, true);
				 api_quest_RewardQuestUser(userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 320, true);
				 api_quest_RewardQuestUser(userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 320, true);
				 api_quest_RewardQuestUser(userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 320, true);
				 api_quest_RewardQuestUser(userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 320, true);
				 api_quest_RewardQuestUser(userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 320, true);
				 api_quest_RewardQuestUser(userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 320, true);
				 api_quest_RewardQuestUser(userObjID, 320, questID, 1);
			 end 
	end
	if npc_talk_index == "n001_elder_harold-7-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 33, 2);
					api_quest_SetQuestStep(userObjID, 33, 1);
					api_quest_SetJournalStep(userObjID, 33, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300009, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300009, api_quest_HasQuestItem(userObjID, 300009, 1));
				end
	end
	if npc_talk_index == "n001_elder_harold-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n001_elder_harold-1", "mq01_033_groping_in_the_dark.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1092_lunaria--------------------------------------------------------------------------------
function mq01_032_befogged_OnTalk_n1092_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1092_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1092_lunaria-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1092_lunaria-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1092_lunaria-4-b" then 
	end
	if npc_talk_index == "n1092_lunaria-4-c" then 
	end
	if npc_talk_index == "n1092_lunaria-4-d" then 
	end
	if npc_talk_index == "n1092_lunaria-4-e" then 
	end
	if npc_talk_index == "n1092_lunaria-4-f" then 
	end
	if npc_talk_index == "n1092_lunaria-4-f" then 
	end
	if npc_talk_index == "n1092_lunaria-4-g" then 
	end
	if npc_talk_index == "n1092_lunaria-4-h" then 
	end
	if npc_talk_index == "n1092_lunaria-4-i" then 
	end
	if npc_talk_index == "n1092_lunaria-4-j" then 
	end
	if npc_talk_index == "n1092_lunaria-4-k" then 
	end
	if npc_talk_index == "n1092_lunaria-4-l" then 
	end
	if npc_talk_index == "n1092_lunaria-4-m" then 
	end
	if npc_talk_index == "n1092_lunaria-4-n" then 
	end
	if npc_talk_index == "n1092_lunaria-4-o" then 
	end
	if npc_talk_index == "n1092_lunaria-4-p" then 
	end
	if npc_talk_index == "n1092_lunaria-4-q" then 
	end
	if npc_talk_index == "n1092_lunaria-4-r" then 
	end
	if npc_talk_index == "n1092_lunaria-4-s" then 
	end
	if npc_talk_index == "n1092_lunaria-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 200129, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 129, 30000);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1093_lunaria--------------------------------------------------------------------------------
function mq01_032_befogged_OnTalk_n1093_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1093_lunaria-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1093_lunaria-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1093_lunaria-6-b" then 
	end
	if npc_talk_index == "n1093_lunaria-6-c" then 
	end
	if npc_talk_index == "n1093_lunaria-6-d" then 
	end
	if npc_talk_index == "n1093_lunaria-6-e" then 
	end
	if npc_talk_index == "n1093_lunaria-6-f" then 
	end
	if npc_talk_index == "n1093_lunaria-6-g" then 
	end
	if npc_talk_index == "n1093_lunaria-6-h" then 
	end
	if npc_talk_index == "n1093_lunaria-6-i" then 
	end
	if npc_talk_index == "n1093_lunaria-6-j" then 
	end
	if npc_talk_index == "n1093_lunaria-6-k" then 
	end
	if npc_talk_index == "n1093_lunaria-6-l" then 
	end
	if npc_talk_index == "n1093_lunaria-6-m" then 
	end
	if npc_talk_index == "n1093_lunaria-6-n" then 
	end
	if npc_talk_index == "n1093_lunaria-6-o" then 
	end
	if npc_talk_index == "n1093_lunaria-6-p" then 
	end
	if npc_talk_index == "n1093_lunaria-6-q" then 
	end
	if npc_talk_index == "n1093_lunaria-6-r" then 
	end
	if npc_talk_index == "n1093_lunaria-6-s" then 
	end
	if npc_talk_index == "n1093_lunaria-6-t" then 
	end
	if npc_talk_index == "n1093_lunaria-6-u" then 
	end
	if npc_talk_index == "n1093_lunaria-6-v" then 
	end
	if npc_talk_index == "n1093_lunaria-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n572_shadow_meow--------------------------------------------------------------------------------
function mq01_032_befogged_OnTalk_n572_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n572_shadow_meow-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n572_shadow_meow-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n572_shadow_meow-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n572_shadow_meow-2-b" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-c" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-d" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-e" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-f" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-g" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-h" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-i" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-j" then 
	end
	if npc_talk_index == "n572_shadow_meow-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_032_befogged_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 32);
	if qstep == 5 and CountIndex == 200129 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300009, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300009, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);

	end
	if qstep == 5 and CountIndex == 129 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300009, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300009, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);

	end
end

function mq01_032_befogged_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 32);
	if qstep == 5 and CountIndex == 200129 and Count >= TargetCount  then

	end
	if qstep == 5 and CountIndex == 129 and Count >= TargetCount  then

	end
end

function mq01_032_befogged_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 32);
	local questID=32;
end

function mq01_032_befogged_OnRemoteStart( userObjID, questID )
end

function mq01_032_befogged_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq01_032_befogged_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,32, 2);
				api_quest_SetJournalStep(userObjID,32, 1);
				api_quest_SetQuestStep(userObjID,32, 1);
				npc_talk_index = "n001_elder_harold-1";
end

</VillageServer>

<GameServer>
function mq01_032_befogged_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1 then
		mq01_032_befogged_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1092 then
		mq01_032_befogged_OnTalk_n1092_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1093 then
		mq01_032_befogged_OnTalk_n1093_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 572 then
		mq01_032_befogged_OnTalk_n572_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mq01_032_befogged_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n001_elder_harold-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n001_elder_harold-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n001_elder_harold-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n001_elder_harold-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n001_elder_harold-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n001_elder_harold-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,32, 2);
				api_quest_SetJournalStep( pRoom, userObjID,32, 1);
				api_quest_SetQuestStep( pRoom, userObjID,32, 1);
				npc_talk_index = "n001_elder_harold-1";

	end
	if npc_talk_index == "n001_elder_harold-1-b" then 
	end
	if npc_talk_index == "n001_elder_harold-1-c" then 
	end
	if npc_talk_index == "n001_elder_harold-1-d" then 
	end
	if npc_talk_index == "n001_elder_harold-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n001_elder_harold-3-b" then 
	end
	if npc_talk_index == "n001_elder_harold-3-b" then 
	end
	if npc_talk_index == "n001_elder_harold-3-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n001_elder_harold-7-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 320, questID, 1);
			 end 
	end
	if npc_talk_index == "n001_elder_harold-7-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 33, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 33, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 33, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300009, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300009, api_quest_HasQuestItem( pRoom, userObjID, 300009, 1));
				end
	end
	if npc_talk_index == "n001_elder_harold-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n001_elder_harold-1", "mq01_033_groping_in_the_dark.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1092_lunaria--------------------------------------------------------------------------------
function mq01_032_befogged_OnTalk_n1092_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1092_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1092_lunaria-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1092_lunaria-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1092_lunaria-4-b" then 
	end
	if npc_talk_index == "n1092_lunaria-4-c" then 
	end
	if npc_talk_index == "n1092_lunaria-4-d" then 
	end
	if npc_talk_index == "n1092_lunaria-4-e" then 
	end
	if npc_talk_index == "n1092_lunaria-4-f" then 
	end
	if npc_talk_index == "n1092_lunaria-4-f" then 
	end
	if npc_talk_index == "n1092_lunaria-4-g" then 
	end
	if npc_talk_index == "n1092_lunaria-4-h" then 
	end
	if npc_talk_index == "n1092_lunaria-4-i" then 
	end
	if npc_talk_index == "n1092_lunaria-4-j" then 
	end
	if npc_talk_index == "n1092_lunaria-4-k" then 
	end
	if npc_talk_index == "n1092_lunaria-4-l" then 
	end
	if npc_talk_index == "n1092_lunaria-4-m" then 
	end
	if npc_talk_index == "n1092_lunaria-4-n" then 
	end
	if npc_talk_index == "n1092_lunaria-4-o" then 
	end
	if npc_talk_index == "n1092_lunaria-4-p" then 
	end
	if npc_talk_index == "n1092_lunaria-4-q" then 
	end
	if npc_talk_index == "n1092_lunaria-4-r" then 
	end
	if npc_talk_index == "n1092_lunaria-4-s" then 
	end
	if npc_talk_index == "n1092_lunaria-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 200129, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 129, 30000);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1093_lunaria--------------------------------------------------------------------------------
function mq01_032_befogged_OnTalk_n1093_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1093_lunaria-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1093_lunaria-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1093_lunaria-6-b" then 
	end
	if npc_talk_index == "n1093_lunaria-6-c" then 
	end
	if npc_talk_index == "n1093_lunaria-6-d" then 
	end
	if npc_talk_index == "n1093_lunaria-6-e" then 
	end
	if npc_talk_index == "n1093_lunaria-6-f" then 
	end
	if npc_talk_index == "n1093_lunaria-6-g" then 
	end
	if npc_talk_index == "n1093_lunaria-6-h" then 
	end
	if npc_talk_index == "n1093_lunaria-6-i" then 
	end
	if npc_talk_index == "n1093_lunaria-6-j" then 
	end
	if npc_talk_index == "n1093_lunaria-6-k" then 
	end
	if npc_talk_index == "n1093_lunaria-6-l" then 
	end
	if npc_talk_index == "n1093_lunaria-6-m" then 
	end
	if npc_talk_index == "n1093_lunaria-6-n" then 
	end
	if npc_talk_index == "n1093_lunaria-6-o" then 
	end
	if npc_talk_index == "n1093_lunaria-6-p" then 
	end
	if npc_talk_index == "n1093_lunaria-6-q" then 
	end
	if npc_talk_index == "n1093_lunaria-6-r" then 
	end
	if npc_talk_index == "n1093_lunaria-6-s" then 
	end
	if npc_talk_index == "n1093_lunaria-6-t" then 
	end
	if npc_talk_index == "n1093_lunaria-6-u" then 
	end
	if npc_talk_index == "n1093_lunaria-6-v" then 
	end
	if npc_talk_index == "n1093_lunaria-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n572_shadow_meow--------------------------------------------------------------------------------
function mq01_032_befogged_OnTalk_n572_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n572_shadow_meow-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n572_shadow_meow-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n572_shadow_meow-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n572_shadow_meow-2-b" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-c" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-d" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-e" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-f" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-g" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-h" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-i" then 
	end
	if npc_talk_index == "n572_shadow_meow-2-j" then 
	end
	if npc_talk_index == "n572_shadow_meow-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_032_befogged_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 32);
	if qstep == 5 and CountIndex == 200129 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300009, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300009, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);

	end
	if qstep == 5 and CountIndex == 129 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300009, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300009, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);

	end
end

function mq01_032_befogged_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 32);
	if qstep == 5 and CountIndex == 200129 and Count >= TargetCount  then

	end
	if qstep == 5 and CountIndex == 129 and Count >= TargetCount  then

	end
end

function mq01_032_befogged_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 32);
	local questID=32;
end

function mq01_032_befogged_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq01_032_befogged_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq01_032_befogged_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,32, 2);
				api_quest_SetJournalStep( pRoom, userObjID,32, 1);
				api_quest_SetQuestStep( pRoom, userObjID,32, 1);
				npc_talk_index = "n001_elder_harold-1";
end

</GameServer>