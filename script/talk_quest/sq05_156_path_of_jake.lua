<VillageServer>

function sq05_156_path_of_jake_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1225 then
		sq05_156_path_of_jake_OnTalk_n1225_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 13 then
		sq05_156_path_of_jake_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 179 then
		sq05_156_path_of_jake_OnTalk_n179_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 885 then
		sq05_156_path_of_jake_OnTalk_n885_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1225_cleric_tomas--------------------------------------------------------------------------------
function sq05_156_path_of_jake_OnTalk_n1225_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1225_cleric_tomas-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1225_cleric_tomas-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1225_cleric_tomas-3-b" then 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-c" then 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-d" then 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-e" then 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-f" then 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1560, true);
				 api_quest_RewardQuestUser(userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1560, true);
				 api_quest_RewardQuestUser(userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1560, true);
				 api_quest_RewardQuestUser(userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1560, true);
				 api_quest_RewardQuestUser(userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1560, true);
				 api_quest_RewardQuestUser(userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1560, true);
				 api_quest_RewardQuestUser(userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1560, true);
				 api_quest_RewardQuestUser(userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1560, true);
				 api_quest_RewardQuestUser(userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1560, true);
				 api_quest_RewardQuestUser(userObjID, 1560, questID, 1);
			 end 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 157, 2);
					api_quest_SetQuestStep(userObjID, 157, 1);
					api_quest_SetJournalStep(userObjID, 157, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1225_cleric_tomas-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1225_cleric_tomas-1", "sq05_157_the_reverse.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n013_cleric_tomas--------------------------------------------------------------------------------
function sq05_156_path_of_jake_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n013_cleric_tomas-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n013_cleric_tomas-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted" then
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest(userObjID,156, 2);
				api_quest_SetJournalStep(userObjID,156, 1);
				api_quest_SetQuestStep(userObjID,156, 1);

	end
	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted2" then
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest(userObjID,156, 2);
				api_quest_SetJournalStep(userObjID,156, 1);
				api_quest_SetQuestStep(userObjID,156, 1);

	end
	if npc_talk_index == "n013_cleric_tomas-1-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-d" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-e" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-f" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-g" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-h" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-i" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-j" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-k" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-l" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-m" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-n" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-o" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-warf_01" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13027,1);
	end
	if npc_talk_index == "n013_cleric_tomas-2-check_party2" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n013_cleric_tomas-2-a";

				else
									npc_talk_index = "n013_cleric_tomas-2-b";

				end
	end
	if npc_talk_index == "n013_cleric_tomas-2-warf_tomas_02" then 
				api_user_ChangeMap(userObjID,13027,1);
	end
	if npc_talk_index == "n013_cleric_tomas-3-check_party2" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_user_ChangeMap(userObjID,13027,1);

				else
									npc_talk_index = "n013_cleric_tomas-3-a";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n179_light_pole--------------------------------------------------------------------------------
function sq05_156_path_of_jake_OnTalk_n179_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n179_light_pole-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n179_light_pole-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n179_light_pole-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n179_light_pole-2-cutscene" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n885_light_pole--------------------------------------------------------------------------------
function sq05_156_path_of_jake_OnTalk_n885_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n885_light_pole-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n885_light_pole-1-b" then 
	end
	if npc_talk_index == "n885_light_pole-1-warf_02" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13027,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_156_path_of_jake_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 156);
end

function sq05_156_path_of_jake_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 156);
end

function sq05_156_path_of_jake_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 156);
	local questID=156;
end

function sq05_156_path_of_jake_OnRemoteStart( userObjID, questID )
end

function sq05_156_path_of_jake_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_156_path_of_jake_ForceAccept( userObjID, npcObjID, questID )
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest(userObjID,156, 2);
				api_quest_SetJournalStep(userObjID,156, 1);
				api_quest_SetQuestStep(userObjID,156, 1);
end

</VillageServer>

<GameServer>
function sq05_156_path_of_jake_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1225 then
		sq05_156_path_of_jake_OnTalk_n1225_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 13 then
		sq05_156_path_of_jake_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 179 then
		sq05_156_path_of_jake_OnTalk_n179_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 885 then
		sq05_156_path_of_jake_OnTalk_n885_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1225_cleric_tomas--------------------------------------------------------------------------------
function sq05_156_path_of_jake_OnTalk_n1225_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1225_cleric_tomas-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1225_cleric_tomas-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1225_cleric_tomas-3-b" then 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-c" then 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-d" then 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-e" then 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-f" then 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1560, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1560, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1560, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1560, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1560, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1560, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1560, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1560, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1560, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1560, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1560, questID, 1);
			 end 
	end
	if npc_talk_index == "n1225_cleric_tomas-3-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 157, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 157, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 157, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1225_cleric_tomas-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1225_cleric_tomas-1", "sq05_157_the_reverse.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n013_cleric_tomas--------------------------------------------------------------------------------
function sq05_156_path_of_jake_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n013_cleric_tomas-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n013_cleric_tomas-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted" then
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest( pRoom, userObjID,156, 2);
				api_quest_SetJournalStep( pRoom, userObjID,156, 1);
				api_quest_SetQuestStep( pRoom, userObjID,156, 1);

	end
	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted2" then
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest( pRoom, userObjID,156, 2);
				api_quest_SetJournalStep( pRoom, userObjID,156, 1);
				api_quest_SetQuestStep( pRoom, userObjID,156, 1);

	end
	if npc_talk_index == "n013_cleric_tomas-1-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-d" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-e" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-f" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-g" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-h" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-i" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-j" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-k" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-l" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-m" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-n" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-o" then 
	end
	if npc_talk_index == "n013_cleric_tomas-1-warf_01" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13027,1);
	end
	if npc_talk_index == "n013_cleric_tomas-2-check_party2" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n013_cleric_tomas-2-a";

				else
									npc_talk_index = "n013_cleric_tomas-2-b";

				end
	end
	if npc_talk_index == "n013_cleric_tomas-2-warf_tomas_02" then 
				api_user_ChangeMap( pRoom, userObjID,13027,1);
	end
	if npc_talk_index == "n013_cleric_tomas-3-check_party2" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_user_ChangeMap( pRoom, userObjID,13027,1);

				else
									npc_talk_index = "n013_cleric_tomas-3-a";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n179_light_pole--------------------------------------------------------------------------------
function sq05_156_path_of_jake_OnTalk_n179_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n179_light_pole-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n179_light_pole-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n179_light_pole-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n179_light_pole-2-cutscene" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n885_light_pole--------------------------------------------------------------------------------
function sq05_156_path_of_jake_OnTalk_n885_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n885_light_pole-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n885_light_pole-1-b" then 
	end
	if npc_talk_index == "n885_light_pole-1-warf_02" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13027,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_156_path_of_jake_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 156);
end

function sq05_156_path_of_jake_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 156);
end

function sq05_156_path_of_jake_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 156);
	local questID=156;
end

function sq05_156_path_of_jake_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq05_156_path_of_jake_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_156_path_of_jake_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				npc_talk_index = "n013_cleric_tomas-1";
				api_quest_AddQuest( pRoom, userObjID,156, 2);
				api_quest_SetJournalStep( pRoom, userObjID,156, 1);
				api_quest_SetQuestStep( pRoom, userObjID,156, 1);
end

</GameServer>