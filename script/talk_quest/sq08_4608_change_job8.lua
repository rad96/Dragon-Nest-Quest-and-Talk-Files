<VillageServer>

function sq08_4608_change_job8_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 100 then
		sq08_4608_change_job8_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1690 then
		sq08_4608_change_job8_OnTalk_n1690_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq08_4608_change_job8_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-1-b" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-c" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-reward_check_1" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-e";
				api_quest_SetQuestMemo(userObjID, questID, 1, 1);
	end
	if npc_talk_index == "n100_event_ilyn-1-reward_check_2" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-e";
				api_quest_SetQuestMemo(userObjID, questID, 1, 2);
	end
	if npc_talk_index == "n100_event_ilyn-1-reward_check_3" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-e";
				api_quest_SetQuestMemo(userObjID, questID, 1, 3);
	end
	if npc_talk_index == "n100_event_ilyn-1-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-rewoard" then 
				if api_quest_GetQuestMemo(userObjID, questID, 1) == 1  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46081, true);
				 api_quest_RewardQuestUser(userObjID, 46081, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-f";

				else
									if api_quest_GetQuestMemo(userObjID, questID, 1) == 2  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46082, true);
				 api_quest_RewardQuestUser(userObjID, 46082, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-f";

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46083, true);
				 api_quest_RewardQuestUser(userObjID, 46083, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-f";

				end

				end
	end
	if npc_talk_index == "n100_event_ilyn-1-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1690_eltia--------------------------------------------------------------------------------
function sq08_4608_change_job8_OnTalk_n1690_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1690_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1690_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1690_eltia-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46081, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46081, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46081, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46081, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46081, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46081, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46081, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46081, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46081, false);
			 end 

	end
	if npc_talk_index == "n1690_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4608, 1);
				api_quest_SetJournalStep(userObjID,4608, 1);
				api_quest_SetQuestStep(userObjID,4608, 1);
				npc_talk_index = "n1690_eltia-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_4608_change_job8_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4608);
end

function sq08_4608_change_job8_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4608);
end

function sq08_4608_change_job8_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4608);
	local questID=4608;
end

function sq08_4608_change_job8_OnRemoteStart( userObjID, questID )
end

function sq08_4608_change_job8_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_4608_change_job8_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4608, 1);
				api_quest_SetJournalStep(userObjID,4608, 1);
				api_quest_SetQuestStep(userObjID,4608, 1);
				npc_talk_index = "n1690_eltia-1";
end

</VillageServer>

<GameServer>
function sq08_4608_change_job8_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 100 then
		sq08_4608_change_job8_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1690 then
		sq08_4608_change_job8_OnTalk_n1690_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function sq08_4608_change_job8_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-1-b" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-c" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-reward_check_1" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-e";
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 1);
	end
	if npc_talk_index == "n100_event_ilyn-1-reward_check_2" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-e";
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 2);
	end
	if npc_talk_index == "n100_event_ilyn-1-reward_check_3" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-e";
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 3);
	end
	if npc_talk_index == "n100_event_ilyn-1-d" then 
	end
	if npc_talk_index == "n100_event_ilyn-1-rewoard" then 
				if api_quest_GetQuestMemo( pRoom, userObjID, questID, 1) == 1  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46081, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-f";

				else
									if api_quest_GetQuestMemo( pRoom, userObjID, questID, 1) == 2  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46082, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-f";

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46083, questID, 1);
			 end 
				npc_talk_index = "n100_event_ilyn-1-f";

				end

				end
	end
	if npc_talk_index == "n100_event_ilyn-1-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1690_eltia--------------------------------------------------------------------------------
function sq08_4608_change_job8_OnTalk_n1690_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1690_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1690_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1690_eltia-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46081, false);
			 end 

	end
	if npc_talk_index == "n1690_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4608, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4608, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4608, 1);
				npc_talk_index = "n1690_eltia-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_4608_change_job8_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4608);
end

function sq08_4608_change_job8_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4608);
end

function sq08_4608_change_job8_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4608);
	local questID=4608;
end

function sq08_4608_change_job8_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_4608_change_job8_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_4608_change_job8_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4608, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4608, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4608, 1);
				npc_talk_index = "n1690_eltia-1";
end

</GameServer>