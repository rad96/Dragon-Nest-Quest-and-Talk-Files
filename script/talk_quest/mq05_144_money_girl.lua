<VillageServer>

function mq05_144_money_girl_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 11 then
		mq05_144_money_girl_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 12 then
		mq05_144_money_girl_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function mq05_144_money_girl_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n011_priest_leonardo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n011_priest_leonardo-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n011_priest_leonardo-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n011_priest_leonardo-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n011_priest_leonardo-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-accepting-acceptted" then
				api_quest_AddQuest(userObjID,144, 2);
				api_quest_SetJournalStep(userObjID,144, 1);
				api_quest_SetQuestStep(userObjID,144, 1);
				npc_talk_index = "n011_priest_leonardo-1";

	end
	if npc_talk_index == "n011_priest_leonardo-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n011_priest_leonardo-3-a" then 
				api_quest_DelQuestItem(userObjID, 300028, 1);
	end
	if npc_talk_index == "n011_priest_leonardo-3-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-3-c" then 
	end
	if npc_talk_index == "n011_priest_leonardo-3-d" then 
	end
	if npc_talk_index == "n011_priest_leonardo-3-cutscene_3" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_user_PlayCutScene(userObjID,npcObjID,60);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_144_money_girl_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n012_sorceress_master_cynthia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n012_sorceress_master_cynthia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n012_sorceress_master_cynthia-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n012_sorceress_master_cynthia-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-2-a" then 
				api_quest_DelQuestItem(userObjID, 300059, 1);
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-g" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-h" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-i" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-lv_chk5" then 
				if api_user_GetUserLevel(userObjID) >= 9 then
									npc_talk_index = "n012_sorceress_master_cynthia-5-a";

				else
									npc_talk_index = "n012_sorceress_master_cynthia-5-l";

				end
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-g" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-h" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-h" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-i" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1440, true);
				 api_quest_RewardQuestUser(userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1440, true);
				 api_quest_RewardQuestUser(userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1440, true);
				 api_quest_RewardQuestUser(userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1440, true);
				 api_quest_RewardQuestUser(userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1440, true);
				 api_quest_RewardQuestUser(userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1440, true);
				 api_quest_RewardQuestUser(userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1440, true);
				 api_quest_RewardQuestUser(userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1440, true);
				 api_quest_RewardQuestUser(userObjID, 1440, questID, 1);
			 end 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-k" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 201, 2);
					api_quest_SetQuestStep(userObjID, 201, 1);
					api_quest_SetJournalStep(userObjID, 201, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300012, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300012, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_144_money_girl_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 144);
end

function mq05_144_money_girl_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 144);
end

function mq05_144_money_girl_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 144);
	local questID=144;
end

function mq05_144_money_girl_OnRemoteStart( userObjID, questID )
end

function mq05_144_money_girl_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq05_144_money_girl_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,144, 2);
				api_quest_SetJournalStep(userObjID,144, 1);
				api_quest_SetQuestStep(userObjID,144, 1);
				npc_talk_index = "n011_priest_leonardo-1";
end

</VillageServer>

<GameServer>
function mq05_144_money_girl_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 11 then
		mq05_144_money_girl_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 12 then
		mq05_144_money_girl_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function mq05_144_money_girl_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n011_priest_leonardo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n011_priest_leonardo-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n011_priest_leonardo-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n011_priest_leonardo-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n011_priest_leonardo-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,144, 2);
				api_quest_SetJournalStep( pRoom, userObjID,144, 1);
				api_quest_SetQuestStep( pRoom, userObjID,144, 1);
				npc_talk_index = "n011_priest_leonardo-1";

	end
	if npc_talk_index == "n011_priest_leonardo-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n011_priest_leonardo-3-a" then 
				api_quest_DelQuestItem( pRoom, userObjID, 300028, 1);
	end
	if npc_talk_index == "n011_priest_leonardo-3-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-3-c" then 
	end
	if npc_talk_index == "n011_priest_leonardo-3-d" then 
	end
	if npc_talk_index == "n011_priest_leonardo-3-cutscene_3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_user_PlayCutScene( pRoom, userObjID,npcObjID,60);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_144_money_girl_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n012_sorceress_master_cynthia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n012_sorceress_master_cynthia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n012_sorceress_master_cynthia-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n012_sorceress_master_cynthia-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-2-a" then 
				api_quest_DelQuestItem( pRoom, userObjID, 300059, 1);
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-g" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-h" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-2-i" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-4-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-lv_chk5" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 9 then
									npc_talk_index = "n012_sorceress_master_cynthia-5-a";

				else
									npc_talk_index = "n012_sorceress_master_cynthia-5-l";

				end
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-e" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-f" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-g" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-h" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-h" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-i" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1440, questID, 1);
			 end 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-5-k" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 201, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 201, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 201, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300012, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300012, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_144_money_girl_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 144);
end

function mq05_144_money_girl_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 144);
end

function mq05_144_money_girl_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 144);
	local questID=144;
end

function mq05_144_money_girl_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq05_144_money_girl_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq05_144_money_girl_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,144, 2);
				api_quest_SetJournalStep( pRoom, userObjID,144, 1);
				api_quest_SetQuestStep( pRoom, userObjID,144, 1);
				npc_talk_index = "n011_priest_leonardo-1";
end

</GameServer>