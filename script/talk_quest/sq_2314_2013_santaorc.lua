<VillageServer>

function sq_2314_2013_santaorc_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1262 then
		sq_2314_2013_santaorc_OnTalk_n1262_sanataorc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1262_sanataorc--------------------------------------------------------------------------------
function sq_2314_2013_santaorc_OnTalk_n1262_sanataorc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1262_sanataorc-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1262_sanataorc-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1262_sanataorc-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23140, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23140, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23140, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23140, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23140, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23140, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23140, false);
			 end 

	end
	if npc_talk_index == "n1262_sanataorc-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2314, 1);
				api_quest_SetJournalStep(userObjID,2314, 1);
				api_quest_SetQuestStep(userObjID,2314, 1);
				npc_talk_index = "n1262_sanataorc-1";

	end
	if npc_talk_index == "n1262_sanataorc-1-b" then 
	end
	if npc_talk_index == "n1262_sanataorc-1-c" then 
	end
	if npc_talk_index == "n1262_sanataorc-1-d" then 
	end
	if npc_talk_index == "n1262_sanataorc-1-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23140, true);
				 api_quest_RewardQuestUser(userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23140, true);
				 api_quest_RewardQuestUser(userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23140, true);
				 api_quest_RewardQuestUser(userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23140, true);
				 api_quest_RewardQuestUser(userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23140, true);
				 api_quest_RewardQuestUser(userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23140, true);
				 api_quest_RewardQuestUser(userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23140, true);
				 api_quest_RewardQuestUser(userObjID, 23140, questID, 1);
			 end 
	end
	if npc_talk_index == "n1262_sanataorc-1-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2314_2013_santaorc_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2314);
end

function sq_2314_2013_santaorc_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2314);
end

function sq_2314_2013_santaorc_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2314);
	local questID=2314;
end

function sq_2314_2013_santaorc_OnRemoteStart( userObjID, questID )
end

function sq_2314_2013_santaorc_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_2314_2013_santaorc_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2314, 1);
				api_quest_SetJournalStep(userObjID,2314, 1);
				api_quest_SetQuestStep(userObjID,2314, 1);
				npc_talk_index = "n1262_sanataorc-1";
end

</VillageServer>

<GameServer>
function sq_2314_2013_santaorc_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1262 then
		sq_2314_2013_santaorc_OnTalk_n1262_sanataorc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1262_sanataorc--------------------------------------------------------------------------------
function sq_2314_2013_santaorc_OnTalk_n1262_sanataorc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1262_sanataorc-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1262_sanataorc-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1262_sanataorc-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, false);
			 end 

	end
	if npc_talk_index == "n1262_sanataorc-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2314, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2314, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2314, 1);
				npc_talk_index = "n1262_sanataorc-1";

	end
	if npc_talk_index == "n1262_sanataorc-1-b" then 
	end
	if npc_talk_index == "n1262_sanataorc-1-c" then 
	end
	if npc_talk_index == "n1262_sanataorc-1-d" then 
	end
	if npc_talk_index == "n1262_sanataorc-1-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23140, questID, 1);
			 end 
	end
	if npc_talk_index == "n1262_sanataorc-1-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2314_2013_santaorc_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2314);
end

function sq_2314_2013_santaorc_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2314);
end

function sq_2314_2013_santaorc_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2314);
	local questID=2314;
end

function sq_2314_2013_santaorc_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_2314_2013_santaorc_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_2314_2013_santaorc_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2314, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2314, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2314, 1);
				npc_talk_index = "n1262_sanataorc-1";
end

</GameServer>