<VillageServer>

function sq08_293_battle_magic2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 35 then
		sq08_293_battle_magic2_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_293_battle_magic2_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n035_soceress_master_tiana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2930, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2930, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2930, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2930, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2930, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2930, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2930, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2930, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2930, false);
			 end 

	end
	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest(userObjID,293, 1);
				api_quest_SetJournalStep(userObjID,293, 1);
				api_quest_SetQuestStep(userObjID,293, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-h" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-i" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 612, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200612, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 21014, 30000);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2930, true);
				 api_quest_RewardQuestUser(userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2930, true);
				 api_quest_RewardQuestUser(userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2930, true);
				 api_quest_RewardQuestUser(userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2930, true);
				 api_quest_RewardQuestUser(userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2930, true);
				 api_quest_RewardQuestUser(userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2930, true);
				 api_quest_RewardQuestUser(userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2930, true);
				 api_quest_RewardQuestUser(userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2930, true);
				 api_quest_RewardQuestUser(userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2930, true);
				 api_quest_RewardQuestUser(userObjID, 2930, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-h" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_293_battle_magic2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 293);
	if qstep == 2 and CountIndex == 612 then
				if  api_user_GetStageConstructionLevel( userObjID ) >= 0 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

	end
	if qstep == 2 and CountIndex == 200612 then
				if  api_user_GetStageConstructionLevel( userObjID ) >= 0 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

	end
	if qstep == 2 and CountIndex == 21014 then
				if  api_user_GetStageConstructionLevel( userObjID ) >= 0 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

	end
end

function sq08_293_battle_magic2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 293);
	if qstep == 2 and CountIndex == 612 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200612 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 21014 and Count >= TargetCount  then

	end
end

function sq08_293_battle_magic2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 293);
	local questID=293;
end

function sq08_293_battle_magic2_OnRemoteStart( userObjID, questID )
end

function sq08_293_battle_magic2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_293_battle_magic2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,293, 1);
				api_quest_SetJournalStep(userObjID,293, 1);
				api_quest_SetQuestStep(userObjID,293, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</VillageServer>

<GameServer>
function sq08_293_battle_magic2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 35 then
		sq08_293_battle_magic2_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_293_battle_magic2_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n035_soceress_master_tiana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, false);
			 end 

	end
	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,293, 1);
				api_quest_SetJournalStep( pRoom, userObjID,293, 1);
				api_quest_SetQuestStep( pRoom, userObjID,293, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-h" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-i" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 612, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200612, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 21014, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2930, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-g" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-h" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_293_battle_magic2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 293);
	if qstep == 2 and CountIndex == 612 then
				if  api_user_GetStageConstructionLevel( pRoom,  userObjID ) >= 0 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

	end
	if qstep == 2 and CountIndex == 200612 then
				if  api_user_GetStageConstructionLevel( pRoom,  userObjID ) >= 0 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

	end
	if qstep == 2 and CountIndex == 21014 then
				if  api_user_GetStageConstructionLevel( pRoom,  userObjID ) >= 0 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

	end
end

function sq08_293_battle_magic2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 293);
	if qstep == 2 and CountIndex == 612 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200612 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 21014 and Count >= TargetCount  then

	end
end

function sq08_293_battle_magic2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 293);
	local questID=293;
end

function sq08_293_battle_magic2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_293_battle_magic2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_293_battle_magic2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,293, 1);
				api_quest_SetJournalStep( pRoom, userObjID,293, 1);
				api_quest_SetQuestStep( pRoom, userObjID,293, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</GameServer>