<VillageServer>

function sq11_625_revenge_of_orc_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 383 then
		sq11_625_revenge_of_orc_OnTalk_n383_jester_jack(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq11_625_revenge_of_orc_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n383_jester_jack--------------------------------------------------------------------------------
function sq11_625_revenge_of_orc_OnTalk_n383_jester_jack(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n383_jester_jack-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n383_jester_jack-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n383_jester_jack-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n383_jester_jack-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n383_jester_jack-1-b" then 
	end
	if npc_talk_index == "n383_jester_jack-1-c" then 
	end
	if npc_talk_index == "n383_jester_jack-1-d" then 
	end
	if npc_talk_index == "n383_jester_jack-1-e" then 
	end
	if npc_talk_index == "n383_jester_jack-1-f" then 
	end
	if npc_talk_index == "n383_jester_jack-1-g" then 
	end
	if npc_talk_index == "n383_jester_jack-1-h" then 
	end
	if npc_talk_index == "n383_jester_jack-2" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 21576, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 21575, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 21577, 30000);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 23345, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 23346, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 23347, 30000);
	end
	if npc_talk_index == "n383_jester_jack-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6250, true);
				 api_quest_RewardQuestUser(userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6250, true);
				 api_quest_RewardQuestUser(userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6250, true);
				 api_quest_RewardQuestUser(userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6250, true);
				 api_quest_RewardQuestUser(userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6250, true);
				 api_quest_RewardQuestUser(userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6250, true);
				 api_quest_RewardQuestUser(userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6250, true);
				 api_quest_RewardQuestUser(userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6250, true);
				 api_quest_RewardQuestUser(userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6250, true);
				 api_quest_RewardQuestUser(userObjID, 6250, questID, 1);
			 end 
	end
	if npc_talk_index == "n383_jester_jack-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_625_revenge_of_orc_OnTalk_n098_warrior_master_lodrigo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-l" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6250, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6250, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6250, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6250, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6250, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6250, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6250, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6250, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6250, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest(userObjID,625, 1);
				api_quest_SetJournalStep(userObjID,625, 1);
				api_quest_SetQuestStep(userObjID,625, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_625_revenge_of_orc_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 625);
	if qstep == 2 and CountIndex == 21576 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetQuestStep(userObjID, questID,3);

	end
	if qstep == 2 and CountIndex == 21575 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetQuestStep(userObjID, questID,3);

	end
	if qstep == 2 and CountIndex == 21577 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetQuestStep(userObjID, questID,3);

	end
	if qstep == 2 and CountIndex == 23345 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetQuestStep(userObjID, questID,3);

	end
	if qstep == 2 and CountIndex == 23346 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetQuestStep(userObjID, questID,3);

	end
	if qstep == 2 and CountIndex == 23347 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetQuestStep(userObjID, questID,3);

	end
end

function sq11_625_revenge_of_orc_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 625);
	if qstep == 2 and CountIndex == 21576 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 21575 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 21577 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 23345 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 23346 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 23347 and Count >= TargetCount  then

	end
end

function sq11_625_revenge_of_orc_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 625);
	local questID=625;
end

function sq11_625_revenge_of_orc_OnRemoteStart( userObjID, questID )
end

function sq11_625_revenge_of_orc_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_625_revenge_of_orc_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,625, 1);
				api_quest_SetJournalStep(userObjID,625, 1);
				api_quest_SetQuestStep(userObjID,625, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</VillageServer>

<GameServer>
function sq11_625_revenge_of_orc_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 383 then
		sq11_625_revenge_of_orc_OnTalk_n383_jester_jack( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 98 then
		sq11_625_revenge_of_orc_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n383_jester_jack--------------------------------------------------------------------------------
function sq11_625_revenge_of_orc_OnTalk_n383_jester_jack( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n383_jester_jack-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n383_jester_jack-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n383_jester_jack-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n383_jester_jack-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n383_jester_jack-1-b" then 
	end
	if npc_talk_index == "n383_jester_jack-1-c" then 
	end
	if npc_talk_index == "n383_jester_jack-1-d" then 
	end
	if npc_talk_index == "n383_jester_jack-1-e" then 
	end
	if npc_talk_index == "n383_jester_jack-1-f" then 
	end
	if npc_talk_index == "n383_jester_jack-1-g" then 
	end
	if npc_talk_index == "n383_jester_jack-1-h" then 
	end
	if npc_talk_index == "n383_jester_jack-2" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 21576, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 21575, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 21577, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 23345, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 23346, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 23347, 30000);
	end
	if npc_talk_index == "n383_jester_jack-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6250, questID, 1);
			 end 
	end
	if npc_talk_index == "n383_jester_jack-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n098_warrior_master_lodrigo--------------------------------------------------------------------------------
function sq11_625_revenge_of_orc_OnTalk_n098_warrior_master_lodrigo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n098_warrior_master_lodrigo-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n098_warrior_master_lodrigo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-l" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6250, false);
			 end 

	end
	if npc_talk_index == "n098_warrior_master_lodrigo-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,625, 1);
				api_quest_SetJournalStep( pRoom, userObjID,625, 1);
				api_quest_SetQuestStep( pRoom, userObjID,625, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_625_revenge_of_orc_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 625);
	if qstep == 2 and CountIndex == 21576 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);

	end
	if qstep == 2 and CountIndex == 21575 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);

	end
	if qstep == 2 and CountIndex == 21577 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);

	end
	if qstep == 2 and CountIndex == 23345 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);

	end
	if qstep == 2 and CountIndex == 23346 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);

	end
	if qstep == 2 and CountIndex == 23347 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);

	end
end

function sq11_625_revenge_of_orc_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 625);
	if qstep == 2 and CountIndex == 21576 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 21575 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 21577 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 23345 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 23346 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 23347 and Count >= TargetCount  then

	end
end

function sq11_625_revenge_of_orc_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 625);
	local questID=625;
end

function sq11_625_revenge_of_orc_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_625_revenge_of_orc_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_625_revenge_of_orc_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,625, 1);
				api_quest_SetJournalStep( pRoom, userObjID,625, 1);
				api_quest_SetQuestStep( pRoom, userObjID,625, 1);
				npc_talk_index = "n098_warrior_master_lodrigo-1";
end

</GameServer>