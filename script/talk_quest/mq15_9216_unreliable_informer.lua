<VillageServer>

function mq15_9216_unreliable_informer_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 707 then
		mq15_9216_unreliable_informer_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq15_9216_unreliable_informer_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 831 then
		mq15_9216_unreliable_informer_OnTalk_n831_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 832 then
		mq15_9216_unreliable_informer_OnTalk_n832_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 833 then
		mq15_9216_unreliable_informer_OnTalk_n833_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mq15_9216_unreliable_informer_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq15_9216_unreliable_informer_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9216, 2);
				api_quest_SetJournalStep(userObjID,9216, 1);
				api_quest_SetQuestStep(userObjID,9216, 1);
				npc_talk_index = "n822_teramai-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n831_yuvenciel--------------------------------------------------------------------------------
function mq15_9216_unreliable_informer_OnTalk_n831_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n831_yuvenciel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n832_velskud--------------------------------------------------------------------------------
function mq15_9216_unreliable_informer_OnTalk_n832_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n832_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n832_velskud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n832_velskud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n832_velskud-2-b" then 
	end
	if npc_talk_index == "n832_velskud-2-c" then 
	end
	if npc_talk_index == "n832_velskud-2-d" then 
	end
	if npc_talk_index == "n832_velskud-2-e" then 
	end
	if npc_talk_index == "n832_velskud-2-f" then 
	end
	if npc_talk_index == "n832_velskud-2-g" then 
	end
	if npc_talk_index == "n832_velskud-2-class_check" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n832_velskud-2-h";

				else
									if api_user_GetUserClassID(userObjID) == 2 then
									npc_talk_index = "n832_velskud-2-m";

				else
									npc_talk_index = "n832_velskud-2-n";

				end

				end
	end
	if npc_talk_index == "n832_velskud-2-i" then 
	end
	if npc_talk_index == "n832_velskud-2-j" then 
	end
	if npc_talk_index == "n832_velskud-2-k" then 
	end
	if npc_talk_index == "n832_velskud-2-l" then 
	end
	if npc_talk_index == "n832_velskud-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n832_velskud-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n832_velskud-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n833_velskud--------------------------------------------------------------------------------
function mq15_9216_unreliable_informer_OnTalk_n833_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n833_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n833_velskud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n833_velskud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n833_velskud-1-chk_aca" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n833_velskud-1-a";

				else
									npc_talk_index = "n833_velskud-1-b";

				end
	end
	if npc_talk_index == "n833_velskud-1-c" then 
	end
	if npc_talk_index == "n833_velskud-1-c" then 
	end
	if npc_talk_index == "n833_velskud-1-d" then 
	end
	if npc_talk_index == "n833_velskud-1-e" then 
	end
	if npc_talk_index == "n833_velskud-1-f" then 
	end
	if npc_talk_index == "n833_velskud-1-g" then 
	end
	if npc_talk_index == "n833_velskud-1-h" then 
	end
	if npc_talk_index == "n833_velskud-1-i" then 
	end
	if npc_talk_index == "n833_velskud-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n833_velskud-3-b" then 
	end
	if npc_talk_index == "n833_velskud-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92161, true);
				 api_quest_RewardQuestUser(userObjID, 92161, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92162, true);
				 api_quest_RewardQuestUser(userObjID, 92162, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92163, true);
				 api_quest_RewardQuestUser(userObjID, 92163, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92164, true);
				 api_quest_RewardQuestUser(userObjID, 92164, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92165, true);
				 api_quest_RewardQuestUser(userObjID, 92165, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92166, true);
				 api_quest_RewardQuestUser(userObjID, 92166, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92161, true);
				 api_quest_RewardQuestUser(userObjID, 92161, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92161, true);
				 api_quest_RewardQuestUser(userObjID, 92161, questID, 1);
			 end 
	end
	if npc_talk_index == "n833_velskud-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9217, 2);
					api_quest_SetQuestStep(userObjID, 9217, 1);
					api_quest_SetJournalStep(userObjID, 9217, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n833_velskud-1", "mq15_9217_mirror_of_truth.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9216_unreliable_informer_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9216);
end

function mq15_9216_unreliable_informer_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9216);
end

function mq15_9216_unreliable_informer_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9216);
	local questID=9216;
end

function mq15_9216_unreliable_informer_OnRemoteStart( userObjID, questID )
end

function mq15_9216_unreliable_informer_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9216_unreliable_informer_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9216, 2);
				api_quest_SetJournalStep(userObjID,9216, 1);
				api_quest_SetQuestStep(userObjID,9216, 1);
				npc_talk_index = "n822_teramai-1";
end

</VillageServer>

<GameServer>
function mq15_9216_unreliable_informer_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 707 then
		mq15_9216_unreliable_informer_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq15_9216_unreliable_informer_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 831 then
		mq15_9216_unreliable_informer_OnTalk_n831_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 832 then
		mq15_9216_unreliable_informer_OnTalk_n832_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 833 then
		mq15_9216_unreliable_informer_OnTalk_n833_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mq15_9216_unreliable_informer_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq15_9216_unreliable_informer_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9216, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9216, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9216, 1);
				npc_talk_index = "n822_teramai-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n831_yuvenciel--------------------------------------------------------------------------------
function mq15_9216_unreliable_informer_OnTalk_n831_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n831_yuvenciel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n832_velskud--------------------------------------------------------------------------------
function mq15_9216_unreliable_informer_OnTalk_n832_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n832_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n832_velskud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n832_velskud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n832_velskud-2-b" then 
	end
	if npc_talk_index == "n832_velskud-2-c" then 
	end
	if npc_talk_index == "n832_velskud-2-d" then 
	end
	if npc_talk_index == "n832_velskud-2-e" then 
	end
	if npc_talk_index == "n832_velskud-2-f" then 
	end
	if npc_talk_index == "n832_velskud-2-g" then 
	end
	if npc_talk_index == "n832_velskud-2-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n832_velskud-2-h";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									npc_talk_index = "n832_velskud-2-m";

				else
									npc_talk_index = "n832_velskud-2-n";

				end

				end
	end
	if npc_talk_index == "n832_velskud-2-i" then 
	end
	if npc_talk_index == "n832_velskud-2-j" then 
	end
	if npc_talk_index == "n832_velskud-2-k" then 
	end
	if npc_talk_index == "n832_velskud-2-l" then 
	end
	if npc_talk_index == "n832_velskud-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n832_velskud-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n832_velskud-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n833_velskud--------------------------------------------------------------------------------
function mq15_9216_unreliable_informer_OnTalk_n833_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n833_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n833_velskud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n833_velskud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n833_velskud-1-chk_aca" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n833_velskud-1-a";

				else
									npc_talk_index = "n833_velskud-1-b";

				end
	end
	if npc_talk_index == "n833_velskud-1-c" then 
	end
	if npc_talk_index == "n833_velskud-1-c" then 
	end
	if npc_talk_index == "n833_velskud-1-d" then 
	end
	if npc_talk_index == "n833_velskud-1-e" then 
	end
	if npc_talk_index == "n833_velskud-1-f" then 
	end
	if npc_talk_index == "n833_velskud-1-g" then 
	end
	if npc_talk_index == "n833_velskud-1-h" then 
	end
	if npc_talk_index == "n833_velskud-1-i" then 
	end
	if npc_talk_index == "n833_velskud-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n833_velskud-3-b" then 
	end
	if npc_talk_index == "n833_velskud-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92161, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92161, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92162, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92162, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92163, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92163, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92164, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92164, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92165, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92165, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92166, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92166, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92161, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92161, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92161, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92161, questID, 1);
			 end 
	end
	if npc_talk_index == "n833_velskud-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9217, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9217, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9217, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n833_velskud-1", "mq15_9217_mirror_of_truth.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9216_unreliable_informer_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9216);
end

function mq15_9216_unreliable_informer_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9216);
end

function mq15_9216_unreliable_informer_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9216);
	local questID=9216;
end

function mq15_9216_unreliable_informer_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9216_unreliable_informer_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9216_unreliable_informer_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9216, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9216, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9216, 1);
				npc_talk_index = "n822_teramai-1";
end

</GameServer>