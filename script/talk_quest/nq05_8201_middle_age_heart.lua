<VillageServer>

function nq05_8201_middle_age_heart_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 46 then
		nq05_8201_middle_age_heart_OnTalk_n046_cleric_master_enoc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 92 then
		nq05_8201_middle_age_heart_OnTalk_n092_trader_kelly(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n046_cleric_master_enoc--------------------------------------------------------------------------------
function nq05_8201_middle_age_heart_OnTalk_n046_cleric_master_enoc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n046_cleric_master_enoc-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n046_cleric_master_enoc-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n046_cleric_master_enoc-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n046_cleric_master_enoc-accepting-g" then
				api_npc_Disappoint(userObjID, 46 );

	end
	if npc_talk_index == "n046_cleric_master_enoc-accepting-acceptted" then
				api_quest_AddQuest(userObjID,8201, 1);
				api_quest_SetJournalStep(userObjID,8201, 1);
				api_quest_SetQuestStep(userObjID,8201, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";

	end
	if npc_talk_index == "n046_cleric_master_enoc-1-b" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-c" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-d" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-e" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-f" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-g" then 
				api_npc_Disappoint(userObjID, 46 );
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-h" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-i" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-2" then 
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n046_cleric_master_enoc-3-b" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 46, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_Disappoint(userObjID, 46 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n092_trader_kelly--------------------------------------------------------------------------------
function nq05_8201_middle_age_heart_OnTalk_n092_trader_kelly(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n092_trader_kelly-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n092_trader_kelly-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n092_trader_kelly-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n092_trader_kelly-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n092_trader_kelly-2-b" then 
	end
	if npc_talk_index == "n092_trader_kelly-2-c" then 
	end
	if npc_talk_index == "n092_trader_kelly-2-d" then 
	end
	if npc_talk_index == "n092_trader_kelly-3" then 
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetQuestStep(userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq05_8201_middle_age_heart_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8201);
end

function nq05_8201_middle_age_heart_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8201);
end

function nq05_8201_middle_age_heart_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8201);
	local questID=8201;
end

function nq05_8201_middle_age_heart_OnRemoteStart( userObjID, questID )
end

function nq05_8201_middle_age_heart_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq05_8201_middle_age_heart_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,8201, 1);
				api_quest_SetJournalStep(userObjID,8201, 1);
				api_quest_SetQuestStep(userObjID,8201, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";
end

</VillageServer>

<GameServer>
function nq05_8201_middle_age_heart_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 46 then
		nq05_8201_middle_age_heart_OnTalk_n046_cleric_master_enoc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 92 then
		nq05_8201_middle_age_heart_OnTalk_n092_trader_kelly( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n046_cleric_master_enoc--------------------------------------------------------------------------------
function nq05_8201_middle_age_heart_OnTalk_n046_cleric_master_enoc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n046_cleric_master_enoc-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n046_cleric_master_enoc-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n046_cleric_master_enoc-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n046_cleric_master_enoc-accepting-g" then
				api_npc_Disappoint( pRoom, userObjID, 46 );

	end
	if npc_talk_index == "n046_cleric_master_enoc-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,8201, 1);
				api_quest_SetJournalStep( pRoom, userObjID,8201, 1);
				api_quest_SetQuestStep( pRoom, userObjID,8201, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";

	end
	if npc_talk_index == "n046_cleric_master_enoc-1-b" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-c" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-d" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-e" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-f" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-g" then 
				api_npc_Disappoint( pRoom, userObjID, 46 );
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-h" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-1-i" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-2" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n046_cleric_master_enoc-3-b" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 46, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_Disappoint( pRoom, userObjID, 46 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n092_trader_kelly--------------------------------------------------------------------------------
function nq05_8201_middle_age_heart_OnTalk_n092_trader_kelly( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n092_trader_kelly-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n092_trader_kelly-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n092_trader_kelly-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n092_trader_kelly-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n092_trader_kelly-2-b" then 
	end
	if npc_talk_index == "n092_trader_kelly-2-c" then 
	end
	if npc_talk_index == "n092_trader_kelly-2-d" then 
	end
	if npc_talk_index == "n092_trader_kelly-3" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq05_8201_middle_age_heart_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8201);
end

function nq05_8201_middle_age_heart_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8201);
end

function nq05_8201_middle_age_heart_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8201);
	local questID=8201;
end

function nq05_8201_middle_age_heart_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq05_8201_middle_age_heart_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq05_8201_middle_age_heart_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,8201, 1);
				api_quest_SetJournalStep( pRoom, userObjID,8201, 1);
				api_quest_SetQuestStep( pRoom, userObjID,8201, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";
end

</GameServer>