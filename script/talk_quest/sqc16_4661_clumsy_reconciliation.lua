<VillageServer>

function sqc16_4661_clumsy_reconciliation_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1796 then
		sqc16_4661_clumsy_reconciliation_OnTalk_n1796_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		sqc16_4661_clumsy_reconciliation_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1796_rubinat--------------------------------------------------------------------------------
function sqc16_4661_clumsy_reconciliation_OnTalk_n1796_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1796_rubinat-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1796_rubinat-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1796_rubinat-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1796_rubinat-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1796_rubinat-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46610, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46610, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46610, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46610, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46610, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46610, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46610, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46610, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46610, false);
			 end 

	end
	if npc_talk_index == "n1796_rubinat-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4661, 1);
				api_quest_SetJournalStep(userObjID,4661, 1);
				api_quest_SetQuestStep(userObjID,4661, 1);
				npc_talk_index = "n1796_rubinat-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 1080, 1 );

	end
	if npc_talk_index == "n1796_rubinat-2-b" then 
	end
	if npc_talk_index == "n1796_rubinat-2-c" then 
	end
	if npc_talk_index == "n1796_rubinat-2-d" then 
	end
	if npc_talk_index == "n1796_rubinat-2-d" then 
	end
	if npc_talk_index == "n1796_rubinat-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function sqc16_4661_clumsy_reconciliation_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n776_darklair_mardlen-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-3-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-g" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-h" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-i" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-i" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-j" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-k" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-l" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-m" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-n" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-n" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-o" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-p" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-p" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-q" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-r" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-s" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-t" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-u" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46610, true);
				 api_quest_RewardQuestUser(userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46610, true);
				 api_quest_RewardQuestUser(userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46610, true);
				 api_quest_RewardQuestUser(userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46610, true);
				 api_quest_RewardQuestUser(userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46610, true);
				 api_quest_RewardQuestUser(userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46610, true);
				 api_quest_RewardQuestUser(userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46610, true);
				 api_quest_RewardQuestUser(userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46610, true);
				 api_quest_RewardQuestUser(userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46610, true);
				 api_quest_RewardQuestUser(userObjID, 46610, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-v" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-3-w" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4661_clumsy_reconciliation_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4661);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( userObjID ) < 4 then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
		 end 
	 end 
end

function sqc16_4661_clumsy_reconciliation_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4661);
end

function sqc16_4661_clumsy_reconciliation_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4661);
	local questID=4661;
end

function sqc16_4661_clumsy_reconciliation_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 1080, 1 );
end

function sqc16_4661_clumsy_reconciliation_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc16_4661_clumsy_reconciliation_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4661, 1);
				api_quest_SetJournalStep(userObjID,4661, 1);
				api_quest_SetQuestStep(userObjID,4661, 1);
				npc_talk_index = "n1796_rubinat-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 1080, 1 );
end

</VillageServer>

<GameServer>
function sqc16_4661_clumsy_reconciliation_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1796 then
		sqc16_4661_clumsy_reconciliation_OnTalk_n1796_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		sqc16_4661_clumsy_reconciliation_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1796_rubinat--------------------------------------------------------------------------------
function sqc16_4661_clumsy_reconciliation_OnTalk_n1796_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1796_rubinat-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1796_rubinat-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1796_rubinat-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1796_rubinat-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1796_rubinat-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, false);
			 end 

	end
	if npc_talk_index == "n1796_rubinat-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4661, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4661, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4661, 1);
				npc_talk_index = "n1796_rubinat-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 1080, 1 );

	end
	if npc_talk_index == "n1796_rubinat-2-b" then 
	end
	if npc_talk_index == "n1796_rubinat-2-c" then 
	end
	if npc_talk_index == "n1796_rubinat-2-d" then 
	end
	if npc_talk_index == "n1796_rubinat-2-d" then 
	end
	if npc_talk_index == "n1796_rubinat-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function sqc16_4661_clumsy_reconciliation_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n776_darklair_mardlen-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-3-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-g" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-h" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-i" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-i" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-j" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-k" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-l" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-m" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-n" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-n" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-o" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-p" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-p" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-q" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-r" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-s" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-t" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-u" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46610, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-v" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-3-w" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4661_clumsy_reconciliation_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4661);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) < 4 then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
		 end 
	 end 
end

function sqc16_4661_clumsy_reconciliation_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4661);
end

function sqc16_4661_clumsy_reconciliation_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4661);
	local questID=4661;
end

function sqc16_4661_clumsy_reconciliation_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 1080, 1 );
end

function sqc16_4661_clumsy_reconciliation_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc16_4661_clumsy_reconciliation_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4661, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4661, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4661, 1);
				npc_talk_index = "n1796_rubinat-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 1080, 1 );
end

</GameServer>