<VillageServer>

function mqc14_9416_memory_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1508 then
		mqc14_9416_memory_OnTalk_n1508_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9416_memory_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1519 then
		mqc14_9416_memory_OnTalk_n1519_aruno(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1522 then
		mqc14_9416_memory_OnTalk_n1522_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1523 then
		mqc14_9416_memory_OnTalk_n1523_aruno(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1508_argenta--------------------------------------------------------------------------------
function mqc14_9416_memory_OnTalk_n1508_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1508_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1508_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1508_argenta-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1508_argenta-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1508_argenta-1-b" then 
	end
	if npc_talk_index == "n1508_argenta-1-c" then 
	end
	if npc_talk_index == "n1508_argenta-1-d" then 
	end
	if npc_talk_index == "n1508_argenta-1-e" then 
	end
	if npc_talk_index == "n1508_argenta-1-e" then 
	end
	if npc_talk_index == "n1508_argenta-1-e" then 
	end
	if npc_talk_index == "n1508_argenta-1-f" then 
	end
	if npc_talk_index == "n1508_argenta-1-g" then 
	end
	if npc_talk_index == "n1508_argenta-1-h" then 
	end
	if npc_talk_index == "n1508_argenta-1-i" then 
	end
	if npc_talk_index == "n1508_argenta-1-j" then 
	end
	if npc_talk_index == "n1508_argenta-1-k" then 
	end
	if npc_talk_index == "n1508_argenta-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1508_argenta-8-b" then 
	end
	if npc_talk_index == "n1508_argenta-8-c" then 
	end
	if npc_talk_index == "n1508_argenta-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9416_memory_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9416, 2);
				api_quest_SetJournalStep(userObjID,9416, 1);
				api_quest_SetQuestStep(userObjID,9416, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1519_aruno--------------------------------------------------------------------------------
function mqc14_9416_memory_OnTalk_n1519_aruno(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1519_aruno-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1522_for_memory--------------------------------------------------------------------------------
function mqc14_9416_memory_OnTalk_n1522_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1522_for_memory-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1522_for_memory-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1522_for_memory-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1522_for_memory-5-b" then 
	end
	if npc_talk_index == "n1522_for_memory-5-c" then 
	end
	if npc_talk_index == "n1522_for_memory-5-d" then 
	end
	if npc_talk_index == "n1522_for_memory-5-e" then 
	end
	if npc_talk_index == "n1522_for_memory-5-f" then 
	end
	if npc_talk_index == "n1522_for_memory-5-g" then 
	end
	if npc_talk_index == "n1522_for_memory-5-h" then 
	end
	if npc_talk_index == "n1522_for_memory-5-i" then 
	end
	if npc_talk_index == "n1522_for_memory-5-j" then 
	end
	if npc_talk_index == "n1522_for_memory-5-k" then 
	end
	if npc_talk_index == "n1522_for_memory-5-l" then 
	end
	if npc_talk_index == "n1522_for_memory-5-m" then 
	end
	if npc_talk_index == "n1522_for_memory-5-n" then 
	end
	if npc_talk_index == "n1522_for_memory-5-o" then 
	end
	if npc_talk_index == "n1522_for_memory-5-p" then 
	end
	if npc_talk_index == "n1522_for_memory-5-q" then 
	end
	if npc_talk_index == "n1522_for_memory-5-r" then 
	end
	if npc_talk_index == "n1522_for_memory-5-s" then 
	end
	if npc_talk_index == "n1522_for_memory-5-t" then 
	end
	if npc_talk_index == "n1522_for_memory-5-u" then 
	end
	if npc_talk_index == "n1522_for_memory-5-v" then 
	end
	if npc_talk_index == "n1522_for_memory-5-w" then 
	end
	if npc_talk_index == "n1522_for_memory-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
	end
	if npc_talk_index == "n1522_for_memory-6-b" then 
	end
	if npc_talk_index == "n1522_for_memory-6-c" then 
	end
	if npc_talk_index == "n1522_for_memory-6-d" then 
	end
	if npc_talk_index == "n1522_for_memory-6-e" then 
	end
	if npc_talk_index == "n1522_for_memory-6-f" then 
	end
	if npc_talk_index == "n1522_for_memory-6-g" then 
	end
	if npc_talk_index == "n1522_for_memory-6-h" then 
	end
	if npc_talk_index == "n1522_for_memory-6-i" then 
	end
	if npc_talk_index == "n1522_for_memory-6-j" then 
	end
	if npc_talk_index == "n1522_for_memory-6-k" then 
	end
	if npc_talk_index == "n1522_for_memory-6-l" then 
	end
	if npc_talk_index == "n1522_for_memory-6-m" then 
	end
	if npc_talk_index == "n1522_for_memory-6-n" then 
	end
	if npc_talk_index == "n1522_for_memory-6-o" then 
	end
	if npc_talk_index == "n1522_for_memory-6-p" then 
	end
	if npc_talk_index == "n1522_for_memory-6-q" then 
	end
	if npc_talk_index == "n1522_for_memory-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1523_aruno--------------------------------------------------------------------------------
function mqc14_9416_memory_OnTalk_n1523_aruno(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n1523_aruno-10";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1523_aruno-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1523_aruno-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1523_aruno-10-b" then 
	end
	if npc_talk_index == "n1523_aruno-10-c" then 
	end
	if npc_talk_index == "n1523_aruno-10-d" then 
	end
	if npc_talk_index == "n1523_aruno-10-e" then 
	end
	if npc_talk_index == "n1523_aruno-10-f" then 
	end
	if npc_talk_index == "n1523_aruno-10-g" then 
	end
	if npc_talk_index == "n1523_aruno-10-h" then 
	end
	if npc_talk_index == "n1523_aruno-10-i" then 
	end
	if npc_talk_index == "n1523_aruno-10-j" then 
	end
	if npc_talk_index == "n1523_aruno-10-k" then 
	end
	if npc_talk_index == "n1523_aruno-10-l" then 
	end
	if npc_talk_index == "n1523_aruno-10-m" then 
	end
	if npc_talk_index == "n1523_aruno-10-n" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94160, true);
				 api_quest_RewardQuestUser(userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94160, true);
				 api_quest_RewardQuestUser(userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94160, true);
				 api_quest_RewardQuestUser(userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94160, true);
				 api_quest_RewardQuestUser(userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94160, true);
				 api_quest_RewardQuestUser(userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94160, true);
				 api_quest_RewardQuestUser(userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94160, true);
				 api_quest_RewardQuestUser(userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94160, true);
				 api_quest_RewardQuestUser(userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94160, true);
				 api_quest_RewardQuestUser(userObjID, 94160, questID, 1);
			 end 
	end
	if npc_talk_index == "n1523_aruno-10-o" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9417, 2);
					api_quest_SetQuestStep(userObjID, 9417, 1);
					api_quest_SetJournalStep(userObjID, 9417, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1523_aruno-10-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1523_aruno-1", "mqc14_9417_hope.xml");
			return;
		end
	end
	if npc_talk_index == "n1523_aruno-3-b" then 
	end
	if npc_talk_index == "n1523_aruno-3-c" then 
	end
	if npc_talk_index == "n1523_aruno-3-d" then 
	end
	if npc_talk_index == "n1523_aruno-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9416_memory_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9416);
end

function mqc14_9416_memory_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9416);
end

function mqc14_9416_memory_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9416);
	local questID=9416;
end

function mqc14_9416_memory_OnRemoteStart( userObjID, questID )
end

function mqc14_9416_memory_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9416_memory_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9416, 2);
				api_quest_SetJournalStep(userObjID,9416, 1);
				api_quest_SetQuestStep(userObjID,9416, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</VillageServer>

<GameServer>
function mqc14_9416_memory_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1508 then
		mqc14_9416_memory_OnTalk_n1508_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9416_memory_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1519 then
		mqc14_9416_memory_OnTalk_n1519_aruno( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1522 then
		mqc14_9416_memory_OnTalk_n1522_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1523 then
		mqc14_9416_memory_OnTalk_n1523_aruno( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1508_argenta--------------------------------------------------------------------------------
function mqc14_9416_memory_OnTalk_n1508_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1508_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1508_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1508_argenta-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1508_argenta-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1508_argenta-1-b" then 
	end
	if npc_talk_index == "n1508_argenta-1-c" then 
	end
	if npc_talk_index == "n1508_argenta-1-d" then 
	end
	if npc_talk_index == "n1508_argenta-1-e" then 
	end
	if npc_talk_index == "n1508_argenta-1-e" then 
	end
	if npc_talk_index == "n1508_argenta-1-e" then 
	end
	if npc_talk_index == "n1508_argenta-1-f" then 
	end
	if npc_talk_index == "n1508_argenta-1-g" then 
	end
	if npc_talk_index == "n1508_argenta-1-h" then 
	end
	if npc_talk_index == "n1508_argenta-1-i" then 
	end
	if npc_talk_index == "n1508_argenta-1-j" then 
	end
	if npc_talk_index == "n1508_argenta-1-k" then 
	end
	if npc_talk_index == "n1508_argenta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1508_argenta-8-b" then 
	end
	if npc_talk_index == "n1508_argenta-8-c" then 
	end
	if npc_talk_index == "n1508_argenta-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9416_memory_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9416, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9416, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9416, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1519_aruno--------------------------------------------------------------------------------
function mqc14_9416_memory_OnTalk_n1519_aruno( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1519_aruno-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1522_for_memory--------------------------------------------------------------------------------
function mqc14_9416_memory_OnTalk_n1522_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1522_for_memory-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1522_for_memory-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1522_for_memory-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1522_for_memory-5-b" then 
	end
	if npc_talk_index == "n1522_for_memory-5-c" then 
	end
	if npc_talk_index == "n1522_for_memory-5-d" then 
	end
	if npc_talk_index == "n1522_for_memory-5-e" then 
	end
	if npc_talk_index == "n1522_for_memory-5-f" then 
	end
	if npc_talk_index == "n1522_for_memory-5-g" then 
	end
	if npc_talk_index == "n1522_for_memory-5-h" then 
	end
	if npc_talk_index == "n1522_for_memory-5-i" then 
	end
	if npc_talk_index == "n1522_for_memory-5-j" then 
	end
	if npc_talk_index == "n1522_for_memory-5-k" then 
	end
	if npc_talk_index == "n1522_for_memory-5-l" then 
	end
	if npc_talk_index == "n1522_for_memory-5-m" then 
	end
	if npc_talk_index == "n1522_for_memory-5-n" then 
	end
	if npc_talk_index == "n1522_for_memory-5-o" then 
	end
	if npc_talk_index == "n1522_for_memory-5-p" then 
	end
	if npc_talk_index == "n1522_for_memory-5-q" then 
	end
	if npc_talk_index == "n1522_for_memory-5-r" then 
	end
	if npc_talk_index == "n1522_for_memory-5-s" then 
	end
	if npc_talk_index == "n1522_for_memory-5-t" then 
	end
	if npc_talk_index == "n1522_for_memory-5-u" then 
	end
	if npc_talk_index == "n1522_for_memory-5-v" then 
	end
	if npc_talk_index == "n1522_for_memory-5-w" then 
	end
	if npc_talk_index == "n1522_for_memory-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
	end
	if npc_talk_index == "n1522_for_memory-6-b" then 
	end
	if npc_talk_index == "n1522_for_memory-6-c" then 
	end
	if npc_talk_index == "n1522_for_memory-6-d" then 
	end
	if npc_talk_index == "n1522_for_memory-6-e" then 
	end
	if npc_talk_index == "n1522_for_memory-6-f" then 
	end
	if npc_talk_index == "n1522_for_memory-6-g" then 
	end
	if npc_talk_index == "n1522_for_memory-6-h" then 
	end
	if npc_talk_index == "n1522_for_memory-6-i" then 
	end
	if npc_talk_index == "n1522_for_memory-6-j" then 
	end
	if npc_talk_index == "n1522_for_memory-6-k" then 
	end
	if npc_talk_index == "n1522_for_memory-6-l" then 
	end
	if npc_talk_index == "n1522_for_memory-6-m" then 
	end
	if npc_talk_index == "n1522_for_memory-6-n" then 
	end
	if npc_talk_index == "n1522_for_memory-6-o" then 
	end
	if npc_talk_index == "n1522_for_memory-6-p" then 
	end
	if npc_talk_index == "n1522_for_memory-6-q" then 
	end
	if npc_talk_index == "n1522_for_memory-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1523_aruno--------------------------------------------------------------------------------
function mqc14_9416_memory_OnTalk_n1523_aruno( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n1523_aruno-10";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1523_aruno-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1523_aruno-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1523_aruno-10-b" then 
	end
	if npc_talk_index == "n1523_aruno-10-c" then 
	end
	if npc_talk_index == "n1523_aruno-10-d" then 
	end
	if npc_talk_index == "n1523_aruno-10-e" then 
	end
	if npc_talk_index == "n1523_aruno-10-f" then 
	end
	if npc_talk_index == "n1523_aruno-10-g" then 
	end
	if npc_talk_index == "n1523_aruno-10-h" then 
	end
	if npc_talk_index == "n1523_aruno-10-i" then 
	end
	if npc_talk_index == "n1523_aruno-10-j" then 
	end
	if npc_talk_index == "n1523_aruno-10-k" then 
	end
	if npc_talk_index == "n1523_aruno-10-l" then 
	end
	if npc_talk_index == "n1523_aruno-10-m" then 
	end
	if npc_talk_index == "n1523_aruno-10-n" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94160, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94160, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94160, questID, 1);
			 end 
	end
	if npc_talk_index == "n1523_aruno-10-o" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9417, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9417, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9417, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1523_aruno-10-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1523_aruno-1", "mqc14_9417_hope.xml");
			return;
		end
	end
	if npc_talk_index == "n1523_aruno-3-b" then 
	end
	if npc_talk_index == "n1523_aruno-3-c" then 
	end
	if npc_talk_index == "n1523_aruno-3-d" then 
	end
	if npc_talk_index == "n1523_aruno-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9416_memory_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9416);
end

function mqc14_9416_memory_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9416);
end

function mqc14_9416_memory_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9416);
	local questID=9416;
end

function mqc14_9416_memory_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9416_memory_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9416_memory_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9416, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9416, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9416, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</GameServer>