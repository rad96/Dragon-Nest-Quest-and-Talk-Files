<VillageServer>

function mq34_9288_forest_that_has_lost_the_origin_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1085 then
		mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1085_rojalin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-a" then
				api_quest_AddQuest(userObjID,9288, 2);
				api_quest_SetJournalStep(userObjID,9288, 1);
				api_quest_SetQuestStep(userObjID,9288, 1);

	end
	if npc_talk_index == "n1047_argenta-3-b" then 
	end
	if npc_talk_index == "n1047_argenta-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92881, true);
				 api_quest_RewardQuestUser(userObjID, 92881, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92881, true);
				 api_quest_RewardQuestUser(userObjID, 92881, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92883, true);
				 api_quest_RewardQuestUser(userObjID, 92883, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92884, true);
				 api_quest_RewardQuestUser(userObjID, 92884, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92885, true);
				 api_quest_RewardQuestUser(userObjID, 92885, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92886, true);
				 api_quest_RewardQuestUser(userObjID, 92886, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92887, true);
				 api_quest_RewardQuestUser(userObjID, 92887, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92888, true);
				 api_quest_RewardQuestUser(userObjID, 92888, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92889, true);
				 api_quest_RewardQuestUser(userObjID, 92889, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9289, 2);
					api_quest_SetQuestStep(userObjID, 9289, 1);
					api_quest_SetJournalStep(userObjID, 9289, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1047_argenta-1", "mq34_9289_gold_dragon_returns.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1065_geraint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1065_geraint_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9288, 2);
				api_quest_SetJournalStep(userObjID,9288, 1);
				api_quest_SetQuestStep(userObjID,9288, 1);
				npc_talk_index = "n1065_geraint_kid-1";

	end
	if npc_talk_index == "n1065_geraint_kid-1-chk_job_01" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1065_geraint_kid-1-a";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1065_geraint_kid-1-a";

				else
									npc_talk_index = "n1065_geraint_kid-1-m";

				end

				end
	end
	if npc_talk_index == "n1065_geraint_kid-1-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-j" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-k" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-l" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400398, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400398, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1065_geraint_kid-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1085_rojalin--------------------------------------------------------------------------------
function mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1085_rojalin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1085_rojalin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1085_rojalin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1085_rojalin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1085_rojalin-2-b" then 
	end
	if npc_talk_index == "n1085_rojalin-2-c" then 
	end
	if npc_talk_index == "n1085_rojalin-2-d" then 
	end
	if npc_talk_index == "n1085_rojalin-2-e" then 
	end
	if npc_talk_index == "n1085_rojalin-2-f" then 
	end
	if npc_talk_index == "n1085_rojalin-2-f" then 
	end
	if npc_talk_index == "n1085_rojalin-2-g" then 
	end
	if npc_talk_index == "n1085_rojalin-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1085_rojalin-2-a" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9288_forest_that_has_lost_the_origin_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9288);
end

function mq34_9288_forest_that_has_lost_the_origin_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9288);
end

function mq34_9288_forest_that_has_lost_the_origin_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9288);
	local questID=9288;
end

function mq34_9288_forest_that_has_lost_the_origin_OnRemoteStart( userObjID, questID )
end

function mq34_9288_forest_that_has_lost_the_origin_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9288_forest_that_has_lost_the_origin_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9288, 2);
				api_quest_SetJournalStep(userObjID,9288, 1);
				api_quest_SetQuestStep(userObjID,9288, 1);
				npc_talk_index = "n1065_geraint_kid-1";
end

</VillageServer>

<GameServer>
function mq34_9288_forest_that_has_lost_the_origin_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1085 then
		mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1085_rojalin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-a" then
				api_quest_AddQuest( pRoom, userObjID,9288, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9288, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9288, 1);

	end
	if npc_talk_index == "n1047_argenta-3-b" then 
	end
	if npc_talk_index == "n1047_argenta-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92881, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92881, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92881, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92881, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92883, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92883, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92884, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92884, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92885, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92885, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92886, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92886, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92887, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92887, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92888, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92888, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92889, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92889, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9289, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9289, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9289, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1047_argenta-1", "mq34_9289_gold_dragon_returns.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1065_geraint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1065_geraint_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9288, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9288, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9288, 1);
				npc_talk_index = "n1065_geraint_kid-1";

	end
	if npc_talk_index == "n1065_geraint_kid-1-chk_job_01" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1065_geraint_kid-1-a";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1065_geraint_kid-1-a";

				else
									npc_talk_index = "n1065_geraint_kid-1-m";

				end

				end
	end
	if npc_talk_index == "n1065_geraint_kid-1-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-j" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-k" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-l" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400398, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400398, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1065_geraint_kid-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1085_rojalin--------------------------------------------------------------------------------
function mq34_9288_forest_that_has_lost_the_origin_OnTalk_n1085_rojalin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1085_rojalin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1085_rojalin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1085_rojalin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1085_rojalin-2-b" then 
	end
	if npc_talk_index == "n1085_rojalin-2-c" then 
	end
	if npc_talk_index == "n1085_rojalin-2-d" then 
	end
	if npc_talk_index == "n1085_rojalin-2-e" then 
	end
	if npc_talk_index == "n1085_rojalin-2-f" then 
	end
	if npc_talk_index == "n1085_rojalin-2-f" then 
	end
	if npc_talk_index == "n1085_rojalin-2-g" then 
	end
	if npc_talk_index == "n1085_rojalin-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1085_rojalin-2-a" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9288_forest_that_has_lost_the_origin_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9288);
end

function mq34_9288_forest_that_has_lost_the_origin_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9288);
end

function mq34_9288_forest_that_has_lost_the_origin_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9288);
	local questID=9288;
end

function mq34_9288_forest_that_has_lost_the_origin_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9288_forest_that_has_lost_the_origin_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9288_forest_that_has_lost_the_origin_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9288, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9288, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9288, 1);
				npc_talk_index = "n1065_geraint_kid-1";
end

</GameServer>