<VillageServer>

function sqc06_4726_figth_with_strength_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1897 then
		sqc06_4726_figth_with_strength_OnTalk_n1897_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1906 then
		sqc06_4726_figth_with_strength_OnTalk_n1906_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1908 then
		sqc06_4726_figth_with_strength_OnTalk_n1908_machina(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 703 then
		sqc06_4726_figth_with_strength_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1897_shaolong--------------------------------------------------------------------------------
function sqc06_4726_figth_with_strength_OnTalk_n1897_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1897_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1897_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1897_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1897_shaolong-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1897_shaolong-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47260, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47260, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47260, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47260, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47260, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47260, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47260, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47260, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47260, false);
			 end 

	end
	if npc_talk_index == "n1897_shaolong-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4726, 1);
				api_quest_SetJournalStep(userObjID,4726, 1);
				api_quest_SetQuestStep(userObjID,4726, 1);
				npc_talk_index = "n1897_shaolong-1";

	end
	if npc_talk_index == "n1897_shaolong-1-check_comp01" then 
				if api_user_GetUserJobID(userObjID) == 78  then
									npc_talk_index = "n1897_shaolong-7";
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 6);

				else
									if api_user_GetUserJobID(userObjID) == 79  then
									npc_talk_index = "n1897_shaolong-7";
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 6);

				else
									if api_user_GetUserJobID(userObjID) == 77  then
									npc_talk_index = "n1897_shaolong-1-b";

				else
									npc_talk_index = "n1897_shaolong-1-d";

				end

				end

				end
	end
	if npc_talk_index == "n1897_shaolong-1-c" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1897_shaolong-7-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47260, true);
				 api_quest_RewardQuestUser(userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47260, true);
				 api_quest_RewardQuestUser(userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47260, true);
				 api_quest_RewardQuestUser(userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47260, true);
				 api_quest_RewardQuestUser(userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47260, true);
				 api_quest_RewardQuestUser(userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47260, true);
				 api_quest_RewardQuestUser(userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47260, true);
				 api_quest_RewardQuestUser(userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47260, true);
				 api_quest_RewardQuestUser(userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47260, true);
				 api_quest_RewardQuestUser(userObjID, 47260, questID, 1);
			 end 
	end
	if npc_talk_index == "n1897_shaolong-7-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1906_shaolong--------------------------------------------------------------------------------
function sqc06_4726_figth_with_strength_OnTalk_n1906_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1906_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1906_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1906_shaolong-2-check_move_1" then 
				if api_user_GetUserLevel(userObjID) >= 45 then
									if api_user_GetPartymemberCount(userObjID) == 1  then
									api_user_ChangeMap(userObjID,13512,1);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									npc_talk_index = "n1906_shaolong-2-b";

				end

				else
									npc_talk_index = "n1906_shaolong-2-c";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1908_machina--------------------------------------------------------------------------------
function sqc06_4726_figth_with_strength_OnTalk_n1908_machina(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1908_machina-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1908_machina-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1908_machina-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1908_machina-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1908_machina-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1908_machina-3-b" then 
	end
	if npc_talk_index == "n1908_machina-3-c" then 
	end
	if npc_talk_index == "n1908_machina-3-d" then 
	end
	if npc_talk_index == "n1908_machina-3-e" then 
	end
	if npc_talk_index == "n1908_machina-3-f" then 
	end
	if npc_talk_index == "n1908_machina-3-g" then 
	end
	if npc_talk_index == "n1908_machina-3-h" then 
	end
	if npc_talk_index == "n1908_machina-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1908_machina-4-open_ui4" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1908_machina-6-open_ui_5_end" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1908_machina-6-open_ui_5_again" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1908_machina-7-b" then 
	end
	if npc_talk_index == "n1908_machina-7-c" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n703_book_doctor--------------------------------------------------------------------------------
function sqc06_4726_figth_with_strength_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc06_4726_figth_with_strength_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4726);
end

function sqc06_4726_figth_with_strength_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4726);
end

function sqc06_4726_figth_with_strength_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4726);
	local questID=4726;
end

function sqc06_4726_figth_with_strength_OnRemoteStart( userObjID, questID )
end

function sqc06_4726_figth_with_strength_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc06_4726_figth_with_strength_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4726, 1);
				api_quest_SetJournalStep(userObjID,4726, 1);
				api_quest_SetQuestStep(userObjID,4726, 1);
				npc_talk_index = "n1897_shaolong-1";
end

</VillageServer>

<GameServer>
function sqc06_4726_figth_with_strength_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1897 then
		sqc06_4726_figth_with_strength_OnTalk_n1897_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1906 then
		sqc06_4726_figth_with_strength_OnTalk_n1906_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1908 then
		sqc06_4726_figth_with_strength_OnTalk_n1908_machina( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 703 then
		sqc06_4726_figth_with_strength_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1897_shaolong--------------------------------------------------------------------------------
function sqc06_4726_figth_with_strength_OnTalk_n1897_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1897_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1897_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1897_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1897_shaolong-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1897_shaolong-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, false);
			 end 

	end
	if npc_talk_index == "n1897_shaolong-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4726, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4726, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4726, 1);
				npc_talk_index = "n1897_shaolong-1";

	end
	if npc_talk_index == "n1897_shaolong-1-check_comp01" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 78  then
									npc_talk_index = "n1897_shaolong-7";
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 79  then
									npc_talk_index = "n1897_shaolong-7";
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 77  then
									npc_talk_index = "n1897_shaolong-1-b";

				else
									npc_talk_index = "n1897_shaolong-1-d";

				end

				end

				end
	end
	if npc_talk_index == "n1897_shaolong-1-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1897_shaolong-7-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47260, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47260, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47260, questID, 1);
			 end 
	end
	if npc_talk_index == "n1897_shaolong-7-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1906_shaolong--------------------------------------------------------------------------------
function sqc06_4726_figth_with_strength_OnTalk_n1906_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1906_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1906_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1906_shaolong-2-check_move_1" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 45 then
									if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									api_user_ChangeMap( pRoom, userObjID,13512,1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									npc_talk_index = "n1906_shaolong-2-b";

				end

				else
									npc_talk_index = "n1906_shaolong-2-c";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1908_machina--------------------------------------------------------------------------------
function sqc06_4726_figth_with_strength_OnTalk_n1908_machina( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1908_machina-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1908_machina-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1908_machina-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1908_machina-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1908_machina-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1908_machina-3-b" then 
	end
	if npc_talk_index == "n1908_machina-3-c" then 
	end
	if npc_talk_index == "n1908_machina-3-d" then 
	end
	if npc_talk_index == "n1908_machina-3-e" then 
	end
	if npc_talk_index == "n1908_machina-3-f" then 
	end
	if npc_talk_index == "n1908_machina-3-g" then 
	end
	if npc_talk_index == "n1908_machina-3-h" then 
	end
	if npc_talk_index == "n1908_machina-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1908_machina-4-open_ui4" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1908_machina-6-open_ui_5_end" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1908_machina-6-open_ui_5_again" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1908_machina-7-b" then 
	end
	if npc_talk_index == "n1908_machina-7-c" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n703_book_doctor--------------------------------------------------------------------------------
function sqc06_4726_figth_with_strength_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc06_4726_figth_with_strength_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4726);
end

function sqc06_4726_figth_with_strength_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4726);
end

function sqc06_4726_figth_with_strength_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4726);
	local questID=4726;
end

function sqc06_4726_figth_with_strength_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc06_4726_figth_with_strength_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc06_4726_figth_with_strength_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4726, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4726, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4726, 1);
				npc_talk_index = "n1897_shaolong-1";
end

</GameServer>