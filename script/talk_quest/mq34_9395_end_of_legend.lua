<VillageServer>

function mq34_9395_end_of_legend_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1234 then
		mq34_9395_end_of_legend_OnTalk_n1234_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq34_9395_end_of_legend_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 249 then
		mq34_9395_end_of_legend_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1235 then
		mq34_9395_end_of_legend_OnTalk_n1235_wanderer_elf_riana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1232 then
		mq34_9395_end_of_legend_OnTalk_n1232_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1234_white_dragon--------------------------------------------------------------------------------
function mq34_9395_end_of_legend_OnTalk_n1234_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1234_white_dragon-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1234_white_dragon-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1234_white_dragon-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1234_white_dragon-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1234_white_dragon-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,9395, 2);
				api_quest_SetJournalStep(userObjID,9395, 1);
				api_quest_SetQuestStep(userObjID,9395, 1);
				npc_talk_index = "n1232_white_dragon-1";

	end
	if npc_talk_index == "n1234_white_dragon-4-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-f" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-f" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-f" then 
	end
	if npc_talk_index == "n1234_white_dragon-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1234_white_dragon-5-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-class_check" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1234_white_dragon-5-e";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1234_white_dragon-5-g";

				else
									npc_talk_index = "n1234_white_dragon-5-c";

				end

				end
	end
	if npc_talk_index == "n1234_white_dragon-5-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-class_check_1" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n1234_white_dragon-5-i";

				else
									npc_talk_index = "n1234_white_dragon-5-j";

				end
	end
	if npc_talk_index == "n1234_white_dragon-5-f" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-j" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-h" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-j" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-j" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-k" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-l" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-m" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-class_check_2" then 
				if api_user_GetUserClassID(userObjID) == 2 then
									npc_talk_index = "n1234_white_dragon-5-o";

				else
									if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1234_white_dragon-5-p";

				else
									if api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n1234_white_dragon-5-q";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1234_white_dragon-5-r";

				else
									npc_talk_index = "n1234_white_dragon-5-n";

				end

				end

				end

				end
	end
	if npc_talk_index == "n1234_white_dragon-5-t" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-t" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-t" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-t" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-s" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-t" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93950, true);
				 api_quest_RewardQuestUser(userObjID, 93950, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-u" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq34_9395_end_of_legend_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n822_teramai-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n822_teramai-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-1-party1" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n822_teramai-1-b";

				else
									npc_talk_index = "n822_teramai-1-c";

				end
	end
	if npc_talk_index == "n822_teramai-1-b" then 
	end
	if npc_talk_index == "n822_teramai-1-cutscene" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_user_ChangeMap(userObjID,13522,1);
	end
	if npc_talk_index == "n822_teramai-2-cutscene2" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_user_ChangeMap(userObjID,13522,1);
				npc_talk_index = "n822_teramai-2-a";

				else
									npc_talk_index = "n822_teramai-2-b";

				end
	end
	if npc_talk_index == "n822_teramai-3-b" then 
	end
	if npc_talk_index == "n822_teramai-3-c" then 
	end
	if npc_talk_index == "n822_teramai-3-d" then 
	end
	if npc_talk_index == "n822_teramai-3-e" then 
	end
	if npc_talk_index == "n822_teramai-3-f" then 
	end
	if npc_talk_index == "n822_teramai-3-g" then 
	end
	if npc_talk_index == "n822_teramai-3-h" then 
	end
	if npc_talk_index == "n822_teramai-3-i" then 
	end
	if npc_talk_index == "n822_teramai-3-j" then 
	end
	if npc_talk_index == "n822_teramai-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n822_teramai-4-class_chk" then 
				if api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n822_teramai-4-b";

				else
									npc_talk_index = "n822_teramai-4-a";

				end
	end
	if npc_talk_index == "n822_teramai-4-c" then 
	end
	if npc_talk_index == "n822_teramai-4-d" then 
	end
	if npc_talk_index == "n822_teramai-4-e" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n249_saint_guard--------------------------------------------------------------------------------
function mq34_9395_end_of_legend_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1235_wanderer_elf_riana--------------------------------------------------------------------------------
function mq34_9395_end_of_legend_OnTalk_n1235_wanderer_elf_riana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1235_wanderer_elf_riana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1232_white_dragon--------------------------------------------------------------------------------
function mq34_9395_end_of_legend_OnTalk_n1232_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1232_white_dragon-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1232_white_dragon-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1232_white_dragon-accepting-acceptted_1" then
				api_quest_AddQuest(userObjID,9395, 2);
				api_quest_SetJournalStep(userObjID,9395, 1);
				api_quest_SetQuestStep(userObjID,9395, 1);
				npc_talk_index = "n1232_white_dragon-1";

	end
	if npc_talk_index == "n1232_white_dragon-1-b" then 
	end
	if npc_talk_index == "n1232_white_dragon-1-c" then 
	end
	if npc_talk_index == "n1232_white_dragon-1-d" then 
	end
	if npc_talk_index == "n1232_white_dragon-1-e" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9395_end_of_legend_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9395);
end

function mq34_9395_end_of_legend_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9395);
end

function mq34_9395_end_of_legend_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9395);
	local questID=9395;
end

function mq34_9395_end_of_legend_OnRemoteStart( userObjID, questID )
end

function mq34_9395_end_of_legend_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9395_end_of_legend_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mq34_9395_end_of_legend_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1234 then
		mq34_9395_end_of_legend_OnTalk_n1234_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq34_9395_end_of_legend_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 249 then
		mq34_9395_end_of_legend_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1235 then
		mq34_9395_end_of_legend_OnTalk_n1235_wanderer_elf_riana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1232 then
		mq34_9395_end_of_legend_OnTalk_n1232_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1234_white_dragon--------------------------------------------------------------------------------
function mq34_9395_end_of_legend_OnTalk_n1234_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1234_white_dragon-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1234_white_dragon-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1234_white_dragon-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1234_white_dragon-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1234_white_dragon-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,9395, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9395, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9395, 1);
				npc_talk_index = "n1232_white_dragon-1";

	end
	if npc_talk_index == "n1234_white_dragon-4-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-f" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-f" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-f" then 
	end
	if npc_talk_index == "n1234_white_dragon-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1234_white_dragon-5-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1234_white_dragon-5-e";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1234_white_dragon-5-g";

				else
									npc_talk_index = "n1234_white_dragon-5-c";

				end

				end
	end
	if npc_talk_index == "n1234_white_dragon-5-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-class_check_1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n1234_white_dragon-5-i";

				else
									npc_talk_index = "n1234_white_dragon-5-j";

				end
	end
	if npc_talk_index == "n1234_white_dragon-5-f" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-j" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-h" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-j" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-j" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-k" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-l" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-m" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-class_check_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									npc_talk_index = "n1234_white_dragon-5-o";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1234_white_dragon-5-p";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n1234_white_dragon-5-q";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1234_white_dragon-5-r";

				else
									npc_talk_index = "n1234_white_dragon-5-n";

				end

				end

				end

				end
	end
	if npc_talk_index == "n1234_white_dragon-5-t" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-t" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-t" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-t" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-s" then 
	end
	if npc_talk_index == "n1234_white_dragon-5-t" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93950, questID, 1);
			 end 
	end
	if npc_talk_index == "n1234_white_dragon-5-u" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq34_9395_end_of_legend_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n822_teramai-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n822_teramai-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-1-party1" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n822_teramai-1-b";

				else
									npc_talk_index = "n822_teramai-1-c";

				end
	end
	if npc_talk_index == "n822_teramai-1-b" then 
	end
	if npc_talk_index == "n822_teramai-1-cutscene" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_user_ChangeMap( pRoom, userObjID,13522,1);
	end
	if npc_talk_index == "n822_teramai-2-cutscene2" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_user_ChangeMap( pRoom, userObjID,13522,1);
				npc_talk_index = "n822_teramai-2-a";

				else
									npc_talk_index = "n822_teramai-2-b";

				end
	end
	if npc_talk_index == "n822_teramai-3-b" then 
	end
	if npc_talk_index == "n822_teramai-3-c" then 
	end
	if npc_talk_index == "n822_teramai-3-d" then 
	end
	if npc_talk_index == "n822_teramai-3-e" then 
	end
	if npc_talk_index == "n822_teramai-3-f" then 
	end
	if npc_talk_index == "n822_teramai-3-g" then 
	end
	if npc_talk_index == "n822_teramai-3-h" then 
	end
	if npc_talk_index == "n822_teramai-3-i" then 
	end
	if npc_talk_index == "n822_teramai-3-j" then 
	end
	if npc_talk_index == "n822_teramai-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n822_teramai-4-class_chk" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n822_teramai-4-b";

				else
									npc_talk_index = "n822_teramai-4-a";

				end
	end
	if npc_talk_index == "n822_teramai-4-c" then 
	end
	if npc_talk_index == "n822_teramai-4-d" then 
	end
	if npc_talk_index == "n822_teramai-4-e" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n249_saint_guard--------------------------------------------------------------------------------
function mq34_9395_end_of_legend_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1235_wanderer_elf_riana--------------------------------------------------------------------------------
function mq34_9395_end_of_legend_OnTalk_n1235_wanderer_elf_riana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1235_wanderer_elf_riana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1232_white_dragon--------------------------------------------------------------------------------
function mq34_9395_end_of_legend_OnTalk_n1232_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1232_white_dragon-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1232_white_dragon-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1232_white_dragon-accepting-acceptted_1" then
				api_quest_AddQuest( pRoom, userObjID,9395, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9395, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9395, 1);
				npc_talk_index = "n1232_white_dragon-1";

	end
	if npc_talk_index == "n1232_white_dragon-1-b" then 
	end
	if npc_talk_index == "n1232_white_dragon-1-c" then 
	end
	if npc_talk_index == "n1232_white_dragon-1-d" then 
	end
	if npc_talk_index == "n1232_white_dragon-1-e" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9395_end_of_legend_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9395);
end

function mq34_9395_end_of_legend_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9395);
end

function mq34_9395_end_of_legend_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9395);
	local questID=9395;
end

function mq34_9395_end_of_legend_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9395_end_of_legend_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9395_end_of_legend_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>