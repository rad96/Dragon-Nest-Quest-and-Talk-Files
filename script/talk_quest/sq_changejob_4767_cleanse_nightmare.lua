<VillageServer>

function sq_changejob_4767_cleanse_nightmare_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 954 then
		sq_changejob_4767_cleanse_nightmare_OnTalk_n954_darklair_rosetta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		sq_changejob_4767_cleanse_nightmare_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n954_darklair_rosetta--------------------------------------------------------------------------------
function sq_changejob_4767_cleanse_nightmare_OnTalk_n954_darklair_rosetta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n954_darklair_rosetta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n954_darklair_rosetta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n954_darklair_rosetta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n954_darklair_rosetta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n954_darklair_rosetta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n954_darklair_rosetta-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47670, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47670, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47670, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47670, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47670, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47670, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47670, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47670, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47670, false);
			 end 

	end
	if npc_talk_index == "n954_darklair_rosetta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4767, 1);
				api_quest_SetJournalStep(userObjID,4767, 1);
				api_quest_SetQuestStep(userObjID,4767, 1);
				npc_talk_index = "n954_darklair_rosetta-1";

	end
	if npc_talk_index == "n954_darklair_rosetta-1-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-h" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-i" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-j" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-k" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-l" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400498, 1);
	end
	if npc_talk_index == "n954_darklair_rosetta-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq_changejob_4767_cleanse_nightmare_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n044_archer_master_ishilien-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-4-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47670, true);
				 api_quest_RewardQuestUser(userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47670, true);
				 api_quest_RewardQuestUser(userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47670, true);
				 api_quest_RewardQuestUser(userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47670, true);
				 api_quest_RewardQuestUser(userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47670, true);
				 api_quest_RewardQuestUser(userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47670, true);
				 api_quest_RewardQuestUser(userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47670, true);
				 api_quest_RewardQuestUser(userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47670, true);
				 api_quest_RewardQuestUser(userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47670, true);
				 api_quest_RewardQuestUser(userObjID, 47670, questID, 1);
			 end 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4768, 1);
					api_quest_SetQuestStep(userObjID, 4768, 1);
					api_quest_SetJournalStep(userObjID, 4768, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n044_archer_master_ishilien-1", "sq_changejob_4768_another_telejia.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_changejob_4767_cleanse_nightmare_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4767);
	if qstep == 2 and CountIndex == 400498 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 600167 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400498, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400498, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 21309 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400498, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400498, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq_changejob_4767_cleanse_nightmare_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4767);
	if qstep == 2 and CountIndex == 400498 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 600167 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 21309 and Count >= TargetCount  then

	end
end

function sq_changejob_4767_cleanse_nightmare_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4767);
	local questID=4767;
end

function sq_changejob_4767_cleanse_nightmare_OnRemoteStart( userObjID, questID )
end

function sq_changejob_4767_cleanse_nightmare_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_changejob_4767_cleanse_nightmare_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4767, 1);
				api_quest_SetJournalStep(userObjID,4767, 1);
				api_quest_SetQuestStep(userObjID,4767, 1);
				npc_talk_index = "n954_darklair_rosetta-1";
end

</VillageServer>

<GameServer>
function sq_changejob_4767_cleanse_nightmare_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 954 then
		sq_changejob_4767_cleanse_nightmare_OnTalk_n954_darklair_rosetta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		sq_changejob_4767_cleanse_nightmare_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n954_darklair_rosetta--------------------------------------------------------------------------------
function sq_changejob_4767_cleanse_nightmare_OnTalk_n954_darklair_rosetta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n954_darklair_rosetta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n954_darklair_rosetta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n954_darklair_rosetta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n954_darklair_rosetta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n954_darklair_rosetta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n954_darklair_rosetta-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, false);
			 end 

	end
	if npc_talk_index == "n954_darklair_rosetta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4767, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4767, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4767, 1);
				npc_talk_index = "n954_darklair_rosetta-1";

	end
	if npc_talk_index == "n954_darklair_rosetta-1-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-h" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-i" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-j" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-k" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-1-l" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400498, 1);
	end
	if npc_talk_index == "n954_darklair_rosetta-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq_changejob_4767_cleanse_nightmare_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n044_archer_master_ishilien-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-4-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47670, questID, 1);
			 end 
	end
	if npc_talk_index == "n044_archer_master_ishilien-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4768, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4768, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4768, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n044_archer_master_ishilien-1", "sq_changejob_4768_another_telejia.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_changejob_4767_cleanse_nightmare_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4767);
	if qstep == 2 and CountIndex == 400498 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 600167 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400498, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400498, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 21309 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400498, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400498, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq_changejob_4767_cleanse_nightmare_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4767);
	if qstep == 2 and CountIndex == 400498 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 600167 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 21309 and Count >= TargetCount  then

	end
end

function sq_changejob_4767_cleanse_nightmare_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4767);
	local questID=4767;
end

function sq_changejob_4767_cleanse_nightmare_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_changejob_4767_cleanse_nightmare_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_changejob_4767_cleanse_nightmare_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4767, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4767, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4767, 1);
				npc_talk_index = "n954_darklair_rosetta-1";
end

</GameServer>