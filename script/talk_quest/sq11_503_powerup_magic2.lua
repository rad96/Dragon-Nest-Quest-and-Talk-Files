<VillageServer>

function sq11_503_powerup_magic2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 45 then
		sq11_503_powerup_magic2_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_503_powerup_magic2_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5030, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5030, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5030, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5030, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5030, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5030, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5030, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5030, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5030, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest(userObjID,503, 1);
				api_quest_SetJournalStep(userObjID,503, 1);
				api_quest_SetQuestStep(userObjID,503, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 393, 1 );
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n045_soceress_master_stella-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5030, true);
				 api_quest_RewardQuestUser(userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5030, true);
				 api_quest_RewardQuestUser(userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5030, true);
				 api_quest_RewardQuestUser(userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5030, true);
				 api_quest_RewardQuestUser(userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5030, true);
				 api_quest_RewardQuestUser(userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5030, true);
				 api_quest_RewardQuestUser(userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5030, true);
				 api_quest_RewardQuestUser(userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5030, true);
				 api_quest_RewardQuestUser(userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5030, true);
				 api_quest_RewardQuestUser(userObjID, 5030, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 504, 1);
					api_quest_SetQuestStep(userObjID, 504, 1);
					api_quest_SetJournalStep(userObjID, 504, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-3-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n045_soceress_master_stella-1", "sq11_504_powerup_magic3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_503_powerup_magic2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 503);
	 local qstep= 1; 
	 if qstep == 1 then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 5 then 
				if  api_user_GetStageConstructionLevel( userObjID ) >= 0 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end
		 end 
	 end 
end

function sq11_503_powerup_magic2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 503);
end

function sq11_503_powerup_magic2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 503);
	local questID=503;
end

function sq11_503_powerup_magic2_OnRemoteStart( userObjID, questID )
end

function sq11_503_powerup_magic2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_503_powerup_magic2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,503, 1);
				api_quest_SetJournalStep(userObjID,503, 1);
				api_quest_SetQuestStep(userObjID,503, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
end

</VillageServer>

<GameServer>
function sq11_503_powerup_magic2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 45 then
		sq11_503_powerup_magic2_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_503_powerup_magic2_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,503, 1);
				api_quest_SetJournalStep( pRoom, userObjID,503, 1);
				api_quest_SetQuestStep( pRoom, userObjID,503, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 393, 1 );
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n045_soceress_master_stella-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5030, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 504, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 504, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 504, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-3-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-1", "sq11_504_powerup_magic3.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_503_powerup_magic2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 503);
	 local qstep= 1; 
	 if qstep == 1 then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 5 then 
				if  api_user_GetStageConstructionLevel( pRoom,  userObjID ) >= 0 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end
		 end 
	 end 
end

function sq11_503_powerup_magic2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 503);
end

function sq11_503_powerup_magic2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 503);
	local questID=503;
end

function sq11_503_powerup_magic2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_503_powerup_magic2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_503_powerup_magic2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,503, 1);
				api_quest_SetJournalStep( pRoom, userObjID,503, 1);
				api_quest_SetQuestStep( pRoom, userObjID,503, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
end

</GameServer>