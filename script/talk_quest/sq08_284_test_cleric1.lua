<VillageServer>

function sq08_284_test_cleric1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 183 then
		sq08_284_test_cleric1_OnTalk_n183_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 34 then
		sq08_284_test_cleric1_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n183_cleric_jake--------------------------------------------------------------------------------
function sq08_284_test_cleric1_OnTalk_n183_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n183_cleric_jake-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n183_cleric_jake-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n183_cleric_jake-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n183_cleric_jake-1-b" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-c" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-d" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-e" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-f" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-g" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-h" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-f" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-j" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-f" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-j" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-f" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-j" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-k" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-l" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-m" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n034_cleric_master_germain--------------------------------------------------------------------------------
function sq08_284_test_cleric1_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n034_cleric_master_germain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n034_cleric_master_germain-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n034_cleric_master_germain-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2840, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2840, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2840, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2840, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2840, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2840, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2840, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2840, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2840, false);
			 end 

	end
	if npc_talk_index == "n034_cleric_master_germain-accepting-acceptted" then
				api_quest_AddQuest(userObjID,284, 1);
				api_quest_SetJournalStep(userObjID,284, 1);
				api_quest_SetQuestStep(userObjID,284, 1);
				npc_talk_index = "n034_cleric_master_germain-1";

	end
	if npc_talk_index == "n034_cleric_master_germain-1-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-member1" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n034_cleric_master_germain-1-f";

				else
									npc_talk_index = "n034_cleric_master_germain-1-g";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-1-changemap1" then 
				api_user_ChangeMap(userObjID,13011,1);
	end
	if npc_talk_index == "n034_cleric_master_germain-2-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n034_cleric_master_germain-3-member2" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n034_cleric_master_germain-3-f";

				else
									npc_talk_index = "n034_cleric_master_germain-3-e";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-3-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3-changemap2" then 
				api_user_ChangeMap(userObjID,13008,1);
	end
	if npc_talk_index == "n034_cleric_master_germain-4-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2840, true);
				 api_quest_RewardQuestUser(userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2840, true);
				 api_quest_RewardQuestUser(userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2840, true);
				 api_quest_RewardQuestUser(userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2840, true);
				 api_quest_RewardQuestUser(userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2840, true);
				 api_quest_RewardQuestUser(userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2840, true);
				 api_quest_RewardQuestUser(userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2840, true);
				 api_quest_RewardQuestUser(userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2840, true);
				 api_quest_RewardQuestUser(userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2840, true);
				 api_quest_RewardQuestUser(userObjID, 2840, questID, 1);
			 end 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 285, 1);
					api_quest_SetQuestStep(userObjID, 285, 1);
					api_quest_SetJournalStep(userObjID, 285, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n034_cleric_master_germain-1", "sq08_285_test_cleric2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_284_test_cleric1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 284);
end

function sq08_284_test_cleric1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 284);
end

function sq08_284_test_cleric1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 284);
	local questID=284;
end

function sq08_284_test_cleric1_OnRemoteStart( userObjID, questID )
end

function sq08_284_test_cleric1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_284_test_cleric1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,284, 1);
				api_quest_SetJournalStep(userObjID,284, 1);
				api_quest_SetQuestStep(userObjID,284, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
end

</VillageServer>

<GameServer>
function sq08_284_test_cleric1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 183 then
		sq08_284_test_cleric1_OnTalk_n183_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 34 then
		sq08_284_test_cleric1_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n183_cleric_jake--------------------------------------------------------------------------------
function sq08_284_test_cleric1_OnTalk_n183_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n183_cleric_jake-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n183_cleric_jake-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n183_cleric_jake-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n183_cleric_jake-1-b" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-c" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-d" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-e" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-f" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-g" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-h" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-f" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-j" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-f" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-j" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-f" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-j" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-k" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-l" then 
	end
	if npc_talk_index == "n183_cleric_jake-1-m" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n034_cleric_master_germain--------------------------------------------------------------------------------
function sq08_284_test_cleric1_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n034_cleric_master_germain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n034_cleric_master_germain-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n034_cleric_master_germain-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, false);
			 end 

	end
	if npc_talk_index == "n034_cleric_master_germain-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,284, 1);
				api_quest_SetJournalStep( pRoom, userObjID,284, 1);
				api_quest_SetQuestStep( pRoom, userObjID,284, 1);
				npc_talk_index = "n034_cleric_master_germain-1";

	end
	if npc_talk_index == "n034_cleric_master_germain-1-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-member1" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n034_cleric_master_germain-1-f";

				else
									npc_talk_index = "n034_cleric_master_germain-1-g";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-1-changemap1" then 
				api_user_ChangeMap( pRoom, userObjID,13011,1);
	end
	if npc_talk_index == "n034_cleric_master_germain-2-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n034_cleric_master_germain-3-member2" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n034_cleric_master_germain-3-f";

				else
									npc_talk_index = "n034_cleric_master_germain-3-e";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-3-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-3-changemap2" then 
				api_user_ChangeMap( pRoom, userObjID,13008,1);
	end
	if npc_talk_index == "n034_cleric_master_germain-4-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2840, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2840, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2840, questID, 1);
			 end 
	end
	if npc_talk_index == "n034_cleric_master_germain-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 285, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 285, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 285, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n034_cleric_master_germain-1", "sq08_285_test_cleric2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_284_test_cleric1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 284);
end

function sq08_284_test_cleric1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 284);
end

function sq08_284_test_cleric1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 284);
	local questID=284;
end

function sq08_284_test_cleric1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_284_test_cleric1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_284_test_cleric1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,284, 1);
				api_quest_SetJournalStep( pRoom, userObjID,284, 1);
				api_quest_SetQuestStep( pRoom, userObjID,284, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
end

</GameServer>