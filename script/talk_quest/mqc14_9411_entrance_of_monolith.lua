<VillageServer>

function mqc14_9411_entrance_of_monolith_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mqc14_9411_entrance_of_monolith_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mqc14_9411_entrance_of_monolith_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1508 then
		mqc14_9411_entrance_of_monolith_OnTalk_n1508_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mqc14_9411_entrance_of_monolith_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9411, 2);
				api_quest_SetJournalStep(userObjID,9411, 1);
				api_quest_SetQuestStep(userObjID,9411, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
	end
	if npc_talk_index == "n1047_argenta-1-f" then 
	end
	if npc_talk_index == "n1047_argenta-1-g" then 
	end
	if npc_talk_index == "n1047_argenta-1-h" then 
	end
	if npc_talk_index == "n1047_argenta-1-i" then 
	end
	if npc_talk_index == "n1047_argenta-1-j" then 
	end
	if npc_talk_index == "n1047_argenta-1-k" then 
	end
	if npc_talk_index == "n1047_argenta-1-l" then 
	end
	if npc_talk_index == "n1047_argenta-1-m" then 
	end
	if npc_talk_index == "n1047_argenta-1-n" then 
	end
	if npc_talk_index == "n1047_argenta-1-o" then 
	end
	if npc_talk_index == "n1047_argenta-1-p" then 
	end
	if npc_talk_index == "n1047_argenta-1-q" then 
	end
	if npc_talk_index == "n1047_argenta-1-r" then 
	end
	if npc_talk_index == "n1047_argenta-1-s" then 
	end
	if npc_talk_index == "n1047_argenta-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-1-a" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc14_9411_entrance_of_monolith_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1508_argenta--------------------------------------------------------------------------------
function mqc14_9411_entrance_of_monolith_OnTalk_n1508_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1508_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1508_argenta-3-b" then 
	end
	if npc_talk_index == "n1508_argenta-3-c" then 
	end
	if npc_talk_index == "n1508_argenta-3-c" then 
	end
	if npc_talk_index == "n1508_argenta-3-c" then 
	end
	if npc_talk_index == "n1508_argenta-3-d" then 
	end
	if npc_talk_index == "n1508_argenta-3-d" then 
	end
	if npc_talk_index == "n1508_argenta-3-d" then 
	end
	if npc_talk_index == "n1508_argenta-3-e" then 
	end
	if npc_talk_index == "n1508_argenta-3-f" then 
	end
	if npc_talk_index == "n1508_argenta-3-g" then 
	end
	if npc_talk_index == "n1508_argenta-3-h" then 
	end
	if npc_talk_index == "n1508_argenta-3-i" then 
	end
	if npc_talk_index == "n1508_argenta-3-j" then 
	end
	if npc_talk_index == "n1508_argenta-3-k" then 
	end
	if npc_talk_index == "n1508_argenta-3-l" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94110, true);
				 api_quest_RewardQuestUser(userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94110, true);
				 api_quest_RewardQuestUser(userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94110, true);
				 api_quest_RewardQuestUser(userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94110, true);
				 api_quest_RewardQuestUser(userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94110, true);
				 api_quest_RewardQuestUser(userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94110, true);
				 api_quest_RewardQuestUser(userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94110, true);
				 api_quest_RewardQuestUser(userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94110, true);
				 api_quest_RewardQuestUser(userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94110, true);
				 api_quest_RewardQuestUser(userObjID, 94110, questID, 1);
			 end 
	end
	if npc_talk_index == "n1508_argenta-3-m" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9412, 2);
					api_quest_SetQuestStep(userObjID, 9412, 1);
					api_quest_SetJournalStep(userObjID, 9412, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1508_argenta-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1508_argenta-1", "mqc14_9412_meeting.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9411_entrance_of_monolith_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9411);
end

function mqc14_9411_entrance_of_monolith_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9411);
end

function mqc14_9411_entrance_of_monolith_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9411);
	local questID=9411;
end

function mqc14_9411_entrance_of_monolith_OnRemoteStart( userObjID, questID )
end

function mqc14_9411_entrance_of_monolith_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9411_entrance_of_monolith_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9411, 2);
				api_quest_SetJournalStep(userObjID,9411, 1);
				api_quest_SetQuestStep(userObjID,9411, 1);
				npc_talk_index = "n1047_argenta-1";
end

</VillageServer>

<GameServer>
function mqc14_9411_entrance_of_monolith_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mqc14_9411_entrance_of_monolith_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mqc14_9411_entrance_of_monolith_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1508 then
		mqc14_9411_entrance_of_monolith_OnTalk_n1508_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mqc14_9411_entrance_of_monolith_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9411, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9411, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9411, 1);
				npc_talk_index = "n1047_argenta-1";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
	end
	if npc_talk_index == "n1047_argenta-1-f" then 
	end
	if npc_talk_index == "n1047_argenta-1-g" then 
	end
	if npc_talk_index == "n1047_argenta-1-h" then 
	end
	if npc_talk_index == "n1047_argenta-1-i" then 
	end
	if npc_talk_index == "n1047_argenta-1-j" then 
	end
	if npc_talk_index == "n1047_argenta-1-k" then 
	end
	if npc_talk_index == "n1047_argenta-1-l" then 
	end
	if npc_talk_index == "n1047_argenta-1-m" then 
	end
	if npc_talk_index == "n1047_argenta-1-n" then 
	end
	if npc_talk_index == "n1047_argenta-1-o" then 
	end
	if npc_talk_index == "n1047_argenta-1-p" then 
	end
	if npc_talk_index == "n1047_argenta-1-q" then 
	end
	if npc_talk_index == "n1047_argenta-1-r" then 
	end
	if npc_talk_index == "n1047_argenta-1-s" then 
	end
	if npc_talk_index == "n1047_argenta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-1-a" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc14_9411_entrance_of_monolith_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1508_argenta--------------------------------------------------------------------------------
function mqc14_9411_entrance_of_monolith_OnTalk_n1508_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1508_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1508_argenta-3-b" then 
	end
	if npc_talk_index == "n1508_argenta-3-c" then 
	end
	if npc_talk_index == "n1508_argenta-3-c" then 
	end
	if npc_talk_index == "n1508_argenta-3-c" then 
	end
	if npc_talk_index == "n1508_argenta-3-d" then 
	end
	if npc_talk_index == "n1508_argenta-3-d" then 
	end
	if npc_talk_index == "n1508_argenta-3-d" then 
	end
	if npc_talk_index == "n1508_argenta-3-e" then 
	end
	if npc_talk_index == "n1508_argenta-3-f" then 
	end
	if npc_talk_index == "n1508_argenta-3-g" then 
	end
	if npc_talk_index == "n1508_argenta-3-h" then 
	end
	if npc_talk_index == "n1508_argenta-3-i" then 
	end
	if npc_talk_index == "n1508_argenta-3-j" then 
	end
	if npc_talk_index == "n1508_argenta-3-k" then 
	end
	if npc_talk_index == "n1508_argenta-3-l" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94110, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94110, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94110, questID, 1);
			 end 
	end
	if npc_talk_index == "n1508_argenta-3-m" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9412, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9412, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9412, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1508_argenta-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1508_argenta-1", "mqc14_9412_meeting.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9411_entrance_of_monolith_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9411);
end

function mqc14_9411_entrance_of_monolith_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9411);
end

function mqc14_9411_entrance_of_monolith_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9411);
	local questID=9411;
end

function mqc14_9411_entrance_of_monolith_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9411_entrance_of_monolith_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9411_entrance_of_monolith_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9411, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9411, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9411, 1);
				npc_talk_index = "n1047_argenta-1";
end

</GameServer>