<VillageServer>

function sq15_864_another_sword_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 708 then
		sq15_864_another_sword_OnTalk_n708_trader_molin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n708_trader_molin--------------------------------------------------------------------------------
function sq15_864_another_sword_OnTalk_n708_trader_molin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n708_trader_molin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n708_trader_molin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n708_trader_molin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n708_trader_molin-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8640, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8640, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8640, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8640, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8640, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8640, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8640, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8640, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8640, false);
			 end 

	end
	if npc_talk_index == "n708_trader_molin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,864, 1);
				api_quest_SetJournalStep(userObjID,864, 1);
				api_quest_SetQuestStep(userObjID,864, 1);
				npc_talk_index = "n708_trader_molin-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1701, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201701, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300493, 3);

	end
	if npc_talk_index == "n708_trader_molin-2-b" then 
	end
	if npc_talk_index == "n708_trader_molin-2-c" then 
	end
	if npc_talk_index == "n708_trader_molin-2-d" then 
	end
	if npc_talk_index == "n708_trader_molin-2-e" then 
	end
	if npc_talk_index == "n708_trader_molin-2-e" then 
	end
	if npc_talk_index == "n708_trader_molin-2-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8640, true);
				 api_quest_RewardQuestUser(userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8640, true);
				 api_quest_RewardQuestUser(userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8640, true);
				 api_quest_RewardQuestUser(userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8640, true);
				 api_quest_RewardQuestUser(userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8640, true);
				 api_quest_RewardQuestUser(userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8640, true);
				 api_quest_RewardQuestUser(userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8640, true);
				 api_quest_RewardQuestUser(userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8640, true);
				 api_quest_RewardQuestUser(userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8640, true);
				 api_quest_RewardQuestUser(userObjID, 8640, questID, 1);
			 end 
	end
	if npc_talk_index == "n708_trader_molin-2-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem(userObjID, 300493, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300493, api_quest_HasQuestItem(userObjID, 300493, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 865, 1);
					api_quest_SetQuestStep(userObjID, 865, 1);
					api_quest_SetJournalStep(userObjID, 865, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n708_trader_molin-1", "sq15_865_how_use.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_864_another_sword_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 864);
	if qstep == 1 and CountIndex == 1701 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300493, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300493, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201701 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300493, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300493, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300493 then

	end
end

function sq15_864_another_sword_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 864);
	if qstep == 1 and CountIndex == 1701 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201701 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300493 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function sq15_864_another_sword_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 864);
	local questID=864;
end

function sq15_864_another_sword_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1701, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201701, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300493, 3);
end

function sq15_864_another_sword_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_864_another_sword_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,864, 1);
				api_quest_SetJournalStep(userObjID,864, 1);
				api_quest_SetQuestStep(userObjID,864, 1);
				npc_talk_index = "n708_trader_molin-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1701, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201701, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300493, 3);
end

</VillageServer>

<GameServer>
function sq15_864_another_sword_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 708 then
		sq15_864_another_sword_OnTalk_n708_trader_molin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n708_trader_molin--------------------------------------------------------------------------------
function sq15_864_another_sword_OnTalk_n708_trader_molin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n708_trader_molin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n708_trader_molin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n708_trader_molin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n708_trader_molin-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, false);
			 end 

	end
	if npc_talk_index == "n708_trader_molin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,864, 1);
				api_quest_SetJournalStep( pRoom, userObjID,864, 1);
				api_quest_SetQuestStep( pRoom, userObjID,864, 1);
				npc_talk_index = "n708_trader_molin-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1701, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201701, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300493, 3);

	end
	if npc_talk_index == "n708_trader_molin-2-b" then 
	end
	if npc_talk_index == "n708_trader_molin-2-c" then 
	end
	if npc_talk_index == "n708_trader_molin-2-d" then 
	end
	if npc_talk_index == "n708_trader_molin-2-e" then 
	end
	if npc_talk_index == "n708_trader_molin-2-e" then 
	end
	if npc_talk_index == "n708_trader_molin-2-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8640, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8640, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8640, questID, 1);
			 end 
	end
	if npc_talk_index == "n708_trader_molin-2-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem( pRoom, userObjID, 300493, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300493, api_quest_HasQuestItem( pRoom, userObjID, 300493, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 865, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 865, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 865, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n708_trader_molin-1", "sq15_865_how_use.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_864_another_sword_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 864);
	if qstep == 1 and CountIndex == 1701 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300493, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300493, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201701 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300493, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300493, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300493 then

	end
end

function sq15_864_another_sword_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 864);
	if qstep == 1 and CountIndex == 1701 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201701 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300493 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function sq15_864_another_sword_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 864);
	local questID=864;
end

function sq15_864_another_sword_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1701, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201701, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300493, 3);
end

function sq15_864_another_sword_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_864_another_sword_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,864, 1);
				api_quest_SetJournalStep( pRoom, userObjID,864, 1);
				api_quest_SetQuestStep( pRoom, userObjID,864, 1);
				npc_talk_index = "n708_trader_molin-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1701, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201701, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300493, 3);
end

</GameServer>