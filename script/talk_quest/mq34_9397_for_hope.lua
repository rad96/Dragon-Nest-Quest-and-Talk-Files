<VillageServer>

function mq34_9397_for_hope_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1234 then
		mq34_9397_for_hope_OnTalk_n1234_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mq34_9397_for_hope_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1233 then
		mq34_9397_for_hope_OnTalk_n1233_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1250 then
		mq34_9397_for_hope_OnTalk_n1250_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1234_white_dragon--------------------------------------------------------------------------------
function mq34_9397_for_hope_OnTalk_n1234_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1234_white_dragon-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1234_white_dragon-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1234_white_dragon-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1234_white_dragon-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1234_white_dragon-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9397, 2);
				api_quest_SetJournalStep(userObjID,9397, 1);
				api_quest_SetQuestStep(userObjID,9397, 1);
				npc_talk_index = "n1234_white_dragon-1";

	end
	if npc_talk_index == "n1234_white_dragon-2-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-f" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-g" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-h" then 
	end
	if npc_talk_index == "n1234_white_dragon-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9397_for_hope_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1067_elder_elf-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1067_elder_elf-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-m" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-7-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93970, true);
				 api_quest_RewardQuestUser(userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93970, true);
				 api_quest_RewardQuestUser(userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93970, true);
				 api_quest_RewardQuestUser(userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93970, true);
				 api_quest_RewardQuestUser(userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93970, true);
				 api_quest_RewardQuestUser(userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93970, true);
				 api_quest_RewardQuestUser(userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93970, true);
				 api_quest_RewardQuestUser(userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93970, true);
				 api_quest_RewardQuestUser(userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93970, true);
				 api_quest_RewardQuestUser(userObjID, 93970, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-7-l" then 
				api_quest_SetJournalStep(userObjID, questID, 6);

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1233_white_dragon--------------------------------------------------------------------------------
function mq34_9397_for_hope_OnTalk_n1233_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1233_white_dragon-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1233_white_dragon-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1233_white_dragon-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1233_white_dragon-3-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1233_white_dragon-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1233_white_dragon-4-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-n" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-p" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-q" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-r" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-s" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-t" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-u" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-v" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-w" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-x" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-y" then 
	end
	if npc_talk_index == "n1233_white_dragon-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n1233_white_dragon-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1250_for_memory--------------------------------------------------------------------------------
function mq34_9397_for_hope_OnTalk_n1250_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1250_for_memory-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1250_for_memory-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1250_for_memory-6-b" then 
	end
	if npc_talk_index == "n1250_for_memory-6-c" then 
	end
	if npc_talk_index == "n1250_for_memory-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9397_for_hope_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9397);
end

function mq34_9397_for_hope_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9397);
end

function mq34_9397_for_hope_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9397);
	local questID=9397;
end

function mq34_9397_for_hope_OnRemoteStart( userObjID, questID )
end

function mq34_9397_for_hope_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9397_for_hope_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9397, 2);
				api_quest_SetJournalStep(userObjID,9397, 1);
				api_quest_SetQuestStep(userObjID,9397, 1);
				npc_talk_index = "n1234_white_dragon-1";
end

</VillageServer>

<GameServer>
function mq34_9397_for_hope_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1234 then
		mq34_9397_for_hope_OnTalk_n1234_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mq34_9397_for_hope_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1233 then
		mq34_9397_for_hope_OnTalk_n1233_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1250 then
		mq34_9397_for_hope_OnTalk_n1250_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1234_white_dragon--------------------------------------------------------------------------------
function mq34_9397_for_hope_OnTalk_n1234_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1234_white_dragon-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1234_white_dragon-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1234_white_dragon-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1234_white_dragon-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1234_white_dragon-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9397, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9397, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9397, 1);
				npc_talk_index = "n1234_white_dragon-1";

	end
	if npc_talk_index == "n1234_white_dragon-2-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-f" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-g" then 
	end
	if npc_talk_index == "n1234_white_dragon-2-h" then 
	end
	if npc_talk_index == "n1234_white_dragon-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9397_for_hope_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1067_elder_elf-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1067_elder_elf-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-m" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-7-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93970, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-7-l" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1233_white_dragon--------------------------------------------------------------------------------
function mq34_9397_for_hope_OnTalk_n1233_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1233_white_dragon-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1233_white_dragon-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1233_white_dragon-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1233_white_dragon-3-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1233_white_dragon-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1233_white_dragon-4-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-n" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-p" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-q" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-r" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-s" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-t" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-u" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-v" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-w" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-x" then 
	end
	if npc_talk_index == "n1233_white_dragon-4-y" then 
	end
	if npc_talk_index == "n1233_white_dragon-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n1233_white_dragon-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1250_for_memory--------------------------------------------------------------------------------
function mq34_9397_for_hope_OnTalk_n1250_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1250_for_memory-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1250_for_memory-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1250_for_memory-6-b" then 
	end
	if npc_talk_index == "n1250_for_memory-6-c" then 
	end
	if npc_talk_index == "n1250_for_memory-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9397_for_hope_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9397);
end

function mq34_9397_for_hope_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9397);
end

function mq34_9397_for_hope_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9397);
	local questID=9397;
end

function mq34_9397_for_hope_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9397_for_hope_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9397_for_hope_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9397, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9397, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9397, 1);
				npc_talk_index = "n1234_white_dragon-1";
end

</GameServer>