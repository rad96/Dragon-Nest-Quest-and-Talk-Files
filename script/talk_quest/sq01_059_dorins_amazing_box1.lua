<VillageServer>

function sq01_059_dorins_amazing_box1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 2 then
		sq01_059_dorins_amazing_box1_OnTalk_n002_trader_dorin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 361 then
		sq01_059_dorins_amazing_box1_OnTalk_n361_item_destroy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n002_trader_dorin--------------------------------------------------------------------------------
function sq01_059_dorins_amazing_box1_OnTalk_n002_trader_dorin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n002_trader_dorin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n002_trader_dorin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n002_trader_dorin-2";
				if api_user_HasItem(userObjID, 300152, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				npc_talk_index = "n002_trader_dorin-3";
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 4);

				else
									if api_user_HasItemWarehouse(userObjID, 300152 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-2-j";

				else
									if api_user_HasItem(userObjID, 300151, 1) >= 1 then
									npc_talk_index = "n002_trader_dorin-2-a";

				else
									if api_user_HasItemWarehouse(userObjID, 300151 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-2-j";

				else
									if api_user_HasItem(userObjID, 300150, 1) >= 1 then
									npc_talk_index = "n002_trader_dorin-2";

				else
									if api_user_HasItemWarehouse(userObjID, 300150 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-2-j";

				else
									npc_talk_index = "n002_trader_dorin-2-i";
				if api_user_CheckInvenForAddItem(userObjID, 300150, 1) == 1 then
					api_user_AddItem(userObjID, 300150, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

				end

				end

				end

				end

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n002_trader_dorin-3";
				if api_user_HasItem(userObjID, 300152, 1) >= 1 then
									npc_talk_index = "n002_trader_dorin-3";

				else
									if api_user_HasItemWarehouse(userObjID, 300152 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-3-g";

				else
									if api_user_HasItem(userObjID, 300151, 1) >= 1 then
									npc_talk_index = "n002_trader_dorin-3-f";

				else
									if api_user_HasItemWarehouse(userObjID, 300151 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-3-g";

				else
									if api_user_HasItem(userObjID, 130150, 1) >= 1 then
									npc_talk_index = "n002_trader_dorin-3-g";

				else
									if api_user_HasItemWarehouse(userObjID, 300150 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-3-g";

				else
									npc_talk_index = "n002_trader_dorin-3-e";
				if api_user_CheckInvenForAddItem(userObjID, 300151, 1) == 1 then
					api_user_AddItem(userObjID, 300151, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

				end

				end

				end

				end

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n002_trader_dorin-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 591, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 592, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 592, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 592, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 591, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 596, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 597, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 591, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 591, false);
			 end 

	end
	if npc_talk_index == "n002_trader_dorin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,59, 1);
				api_quest_SetJournalStep(userObjID,59, 1);
				api_quest_SetQuestStep(userObjID,59, 1);
				npc_talk_index = "n002_trader_dorin-1";

	end
	if npc_talk_index == "n002_trader_dorin-1-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-1-c" then 
	end
	if npc_talk_index == "n002_trader_dorin-1-d" then 
	end
	if npc_talk_index == "n002_trader_dorin-1-e" then 
				if api_user_CheckInvenForAddItem(userObjID, 300150, 1) == 1 then
					api_user_AddItem(userObjID, 300150, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300152, 30000);
	end
	if npc_talk_index == "n002_trader_dorin-2-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-class_check_2" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n002_trader_dorin-2-c";

				else
									npc_talk_index = "n002_trader_dorin-2-d";

				end
	end
	if npc_talk_index == "n002_trader_dorin-2-e" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-e" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-f" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-g" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-h" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300152, 30000);
	end
	if npc_talk_index == "n002_trader_dorin-3-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-3-c" then 
	end
	if npc_talk_index == "n002_trader_dorin-3-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 591, true);
				 api_quest_RewardQuestUser(userObjID, 591, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 592, true);
				 api_quest_RewardQuestUser(userObjID, 592, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 593, true);
				 api_quest_RewardQuestUser(userObjID, 593, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 594, true);
				 api_quest_RewardQuestUser(userObjID, 594, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 595, true);
				 api_quest_RewardQuestUser(userObjID, 595, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 596, true);
				 api_quest_RewardQuestUser(userObjID, 596, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 597, true);
				 api_quest_RewardQuestUser(userObjID, 597, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 591, true);
				 api_quest_RewardQuestUser(userObjID, 591, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 591, true);
				 api_quest_RewardQuestUser(userObjID, 591, questID, 1);
			 end 
	end
	if npc_talk_index == "n002_trader_dorin-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 60, 1);
					api_quest_SetQuestStep(userObjID, 60, 1);
					api_quest_SetJournalStep(userObjID, 60, 1);

					
				if api_user_HasItem(userObjID, 300150, 1) > 0 then 
					api_user_DelItem(userObjID, 300150, api_user_HasItem(userObjID, 300150, 1, questID));
				end

				if api_user_HasItem(userObjID, 300151, 1) > 0 then 
					api_user_DelItem(userObjID, 300151, api_user_HasItem(userObjID, 300151, 1, questID));
				end

				if api_user_HasItem(userObjID, 300152, 1) > 0 then 
					api_user_DelItem(userObjID, 300152, api_user_HasItem(userObjID, 300152, 1, questID));
				end


				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n002_trader_dorin-1", "sq01_060_dorins_amazing_box2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n361_item_destroy--------------------------------------------------------------------------------
function sq01_059_dorins_amazing_box1_OnTalk_n361_item_destroy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_059_dorins_amazing_box1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 59);
	if qstep == 2 and CountIndex == 300152 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 3 and CountIndex == 300152 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetJournalStep(userObjID, questID, 4);

	end
end

function sq01_059_dorins_amazing_box1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 59);
	if qstep == 2 and CountIndex == 300152 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300152 and Count >= TargetCount  then

	end
end

function sq01_059_dorins_amazing_box1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 59);
	local questID=59;
end

function sq01_059_dorins_amazing_box1_OnRemoteStart( userObjID, questID )
end

function sq01_059_dorins_amazing_box1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq01_059_dorins_amazing_box1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,59, 1);
				api_quest_SetJournalStep(userObjID,59, 1);
				api_quest_SetQuestStep(userObjID,59, 1);
				npc_talk_index = "n002_trader_dorin-1";
end

</VillageServer>

<GameServer>
function sq01_059_dorins_amazing_box1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 2 then
		sq01_059_dorins_amazing_box1_OnTalk_n002_trader_dorin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 361 then
		sq01_059_dorins_amazing_box1_OnTalk_n361_item_destroy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n002_trader_dorin--------------------------------------------------------------------------------
function sq01_059_dorins_amazing_box1_OnTalk_n002_trader_dorin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n002_trader_dorin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n002_trader_dorin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n002_trader_dorin-2";
				if api_user_HasItem( pRoom, userObjID, 300152, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				npc_talk_index = "n002_trader_dorin-3";
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

				else
									if api_user_HasItemWarehouse( pRoom, userObjID, 300152 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-2-j";

				else
									if api_user_HasItem( pRoom, userObjID, 300151, 1) >= 1 then
									npc_talk_index = "n002_trader_dorin-2-a";

				else
									if api_user_HasItemWarehouse( pRoom, userObjID, 300151 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-2-j";

				else
									if api_user_HasItem( pRoom, userObjID, 300150, 1) >= 1 then
									npc_talk_index = "n002_trader_dorin-2";

				else
									if api_user_HasItemWarehouse( pRoom, userObjID, 300150 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-2-j";

				else
									npc_talk_index = "n002_trader_dorin-2-i";
				if api_user_CheckInvenForAddItem( pRoom, userObjID, 300150, 1) == 1 then
					api_user_AddItem( pRoom, userObjID, 300150, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

				end

				end

				end

				end

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n002_trader_dorin-3";
				if api_user_HasItem( pRoom, userObjID, 300152, 1) >= 1 then
									npc_talk_index = "n002_trader_dorin-3";

				else
									if api_user_HasItemWarehouse( pRoom, userObjID, 300152 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-3-g";

				else
									if api_user_HasItem( pRoom, userObjID, 300151, 1) >= 1 then
									npc_talk_index = "n002_trader_dorin-3-f";

				else
									if api_user_HasItemWarehouse( pRoom, userObjID, 300151 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-3-g";

				else
									if api_user_HasItem( pRoom, userObjID, 130150, 1) >= 1 then
									npc_talk_index = "n002_trader_dorin-3-g";

				else
									if api_user_HasItemWarehouse( pRoom, userObjID, 300150 , 1) >= 1  then
									npc_talk_index = "n002_trader_dorin-3-g";

				else
									npc_talk_index = "n002_trader_dorin-3-e";
				if api_user_CheckInvenForAddItem( pRoom, userObjID, 300151, 1) == 1 then
					api_user_AddItem( pRoom, userObjID, 300151, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

				end

				end

				end

				end

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n002_trader_dorin-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 591, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 592, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 592, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 592, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 591, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 596, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 597, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 591, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 591, false);
			 end 

	end
	if npc_talk_index == "n002_trader_dorin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,59, 1);
				api_quest_SetJournalStep( pRoom, userObjID,59, 1);
				api_quest_SetQuestStep( pRoom, userObjID,59, 1);
				npc_talk_index = "n002_trader_dorin-1";

	end
	if npc_talk_index == "n002_trader_dorin-1-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-1-c" then 
	end
	if npc_talk_index == "n002_trader_dorin-1-d" then 
	end
	if npc_talk_index == "n002_trader_dorin-1-e" then 
				if api_user_CheckInvenForAddItem( pRoom, userObjID, 300150, 1) == 1 then
					api_user_AddItem( pRoom, userObjID, 300150, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300152, 30000);
	end
	if npc_talk_index == "n002_trader_dorin-2-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-class_check_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n002_trader_dorin-2-c";

				else
									npc_talk_index = "n002_trader_dorin-2-d";

				end
	end
	if npc_talk_index == "n002_trader_dorin-2-e" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-e" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-f" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-g" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-h" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300152, 30000);
	end
	if npc_talk_index == "n002_trader_dorin-3-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-3-c" then 
	end
	if npc_talk_index == "n002_trader_dorin-3-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 591, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 591, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 592, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 592, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 593, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 593, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 594, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 594, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 595, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 595, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 596, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 596, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 597, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 597, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 591, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 591, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 591, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 591, questID, 1);
			 end 
	end
	if npc_talk_index == "n002_trader_dorin-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 60, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 60, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 60, 1);

					
				if api_user_HasItem( pRoom, userObjID, 300150, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300150, api_user_HasItem( pRoom, userObjID, 300150, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300151, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300151, api_user_HasItem( pRoom, userObjID, 300151, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300152, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300152, api_user_HasItem( pRoom, userObjID, 300152, 1, questID));
				end


				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n002_trader_dorin-1", "sq01_060_dorins_amazing_box2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n361_item_destroy--------------------------------------------------------------------------------
function sq01_059_dorins_amazing_box1_OnTalk_n361_item_destroy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_059_dorins_amazing_box1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 59);
	if qstep == 2 and CountIndex == 300152 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 3 and CountIndex == 300152 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

	end
end

function sq01_059_dorins_amazing_box1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 59);
	if qstep == 2 and CountIndex == 300152 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300152 and Count >= TargetCount  then

	end
end

function sq01_059_dorins_amazing_box1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 59);
	local questID=59;
end

function sq01_059_dorins_amazing_box1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq01_059_dorins_amazing_box1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq01_059_dorins_amazing_box1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,59, 1);
				api_quest_SetJournalStep( pRoom, userObjID,59, 1);
				api_quest_SetQuestStep( pRoom, userObjID,59, 1);
				npc_talk_index = "n002_trader_dorin-1";
end

</GameServer>