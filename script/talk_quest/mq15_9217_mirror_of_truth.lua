<VillageServer>

function mq15_9217_mirror_of_truth_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 822 then
		mq15_9217_mirror_of_truth_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 833 then
		mq15_9217_mirror_of_truth_OnTalk_n833_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n822_teramai--------------------------------------------------------------------------------
function mq15_9217_mirror_of_truth_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n822_teramai-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9217, 2);
				api_quest_SetJournalStep(userObjID,9217, 1);
				api_quest_SetQuestStep(userObjID,9217, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-1-b" then 
	end
	if npc_talk_index == "n822_teramai-1-c" then 
	end
	if npc_talk_index == "n822_teramai-1-d" then 
	end
	if npc_talk_index == "n822_teramai-1-e" then 
	end
	if npc_talk_index == "n822_teramai-1-f" then 
	end
	if npc_talk_index == "n822_teramai-1-g" then 
	end
	if npc_talk_index == "n822_teramai-1-h" then 
	end
	if npc_talk_index == "n822_teramai-1-i" then 
	end
	if npc_talk_index == "n822_teramai-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 600117, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700117, 30000);
	end
	if npc_talk_index == "n822_teramai-3-lv_check" then 
				if api_user_GetUserLevel(userObjID) >= 55 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92171, true);
				 api_quest_RewardQuestUser(userObjID, 92171, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92172, true);
				 api_quest_RewardQuestUser(userObjID, 92172, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92173, true);
				 api_quest_RewardQuestUser(userObjID, 92173, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92174, true);
				 api_quest_RewardQuestUser(userObjID, 92174, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92175, true);
				 api_quest_RewardQuestUser(userObjID, 92175, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92176, true);
				 api_quest_RewardQuestUser(userObjID, 92176, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92171, true);
				 api_quest_RewardQuestUser(userObjID, 92171, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92171, true);
				 api_quest_RewardQuestUser(userObjID, 92171, questID, 1);
			 end 
				npc_talk_index = "n822_teramai-3-b";

				else
									npc_talk_index = "n822_teramai-3-f";

				end
	end
	if npc_talk_index == "n822_teramai-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9218, 2);
					api_quest_SetQuestStep(userObjID, 9218, 1);
					api_quest_SetJournalStep(userObjID, 9218, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n822_teramai-3-d" then 
	end
	if npc_talk_index == "n822_teramai-3-e" then 
	end
	if npc_talk_index == "n822_teramai-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n822_teramai-1", "mq15_9218_how_to_see_the_truth.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n833_velskud--------------------------------------------------------------------------------
function mq15_9217_mirror_of_truth_OnTalk_n833_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n833_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9217_mirror_of_truth_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9217);
	if qstep == 2 and CountIndex == 600117 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 700117 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function mq15_9217_mirror_of_truth_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9217);
	if qstep == 2 and CountIndex == 600117 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700117 and Count >= TargetCount  then

	end
end

function mq15_9217_mirror_of_truth_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9217);
	local questID=9217;
end

function mq15_9217_mirror_of_truth_OnRemoteStart( userObjID, questID )
end

function mq15_9217_mirror_of_truth_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9217_mirror_of_truth_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9217, 2);
				api_quest_SetJournalStep(userObjID,9217, 1);
				api_quest_SetQuestStep(userObjID,9217, 1);
				npc_talk_index = "n822_teramai-1";
end

</VillageServer>

<GameServer>
function mq15_9217_mirror_of_truth_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 822 then
		mq15_9217_mirror_of_truth_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 833 then
		mq15_9217_mirror_of_truth_OnTalk_n833_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n822_teramai--------------------------------------------------------------------------------
function mq15_9217_mirror_of_truth_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n822_teramai-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9217, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9217, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9217, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-1-b" then 
	end
	if npc_talk_index == "n822_teramai-1-c" then 
	end
	if npc_talk_index == "n822_teramai-1-d" then 
	end
	if npc_talk_index == "n822_teramai-1-e" then 
	end
	if npc_talk_index == "n822_teramai-1-f" then 
	end
	if npc_talk_index == "n822_teramai-1-g" then 
	end
	if npc_talk_index == "n822_teramai-1-h" then 
	end
	if npc_talk_index == "n822_teramai-1-i" then 
	end
	if npc_talk_index == "n822_teramai-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 600117, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700117, 30000);
	end
	if npc_talk_index == "n822_teramai-3-lv_check" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 55 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92171, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92171, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92172, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92172, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92173, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92173, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92174, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92174, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92175, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92175, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92176, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92176, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92171, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92171, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92171, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92171, questID, 1);
			 end 
				npc_talk_index = "n822_teramai-3-b";

				else
									npc_talk_index = "n822_teramai-3-f";

				end
	end
	if npc_talk_index == "n822_teramai-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9218, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9218, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9218, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n822_teramai-3-d" then 
	end
	if npc_talk_index == "n822_teramai-3-e" then 
	end
	if npc_talk_index == "n822_teramai-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n822_teramai-1", "mq15_9218_how_to_see_the_truth.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n833_velskud--------------------------------------------------------------------------------
function mq15_9217_mirror_of_truth_OnTalk_n833_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n833_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9217_mirror_of_truth_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9217);
	if qstep == 2 and CountIndex == 600117 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 700117 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function mq15_9217_mirror_of_truth_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9217);
	if qstep == 2 and CountIndex == 600117 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700117 and Count >= TargetCount  then

	end
end

function mq15_9217_mirror_of_truth_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9217);
	local questID=9217;
end

function mq15_9217_mirror_of_truth_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9217_mirror_of_truth_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9217_mirror_of_truth_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9217, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9217, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9217, 1);
				npc_talk_index = "n822_teramai-1";
end

</GameServer>