<VillageServer>

function sq15_4150_find_treasure3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 249 then
		sq15_4150_find_treasure3_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 49 then
		sq15_4150_find_treasure3_OnTalk_n049_sainthaven_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n249_saint_guard--------------------------------------------------------------------------------
function sq15_4150_find_treasure3_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n249_saint_guard-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n249_saint_guard-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n249_saint_guard-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n249_saint_guard-3-b" then 
	end
	if npc_talk_index == "n249_saint_guard-3-c" then 
	end
	if npc_talk_index == "n249_saint_guard-3-d" then 
	end
	if npc_talk_index == "n249_saint_guard-3-e" then 
	end
	if npc_talk_index == "n249_saint_guard-3-f" then 
	end
	if npc_talk_index == "n249_saint_guard-3-g" then 
	end
	if npc_talk_index == "n249_saint_guard-3-h" then 
	end
	if npc_talk_index == "n249_saint_guard-3-i" then 
	end
	if npc_talk_index == "n249_saint_guard-3-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41500, true);
				 api_quest_RewardQuestUser(userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41500, true);
				 api_quest_RewardQuestUser(userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41500, true);
				 api_quest_RewardQuestUser(userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41500, true);
				 api_quest_RewardQuestUser(userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41500, true);
				 api_quest_RewardQuestUser(userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41500, true);
				 api_quest_RewardQuestUser(userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41500, true);
				 api_quest_RewardQuestUser(userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41500, true);
				 api_quest_RewardQuestUser(userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41500, true);
				 api_quest_RewardQuestUser(userObjID, 41500, questID, 1);
			 end 
	end
	if npc_talk_index == "n249_saint_guard-3-k" then 

				if api_quest_HasQuestItem(userObjID, 400354, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400354, api_quest_HasQuestItem(userObjID, 400354, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n249_saint_guard-3-l" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n049_sainthaven_kid--------------------------------------------------------------------------------
function sq15_4150_find_treasure3_OnTalk_n049_sainthaven_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n049_sainthaven_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n049_sainthaven_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n049_sainthaven_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n049_sainthaven_kid-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n049_sainthaven_kid-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41500, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41500, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41500, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41500, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41500, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41500, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41500, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41500, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41500, false);
			 end 

	end
	if npc_talk_index == "n049_sainthaven_kid-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4150, 1);
				api_quest_SetJournalStep(userObjID,4150, 1);
				api_quest_SetQuestStep(userObjID,4150, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400355, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700656, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 600656, 30000);
				npc_talk_index = "n049_sainthaven_kid-1";

	end
	if npc_talk_index == "n049_sainthaven_kid-2-b" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				if api_quest_HasQuestItem(userObjID, 400355, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400355, api_quest_HasQuestItem(userObjID, 400355, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400354, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400354, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n049_sainthaven_kid-3";
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4150_find_treasure3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4150);
	if qstep == 1 and CountIndex == 400355 then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 700656 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400355, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400355, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600656 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400355, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400355, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4150_find_treasure3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4150);
	if qstep == 1 and CountIndex == 400355 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700656 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600656 and Count >= TargetCount  then

	end
end

function sq15_4150_find_treasure3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4150);
	local questID=4150;
end

function sq15_4150_find_treasure3_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400355, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700656, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 600656, 30000);
end

function sq15_4150_find_treasure3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4150_find_treasure3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4150, 1);
				api_quest_SetJournalStep(userObjID,4150, 1);
				api_quest_SetQuestStep(userObjID,4150, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400355, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700656, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 600656, 30000);
				npc_talk_index = "n049_sainthaven_kid-1";
end

</VillageServer>

<GameServer>
function sq15_4150_find_treasure3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 249 then
		sq15_4150_find_treasure3_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 49 then
		sq15_4150_find_treasure3_OnTalk_n049_sainthaven_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n249_saint_guard--------------------------------------------------------------------------------
function sq15_4150_find_treasure3_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n249_saint_guard-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n249_saint_guard-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n249_saint_guard-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n249_saint_guard-3-b" then 
	end
	if npc_talk_index == "n249_saint_guard-3-c" then 
	end
	if npc_talk_index == "n249_saint_guard-3-d" then 
	end
	if npc_talk_index == "n249_saint_guard-3-e" then 
	end
	if npc_talk_index == "n249_saint_guard-3-f" then 
	end
	if npc_talk_index == "n249_saint_guard-3-g" then 
	end
	if npc_talk_index == "n249_saint_guard-3-h" then 
	end
	if npc_talk_index == "n249_saint_guard-3-i" then 
	end
	if npc_talk_index == "n249_saint_guard-3-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41500, questID, 1);
			 end 
	end
	if npc_talk_index == "n249_saint_guard-3-k" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400354, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400354, api_quest_HasQuestItem( pRoom, userObjID, 400354, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n249_saint_guard-3-l" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n049_sainthaven_kid--------------------------------------------------------------------------------
function sq15_4150_find_treasure3_OnTalk_n049_sainthaven_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n049_sainthaven_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n049_sainthaven_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n049_sainthaven_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n049_sainthaven_kid-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n049_sainthaven_kid-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41500, false);
			 end 

	end
	if npc_talk_index == "n049_sainthaven_kid-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4150, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4150, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4150, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400355, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700656, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 600656, 30000);
				npc_talk_index = "n049_sainthaven_kid-1";

	end
	if npc_talk_index == "n049_sainthaven_kid-2-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				if api_quest_HasQuestItem( pRoom, userObjID, 400355, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400355, api_quest_HasQuestItem( pRoom, userObjID, 400355, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400354, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400354, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n049_sainthaven_kid-3";
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4150_find_treasure3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4150);
	if qstep == 1 and CountIndex == 400355 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 700656 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400355, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400355, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600656 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400355, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400355, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4150_find_treasure3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4150);
	if qstep == 1 and CountIndex == 400355 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700656 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600656 and Count >= TargetCount  then

	end
end

function sq15_4150_find_treasure3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4150);
	local questID=4150;
end

function sq15_4150_find_treasure3_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400355, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700656, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 600656, 30000);
end

function sq15_4150_find_treasure3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4150_find_treasure3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4150, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4150, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4150, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400355, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700656, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 600656, 30000);
				npc_talk_index = "n049_sainthaven_kid-1";
end

</GameServer>