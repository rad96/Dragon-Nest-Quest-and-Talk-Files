<VillageServer>

function mq15_712_karahan_recipe_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 707 then
		mq15_712_karahan_recipe_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 724 then
		mq15_712_karahan_recipe_OnTalk_n724_karahan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 725 then
		mq15_712_karahan_recipe_OnTalk_n725_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mq15_712_karahan_recipe_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,712, 2);
				api_quest_SetJournalStep(userObjID,712, 1);
				api_quest_SetQuestStep(userObjID,712, 1);
				npc_talk_index = "n707_sidel-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n724_karahan--------------------------------------------------------------------------------
function mq15_712_karahan_recipe_OnTalk_n724_karahan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n724_karahan-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n724_karahan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n724_karahan-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n724_karahan-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n724_karahan-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n724_karahan-1-b" then 
	end
	if npc_talk_index == "n724_karahan-1-c" then 
	end
	if npc_talk_index == "n724_karahan-1-d" then 
	end
	if npc_talk_index == "n724_karahan-1-e" then 
	end
	if npc_talk_index == "n724_karahan-1-f" then 
	end
	if npc_talk_index == "n724_karahan-1-g" then 
	end
	if npc_talk_index == "n724_karahan-1-h" then 
	end
	if npc_talk_index == "n724_karahan-1-i" then 
	end
	if npc_talk_index == "n724_karahan-1-j" then 
	end
	if npc_talk_index == "n724_karahan-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300383, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300382, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300384, 1);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 1559, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 201559, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 1402, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 201402, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 1391, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 8, 2, 201391, 30000);
	end
	if npc_talk_index == "n724_karahan-2-b" then 
	end
	if npc_talk_index == "n724_karahan-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

				if api_quest_HasQuestItem(userObjID, 300382, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300382, api_quest_HasQuestItem(userObjID, 300382, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300383, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300383, api_quest_HasQuestItem(userObjID, 300383, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300384, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300384, api_quest_HasQuestItem(userObjID, 300384, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300385, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300385, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n725_charty--------------------------------------------------------------------------------
function mq15_712_karahan_recipe_OnTalk_n725_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n725_charty-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n725_charty-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n725_charty-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n725_charty-4-b" then 
	end
	if npc_talk_index == "n725_charty-4-c" then 
	end
	if npc_talk_index == "n725_charty-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7121, true);
				 api_quest_RewardQuestUser(userObjID, 7121, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7122, true);
				 api_quest_RewardQuestUser(userObjID, 7122, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7123, true);
				 api_quest_RewardQuestUser(userObjID, 7123, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7124, true);
				 api_quest_RewardQuestUser(userObjID, 7124, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7125, true);
				 api_quest_RewardQuestUser(userObjID, 7125, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7126, true);
				 api_quest_RewardQuestUser(userObjID, 7126, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7121, true);
				 api_quest_RewardQuestUser(userObjID, 7121, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7121, true);
				 api_quest_RewardQuestUser(userObjID, 7121, questID, 1);
			 end 
	end
	if npc_talk_index == "n725_charty-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 713, 2);
					api_quest_SetQuestStep(userObjID, 713, 1);
					api_quest_SetJournalStep(userObjID, 713, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300385, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300385, api_quest_HasQuestItem(userObjID, 300385, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n725_charty-1", "mq15_713_across_mind.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_712_karahan_recipe_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 712);
	if qstep == 2 and CountIndex == 300383 then

	end
	if qstep == 2 and CountIndex == 300382 then

	end
	if qstep == 2 and CountIndex == 300384 then

	end
	if qstep == 2 and CountIndex == 1559 then
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201559 then
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 1402 then
				if api_quest_HasQuestItem(userObjID, 300383, 3) >= 3 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300383, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300383, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201402 then
				if api_quest_HasQuestItem(userObjID, 300383, 3) >= 3 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300383, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300383, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 1391 then
				if api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300384, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300384, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201391 then
				if api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300384, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300384, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
end

function mq15_712_karahan_recipe_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 712);
	if qstep == 2 and CountIndex == 300383 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300382 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300384 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1559 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201559 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1402 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201402 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1391 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201391 and Count >= TargetCount  then

	end
end

function mq15_712_karahan_recipe_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 712);
	local questID=712;
end

function mq15_712_karahan_recipe_OnRemoteStart( userObjID, questID )
end

function mq15_712_karahan_recipe_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_712_karahan_recipe_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,712, 2);
				api_quest_SetJournalStep(userObjID,712, 1);
				api_quest_SetQuestStep(userObjID,712, 1);
				npc_talk_index = "n707_sidel-1";
end

</VillageServer>

<GameServer>
function mq15_712_karahan_recipe_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 707 then
		mq15_712_karahan_recipe_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 724 then
		mq15_712_karahan_recipe_OnTalk_n724_karahan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 725 then
		mq15_712_karahan_recipe_OnTalk_n725_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mq15_712_karahan_recipe_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,712, 2);
				api_quest_SetJournalStep( pRoom, userObjID,712, 1);
				api_quest_SetQuestStep( pRoom, userObjID,712, 1);
				npc_talk_index = "n707_sidel-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n724_karahan--------------------------------------------------------------------------------
function mq15_712_karahan_recipe_OnTalk_n724_karahan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n724_karahan-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n724_karahan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n724_karahan-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n724_karahan-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n724_karahan-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n724_karahan-1-b" then 
	end
	if npc_talk_index == "n724_karahan-1-c" then 
	end
	if npc_talk_index == "n724_karahan-1-d" then 
	end
	if npc_talk_index == "n724_karahan-1-e" then 
	end
	if npc_talk_index == "n724_karahan-1-f" then 
	end
	if npc_talk_index == "n724_karahan-1-g" then 
	end
	if npc_talk_index == "n724_karahan-1-h" then 
	end
	if npc_talk_index == "n724_karahan-1-i" then 
	end
	if npc_talk_index == "n724_karahan-1-j" then 
	end
	if npc_talk_index == "n724_karahan-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300383, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300382, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300384, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 1559, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 201559, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 1402, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 201402, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 1391, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 2, 201391, 30000);
	end
	if npc_talk_index == "n724_karahan-2-b" then 
	end
	if npc_talk_index == "n724_karahan-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300382, api_quest_HasQuestItem( pRoom, userObjID, 300382, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300383, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300383, api_quest_HasQuestItem( pRoom, userObjID, 300383, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300384, api_quest_HasQuestItem( pRoom, userObjID, 300384, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300385, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300385, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n725_charty--------------------------------------------------------------------------------
function mq15_712_karahan_recipe_OnTalk_n725_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n725_charty-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n725_charty-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n725_charty-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n725_charty-4-b" then 
	end
	if npc_talk_index == "n725_charty-4-c" then 
	end
	if npc_talk_index == "n725_charty-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7121, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7121, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7122, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7122, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7123, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7123, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7124, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7124, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7125, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7125, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7126, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7126, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7121, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7121, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7121, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7121, questID, 1);
			 end 
	end
	if npc_talk_index == "n725_charty-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 713, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 713, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 713, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300385, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300385, api_quest_HasQuestItem( pRoom, userObjID, 300385, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n725_charty-1", "mq15_713_across_mind.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_712_karahan_recipe_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 712);
	if qstep == 2 and CountIndex == 300383 then

	end
	if qstep == 2 and CountIndex == 300382 then

	end
	if qstep == 2 and CountIndex == 300384 then

	end
	if qstep == 2 and CountIndex == 1559 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201559 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 1402 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300383, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300383, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201402 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300383, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300383, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 1391 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300384, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300384, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201391 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300384, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300384, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
end

function mq15_712_karahan_recipe_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 712);
	if qstep == 2 and CountIndex == 300383 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300382 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300384 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1559 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201559 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1402 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201402 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1391 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201391 and Count >= TargetCount  then

	end
end

function mq15_712_karahan_recipe_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 712);
	local questID=712;
end

function mq15_712_karahan_recipe_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_712_karahan_recipe_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_712_karahan_recipe_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,712, 2);
				api_quest_SetJournalStep( pRoom, userObjID,712, 1);
				api_quest_SetQuestStep( pRoom, userObjID,712, 1);
				npc_talk_index = "n707_sidel-1";
end

</GameServer>