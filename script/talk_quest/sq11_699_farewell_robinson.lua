<VillageServer>

function sq11_699_farewell_robinson_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 40 then
		sq11_699_farewell_robinson_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 452 then
		sq11_699_farewell_robinson_OnTalk_n452_robinson(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n040_king_casius--------------------------------------------------------------------------------
function sq11_699_farewell_robinson_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n040_king_casius-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n040_king_casius-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n040_king_casius-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6990, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6990, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6990, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6990, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6990, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6990, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6990, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6990, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6990, false);
			 end 

	end
	if npc_talk_index == "n040_king_casius-accepting-acceptted" then
				api_quest_AddQuest(userObjID,699, 1);
				api_quest_SetJournalStep(userObjID,699, 1);
				api_quest_SetQuestStep(userObjID,699, 1);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300376, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300376, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n040_king_casius-1";

	end
	if npc_talk_index == "n040_king_casius-2-b" then 
	end
	if npc_talk_index == "n040_king_casius-2-c" then 
	end
	if npc_talk_index == "n040_king_casius-2-d" then 
	end
	if npc_talk_index == "n040_king_casius-2-e" then 
	end
	if npc_talk_index == "n040_king_casius-2-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6990, true);
				 api_quest_RewardQuestUser(userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6990, true);
				 api_quest_RewardQuestUser(userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6990, true);
				 api_quest_RewardQuestUser(userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6990, true);
				 api_quest_RewardQuestUser(userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6990, true);
				 api_quest_RewardQuestUser(userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6990, true);
				 api_quest_RewardQuestUser(userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6990, true);
				 api_quest_RewardQuestUser(userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6990, true);
				 api_quest_RewardQuestUser(userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6990, true);
				 api_quest_RewardQuestUser(userObjID, 6990, questID, 1);
			 end 
	end
	if npc_talk_index == "n040_king_casius-2-g" then 

				if api_quest_HasQuestItem(userObjID, 300377, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300377, api_quest_HasQuestItem(userObjID, 300377, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 40, 270 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n452_robinson--------------------------------------------------------------------------------
function sq11_699_farewell_robinson_OnTalk_n452_robinson(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n452_robinson-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n452_robinson-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n452_robinson-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n452_robinson-1-b" then 
	end
	if npc_talk_index == "n452_robinson-1-b" then 
	end
	if npc_talk_index == "n452_robinson-1-c" then 
	end
	if npc_talk_index == "n452_robinson-1-d" then 
	end
	if npc_talk_index == "n452_robinson-1-e" then 
	end
	if npc_talk_index == "n452_robinson-1-f" then 
	end
	if npc_talk_index == "n452_robinson-1-g" then 
	end
	if npc_talk_index == "n452_robinson-1-h" then 
	end
	if npc_talk_index == "n452_robinson-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);

				if api_quest_HasQuestItem(userObjID, 300376, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300376, api_quest_HasQuestItem(userObjID, 300376, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300377, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300377, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_699_farewell_robinson_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 699);
end

function sq11_699_farewell_robinson_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 699);
end

function sq11_699_farewell_robinson_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 699);
	local questID=699;
end

function sq11_699_farewell_robinson_OnRemoteStart( userObjID, questID )
end

function sq11_699_farewell_robinson_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_699_farewell_robinson_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,699, 1);
				api_quest_SetJournalStep(userObjID,699, 1);
				api_quest_SetQuestStep(userObjID,699, 1);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300376, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300376, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n040_king_casius-1";
end

</VillageServer>

<GameServer>
function sq11_699_farewell_robinson_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 40 then
		sq11_699_farewell_robinson_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 452 then
		sq11_699_farewell_robinson_OnTalk_n452_robinson( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n040_king_casius--------------------------------------------------------------------------------
function sq11_699_farewell_robinson_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n040_king_casius-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n040_king_casius-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n040_king_casius-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, false);
			 end 

	end
	if npc_talk_index == "n040_king_casius-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,699, 1);
				api_quest_SetJournalStep( pRoom, userObjID,699, 1);
				api_quest_SetQuestStep( pRoom, userObjID,699, 1);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300376, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300376, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n040_king_casius-1";

	end
	if npc_talk_index == "n040_king_casius-2-b" then 
	end
	if npc_talk_index == "n040_king_casius-2-c" then 
	end
	if npc_talk_index == "n040_king_casius-2-d" then 
	end
	if npc_talk_index == "n040_king_casius-2-e" then 
	end
	if npc_talk_index == "n040_king_casius-2-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6990, questID, 1);
			 end 
	end
	if npc_talk_index == "n040_king_casius-2-g" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300377, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300377, api_quest_HasQuestItem( pRoom, userObjID, 300377, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 40, 270 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n452_robinson--------------------------------------------------------------------------------
function sq11_699_farewell_robinson_OnTalk_n452_robinson( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n452_robinson-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n452_robinson-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n452_robinson-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n452_robinson-1-b" then 
	end
	if npc_talk_index == "n452_robinson-1-b" then 
	end
	if npc_talk_index == "n452_robinson-1-c" then 
	end
	if npc_talk_index == "n452_robinson-1-d" then 
	end
	if npc_talk_index == "n452_robinson-1-e" then 
	end
	if npc_talk_index == "n452_robinson-1-f" then 
	end
	if npc_talk_index == "n452_robinson-1-g" then 
	end
	if npc_talk_index == "n452_robinson-1-h" then 
	end
	if npc_talk_index == "n452_robinson-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);

				if api_quest_HasQuestItem( pRoom, userObjID, 300376, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300376, api_quest_HasQuestItem( pRoom, userObjID, 300376, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300377, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300377, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_699_farewell_robinson_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 699);
end

function sq11_699_farewell_robinson_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 699);
end

function sq11_699_farewell_robinson_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 699);
	local questID=699;
end

function sq11_699_farewell_robinson_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_699_farewell_robinson_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_699_farewell_robinson_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,699, 1);
				api_quest_SetJournalStep( pRoom, userObjID,699, 1);
				api_quest_SetQuestStep( pRoom, userObjID,699, 1);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300376, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300376, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n040_king_casius-1";
end

</GameServer>