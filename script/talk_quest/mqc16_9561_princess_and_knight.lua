<VillageServer>

function mqc16_9561_princess_and_knight_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1065 then
		mqc16_9561_princess_and_knight_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1770 then
		mqc16_9561_princess_and_knight_OnTalk_n1770_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1771 then
		mqc16_9561_princess_and_knight_OnTalk_n1771_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1772 then
		mqc16_9561_princess_and_knight_OnTalk_n1772_princess_elizabeth(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1773 then
		mqc16_9561_princess_and_knight_OnTalk_n1773_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc16_9561_princess_and_knight_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1065_geraint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1065_geraint_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9561, 2);
				api_quest_SetJournalStep(userObjID,9561, 1);
				api_quest_SetQuestStep(userObjID,9561, 1);
				npc_talk_index = "n1065_geraint_kid-1";

	end
	if npc_talk_index == "n1065_geraint_kid-1-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-j" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-k" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-m" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-n" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-o" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-q" then 
	end
	if npc_talk_index == "n1065_geraint_kid-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1770_gereint_kid--------------------------------------------------------------------------------
function mqc16_9561_princess_and_knight_OnTalk_n1770_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1770_gereint_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1770_gereint_kid-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1770_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-d" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-e" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-e" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-e" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95610, true);
				 api_quest_RewardQuestUser(userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95610, true);
				 api_quest_RewardQuestUser(userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95610, true);
				 api_quest_RewardQuestUser(userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95610, true);
				 api_quest_RewardQuestUser(userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95610, true);
				 api_quest_RewardQuestUser(userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95610, true);
				 api_quest_RewardQuestUser(userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95610, true);
				 api_quest_RewardQuestUser(userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95610, true);
				 api_quest_RewardQuestUser(userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95610, true);
				 api_quest_RewardQuestUser(userObjID, 95610, questID, 1);
			 end 
	end
	if npc_talk_index == "n1770_gereint_kid-6-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9562, 2);
					api_quest_SetQuestStep(userObjID, 9562, 1);
					api_quest_SetJournalStep(userObjID, 9562, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1770_gereint_kid-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1770_gereint_kid-1", "mqc16_9562_elf_and_knight.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1771_book--------------------------------------------------------------------------------
function mqc16_9561_princess_and_knight_OnTalk_n1771_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1771_book-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1771_book-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1771_book-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1771_book-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1771_book-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1771_book-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1771_book-2-b" then 
	end
	if npc_talk_index == "n1771_book-2-c" then 
	end
	if npc_talk_index == "n1771_book-2-d" then 
	end
	if npc_talk_index == "n1771_book-2-e" then 
	end
	if npc_talk_index == "n1771_book-2-f" then 
	end
	if npc_talk_index == "n1771_book-2-g" then 
	end
	if npc_talk_index == "n1771_book-2-h" then 
	end
	if npc_talk_index == "n1771_book-2-i" then 
	end
	if npc_talk_index == "n1771_book-2-i" then 
	end
	if npc_talk_index == "n1771_book-2-j" then 
	end
	if npc_talk_index == "n1771_book-2-j" then 
	end
	if npc_talk_index == "n1771_book-2-k" then 
	end
	if npc_talk_index == "n1771_book-2-k" then 
	end
	if npc_talk_index == "n1771_book-2-l" then 
	end
	if npc_talk_index == "n1771_book-2-m" then 
	end
	if npc_talk_index == "n1771_book-2-n" then 
	end
	if npc_talk_index == "n1771_book-2-n" then 
	end
	if npc_talk_index == "n1771_book-2-n" then 
	end
	if npc_talk_index == "n1771_book-2-n" then 
	end
	if npc_talk_index == "n1771_book-2-o" then 
	end
	if npc_talk_index == "n1771_book-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1771_book-3-b" then 
	end
	if npc_talk_index == "n1771_book-3-b" then 
	end
	if npc_talk_index == "n1771_book-3-c" then 
	end
	if npc_talk_index == "n1771_book-3-d" then 
	end
	if npc_talk_index == "n1771_book-3-e" then 
	end
	if npc_talk_index == "n1771_book-3-f" then 
	end
	if npc_talk_index == "n1771_book-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1771_book-4-b" then 
	end
	if npc_talk_index == "n1771_book-4-c" then 
	end
	if npc_talk_index == "n1771_book-4-d" then 
	end
	if npc_talk_index == "n1771_book-4-e" then 
	end
	if npc_talk_index == "n1771_book-4-f" then 
	end
	if npc_talk_index == "n1771_book-4-g" then 
	end
	if npc_talk_index == "n1771_book-4-h" then 
	end
	if npc_talk_index == "n1771_book-4-i" then 
	end
	if npc_talk_index == "n1771_book-4-j" then 
	end
	if npc_talk_index == "n1771_book-4-k" then 
	end
	if npc_talk_index == "n1771_book-4-l" then 
	end
	if npc_talk_index == "n1771_book-4-m" then 
	end
	if npc_talk_index == "n1771_book-4-n" then 
	end
	if npc_talk_index == "n1771_book-4-o" then 
	end
	if npc_talk_index == "n1771_book-4-p" then 
	end
	if npc_talk_index == "n1771_book-4-q" then 
	end
	if npc_talk_index == "n1771_book-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n1771_book-5-b" then 
	end
	if npc_talk_index == "n1771_book-5-c" then 
	end
	if npc_talk_index == "n1771_book-5-d" then 
	end
	if npc_talk_index == "n1771_book-5-e" then 
	end
	if npc_talk_index == "n1771_book-5-f" then 
	end
	if npc_talk_index == "n1771_book-5-g" then 
	end
	if npc_talk_index == "n1771_book-5-h" then 
	end
	if npc_talk_index == "n1771_book-5-i" then 
	end
	if npc_talk_index == "n1771_book-5-j" then 
	end
	if npc_talk_index == "n1771_book-5-k" then 
	end
	if npc_talk_index == "n1771_book-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1772_princess_elizabeth--------------------------------------------------------------------------------
function mqc16_9561_princess_and_knight_OnTalk_n1772_princess_elizabeth(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1773_pether--------------------------------------------------------------------------------
function mqc16_9561_princess_and_knight_OnTalk_n1773_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9561_princess_and_knight_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9561);
end

function mqc16_9561_princess_and_knight_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9561);
end

function mqc16_9561_princess_and_knight_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9561);
	local questID=9561;
end

function mqc16_9561_princess_and_knight_OnRemoteStart( userObjID, questID )
end

function mqc16_9561_princess_and_knight_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc16_9561_princess_and_knight_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9561, 2);
				api_quest_SetJournalStep(userObjID,9561, 1);
				api_quest_SetQuestStep(userObjID,9561, 1);
				npc_talk_index = "n1065_geraint_kid-1";
end

</VillageServer>

<GameServer>
function mqc16_9561_princess_and_knight_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1065 then
		mqc16_9561_princess_and_knight_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1770 then
		mqc16_9561_princess_and_knight_OnTalk_n1770_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1771 then
		mqc16_9561_princess_and_knight_OnTalk_n1771_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1772 then
		mqc16_9561_princess_and_knight_OnTalk_n1772_princess_elizabeth( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1773 then
		mqc16_9561_princess_and_knight_OnTalk_n1773_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc16_9561_princess_and_knight_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1065_geraint_kid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1065_geraint_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1065_geraint_kid-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9561, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9561, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9561, 1);
				npc_talk_index = "n1065_geraint_kid-1";

	end
	if npc_talk_index == "n1065_geraint_kid-1-b" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-c" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-d" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-e" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-f" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-g" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-h" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-i" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-j" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-k" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-l" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-m" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-n" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-o" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-p" then 
	end
	if npc_talk_index == "n1065_geraint_kid-1-q" then 
	end
	if npc_talk_index == "n1065_geraint_kid-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1770_gereint_kid--------------------------------------------------------------------------------
function mqc16_9561_princess_and_knight_OnTalk_n1770_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1770_gereint_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1770_gereint_kid-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1770_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-d" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-e" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-e" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-e" then 
	end
	if npc_talk_index == "n1770_gereint_kid-6-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95610, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95610, questID, 1);
			 end 
	end
	if npc_talk_index == "n1770_gereint_kid-6-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9562, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9562, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9562, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1770_gereint_kid-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1770_gereint_kid-1", "mqc16_9562_elf_and_knight.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1771_book--------------------------------------------------------------------------------
function mqc16_9561_princess_and_knight_OnTalk_n1771_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1771_book-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1771_book-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1771_book-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1771_book-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1771_book-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1771_book-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1771_book-2-b" then 
	end
	if npc_talk_index == "n1771_book-2-c" then 
	end
	if npc_talk_index == "n1771_book-2-d" then 
	end
	if npc_talk_index == "n1771_book-2-e" then 
	end
	if npc_talk_index == "n1771_book-2-f" then 
	end
	if npc_talk_index == "n1771_book-2-g" then 
	end
	if npc_talk_index == "n1771_book-2-h" then 
	end
	if npc_talk_index == "n1771_book-2-i" then 
	end
	if npc_talk_index == "n1771_book-2-i" then 
	end
	if npc_talk_index == "n1771_book-2-j" then 
	end
	if npc_talk_index == "n1771_book-2-j" then 
	end
	if npc_talk_index == "n1771_book-2-k" then 
	end
	if npc_talk_index == "n1771_book-2-k" then 
	end
	if npc_talk_index == "n1771_book-2-l" then 
	end
	if npc_talk_index == "n1771_book-2-m" then 
	end
	if npc_talk_index == "n1771_book-2-n" then 
	end
	if npc_talk_index == "n1771_book-2-n" then 
	end
	if npc_talk_index == "n1771_book-2-n" then 
	end
	if npc_talk_index == "n1771_book-2-n" then 
	end
	if npc_talk_index == "n1771_book-2-o" then 
	end
	if npc_talk_index == "n1771_book-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1771_book-3-b" then 
	end
	if npc_talk_index == "n1771_book-3-b" then 
	end
	if npc_talk_index == "n1771_book-3-c" then 
	end
	if npc_talk_index == "n1771_book-3-d" then 
	end
	if npc_talk_index == "n1771_book-3-e" then 
	end
	if npc_talk_index == "n1771_book-3-f" then 
	end
	if npc_talk_index == "n1771_book-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1771_book-4-b" then 
	end
	if npc_talk_index == "n1771_book-4-c" then 
	end
	if npc_talk_index == "n1771_book-4-d" then 
	end
	if npc_talk_index == "n1771_book-4-e" then 
	end
	if npc_talk_index == "n1771_book-4-f" then 
	end
	if npc_talk_index == "n1771_book-4-g" then 
	end
	if npc_talk_index == "n1771_book-4-h" then 
	end
	if npc_talk_index == "n1771_book-4-i" then 
	end
	if npc_talk_index == "n1771_book-4-j" then 
	end
	if npc_talk_index == "n1771_book-4-k" then 
	end
	if npc_talk_index == "n1771_book-4-l" then 
	end
	if npc_talk_index == "n1771_book-4-m" then 
	end
	if npc_talk_index == "n1771_book-4-n" then 
	end
	if npc_talk_index == "n1771_book-4-o" then 
	end
	if npc_talk_index == "n1771_book-4-p" then 
	end
	if npc_talk_index == "n1771_book-4-q" then 
	end
	if npc_talk_index == "n1771_book-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n1771_book-5-b" then 
	end
	if npc_talk_index == "n1771_book-5-c" then 
	end
	if npc_talk_index == "n1771_book-5-d" then 
	end
	if npc_talk_index == "n1771_book-5-e" then 
	end
	if npc_talk_index == "n1771_book-5-f" then 
	end
	if npc_talk_index == "n1771_book-5-g" then 
	end
	if npc_talk_index == "n1771_book-5-h" then 
	end
	if npc_talk_index == "n1771_book-5-i" then 
	end
	if npc_talk_index == "n1771_book-5-j" then 
	end
	if npc_talk_index == "n1771_book-5-k" then 
	end
	if npc_talk_index == "n1771_book-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1772_princess_elizabeth--------------------------------------------------------------------------------
function mqc16_9561_princess_and_knight_OnTalk_n1772_princess_elizabeth( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1773_pether--------------------------------------------------------------------------------
function mqc16_9561_princess_and_knight_OnTalk_n1773_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc16_9561_princess_and_knight_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9561);
end

function mqc16_9561_princess_and_knight_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9561);
end

function mqc16_9561_princess_and_knight_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9561);
	local questID=9561;
end

function mqc16_9561_princess_and_knight_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc16_9561_princess_and_knight_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc16_9561_princess_and_knight_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9561, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9561, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9561, 1);
				npc_talk_index = "n1065_geraint_kid-1";
end

</GameServer>