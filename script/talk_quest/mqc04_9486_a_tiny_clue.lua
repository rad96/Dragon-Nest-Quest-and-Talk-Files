<VillageServer>

function mqc04_9486_a_tiny_clue_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1737 then
		mqc04_9486_a_tiny_clue_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1742 then
		mqc04_9486_a_tiny_clue_OnTalk_n1742_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1737_eltia--------------------------------------------------------------------------------
function mqc04_9486_a_tiny_clue_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1737_eltia-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9486, 2);
				api_quest_SetJournalStep(userObjID,9486, 1);
				api_quest_SetQuestStep(userObjID,9486, 1);
				npc_talk_index = "n1737_eltia-1";

	end
	if npc_talk_index == "n1737_eltia-1-b" then 
	end
	if npc_talk_index == "n1737_eltia-1-c" then 
	end
	if npc_talk_index == "n1737_eltia-1-d" then 
	end
	if npc_talk_index == "n1737_eltia-1-e" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 913, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200913, 30000);
	end
	if npc_talk_index == "n1737_eltia-4-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94860, true);
				 api_quest_RewardQuestUser(userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94860, true);
				 api_quest_RewardQuestUser(userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94860, true);
				 api_quest_RewardQuestUser(userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94860, true);
				 api_quest_RewardQuestUser(userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94860, true);
				 api_quest_RewardQuestUser(userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94860, true);
				 api_quest_RewardQuestUser(userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94860, true);
				 api_quest_RewardQuestUser(userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94860, true);
				 api_quest_RewardQuestUser(userObjID, 94860, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-4-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9487, 2);
					api_quest_SetQuestStep(userObjID, 9487, 1);
					api_quest_SetJournalStep(userObjID, 9487, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1737_eltia-1", "mqc04_9487_chasing_a_trail.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1742_kaye--------------------------------------------------------------------------------
function mqc04_9486_a_tiny_clue_OnTalk_n1742_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1742_kaye-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1742_kaye-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1742_kaye-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1742_kaye-3-b" then 
	end
	if npc_talk_index == "n1742_kaye-3-c" then 
	end
	if npc_talk_index == "n1742_kaye-3-d" then 
	end
	if npc_talk_index == "n1742_kaye-3-e" then 

				if api_quest_HasQuestItem(userObjID, 400469, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400469, api_quest_HasQuestItem(userObjID, 400469, 1));
				end
	end
	if npc_talk_index == "n1742_kaye-3-f" then 
	end
	if npc_talk_index == "n1742_kaye-3-g" then 
	end
	if npc_talk_index == "n1742_kaye-3-h" then 
	end
	if npc_talk_index == "n1742_kaye-3-i" then 
	end
	if npc_talk_index == "n1742_kaye-3-j" then 
	end
	if npc_talk_index == "n1742_kaye-3-k" then 
	end
	if npc_talk_index == "n1742_kaye-3-l" then 
	end
	if npc_talk_index == "n1742_kaye-3-m" then 
	end
	if npc_talk_index == "n1742_kaye-3-o" then 
	end
	if npc_talk_index == "n1742_kaye-3-o" then 
	end
	if npc_talk_index == "n1742_kaye-3-p" then 
	end
	if npc_talk_index == "n1742_kaye-3-q" then 
	end
	if npc_talk_index == "n1742_kaye-3-r" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc04_9486_a_tiny_clue_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9486);
	if qstep == 2 and CountIndex == 400469 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 913 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400469, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400469, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200913 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400469, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400469, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function mqc04_9486_a_tiny_clue_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9486);
	if qstep == 2 and CountIndex == 400469 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 913 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200913 and Count >= TargetCount  then

	end
end

function mqc04_9486_a_tiny_clue_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9486);
	local questID=9486;
end

function mqc04_9486_a_tiny_clue_OnRemoteStart( userObjID, questID )
end

function mqc04_9486_a_tiny_clue_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc04_9486_a_tiny_clue_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9486, 2);
				api_quest_SetJournalStep(userObjID,9486, 1);
				api_quest_SetQuestStep(userObjID,9486, 1);
				npc_talk_index = "n1737_eltia-1";
end

</VillageServer>

<GameServer>
function mqc04_9486_a_tiny_clue_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1737 then
		mqc04_9486_a_tiny_clue_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1742 then
		mqc04_9486_a_tiny_clue_OnTalk_n1742_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1737_eltia--------------------------------------------------------------------------------
function mqc04_9486_a_tiny_clue_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1737_eltia-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9486, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9486, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9486, 1);
				npc_talk_index = "n1737_eltia-1";

	end
	if npc_talk_index == "n1737_eltia-1-b" then 
	end
	if npc_talk_index == "n1737_eltia-1-c" then 
	end
	if npc_talk_index == "n1737_eltia-1-d" then 
	end
	if npc_talk_index == "n1737_eltia-1-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 913, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200913, 30000);
	end
	if npc_talk_index == "n1737_eltia-4-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94860, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94860, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94860, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-4-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9487, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9487, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9487, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1737_eltia-1", "mqc04_9487_chasing_a_trail.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1742_kaye--------------------------------------------------------------------------------
function mqc04_9486_a_tiny_clue_OnTalk_n1742_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1742_kaye-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1742_kaye-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1742_kaye-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1742_kaye-3-b" then 
	end
	if npc_talk_index == "n1742_kaye-3-c" then 
	end
	if npc_talk_index == "n1742_kaye-3-d" then 
	end
	if npc_talk_index == "n1742_kaye-3-e" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400469, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400469, api_quest_HasQuestItem( pRoom, userObjID, 400469, 1));
				end
	end
	if npc_talk_index == "n1742_kaye-3-f" then 
	end
	if npc_talk_index == "n1742_kaye-3-g" then 
	end
	if npc_talk_index == "n1742_kaye-3-h" then 
	end
	if npc_talk_index == "n1742_kaye-3-i" then 
	end
	if npc_talk_index == "n1742_kaye-3-j" then 
	end
	if npc_talk_index == "n1742_kaye-3-k" then 
	end
	if npc_talk_index == "n1742_kaye-3-l" then 
	end
	if npc_talk_index == "n1742_kaye-3-m" then 
	end
	if npc_talk_index == "n1742_kaye-3-o" then 
	end
	if npc_talk_index == "n1742_kaye-3-o" then 
	end
	if npc_talk_index == "n1742_kaye-3-p" then 
	end
	if npc_talk_index == "n1742_kaye-3-q" then 
	end
	if npc_talk_index == "n1742_kaye-3-r" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc04_9486_a_tiny_clue_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9486);
	if qstep == 2 and CountIndex == 400469 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 913 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400469, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400469, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200913 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400469, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400469, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function mqc04_9486_a_tiny_clue_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9486);
	if qstep == 2 and CountIndex == 400469 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 913 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200913 and Count >= TargetCount  then

	end
end

function mqc04_9486_a_tiny_clue_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9486);
	local questID=9486;
end

function mqc04_9486_a_tiny_clue_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc04_9486_a_tiny_clue_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc04_9486_a_tiny_clue_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9486, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9486, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9486, 1);
				npc_talk_index = "n1737_eltia-1";
end

</GameServer>