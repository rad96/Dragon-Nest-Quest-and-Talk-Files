<VillageServer>

function sq_guide_4633_sea_fishing_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 389 then
		sq_guide_4633_sea_fishing_OnTalk_n389_fish_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n389_fish_king--------------------------------------------------------------------------------
function sq_guide_4633_sea_fishing_OnTalk_n389_fish_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n389_fish_king-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n389_fish_king-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n389_fish_king-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46330, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46330, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46330, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46330, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46330, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46330, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46330, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46330, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46330, false);
			 end 

	end
	if npc_talk_index == "n389_fish_king-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4633, 1);
				api_quest_SetJournalStep(userObjID,4633, 1);
				api_quest_SetQuestStep(userObjID,4633, 1);
				npc_talk_index = "n389_fish_king-1";

	end
	if npc_talk_index == "n389_fish_king-1-b" then 
	end
	if npc_talk_index == "n389_fish_king-1-c" then 
	end
	if npc_talk_index == "n389_fish_king-1-d" then 
	end
	if npc_talk_index == "n389_fish_king-1-e" then 
	end
	if npc_talk_index == "n389_fish_king-1-f" then 
	end
	if npc_talk_index == "n389_fish_king-1-g" then 
	end
	if npc_talk_index == "n389_fish_king-1-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46330, true);
				 api_quest_RewardQuestUser(userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46330, true);
				 api_quest_RewardQuestUser(userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46330, true);
				 api_quest_RewardQuestUser(userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46330, true);
				 api_quest_RewardQuestUser(userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46330, true);
				 api_quest_RewardQuestUser(userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46330, true);
				 api_quest_RewardQuestUser(userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46330, true);
				 api_quest_RewardQuestUser(userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46330, true);
				 api_quest_RewardQuestUser(userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46330, true);
				 api_quest_RewardQuestUser(userObjID, 46330, questID, 1);
			 end 
	end
	if npc_talk_index == "n389_fish_king-1-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n389_fish_king-1-back" then 
				api_npc_NextScript(userObjID, npcObjID, "001", "n389_fish_king.xml");
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_guide_4633_sea_fishing_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4633);
end

function sq_guide_4633_sea_fishing_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4633);
end

function sq_guide_4633_sea_fishing_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4633);
	local questID=4633;
end

function sq_guide_4633_sea_fishing_OnRemoteStart( userObjID, questID )
end

function sq_guide_4633_sea_fishing_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_guide_4633_sea_fishing_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4633, 1);
				api_quest_SetJournalStep(userObjID,4633, 1);
				api_quest_SetQuestStep(userObjID,4633, 1);
				npc_talk_index = "n389_fish_king-1";
end

</VillageServer>

<GameServer>
function sq_guide_4633_sea_fishing_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 389 then
		sq_guide_4633_sea_fishing_OnTalk_n389_fish_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n389_fish_king--------------------------------------------------------------------------------
function sq_guide_4633_sea_fishing_OnTalk_n389_fish_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n389_fish_king-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n389_fish_king-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n389_fish_king-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, false);
			 end 

	end
	if npc_talk_index == "n389_fish_king-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4633, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4633, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4633, 1);
				npc_talk_index = "n389_fish_king-1";

	end
	if npc_talk_index == "n389_fish_king-1-b" then 
	end
	if npc_talk_index == "n389_fish_king-1-c" then 
	end
	if npc_talk_index == "n389_fish_king-1-d" then 
	end
	if npc_talk_index == "n389_fish_king-1-e" then 
	end
	if npc_talk_index == "n389_fish_king-1-f" then 
	end
	if npc_talk_index == "n389_fish_king-1-g" then 
	end
	if npc_talk_index == "n389_fish_king-1-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46330, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46330, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46330, questID, 1);
			 end 
	end
	if npc_talk_index == "n389_fish_king-1-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n389_fish_king-1-back" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "001", "n389_fish_king.xml");
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_guide_4633_sea_fishing_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4633);
end

function sq_guide_4633_sea_fishing_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4633);
end

function sq_guide_4633_sea_fishing_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4633);
	local questID=4633;
end

function sq_guide_4633_sea_fishing_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_guide_4633_sea_fishing_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_guide_4633_sea_fishing_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4633, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4633, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4633, 1);
				npc_talk_index = "n389_fish_king-1";
end

</GameServer>