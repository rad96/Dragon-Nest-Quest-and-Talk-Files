<VillageServer>

function mqc07_9518_uninvited_guests_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1706 then
		mqc07_9518_uninvited_guests_OnTalk_n1706_sidel_west(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mqc07_9518_uninvited_guests_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc07_9518_uninvited_guests_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 726 then
		mqc07_9518_uninvited_guests_OnTalk_n726_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1706_sidel_west--------------------------------------------------------------------------------
function mqc07_9518_uninvited_guests_OnTalk_n1706_sidel_west(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1706_sidel_west-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1706_sidel_west-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1706_sidel_west-3-b" then 
	end
	if npc_talk_index == "n1706_sidel_west-3-c" then 
	end
	if npc_talk_index == "n1706_sidel_west-3-d" then 
	end
	if npc_talk_index == "n1706_sidel_west-3-e" then 
	end
	if npc_talk_index == "n1706_sidel_west-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mqc07_9518_uninvited_guests_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9518, 2);
				api_quest_SetJournalStep(userObjID,9518, 1);
				api_quest_SetQuestStep(userObjID,9518, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-4-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95180, true);
				 api_quest_RewardQuestUser(userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95180, true);
				 api_quest_RewardQuestUser(userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95180, true);
				 api_quest_RewardQuestUser(userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95180, true);
				 api_quest_RewardQuestUser(userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95180, true);
				 api_quest_RewardQuestUser(userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95180, true);
				 api_quest_RewardQuestUser(userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95180, true);
				 api_quest_RewardQuestUser(userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95180, true);
				 api_quest_RewardQuestUser(userObjID, 95180, questID, 1);
			 end 
	end
	if npc_talk_index == "n701_oldkarakule-4-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9519, 2);
					api_quest_SetQuestStep(userObjID, 9519, 1);
					api_quest_SetJournalStep(userObjID, 9519, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n701_oldkarakule-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n701_oldkarakule-1", "mqc07_9519_old_grudge.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc07_9518_uninvited_guests_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-1-d" then 
	end
	if npc_talk_index == "n707_sidel-1-e" then 
	end
	if npc_talk_index == "n707_sidel-1-f" then 
	end
	if npc_talk_index == "n707_sidel-1-g" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n726_velskud--------------------------------------------------------------------------------
function mqc07_9518_uninvited_guests_OnTalk_n726_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n726_velskud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n726_velskud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n726_velskud-2-b" then 
	end
	if npc_talk_index == "n726_velskud-2-c" then 
	end
	if npc_talk_index == "n726_velskud-2-d" then 
	end
	if npc_talk_index == "n726_velskud-2-e" then 
	end
	if npc_talk_index == "n726_velskud-2-f" then 
	end
	if npc_talk_index == "n726_velskud-2-g" then 
	end
	if npc_talk_index == "n726_velskud-2-h" then 
	end
	if npc_talk_index == "n726_velskud-2-i" then 
	end
	if npc_talk_index == "n726_velskud-2-j" then 
	end
	if npc_talk_index == "n726_velskud-2-k" then 
	end
	if npc_talk_index == "n726_velskud-2-l" then 
	end
	if npc_talk_index == "n726_velskud-2-m" then 
	end
	if npc_talk_index == "n726_velskud-2-n" then 
	end
	if npc_talk_index == "n726_velskud-2-o" then 
	end
	if npc_talk_index == "n726_velskud-2-p" then 
	end
	if npc_talk_index == "n726_velskud-2-q" then 
	end
	if npc_talk_index == "n726_velskud-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc07_9518_uninvited_guests_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9518);
end

function mqc07_9518_uninvited_guests_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9518);
end

function mqc07_9518_uninvited_guests_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9518);
	local questID=9518;
end

function mqc07_9518_uninvited_guests_OnRemoteStart( userObjID, questID )
end

function mqc07_9518_uninvited_guests_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc07_9518_uninvited_guests_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9518, 2);
				api_quest_SetJournalStep(userObjID,9518, 1);
				api_quest_SetQuestStep(userObjID,9518, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</VillageServer>

<GameServer>
function mqc07_9518_uninvited_guests_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1706 then
		mqc07_9518_uninvited_guests_OnTalk_n1706_sidel_west( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mqc07_9518_uninvited_guests_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mqc07_9518_uninvited_guests_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 726 then
		mqc07_9518_uninvited_guests_OnTalk_n726_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1706_sidel_west--------------------------------------------------------------------------------
function mqc07_9518_uninvited_guests_OnTalk_n1706_sidel_west( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1706_sidel_west-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1706_sidel_west-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1706_sidel_west-3-b" then 
	end
	if npc_talk_index == "n1706_sidel_west-3-c" then 
	end
	if npc_talk_index == "n1706_sidel_west-3-d" then 
	end
	if npc_talk_index == "n1706_sidel_west-3-e" then 
	end
	if npc_talk_index == "n1706_sidel_west-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mqc07_9518_uninvited_guests_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9518, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9518, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9518, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-4-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-4-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95180, questID, 1);
			 end 
	end
	if npc_talk_index == "n701_oldkarakule-4-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9519, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9519, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9519, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n701_oldkarakule-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n701_oldkarakule-1", "mqc07_9519_old_grudge.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mqc07_9518_uninvited_guests_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-1-d" then 
	end
	if npc_talk_index == "n707_sidel-1-e" then 
	end
	if npc_talk_index == "n707_sidel-1-f" then 
	end
	if npc_talk_index == "n707_sidel-1-g" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n726_velskud--------------------------------------------------------------------------------
function mqc07_9518_uninvited_guests_OnTalk_n726_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n726_velskud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n726_velskud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n726_velskud-2-b" then 
	end
	if npc_talk_index == "n726_velskud-2-c" then 
	end
	if npc_talk_index == "n726_velskud-2-d" then 
	end
	if npc_talk_index == "n726_velskud-2-e" then 
	end
	if npc_talk_index == "n726_velskud-2-f" then 
	end
	if npc_talk_index == "n726_velskud-2-g" then 
	end
	if npc_talk_index == "n726_velskud-2-h" then 
	end
	if npc_talk_index == "n726_velskud-2-i" then 
	end
	if npc_talk_index == "n726_velskud-2-j" then 
	end
	if npc_talk_index == "n726_velskud-2-k" then 
	end
	if npc_talk_index == "n726_velskud-2-l" then 
	end
	if npc_talk_index == "n726_velskud-2-m" then 
	end
	if npc_talk_index == "n726_velskud-2-n" then 
	end
	if npc_talk_index == "n726_velskud-2-o" then 
	end
	if npc_talk_index == "n726_velskud-2-p" then 
	end
	if npc_talk_index == "n726_velskud-2-q" then 
	end
	if npc_talk_index == "n726_velskud-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc07_9518_uninvited_guests_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9518);
end

function mqc07_9518_uninvited_guests_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9518);
end

function mqc07_9518_uninvited_guests_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9518);
	local questID=9518;
end

function mqc07_9518_uninvited_guests_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc07_9518_uninvited_guests_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc07_9518_uninvited_guests_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9518, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9518, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9518, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</GameServer>