<VillageServer>

function sq15_860_real_treasure1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 710 then
		sq15_860_real_treasure1_OnTalk_n710_scholar_hancock(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		sq15_860_real_treasure1_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n710_scholar_hancock--------------------------------------------------------------------------------
function sq15_860_real_treasure1_OnTalk_n710_scholar_hancock(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n710_scholar_hancock-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n710_scholar_hancock-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n710_scholar_hancock-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n710_scholar_hancock-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n710_scholar_hancock-1-b" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-c" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-d" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-e" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1703, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201703, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300488, 1);
	end
	if npc_talk_index == "n710_scholar_hancock-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8600, true);
				 api_quest_RewardQuestUser(userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8600, true);
				 api_quest_RewardQuestUser(userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8600, true);
				 api_quest_RewardQuestUser(userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8600, true);
				 api_quest_RewardQuestUser(userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8600, true);
				 api_quest_RewardQuestUser(userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8600, true);
				 api_quest_RewardQuestUser(userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8600, true);
				 api_quest_RewardQuestUser(userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8600, true);
				 api_quest_RewardQuestUser(userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8600, true);
				 api_quest_RewardQuestUser(userObjID, 8600, questID, 1);
			 end 
	end
	if npc_talk_index == "n710_scholar_hancock-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 861, 1);
					api_quest_SetQuestStep(userObjID, 861, 1);
					api_quest_SetJournalStep(userObjID, 861, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300488, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300488, api_quest_HasQuestItem(userObjID, 300488, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n710_scholar_hancock-1", "sq15_861_real_treasure2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_860_real_treasure1_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n711_warrior_fedro-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8600, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8600, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8600, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8600, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8600, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8600, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8600, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8600, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8600, false);
			 end 

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest(userObjID,860, 1);
				api_quest_SetJournalStep(userObjID,860, 1);
				api_quest_SetQuestStep(userObjID,860, 1);
				npc_talk_index = "n711_warrior_fedro-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300487, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300487, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_860_real_treasure1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 860);
	if qstep == 2 and CountIndex == 1703 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300488, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300488, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201703 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300488, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300488, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300488 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function sq15_860_real_treasure1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 860);
	if qstep == 2 and CountIndex == 1703 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201703 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300488 and Count >= TargetCount  then

	end
end

function sq15_860_real_treasure1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 860);
	local questID=860;
end

function sq15_860_real_treasure1_OnRemoteStart( userObjID, questID )
end

function sq15_860_real_treasure1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_860_real_treasure1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,860, 1);
				api_quest_SetJournalStep(userObjID,860, 1);
				api_quest_SetQuestStep(userObjID,860, 1);
				npc_talk_index = "n711_warrior_fedro-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300487, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300487, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
end

</VillageServer>

<GameServer>
function sq15_860_real_treasure1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 710 then
		sq15_860_real_treasure1_OnTalk_n710_scholar_hancock( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		sq15_860_real_treasure1_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n710_scholar_hancock--------------------------------------------------------------------------------
function sq15_860_real_treasure1_OnTalk_n710_scholar_hancock( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n710_scholar_hancock-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n710_scholar_hancock-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n710_scholar_hancock-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n710_scholar_hancock-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n710_scholar_hancock-1-b" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-c" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-d" then 
	end
	if npc_talk_index == "n710_scholar_hancock-1-e" then 
	end
	if npc_talk_index == "n710_scholar_hancock-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1703, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201703, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300488, 1);
	end
	if npc_talk_index == "n710_scholar_hancock-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8600, questID, 1);
			 end 
	end
	if npc_talk_index == "n710_scholar_hancock-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 861, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 861, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 861, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300488, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300488, api_quest_HasQuestItem( pRoom, userObjID, 300488, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n710_scholar_hancock-1", "sq15_861_real_treasure2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_860_real_treasure1_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n711_warrior_fedro-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8600, false);
			 end 

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,860, 1);
				api_quest_SetJournalStep( pRoom, userObjID,860, 1);
				api_quest_SetQuestStep( pRoom, userObjID,860, 1);
				npc_talk_index = "n711_warrior_fedro-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300487, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300487, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_860_real_treasure1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 860);
	if qstep == 2 and CountIndex == 1703 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300488, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300488, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201703 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300488, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300488, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300488 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function sq15_860_real_treasure1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 860);
	if qstep == 2 and CountIndex == 1703 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201703 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300488 and Count >= TargetCount  then

	end
end

function sq15_860_real_treasure1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 860);
	local questID=860;
end

function sq15_860_real_treasure1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_860_real_treasure1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_860_real_treasure1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,860, 1);
				api_quest_SetJournalStep( pRoom, userObjID,860, 1);
				api_quest_SetQuestStep( pRoom, userObjID,860, 1);
				npc_talk_index = "n711_warrior_fedro-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300487, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300487, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
end

</GameServer>