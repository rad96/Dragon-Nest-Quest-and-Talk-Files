<VillageServer>

function mq15_9224_resurrection_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 822 then
		mq15_9224_resurrection_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 836 then
		mq15_9224_resurrection_OnTalk_n836_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n822_teramai--------------------------------------------------------------------------------
function mq15_9224_resurrection_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9224, 2);
				api_quest_SetJournalStep(userObjID,9224, 1);
				api_quest_SetQuestStep(userObjID,9224, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-2-b" then 
	end
	if npc_talk_index == "n822_teramai-2-c" then 
	end
	if npc_talk_index == "n822_teramai-2-d" then 
	end
	if npc_talk_index == "n822_teramai-2-e" then 
	end
	if npc_talk_index == "n822_teramai-2-f" then 
	end
	if npc_talk_index == "n822_teramai-2-g" then 
	end
	if npc_talk_index == "n822_teramai-2-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92241, true);
				 api_quest_RewardQuestUser(userObjID, 92241, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92242, true);
				 api_quest_RewardQuestUser(userObjID, 92242, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92243, true);
				 api_quest_RewardQuestUser(userObjID, 92243, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92244, true);
				 api_quest_RewardQuestUser(userObjID, 92244, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92245, true);
				 api_quest_RewardQuestUser(userObjID, 92245, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92246, true);
				 api_quest_RewardQuestUser(userObjID, 92246, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92241, true);
				 api_quest_RewardQuestUser(userObjID, 92241, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92241, true);
				 api_quest_RewardQuestUser(userObjID, 92241, questID, 1);
			 end 
	end
	if npc_talk_index == "n822_teramai-2-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n822_teramai-2-j" then 
	end
	if npc_talk_index == "n822_teramai-2-cutscene" then 
				api_user_ChangeMap(userObjID,13514,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n836_velskud--------------------------------------------------------------------------------
function mq15_9224_resurrection_OnTalk_n836_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n836_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n836_velskud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n836_velskud-1-b" then 
	end
	if npc_talk_index == "n836_velskud-1-class_check" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n836_velskud-1-c";

				else
									npc_talk_index = "n836_velskud-1-k";

				end
	end
	if npc_talk_index == "n836_velskud-1-d" then 
	end
	if npc_talk_index == "n836_velskud-1-e" then 
	end
	if npc_talk_index == "n836_velskud-1-f" then 
	end
	if npc_talk_index == "n836_velskud-1-g" then 
	end
	if npc_talk_index == "n836_velskud-1-h" then 
	end
	if npc_talk_index == "n836_velskud-1-i" then 
	end
	if npc_talk_index == "n836_velskud-1-j" then 
	end
	if npc_talk_index == "n836_velskud-1-wstep_2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n836_velskud-2";
	end
	if npc_talk_index == "n836_velskud-1-l" then 
	end
	if npc_talk_index == "n836_velskud-1-m" then 
	end
	if npc_talk_index == "n836_velskud-1-n" then 
	end
	if npc_talk_index == "n836_velskud-1-chk_class_2" then 
				if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n836_velskud-1-q";

				else
									npc_talk_index = "n836_velskud-1-o";

				end
	end
	if npc_talk_index == "n836_velskud-1-p" then 
	end
	if npc_talk_index == "n836_velskud-1-s" then 
	end
	if npc_talk_index == "n836_velskud-1-r" then 
	end
	if npc_talk_index == "n836_velskud-1-step_2_kali" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n836_velskud-2";
	end
	if npc_talk_index == "n836_velskud-1-step_2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n836_velskud-2";
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9224_resurrection_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9224);
end

function mq15_9224_resurrection_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9224);
end

function mq15_9224_resurrection_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9224);
	local questID=9224;
end

function mq15_9224_resurrection_OnRemoteStart( userObjID, questID )
end

function mq15_9224_resurrection_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9224_resurrection_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9224, 2);
				api_quest_SetJournalStep(userObjID,9224, 1);
				api_quest_SetQuestStep(userObjID,9224, 1);
				npc_talk_index = "n822_teramai-1";
end

</VillageServer>

<GameServer>
function mq15_9224_resurrection_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 822 then
		mq15_9224_resurrection_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 836 then
		mq15_9224_resurrection_OnTalk_n836_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n822_teramai--------------------------------------------------------------------------------
function mq15_9224_resurrection_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9224, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9224, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9224, 1);
				npc_talk_index = "n822_teramai-1";

	end
	if npc_talk_index == "n822_teramai-2-b" then 
	end
	if npc_talk_index == "n822_teramai-2-c" then 
	end
	if npc_talk_index == "n822_teramai-2-d" then 
	end
	if npc_talk_index == "n822_teramai-2-e" then 
	end
	if npc_talk_index == "n822_teramai-2-f" then 
	end
	if npc_talk_index == "n822_teramai-2-g" then 
	end
	if npc_talk_index == "n822_teramai-2-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92241, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92241, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92242, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92242, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92243, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92243, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92244, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92244, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92245, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92245, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92246, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92246, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92241, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92241, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92241, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92241, questID, 1);
			 end 
	end
	if npc_talk_index == "n822_teramai-2-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n822_teramai-2-j" then 
	end
	if npc_talk_index == "n822_teramai-2-cutscene" then 
				api_user_ChangeMap( pRoom, userObjID,13514,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n836_velskud--------------------------------------------------------------------------------
function mq15_9224_resurrection_OnTalk_n836_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n836_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n836_velskud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n836_velskud-1-b" then 
	end
	if npc_talk_index == "n836_velskud-1-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n836_velskud-1-c";

				else
									npc_talk_index = "n836_velskud-1-k";

				end
	end
	if npc_talk_index == "n836_velskud-1-d" then 
	end
	if npc_talk_index == "n836_velskud-1-e" then 
	end
	if npc_talk_index == "n836_velskud-1-f" then 
	end
	if npc_talk_index == "n836_velskud-1-g" then 
	end
	if npc_talk_index == "n836_velskud-1-h" then 
	end
	if npc_talk_index == "n836_velskud-1-i" then 
	end
	if npc_talk_index == "n836_velskud-1-j" then 
	end
	if npc_talk_index == "n836_velskud-1-wstep_2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n836_velskud-2";
	end
	if npc_talk_index == "n836_velskud-1-l" then 
	end
	if npc_talk_index == "n836_velskud-1-m" then 
	end
	if npc_talk_index == "n836_velskud-1-n" then 
	end
	if npc_talk_index == "n836_velskud-1-chk_class_2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n836_velskud-1-q";

				else
									npc_talk_index = "n836_velskud-1-o";

				end
	end
	if npc_talk_index == "n836_velskud-1-p" then 
	end
	if npc_talk_index == "n836_velskud-1-s" then 
	end
	if npc_talk_index == "n836_velskud-1-r" then 
	end
	if npc_talk_index == "n836_velskud-1-step_2_kali" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n836_velskud-2";
	end
	if npc_talk_index == "n836_velskud-1-step_2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n836_velskud-2";
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9224_resurrection_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9224);
end

function mq15_9224_resurrection_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9224);
end

function mq15_9224_resurrection_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9224);
	local questID=9224;
end

function mq15_9224_resurrection_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9224_resurrection_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9224_resurrection_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9224, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9224, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9224, 1);
				npc_talk_index = "n822_teramai-1";
end

</GameServer>