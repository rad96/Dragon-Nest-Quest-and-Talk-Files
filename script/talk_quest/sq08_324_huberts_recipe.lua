<VillageServer>

function sq08_324_huberts_recipe_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 26 then
		sq08_324_huberts_recipe_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 99 then
		sq08_324_huberts_recipe_OnTalk_n099_engineer_hubert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_324_huberts_recipe_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n026_trader_may-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3241, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3242, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3243, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3243, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3245, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3246, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3247, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3248, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3249, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest(userObjID,324, 1);
				api_quest_SetJournalStep(userObjID,324, 1);
				api_quest_SetQuestStep(userObjID,324, 1);
				npc_talk_index = "n026_trader_may-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400132, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400132, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);

	end
	if npc_talk_index == "n026_trader_may-2" then 
				if api_quest_HasQuestItem(userObjID, 400132, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400132, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400132, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end
	end
	if npc_talk_index == "n026_trader_may-5-b" then 
	end
	if npc_talk_index == "n026_trader_may-5-c" then 
	end
	if npc_talk_index == "n026_trader_may-5-d" then 
	end
	if npc_talk_index == "n026_trader_may-5-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3241, true);
				 api_quest_RewardQuestUser(userObjID, 3241, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3242, true);
				 api_quest_RewardQuestUser(userObjID, 3242, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3243, true);
				 api_quest_RewardQuestUser(userObjID, 3243, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3244, true);
				 api_quest_RewardQuestUser(userObjID, 3244, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3245, true);
				 api_quest_RewardQuestUser(userObjID, 3245, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3246, true);
				 api_quest_RewardQuestUser(userObjID, 3246, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3247, true);
				 api_quest_RewardQuestUser(userObjID, 3247, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3248, true);
				 api_quest_RewardQuestUser(userObjID, 3248, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3249, true);
				 api_quest_RewardQuestUser(userObjID, 3249, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-5-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400114, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400114, api_quest_HasQuestItem(userObjID, 400114, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400115, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400115, api_quest_HasQuestItem(userObjID, 400115, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400132, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400132, api_quest_HasQuestItem(userObjID, 400132, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n099_engineer_hubert--------------------------------------------------------------------------------
function sq08_324_huberts_recipe_OnTalk_n099_engineer_hubert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n099_engineer_hubert-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n099_engineer_hubert-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n099_engineer_hubert-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n099_engineer_hubert-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n099_engineer_hubert-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n099_engineer_hubert-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n099_engineer_hubert-2-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-c" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-d" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-e" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-f" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-g" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-h" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-i" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-j" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-k" then 
	end
	if npc_talk_index == "n099_engineer_hubert-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 427, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200427, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400114, 1);

				if api_quest_HasQuestItem(userObjID, 400132, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400132, api_quest_HasQuestItem(userObjID, 400132, 1));
				end
	end
	if npc_talk_index == "n099_engineer_hubert-4-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-4-c" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400115, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400115, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_324_huberts_recipe_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 324);
	if qstep == 3 and CountIndex == 400114 then

	end
	if qstep == 3 and CountIndex == 427 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400114, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400114, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200427 then
				if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400114, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400114, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
end

function sq08_324_huberts_recipe_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 324);
	if qstep == 3 and CountIndex == 400114 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 3 and CountIndex == 427 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200427 and Count >= TargetCount  then

	end
end

function sq08_324_huberts_recipe_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 324);
	local questID=324;
end

function sq08_324_huberts_recipe_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,2);
end

function sq08_324_huberts_recipe_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_324_huberts_recipe_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,324, 1);
				api_quest_SetJournalStep(userObjID,324, 1);
				api_quest_SetQuestStep(userObjID,324, 1);
				npc_talk_index = "n026_trader_may-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400132, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400132, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
end

</VillageServer>

<GameServer>
function sq08_324_huberts_recipe_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 26 then
		sq08_324_huberts_recipe_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 99 then
		sq08_324_huberts_recipe_OnTalk_n099_engineer_hubert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_324_huberts_recipe_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n026_trader_may-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3241, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3242, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3243, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3243, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3245, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3246, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3247, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3248, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3249, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,324, 1);
				api_quest_SetJournalStep( pRoom, userObjID,324, 1);
				api_quest_SetQuestStep( pRoom, userObjID,324, 1);
				npc_talk_index = "n026_trader_may-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400132, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400132, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);

	end
	if npc_talk_index == "n026_trader_may-2" then 
				if api_quest_HasQuestItem( pRoom, userObjID, 400132, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400132, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400132, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end
	end
	if npc_talk_index == "n026_trader_may-5-b" then 
	end
	if npc_talk_index == "n026_trader_may-5-c" then 
	end
	if npc_talk_index == "n026_trader_may-5-d" then 
	end
	if npc_talk_index == "n026_trader_may-5-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3241, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3241, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3242, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3242, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3243, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3243, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3244, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3244, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3245, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3245, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3246, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3246, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3247, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3247, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3248, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3248, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3249, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3249, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-5-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400114, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400114, api_quest_HasQuestItem( pRoom, userObjID, 400114, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400115, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400115, api_quest_HasQuestItem( pRoom, userObjID, 400115, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400132, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400132, api_quest_HasQuestItem( pRoom, userObjID, 400132, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n099_engineer_hubert--------------------------------------------------------------------------------
function sq08_324_huberts_recipe_OnTalk_n099_engineer_hubert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n099_engineer_hubert-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n099_engineer_hubert-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n099_engineer_hubert-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n099_engineer_hubert-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n099_engineer_hubert-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n099_engineer_hubert-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n099_engineer_hubert-2-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-c" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-d" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-e" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-f" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-g" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-h" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-i" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-j" then 
	end
	if npc_talk_index == "n099_engineer_hubert-2-k" then 
	end
	if npc_talk_index == "n099_engineer_hubert-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 427, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200427, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400114, 1);

				if api_quest_HasQuestItem( pRoom, userObjID, 400132, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400132, api_quest_HasQuestItem( pRoom, userObjID, 400132, 1));
				end
	end
	if npc_talk_index == "n099_engineer_hubert-4-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-4-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400115, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400115, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_324_huberts_recipe_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 324);
	if qstep == 3 and CountIndex == 400114 then

	end
	if qstep == 3 and CountIndex == 427 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400114, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400114, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200427 then
				if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400114, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400114, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
end

function sq08_324_huberts_recipe_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 324);
	if qstep == 3 and CountIndex == 400114 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 3 and CountIndex == 427 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200427 and Count >= TargetCount  then

	end
end

function sq08_324_huberts_recipe_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 324);
	local questID=324;
end

function sq08_324_huberts_recipe_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
end

function sq08_324_huberts_recipe_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_324_huberts_recipe_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,324, 1);
				api_quest_SetJournalStep( pRoom, userObjID,324, 1);
				api_quest_SetQuestStep( pRoom, userObjID,324, 1);
				npc_talk_index = "n026_trader_may-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400132, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400132, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
end

</GameServer>