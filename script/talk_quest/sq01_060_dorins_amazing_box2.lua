<VillageServer>

function sq01_060_dorins_amazing_box2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 2 then
		sq01_060_dorins_amazing_box2_OnTalk_n002_trader_dorin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 3 then
		sq01_060_dorins_amazing_box2_OnTalk_n003_trader_dolis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n002_trader_dorin--------------------------------------------------------------------------------
function sq01_060_dorins_amazing_box2_OnTalk_n002_trader_dorin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n002_trader_dorin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n002_trader_dorin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n002_trader_dorin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n002_trader_dorin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n002_trader_dorin-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n002_trader_dorin-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 600, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 607, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 607, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 607, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 607, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 606, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 608, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 600, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 600, false);
			 end 

	end
	if npc_talk_index == "n002_trader_dorin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,60, 1);
				api_quest_SetJournalStep(userObjID,60, 1);
				api_quest_SetQuestStep(userObjID,60, 1);
				npc_talk_index = "n002_trader_dorin-1";

	end
	if npc_talk_index == "n002_trader_dorin-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				if api_user_CheckInvenForAddItem(userObjID, 300152, 1) == 1 then
					api_user_AddItem(userObjID, 300152, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n002_trader_dorin-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n002_trader_dorin-4-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-4-c" then 
				if api_user_GetUserClassID(userObjID) == 1 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 600, true);
				 api_quest_RewardQuestUser(userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 600, true);
				 api_quest_RewardQuestUser(userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 600, true);
				 api_quest_RewardQuestUser(userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 600, true);
				 api_quest_RewardQuestUser(userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 600, true);
				 api_quest_RewardQuestUser(userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 600, true);
				 api_quest_RewardQuestUser(userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 600, true);
				 api_quest_RewardQuestUser(userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 600, true);
				 api_quest_RewardQuestUser(userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 600, true);
				 api_quest_RewardQuestUser(userObjID, 600, questID, 1);
			 end 

				else
									if api_user_GetUserClassID(userObjID) == 2 then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 607, true);
				 api_quest_RewardQuestUser(userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 607, true);
				 api_quest_RewardQuestUser(userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 607, true);
				 api_quest_RewardQuestUser(userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 607, true);
				 api_quest_RewardQuestUser(userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 607, true);
				 api_quest_RewardQuestUser(userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 607, true);
				 api_quest_RewardQuestUser(userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 607, true);
				 api_quest_RewardQuestUser(userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 607, true);
				 api_quest_RewardQuestUser(userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 607, true);
				 api_quest_RewardQuestUser(userObjID, 607, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 606, true);
				 api_quest_RewardQuestUser(userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 606, true);
				 api_quest_RewardQuestUser(userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 606, true);
				 api_quest_RewardQuestUser(userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 606, true);
				 api_quest_RewardQuestUser(userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 606, true);
				 api_quest_RewardQuestUser(userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 606, true);
				 api_quest_RewardQuestUser(userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 608, true);
				 api_quest_RewardQuestUser(userObjID, 608, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 606, true);
				 api_quest_RewardQuestUser(userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 606, true);
				 api_quest_RewardQuestUser(userObjID, 606, questID, 1);
			 end 

				end

				end
	end
	if npc_talk_index == "n002_trader_dorin-4-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_user_HasItem(userObjID, 300153, 1) > 0 then 
					api_user_DelItem(userObjID, 300153, api_user_HasItem(userObjID, 300153, 1, questID));
				end

				if api_user_HasItem(userObjID, 300154, 1) > 0 then 
					api_user_DelItem(userObjID, 300154, api_user_HasItem(userObjID, 300154, 1, questID));
				end

				if api_user_HasItem(userObjID, 300155, 1) > 0 then 
					api_user_DelItem(userObjID, 300155, api_user_HasItem(userObjID, 300155, 1, questID));
				end

				if api_user_HasItem(userObjID, 300156, 1) > 0 then 
					api_user_DelItem(userObjID, 300156, api_user_HasItem(userObjID, 300156, 1, questID));
				end

				if api_user_HasItem(userObjID, 300157, 1) > 0 then 
					api_user_DelItem(userObjID, 300157, api_user_HasItem(userObjID, 300157, 1, questID));
				end

				if api_user_HasItem(userObjID, 300158, 1) > 0 then 
					api_user_DelItem(userObjID, 300158, api_user_HasItem(userObjID, 300158, 1, questID));
				end

				if api_user_HasItem(userObjID, 536938497, 1) > 0 then 
					api_user_DelItem(userObjID, 536938497, api_user_HasItem(userObjID, 536938497, 1, questID));
				end

				if api_user_HasItem(userObjID, 537987073, 1) > 0 then 
					api_user_DelItem(userObjID, 537987073, api_user_HasItem(userObjID, 537987073, 1, questID));
				end

				if api_user_HasItem(userObjID, 537462785, 1) > 0 then 
					api_user_DelItem(userObjID, 537462785, api_user_HasItem(userObjID, 537462785, 1, questID));
				end

				if api_user_HasItem(userObjID, 539035649, 1) > 0 then 
					api_user_DelItem(userObjID, 539035649, api_user_HasItem(userObjID, 539035649, 1, questID));
				end

				if api_user_HasItem(userObjID, 539559937, 1) > 0 then 
					api_user_DelItem(userObjID, 539559937, api_user_HasItem(userObjID, 539559937, 1, questID));
				end

				if api_user_HasItem(userObjID, 540084225, 1) > 0 then 
					api_user_DelItem(userObjID, 540084225, api_user_HasItem(userObjID, 540084225, 1, questID));
				end

				if api_user_HasItem(userObjID, 300152 , 1) > 0 then 
					api_user_DelItem(userObjID, 300152 , api_user_HasItem(userObjID, 300152 , 1, questID));
				end
	end
	if npc_talk_index == "n002_trader_dorin-4-opencompounditem2" then 
				api_ui_OpenShop(userObjID,25,100);
	end
	if npc_talk_index == "n002_trader_dorin-4-a" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n003_trader_dolis--------------------------------------------------------------------------------
function sq01_060_dorins_amazing_box2_OnTalk_n003_trader_dolis(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n003_trader_dolis-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n003_trader_dolis-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n003_trader_dolis-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n003_trader_dolis-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n003_trader_dolis-2-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-2-kali_che" then 
				if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n003_trader_dolis-2-e";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n003_trader_dolis-2-f";

				else
									npc_talk_index = "n003_trader_dolis-2-c";

				end

				end
	end
	if npc_talk_index == "n003_trader_dolis-2-non_kali" then 
				npc_talk_index = "n003_trader_dolis-2-d";
	end
	if npc_talk_index == "n003_trader_dolis-2-no_kali_to_dorin" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n003_trader_dolis-3";
				 local TableItem = { 				 { 300153, 1 }  , 
				 { 300154, 1 }  , 
				 { 300155, 1 }  
 				} 
				if api_user_CheckInvenForAddItemList(userObjID, TableItem) == 1 then
					api_user_AddItem(userObjID, 300153, 1, questID);
					api_user_AddItem(userObjID, 300154, 1, questID);
					api_user_AddItem(userObjID, 300155, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_user_GetUserClassID(userObjID) == 6  or api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n003_trader_dolis-3";
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									npc_talk_index = "n003_trader_dolis-3";
				 local TableItem = { 				 { 300156, 1 }  , 
				 { 300157, 1 }  , 
				 { 300158, 1 }  
 				} 
				if api_user_CheckInvenForAddItemList(userObjID, TableItem) == 1 then
					api_user_AddItem(userObjID, 300156, 1, questID);
					api_user_AddItem(userObjID, 300157, 1, questID);
					api_user_AddItem(userObjID, 300158, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				end

				end
	end
	if npc_talk_index == "n003_trader_dolis-2-kali_to_dorin" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n003_trader_dolis-3";
	end
	if npc_talk_index == "n003_trader_dolis-2-kali_to_dorin" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				npc_talk_index = "n003_trader_dolis-3";
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_060_dorins_amazing_box2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 60);
end

function sq01_060_dorins_amazing_box2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 60);
end

function sq01_060_dorins_amazing_box2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 60);
	local questID=60;
end

function sq01_060_dorins_amazing_box2_OnRemoteStart( userObjID, questID )
end

function sq01_060_dorins_amazing_box2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq01_060_dorins_amazing_box2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,60, 1);
				api_quest_SetJournalStep(userObjID,60, 1);
				api_quest_SetQuestStep(userObjID,60, 1);
				npc_talk_index = "n002_trader_dorin-1";
end

</VillageServer>

<GameServer>
function sq01_060_dorins_amazing_box2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 2 then
		sq01_060_dorins_amazing_box2_OnTalk_n002_trader_dorin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 3 then
		sq01_060_dorins_amazing_box2_OnTalk_n003_trader_dolis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n002_trader_dorin--------------------------------------------------------------------------------
function sq01_060_dorins_amazing_box2_OnTalk_n002_trader_dorin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n002_trader_dorin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n002_trader_dorin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n002_trader_dorin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n002_trader_dorin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n002_trader_dorin-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n002_trader_dorin-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 606, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 608, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, false);
			 end 

	end
	if npc_talk_index == "n002_trader_dorin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,60, 1);
				api_quest_SetJournalStep( pRoom, userObjID,60, 1);
				api_quest_SetQuestStep( pRoom, userObjID,60, 1);
				npc_talk_index = "n002_trader_dorin-1";

	end
	if npc_talk_index == "n002_trader_dorin-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				if api_user_CheckInvenForAddItem( pRoom, userObjID, 300152, 1) == 1 then
					api_user_AddItem( pRoom, userObjID, 300152, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n002_trader_dorin-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n002_trader_dorin-4-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-4-c" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 600, questID, 1);
			 end 

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 607, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 607, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 607, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 606, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 606, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 606, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 606, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 606, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 606, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 608, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 608, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 606, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 606, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 606, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 606, questID, 1);
			 end 

				end

				end
	end
	if npc_talk_index == "n002_trader_dorin-4-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_user_HasItem( pRoom, userObjID, 300153, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300153, api_user_HasItem( pRoom, userObjID, 300153, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300154, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300154, api_user_HasItem( pRoom, userObjID, 300154, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300155, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300155, api_user_HasItem( pRoom, userObjID, 300155, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300156, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300156, api_user_HasItem( pRoom, userObjID, 300156, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300157, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300157, api_user_HasItem( pRoom, userObjID, 300157, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300158, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300158, api_user_HasItem( pRoom, userObjID, 300158, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 536938497, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 536938497, api_user_HasItem( pRoom, userObjID, 536938497, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 537987073, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 537987073, api_user_HasItem( pRoom, userObjID, 537987073, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 537462785, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 537462785, api_user_HasItem( pRoom, userObjID, 537462785, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 539035649, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 539035649, api_user_HasItem( pRoom, userObjID, 539035649, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 539559937, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 539559937, api_user_HasItem( pRoom, userObjID, 539559937, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 540084225, 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 540084225, api_user_HasItem( pRoom, userObjID, 540084225, 1, questID));
				end

				if api_user_HasItem( pRoom, userObjID, 300152 , 1) > 0 then 
					api_user_DelItem( pRoom, userObjID, 300152 , api_user_HasItem( pRoom, userObjID, 300152 , 1, questID));
				end
	end
	if npc_talk_index == "n002_trader_dorin-4-opencompounditem2" then 
				api_ui_OpenShop( pRoom, userObjID,25,100);
	end
	if npc_talk_index == "n002_trader_dorin-4-a" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n003_trader_dolis--------------------------------------------------------------------------------
function sq01_060_dorins_amazing_box2_OnTalk_n003_trader_dolis( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n003_trader_dolis-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n003_trader_dolis-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n003_trader_dolis-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n003_trader_dolis-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n003_trader_dolis-2-b" then 
	end
	if npc_talk_index == "n003_trader_dolis-2-kali_che" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n003_trader_dolis-2-e";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n003_trader_dolis-2-f";

				else
									npc_talk_index = "n003_trader_dolis-2-c";

				end

				end
	end
	if npc_talk_index == "n003_trader_dolis-2-non_kali" then 
				npc_talk_index = "n003_trader_dolis-2-d";
	end
	if npc_talk_index == "n003_trader_dolis-2-no_kali_to_dorin" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n003_trader_dolis-3";
				 local TableItem = { 				 { 300153, 1 }  , 
				 { 300154, 1 }  , 
				 { 300155, 1 }  
 				} 
				if api_user_CheckInvenForAddItemList( pRoom, userObjID, TableItem) == 1 then
					api_user_AddItem( pRoom, userObjID, 300153, 1, questID);
					api_user_AddItem( pRoom, userObjID, 300154, 1, questID);
					api_user_AddItem( pRoom, userObjID, 300155, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 6  or api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n003_trader_dolis-3";
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									npc_talk_index = "n003_trader_dolis-3";
				 local TableItem = { 				 { 300156, 1 }  , 
				 { 300157, 1 }  , 
				 { 300158, 1 }  
 				} 
				if api_user_CheckInvenForAddItemList( pRoom, userObjID, TableItem) == 1 then
					api_user_AddItem( pRoom, userObjID, 300156, 1, questID);
					api_user_AddItem( pRoom, userObjID, 300157, 1, questID);
					api_user_AddItem( pRoom, userObjID, 300158, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				end

				end
	end
	if npc_talk_index == "n003_trader_dolis-2-kali_to_dorin" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n003_trader_dolis-3";
	end
	if npc_talk_index == "n003_trader_dolis-2-kali_to_dorin" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				npc_talk_index = "n003_trader_dolis-3";
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq01_060_dorins_amazing_box2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 60);
end

function sq01_060_dorins_amazing_box2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 60);
end

function sq01_060_dorins_amazing_box2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 60);
	local questID=60;
end

function sq01_060_dorins_amazing_box2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq01_060_dorins_amazing_box2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq01_060_dorins_amazing_box2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,60, 1);
				api_quest_SetJournalStep( pRoom, userObjID,60, 1);
				api_quest_SetQuestStep( pRoom, userObjID,60, 1);
				npc_talk_index = "n002_trader_dorin-1";
end

</GameServer>