<VillageServer>

function sq08_246_change1_machina_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1848 then
		sq08_246_change1_machina_OnTalk_n1848_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1907 then
		sq08_246_change1_machina_OnTalk_n1907_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1848_shaolong--------------------------------------------------------------------------------
function sq08_246_change1_machina_OnTalk_n1848_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1848_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1848_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1848_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1848_shaolong-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1848_shaolong-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2460, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2460, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2460, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2460, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2460, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2460, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2460, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2460, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2460, false);
			 end 

	end
	if npc_talk_index == "n1848_shaolong-accepting-acceptted" then
				api_quest_AddQuest(userObjID,246, 1);
				api_quest_SetJournalStep(userObjID,246, 1);
				api_quest_SetQuestStep(userObjID,246, 1);
				npc_talk_index = "n1848_shaolong-1";

	end
	if npc_talk_index == "n1848_shaolong-1-b" then 
	end
	if npc_talk_index == "n1848_shaolong-1-c" then 
	end
	if npc_talk_index == "n1848_shaolong-1-d" then 
	end
	if npc_talk_index == "n1848_shaolong-1-e" then 
	end
	if npc_talk_index == "n1848_shaolong-1-f" then 
				if api_user_GetUserJobID(userObjID) == 77  then
									npc_talk_index = "n1848_shaolong-4";
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);

				else
									if api_user_GetUserJobID(userObjID) == 78  then
									npc_talk_index = "n1848_shaolong-4";
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);

				else
									if api_user_GetUserJobID(userObjID) == 79  then
									npc_talk_index = "n1848_shaolong-4";
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);

				else
									if api_user_IsPartymember(userObjID) == 0  then
									api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13520,1);

				else
									npc_talk_index = "n1848_shaolong-1-f";

				end

				end

				end

				end
	end
	if npc_talk_index == "n1848_shaolong-2-a" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_user_ChangeMap(userObjID,13520,1);

				else
									npc_talk_index = "n1848_shaolong-2-a";

				end
	end
	if npc_talk_index == "n1848_shaolong-4-b" then 
	end
	if npc_talk_index == "n1848_shaolong-4-c" then 
	end
	if npc_talk_index == "n1848_shaolong-4-d" then 
	end
	if npc_talk_index == "n1848_shaolong-4-e" then 
	end
	if npc_talk_index == "n1848_shaolong-4-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2460, true);
				 api_quest_RewardQuestUser(userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2460, true);
				 api_quest_RewardQuestUser(userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2460, true);
				 api_quest_RewardQuestUser(userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2460, true);
				 api_quest_RewardQuestUser(userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2460, true);
				 api_quest_RewardQuestUser(userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2460, true);
				 api_quest_RewardQuestUser(userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2460, true);
				 api_quest_RewardQuestUser(userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2460, true);
				 api_quest_RewardQuestUser(userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2460, true);
				 api_quest_RewardQuestUser(userObjID, 2460, questID, 1);
			 end 
	end
	if npc_talk_index == "n1848_shaolong-4-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4727, 1);
					api_quest_SetQuestStep(userObjID, 4727, 1);
					api_quest_SetJournalStep(userObjID, 4727, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1848_shaolong-4-h" then 
	end
	if npc_talk_index == "n1848_shaolong-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1848_shaolong-1", "sq08_4727_change_job9.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1907_shaolong--------------------------------------------------------------------------------
function sq08_246_change1_machina_OnTalk_n1907_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1907_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1907_shaolong-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1907_shaolong-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1907_shaolong-2-b" then 
	end
	if npc_talk_index == "n1907_shaolong-2-c" then 
	end
	if npc_talk_index == "n1907_shaolong-2-open_ui_2" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1907_shaolong-3-open_ui3" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1907_shaolong-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_246_change1_machina_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 246);
end

function sq08_246_change1_machina_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 246);
end

function sq08_246_change1_machina_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 246);
	local questID=246;
end

function sq08_246_change1_machina_OnRemoteStart( userObjID, questID )
end

function sq08_246_change1_machina_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_246_change1_machina_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,246, 1);
				api_quest_SetJournalStep(userObjID,246, 1);
				api_quest_SetQuestStep(userObjID,246, 1);
				npc_talk_index = "n1848_shaolong-1";
end

</VillageServer>

<GameServer>
function sq08_246_change1_machina_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1848 then
		sq08_246_change1_machina_OnTalk_n1848_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1907 then
		sq08_246_change1_machina_OnTalk_n1907_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1848_shaolong--------------------------------------------------------------------------------
function sq08_246_change1_machina_OnTalk_n1848_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1848_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1848_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1848_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1848_shaolong-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1848_shaolong-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, false);
			 end 

	end
	if npc_talk_index == "n1848_shaolong-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,246, 1);
				api_quest_SetJournalStep( pRoom, userObjID,246, 1);
				api_quest_SetQuestStep( pRoom, userObjID,246, 1);
				npc_talk_index = "n1848_shaolong-1";

	end
	if npc_talk_index == "n1848_shaolong-1-b" then 
	end
	if npc_talk_index == "n1848_shaolong-1-c" then 
	end
	if npc_talk_index == "n1848_shaolong-1-d" then 
	end
	if npc_talk_index == "n1848_shaolong-1-e" then 
	end
	if npc_talk_index == "n1848_shaolong-1-f" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 77  then
									npc_talk_index = "n1848_shaolong-4";
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 78  then
									npc_talk_index = "n1848_shaolong-4";
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 79  then
									npc_talk_index = "n1848_shaolong-4";
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);

				else
									if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13520,1);

				else
									npc_talk_index = "n1848_shaolong-1-f";

				end

				end

				end

				end
	end
	if npc_talk_index == "n1848_shaolong-2-a" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_user_ChangeMap( pRoom, userObjID,13520,1);

				else
									npc_talk_index = "n1848_shaolong-2-a";

				end
	end
	if npc_talk_index == "n1848_shaolong-4-b" then 
	end
	if npc_talk_index == "n1848_shaolong-4-c" then 
	end
	if npc_talk_index == "n1848_shaolong-4-d" then 
	end
	if npc_talk_index == "n1848_shaolong-4-e" then 
	end
	if npc_talk_index == "n1848_shaolong-4-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2460, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2460, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2460, questID, 1);
			 end 
	end
	if npc_talk_index == "n1848_shaolong-4-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4727, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4727, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4727, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1848_shaolong-4-h" then 
	end
	if npc_talk_index == "n1848_shaolong-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1848_shaolong-1", "sq08_4727_change_job9.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1907_shaolong--------------------------------------------------------------------------------
function sq08_246_change1_machina_OnTalk_n1907_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1907_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1907_shaolong-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1907_shaolong-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1907_shaolong-2-b" then 
	end
	if npc_talk_index == "n1907_shaolong-2-c" then 
	end
	if npc_talk_index == "n1907_shaolong-2-open_ui_2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1907_shaolong-3-open_ui3" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1907_shaolong-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_246_change1_machina_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 246);
end

function sq08_246_change1_machina_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 246);
end

function sq08_246_change1_machina_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 246);
	local questID=246;
end

function sq08_246_change1_machina_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_246_change1_machina_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_246_change1_machina_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,246, 1);
				api_quest_SetJournalStep( pRoom, userObjID,246, 1);
				api_quest_SetQuestStep( pRoom, userObjID,246, 1);
				npc_talk_index = "n1848_shaolong-1";
end

</GameServer>