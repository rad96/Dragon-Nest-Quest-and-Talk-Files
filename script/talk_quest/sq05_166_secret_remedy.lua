<VillageServer>

function sq05_166_secret_remedy_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 15 then
		sq05_166_secret_remedy_OnTalk_n015_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n015_sorceress_tara--------------------------------------------------------------------------------
function sq05_166_secret_remedy_OnTalk_n015_sorceress_tara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n015_sorceress_tara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n015_sorceress_tara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n015_sorceress_tara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n015_sorceress_tara-accepting-who1" then
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-accepting-a";

				else
									npc_talk_index = "n015_sorceress_tara-accepting-d";

				end

	end
	if npc_talk_index == "n015_sorceress_tara-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1661, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1661, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1661, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1662, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1663, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1661, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1661, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1661, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1661, false);
			 end 

	end
	if npc_talk_index == "n015_sorceress_tara-accepting-acceptted" then
				api_quest_AddQuest(userObjID,166, 1);
				api_quest_SetJournalStep(userObjID,166, 1);
				api_quest_SetQuestStep(userObjID,166, 1);
				npc_talk_index = "n015_sorceress_tara-1";

	end
	if npc_talk_index == "n015_sorceress_tara-2-checkc2" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-2-a";

				else
									npc_talk_index = "n015_sorceress_tara-2-b";

				end
	end
	if npc_talk_index == "n015_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1661, true);
				 api_quest_RewardQuestUser(userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1661, true);
				 api_quest_RewardQuestUser(userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1661, true);
				 api_quest_RewardQuestUser(userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1662, true);
				 api_quest_RewardQuestUser(userObjID, 1662, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1663, true);
				 api_quest_RewardQuestUser(userObjID, 1663, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1661, true);
				 api_quest_RewardQuestUser(userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1661, true);
				 api_quest_RewardQuestUser(userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1661, true);
				 api_quest_RewardQuestUser(userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1661, true);
				 api_quest_RewardQuestUser(userObjID, 1661, questID, 1);
			 end 
	end
	if npc_talk_index == "n015_sorceress_tara-2-e" then 

				if api_quest_HasQuestItem(userObjID, 300095, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300095, api_quest_HasQuestItem(userObjID, 300095, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_166_secret_remedy_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 166);
	if qstep == 1 and CountIndex == 300095 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function sq05_166_secret_remedy_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 166);
	if qstep == 1 and CountIndex == 300095 and Count >= TargetCount  then

	end
end

function sq05_166_secret_remedy_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 166);
	local questID=166;
end

function sq05_166_secret_remedy_OnRemoteStart( userObjID, questID )
end

function sq05_166_secret_remedy_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_166_secret_remedy_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,166, 1);
				api_quest_SetJournalStep(userObjID,166, 1);
				api_quest_SetQuestStep(userObjID,166, 1);
				npc_talk_index = "n015_sorceress_tara-1";
end

</VillageServer>

<GameServer>
function sq05_166_secret_remedy_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 15 then
		sq05_166_secret_remedy_OnTalk_n015_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n015_sorceress_tara--------------------------------------------------------------------------------
function sq05_166_secret_remedy_OnTalk_n015_sorceress_tara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n015_sorceress_tara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n015_sorceress_tara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n015_sorceress_tara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n015_sorceress_tara-accepting-who1" then
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-accepting-a";

				else
									npc_talk_index = "n015_sorceress_tara-accepting-d";

				end

	end
	if npc_talk_index == "n015_sorceress_tara-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1662, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1663, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, false);
			 end 

	end
	if npc_talk_index == "n015_sorceress_tara-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,166, 1);
				api_quest_SetJournalStep( pRoom, userObjID,166, 1);
				api_quest_SetQuestStep( pRoom, userObjID,166, 1);
				npc_talk_index = "n015_sorceress_tara-1";

	end
	if npc_talk_index == "n015_sorceress_tara-2-checkc2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n015_sorceress_tara-2-a";

				else
									npc_talk_index = "n015_sorceress_tara-2-b";

				end
	end
	if npc_talk_index == "n015_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-c" then 
	end
	if npc_talk_index == "n015_sorceress_tara-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1662, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1662, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1663, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1663, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1661, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1661, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1661, questID, 1);
			 end 
	end
	if npc_talk_index == "n015_sorceress_tara-2-e" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300095, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300095, api_quest_HasQuestItem( pRoom, userObjID, 300095, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_166_secret_remedy_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 166);
	if qstep == 1 and CountIndex == 300095 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function sq05_166_secret_remedy_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 166);
	if qstep == 1 and CountIndex == 300095 and Count >= TargetCount  then

	end
end

function sq05_166_secret_remedy_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 166);
	local questID=166;
end

function sq05_166_secret_remedy_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq05_166_secret_remedy_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_166_secret_remedy_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,166, 1);
				api_quest_SetJournalStep( pRoom, userObjID,166, 1);
				api_quest_SetQuestStep( pRoom, userObjID,166, 1);
				npc_talk_index = "n015_sorceress_tara-1";
end

</GameServer>