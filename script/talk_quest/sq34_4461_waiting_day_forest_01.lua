<VillageServer>

function sq34_4461_waiting_day_forest_01_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 999 then
		sq34_4461_waiting_day_forest_01_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n999_remote--------------------------------------------------------------------------------
function sq34_4461_waiting_day_forest_01_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n999_remote-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n999_remote-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n999_remote-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n999_remote-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4461, 1);
				api_quest_SetJournalStep(userObjID,4461, 1);
				api_quest_SetQuestStep(userObjID,4461, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 701883, 3);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4461_waiting_day_forest_01_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4461);
	if qstep == 1 and CountIndex == 701883 then

	end
end

function sq34_4461_waiting_day_forest_01_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4461);
	if qstep == 1 and CountIndex == 701883 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function sq34_4461_waiting_day_forest_01_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4461);
	local questID=4461;
end

function sq34_4461_waiting_day_forest_01_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 701883, 3);
end

function sq34_4461_waiting_day_forest_01_OnRemoteComplete( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, questID);
	if qstep == 2 then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_quest_RewardQuestUser(userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_quest_RewardQuestUser(userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_quest_RewardQuestUser(userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_quest_RewardQuestUser(userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_quest_RewardQuestUser(userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_quest_RewardQuestUser(userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_quest_RewardQuestUser(userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_quest_RewardQuestUser(userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_quest_RewardQuestUser(userObjID, 44610, questID, 1);
			 end 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
end

function sq34_4461_waiting_day_forest_01_CanRemoteCompleteStep( userObjID, questID, questStep )
	if questStep == 2 then
		return true;
	end

	return false;
end

function sq34_4461_waiting_day_forest_01_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4461, 1);
				api_quest_SetJournalStep(userObjID,4461, 1);
				api_quest_SetQuestStep(userObjID,4461, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 701883, 3);
end

</VillageServer>

<GameServer>
function sq34_4461_waiting_day_forest_01_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 999 then
		sq34_4461_waiting_day_forest_01_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n999_remote--------------------------------------------------------------------------------
function sq34_4461_waiting_day_forest_01_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n999_remote-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n999_remote-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n999_remote-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n999_remote-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4461, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4461, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4461, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 701883, 3);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4461_waiting_day_forest_01_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4461);
	if qstep == 1 and CountIndex == 701883 then

	end
end

function sq34_4461_waiting_day_forest_01_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4461);
	if qstep == 1 and CountIndex == 701883 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function sq34_4461_waiting_day_forest_01_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4461);
	local questID=4461;
end

function sq34_4461_waiting_day_forest_01_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 701883, 3);
end

function sq34_4461_waiting_day_forest_01_OnRemoteComplete( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, questID);
	if qstep == 2 then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 44610, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_quest_RewardQuestUser( pRoom, userObjID, 44610, questID, 1);
			 end 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
end

function sq34_4461_waiting_day_forest_01_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	if questStep == 2 then
		return true;
	end

	return false;
end

function sq34_4461_waiting_day_forest_01_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4461, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4461, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4461, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 701883, 3);
end

</GameServer>