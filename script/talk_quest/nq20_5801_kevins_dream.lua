<VillageServer>

function nq20_5801_kevins_dream_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 29 then
		nq20_5801_kevins_dream_OnTalk_n029_crew_kevin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n029_crew_kevin--------------------------------------------------------------------------------
function nq20_5801_kevins_dream_OnTalk_n029_crew_kevin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n029_crew_kevin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n029_crew_kevin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n029_crew_kevin-accepting-acceppted" then
				api_quest_AddQuest(userObjID,5801, 1);
				api_quest_SetJournalStep(userObjID,5801, 1);
				api_quest_SetQuestStep(userObjID,5801, 1);
				npc_talk_index = "n029_crew_kevin-1";

	end
	if npc_talk_index == "n029_crew_kevin-1-c" then 
	end
	if npc_talk_index == "n029_crew_kevin-1-c" then 
	end
	if npc_talk_index == "n029_crew_kevin-1-d" then 
	end
	if npc_talk_index == "n029_crew_kevin-1-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 29, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq20_5801_kevins_dream_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5801);
end

function nq20_5801_kevins_dream_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5801);
end

function nq20_5801_kevins_dream_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5801);
	local questID=5801;
end

function nq20_5801_kevins_dream_OnRemoteStart( userObjID, questID )
end

function nq20_5801_kevins_dream_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq20_5801_kevins_dream_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq20_5801_kevins_dream_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 29 then
		nq20_5801_kevins_dream_OnTalk_n029_crew_kevin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n029_crew_kevin--------------------------------------------------------------------------------
function nq20_5801_kevins_dream_OnTalk_n029_crew_kevin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n029_crew_kevin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n029_crew_kevin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n029_crew_kevin-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,5801, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5801, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5801, 1);
				npc_talk_index = "n029_crew_kevin-1";

	end
	if npc_talk_index == "n029_crew_kevin-1-c" then 
	end
	if npc_talk_index == "n029_crew_kevin-1-c" then 
	end
	if npc_talk_index == "n029_crew_kevin-1-d" then 
	end
	if npc_talk_index == "n029_crew_kevin-1-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 29, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq20_5801_kevins_dream_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5801);
end

function nq20_5801_kevins_dream_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5801);
end

function nq20_5801_kevins_dream_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5801);
	local questID=5801;
end

function nq20_5801_kevins_dream_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq20_5801_kevins_dream_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq20_5801_kevins_dream_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>