<VillageServer>

function sq08_296_find_out_abyss1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 28 then
		sq08_296_find_out_abyss1_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_296_find_out_abyss1_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n028_scholar_bailey-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2961, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2962, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2963, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2964, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2965, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2966, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2967, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2967, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2967, false);
			 end 

	end
	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,296, 1);
				api_quest_SetJournalStep(userObjID,296, 1);
				api_quest_SetQuestStep(userObjID,296, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 200350, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200371, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200395, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 300121, 1);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300122, 1);
				api_quest_SetCountingInfo(userObjID, questID, 5, 3, 300123, 1);
	end
	if npc_talk_index == "n028_scholar_bailey-3-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-e" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2961, true);
				 api_quest_RewardQuestUser(userObjID, 2961, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2962, true);
				 api_quest_RewardQuestUser(userObjID, 2962, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2963, true);
				 api_quest_RewardQuestUser(userObjID, 2963, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2964, true);
				 api_quest_RewardQuestUser(userObjID, 2964, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2965, true);
				 api_quest_RewardQuestUser(userObjID, 2965, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2966, true);
				 api_quest_RewardQuestUser(userObjID, 2966, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2967, true);
				 api_quest_RewardQuestUser(userObjID, 2967, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2967, true);
				 api_quest_RewardQuestUser(userObjID, 2967, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2967, true);
				 api_quest_RewardQuestUser(userObjID, 2967, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-3-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem(userObjID, 300121, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300121, api_quest_HasQuestItem(userObjID, 300121, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300122, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300122, api_quest_HasQuestItem(userObjID, 300122, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300123, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300123, api_quest_HasQuestItem(userObjID, 300123, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 297, 1);
					api_quest_SetQuestStep(userObjID, 297, 1);
					api_quest_SetJournalStep(userObjID, 297, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n028_scholar_bailey-1", "sq08_297_find_out_abyss2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_296_find_out_abyss1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 296);
	if qstep == 2 and CountIndex == 200350 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300121, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300121, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200371 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300122, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300122, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200395 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300123, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300123, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300121 then

	end
	if qstep == 2 and CountIndex == 300122 then

	end
	if qstep == 2 and CountIndex == 300123 then

	end
end

function sq08_296_find_out_abyss1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 296);
	if qstep == 2 and CountIndex == 200350 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200371 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200395 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300121 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300122 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300123 and Count >= TargetCount  then

	end
end

function sq08_296_find_out_abyss1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 296);
	local questID=296;
end

function sq08_296_find_out_abyss1_OnRemoteStart( userObjID, questID )
end

function sq08_296_find_out_abyss1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_296_find_out_abyss1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,296, 1);
				api_quest_SetJournalStep(userObjID,296, 1);
				api_quest_SetQuestStep(userObjID,296, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</VillageServer>

<GameServer>
function sq08_296_find_out_abyss1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 28 then
		sq08_296_find_out_abyss1_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_296_find_out_abyss1_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n028_scholar_bailey-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2961, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2962, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2963, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2964, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2965, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2966, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2967, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2967, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2967, false);
			 end 

	end
	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,296, 1);
				api_quest_SetJournalStep( pRoom, userObjID,296, 1);
				api_quest_SetQuestStep( pRoom, userObjID,296, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 200350, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200371, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200395, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 300121, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300122, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 3, 300123, 1);
	end
	if npc_talk_index == "n028_scholar_bailey-3-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-e" then 
	end
	if npc_talk_index == "n028_scholar_bailey-3-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2961, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2961, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2962, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2962, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2963, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2963, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2964, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2964, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2965, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2965, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2966, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2966, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2967, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2967, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2967, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2967, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2967, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2967, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-3-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem( pRoom, userObjID, 300121, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300121, api_quest_HasQuestItem( pRoom, userObjID, 300121, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300122, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300122, api_quest_HasQuestItem( pRoom, userObjID, 300122, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300123, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300123, api_quest_HasQuestItem( pRoom, userObjID, 300123, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 297, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 297, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 297, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n028_scholar_bailey-1", "sq08_297_find_out_abyss2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_296_find_out_abyss1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 296);
	if qstep == 2 and CountIndex == 200350 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300121, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300121, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200371 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300122, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300122, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200395 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300123, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300123, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300121 then

	end
	if qstep == 2 and CountIndex == 300122 then

	end
	if qstep == 2 and CountIndex == 300123 then

	end
end

function sq08_296_find_out_abyss1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 296);
	if qstep == 2 and CountIndex == 200350 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200371 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200395 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300121 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300122 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300123 and Count >= TargetCount  then

	end
end

function sq08_296_find_out_abyss1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 296);
	local questID=296;
end

function sq08_296_find_out_abyss1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_296_find_out_abyss1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_296_find_out_abyss1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,296, 1);
				api_quest_SetJournalStep( pRoom, userObjID,296, 1);
				api_quest_SetQuestStep( pRoom, userObjID,296, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</GameServer>