<VillageServer>

function sq11_477_proof_of_real_adventurer_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 36 then
		sq11_477_proof_of_real_adventurer_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_477_proof_of_real_adventurer_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4771, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4772, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4773, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4774, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4775, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4776, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4777, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4778, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4779, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceppted" then
				api_quest_AddQuest(userObjID,477, 1);
				api_quest_SetJournalStep(userObjID,477, 1);
				api_quest_SetQuestStep(userObjID,477, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1215, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201215, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300280, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-b" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-c" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-d" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-e" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-f" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-g" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-h" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4771, true);
				 api_quest_RewardQuestUser(userObjID, 4771, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4772, true);
				 api_quest_RewardQuestUser(userObjID, 4772, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4773, true);
				 api_quest_RewardQuestUser(userObjID, 4773, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4774, true);
				 api_quest_RewardQuestUser(userObjID, 4774, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4775, true);
				 api_quest_RewardQuestUser(userObjID, 4775, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4776, true);
				 api_quest_RewardQuestUser(userObjID, 4776, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4777, true);
				 api_quest_RewardQuestUser(userObjID, 4777, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4778, true);
				 api_quest_RewardQuestUser(userObjID, 4778, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4779, true);
				 api_quest_RewardQuestUser(userObjID, 4779, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-j" then 

				if api_quest_HasQuestItem(userObjID, 399281, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 399281, api_quest_HasQuestItem(userObjID, 399281, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 36, 480 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_477_proof_of_real_adventurer_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 477);
	if qstep == 1 and CountIndex == 1215 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300280, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300280, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300280 then

	end
	if qstep == 1 and CountIndex == 201215 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300280, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300280, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_477_proof_of_real_adventurer_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 477);
	if qstep == 1 and CountIndex == 1215 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300280 and Count >= TargetCount  then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300281, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300281, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201215 and Count >= TargetCount  then

	end
end

function sq11_477_proof_of_real_adventurer_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 477);
	local questID=477;
end

function sq11_477_proof_of_real_adventurer_OnRemoteStart( userObjID, questID )
end

function sq11_477_proof_of_real_adventurer_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_477_proof_of_real_adventurer_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_477_proof_of_real_adventurer_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 36 then
		sq11_477_proof_of_real_adventurer_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_477_proof_of_real_adventurer_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4771, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4772, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4773, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4774, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4775, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4776, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4777, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4778, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4779, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,477, 1);
				api_quest_SetJournalStep( pRoom, userObjID,477, 1);
				api_quest_SetQuestStep( pRoom, userObjID,477, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1215, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201215, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300280, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-b" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-c" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-d" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-e" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-f" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-g" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-h" then 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4771, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4771, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4772, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4772, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4773, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4773, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4774, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4774, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4775, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4775, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4776, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4776, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4777, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4777, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4778, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4778, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4779, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4779, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-j" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 399281, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 399281, api_quest_HasQuestItem( pRoom, userObjID, 399281, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 36, 480 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_477_proof_of_real_adventurer_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 477);
	if qstep == 1 and CountIndex == 1215 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300280, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300280, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300280 then

	end
	if qstep == 1 and CountIndex == 201215 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300280, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300280, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_477_proof_of_real_adventurer_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 477);
	if qstep == 1 and CountIndex == 1215 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300280 and Count >= TargetCount  then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300281, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300281, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201215 and Count >= TargetCount  then

	end
end

function sq11_477_proof_of_real_adventurer_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 477);
	local questID=477;
end

function sq11_477_proof_of_real_adventurer_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_477_proof_of_real_adventurer_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_477_proof_of_real_adventurer_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>