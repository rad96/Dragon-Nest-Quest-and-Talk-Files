<VillageServer>

function mq11_422_question_of_dragon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 209 then
		mq11_422_question_of_dragon_OnTalk_n209_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 216 then
		mq11_422_question_of_dragon_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		mq11_422_question_of_dragon_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mq11_422_question_of_dragon_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n209_elite_soldier--------------------------------------------------------------------------------
function mq11_422_question_of_dragon_OnTalk_n209_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n209_elite_soldier-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n209_elite_soldier-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n209_elite_soldier-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n209_elite_soldier-accepting-acceptted" then
				api_quest_AddQuest(userObjID,422, 2);
				api_quest_SetJournalStep(userObjID,422, 1);
				api_quest_SetQuestStep(userObjID,422, 1);
				npc_talk_index = "n209_elite_soldier-1";

	end
	if npc_talk_index == "n209_elite_soldier-1-b" then 
	end
	if npc_talk_index == "n209_elite_soldier-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n216_geraint_wounded--------------------------------------------------------------------------------
function mq11_422_question_of_dragon_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n216_geraint_wounded-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n216_geraint_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n216_geraint_wounded-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n216_geraint_wounded-6-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4221, true);
				 api_quest_RewardQuestUser(userObjID, 4221, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4222, true);
				 api_quest_RewardQuestUser(userObjID, 4222, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4223, true);
				 api_quest_RewardQuestUser(userObjID, 4223, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4224, true);
				 api_quest_RewardQuestUser(userObjID, 4224, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4224, true);
				 api_quest_RewardQuestUser(userObjID, 4224, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4226, true);
				 api_quest_RewardQuestUser(userObjID, 4226, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4221, true);
				 api_quest_RewardQuestUser(userObjID, 4221, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4221, true);
				 api_quest_RewardQuestUser(userObjID, 4221, questID, 1);
			 end 
	end
	if npc_talk_index == "n216_geraint_wounded-6-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 423, 2);
					api_quest_SetQuestStep(userObjID, 423, 1);
					api_quest_SetJournalStep(userObjID, 423, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function mq11_422_question_of_dragon_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n044_archer_master_ishilien-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n044_archer_master_ishilien-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-5-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mq11_422_question_of_dragon_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n088_scholar_starshy-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n088_scholar_starshy-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-f" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1197, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201197, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300253, 1);
	end
	if npc_talk_index == "n088_scholar_starshy-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n088_scholar_starshy-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n088_scholar_starshy-4-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-k" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-l" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-m" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-n" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-o" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5" then 

				if api_quest_HasQuestItem(userObjID, 300253, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300253, api_quest_HasQuestItem(userObjID, 300253, 1));
				end
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_422_question_of_dragon_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 422);
	if qstep == 3 and CountIndex == 1197 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300253, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300253, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300253 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

	end
	if qstep == 3 and CountIndex == 201197 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300253, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300253, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq11_422_question_of_dragon_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 422);
	if qstep == 3 and CountIndex == 1197 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300253 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 201197 and Count >= TargetCount  then

	end
end

function mq11_422_question_of_dragon_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 422);
	local questID=422;
end

function mq11_422_question_of_dragon_OnRemoteStart( userObjID, questID )
end

function mq11_422_question_of_dragon_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_422_question_of_dragon_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,422, 2);
				api_quest_SetJournalStep(userObjID,422, 1);
				api_quest_SetQuestStep(userObjID,422, 1);
				npc_talk_index = "n209_elite_soldier-1";
end

</VillageServer>

<GameServer>
function mq11_422_question_of_dragon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 209 then
		mq11_422_question_of_dragon_OnTalk_n209_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 216 then
		mq11_422_question_of_dragon_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 44 then
		mq11_422_question_of_dragon_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mq11_422_question_of_dragon_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n209_elite_soldier--------------------------------------------------------------------------------
function mq11_422_question_of_dragon_OnTalk_n209_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n209_elite_soldier-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n209_elite_soldier-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n209_elite_soldier-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n209_elite_soldier-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,422, 2);
				api_quest_SetJournalStep( pRoom, userObjID,422, 1);
				api_quest_SetQuestStep( pRoom, userObjID,422, 1);
				npc_talk_index = "n209_elite_soldier-1";

	end
	if npc_talk_index == "n209_elite_soldier-1-b" then 
	end
	if npc_talk_index == "n209_elite_soldier-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n216_geraint_wounded--------------------------------------------------------------------------------
function mq11_422_question_of_dragon_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n216_geraint_wounded-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n216_geraint_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n216_geraint_wounded-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n216_geraint_wounded-6-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4221, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4221, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4222, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4222, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4223, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4223, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4224, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4224, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4224, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4224, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4226, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4226, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4221, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4221, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4221, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4221, questID, 1);
			 end 
	end
	if npc_talk_index == "n216_geraint_wounded-6-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 423, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 423, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 423, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n044_archer_master_ishilien--------------------------------------------------------------------------------
function mq11_422_question_of_dragon_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n044_archer_master_ishilien-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n044_archer_master_ishilien-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-5-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-d" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-e" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-f" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-g" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-5-h" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mq11_422_question_of_dragon_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n088_scholar_starshy-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n088_scholar_starshy-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n088_scholar_starshy-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-f" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1197, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201197, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300253, 1);
	end
	if npc_talk_index == "n088_scholar_starshy-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n088_scholar_starshy-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n088_scholar_starshy-4-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-k" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-l" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-m" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-n" then 
	end
	if npc_talk_index == "n088_scholar_starshy-4-o" then 
	end
	if npc_talk_index == "n088_scholar_starshy-5" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300253, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300253, api_quest_HasQuestItem( pRoom, userObjID, 300253, 1));
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_422_question_of_dragon_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 422);
	if qstep == 3 and CountIndex == 1197 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300253, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300253, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300253 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

	end
	if qstep == 3 and CountIndex == 201197 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300253, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300253, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq11_422_question_of_dragon_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 422);
	if qstep == 3 and CountIndex == 1197 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300253 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 201197 and Count >= TargetCount  then

	end
end

function mq11_422_question_of_dragon_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 422);
	local questID=422;
end

function mq11_422_question_of_dragon_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_422_question_of_dragon_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_422_question_of_dragon_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,422, 2);
				api_quest_SetJournalStep( pRoom, userObjID,422, 1);
				api_quest_SetQuestStep( pRoom, userObjID,422, 1);
				npc_talk_index = "n209_elite_soldier-1";
end

</GameServer>