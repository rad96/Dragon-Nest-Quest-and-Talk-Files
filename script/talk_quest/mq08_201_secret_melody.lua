<VillageServer>

function mq08_201_secret_melody_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1 then
		mq08_201_secret_melody_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 23 then
		mq08_201_secret_melody_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mq08_201_secret_melody_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mq08_201_secret_melody_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n001_elder_harold-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n023_ranger_fugus--------------------------------------------------------------------------------
function mq08_201_secret_melody_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n023_ranger_fugus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n023_ranger_fugus-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n023_ranger_fugus-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n023_ranger_fugus-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n023_ranger_fugus-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n023_ranger_fugus-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n023_ranger_fugus-accepting-a" then
				if api_quest_IsMarkingCompleteQuest(userObjID, 10) == 1   or api_quest_IsMarkingCompleteQuest(userObjID, 110) == 1   or api_quest_IsMarkingCompleteQuest(userObjID, 120) == 1   or api_quest_IsMarkingCompleteQuest(userObjID, 21) == 1   or api_quest_IsMarkingCompleteQuest(userObjID, 134) == 1   or api_quest_IsMarkingCompleteQuest(userObjID, 144) == 1   or api_quest_IsMarkingCompleteQuest(userObjID, 30) == 1  then
									npc_talk_index = "n023_ranger_fugus-accepting-a";

				else
									npc_talk_index = "n023_ranger_fugus-accepting-b";

				end

	end
	if npc_talk_index == "n023_ranger_fugus-accepting-acceptted" then
				api_quest_AddQuest(userObjID,201, 2);
				api_quest_SetJournalStep(userObjID,201, 1);
				api_quest_SetQuestStep(userObjID,201, 1);
				npc_talk_index = "n023_ranger_fugus-1";

	end
	if npc_talk_index == "n023_ranger_fugus-1-a" then 
				if api_user_GetUserClassID(userObjID) == 1  or api_user_GetUserClassID(userObjID) == 2  or api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n023_ranger_fugus-1-c";

				else
									if api_user_GetUserClassID(userObjID) == 3  or api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n023_ranger_fugus-1-a";

				else
									npc_talk_index = "n023_ranger_fugus-1-f";

				end

				end
	end
	if npc_talk_index == "n023_ranger_fugus-1-b" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				if api_quest_HasQuestItem(userObjID, 300012, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300012, api_quest_HasQuestItem(userObjID, 300012, 1));
				end
	end
	if npc_talk_index == "n023_ranger_fugus-1-d" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				if api_quest_HasQuestItem(userObjID, 300012, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300012, api_quest_HasQuestItem(userObjID, 300012, 1));
				end
	end
	if npc_talk_index == "n023_ranger_fugus-3-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-c" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-d" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-e" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-f" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-g" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-h" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-i" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-j" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-k" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 350, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200350, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300015, 1);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n023_ranger_fugus-5-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-5-c" then 
	end
	if npc_talk_index == "n023_ranger_fugus-5-d" then 
	end
	if npc_talk_index == "n023_ranger_fugus-5-e" then 
	end
	if npc_talk_index == "n023_ranger_fugus-5-f" then 
	end
	if npc_talk_index == "n023_ranger_fugus-5-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2011, true);
				 api_quest_RewardQuestUser(userObjID, 2011, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2012, true);
				 api_quest_RewardQuestUser(userObjID, 2012, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2013, true);
				 api_quest_RewardQuestUser(userObjID, 2013, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2014, true);
				 api_quest_RewardQuestUser(userObjID, 2014, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2014, true);
				 api_quest_RewardQuestUser(userObjID, 2014, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2016, true);
				 api_quest_RewardQuestUser(userObjID, 2016, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2011, true);
				 api_quest_RewardQuestUser(userObjID, 2011, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2011, true);
				 api_quest_RewardQuestUser(userObjID, 2011, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2011, true);
				 api_quest_RewardQuestUser(userObjID, 2011, questID, 1);
			 end 
	end
	if npc_talk_index == "n023_ranger_fugus-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 202, 2);
					api_quest_SetQuestStep(userObjID, 202, 1);
					api_quest_SetJournalStep(userObjID, 202, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300089, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300089, api_quest_HasQuestItem(userObjID, 300089, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n023_ranger_fugus-1", "mq08_202_rescue_girl.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_201_secret_melody_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n024_adventurer_guildmaster_decud-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				if api_user_GetUserClassID(userObjID) == 1  or api_user_GetUserClassID(userObjID) == 2  or api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n024_adventurer_guildmaster_decud-2-k";

				else
									if api_user_GetUserClassID(userObjID) == 3  or api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n024_adventurer_guildmaster_decud-2";

				else
									npc_talk_index = "n024_adventurer_guildmaster_decud-2-u";

				end

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-w" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-g" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-h" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-i" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-j" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-l" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-m" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-n" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-o" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-p" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-q" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-r" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-s" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-t" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-a" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_201_secret_melody_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 201);
	if qstep == 4 and CountIndex == 350 then
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				 local TableItem = { 
				 { 300089, 1 }  , 
				 { 300015, 1 }  
 				} 
				if api_quest_CheckQuestInvenForAddItemList(userObjID, TableItem) == 1 then
						api_quest_AddQuestItem(userObjID, 300089, 1, questID);
						api_quest_AddQuestItem(userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 4 and CountIndex == 300015 then

	end
	if qstep == 4 and CountIndex == 200350 then
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				 local TableItem = { 
				 { 300089, 1 }  , 
				 { 300015, 1 }  
 				} 
				if api_quest_CheckQuestInvenForAddItemList(userObjID, TableItem) == 1 then
						api_quest_AddQuestItem(userObjID, 300089, 1, questID);
						api_quest_AddQuestItem(userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function mq08_201_secret_melody_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 201);
	if qstep == 4 and CountIndex == 350 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 300015 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 200350 and Count >= TargetCount  then

	end
end

function mq08_201_secret_melody_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 201);
	local questID=201;
end

function mq08_201_secret_melody_OnRemoteStart( userObjID, questID )
end

function mq08_201_secret_melody_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_201_secret_melody_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,201, 2);
				api_quest_SetJournalStep(userObjID,201, 1);
				api_quest_SetQuestStep(userObjID,201, 1);
				npc_talk_index = "n023_ranger_fugus-1";
end

</VillageServer>

<GameServer>
function mq08_201_secret_melody_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1 then
		mq08_201_secret_melody_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 23 then
		mq08_201_secret_melody_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mq08_201_secret_melody_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mq08_201_secret_melody_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n001_elder_harold-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n023_ranger_fugus--------------------------------------------------------------------------------
function mq08_201_secret_melody_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n023_ranger_fugus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n023_ranger_fugus-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n023_ranger_fugus-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n023_ranger_fugus-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n023_ranger_fugus-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n023_ranger_fugus-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n023_ranger_fugus-accepting-a" then
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 10) == 1   or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 110) == 1   or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 120) == 1   or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 21) == 1   or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 134) == 1   or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 144) == 1   or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 30) == 1  then
									npc_talk_index = "n023_ranger_fugus-accepting-a";

				else
									npc_talk_index = "n023_ranger_fugus-accepting-b";

				end

	end
	if npc_talk_index == "n023_ranger_fugus-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,201, 2);
				api_quest_SetJournalStep( pRoom, userObjID,201, 1);
				api_quest_SetQuestStep( pRoom, userObjID,201, 1);
				npc_talk_index = "n023_ranger_fugus-1";

	end
	if npc_talk_index == "n023_ranger_fugus-1-a" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1  or api_user_GetUserClassID( pRoom, userObjID) == 2  or api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n023_ranger_fugus-1-c";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3  or api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n023_ranger_fugus-1-a";

				else
									npc_talk_index = "n023_ranger_fugus-1-f";

				end

				end
	end
	if npc_talk_index == "n023_ranger_fugus-1-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				if api_quest_HasQuestItem( pRoom, userObjID, 300012, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300012, api_quest_HasQuestItem( pRoom, userObjID, 300012, 1));
				end
	end
	if npc_talk_index == "n023_ranger_fugus-1-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				if api_quest_HasQuestItem( pRoom, userObjID, 300012, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300012, api_quest_HasQuestItem( pRoom, userObjID, 300012, 1));
				end
	end
	if npc_talk_index == "n023_ranger_fugus-3-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-c" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-d" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-e" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-f" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-g" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-h" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-i" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-j" then 
	end
	if npc_talk_index == "n023_ranger_fugus-3-k" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 350, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200350, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300015, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n023_ranger_fugus-5-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-5-c" then 
	end
	if npc_talk_index == "n023_ranger_fugus-5-d" then 
	end
	if npc_talk_index == "n023_ranger_fugus-5-e" then 
	end
	if npc_talk_index == "n023_ranger_fugus-5-f" then 
	end
	if npc_talk_index == "n023_ranger_fugus-5-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2011, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2011, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2012, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2012, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2013, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2013, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2014, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2014, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2014, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2014, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2016, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2016, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2011, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2011, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2011, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2011, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2011, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2011, questID, 1);
			 end 
	end
	if npc_talk_index == "n023_ranger_fugus-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 202, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 202, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 202, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300089, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300089, api_quest_HasQuestItem( pRoom, userObjID, 300089, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n023_ranger_fugus-1", "mq08_202_rescue_girl.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_201_secret_melody_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n024_adventurer_guildmaster_decud-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				if api_user_GetUserClassID( pRoom, userObjID) == 1  or api_user_GetUserClassID( pRoom, userObjID) == 2  or api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n024_adventurer_guildmaster_decud-2-k";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3  or api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n024_adventurer_guildmaster_decud-2";

				else
									npc_talk_index = "n024_adventurer_guildmaster_decud-2-u";

				end

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-w" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-g" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-h" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-i" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-j" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-l" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-m" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-n" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-o" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-p" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-q" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-r" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-s" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-t" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-a" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_201_secret_melody_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 201);
	if qstep == 4 and CountIndex == 350 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				 local TableItem = { 
				 { 300089, 1 }  , 
				 { 300015, 1 }  
 				} 
				if api_quest_CheckQuestInvenForAddItemList( pRoom, userObjID, TableItem) == 1 then
						api_quest_AddQuestItem( pRoom, userObjID, 300089, 1, questID);
						api_quest_AddQuestItem( pRoom, userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 4 and CountIndex == 300015 then

	end
	if qstep == 4 and CountIndex == 200350 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				 local TableItem = { 
				 { 300089, 1 }  , 
				 { 300015, 1 }  
 				} 
				if api_quest_CheckQuestInvenForAddItemList( pRoom, userObjID, TableItem) == 1 then
						api_quest_AddQuestItem( pRoom, userObjID, 300089, 1, questID);
						api_quest_AddQuestItem( pRoom, userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function mq08_201_secret_melody_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 201);
	if qstep == 4 and CountIndex == 350 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 300015 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 200350 and Count >= TargetCount  then

	end
end

function mq08_201_secret_melody_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 201);
	local questID=201;
end

function mq08_201_secret_melody_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq08_201_secret_melody_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_201_secret_melody_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,201, 2);
				api_quest_SetJournalStep( pRoom, userObjID,201, 1);
				api_quest_SetQuestStep( pRoom, userObjID,201, 1);
				npc_talk_index = "n023_ranger_fugus-1";
end

</GameServer>