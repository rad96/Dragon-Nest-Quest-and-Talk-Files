<VillageServer>

function sq11_606_eyewitness_stella_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 112 then
		sq11_606_eyewitness_stella_OnTalk_n112_wisp_kevin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		sq11_606_eyewitness_stella_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n112_wisp_kevin--------------------------------------------------------------------------------
function sq11_606_eyewitness_stella_OnTalk_n112_wisp_kevin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n112_wisp_kevin-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n112_wisp_kevin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n112_wisp_kevin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n112_wisp_kevin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n112_wisp_kevin-1-b" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-b" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-c" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-d" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-d" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-e" then 
	end
	if npc_talk_index == "n112_wisp_kevin-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1081, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201081, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300298, 1);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n112_wisp_kevin-3-b" then 
	end
	if npc_talk_index == "n112_wisp_kevin-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6060, true);
				 api_quest_RewardQuestUser(userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6060, true);
				 api_quest_RewardQuestUser(userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6060, true);
				 api_quest_RewardQuestUser(userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6060, true);
				 api_quest_RewardQuestUser(userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6060, true);
				 api_quest_RewardQuestUser(userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6060, true);
				 api_quest_RewardQuestUser(userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6060, true);
				 api_quest_RewardQuestUser(userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6060, true);
				 api_quest_RewardQuestUser(userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6060, true);
				 api_quest_RewardQuestUser(userObjID, 6060, questID, 1);
			 end 
	end
	if npc_talk_index == "n112_wisp_kevin-3-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem(userObjID, 300298, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300298, api_quest_HasQuestItem(userObjID, 300298, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 607, 1);
					api_quest_SetQuestStep(userObjID, 607, 1);
					api_quest_SetJournalStep(userObjID, 607, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n112_wisp_kevin-1", "sq11_607_engineer_of_future.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_606_eyewitness_stella_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6060, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6060, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6060, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6060, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6060, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6060, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6060, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6060, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6060, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceppted" then
				api_quest_AddQuest(userObjID,606, 1);
				api_quest_SetJournalStep(userObjID,606, 1);
				api_quest_SetQuestStep(userObjID,606, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_606_eyewitness_stella_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 606);
	if qstep == 2 and CountIndex == 1081 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300298, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300298, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 201081 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300298, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300298, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300298 then

	end
end

function sq11_606_eyewitness_stella_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 606);
	if qstep == 2 and CountIndex == 1081 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201081 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300298 and Count >= TargetCount  then

	end
end

function sq11_606_eyewitness_stella_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 606);
	local questID=606;
end

function sq11_606_eyewitness_stella_OnRemoteStart( userObjID, questID )
end

function sq11_606_eyewitness_stella_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_606_eyewitness_stella_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_606_eyewitness_stella_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 112 then
		sq11_606_eyewitness_stella_OnTalk_n112_wisp_kevin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		sq11_606_eyewitness_stella_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n112_wisp_kevin--------------------------------------------------------------------------------
function sq11_606_eyewitness_stella_OnTalk_n112_wisp_kevin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n112_wisp_kevin-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n112_wisp_kevin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n112_wisp_kevin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n112_wisp_kevin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n112_wisp_kevin-1-b" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-b" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-c" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-d" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-d" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-e" then 
	end
	if npc_talk_index == "n112_wisp_kevin-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1081, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201081, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300298, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n112_wisp_kevin-3-b" then 
	end
	if npc_talk_index == "n112_wisp_kevin-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6060, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6060, questID, 1);
			 end 
	end
	if npc_talk_index == "n112_wisp_kevin-3-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem( pRoom, userObjID, 300298, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300298, api_quest_HasQuestItem( pRoom, userObjID, 300298, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 607, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 607, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 607, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n112_wisp_kevin-1", "sq11_607_engineer_of_future.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_606_eyewitness_stella_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6060, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,606, 1);
				api_quest_SetJournalStep( pRoom, userObjID,606, 1);
				api_quest_SetQuestStep( pRoom, userObjID,606, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_606_eyewitness_stella_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 606);
	if qstep == 2 and CountIndex == 1081 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300298, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300298, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 201081 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300298, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300298, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300298 then

	end
end

function sq11_606_eyewitness_stella_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 606);
	if qstep == 2 and CountIndex == 1081 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201081 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300298 and Count >= TargetCount  then

	end
end

function sq11_606_eyewitness_stella_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 606);
	local questID=606;
end

function sq11_606_eyewitness_stella_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_606_eyewitness_stella_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_606_eyewitness_stella_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>