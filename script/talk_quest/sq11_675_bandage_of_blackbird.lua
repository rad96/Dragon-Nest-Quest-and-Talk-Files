<VillageServer>

function sq11_675_bandage_of_blackbird_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 338 then
		sq11_675_bandage_of_blackbird_OnTalk_n338_littlegirl_daisy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 339 then
		sq11_675_bandage_of_blackbird_OnTalk_n339_mystery_man(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n338_littlegirl_daisy--------------------------------------------------------------------------------
function sq11_675_bandage_of_blackbird_OnTalk_n338_littlegirl_daisy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n338_littlegirl_daisy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n338_littlegirl_daisy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n338_littlegirl_daisy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n338_littlegirl_daisy-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n338_littlegirl_daisy-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6750, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6750, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6750, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6750, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6750, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6750, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6750, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6750, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6750, false);
			 end 

	end
	if npc_talk_index == "n338_littlegirl_daisy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,675, 1);
				api_quest_SetJournalStep(userObjID,675, 1);
				api_quest_SetQuestStep(userObjID,675, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";

	end
	if npc_talk_index == "n338_littlegirl_daisy-1-b" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-c" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-d" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-e" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-f" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400297, 20);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 989, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 991, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 994, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200989, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200991, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 900994, 30000);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n338_littlegirl_daisy-5-b" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-5-c" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-5-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6750, true);
				 api_quest_RewardQuestUser(userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6750, true);
				 api_quest_RewardQuestUser(userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6750, true);
				 api_quest_RewardQuestUser(userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6750, true);
				 api_quest_RewardQuestUser(userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6750, true);
				 api_quest_RewardQuestUser(userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6750, true);
				 api_quest_RewardQuestUser(userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6750, true);
				 api_quest_RewardQuestUser(userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6750, true);
				 api_quest_RewardQuestUser(userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6750, true);
				 api_quest_RewardQuestUser(userObjID, 6750, questID, 1);
			 end 
	end
	if npc_talk_index == "n338_littlegirl_daisy-5-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400297, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400297, api_quest_HasQuestItem(userObjID, 400297, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n339_mystery_man--------------------------------------------------------------------------------
function sq11_675_bandage_of_blackbird_OnTalk_n339_mystery_man(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n339_mystery_man-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n339_mystery_man-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n339_mystery_man-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n339_mystery_man-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n339_mystery_man-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n339_mystery_man-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n339_mystery_man-3-check_j1" then 
				if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n339_mystery_man-3-l";

				else
									npc_talk_index = "n339_mystery_man-3-a";

				end
	end
	if npc_talk_index == "n339_mystery_man-3-check_j2" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n339_mystery_man-4";
				api_quest_SetQuestStep(userObjID, questID,4);

				else
									npc_talk_index = "n339_mystery_man-3-b";

				end
	end
	if npc_talk_index == "n339_mystery_man-3-c" then 
	end
	if npc_talk_index == "n339_mystery_man-3-d" then 
	end
	if npc_talk_index == "n339_mystery_man-3-e" then 
	end
	if npc_talk_index == "n339_mystery_man-3-f" then 
	end
	if npc_talk_index == "n339_mystery_man-3-g" then 
	end
	if npc_talk_index == "n339_mystery_man-3-h" then 
	end
	if npc_talk_index == "n339_mystery_man-3-i" then 
	end
	if npc_talk_index == "n339_mystery_man-3-j" then 
	end
	if npc_talk_index == "n339_mystery_man-3-k" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n339_mystery_man-3-m" then 
	end
	if npc_talk_index == "n339_mystery_man-3-n" then 
	end
	if npc_talk_index == "n339_mystery_man-3-o" then 
	end
	if npc_talk_index == "n339_mystery_man-3-f" then 
	end
	if npc_talk_index == "n339_mystery_man-4-b" then 
	end
	if npc_talk_index == "n339_mystery_man-4-c" then 
	end
	if npc_talk_index == "n339_mystery_man-4-d" then 
	end
	if npc_talk_index == "n339_mystery_man-4-e" then 
	end
	if npc_talk_index == "n339_mystery_man-4-f" then 
	end
	if npc_talk_index == "n339_mystery_man-4-g" then 
	end
	if npc_talk_index == "n339_mystery_man-4-h" then 
	end
	if npc_talk_index == "n339_mystery_man-4-i" then 
	end
	if npc_talk_index == "n339_mystery_man-4-j" then 
	end
	if npc_talk_index == "n339_mystery_man-4-k" then 
	end
	if npc_talk_index == "n339_mystery_man-4-l" then 
	end
	if npc_talk_index == "n339_mystery_man-4-m" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_675_bandage_of_blackbird_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 675);
	if qstep == 2 and CountIndex == 400297 then

	end
	if qstep == 2 and CountIndex == 989 then
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 991 then
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 994 then
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200989 then
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200991 then
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 900994 then
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

				end

	end
end

function sq11_675_bandage_of_blackbird_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 675);
	if qstep == 2 and CountIndex == 400297 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 989 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 991 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 994 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200989 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200991 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 900994 and Count >= TargetCount  then

	end
end

function sq11_675_bandage_of_blackbird_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 675);
	local questID=675;
end

function sq11_675_bandage_of_blackbird_OnRemoteStart( userObjID, questID )
end

function sq11_675_bandage_of_blackbird_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_675_bandage_of_blackbird_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,675, 1);
				api_quest_SetJournalStep(userObjID,675, 1);
				api_quest_SetQuestStep(userObjID,675, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";
end

</VillageServer>

<GameServer>
function sq11_675_bandage_of_blackbird_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 338 then
		sq11_675_bandage_of_blackbird_OnTalk_n338_littlegirl_daisy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 339 then
		sq11_675_bandage_of_blackbird_OnTalk_n339_mystery_man( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n338_littlegirl_daisy--------------------------------------------------------------------------------
function sq11_675_bandage_of_blackbird_OnTalk_n338_littlegirl_daisy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n338_littlegirl_daisy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n338_littlegirl_daisy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n338_littlegirl_daisy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n338_littlegirl_daisy-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n338_littlegirl_daisy-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, false);
			 end 

	end
	if npc_talk_index == "n338_littlegirl_daisy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,675, 1);
				api_quest_SetJournalStep( pRoom, userObjID,675, 1);
				api_quest_SetQuestStep( pRoom, userObjID,675, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";

	end
	if npc_talk_index == "n338_littlegirl_daisy-1-b" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-c" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-d" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-e" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400297, 20);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 989, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 991, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 994, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200989, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200991, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 900994, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n338_littlegirl_daisy-5-b" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-5-c" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-5-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6750, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6750, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6750, questID, 1);
			 end 
	end
	if npc_talk_index == "n338_littlegirl_daisy-5-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400297, api_quest_HasQuestItem( pRoom, userObjID, 400297, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n339_mystery_man--------------------------------------------------------------------------------
function sq11_675_bandage_of_blackbird_OnTalk_n339_mystery_man( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n339_mystery_man-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n339_mystery_man-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n339_mystery_man-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n339_mystery_man-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n339_mystery_man-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n339_mystery_man-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n339_mystery_man-3-check_j1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n339_mystery_man-3-l";

				else
									npc_talk_index = "n339_mystery_man-3-a";

				end
	end
	if npc_talk_index == "n339_mystery_man-3-check_j2" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n339_mystery_man-4";
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);

				else
									npc_talk_index = "n339_mystery_man-3-b";

				end
	end
	if npc_talk_index == "n339_mystery_man-3-c" then 
	end
	if npc_talk_index == "n339_mystery_man-3-d" then 
	end
	if npc_talk_index == "n339_mystery_man-3-e" then 
	end
	if npc_talk_index == "n339_mystery_man-3-f" then 
	end
	if npc_talk_index == "n339_mystery_man-3-g" then 
	end
	if npc_talk_index == "n339_mystery_man-3-h" then 
	end
	if npc_talk_index == "n339_mystery_man-3-i" then 
	end
	if npc_talk_index == "n339_mystery_man-3-j" then 
	end
	if npc_talk_index == "n339_mystery_man-3-k" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n339_mystery_man-3-m" then 
	end
	if npc_talk_index == "n339_mystery_man-3-n" then 
	end
	if npc_talk_index == "n339_mystery_man-3-o" then 
	end
	if npc_talk_index == "n339_mystery_man-3-f" then 
	end
	if npc_talk_index == "n339_mystery_man-4-b" then 
	end
	if npc_talk_index == "n339_mystery_man-4-c" then 
	end
	if npc_talk_index == "n339_mystery_man-4-d" then 
	end
	if npc_talk_index == "n339_mystery_man-4-e" then 
	end
	if npc_talk_index == "n339_mystery_man-4-f" then 
	end
	if npc_talk_index == "n339_mystery_man-4-g" then 
	end
	if npc_talk_index == "n339_mystery_man-4-h" then 
	end
	if npc_talk_index == "n339_mystery_man-4-i" then 
	end
	if npc_talk_index == "n339_mystery_man-4-j" then 
	end
	if npc_talk_index == "n339_mystery_man-4-k" then 
	end
	if npc_talk_index == "n339_mystery_man-4-l" then 
	end
	if npc_talk_index == "n339_mystery_man-4-m" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_675_bandage_of_blackbird_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 675);
	if qstep == 2 and CountIndex == 400297 then

	end
	if qstep == 2 and CountIndex == 989 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 991 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 994 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200989 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200991 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 900994 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400297, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400297, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400297, 20) >= 20 then
									api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

				end

	end
end

function sq11_675_bandage_of_blackbird_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 675);
	if qstep == 2 and CountIndex == 400297 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 989 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 991 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 994 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200989 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200991 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 900994 and Count >= TargetCount  then

	end
end

function sq11_675_bandage_of_blackbird_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 675);
	local questID=675;
end

function sq11_675_bandage_of_blackbird_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_675_bandage_of_blackbird_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_675_bandage_of_blackbird_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,675, 1);
				api_quest_SetJournalStep( pRoom, userObjID,675, 1);
				api_quest_SetQuestStep( pRoom, userObjID,675, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";
end

</GameServer>