<VillageServer>

function sq15_757_stronger_powers_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 458 then
		sq15_757_stronger_powers_OnTalk_n458_sword_of_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		sq15_757_stronger_powers_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n458_sword_of_geraint--------------------------------------------------------------------------------
function sq15_757_stronger_powers_OnTalk_n458_sword_of_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n458_sword_of_geraint-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n458_sword_of_geraint-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n458_sword_of_geraint-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n458_sword_of_geraint-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n458_sword_of_geraint-1-b" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-c" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-d" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-e" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-f" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-g" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 201400, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1400, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300421, 1);
	end
	if npc_talk_index == "n458_sword_of_geraint-3-b" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7570, true);
				 api_quest_RewardQuestUser(userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7570, true);
				 api_quest_RewardQuestUser(userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7570, true);
				 api_quest_RewardQuestUser(userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7570, true);
				 api_quest_RewardQuestUser(userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7570, true);
				 api_quest_RewardQuestUser(userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7570, true);
				 api_quest_RewardQuestUser(userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7570, true);
				 api_quest_RewardQuestUser(userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7570, true);
				 api_quest_RewardQuestUser(userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7570, true);
				 api_quest_RewardQuestUser(userObjID, 7570, questID, 1);
			 end 
	end
	if npc_talk_index == "n458_sword_of_geraint-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 758, 1);
					api_quest_SetQuestStep(userObjID, 758, 1);
					api_quest_SetJournalStep(userObjID, 758, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300421, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300421, api_quest_HasQuestItem(userObjID, 300421, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300422, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300422, api_quest_HasQuestItem(userObjID, 300422, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300423, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300423, api_quest_HasQuestItem(userObjID, 300423, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300424, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300424, api_quest_HasQuestItem(userObjID, 300424, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300425, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300425, api_quest_HasQuestItem(userObjID, 300425, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n458_sword_of_geraint-1", "sq15_758_ordeal_and_answer.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_757_stronger_powers_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7570, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7570, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7570, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7570, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7570, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7570, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7570, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7570, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7570, false);
			 end 

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest(userObjID,757, 1);
				api_quest_SetJournalStep(userObjID,757, 1);
				api_quest_SetQuestStep(userObjID,757, 1);
				npc_talk_index = "n711_warrior_fedro-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_757_stronger_powers_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 757);
	if qstep == 2 and CountIndex == 201400 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300421, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300421, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 1400 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300421, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300421, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300421 then

	end
end

function sq15_757_stronger_powers_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 757);
	if qstep == 2 and CountIndex == 201400 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1400 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300421 and Count >= TargetCount  then

	end
end

function sq15_757_stronger_powers_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 757);
	local questID=757;
end

function sq15_757_stronger_powers_OnRemoteStart( userObjID, questID )
end

function sq15_757_stronger_powers_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_757_stronger_powers_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,757, 1);
				api_quest_SetJournalStep(userObjID,757, 1);
				api_quest_SetQuestStep(userObjID,757, 1);
				npc_talk_index = "n711_warrior_fedro-1";
end

</VillageServer>

<GameServer>
function sq15_757_stronger_powers_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 458 then
		sq15_757_stronger_powers_OnTalk_n458_sword_of_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 711 then
		sq15_757_stronger_powers_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n458_sword_of_geraint--------------------------------------------------------------------------------
function sq15_757_stronger_powers_OnTalk_n458_sword_of_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n458_sword_of_geraint-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n458_sword_of_geraint-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n458_sword_of_geraint-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n458_sword_of_geraint-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n458_sword_of_geraint-1-b" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-c" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-d" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-e" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-f" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-g" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 201400, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1400, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300421, 1);
	end
	if npc_talk_index == "n458_sword_of_geraint-3-b" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7570, questID, 1);
			 end 
	end
	if npc_talk_index == "n458_sword_of_geraint-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 758, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 758, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 758, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300421, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300421, api_quest_HasQuestItem( pRoom, userObjID, 300421, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300422, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300422, api_quest_HasQuestItem( pRoom, userObjID, 300422, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300423, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300423, api_quest_HasQuestItem( pRoom, userObjID, 300423, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300424, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300424, api_quest_HasQuestItem( pRoom, userObjID, 300424, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300425, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300425, api_quest_HasQuestItem( pRoom, userObjID, 300425, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n458_sword_of_geraint-1", "sq15_758_ordeal_and_answer.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_757_stronger_powers_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7570, false);
			 end 

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,757, 1);
				api_quest_SetJournalStep( pRoom, userObjID,757, 1);
				api_quest_SetQuestStep( pRoom, userObjID,757, 1);
				npc_talk_index = "n711_warrior_fedro-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_757_stronger_powers_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 757);
	if qstep == 2 and CountIndex == 201400 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300421, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300421, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 1400 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300421, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300421, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300421 then

	end
end

function sq15_757_stronger_powers_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 757);
	if qstep == 2 and CountIndex == 201400 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1400 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300421 and Count >= TargetCount  then

	end
end

function sq15_757_stronger_powers_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 757);
	local questID=757;
end

function sq15_757_stronger_powers_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_757_stronger_powers_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_757_stronger_powers_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,757, 1);
				api_quest_SetJournalStep( pRoom, userObjID,757, 1);
				api_quest_SetQuestStep( pRoom, userObjID,757, 1);
				npc_talk_index = "n711_warrior_fedro-1";
end

</GameServer>