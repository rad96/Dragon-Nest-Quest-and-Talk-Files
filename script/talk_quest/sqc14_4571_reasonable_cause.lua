<VillageServer>

function sqc14_4571_reasonable_cause_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1619 then
		sqc14_4571_reasonable_cause_OnTalk_n1619_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1619_sidel--------------------------------------------------------------------------------
function sqc14_4571_reasonable_cause_OnTalk_n1619_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1619_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1619_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1619_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1619_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1619_sidel-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45710, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45710, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45710, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45710, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45710, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45710, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45710, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45710, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45710, false);
			 end 

	end
	if npc_talk_index == "n1619_sidel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4571, 1);
				api_quest_SetJournalStep(userObjID,4571, 1);
				api_quest_SetQuestStep(userObjID,4571, 1);
				npc_talk_index = "n1619_sidel-1";

	end
	if npc_talk_index == "n1619_sidel-1-b" then 
	end
	if npc_talk_index == "n1619_sidel-1-c" then 
	end
	if npc_talk_index == "n1619_sidel-1-d" then 
	end
	if npc_talk_index == "n1619_sidel-1-e" then 
	end
	if npc_talk_index == "n1619_sidel-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 602033, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 702033, 30000);
	end
	if npc_talk_index == "n1619_sidel-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45710, true);
				 api_quest_RewardQuestUser(userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45710, true);
				 api_quest_RewardQuestUser(userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45710, true);
				 api_quest_RewardQuestUser(userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45710, true);
				 api_quest_RewardQuestUser(userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45710, true);
				 api_quest_RewardQuestUser(userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45710, true);
				 api_quest_RewardQuestUser(userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45710, true);
				 api_quest_RewardQuestUser(userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45710, true);
				 api_quest_RewardQuestUser(userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45710, true);
				 api_quest_RewardQuestUser(userObjID, 45710, questID, 1);
			 end 
	end
	if npc_talk_index == "n1619_sidel-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4572, 1);
					api_quest_SetQuestStep(userObjID, 4572, 1);
					api_quest_SetJournalStep(userObjID, 4572, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1619_sidel-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1619_sidel-1", "sqc14_4572_dangerous_monsters.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4571_reasonable_cause_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4571);
	if qstep == 2 and CountIndex == 602033 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 702033 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function sqc14_4571_reasonable_cause_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4571);
	if qstep == 2 and CountIndex == 602033 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 702033 and Count >= TargetCount  then

	end
end

function sqc14_4571_reasonable_cause_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4571);
	local questID=4571;
end

function sqc14_4571_reasonable_cause_OnRemoteStart( userObjID, questID )
end

function sqc14_4571_reasonable_cause_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc14_4571_reasonable_cause_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4571, 1);
				api_quest_SetJournalStep(userObjID,4571, 1);
				api_quest_SetQuestStep(userObjID,4571, 1);
				npc_talk_index = "n1619_sidel-1";
end

</VillageServer>

<GameServer>
function sqc14_4571_reasonable_cause_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1619 then
		sqc14_4571_reasonable_cause_OnTalk_n1619_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1619_sidel--------------------------------------------------------------------------------
function sqc14_4571_reasonable_cause_OnTalk_n1619_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1619_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1619_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1619_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1619_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1619_sidel-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, false);
			 end 

	end
	if npc_talk_index == "n1619_sidel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4571, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4571, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4571, 1);
				npc_talk_index = "n1619_sidel-1";

	end
	if npc_talk_index == "n1619_sidel-1-b" then 
	end
	if npc_talk_index == "n1619_sidel-1-c" then 
	end
	if npc_talk_index == "n1619_sidel-1-d" then 
	end
	if npc_talk_index == "n1619_sidel-1-e" then 
	end
	if npc_talk_index == "n1619_sidel-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 602033, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 702033, 30000);
	end
	if npc_talk_index == "n1619_sidel-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45710, questID, 1);
			 end 
	end
	if npc_talk_index == "n1619_sidel-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4572, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4572, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4572, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1619_sidel-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1619_sidel-1", "sqc14_4572_dangerous_monsters.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4571_reasonable_cause_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4571);
	if qstep == 2 and CountIndex == 602033 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 702033 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function sqc14_4571_reasonable_cause_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4571);
	if qstep == 2 and CountIndex == 602033 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 702033 and Count >= TargetCount  then

	end
end

function sqc14_4571_reasonable_cause_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4571);
	local questID=4571;
end

function sqc14_4571_reasonable_cause_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc14_4571_reasonable_cause_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc14_4571_reasonable_cause_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4571, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4571, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4571, 1);
				npc_talk_index = "n1619_sidel-1";
end

</GameServer>