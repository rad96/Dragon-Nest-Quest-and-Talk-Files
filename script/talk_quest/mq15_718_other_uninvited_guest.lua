<VillageServer>

function mq15_718_other_uninvited_guest_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 701 then
		mq15_718_other_uninvited_guest_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 713 then
		mq15_718_other_uninvited_guest_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 726 then
		mq15_718_other_uninvited_guest_OnTalk_n726_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_718_other_uninvited_guest_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest(userObjID,718, 2);
				api_quest_SetJournalStep(userObjID,718, 1);
				api_quest_SetQuestStep(userObjID,718, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-2-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-o" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				if api_quest_HasQuestItem(userObjID, 300389, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300389, api_quest_HasQuestItem(userObjID, 300389, 1));
				end
	end
	if npc_talk_index == "n701_oldkarakule-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7181, true);
				 api_quest_RewardQuestUser(userObjID, 7181, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7182, true);
				 api_quest_RewardQuestUser(userObjID, 7182, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7183, true);
				 api_quest_RewardQuestUser(userObjID, 7183, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7184, true);
				 api_quest_RewardQuestUser(userObjID, 7184, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7185, true);
				 api_quest_RewardQuestUser(userObjID, 7185, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7186, true);
				 api_quest_RewardQuestUser(userObjID, 7186, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7181, true);
				 api_quest_RewardQuestUser(userObjID, 7181, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7181, true);
				 api_quest_RewardQuestUser(userObjID, 7181, questID, 1);
			 end 
	end
	if npc_talk_index == "n701_oldkarakule-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 719, 2);
					api_quest_SetQuestStep(userObjID, 719, 1);
					api_quest_SetJournalStep(userObjID, 719, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n701_oldkarakule-1", "mq15_719_dark_past.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n713_soceress_tamara--------------------------------------------------------------------------------
function mq15_718_other_uninvited_guest_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n713_soceress_tamara-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n713_soceress_tamara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-1-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-c" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-d" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-e" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-f" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-g" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-h" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-i" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-j" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-k" then 
	end
	if npc_talk_index == "n713_soceress_tamara-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300389, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300389, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n726_velskud--------------------------------------------------------------------------------
function mq15_718_other_uninvited_guest_OnTalk_n726_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n726_velskud-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n726_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n726_velskud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n726_velskud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n726_velskud-3-b" then 
	end
	if npc_talk_index == "n726_velskud-3-c" then 
	end
	if npc_talk_index == "n726_velskud-3-d" then 
	end
	if npc_talk_index == "n726_velskud-3-e" then 
	end
	if npc_talk_index == "n726_velskud-3-f" then 
	end
	if npc_talk_index == "n726_velskud-3-g" then 
	end
	if npc_talk_index == "n726_velskud-3-h" then 
	end
	if npc_talk_index == "n726_velskud-3-i" then 
	end
	if npc_talk_index == "n726_velskud-3-j" then 
	end
	if npc_talk_index == "n726_velskud-3-k" then 
	end
	if npc_talk_index == "n726_velskud-3-l" then 
	end
	if npc_talk_index == "n726_velskud-3-m" then 
	end
	if npc_talk_index == "n726_velskud-3-n" then 
	end
	if npc_talk_index == "n726_velskud-3-o" then 
	end
	if npc_talk_index == "n726_velskud-3-p" then 
	end
	if npc_talk_index == "n726_velskud-3-q" then 
	end
	if npc_talk_index == "n726_velskud-3-r" then 
	end
	if npc_talk_index == "n726_velskud-3-s" then 
	end
	if npc_talk_index == "n726_velskud-3-t" then 
	end
	if npc_talk_index == "n726_velskud-3-u" then 
	end
	if npc_talk_index == "n726_velskud-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_718_other_uninvited_guest_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 718);
end

function mq15_718_other_uninvited_guest_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 718);
end

function mq15_718_other_uninvited_guest_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 718);
	local questID=718;
end

function mq15_718_other_uninvited_guest_OnRemoteStart( userObjID, questID )
end

function mq15_718_other_uninvited_guest_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_718_other_uninvited_guest_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,718, 2);
				api_quest_SetJournalStep(userObjID,718, 1);
				api_quest_SetQuestStep(userObjID,718, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</VillageServer>

<GameServer>
function mq15_718_other_uninvited_guest_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 701 then
		mq15_718_other_uninvited_guest_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 713 then
		mq15_718_other_uninvited_guest_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 726 then
		mq15_718_other_uninvited_guest_OnTalk_n726_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_718_other_uninvited_guest_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n701_oldkarakule-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,718, 2);
				api_quest_SetJournalStep( pRoom, userObjID,718, 1);
				api_quest_SetQuestStep( pRoom, userObjID,718, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-2-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-i" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-j" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-k" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-l" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-m" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-n" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-o" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				if api_quest_HasQuestItem( pRoom, userObjID, 300389, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300389, api_quest_HasQuestItem( pRoom, userObjID, 300389, 1));
				end
	end
	if npc_talk_index == "n701_oldkarakule-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7181, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7182, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7182, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7183, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7183, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7184, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7184, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7185, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7185, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7186, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7186, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7181, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7181, questID, 1);
			 end 
	end
	if npc_talk_index == "n701_oldkarakule-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 719, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 719, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 719, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n701_oldkarakule-1", "mq15_719_dark_past.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n713_soceress_tamara--------------------------------------------------------------------------------
function mq15_718_other_uninvited_guest_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n713_soceress_tamara-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n713_soceress_tamara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-1-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-c" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-d" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-e" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-f" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-g" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-h" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-i" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-j" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-k" then 
	end
	if npc_talk_index == "n713_soceress_tamara-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300389, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300389, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n726_velskud--------------------------------------------------------------------------------
function mq15_718_other_uninvited_guest_OnTalk_n726_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n726_velskud-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n726_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n726_velskud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n726_velskud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n726_velskud-3-b" then 
	end
	if npc_talk_index == "n726_velskud-3-c" then 
	end
	if npc_talk_index == "n726_velskud-3-d" then 
	end
	if npc_talk_index == "n726_velskud-3-e" then 
	end
	if npc_talk_index == "n726_velskud-3-f" then 
	end
	if npc_talk_index == "n726_velskud-3-g" then 
	end
	if npc_talk_index == "n726_velskud-3-h" then 
	end
	if npc_talk_index == "n726_velskud-3-i" then 
	end
	if npc_talk_index == "n726_velskud-3-j" then 
	end
	if npc_talk_index == "n726_velskud-3-k" then 
	end
	if npc_talk_index == "n726_velskud-3-l" then 
	end
	if npc_talk_index == "n726_velskud-3-m" then 
	end
	if npc_talk_index == "n726_velskud-3-n" then 
	end
	if npc_talk_index == "n726_velskud-3-o" then 
	end
	if npc_talk_index == "n726_velskud-3-p" then 
	end
	if npc_talk_index == "n726_velskud-3-q" then 
	end
	if npc_talk_index == "n726_velskud-3-r" then 
	end
	if npc_talk_index == "n726_velskud-3-s" then 
	end
	if npc_talk_index == "n726_velskud-3-t" then 
	end
	if npc_talk_index == "n726_velskud-3-u" then 
	end
	if npc_talk_index == "n726_velskud-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_718_other_uninvited_guest_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 718);
end

function mq15_718_other_uninvited_guest_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 718);
end

function mq15_718_other_uninvited_guest_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 718);
	local questID=718;
end

function mq15_718_other_uninvited_guest_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_718_other_uninvited_guest_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_718_other_uninvited_guest_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,718, 2);
				api_quest_SetJournalStep( pRoom, userObjID,718, 1);
				api_quest_SetQuestStep( pRoom, userObjID,718, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</GameServer>