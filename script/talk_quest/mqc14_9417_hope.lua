<VillageServer>

function mqc14_9417_hope_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1523 then
		mqc14_9417_hope_OnTalk_n1523_aruno(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9417_hope_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1524 then
		mqc14_9417_hope_OnTalk_n1524_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1525 then
		mqc14_9417_hope_OnTalk_n1525_aruno(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1526 then
		mqc14_9417_hope_OnTalk_n1526_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1527 then
		mqc14_9417_hope_OnTalk_n1527_people(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1532 then
		mqc14_9417_hope_OnTalk_n1532_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1523_aruno--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1523_aruno(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1523_aruno-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9417, 2);
				api_quest_SetJournalStep(userObjID,9417, 1);
				api_quest_SetQuestStep(userObjID,9417, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1524_argenta--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1524_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1524_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1525_aruno--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1525_aruno(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1525_aruno-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1525_aruno-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1525_aruno-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1525_aruno-5-class_check" then 
				if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n1525_aruno-5-d";

				else
									npc_talk_index = "n1525_aruno-5-b";

				end
	end
	if npc_talk_index == "n1525_aruno-5-c" then 
	end
	if npc_talk_index == "n1525_aruno-5-g" then 
	end
	if npc_talk_index == "n1525_aruno-5-e" then 
	end
	if npc_talk_index == "n1525_aruno-5-f" then 
	end
	if npc_talk_index == "n1525_aruno-5-g" then 
	end
	if npc_talk_index == "n1525_aruno-5-h" then 
	end
	if npc_talk_index == "n1525_aruno-5-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94170, true);
				 api_quest_RewardQuestUser(userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94170, true);
				 api_quest_RewardQuestUser(userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94170, true);
				 api_quest_RewardQuestUser(userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94170, true);
				 api_quest_RewardQuestUser(userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94170, true);
				 api_quest_RewardQuestUser(userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94170, true);
				 api_quest_RewardQuestUser(userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94170, true);
				 api_quest_RewardQuestUser(userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94170, true);
				 api_quest_RewardQuestUser(userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94170, true);
				 api_quest_RewardQuestUser(userObjID, 94170, questID, 1);
			 end 
	end
	if npc_talk_index == "n1525_aruno-5-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9418, 2);
					api_quest_SetQuestStep(userObjID, 9418, 1);
					api_quest_SetJournalStep(userObjID, 9418, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1525_aruno-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1525_aruno-1", "mqc14_9418_cry.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1526_for_memory--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1526_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1526_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1526_for_memory-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1526_for_memory-3";
				if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n1526_for_memory-3-a";

				else
									npc_talk_index = "n1526_for_memory-3";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1526_for_memory-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1526_for_memory-1-b" then 
	end
	if npc_talk_index == "n1526_for_memory-1-c" then 
	end
	if npc_talk_index == "n1526_for_memory-1-d" then 
	end
	if npc_talk_index == "n1526_for_memory-1-e" then 
	end
	if npc_talk_index == "n1526_for_memory-1-f" then 
	end
	if npc_talk_index == "n1526_for_memory-1-g" then 
	end
	if npc_talk_index == "n1526_for_memory-1-h" then 
	end
	if npc_talk_index == "n1526_for_memory-1-i" then 
	end
	if npc_talk_index == "n1526_for_memory-1-j" then 
	end
	if npc_talk_index == "n1526_for_memory-1-k" then 
	end
	if npc_talk_index == "n1526_for_memory-1-l" then 
	end
	if npc_talk_index == "n1526_for_memory-1-m" then 
	end
	if npc_talk_index == "n1526_for_memory-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n1526_for_memory-3-b" then 
	end
	if npc_talk_index == "n1526_for_memory-3-c" then 
	end
	if npc_talk_index == "n1526_for_memory-3-d" then 
	end
	if npc_talk_index == "n1526_for_memory-3-e" then 
	end
	if npc_talk_index == "n1526_for_memory-3-f" then 
	end
	if npc_talk_index == "n1526_for_memory-3-g" then 
	end
	if npc_talk_index == "n1526_for_memory-3-h" then 
	end
	if npc_talk_index == "n1526_for_memory-3-i" then 
	end
	if npc_talk_index == "n1526_for_memory-3-j" then 
	end
	if npc_talk_index == "n1526_for_memory-3-k" then 
	end
	if npc_talk_index == "n1526_for_memory-3-l" then 
	end
	if npc_talk_index == "n1526_for_memory-3-m" then 
	end
	if npc_talk_index == "n1526_for_memory-3-n" then 
	end
	if npc_talk_index == "n1526_for_memory-3-o" then 
	end
	if npc_talk_index == "n1526_for_memory-3-p" then 
	end
	if npc_talk_index == "n1526_for_memory-3-q" then 
	end
	if npc_talk_index == "n1526_for_memory-3-r" then 
	end
	if npc_talk_index == "n1526_for_memory-3-s" then 
	end
	if npc_talk_index == "n1526_for_memory-3-t" then 
	end
	if npc_talk_index == "n1526_for_memory-3-u" then 
	end
	if npc_talk_index == "n1526_for_memory-3-v" then 
	end
	if npc_talk_index == "n1526_for_memory-3-w" then 
	end
	if npc_talk_index == "n1526_for_memory-3-x" then 
	end
	if npc_talk_index == "n1526_for_memory-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1527_people--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1527_people(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1527_people-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1527_people-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1527_people-4-b" then 
	end
	if npc_talk_index == "n1527_people-4-c" then 
	end
	if npc_talk_index == "n1527_people-4-d" then 
	end
	if npc_talk_index == "n1527_people-4-e" then 
	end
	if npc_talk_index == "n1527_people-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1532_gaharam--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1532_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1532_gaharam-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9417_hope_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9417);
end

function mqc14_9417_hope_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9417);
end

function mqc14_9417_hope_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9417);
	local questID=9417;
end

function mqc14_9417_hope_OnRemoteStart( userObjID, questID )
end

function mqc14_9417_hope_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9417_hope_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9417, 2);
				api_quest_SetJournalStep(userObjID,9417, 1);
				api_quest_SetQuestStep(userObjID,9417, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</VillageServer>

<GameServer>
function mqc14_9417_hope_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1523 then
		mqc14_9417_hope_OnTalk_n1523_aruno( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9417_hope_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1524 then
		mqc14_9417_hope_OnTalk_n1524_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1525 then
		mqc14_9417_hope_OnTalk_n1525_aruno( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1526 then
		mqc14_9417_hope_OnTalk_n1526_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1527 then
		mqc14_9417_hope_OnTalk_n1527_people( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1532 then
		mqc14_9417_hope_OnTalk_n1532_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1523_aruno--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1523_aruno( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1523_aruno-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9417, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9417, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9417, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1524_argenta--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1524_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1524_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1525_aruno--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1525_aruno( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1525_aruno-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1525_aruno-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1525_aruno-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1525_aruno-5-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n1525_aruno-5-d";

				else
									npc_talk_index = "n1525_aruno-5-b";

				end
	end
	if npc_talk_index == "n1525_aruno-5-c" then 
	end
	if npc_talk_index == "n1525_aruno-5-g" then 
	end
	if npc_talk_index == "n1525_aruno-5-e" then 
	end
	if npc_talk_index == "n1525_aruno-5-f" then 
	end
	if npc_talk_index == "n1525_aruno-5-g" then 
	end
	if npc_talk_index == "n1525_aruno-5-h" then 
	end
	if npc_talk_index == "n1525_aruno-5-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94170, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94170, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94170, questID, 1);
			 end 
	end
	if npc_talk_index == "n1525_aruno-5-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9418, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9418, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9418, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1525_aruno-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1525_aruno-1", "mqc14_9418_cry.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1526_for_memory--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1526_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1526_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1526_for_memory-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1526_for_memory-3";
				if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n1526_for_memory-3-a";

				else
									npc_talk_index = "n1526_for_memory-3";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1526_for_memory-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1526_for_memory-1-b" then 
	end
	if npc_talk_index == "n1526_for_memory-1-c" then 
	end
	if npc_talk_index == "n1526_for_memory-1-d" then 
	end
	if npc_talk_index == "n1526_for_memory-1-e" then 
	end
	if npc_talk_index == "n1526_for_memory-1-f" then 
	end
	if npc_talk_index == "n1526_for_memory-1-g" then 
	end
	if npc_talk_index == "n1526_for_memory-1-h" then 
	end
	if npc_talk_index == "n1526_for_memory-1-i" then 
	end
	if npc_talk_index == "n1526_for_memory-1-j" then 
	end
	if npc_talk_index == "n1526_for_memory-1-k" then 
	end
	if npc_talk_index == "n1526_for_memory-1-l" then 
	end
	if npc_talk_index == "n1526_for_memory-1-m" then 
	end
	if npc_talk_index == "n1526_for_memory-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n1526_for_memory-3-b" then 
	end
	if npc_talk_index == "n1526_for_memory-3-c" then 
	end
	if npc_talk_index == "n1526_for_memory-3-d" then 
	end
	if npc_talk_index == "n1526_for_memory-3-e" then 
	end
	if npc_talk_index == "n1526_for_memory-3-f" then 
	end
	if npc_talk_index == "n1526_for_memory-3-g" then 
	end
	if npc_talk_index == "n1526_for_memory-3-h" then 
	end
	if npc_talk_index == "n1526_for_memory-3-i" then 
	end
	if npc_talk_index == "n1526_for_memory-3-j" then 
	end
	if npc_talk_index == "n1526_for_memory-3-k" then 
	end
	if npc_talk_index == "n1526_for_memory-3-l" then 
	end
	if npc_talk_index == "n1526_for_memory-3-m" then 
	end
	if npc_talk_index == "n1526_for_memory-3-n" then 
	end
	if npc_talk_index == "n1526_for_memory-3-o" then 
	end
	if npc_talk_index == "n1526_for_memory-3-p" then 
	end
	if npc_talk_index == "n1526_for_memory-3-q" then 
	end
	if npc_talk_index == "n1526_for_memory-3-r" then 
	end
	if npc_talk_index == "n1526_for_memory-3-s" then 
	end
	if npc_talk_index == "n1526_for_memory-3-t" then 
	end
	if npc_talk_index == "n1526_for_memory-3-u" then 
	end
	if npc_talk_index == "n1526_for_memory-3-v" then 
	end
	if npc_talk_index == "n1526_for_memory-3-w" then 
	end
	if npc_talk_index == "n1526_for_memory-3-x" then 
	end
	if npc_talk_index == "n1526_for_memory-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1527_people--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1527_people( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1527_people-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1527_people-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1527_people-4-b" then 
	end
	if npc_talk_index == "n1527_people-4-c" then 
	end
	if npc_talk_index == "n1527_people-4-d" then 
	end
	if npc_talk_index == "n1527_people-4-e" then 
	end
	if npc_talk_index == "n1527_people-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1532_gaharam--------------------------------------------------------------------------------
function mqc14_9417_hope_OnTalk_n1532_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1532_gaharam-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9417_hope_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9417);
end

function mqc14_9417_hope_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9417);
end

function mqc14_9417_hope_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9417);
	local questID=9417;
end

function mqc14_9417_hope_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9417_hope_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9417_hope_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9417, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9417, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9417, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</GameServer>