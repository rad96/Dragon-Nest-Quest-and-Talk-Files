<VillageServer>

function nq22_7902_pope_talk_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 277 then
		nq22_7902_pope_talk_OnTalk_n277_bluff_dealers_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_pope--------------------------------------------------------------------------------
function nq22_7902_pope_talk_OnTalk_n277_bluff_dealers_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n277_bluff_dealers_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_pope-accepting-a" then
				api_npc_AddFavorPoint( userObjID, 277, 200 );
				api_quest_ForceCompleteQuest(userObjID, 7902, 0, 1, 1, 0);

	end
	if npc_talk_index == "n277_bluff_dealers_pope-accepting-b" then
				api_npc_AddFavorPoint( userObjID, 277, 400 );
				api_quest_ForceCompleteQuest(userObjID, 7902, 0, 1, 1, 0);

	end
	if npc_talk_index == "n277_bluff_dealers_pope-accepting-c" then
				api_npc_AddMalicePoint( userObjID, 277, 400 );
				api_quest_ForceCompleteQuest(userObjID, 7902, 0, 1, 1, 0);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq22_7902_pope_talk_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7902);
end

function nq22_7902_pope_talk_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7902);
end

function nq22_7902_pope_talk_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7902);
	local questID=7902;
end

function nq22_7902_pope_talk_OnRemoteStart( userObjID, questID )
end

function nq22_7902_pope_talk_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq22_7902_pope_talk_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq22_7902_pope_talk_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 277 then
		nq22_7902_pope_talk_OnTalk_n277_bluff_dealers_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n277_bluff_dealers_pope--------------------------------------------------------------------------------
function nq22_7902_pope_talk_OnTalk_n277_bluff_dealers_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n277_bluff_dealers_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n277_bluff_dealers_pope-accepting-a" then
				api_npc_AddFavorPoint( pRoom,  userObjID, 277, 200 );
				api_quest_ForceCompleteQuest( pRoom, userObjID, 7902, 0, 1, 1, 0);

	end
	if npc_talk_index == "n277_bluff_dealers_pope-accepting-b" then
				api_npc_AddFavorPoint( pRoom,  userObjID, 277, 400 );
				api_quest_ForceCompleteQuest( pRoom, userObjID, 7902, 0, 1, 1, 0);

	end
	if npc_talk_index == "n277_bluff_dealers_pope-accepting-c" then
				api_npc_AddMalicePoint( pRoom,  userObjID, 277, 400 );
				api_quest_ForceCompleteQuest( pRoom, userObjID, 7902, 0, 1, 1, 0);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq22_7902_pope_talk_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7902);
end

function nq22_7902_pope_talk_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7902);
end

function nq22_7902_pope_talk_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7902);
	local questID=7902;
end

function nq22_7902_pope_talk_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq22_7902_pope_talk_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq22_7902_pope_talk_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>