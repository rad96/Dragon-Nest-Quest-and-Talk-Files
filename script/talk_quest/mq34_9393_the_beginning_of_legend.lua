<VillageServer>

function mq34_9393_the_beginning_of_legend_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1231 then
		mq34_9393_the_beginning_of_legend_OnTalk_n1231_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1234 then
		mq34_9393_the_beginning_of_legend_OnTalk_n1234_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mq34_9393_the_beginning_of_legend_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1231_white_dragon--------------------------------------------------------------------------------
function mq34_9393_the_beginning_of_legend_OnTalk_n1231_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1231_white_dragon-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1231_white_dragon-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1231_white_dragon-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1231_white_dragon-4-b" then 
	end
	if npc_talk_index == "n1231_white_dragon-4-c" then 
	end
	if npc_talk_index == "n1231_white_dragon-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400418, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400418, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1234_white_dragon--------------------------------------------------------------------------------
function mq34_9393_the_beginning_of_legend_OnTalk_n1234_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1234_white_dragon-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1234_white_dragon-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1234_white_dragon-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1234_white_dragon-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1234_white_dragon-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1234_white_dragon-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1234_white_dragon-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1234_white_dragon-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9393, 2);
				api_quest_SetJournalStep(userObjID,9393, 1);
				api_quest_SetQuestStep(userObjID,9393, 1);
				npc_talk_index = "n1234_white_dragon-1";

	end
	if npc_talk_index == "n1234_white_dragon-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n1234_white_dragon-3-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-3-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1234_white_dragon-6-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-class_check" then 
				if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1234_white_dragon-6-g";

				else
									npc_talk_index = "n1234_white_dragon-6-f";

				end
	end
	if npc_talk_index == "n1234_white_dragon-6-class_check_1" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1234_white_dragon-6-h";

				else
									npc_talk_index = "n1234_white_dragon-6-p";

				end
	end
	if npc_talk_index == "n1234_white_dragon-6-class_check_1" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1234_white_dragon-6-h";

				else
									npc_talk_index = "n1234_white_dragon-6-p";

				end
	end
	if npc_talk_index == "n1234_white_dragon-6-class_check_1" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1234_white_dragon-6-h";

				else
									npc_talk_index = "n1234_white_dragon-6-p";

				end
	end
	if npc_talk_index == "n1234_white_dragon-6-p" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-p" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-p" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-i" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-j" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-k" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-l" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-m" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-n" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-o" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-r" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-q" then 
	end
	if npc_talk_index == "n1234_white_dragon-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1234_white_dragon-6-s" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-p" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mq34_9393_the_beginning_of_legend_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n088_scholar_starshy-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-7-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93930, true);
				 api_quest_RewardQuestUser(userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93930, true);
				 api_quest_RewardQuestUser(userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93930, true);
				 api_quest_RewardQuestUser(userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93930, true);
				 api_quest_RewardQuestUser(userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93930, true);
				 api_quest_RewardQuestUser(userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93930, true);
				 api_quest_RewardQuestUser(userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93930, true);
				 api_quest_RewardQuestUser(userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93930, true);
				 api_quest_RewardQuestUser(userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93930, true);
				 api_quest_RewardQuestUser(userObjID, 93930, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-7-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9394, 2);
					api_quest_SetQuestStep(userObjID, 9394, 1);
					api_quest_SetJournalStep(userObjID, 9394, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400417, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400417, api_quest_HasQuestItem(userObjID, 400417, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400418, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400418, api_quest_HasQuestItem(userObjID, 400418, 1));
				end
	end
	if npc_talk_index == "n088_scholar_starshy-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n088_scholar_starshy-1", "mq34_9394_efforts_for_legend.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9393_the_beginning_of_legend_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9393);
end

function mq34_9393_the_beginning_of_legend_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9393);
end

function mq34_9393_the_beginning_of_legend_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9393);
	local questID=9393;
end

function mq34_9393_the_beginning_of_legend_OnRemoteStart( userObjID, questID )
end

function mq34_9393_the_beginning_of_legend_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9393_the_beginning_of_legend_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9393, 2);
				api_quest_SetJournalStep(userObjID,9393, 1);
				api_quest_SetQuestStep(userObjID,9393, 1);
				npc_talk_index = "n1234_white_dragon-1";
end

</VillageServer>

<GameServer>
function mq34_9393_the_beginning_of_legend_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1231 then
		mq34_9393_the_beginning_of_legend_OnTalk_n1231_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1234 then
		mq34_9393_the_beginning_of_legend_OnTalk_n1234_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		mq34_9393_the_beginning_of_legend_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1231_white_dragon--------------------------------------------------------------------------------
function mq34_9393_the_beginning_of_legend_OnTalk_n1231_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1231_white_dragon-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1231_white_dragon-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1231_white_dragon-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1231_white_dragon-4-b" then 
	end
	if npc_talk_index == "n1231_white_dragon-4-c" then 
	end
	if npc_talk_index == "n1231_white_dragon-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400418, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400418, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1234_white_dragon--------------------------------------------------------------------------------
function mq34_9393_the_beginning_of_legend_OnTalk_n1234_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1234_white_dragon-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1234_white_dragon-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1234_white_dragon-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1234_white_dragon-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1234_white_dragon-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1234_white_dragon-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1234_white_dragon-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1234_white_dragon-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9393, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9393, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9393, 1);
				npc_talk_index = "n1234_white_dragon-1";

	end
	if npc_talk_index == "n1234_white_dragon-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n1234_white_dragon-3-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-3-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1234_white_dragon-6-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1234_white_dragon-6-g";

				else
									npc_talk_index = "n1234_white_dragon-6-f";

				end
	end
	if npc_talk_index == "n1234_white_dragon-6-class_check_1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1234_white_dragon-6-h";

				else
									npc_talk_index = "n1234_white_dragon-6-p";

				end
	end
	if npc_talk_index == "n1234_white_dragon-6-class_check_1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1234_white_dragon-6-h";

				else
									npc_talk_index = "n1234_white_dragon-6-p";

				end
	end
	if npc_talk_index == "n1234_white_dragon-6-class_check_1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1234_white_dragon-6-h";

				else
									npc_talk_index = "n1234_white_dragon-6-p";

				end
	end
	if npc_talk_index == "n1234_white_dragon-6-p" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-p" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-p" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-i" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-j" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-k" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-l" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-m" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-n" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-o" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-r" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-q" then 
	end
	if npc_talk_index == "n1234_white_dragon-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1234_white_dragon-6-s" then 
	end
	if npc_talk_index == "n1234_white_dragon-6-p" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function mq34_9393_the_beginning_of_legend_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n088_scholar_starshy-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-7-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-7-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93930, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-7-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9394, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9394, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9394, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400417, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400417, api_quest_HasQuestItem( pRoom, userObjID, 400417, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400418, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400418, api_quest_HasQuestItem( pRoom, userObjID, 400418, 1));
				end
	end
	if npc_talk_index == "n088_scholar_starshy-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n088_scholar_starshy-1", "mq34_9394_efforts_for_legend.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9393_the_beginning_of_legend_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9393);
end

function mq34_9393_the_beginning_of_legend_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9393);
end

function mq34_9393_the_beginning_of_legend_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9393);
	local questID=9393;
end

function mq34_9393_the_beginning_of_legend_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9393_the_beginning_of_legend_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9393_the_beginning_of_legend_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9393, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9393, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9393, 1);
				npc_talk_index = "n1234_white_dragon-1";
end

</GameServer>