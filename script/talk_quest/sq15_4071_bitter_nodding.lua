<VillageServer>

function sq15_4071_bitter_nodding_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 703 then
		sq15_4071_bitter_nodding_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 865 then
		sq15_4071_bitter_nodding_OnTalk_n865_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n703_book_doctor--------------------------------------------------------------------------------
function sq15_4071_bitter_nodding_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n703_book_doctor-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40710, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40710, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40710, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40710, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40710, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40710, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40710, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40710, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40710, false);
			 end 

	end
	if npc_talk_index == "n703_book_doctor-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4071, 1);
				api_quest_SetJournalStep(userObjID,4071, 1);
				api_quest_SetQuestStep(userObjID,4071, 1);
				npc_talk_index = "n703_book_doctor-1";

	end
	if npc_talk_index == "n703_book_doctor-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40710, true);
				 api_quest_RewardQuestUser(userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40710, true);
				 api_quest_RewardQuestUser(userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40710, true);
				 api_quest_RewardQuestUser(userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40710, true);
				 api_quest_RewardQuestUser(userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40710, true);
				 api_quest_RewardQuestUser(userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40710, true);
				 api_quest_RewardQuestUser(userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40710, true);
				 api_quest_RewardQuestUser(userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40710, true);
				 api_quest_RewardQuestUser(userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40710, true);
				 api_quest_RewardQuestUser(userObjID, 40710, questID, 1);
			 end 
	end
	if npc_talk_index == "n703_book_doctor-2-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4072, 1);
					api_quest_SetQuestStep(userObjID, 4072, 1);
					api_quest_SetJournalStep(userObjID, 4072, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300548, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300548, api_quest_HasQuestItem(userObjID, 300548, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n703_book_doctor-1", "sq15_4072_new_power.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n865_rambert--------------------------------------------------------------------------------
function sq15_4071_bitter_nodding_OnTalk_n865_rambert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n865_rambert-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n865_rambert-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n865_rambert-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n865_rambert-1-b" then 
	end
	if npc_talk_index == "n865_rambert-1-c" then 
	end
	if npc_talk_index == "n865_rambert-1-d" then 
	end
	if npc_talk_index == "n865_rambert-1-e" then 
	end
	if npc_talk_index == "n865_rambert-1-f" then 
	end
	if npc_talk_index == "n865_rambert-1-g" then 
	end
	if npc_talk_index == "n865_rambert-1-h" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300548, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300548, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4071_bitter_nodding_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4071);
end

function sq15_4071_bitter_nodding_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4071);
end

function sq15_4071_bitter_nodding_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4071);
	local questID=4071;
end

function sq15_4071_bitter_nodding_OnRemoteStart( userObjID, questID )
end

function sq15_4071_bitter_nodding_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4071_bitter_nodding_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4071, 1);
				api_quest_SetJournalStep(userObjID,4071, 1);
				api_quest_SetQuestStep(userObjID,4071, 1);
				npc_talk_index = "n703_book_doctor-1";
end

</VillageServer>

<GameServer>
function sq15_4071_bitter_nodding_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 703 then
		sq15_4071_bitter_nodding_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 865 then
		sq15_4071_bitter_nodding_OnTalk_n865_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n703_book_doctor--------------------------------------------------------------------------------
function sq15_4071_bitter_nodding_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n703_book_doctor-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, false);
			 end 

	end
	if npc_talk_index == "n703_book_doctor-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4071, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4071, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4071, 1);
				npc_talk_index = "n703_book_doctor-1";

	end
	if npc_talk_index == "n703_book_doctor-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40710, questID, 1);
			 end 
	end
	if npc_talk_index == "n703_book_doctor-2-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4072, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4072, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4072, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300548, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300548, api_quest_HasQuestItem( pRoom, userObjID, 300548, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n703_book_doctor-1", "sq15_4072_new_power.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n865_rambert--------------------------------------------------------------------------------
function sq15_4071_bitter_nodding_OnTalk_n865_rambert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n865_rambert-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n865_rambert-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n865_rambert-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n865_rambert-1-b" then 
	end
	if npc_talk_index == "n865_rambert-1-c" then 
	end
	if npc_talk_index == "n865_rambert-1-d" then 
	end
	if npc_talk_index == "n865_rambert-1-e" then 
	end
	if npc_talk_index == "n865_rambert-1-f" then 
	end
	if npc_talk_index == "n865_rambert-1-g" then 
	end
	if npc_talk_index == "n865_rambert-1-h" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300548, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300548, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4071_bitter_nodding_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4071);
end

function sq15_4071_bitter_nodding_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4071);
end

function sq15_4071_bitter_nodding_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4071);
	local questID=4071;
end

function sq15_4071_bitter_nodding_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4071_bitter_nodding_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4071_bitter_nodding_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4071, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4071, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4071, 1);
				npc_talk_index = "n703_book_doctor-1";
end

</GameServer>