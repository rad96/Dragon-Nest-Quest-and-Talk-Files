<VillageServer>

function mqc15_9434_missing_result_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1565 then
		mqc15_9434_missing_result_OnTalk_n1565_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1566 then
		mqc15_9434_missing_result_OnTalk_n1566_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1567 then
		mqc15_9434_missing_result_OnTalk_n1567_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1565_meriendel--------------------------------------------------------------------------------
function mqc15_9434_missing_result_OnTalk_n1565_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1565_meriendel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1565_meriendel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1565_meriendel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9434, 2);
				api_quest_SetJournalStep(userObjID,9434, 1);
				api_quest_SetQuestStep(userObjID,9434, 1);
				npc_talk_index = "n1565_meriendel-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1566_pether--------------------------------------------------------------------------------
function mqc15_9434_missing_result_OnTalk_n1566_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1566_pether-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1567_lunaria--------------------------------------------------------------------------------
function mqc15_9434_missing_result_OnTalk_n1567_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1567_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1567_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1567_lunaria-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1567_lunaria-4";
				if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1567_lunaria-4-l";

				else
									npc_talk_index = "n1567_lunaria-4-b";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1567_lunaria-1-b" then 
	end
	if npc_talk_index == "n1567_lunaria-1-c" then 
	end
	if npc_talk_index == "n1567_lunaria-1-d" then 
	end
	if npc_talk_index == "n1567_lunaria-1-e" then 
	end
	if npc_talk_index == "n1567_lunaria-1-f" then 
	end
	if npc_talk_index == "n1567_lunaria-1-g" then 
	end
	if npc_talk_index == "n1567_lunaria-1-h" then 
	end
	if npc_talk_index == "n1567_lunaria-1-i" then 
	end
	if npc_talk_index == "n1567_lunaria-1-j" then 
	end
	if npc_talk_index == "n1567_lunaria-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n1567_lunaria-3-b" then 
	end
	if npc_talk_index == "n1567_lunaria-3-c" then 
	end
	if npc_talk_index == "n1567_lunaria-3-d" then 
	end
	if npc_talk_index == "n1567_lunaria-3-class_check_0" then 
				if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1567_lunaria-3-f";

				else
									npc_talk_index = "n1567_lunaria-3-e";

				end
	end
	if npc_talk_index == "n1567_lunaria-3-g" then 
	end
	if npc_talk_index == "n1567_lunaria-3-i" then 
	end
	if npc_talk_index == "n1567_lunaria-3-h" then 
	end
	if npc_talk_index == "n1567_lunaria-3-j" then 
	end
	if npc_talk_index == "n1567_lunaria-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1567_lunaria-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1567_lunaria-4-b" then 
	end
	if npc_talk_index == "n1567_lunaria-4-c" then 
	end
	if npc_talk_index == "n1567_lunaria-4-d" then 
	end
	if npc_talk_index == "n1567_lunaria-4-e" then 
	end
	if npc_talk_index == "n1567_lunaria-4-f" then 
	end
	if npc_talk_index == "n1567_lunaria-4-g" then 
	end
	if npc_talk_index == "n1567_lunaria-4-h" then 
	end
	if npc_talk_index == "n1567_lunaria-4-i" then 
	end
	if npc_talk_index == "n1567_lunaria-4-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 end 
	end
	if npc_talk_index == "n1567_lunaria-4-class_check_2" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1567_lunaria-4-x";

				else
									npc_talk_index = "n1567_lunaria-4-k";

				end
	end
	if npc_talk_index == "n1567_lunaria-4-m" then 
	end
	if npc_talk_index == "n1567_lunaria-4-n" then 
	end
	if npc_talk_index == "n1567_lunaria-4-o" then 
	end
	if npc_talk_index == "n1567_lunaria-4-p" then 
	end
	if npc_talk_index == "n1567_lunaria-4-q" then 
	end
	if npc_talk_index == "n1567_lunaria-4-r" then 
	end
	if npc_talk_index == "n1567_lunaria-4-s" then 
	end
	if npc_talk_index == "n1567_lunaria-4-t" then 
	end
	if npc_talk_index == "n1567_lunaria-4-u" then 
	end
	if npc_talk_index == "n1567_lunaria-4-v" then 
	end
	if npc_talk_index == "n1567_lunaria-4-w" then 
	end
	if npc_talk_index == "n1567_lunaria-4-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94340, true);
				 api_quest_RewardQuestUser(userObjID, 94340, questID, 1);
			 end 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9434_missing_result_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9434);
end

function mqc15_9434_missing_result_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9434);
end

function mqc15_9434_missing_result_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9434);
	local questID=9434;
end

function mqc15_9434_missing_result_OnRemoteStart( userObjID, questID )
end

function mqc15_9434_missing_result_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9434_missing_result_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9434, 2);
				api_quest_SetJournalStep(userObjID,9434, 1);
				api_quest_SetQuestStep(userObjID,9434, 1);
				npc_talk_index = "n1565_meriendel-1";
end

</VillageServer>

<GameServer>
function mqc15_9434_missing_result_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1565 then
		mqc15_9434_missing_result_OnTalk_n1565_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1566 then
		mqc15_9434_missing_result_OnTalk_n1566_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1567 then
		mqc15_9434_missing_result_OnTalk_n1567_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1565_meriendel--------------------------------------------------------------------------------
function mqc15_9434_missing_result_OnTalk_n1565_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1565_meriendel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1565_meriendel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1565_meriendel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9434, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9434, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9434, 1);
				npc_talk_index = "n1565_meriendel-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1566_pether--------------------------------------------------------------------------------
function mqc15_9434_missing_result_OnTalk_n1566_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1566_pether-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1567_lunaria--------------------------------------------------------------------------------
function mqc15_9434_missing_result_OnTalk_n1567_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1567_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1567_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1567_lunaria-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1567_lunaria-4";
				if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1567_lunaria-4-l";

				else
									npc_talk_index = "n1567_lunaria-4-b";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1567_lunaria-1-b" then 
	end
	if npc_talk_index == "n1567_lunaria-1-c" then 
	end
	if npc_talk_index == "n1567_lunaria-1-d" then 
	end
	if npc_talk_index == "n1567_lunaria-1-e" then 
	end
	if npc_talk_index == "n1567_lunaria-1-f" then 
	end
	if npc_talk_index == "n1567_lunaria-1-g" then 
	end
	if npc_talk_index == "n1567_lunaria-1-h" then 
	end
	if npc_talk_index == "n1567_lunaria-1-i" then 
	end
	if npc_talk_index == "n1567_lunaria-1-j" then 
	end
	if npc_talk_index == "n1567_lunaria-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n1567_lunaria-3-b" then 
	end
	if npc_talk_index == "n1567_lunaria-3-c" then 
	end
	if npc_talk_index == "n1567_lunaria-3-d" then 
	end
	if npc_talk_index == "n1567_lunaria-3-class_check_0" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1567_lunaria-3-f";

				else
									npc_talk_index = "n1567_lunaria-3-e";

				end
	end
	if npc_talk_index == "n1567_lunaria-3-g" then 
	end
	if npc_talk_index == "n1567_lunaria-3-i" then 
	end
	if npc_talk_index == "n1567_lunaria-3-h" then 
	end
	if npc_talk_index == "n1567_lunaria-3-j" then 
	end
	if npc_talk_index == "n1567_lunaria-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1567_lunaria-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1567_lunaria-4-b" then 
	end
	if npc_talk_index == "n1567_lunaria-4-c" then 
	end
	if npc_talk_index == "n1567_lunaria-4-d" then 
	end
	if npc_talk_index == "n1567_lunaria-4-e" then 
	end
	if npc_talk_index == "n1567_lunaria-4-f" then 
	end
	if npc_talk_index == "n1567_lunaria-4-g" then 
	end
	if npc_talk_index == "n1567_lunaria-4-h" then 
	end
	if npc_talk_index == "n1567_lunaria-4-i" then 
	end
	if npc_talk_index == "n1567_lunaria-4-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 end 
	end
	if npc_talk_index == "n1567_lunaria-4-class_check_2" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1567_lunaria-4-x";

				else
									npc_talk_index = "n1567_lunaria-4-k";

				end
	end
	if npc_talk_index == "n1567_lunaria-4-m" then 
	end
	if npc_talk_index == "n1567_lunaria-4-n" then 
	end
	if npc_talk_index == "n1567_lunaria-4-o" then 
	end
	if npc_talk_index == "n1567_lunaria-4-p" then 
	end
	if npc_talk_index == "n1567_lunaria-4-q" then 
	end
	if npc_talk_index == "n1567_lunaria-4-r" then 
	end
	if npc_talk_index == "n1567_lunaria-4-s" then 
	end
	if npc_talk_index == "n1567_lunaria-4-t" then 
	end
	if npc_talk_index == "n1567_lunaria-4-u" then 
	end
	if npc_talk_index == "n1567_lunaria-4-v" then 
	end
	if npc_talk_index == "n1567_lunaria-4-w" then 
	end
	if npc_talk_index == "n1567_lunaria-4-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94340, questID, 1);
			 end 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9434_missing_result_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9434);
end

function mqc15_9434_missing_result_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9434);
end

function mqc15_9434_missing_result_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9434);
	local questID=9434;
end

function mqc15_9434_missing_result_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9434_missing_result_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9434_missing_result_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9434, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9434, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9434, 1);
				npc_talk_index = "n1565_meriendel-1";
end

</GameServer>