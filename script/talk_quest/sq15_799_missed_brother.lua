<VillageServer>

function sq15_799_missed_brother_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 709 then
		sq15_799_missed_brother_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 769 then
		sq15_799_missed_brother_OnTalk_n769_lucita_bro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n709_trader_lucita--------------------------------------------------------------------------------
function sq15_799_missed_brother_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n709_trader_lucita-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n709_trader_lucita-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n709_trader_lucita-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n709_trader_lucita-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7990, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7990, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7990, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7990, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7990, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7990, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7990, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7990, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7990, false);
			 end 

	end
	if npc_talk_index == "n709_trader_lucita-accepting-acceptted" then
				api_quest_AddQuest(userObjID,799, 1);
				api_quest_SetJournalStep(userObjID,799, 1);
				api_quest_SetQuestStep(userObjID,799, 1);
				npc_talk_index = "n709_trader_lucita-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300411, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300411, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n709_trader_lucita-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7990, true);
				 api_quest_RewardQuestUser(userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7990, true);
				 api_quest_RewardQuestUser(userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7990, true);
				 api_quest_RewardQuestUser(userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7990, true);
				 api_quest_RewardQuestUser(userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7990, true);
				 api_quest_RewardQuestUser(userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7990, true);
				 api_quest_RewardQuestUser(userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7990, true);
				 api_quest_RewardQuestUser(userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7990, true);
				 api_quest_RewardQuestUser(userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7990, true);
				 api_quest_RewardQuestUser(userObjID, 7990, questID, 1);
			 end 
	end
	if npc_talk_index == "n709_trader_lucita-2-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n769_lucita_bro--------------------------------------------------------------------------------
function sq15_799_missed_brother_OnTalk_n769_lucita_bro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n769_lucita_bro-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n769_lucita_bro-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n769_lucita_bro-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n769_lucita_bro-1-b" then 

				if api_quest_HasQuestItem(userObjID, 300411, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300411, api_quest_HasQuestItem(userObjID, 300411, 1));
				end
	end
	if npc_talk_index == "n769_lucita_bro-1-c" then 
	end
	if npc_talk_index == "n769_lucita_bro-1-d" then 
	end
	if npc_talk_index == "n769_lucita_bro-1-e" then 
	end
	if npc_talk_index == "n769_lucita_bro-1-f" then 
	end
	if npc_talk_index == "n769_lucita_bro-1-f" then 
	end
	if npc_talk_index == "n769_lucita_bro-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_799_missed_brother_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 799);
end

function sq15_799_missed_brother_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 799);
end

function sq15_799_missed_brother_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 799);
	local questID=799;
end

function sq15_799_missed_brother_OnRemoteStart( userObjID, questID )
end

function sq15_799_missed_brother_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_799_missed_brother_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,799, 1);
				api_quest_SetJournalStep(userObjID,799, 1);
				api_quest_SetQuestStep(userObjID,799, 1);
				npc_talk_index = "n709_trader_lucita-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300411, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300411, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
end

</VillageServer>

<GameServer>
function sq15_799_missed_brother_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 709 then
		sq15_799_missed_brother_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 769 then
		sq15_799_missed_brother_OnTalk_n769_lucita_bro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n709_trader_lucita--------------------------------------------------------------------------------
function sq15_799_missed_brother_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n709_trader_lucita-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n709_trader_lucita-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n709_trader_lucita-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n709_trader_lucita-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, false);
			 end 

	end
	if npc_talk_index == "n709_trader_lucita-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,799, 1);
				api_quest_SetJournalStep( pRoom, userObjID,799, 1);
				api_quest_SetQuestStep( pRoom, userObjID,799, 1);
				npc_talk_index = "n709_trader_lucita-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300411, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300411, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n709_trader_lucita-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7990, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7990, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7990, questID, 1);
			 end 
	end
	if npc_talk_index == "n709_trader_lucita-2-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n769_lucita_bro--------------------------------------------------------------------------------
function sq15_799_missed_brother_OnTalk_n769_lucita_bro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n769_lucita_bro-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n769_lucita_bro-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n769_lucita_bro-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n769_lucita_bro-1-b" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300411, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300411, api_quest_HasQuestItem( pRoom, userObjID, 300411, 1));
				end
	end
	if npc_talk_index == "n769_lucita_bro-1-c" then 
	end
	if npc_talk_index == "n769_lucita_bro-1-d" then 
	end
	if npc_talk_index == "n769_lucita_bro-1-e" then 
	end
	if npc_talk_index == "n769_lucita_bro-1-f" then 
	end
	if npc_talk_index == "n769_lucita_bro-1-f" then 
	end
	if npc_talk_index == "n769_lucita_bro-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_799_missed_brother_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 799);
end

function sq15_799_missed_brother_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 799);
end

function sq15_799_missed_brother_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 799);
	local questID=799;
end

function sq15_799_missed_brother_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_799_missed_brother_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_799_missed_brother_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,799, 1);
				api_quest_SetJournalStep( pRoom, userObjID,799, 1);
				api_quest_SetQuestStep( pRoom, userObjID,799, 1);
				npc_talk_index = "n709_trader_lucita-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300411, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300411, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
end

</GameServer>