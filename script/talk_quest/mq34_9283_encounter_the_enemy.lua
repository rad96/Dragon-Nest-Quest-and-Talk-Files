<VillageServer>

function mq34_9283_encounter_the_enemy_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9283_encounter_the_enemy_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9283_encounter_the_enemy_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9283, 2);
				api_quest_SetJournalStep(userObjID,9283, 1);
				api_quest_SetQuestStep(userObjID,9283, 1);
				npc_talk_index = "n1047_argenta-accepting-";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 

				if api_quest_HasQuestItem(userObjID, 400381, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400381, api_quest_HasQuestItem(userObjID, 400381, 1));
				end
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
	end
	if npc_talk_index == "n1047_argenta-1-f" then 
	end
	if npc_talk_index == "n1047_argenta-1-g" then 
	end
	if npc_talk_index == "n1047_argenta-1-h" then 
	end
	if npc_talk_index == "n1047_argenta-1-i" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-3-class_check" then 
				if api_user_GetUserClassID(userObjID) == 9 then
									npc_talk_index = "n1047_argenta-3-h";

				else
									npc_talk_index = "n1047_argenta-3-a";

				end
	end
	if npc_talk_index == "n1047_argenta-3-b" then 
	end
	if npc_talk_index == "n1047_argenta-3-d" then 
	end
	if npc_talk_index == "n1047_argenta-3-d" then 
	end
	if npc_talk_index == "n1047_argenta-3-e" then 
	end
	if npc_talk_index == "n1047_argenta-3-f" then 
	end
	if npc_talk_index == "n1047_argenta-3-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92831, true);
				 api_quest_RewardQuestUser(userObjID, 92831, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92831, true);
				 api_quest_RewardQuestUser(userObjID, 92831, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92833, true);
				 api_quest_RewardQuestUser(userObjID, 92833, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92834, true);
				 api_quest_RewardQuestUser(userObjID, 92834, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92835, true);
				 api_quest_RewardQuestUser(userObjID, 92835, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92836, true);
				 api_quest_RewardQuestUser(userObjID, 92836, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92837, true);
				 api_quest_RewardQuestUser(userObjID, 92837, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92838, true);
				 api_quest_RewardQuestUser(userObjID, 92838, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92839, true);
				 api_quest_RewardQuestUser(userObjID, 92839, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9284, 2);
					api_quest_SetQuestStep(userObjID, 9284, 1);
					api_quest_SetJournalStep(userObjID, 9284, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1047_argenta-1", "mq34_9284_suffered_new_moon_forest.xml");
			return;
		end
	end
	if npc_talk_index == "n1047_argenta-3-i" then 
	end
	if npc_talk_index == "n1047_argenta-3-j" then 
	end
	if npc_talk_index == "n1047_argenta-3-k" then 
	end
	if npc_talk_index == "n1047_argenta-3-l" then 
	end
	if npc_talk_index == "n1047_argenta-3-m" then 
	end
	if npc_talk_index == "n1047_argenta-3-n" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92831, true);
				 api_quest_RewardQuestUser(userObjID, 92831, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92831, true);
				 api_quest_RewardQuestUser(userObjID, 92831, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92833, true);
				 api_quest_RewardQuestUser(userObjID, 92833, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92834, true);
				 api_quest_RewardQuestUser(userObjID, 92834, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92835, true);
				 api_quest_RewardQuestUser(userObjID, 92835, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92836, true);
				 api_quest_RewardQuestUser(userObjID, 92836, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92837, true);
				 api_quest_RewardQuestUser(userObjID, 92837, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92838, true);
				 api_quest_RewardQuestUser(userObjID, 92838, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92839, true);
				 api_quest_RewardQuestUser(userObjID, 92839, questID, 1);
			 end 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9283_encounter_the_enemy_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9283);
end

function mq34_9283_encounter_the_enemy_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9283);
end

function mq34_9283_encounter_the_enemy_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9283);
	local questID=9283;
end

function mq34_9283_encounter_the_enemy_OnRemoteStart( userObjID, questID )
end

function mq34_9283_encounter_the_enemy_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9283_encounter_the_enemy_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9283, 2);
				api_quest_SetJournalStep(userObjID,9283, 1);
				api_quest_SetQuestStep(userObjID,9283, 1);
				npc_talk_index = "n1047_argenta-accepting-";
end

</VillageServer>

<GameServer>
function mq34_9283_encounter_the_enemy_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9283_encounter_the_enemy_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9283_encounter_the_enemy_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1047_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1047_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1047_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9283, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9283, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9283, 1);
				npc_talk_index = "n1047_argenta-accepting-";

	end
	if npc_talk_index == "n1047_argenta-1-b" then 
	end
	if npc_talk_index == "n1047_argenta-1-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400381, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400381, api_quest_HasQuestItem( pRoom, userObjID, 400381, 1));
				end
	end
	if npc_talk_index == "n1047_argenta-1-d" then 
	end
	if npc_talk_index == "n1047_argenta-1-e" then 
	end
	if npc_talk_index == "n1047_argenta-1-f" then 
	end
	if npc_talk_index == "n1047_argenta-1-g" then 
	end
	if npc_talk_index == "n1047_argenta-1-h" then 
	end
	if npc_talk_index == "n1047_argenta-1-i" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1047_argenta-3-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 9 then
									npc_talk_index = "n1047_argenta-3-h";

				else
									npc_talk_index = "n1047_argenta-3-a";

				end
	end
	if npc_talk_index == "n1047_argenta-3-b" then 
	end
	if npc_talk_index == "n1047_argenta-3-d" then 
	end
	if npc_talk_index == "n1047_argenta-3-d" then 
	end
	if npc_talk_index == "n1047_argenta-3-e" then 
	end
	if npc_talk_index == "n1047_argenta-3-f" then 
	end
	if npc_talk_index == "n1047_argenta-3-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92831, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92831, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92831, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92831, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92833, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92833, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92834, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92834, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92835, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92835, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92836, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92836, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92837, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92837, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92838, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92838, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92839, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92839, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9284, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9284, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9284, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1047_argenta-1", "mq34_9284_suffered_new_moon_forest.xml");
			return;
		end
	end
	if npc_talk_index == "n1047_argenta-3-i" then 
	end
	if npc_talk_index == "n1047_argenta-3-j" then 
	end
	if npc_talk_index == "n1047_argenta-3-k" then 
	end
	if npc_talk_index == "n1047_argenta-3-l" then 
	end
	if npc_talk_index == "n1047_argenta-3-m" then 
	end
	if npc_talk_index == "n1047_argenta-3-n" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92831, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92831, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92831, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92831, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92833, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92833, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92834, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92834, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92835, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92835, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92836, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92836, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92837, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92837, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92838, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92838, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92839, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92839, questID, 1);
			 end 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9283_encounter_the_enemy_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9283);
end

function mq34_9283_encounter_the_enemy_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9283);
end

function mq34_9283_encounter_the_enemy_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9283);
	local questID=9283;
end

function mq34_9283_encounter_the_enemy_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9283_encounter_the_enemy_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9283_encounter_the_enemy_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9283, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9283, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9283, 1);
				npc_talk_index = "n1047_argenta-accepting-";
end

</GameServer>