<VillageServer>

function sq11_514_birth_of_abyss2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 88 then
		sq11_514_birth_of_abyss2_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_514_birth_of_abyss2_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,514, 1);
				api_quest_SetJournalStep(userObjID,514, 1);
				api_quest_SetQuestStep(userObjID,514, 1);
				npc_talk_index = "n088_scholar_starshy-1";

	end
	if npc_talk_index == "n088_scholar_starshy-1-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5141, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5142, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5143, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5144, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5145, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5146, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5147, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5147, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5147, false);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-1-g" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300221, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300222, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300223, 1);
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 300224, 1);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300225, 1);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200913, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 201131, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 200973, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 8, 2, 200951, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 9, 2, 200987, 30000);
	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5141, true);
				 api_quest_RewardQuestUser(userObjID, 5141, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5142, true);
				 api_quest_RewardQuestUser(userObjID, 5142, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5143, true);
				 api_quest_RewardQuestUser(userObjID, 5143, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5144, true);
				 api_quest_RewardQuestUser(userObjID, 5144, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5145, true);
				 api_quest_RewardQuestUser(userObjID, 5145, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5146, true);
				 api_quest_RewardQuestUser(userObjID, 5146, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5147, true);
				 api_quest_RewardQuestUser(userObjID, 5147, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5147, true);
				 api_quest_RewardQuestUser(userObjID, 5147, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5147, true);
				 api_quest_RewardQuestUser(userObjID, 5147, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 

				if api_quest_HasQuestItem(userObjID, 300221, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300221, api_quest_HasQuestItem(userObjID, 300221, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300222, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300222, api_quest_HasQuestItem(userObjID, 300222, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300223, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300223, api_quest_HasQuestItem(userObjID, 300223, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300224, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300224, api_quest_HasQuestItem(userObjID, 300224, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300225, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300225, api_quest_HasQuestItem(userObjID, 300225, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_514_birth_of_abyss2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 514);
	if qstep == 2 and CountIndex == 200913 then
				if api_quest_HasQuestItem(userObjID, 300221, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300221, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300221, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201131 then
				if api_quest_HasQuestItem(userObjID, 300222, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300222, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300222, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200973 then
				if api_quest_HasQuestItem(userObjID, 300223, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300223, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300223, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200951 then
				if api_quest_HasQuestItem(userObjID, 300224, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300224, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300224, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200987 then
				if api_quest_HasQuestItem(userObjID, 300225, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300225, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300225, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 300221 then

	end
	if qstep == 2 and CountIndex == 300222 then

	end
	if qstep == 2 and CountIndex == 300223 then

	end
	if qstep == 2 and CountIndex == 300224 then

	end
	if qstep == 2 and CountIndex == 300225 then

	end
end

function sq11_514_birth_of_abyss2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 514);
	if qstep == 2 and CountIndex == 200913 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201131 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200973 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200951 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200987 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300221 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300222 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300223 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300224 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300225 and Count >= TargetCount  then

	end
end

function sq11_514_birth_of_abyss2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 514);
	local questID=514;
end

function sq11_514_birth_of_abyss2_OnRemoteStart( userObjID, questID )
end

function sq11_514_birth_of_abyss2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_514_birth_of_abyss2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,514, 1);
				api_quest_SetJournalStep(userObjID,514, 1);
				api_quest_SetQuestStep(userObjID,514, 1);
				npc_talk_index = "n088_scholar_starshy-1";
end

</VillageServer>

<GameServer>
function sq11_514_birth_of_abyss2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 88 then
		sq11_514_birth_of_abyss2_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_514_birth_of_abyss2_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,514, 1);
				api_quest_SetJournalStep( pRoom, userObjID,514, 1);
				api_quest_SetQuestStep( pRoom, userObjID,514, 1);
				npc_talk_index = "n088_scholar_starshy-1";

	end
	if npc_talk_index == "n088_scholar_starshy-1-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5141, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5142, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5143, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5144, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5145, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5146, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5147, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5147, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5147, false);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-1-g" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300221, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300222, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300223, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 300224, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300225, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200913, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 201131, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 200973, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 2, 200951, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 9, 2, 200987, 30000);
	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5141, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5141, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5142, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5142, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5143, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5143, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5144, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5144, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5145, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5145, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5146, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5146, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5147, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5147, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5147, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5147, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5147, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5147, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300221, api_quest_HasQuestItem( pRoom, userObjID, 300221, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300222, api_quest_HasQuestItem( pRoom, userObjID, 300222, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300223, api_quest_HasQuestItem( pRoom, userObjID, 300223, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300224, api_quest_HasQuestItem( pRoom, userObjID, 300224, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300225, api_quest_HasQuestItem( pRoom, userObjID, 300225, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_514_birth_of_abyss2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 514);
	if qstep == 2 and CountIndex == 200913 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300221, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300221, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201131 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300222, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300222, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200973 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300223, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300223, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200951 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300224, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300224, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200987 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300225, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300225, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300221, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300222, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300223, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300224, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300225, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 300221 then

	end
	if qstep == 2 and CountIndex == 300222 then

	end
	if qstep == 2 and CountIndex == 300223 then

	end
	if qstep == 2 and CountIndex == 300224 then

	end
	if qstep == 2 and CountIndex == 300225 then

	end
end

function sq11_514_birth_of_abyss2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 514);
	if qstep == 2 and CountIndex == 200913 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201131 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200973 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200951 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200987 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300221 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300222 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300223 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300224 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300225 and Count >= TargetCount  then

	end
end

function sq11_514_birth_of_abyss2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 514);
	local questID=514;
end

function sq11_514_birth_of_abyss2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_514_birth_of_abyss2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_514_birth_of_abyss2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,514, 1);
				api_quest_SetJournalStep( pRoom, userObjID,514, 1);
				api_quest_SetQuestStep( pRoom, userObjID,514, 1);
				npc_talk_index = "n088_scholar_starshy-1";
end

</GameServer>