<VillageServer>

function mqc17_9725_child_of_darkness_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1835 then
		mqc17_9725_child_of_darkness_OnTalk_n1835_conscience_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1830 then
		mqc17_9725_child_of_darkness_OnTalk_n1830_tryan2_iris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1835_conscience_pope--------------------------------------------------------------------------------
function mqc17_9725_child_of_darkness_OnTalk_n1835_conscience_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1835_conscience_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1835_conscience_pope-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1835_conscience_pope-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1835_conscience_pope-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1835_conscience_pope-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9725, 2);
				api_quest_SetJournalStep(userObjID,9725, 1);
				api_quest_SetQuestStep(userObjID,9725, 1);
				npc_talk_index = "n1835_conscience_pope-1";

	end
	if npc_talk_index == "n1835_conscience_pope-1-b" then 
	end
	if npc_talk_index == "n1835_conscience_pope-1-c" then 
	end
	if npc_talk_index == "n1835_conscience_pope-1-d" then 
	end
	if npc_talk_index == "n1835_conscience_pope-1-e" then 
	end
	if npc_talk_index == "n1835_conscience_pope-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1835_conscience_pope-4-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97250, true);
				 api_quest_RewardQuestUser(userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97250, true);
				 api_quest_RewardQuestUser(userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97250, true);
				 api_quest_RewardQuestUser(userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97250, true);
				 api_quest_RewardQuestUser(userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97250, true);
				 api_quest_RewardQuestUser(userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97250, true);
				 api_quest_RewardQuestUser(userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97250, true);
				 api_quest_RewardQuestUser(userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97250, true);
				 api_quest_RewardQuestUser(userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97250, true);
				 api_quest_RewardQuestUser(userObjID, 97250, questID, 1);
			 end 
	end
	if npc_talk_index == "n1835_conscience_pope-4-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9726, 2);
					api_quest_SetQuestStep(userObjID, 9726, 1);
					api_quest_SetJournalStep(userObjID, 9726, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1835_conscience_pope-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1835_conscience_pope-1", "mqc17_9726_midnight_stars.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1830_tryan2_iris--------------------------------------------------------------------------------
function mqc17_9725_child_of_darkness_OnTalk_n1830_tryan2_iris(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1830_tryan2_iris-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1830_tryan2_iris-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1830_tryan2_iris-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1830_tryan2_iris-3-b" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-c" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-d" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-e" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-f" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-g" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-h" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-i" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-j" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-k" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-l" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-m" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-n" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-o" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-p" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-q" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9725_child_of_darkness_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9725);
end

function mqc17_9725_child_of_darkness_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9725);
end

function mqc17_9725_child_of_darkness_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9725);
	local questID=9725;
end

function mqc17_9725_child_of_darkness_OnRemoteStart( userObjID, questID )
end

function mqc17_9725_child_of_darkness_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc17_9725_child_of_darkness_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9725, 2);
				api_quest_SetJournalStep(userObjID,9725, 1);
				api_quest_SetQuestStep(userObjID,9725, 1);
				npc_talk_index = "n1835_conscience_pope-1";
end

</VillageServer>

<GameServer>
function mqc17_9725_child_of_darkness_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1835 then
		mqc17_9725_child_of_darkness_OnTalk_n1835_conscience_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1830 then
		mqc17_9725_child_of_darkness_OnTalk_n1830_tryan2_iris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1835_conscience_pope--------------------------------------------------------------------------------
function mqc17_9725_child_of_darkness_OnTalk_n1835_conscience_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1835_conscience_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1835_conscience_pope-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1835_conscience_pope-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1835_conscience_pope-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1835_conscience_pope-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9725, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9725, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9725, 1);
				npc_talk_index = "n1835_conscience_pope-1";

	end
	if npc_talk_index == "n1835_conscience_pope-1-b" then 
	end
	if npc_talk_index == "n1835_conscience_pope-1-c" then 
	end
	if npc_talk_index == "n1835_conscience_pope-1-d" then 
	end
	if npc_talk_index == "n1835_conscience_pope-1-e" then 
	end
	if npc_talk_index == "n1835_conscience_pope-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1835_conscience_pope-4-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97250, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97250, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97250, questID, 1);
			 end 
	end
	if npc_talk_index == "n1835_conscience_pope-4-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9726, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9726, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9726, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1835_conscience_pope-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1835_conscience_pope-1", "mqc17_9726_midnight_stars.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1830_tryan2_iris--------------------------------------------------------------------------------
function mqc17_9725_child_of_darkness_OnTalk_n1830_tryan2_iris( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1830_tryan2_iris-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1830_tryan2_iris-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1830_tryan2_iris-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1830_tryan2_iris-3-b" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-c" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-d" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-e" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-f" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-g" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-h" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-i" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-j" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-k" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-l" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-m" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-n" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-o" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-p" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-3-q" then 
	end
	if npc_talk_index == "n1830_tryan2_iris-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9725_child_of_darkness_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9725);
end

function mqc17_9725_child_of_darkness_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9725);
end

function mqc17_9725_child_of_darkness_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9725);
	local questID=9725;
end

function mqc17_9725_child_of_darkness_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc17_9725_child_of_darkness_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc17_9725_child_of_darkness_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9725, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9725, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9725, 1);
				npc_talk_index = "n1835_conscience_pope-1";
end

</GameServer>