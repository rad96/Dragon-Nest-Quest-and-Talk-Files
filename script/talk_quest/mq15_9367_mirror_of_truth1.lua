<VillageServer>

function mq15_9367_mirror_of_truth1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 703 then
		mq15_9367_mirror_of_truth1_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq15_9367_mirror_of_truth1_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n703_book_doctor--------------------------------------------------------------------------------
function mq15_9367_mirror_of_truth1_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n703_book_doctor-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-1-b" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-1-d" then 
	end
	if npc_talk_index == "n703_book_doctor-1-e" then 
	end
	if npc_talk_index == "n703_book_doctor-1-f" then 
	end
	if npc_talk_index == "n703_book_doctor-1-g" then 
	end
	if npc_talk_index == "n703_book_doctor-1-h" then 
	end
	if npc_talk_index == "n703_book_doctor-1-i" then 
	end
	if npc_talk_index == "n703_book_doctor-1-j" then 
	end
	if npc_talk_index == "n703_book_doctor-1-k" then 
	end
	if npc_talk_index == "n703_book_doctor-1-l" then 
	end
	if npc_talk_index == "n703_book_doctor-1-m" then 
	end
	if npc_talk_index == "n703_book_doctor-1-n" then 
	end
	if npc_talk_index == "n703_book_doctor-1-o" then 
	end
	if npc_talk_index == "n703_book_doctor-1-p" then 
	end
	if npc_talk_index == "n703_book_doctor-1-q" then 
	end
	if npc_talk_index == "n703_book_doctor-1-r" then 
	end
	if npc_talk_index == "n703_book_doctor-1-s" then 
	end
	if npc_talk_index == "n703_book_doctor-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400318, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 600214, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700214, 30000);
	end
	if npc_talk_index == "n703_book_doctor-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93670, true);
				 api_quest_RewardQuestUser(userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93670, true);
				 api_quest_RewardQuestUser(userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93670, true);
				 api_quest_RewardQuestUser(userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93670, true);
				 api_quest_RewardQuestUser(userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93670, true);
				 api_quest_RewardQuestUser(userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93670, true);
				 api_quest_RewardQuestUser(userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93670, true);
				 api_quest_RewardQuestUser(userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93670, true);
				 api_quest_RewardQuestUser(userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93670, true);
				 api_quest_RewardQuestUser(userObjID, 93670, questID, 1);
			 end 
	end
	if npc_talk_index == "n703_book_doctor-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9368, 2);
					api_quest_SetQuestStep(userObjID, 9368, 1);
					api_quest_SetJournalStep(userObjID, 9368, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n703_book_doctor-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n703_book_doctor-1", "mq15_9368_mirror_of_truth2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq15_9367_mirror_of_truth1_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9367, 2);
				api_quest_SetJournalStep(userObjID,9367, 1);
				api_quest_SetQuestStep(userObjID,9367, 1);
				npc_talk_index = "n822_teramai-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9367_mirror_of_truth1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9367);
	if qstep == 2 and CountIndex == 400318 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 600214 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400318, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400318, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 700214 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400318, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400318, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq15_9367_mirror_of_truth1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9367);
	if qstep == 2 and CountIndex == 400318 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 600214 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700214 and Count >= TargetCount  then

	end
end

function mq15_9367_mirror_of_truth1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9367);
	local questID=9367;
end

function mq15_9367_mirror_of_truth1_OnRemoteStart( userObjID, questID )
end

function mq15_9367_mirror_of_truth1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9367_mirror_of_truth1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9367, 2);
				api_quest_SetJournalStep(userObjID,9367, 1);
				api_quest_SetQuestStep(userObjID,9367, 1);
				npc_talk_index = "n822_teramai-1";
end

</VillageServer>

<GameServer>
function mq15_9367_mirror_of_truth1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 703 then
		mq15_9367_mirror_of_truth1_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq15_9367_mirror_of_truth1_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n703_book_doctor--------------------------------------------------------------------------------
function mq15_9367_mirror_of_truth1_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n703_book_doctor-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-1-b" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-1-d" then 
	end
	if npc_talk_index == "n703_book_doctor-1-e" then 
	end
	if npc_talk_index == "n703_book_doctor-1-f" then 
	end
	if npc_talk_index == "n703_book_doctor-1-g" then 
	end
	if npc_talk_index == "n703_book_doctor-1-h" then 
	end
	if npc_talk_index == "n703_book_doctor-1-i" then 
	end
	if npc_talk_index == "n703_book_doctor-1-j" then 
	end
	if npc_talk_index == "n703_book_doctor-1-k" then 
	end
	if npc_talk_index == "n703_book_doctor-1-l" then 
	end
	if npc_talk_index == "n703_book_doctor-1-m" then 
	end
	if npc_talk_index == "n703_book_doctor-1-n" then 
	end
	if npc_talk_index == "n703_book_doctor-1-o" then 
	end
	if npc_talk_index == "n703_book_doctor-1-p" then 
	end
	if npc_talk_index == "n703_book_doctor-1-q" then 
	end
	if npc_talk_index == "n703_book_doctor-1-r" then 
	end
	if npc_talk_index == "n703_book_doctor-1-s" then 
	end
	if npc_talk_index == "n703_book_doctor-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400318, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 600214, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700214, 30000);
	end
	if npc_talk_index == "n703_book_doctor-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93670, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93670, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93670, questID, 1);
			 end 
	end
	if npc_talk_index == "n703_book_doctor-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9368, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9368, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9368, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n703_book_doctor-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n703_book_doctor-1", "mq15_9368_mirror_of_truth2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq15_9367_mirror_of_truth1_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n822_teramai-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9367, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9367, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9367, 1);
				npc_talk_index = "n822_teramai-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9367_mirror_of_truth1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9367);
	if qstep == 2 and CountIndex == 400318 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 600214 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400318, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400318, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 700214 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400318, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400318, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq15_9367_mirror_of_truth1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9367);
	if qstep == 2 and CountIndex == 400318 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 600214 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700214 and Count >= TargetCount  then

	end
end

function mq15_9367_mirror_of_truth1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9367);
	local questID=9367;
end

function mq15_9367_mirror_of_truth1_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9367_mirror_of_truth1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9367_mirror_of_truth1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9367, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9367, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9367, 1);
				npc_talk_index = "n822_teramai-1";
end

</GameServer>