<VillageServer>

function sq15_4036_pay_attention_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 91 then
		sq15_4036_pay_attention_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n091_trader_bellin--------------------------------------------------------------------------------
function sq15_4036_pay_attention_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40360, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40360, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40360, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40360, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40360, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40360, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40360, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40360, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40360, false);
			 end 

	end
	if npc_talk_index == "n091_trader_bellin-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4036, 1);
				api_quest_SetJournalStep(userObjID,4036, 1);
				api_quest_SetQuestStep(userObjID,4036, 1);
				npc_talk_index = "n091_trader_bellin-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300531, 10);

	end
	if npc_talk_index == "n091_trader_bellin-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40360, true);
				 api_quest_RewardQuestUser(userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40360, true);
				 api_quest_RewardQuestUser(userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40360, true);
				 api_quest_RewardQuestUser(userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40360, true);
				 api_quest_RewardQuestUser(userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40360, true);
				 api_quest_RewardQuestUser(userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40360, true);
				 api_quest_RewardQuestUser(userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40360, true);
				 api_quest_RewardQuestUser(userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40360, true);
				 api_quest_RewardQuestUser(userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40360, true);
				 api_quest_RewardQuestUser(userObjID, 40360, questID, 1);
			 end 
	end
	if npc_talk_index == "n091_trader_bellin-2-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem(userObjID, 300531, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300531, api_quest_HasQuestItem(userObjID, 300531, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4037, 1);
					api_quest_SetQuestStep(userObjID, 4037, 1);
					api_quest_SetJournalStep(userObjID, 4037, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n091_trader_bellin-1", "sq15_4037_found_corp.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4036_pay_attention_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4036);
	if qstep == 1 and CountIndex == 300531 then

	end
end

function sq15_4036_pay_attention_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4036);
	if qstep == 1 and CountIndex == 300531 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function sq15_4036_pay_attention_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4036);
	local questID=4036;
end

function sq15_4036_pay_attention_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300531, 10);
end

function sq15_4036_pay_attention_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4036_pay_attention_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4036, 1);
				api_quest_SetJournalStep(userObjID,4036, 1);
				api_quest_SetQuestStep(userObjID,4036, 1);
				npc_talk_index = "n091_trader_bellin-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300531, 10);
end

</VillageServer>

<GameServer>
function sq15_4036_pay_attention_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 91 then
		sq15_4036_pay_attention_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n091_trader_bellin--------------------------------------------------------------------------------
function sq15_4036_pay_attention_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n091_trader_bellin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n091_trader_bellin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, false);
			 end 

	end
	if npc_talk_index == "n091_trader_bellin-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4036, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4036, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4036, 1);
				npc_talk_index = "n091_trader_bellin-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300531, 10);

	end
	if npc_talk_index == "n091_trader_bellin-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40360, questID, 1);
			 end 
	end
	if npc_talk_index == "n091_trader_bellin-2-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem( pRoom, userObjID, 300531, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300531, api_quest_HasQuestItem( pRoom, userObjID, 300531, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4037, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4037, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4037, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n091_trader_bellin-1", "sq15_4037_found_corp.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4036_pay_attention_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4036);
	if qstep == 1 and CountIndex == 300531 then

	end
end

function sq15_4036_pay_attention_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4036);
	if qstep == 1 and CountIndex == 300531 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function sq15_4036_pay_attention_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4036);
	local questID=4036;
end

function sq15_4036_pay_attention_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300531, 10);
end

function sq15_4036_pay_attention_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4036_pay_attention_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4036, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4036, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4036, 1);
				npc_talk_index = "n091_trader_bellin-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300531, 10);
end

</GameServer>