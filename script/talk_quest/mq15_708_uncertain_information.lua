<VillageServer>

function mq15_708_uncertain_information_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 474 then
		mq15_708_uncertain_information_OnTalk_n474_karahan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 480 then
		mq15_708_uncertain_information_OnTalk_n480_overhear(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_708_uncertain_information_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 720 then
		mq15_708_uncertain_information_OnTalk_n720_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n474_karahan--------------------------------------------------------------------------------
function mq15_708_uncertain_information_OnTalk_n474_karahan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n474_karahan-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n480_overhear--------------------------------------------------------------------------------
function mq15_708_uncertain_information_OnTalk_n480_overhear(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n480_overhear-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n480_overhear-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n480_overhear-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n480_overhear-2-b" then 
	end
	if npc_talk_index == "n480_overhear-2-c" then 
	end
	if npc_talk_index == "n480_overhear-2-d" then 
	end
	if npc_talk_index == "n480_overhear-2-e" then 
	end
	if npc_talk_index == "n480_overhear-2-f" then 
	end
	if npc_talk_index == "n480_overhear-2-g" then 
	end
	if npc_talk_index == "n480_overhear-2-h" then 
	end
	if npc_talk_index == "n480_overhear-2-i" then 
	end
	if npc_talk_index == "n480_overhear-2-j" then 
	end
	if npc_talk_index == "n480_overhear-2-k" then 
	end
	if npc_talk_index == "n480_overhear-2-l" then 
	end
	if npc_talk_index == "n480_overhear-2-m" then 
	end
	if npc_talk_index == "n480_overhear-2-n" then 
	end
	if npc_talk_index == "n480_overhear-2-o" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7081, true);
				 api_quest_RewardQuestUser(userObjID, 7081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7082, true);
				 api_quest_RewardQuestUser(userObjID, 7082, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7083, true);
				 api_quest_RewardQuestUser(userObjID, 7083, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7084, true);
				 api_quest_RewardQuestUser(userObjID, 7084, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7085, true);
				 api_quest_RewardQuestUser(userObjID, 7085, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7086, true);
				 api_quest_RewardQuestUser(userObjID, 7086, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7081, true);
				 api_quest_RewardQuestUser(userObjID, 7081, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7081, true);
				 api_quest_RewardQuestUser(userObjID, 7081, questID, 1);
			 end 
	end
	if npc_talk_index == "n480_overhear-2-p" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 709, 2);
					api_quest_SetQuestStep(userObjID, 709, 1);
					api_quest_SetJournalStep(userObjID, 709, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_708_uncertain_information_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,708, 2);
				api_quest_SetJournalStep(userObjID,708, 1);
				api_quest_SetQuestStep(userObjID,708, 1);
				npc_talk_index = "n707_sidel-1";

	end
	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-1-d" then 
	end
	if npc_talk_index == "n707_sidel-1-e" then 
	end
	if npc_talk_index == "n707_sidel-1-f" then 
	end
	if npc_talk_index == "n707_sidel-1-g" then 
	end
	if npc_talk_index == "n707_sidel-1-h" then 
	end
	if npc_talk_index == "n707_sidel-1-i" then 
	end
	if npc_talk_index == "n707_sidel-1-j" then 
	end
	if npc_talk_index == "n707_sidel-1-k" then 
	end
	if npc_talk_index == "n707_sidel-1-l" then 
	end
	if npc_talk_index == "n707_sidel-1-m" then 
	end
	if npc_talk_index == "n707_sidel-1-n" then 
	end
	if npc_talk_index == "n707_sidel-1-o" then 
	end
	if npc_talk_index == "n707_sidel-1-p" then 
	end
	if npc_talk_index == "n707_sidel-1-q" then 
	end
	if npc_talk_index == "n707_sidel-1-r" then 
	end
	if npc_talk_index == "n707_sidel-1-s" then 
	end
	if npc_talk_index == "n707_sidel-1-t" then 
	end
	if npc_talk_index == "n707_sidel-1-u" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n720_charty--------------------------------------------------------------------------------
function mq15_708_uncertain_information_OnTalk_n720_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n720_charty-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_708_uncertain_information_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 708);
end

function mq15_708_uncertain_information_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 708);
end

function mq15_708_uncertain_information_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 708);
	local questID=708;
end

function mq15_708_uncertain_information_OnRemoteStart( userObjID, questID )
end

function mq15_708_uncertain_information_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_708_uncertain_information_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,708, 2);
				api_quest_SetJournalStep(userObjID,708, 1);
				api_quest_SetQuestStep(userObjID,708, 1);
				npc_talk_index = "n707_sidel-1";
end

</VillageServer>

<GameServer>
function mq15_708_uncertain_information_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 474 then
		mq15_708_uncertain_information_OnTalk_n474_karahan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 480 then
		mq15_708_uncertain_information_OnTalk_n480_overhear( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_708_uncertain_information_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 720 then
		mq15_708_uncertain_information_OnTalk_n720_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n474_karahan--------------------------------------------------------------------------------
function mq15_708_uncertain_information_OnTalk_n474_karahan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n474_karahan-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n480_overhear--------------------------------------------------------------------------------
function mq15_708_uncertain_information_OnTalk_n480_overhear( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n480_overhear-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n480_overhear-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n480_overhear-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n480_overhear-2-b" then 
	end
	if npc_talk_index == "n480_overhear-2-c" then 
	end
	if npc_talk_index == "n480_overhear-2-d" then 
	end
	if npc_talk_index == "n480_overhear-2-e" then 
	end
	if npc_talk_index == "n480_overhear-2-f" then 
	end
	if npc_talk_index == "n480_overhear-2-g" then 
	end
	if npc_talk_index == "n480_overhear-2-h" then 
	end
	if npc_talk_index == "n480_overhear-2-i" then 
	end
	if npc_talk_index == "n480_overhear-2-j" then 
	end
	if npc_talk_index == "n480_overhear-2-k" then 
	end
	if npc_talk_index == "n480_overhear-2-l" then 
	end
	if npc_talk_index == "n480_overhear-2-m" then 
	end
	if npc_talk_index == "n480_overhear-2-n" then 
	end
	if npc_talk_index == "n480_overhear-2-o" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7082, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7082, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7083, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7083, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7084, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7084, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7085, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7085, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7086, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7086, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7081, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7081, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7081, questID, 1);
			 end 
	end
	if npc_talk_index == "n480_overhear-2-p" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 709, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 709, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 709, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_708_uncertain_information_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,708, 2);
				api_quest_SetJournalStep( pRoom, userObjID,708, 1);
				api_quest_SetQuestStep( pRoom, userObjID,708, 1);
				npc_talk_index = "n707_sidel-1";

	end
	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-1-d" then 
	end
	if npc_talk_index == "n707_sidel-1-e" then 
	end
	if npc_talk_index == "n707_sidel-1-f" then 
	end
	if npc_talk_index == "n707_sidel-1-g" then 
	end
	if npc_talk_index == "n707_sidel-1-h" then 
	end
	if npc_talk_index == "n707_sidel-1-i" then 
	end
	if npc_talk_index == "n707_sidel-1-j" then 
	end
	if npc_talk_index == "n707_sidel-1-k" then 
	end
	if npc_talk_index == "n707_sidel-1-l" then 
	end
	if npc_talk_index == "n707_sidel-1-m" then 
	end
	if npc_talk_index == "n707_sidel-1-n" then 
	end
	if npc_talk_index == "n707_sidel-1-o" then 
	end
	if npc_talk_index == "n707_sidel-1-p" then 
	end
	if npc_talk_index == "n707_sidel-1-q" then 
	end
	if npc_talk_index == "n707_sidel-1-r" then 
	end
	if npc_talk_index == "n707_sidel-1-s" then 
	end
	if npc_talk_index == "n707_sidel-1-t" then 
	end
	if npc_talk_index == "n707_sidel-1-u" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n720_charty--------------------------------------------------------------------------------
function mq15_708_uncertain_information_OnTalk_n720_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n720_charty-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_708_uncertain_information_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 708);
end

function mq15_708_uncertain_information_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 708);
end

function mq15_708_uncertain_information_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 708);
	local questID=708;
end

function mq15_708_uncertain_information_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_708_uncertain_information_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_708_uncertain_information_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,708, 2);
				api_quest_SetJournalStep( pRoom, userObjID,708, 1);
				api_quest_SetQuestStep( pRoom, userObjID,708, 1);
				npc_talk_index = "n707_sidel-1";
end

</GameServer>