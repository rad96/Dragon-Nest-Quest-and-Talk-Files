<VillageServer>

function sq08_243_change1_kalli_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 564 then
		sq08_243_change1_kalli_OnTalk_n564_kali_totem(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 28 then
		sq08_243_change1_kalli_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 570 then
		sq08_243_change1_kalli_OnTalk_n570_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 571 then
		sq08_243_change1_kalli_OnTalk_n571_other_kali(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1220 then
		sq08_243_change1_kalli_OnTalk_n1220_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n564_kali_totem--------------------------------------------------------------------------------
function sq08_243_change1_kalli_OnTalk_n564_kali_totem(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n564_kali_totem-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n564_kali_totem-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n564_kali_totem-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n564_kali_totem-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n564_kali_totem-2-member1" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n564_kali_totem-2-a";

				else
									npc_talk_index = "n564_kali_totem-2-b";

				end
	end
	if npc_talk_index == "n564_kali_totem-2-go_test_room" then 
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_user_ChangeMap(userObjID,13026,1);
	end
	if npc_talk_index == "n564_kali_totem-4-go_map4" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_user_ChangeMap(userObjID,13520,3);

				else
									npc_talk_index = "n564_kali_totem-4-a";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_243_change1_kalli_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n028_scholar_bailey-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2430, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2430, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2430, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2430, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2430, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2430, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2430, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2430, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2430, false);
			 end 

	end
	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,243, 1);
				api_quest_SetJournalStep(userObjID,243, 1);
				api_quest_SetQuestStep(userObjID,243, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-comp_check1" then 
				if api_user_GetUserJobID(userObjID) == 54  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									if api_user_GetUserJobID(userObjID) == 57  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									if api_user_GetUserJobID(userObjID) == 55  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									if api_user_GetUserJobID(userObjID) == 56  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									if api_user_GetUserJobID(userObjID) == 58  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									if api_user_GetUserJobID(userObjID) == 59  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									npc_talk_index = "n028_scholar_bailey-1-b";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-e" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-f" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-g" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-h" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-i" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-j" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-k" then 
	end
	if npc_talk_index == "n028_scholar_bailey-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n028_scholar_bailey-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 8);
	end
	if npc_talk_index == "n028_scholar_bailey-6-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-6-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-6-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-6-e" then 
				if api_user_GetUserJobID(userObjID) == 54  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2431, true);
				 api_quest_RewardQuestUser(userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2431, true);
				 api_quest_RewardQuestUser(userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2431, true);
				 api_quest_RewardQuestUser(userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2431, true);
				 api_quest_RewardQuestUser(userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2431, true);
				 api_quest_RewardQuestUser(userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2431, true);
				 api_quest_RewardQuestUser(userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2431, true);
				 api_quest_RewardQuestUser(userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2431, true);
				 api_quest_RewardQuestUser(userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2431, true);
				 api_quest_RewardQuestUser(userObjID, 2431, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2432, true);
				 api_quest_RewardQuestUser(userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2432, true);
				 api_quest_RewardQuestUser(userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2432, true);
				 api_quest_RewardQuestUser(userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2432, true);
				 api_quest_RewardQuestUser(userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2432, true);
				 api_quest_RewardQuestUser(userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2432, true);
				 api_quest_RewardQuestUser(userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2432, true);
				 api_quest_RewardQuestUser(userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2432, true);
				 api_quest_RewardQuestUser(userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2432, true);
				 api_quest_RewardQuestUser(userObjID, 2432, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n028_scholar_bailey-6-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 287, 1);
					api_quest_SetQuestStep(userObjID, 287, 1);
					api_quest_SetJournalStep(userObjID, 287, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n028_scholar_bailey-6-g" then 
	end
	if npc_talk_index == "n028_scholar_bailey-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n028_scholar_bailey-1", "sq08_287_change_job6.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n570_gaharam--------------------------------------------------------------------------------
function sq08_243_change1_kalli_OnTalk_n570_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n570_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n570_gaharam-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n570_gaharam-2-b" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n571_other_kali--------------------------------------------------------------------------------
function sq08_243_change1_kalli_OnTalk_n571_other_kali(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n571_other_kali-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n571_other_kali-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n571_other_kali-3-b" then 
	end
	if npc_talk_index == "n571_other_kali-3-c" then 
	end
	if npc_talk_index == "n571_other_kali-3-d" then 
	end
	if npc_talk_index == "n571_other_kali-3-e" then 
	end
	if npc_talk_index == "n571_other_kali-3-f" then 
	end
	if npc_talk_index == "n571_other_kali-3-g" then 
	end
	if npc_talk_index == "n571_other_kali-3-h" then 
	end
	if npc_talk_index == "n571_other_kali-3-i" then 
	end
	if npc_talk_index == "n571_other_kali-3-j" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);
				api_user_ChangeMap(userObjID,13520,3);
	end
	if npc_talk_index == "n571_other_kali-4-a" then 
				api_user_ChangeMap(userObjID,13520,3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1220_gaharam--------------------------------------------------------------------------------
function sq08_243_change1_kalli_OnTalk_n1220_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1220_gaharam-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1220_gaharam-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1220_gaharam-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1220_gaharam-4-b" then 
	end
	if npc_talk_index == "n1220_gaharam-4-c" then 
	end
	if npc_talk_index == "n1220_gaharam-4-d" then 
	end
	if npc_talk_index == "n1220_gaharam-4-e" then 
	end
	if npc_talk_index == "n1220_gaharam-4-f" then 
	end
	if npc_talk_index == "n1220_gaharam-4-g" then 
	end
	if npc_talk_index == "n1220_gaharam-4-open_ui4" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 6);
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1220_gaharam-5-open_ui5" then 
				api_ui_OpenJobChange(userObjID)
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_243_change1_kalli_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 243);
end

function sq08_243_change1_kalli_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 243);
end

function sq08_243_change1_kalli_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 243);
	local questID=243;
end

function sq08_243_change1_kalli_OnRemoteStart( userObjID, questID )
end

function sq08_243_change1_kalli_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_243_change1_kalli_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,243, 1);
				api_quest_SetJournalStep(userObjID,243, 1);
				api_quest_SetQuestStep(userObjID,243, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</VillageServer>

<GameServer>
function sq08_243_change1_kalli_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 564 then
		sq08_243_change1_kalli_OnTalk_n564_kali_totem( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 28 then
		sq08_243_change1_kalli_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 570 then
		sq08_243_change1_kalli_OnTalk_n570_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 571 then
		sq08_243_change1_kalli_OnTalk_n571_other_kali( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1220 then
		sq08_243_change1_kalli_OnTalk_n1220_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n564_kali_totem--------------------------------------------------------------------------------
function sq08_243_change1_kalli_OnTalk_n564_kali_totem( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n564_kali_totem-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n564_kali_totem-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n564_kali_totem-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n564_kali_totem-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n564_kali_totem-2-member1" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n564_kali_totem-2-a";

				else
									npc_talk_index = "n564_kali_totem-2-b";

				end
	end
	if npc_talk_index == "n564_kali_totem-2-go_test_room" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_user_ChangeMap( pRoom, userObjID,13026,1);
	end
	if npc_talk_index == "n564_kali_totem-4-go_map4" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_user_ChangeMap( pRoom, userObjID,13520,3);

				else
									npc_talk_index = "n564_kali_totem-4-a";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n028_scholar_bailey--------------------------------------------------------------------------------
function sq08_243_change1_kalli_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n028_scholar_bailey-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n028_scholar_bailey-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2430, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2430, false);
			 end 

	end
	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,243, 1);
				api_quest_SetJournalStep( pRoom, userObjID,243, 1);
				api_quest_SetQuestStep( pRoom, userObjID,243, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-comp_check1" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 54  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 57  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 55  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 56  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 58  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 59  then
									npc_talk_index = "n028_scholar_bailey-1-l";

				else
									npc_talk_index = "n028_scholar_bailey-1-b";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-e" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-f" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-g" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-h" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-i" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-j" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-k" then 
	end
	if npc_talk_index == "n028_scholar_bailey-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n028_scholar_bailey-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);
	end
	if npc_talk_index == "n028_scholar_bailey-6-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-6-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-6-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-6-e" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 54  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2431, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2431, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2431, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2431, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2431, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2431, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2431, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2431, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2431, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2431, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2431, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2432, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2432, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2432, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2432, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2432, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2432, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2432, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2432, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2432, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2432, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2432, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n028_scholar_bailey-6-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 287, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 287, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 287, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n028_scholar_bailey-6-g" then 
	end
	if npc_talk_index == "n028_scholar_bailey-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n028_scholar_bailey-1", "sq08_287_change_job6.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n570_gaharam--------------------------------------------------------------------------------
function sq08_243_change1_kalli_OnTalk_n570_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n570_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n570_gaharam-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n570_gaharam-2-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n571_other_kali--------------------------------------------------------------------------------
function sq08_243_change1_kalli_OnTalk_n571_other_kali( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n571_other_kali-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n571_other_kali-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n571_other_kali-3-b" then 
	end
	if npc_talk_index == "n571_other_kali-3-c" then 
	end
	if npc_talk_index == "n571_other_kali-3-d" then 
	end
	if npc_talk_index == "n571_other_kali-3-e" then 
	end
	if npc_talk_index == "n571_other_kali-3-f" then 
	end
	if npc_talk_index == "n571_other_kali-3-g" then 
	end
	if npc_talk_index == "n571_other_kali-3-h" then 
	end
	if npc_talk_index == "n571_other_kali-3-i" then 
	end
	if npc_talk_index == "n571_other_kali-3-j" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				api_user_ChangeMap( pRoom, userObjID,13520,3);
	end
	if npc_talk_index == "n571_other_kali-4-a" then 
				api_user_ChangeMap( pRoom, userObjID,13520,3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1220_gaharam--------------------------------------------------------------------------------
function sq08_243_change1_kalli_OnTalk_n1220_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1220_gaharam-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1220_gaharam-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1220_gaharam-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1220_gaharam-4-b" then 
	end
	if npc_talk_index == "n1220_gaharam-4-c" then 
	end
	if npc_talk_index == "n1220_gaharam-4-d" then 
	end
	if npc_talk_index == "n1220_gaharam-4-e" then 
	end
	if npc_talk_index == "n1220_gaharam-4-f" then 
	end
	if npc_talk_index == "n1220_gaharam-4-g" then 
	end
	if npc_talk_index == "n1220_gaharam-4-open_ui4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1220_gaharam-5-open_ui5" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_243_change1_kalli_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 243);
end

function sq08_243_change1_kalli_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 243);
end

function sq08_243_change1_kalli_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 243);
	local questID=243;
end

function sq08_243_change1_kalli_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_243_change1_kalli_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_243_change1_kalli_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,243, 1);
				api_quest_SetJournalStep( pRoom, userObjID,243, 1);
				api_quest_SetQuestStep( pRoom, userObjID,243, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</GameServer>