<VillageServer>

function sq11_562_recycling_iron1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 41 then
		sq11_562_recycling_iron1_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function sq11_562_recycling_iron1_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n041_duke_stwart-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5621, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5622, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5623, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5624, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5625, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5626, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5627, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5628, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5629, false);
			 end 

	end
	if npc_talk_index == "n041_duke_stwart-accepting-acceptted" then
				api_quest_AddQuest(userObjID,562, 1);
				api_quest_SetJournalStep(userObjID,562, 1);
				api_quest_SetQuestStep(userObjID,562, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1065, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201065, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300275, 3);
				npc_talk_index = "n041_duke_stwart-1";

	end
	if npc_talk_index == "n041_duke_stwart-2-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5621, true);
				 api_quest_RewardQuestUser(userObjID, 5621, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5622, true);
				 api_quest_RewardQuestUser(userObjID, 5622, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5623, true);
				 api_quest_RewardQuestUser(userObjID, 5623, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5624, true);
				 api_quest_RewardQuestUser(userObjID, 5624, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5625, true);
				 api_quest_RewardQuestUser(userObjID, 5625, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5626, true);
				 api_quest_RewardQuestUser(userObjID, 5626, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5627, true);
				 api_quest_RewardQuestUser(userObjID, 5627, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5628, true);
				 api_quest_RewardQuestUser(userObjID, 5628, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5629, true);
				 api_quest_RewardQuestUser(userObjID, 5629, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-2-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem(userObjID, 300275, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300275, api_quest_HasQuestItem(userObjID, 300275, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 563, 1);
					api_quest_SetQuestStep(userObjID, 563, 1);
					api_quest_SetJournalStep(userObjID, 563, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 41, 450 );

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n041_duke_stwart-1", "sq11_563_recycling_iron2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_562_recycling_iron1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 562);
	if qstep == 1 and CountIndex == 1065 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300275, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300275, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300275 then

	end
	if qstep == 1 and CountIndex == 201065 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300275, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300275, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 1067 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300275, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300275, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201067 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300275, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300275, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_562_recycling_iron1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 562);
	if qstep == 1 and CountIndex == 1065 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300275 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201065 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1067 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201067 and Count >= TargetCount  then

	end
end

function sq11_562_recycling_iron1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 562);
	local questID=562;
end

function sq11_562_recycling_iron1_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1065, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201065, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300275, 3);
end

function sq11_562_recycling_iron1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_562_recycling_iron1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,562, 1);
				api_quest_SetJournalStep(userObjID,562, 1);
				api_quest_SetQuestStep(userObjID,562, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1065, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201065, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300275, 3);
				npc_talk_index = "n041_duke_stwart-1";
end

</VillageServer>

<GameServer>
function sq11_562_recycling_iron1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 41 then
		sq11_562_recycling_iron1_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function sq11_562_recycling_iron1_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n041_duke_stwart-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5621, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5622, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5623, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5624, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5625, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5626, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5627, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5628, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5629, false);
			 end 

	end
	if npc_talk_index == "n041_duke_stwart-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,562, 1);
				api_quest_SetJournalStep( pRoom, userObjID,562, 1);
				api_quest_SetQuestStep( pRoom, userObjID,562, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1065, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201065, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300275, 3);
				npc_talk_index = "n041_duke_stwart-1";

	end
	if npc_talk_index == "n041_duke_stwart-2-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5621, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5621, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5622, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5622, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5623, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5623, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5624, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5624, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5625, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5625, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5626, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5626, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5627, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5627, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5628, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5628, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5629, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5629, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-2-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem( pRoom, userObjID, 300275, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300275, api_quest_HasQuestItem( pRoom, userObjID, 300275, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 563, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 563, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 563, 1);

				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 41, 450 );

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n041_duke_stwart-1", "sq11_563_recycling_iron2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_562_recycling_iron1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 562);
	if qstep == 1 and CountIndex == 1065 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300275, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300275, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300275 then

	end
	if qstep == 1 and CountIndex == 201065 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300275, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300275, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 1067 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300275, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300275, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201067 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300275, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300275, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_562_recycling_iron1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 562);
	if qstep == 1 and CountIndex == 1065 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300275 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 201065 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 1067 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201067 and Count >= TargetCount  then

	end
end

function sq11_562_recycling_iron1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 562);
	local questID=562;
end

function sq11_562_recycling_iron1_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1065, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201065, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300275, 3);
end

function sq11_562_recycling_iron1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_562_recycling_iron1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,562, 1);
				api_quest_SetJournalStep( pRoom, userObjID,562, 1);
				api_quest_SetQuestStep( pRoom, userObjID,562, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1065, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201065, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300275, 3);
				npc_talk_index = "n041_duke_stwart-1";
end

</GameServer>