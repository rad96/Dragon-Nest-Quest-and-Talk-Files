<VillageServer>

function sq08_241_change1_cleric_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1216 then
		sq08_241_change1_cleric_OnTalk_n1216_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 183 then
		sq08_241_change1_cleric_OnTalk_n183_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 34 then
		sq08_241_change1_cleric_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1216_cleric_jake--------------------------------------------------------------------------------
function sq08_241_change1_cleric_OnTalk_n1216_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1216_cleric_jake-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1216_cleric_jake-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1216_cleric_jake-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1216_cleric_jake-3-b" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-c" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-d" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-e" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-f" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-g" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-open_ui3" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n1216_cleric_jake-4-open_ui4" then 
				api_ui_OpenJobChange(userObjID)
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n183_cleric_jake--------------------------------------------------------------------------------
function sq08_241_change1_cleric_OnTalk_n183_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n183_cleric_jake-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n183_cleric_jake-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n183_cleric_jake-2-b" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-c" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-d" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-e" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-f" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-g" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-h" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n034_cleric_master_germain--------------------------------------------------------------------------------
function sq08_241_change1_cleric_OnTalk_n034_cleric_master_germain(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n034_cleric_master_germain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n034_cleric_master_germain-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n034_cleric_master_germain-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2410, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2410, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2410, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2410, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2410, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2410, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2410, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2410, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2410, false);
			 end 

	end
	if npc_talk_index == "n034_cleric_master_germain-accepting-acceptted" then
				api_quest_AddQuest(userObjID,241, 1);
				api_quest_SetJournalStep(userObjID,241, 1);
				api_quest_SetQuestStep(userObjID,241, 1);
				npc_talk_index = "n034_cleric_master_germain-1";

	end
	if npc_talk_index == "n034_cleric_master_germain-1-comp_check" then 
				if api_user_GetUserJobID(userObjID) == 20  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									if api_user_GetUserJobID(userObjID) == 22  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									if api_user_GetUserJobID(userObjID) == 41  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									if api_user_GetUserJobID(userObjID) == 42  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									if api_user_GetUserJobID(userObjID) == 43  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									if api_user_GetUserJobID(userObjID) == 44  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									npc_talk_index = "n034_cleric_master_germain-1-k";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-1-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-f" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-g" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-party_check1" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13024,1);

				else
									npc_talk_index = "n034_cleric_master_germain-1-h";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-1-i" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n034_cleric_master_germain-1-a" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-party_check2" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_user_ChangeMap(userObjID,13024,1);

				else
									npc_talk_index = "n034_cleric_master_germain-2-a";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-3-change_map3" then 
				if api_user_IsPartymember(userObjID) == 0  then
									api_user_ChangeMap(userObjID,13520,2);

				else
									npc_talk_index = "n034_cleric_master_germain-3-a";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-5-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-5-c" then 
				if api_user_GetUserJobID(userObjID) == 20  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2411, true);
				 api_quest_RewardQuestUser(userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2411, true);
				 api_quest_RewardQuestUser(userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2411, true);
				 api_quest_RewardQuestUser(userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2411, true);
				 api_quest_RewardQuestUser(userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2411, true);
				 api_quest_RewardQuestUser(userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2411, true);
				 api_quest_RewardQuestUser(userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2411, true);
				 api_quest_RewardQuestUser(userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2411, true);
				 api_quest_RewardQuestUser(userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2411, true);
				 api_quest_RewardQuestUser(userObjID, 2411, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2412, true);
				 api_quest_RewardQuestUser(userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2412, true);
				 api_quest_RewardQuestUser(userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2412, true);
				 api_quest_RewardQuestUser(userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2412, true);
				 api_quest_RewardQuestUser(userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2412, true);
				 api_quest_RewardQuestUser(userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2412, true);
				 api_quest_RewardQuestUser(userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2412, true);
				 api_quest_RewardQuestUser(userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2412, true);
				 api_quest_RewardQuestUser(userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2412, true);
				 api_quest_RewardQuestUser(userObjID, 2412, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-5-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 343, 1);
					api_quest_SetQuestStep(userObjID, 343, 1);
					api_quest_SetJournalStep(userObjID, 343, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-5-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n034_cleric_master_germain-1", "sq08_343_change_job4.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_241_change1_cleric_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 241);
end

function sq08_241_change1_cleric_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 241);
end

function sq08_241_change1_cleric_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 241);
	local questID=241;
end

function sq08_241_change1_cleric_OnRemoteStart( userObjID, questID )
end

function sq08_241_change1_cleric_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_241_change1_cleric_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,241, 1);
				api_quest_SetJournalStep(userObjID,241, 1);
				api_quest_SetQuestStep(userObjID,241, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
end

</VillageServer>

<GameServer>
function sq08_241_change1_cleric_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1216 then
		sq08_241_change1_cleric_OnTalk_n1216_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 183 then
		sq08_241_change1_cleric_OnTalk_n183_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 34 then
		sq08_241_change1_cleric_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1216_cleric_jake--------------------------------------------------------------------------------
function sq08_241_change1_cleric_OnTalk_n1216_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1216_cleric_jake-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1216_cleric_jake-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1216_cleric_jake-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1216_cleric_jake-3-b" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-c" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-d" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-e" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-f" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-g" then 
	end
	if npc_talk_index == "n1216_cleric_jake-3-open_ui3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n1216_cleric_jake-4-open_ui4" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n183_cleric_jake--------------------------------------------------------------------------------
function sq08_241_change1_cleric_OnTalk_n183_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n183_cleric_jake-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n183_cleric_jake-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n183_cleric_jake-2-b" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-c" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-d" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-e" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-f" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-g" then 
	end
	if npc_talk_index == "n183_cleric_jake-2-h" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n034_cleric_master_germain--------------------------------------------------------------------------------
function sq08_241_change1_cleric_OnTalk_n034_cleric_master_germain( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n034_cleric_master_germain-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n034_cleric_master_germain-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n034_cleric_master_germain-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n034_cleric_master_germain-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n034_cleric_master_germain-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n034_cleric_master_germain-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2410, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2410, false);
			 end 

	end
	if npc_talk_index == "n034_cleric_master_germain-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,241, 1);
				api_quest_SetJournalStep( pRoom, userObjID,241, 1);
				api_quest_SetQuestStep( pRoom, userObjID,241, 1);
				npc_talk_index = "n034_cleric_master_germain-1";

	end
	if npc_talk_index == "n034_cleric_master_germain-1-comp_check" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 20  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 22  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 41  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 42  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 43  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 44  then
									npc_talk_index = "n034_cleric_master_germain-1-j";

				else
									npc_talk_index = "n034_cleric_master_germain-1-k";

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-1-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-c" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-d" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-f" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-g" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-1-party_check1" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13024,1);

				else
									npc_talk_index = "n034_cleric_master_germain-1-h";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-1-i" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n034_cleric_master_germain-1-a" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-2-party_check2" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_user_ChangeMap( pRoom, userObjID,13024,1);

				else
									npc_talk_index = "n034_cleric_master_germain-2-a";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-3-change_map3" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									api_user_ChangeMap( pRoom, userObjID,13520,2);

				else
									npc_talk_index = "n034_cleric_master_germain-3-a";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-5-b" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-5-c" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 20  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2411, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2411, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2411, questID, 1);
			 end 

				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2412, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2412, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2412, questID, 1);
			 end 

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-5-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 343, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 343, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 343, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n034_cleric_master_germain-5-e" then 
	end
	if npc_talk_index == "n034_cleric_master_germain-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n034_cleric_master_germain-1", "sq08_343_change_job4.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_241_change1_cleric_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 241);
end

function sq08_241_change1_cleric_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 241);
end

function sq08_241_change1_cleric_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 241);
	local questID=241;
end

function sq08_241_change1_cleric_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_241_change1_cleric_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_241_change1_cleric_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,241, 1);
				api_quest_SetJournalStep( pRoom, userObjID,241, 1);
				api_quest_SetQuestStep( pRoom, userObjID,241, 1);
				npc_talk_index = "n034_cleric_master_germain-1";
end

</GameServer>