<VillageServer>

function sq08_271_grandpa_goblin06_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 171 then
		sq08_271_grandpa_goblin06_OnTalk_n171_goblin_grandfather(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 185 then
		sq08_271_grandpa_goblin06_OnTalk_n185_goblin_grandfather(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n171_goblin_grandfather--------------------------------------------------------------------------------
function sq08_271_grandpa_goblin06_OnTalk_n171_goblin_grandfather(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n171_goblin_grandfather-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n171_goblin_grandfather-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n185_goblin_grandfather--------------------------------------------------------------------------------
function sq08_271_grandpa_goblin06_OnTalk_n185_goblin_grandfather(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n185_goblin_grandfather-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n185_goblin_grandfather-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n185_goblin_grandfather-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n185_goblin_grandfather-accepting-clear_check" then
				if api_quest_IsMarkingCompleteQuest(userObjID, 54 or 55 or 56 or 57 or 58) == 1 then
									npc_talk_index = "n185_goblin_grandfather-accepting-d";

				else
									npc_talk_index = "n185_goblin_grandfather-accepting-a";

				end

	end
	if npc_talk_index == "n185_goblin_grandfather-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2711, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2712, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2713, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2714, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2715, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2716, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2717, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2718, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2719, false);
			 end 

	end
	if npc_talk_index == "n185_goblin_grandfather-accepting-acceptted" then
				api_quest_AddQuest(userObjID,271, 1);
				api_quest_SetJournalStep(userObjID,271, 1);
				api_quest_SetQuestStep(userObjID,271, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 463, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 465, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 469, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 472, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200463, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200465, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200469, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 200472, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 8, 3, 300005, 4);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n185_goblin_grandfather-1";

	end
	if npc_talk_index == "n185_goblin_grandfather-2-b" then 
	end
	if npc_talk_index == "n185_goblin_grandfather-2-c" then 
	end
	if npc_talk_index == "n185_goblin_grandfather-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2711, true);
				 api_quest_RewardQuestUser(userObjID, 2711, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2712, true);
				 api_quest_RewardQuestUser(userObjID, 2712, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2713, true);
				 api_quest_RewardQuestUser(userObjID, 2713, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2714, true);
				 api_quest_RewardQuestUser(userObjID, 2714, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2715, true);
				 api_quest_RewardQuestUser(userObjID, 2715, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2716, true);
				 api_quest_RewardQuestUser(userObjID, 2716, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2717, true);
				 api_quest_RewardQuestUser(userObjID, 2717, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2718, true);
				 api_quest_RewardQuestUser(userObjID, 2718, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2719, true);
				 api_quest_RewardQuestUser(userObjID, 2719, questID, 1);
			 end 
	end
	if npc_talk_index == "n185_goblin_grandfather-2-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300005, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300005, api_quest_HasQuestItem(userObjID, 300005, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_271_grandpa_goblin06_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 271);
	if qstep == 1 and CountIndex == 463 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 465 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 469 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 472 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300005 then

	end
	if qstep == 1 and CountIndex == 200463 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200465 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200469 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200472 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_271_grandpa_goblin06_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 271);
	if qstep == 1 and CountIndex == 463 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 465 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 469 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 472 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300005 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 200463 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200465 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200469 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200472 and Count >= TargetCount  then

	end
end

function sq08_271_grandpa_goblin06_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 271);
	local questID=271;
end

function sq08_271_grandpa_goblin06_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 463, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 465, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 469, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 472, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200463, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200465, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200469, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 200472, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 8, 3, 300005, 4);
				api_quest_SetJournalStep(userObjID, questID, 2);
end

function sq08_271_grandpa_goblin06_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_271_grandpa_goblin06_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,271, 1);
				api_quest_SetJournalStep(userObjID,271, 1);
				api_quest_SetQuestStep(userObjID,271, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 463, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 465, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 469, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 472, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200463, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200465, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200469, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 200472, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 8, 3, 300005, 4);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n185_goblin_grandfather-1";
end

</VillageServer>

<GameServer>
function sq08_271_grandpa_goblin06_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 171 then
		sq08_271_grandpa_goblin06_OnTalk_n171_goblin_grandfather( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 185 then
		sq08_271_grandpa_goblin06_OnTalk_n185_goblin_grandfather( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n171_goblin_grandfather--------------------------------------------------------------------------------
function sq08_271_grandpa_goblin06_OnTalk_n171_goblin_grandfather( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n171_goblin_grandfather-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n171_goblin_grandfather-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n185_goblin_grandfather--------------------------------------------------------------------------------
function sq08_271_grandpa_goblin06_OnTalk_n185_goblin_grandfather( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n185_goblin_grandfather-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n185_goblin_grandfather-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n185_goblin_grandfather-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n185_goblin_grandfather-accepting-clear_check" then
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 54 or 55 or 56 or 57 or 58) == 1 then
									npc_talk_index = "n185_goblin_grandfather-accepting-d";

				else
									npc_talk_index = "n185_goblin_grandfather-accepting-a";

				end

	end
	if npc_talk_index == "n185_goblin_grandfather-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2711, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2712, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2713, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2714, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2715, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2716, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2717, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2718, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2719, false);
			 end 

	end
	if npc_talk_index == "n185_goblin_grandfather-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,271, 1);
				api_quest_SetJournalStep( pRoom, userObjID,271, 1);
				api_quest_SetQuestStep( pRoom, userObjID,271, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 463, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 465, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 469, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 472, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200463, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200465, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200469, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 200472, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 3, 300005, 4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n185_goblin_grandfather-1";

	end
	if npc_talk_index == "n185_goblin_grandfather-2-b" then 
	end
	if npc_talk_index == "n185_goblin_grandfather-2-c" then 
	end
	if npc_talk_index == "n185_goblin_grandfather-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2711, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2711, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2712, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2712, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2713, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2713, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2714, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2714, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2715, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2715, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2716, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2716, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2717, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2717, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2718, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2718, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2719, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2719, questID, 1);
			 end 
	end
	if npc_talk_index == "n185_goblin_grandfather-2-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300005, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300005, api_quest_HasQuestItem( pRoom, userObjID, 300005, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_271_grandpa_goblin06_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 271);
	if qstep == 1 and CountIndex == 463 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 465 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 469 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 472 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300005 then

	end
	if qstep == 1 and CountIndex == 200463 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200465 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200469 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200472 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300005, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300005, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_271_grandpa_goblin06_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 271);
	if qstep == 1 and CountIndex == 463 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 465 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 469 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 472 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300005 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 200463 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200465 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200469 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200472 and Count >= TargetCount  then

	end
end

function sq08_271_grandpa_goblin06_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 271);
	local questID=271;
end

function sq08_271_grandpa_goblin06_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 463, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 465, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 469, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 472, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200463, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200465, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200469, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 200472, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 3, 300005, 4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
end

function sq08_271_grandpa_goblin06_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_271_grandpa_goblin06_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,271, 1);
				api_quest_SetJournalStep( pRoom, userObjID,271, 1);
				api_quest_SetQuestStep( pRoom, userObjID,271, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 463, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 465, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 469, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 472, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200463, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200465, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200469, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 200472, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 3, 300005, 4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n185_goblin_grandfather-1";
end

</GameServer>