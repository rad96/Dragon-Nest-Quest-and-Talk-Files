<VillageServer>

function mqc05_9605_conditions_of_confidence_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 39 then
		mqc05_9605_conditions_of_confidence_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 216 then
		mqc05_9605_conditions_of_confidence_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9605_conditions_of_confidence_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n039_bishop_ignasio-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n039_bishop_ignasio-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9605, 2);
				api_quest_SetJournalStep(userObjID,9605, 1);
				api_quest_SetQuestStep(userObjID,9605, 1);
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-1-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300025, 5);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 922, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 925, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200922, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 200925, 30000);
	end
	if npc_talk_index == "n039_bishop_ignasio-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 96050, true);
				 api_quest_RewardQuestUser(userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 96050, true);
				 api_quest_RewardQuestUser(userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 96050, true);
				 api_quest_RewardQuestUser(userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 96050, true);
				 api_quest_RewardQuestUser(userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 96050, true);
				 api_quest_RewardQuestUser(userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 96050, true);
				 api_quest_RewardQuestUser(userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 96050, true);
				 api_quest_RewardQuestUser(userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 96050, true);
				 api_quest_RewardQuestUser(userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 96050, true);
				 api_quest_RewardQuestUser(userObjID, 96050, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9606, 2);
					api_quest_SetQuestStep(userObjID, 9606, 1);
					api_quest_SetJournalStep(userObjID, 9606, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n039_bishop_ignasio-1", "mqc05_9606_people_left.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n216_geraint_wounded--------------------------------------------------------------------------------
function mqc05_9605_conditions_of_confidence_OnTalk_n216_geraint_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n216_geraint_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n216_geraint_wounded-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n216_geraint_wounded-1-b" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-c" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-d" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-e" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-f" then 
	end
	if npc_talk_index == "n216_geraint_wounded-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9605_conditions_of_confidence_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9605);
	if qstep == 3 and CountIndex == 300025 then

	end
	if qstep == 3 and CountIndex == 922 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300025, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300025, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 925 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300025, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300025, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200922 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300025, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300025, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200925 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300025, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300025, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mqc05_9605_conditions_of_confidence_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9605);
	if qstep == 3 and CountIndex == 300025 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 3 and CountIndex == 922 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 925 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200922 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200925 and Count >= TargetCount  then

	end
end

function mqc05_9605_conditions_of_confidence_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9605);
	local questID=9605;
end

function mqc05_9605_conditions_of_confidence_OnRemoteStart( userObjID, questID )
end

function mqc05_9605_conditions_of_confidence_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc05_9605_conditions_of_confidence_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9605, 2);
				api_quest_SetJournalStep(userObjID,9605, 1);
				api_quest_SetQuestStep(userObjID,9605, 1);
				npc_talk_index = "n039_bishop_ignasio-1";
end

</VillageServer>

<GameServer>
function mqc05_9605_conditions_of_confidence_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 39 then
		mqc05_9605_conditions_of_confidence_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 216 then
		mqc05_9605_conditions_of_confidence_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc05_9605_conditions_of_confidence_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n039_bishop_ignasio-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n039_bishop_ignasio-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n039_bishop_ignasio-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9605, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9605, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9605, 1);
				npc_talk_index = "n039_bishop_ignasio-1";

	end
	if npc_talk_index == "n039_bishop_ignasio-1-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-1-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-2-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300025, 5);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 922, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 925, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200922, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 200925, 30000);
	end
	if npc_talk_index == "n039_bishop_ignasio-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 96050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 96050, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9606, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9606, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9606, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-1", "mqc05_9606_people_left.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n216_geraint_wounded--------------------------------------------------------------------------------
function mqc05_9605_conditions_of_confidence_OnTalk_n216_geraint_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n216_geraint_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n216_geraint_wounded-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n216_geraint_wounded-1-b" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-c" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-d" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-e" then 
	end
	if npc_talk_index == "n216_geraint_wounded-1-f" then 
	end
	if npc_talk_index == "n216_geraint_wounded-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9605_conditions_of_confidence_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9605);
	if qstep == 3 and CountIndex == 300025 then

	end
	if qstep == 3 and CountIndex == 922 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300025, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300025, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 925 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300025, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300025, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200922 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300025, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300025, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200925 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300025, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300025, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mqc05_9605_conditions_of_confidence_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9605);
	if qstep == 3 and CountIndex == 300025 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 3 and CountIndex == 922 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 925 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200922 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200925 and Count >= TargetCount  then

	end
end

function mqc05_9605_conditions_of_confidence_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9605);
	local questID=9605;
end

function mqc05_9605_conditions_of_confidence_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc05_9605_conditions_of_confidence_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc05_9605_conditions_of_confidence_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9605, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9605, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9605, 1);
				npc_talk_index = "n039_bishop_ignasio-1";
end

</GameServer>