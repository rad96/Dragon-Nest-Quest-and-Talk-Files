<VillageServer>

function mqc06_9507_reality_of_wonderment_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1691 then
		mqc06_9507_reality_of_wonderment_OnTalk_n1691_eltia_lotus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1696 then
		mqc06_9507_reality_of_wonderment_OnTalk_n1696_kaye_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1697 then
		mqc06_9507_reality_of_wonderment_OnTalk_n1697_kaye_tera_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1691_eltia_lotus--------------------------------------------------------------------------------
function mqc06_9507_reality_of_wonderment_OnTalk_n1691_eltia_lotus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1691_eltia_lotus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1691_eltia_lotus-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1691_eltia_lotus-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1691_eltia_lotus-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1691_eltia_lotus-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9507, 2);
				api_quest_SetJournalStep(userObjID,9507, 1);
				api_quest_SetQuestStep(userObjID,9507, 1);
				npc_talk_index = "n1691_eltia_lotus-1";

	end
	if npc_talk_index == "n1691_eltia_lotus-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1691_eltia_lotus-2-b" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-c" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-d" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-e" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-f" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-g" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-h" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-i" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-j" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-k" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-l" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-m" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1696_kaye_dragon--------------------------------------------------------------------------------
function mqc06_9507_reality_of_wonderment_OnTalk_n1696_kaye_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1696_kaye_dragon-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1696_kaye_dragon-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1696_kaye_dragon-3-b" then 
	end
	if npc_talk_index == "n1696_kaye_dragon-3-c" then 
	end
	if npc_talk_index == "n1696_kaye_dragon-3-d" then 
	end
	if npc_talk_index == "n1696_kaye_dragon-3-e" then 
	end
	if npc_talk_index == "n1696_kaye_dragon-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1455, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 400466, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201455, 30000);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1697_kaye_tera_dragon--------------------------------------------------------------------------------
function mqc06_9507_reality_of_wonderment_OnTalk_n1697_kaye_tera_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1697_kaye_tera_dragon-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1697_kaye_tera_dragon-5-b" then 

				if api_quest_HasQuestItem(userObjID, 400466, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400466, api_quest_HasQuestItem(userObjID, 400466, 1));
				end
	end
	if npc_talk_index == "n1697_kaye_tera_dragon-5-c" then 
	end
	if npc_talk_index == "n1697_kaye_tera_dragon-5-d" then 
	end
	if npc_talk_index == "n1697_kaye_tera_dragon-5-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95070, true);
				 api_quest_RewardQuestUser(userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95070, true);
				 api_quest_RewardQuestUser(userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95070, true);
				 api_quest_RewardQuestUser(userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95070, true);
				 api_quest_RewardQuestUser(userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95070, true);
				 api_quest_RewardQuestUser(userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95070, true);
				 api_quest_RewardQuestUser(userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95070, true);
				 api_quest_RewardQuestUser(userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95070, true);
				 api_quest_RewardQuestUser(userObjID, 95070, questID, 1);
			 end 
	end
	if npc_talk_index == "n1697_kaye_tera_dragon-5-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9508, 2);
					api_quest_SetQuestStep(userObjID, 9508, 1);
					api_quest_SetJournalStep(userObjID, 9508, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1697_kaye_tera_dragon-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1697_kaye_tera_dragon-1", "mqc06_9508_catch_ignasio.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc06_9507_reality_of_wonderment_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9507);
	if qstep == 4 and CountIndex == 1455 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400466, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400466, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 4 and CountIndex == 400466 then
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 4 and CountIndex == 201455 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400466, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400466, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mqc06_9507_reality_of_wonderment_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9507);
	if qstep == 4 and CountIndex == 1455 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 400466 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 201455 and Count >= TargetCount  then

	end
end

function mqc06_9507_reality_of_wonderment_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9507);
	local questID=9507;
end

function mqc06_9507_reality_of_wonderment_OnRemoteStart( userObjID, questID )
end

function mqc06_9507_reality_of_wonderment_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc06_9507_reality_of_wonderment_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9507, 2);
				api_quest_SetJournalStep(userObjID,9507, 1);
				api_quest_SetQuestStep(userObjID,9507, 1);
				npc_talk_index = "n1691_eltia_lotus-1";
end

</VillageServer>

<GameServer>
function mqc06_9507_reality_of_wonderment_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1691 then
		mqc06_9507_reality_of_wonderment_OnTalk_n1691_eltia_lotus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1696 then
		mqc06_9507_reality_of_wonderment_OnTalk_n1696_kaye_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1697 then
		mqc06_9507_reality_of_wonderment_OnTalk_n1697_kaye_tera_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1691_eltia_lotus--------------------------------------------------------------------------------
function mqc06_9507_reality_of_wonderment_OnTalk_n1691_eltia_lotus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1691_eltia_lotus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1691_eltia_lotus-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1691_eltia_lotus-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1691_eltia_lotus-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1691_eltia_lotus-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9507, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9507, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9507, 1);
				npc_talk_index = "n1691_eltia_lotus-1";

	end
	if npc_talk_index == "n1691_eltia_lotus-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1691_eltia_lotus-2-b" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-c" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-d" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-e" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-f" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-g" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-h" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-i" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-j" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-k" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-l" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-2-m" then 
	end
	if npc_talk_index == "n1691_eltia_lotus-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1696_kaye_dragon--------------------------------------------------------------------------------
function mqc06_9507_reality_of_wonderment_OnTalk_n1696_kaye_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1696_kaye_dragon-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1696_kaye_dragon-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1696_kaye_dragon-3-b" then 
	end
	if npc_talk_index == "n1696_kaye_dragon-3-c" then 
	end
	if npc_talk_index == "n1696_kaye_dragon-3-d" then 
	end
	if npc_talk_index == "n1696_kaye_dragon-3-e" then 
	end
	if npc_talk_index == "n1696_kaye_dragon-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1455, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 400466, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201455, 30000);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1697_kaye_tera_dragon--------------------------------------------------------------------------------
function mqc06_9507_reality_of_wonderment_OnTalk_n1697_kaye_tera_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1697_kaye_tera_dragon-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1697_kaye_tera_dragon-5-b" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400466, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400466, api_quest_HasQuestItem( pRoom, userObjID, 400466, 1));
				end
	end
	if npc_talk_index == "n1697_kaye_tera_dragon-5-c" then 
	end
	if npc_talk_index == "n1697_kaye_tera_dragon-5-d" then 
	end
	if npc_talk_index == "n1697_kaye_tera_dragon-5-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95070, questID, 1);
			 end 
	end
	if npc_talk_index == "n1697_kaye_tera_dragon-5-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9508, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9508, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9508, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1697_kaye_tera_dragon-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1697_kaye_tera_dragon-1", "mqc06_9508_catch_ignasio.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc06_9507_reality_of_wonderment_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9507);
	if qstep == 4 and CountIndex == 1455 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400466, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400466, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 4 and CountIndex == 400466 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 4 and CountIndex == 201455 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400466, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400466, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mqc06_9507_reality_of_wonderment_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9507);
	if qstep == 4 and CountIndex == 1455 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 400466 and Count >= TargetCount  then

	end
	if qstep == 4 and CountIndex == 201455 and Count >= TargetCount  then

	end
end

function mqc06_9507_reality_of_wonderment_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9507);
	local questID=9507;
end

function mqc06_9507_reality_of_wonderment_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc06_9507_reality_of_wonderment_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc06_9507_reality_of_wonderment_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9507, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9507, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9507, 1);
				npc_talk_index = "n1691_eltia_lotus-1";
end

</GameServer>