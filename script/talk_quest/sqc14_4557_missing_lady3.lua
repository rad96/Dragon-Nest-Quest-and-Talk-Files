<VillageServer>

function sqc14_4557_missing_lady3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1608 then
		sqc14_4557_missing_lady3_OnTalk_n1608_maid_of_lena(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1608_maid_of_lena--------------------------------------------------------------------------------
function sqc14_4557_missing_lady3_OnTalk_n1608_maid_of_lena(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1608_maid_of_lena-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1608_maid_of_lena-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1608_maid_of_lena-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1608_maid_of_lena-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1608_maid_of_lena-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45570, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45570, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45570, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45570, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45570, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45570, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45570, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45570, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45570, false);
			 end 

	end
	if npc_talk_index == "n1608_maid_of_lena-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4557, 1);
				api_quest_SetJournalStep(userObjID,4557, 1);
				api_quest_SetQuestStep(userObjID,4557, 1);
				npc_talk_index = "n1608_maid_of_lena-1";

	end
	if npc_talk_index == "n1608_maid_of_lena-1-b" then 
	end
	if npc_talk_index == "n1608_maid_of_lena-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 601964, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 400447, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 701964, 30000);
	end
	if npc_talk_index == "n1608_maid_of_lena-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45570, true);
				 api_quest_RewardQuestUser(userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45570, true);
				 api_quest_RewardQuestUser(userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45570, true);
				 api_quest_RewardQuestUser(userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45570, true);
				 api_quest_RewardQuestUser(userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45570, true);
				 api_quest_RewardQuestUser(userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45570, true);
				 api_quest_RewardQuestUser(userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45570, true);
				 api_quest_RewardQuestUser(userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45570, true);
				 api_quest_RewardQuestUser(userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45570, true);
				 api_quest_RewardQuestUser(userObjID, 45570, questID, 1);
			 end 
	end
	if npc_talk_index == "n1608_maid_of_lena-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4558, 1);
					api_quest_SetQuestStep(userObjID, 4558, 1);
					api_quest_SetJournalStep(userObjID, 4558, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1608_maid_of_lena-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1608_maid_of_lena-1", "sqc14_4558_missing_lady4.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4557_missing_lady3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4557);
	if qstep == 2 and CountIndex == 601964 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400447, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400447, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400447 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 701964 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400447, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400447, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sqc14_4557_missing_lady3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4557);
	if qstep == 2 and CountIndex == 601964 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400447 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701964 and Count >= TargetCount  then

	end
end

function sqc14_4557_missing_lady3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4557);
	local questID=4557;
end

function sqc14_4557_missing_lady3_OnRemoteStart( userObjID, questID )
end

function sqc14_4557_missing_lady3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc14_4557_missing_lady3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4557, 1);
				api_quest_SetJournalStep(userObjID,4557, 1);
				api_quest_SetQuestStep(userObjID,4557, 1);
				npc_talk_index = "n1608_maid_of_lena-1";
end

</VillageServer>

<GameServer>
function sqc14_4557_missing_lady3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1608 then
		sqc14_4557_missing_lady3_OnTalk_n1608_maid_of_lena( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1608_maid_of_lena--------------------------------------------------------------------------------
function sqc14_4557_missing_lady3_OnTalk_n1608_maid_of_lena( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1608_maid_of_lena-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1608_maid_of_lena-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1608_maid_of_lena-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1608_maid_of_lena-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1608_maid_of_lena-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, false);
			 end 

	end
	if npc_talk_index == "n1608_maid_of_lena-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4557, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4557, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4557, 1);
				npc_talk_index = "n1608_maid_of_lena-1";

	end
	if npc_talk_index == "n1608_maid_of_lena-1-b" then 
	end
	if npc_talk_index == "n1608_maid_of_lena-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 601964, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 400447, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 701964, 30000);
	end
	if npc_talk_index == "n1608_maid_of_lena-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45570, questID, 1);
			 end 
	end
	if npc_talk_index == "n1608_maid_of_lena-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4558, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4558, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4558, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1608_maid_of_lena-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1608_maid_of_lena-1", "sqc14_4558_missing_lady4.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4557_missing_lady3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4557);
	if qstep == 2 and CountIndex == 601964 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400447, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400447, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 400447 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 701964 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400447, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400447, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sqc14_4557_missing_lady3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4557);
	if qstep == 2 and CountIndex == 601964 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400447 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701964 and Count >= TargetCount  then

	end
end

function sqc14_4557_missing_lady3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4557);
	local questID=4557;
end

function sqc14_4557_missing_lady3_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc14_4557_missing_lady3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc14_4557_missing_lady3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4557, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4557, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4557, 1);
				npc_talk_index = "n1608_maid_of_lena-1";
end

</GameServer>