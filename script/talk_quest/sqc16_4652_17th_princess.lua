<VillageServer>

function sqc16_4652_17th_princess_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1103 then
		sqc16_4652_17th_princess_OnTalk_n1103_trader_phara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1797 then
		sqc16_4652_17th_princess_OnTalk_n1797_sadari(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1798 then
		sqc16_4652_17th_princess_OnTalk_n1798_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1103_trader_phara--------------------------------------------------------------------------------
function sqc16_4652_17th_princess_OnTalk_n1103_trader_phara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1103_trader_phara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1103_trader_phara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1103_trader_phara-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1103_trader_phara-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1103_trader_phara-accepting-g" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46520, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46520, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46520, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46520, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46520, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46520, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46520, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46520, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46520, false);
			 end 

	end
	if npc_talk_index == "n1103_trader_phara-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4652, 1);
				api_quest_SetJournalStep(userObjID,4652, 1);
				api_quest_SetQuestStep(userObjID,4652, 1);
				npc_talk_index = "n1103_trader_phara-1";

	end
	if npc_talk_index == "n1103_trader_phara-3-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-e" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-f" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-g" then 
	end
	if npc_talk_index == "n1103_trader_phara-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n1103_trader_phara-4-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-e" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-f" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-g" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-h" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-i" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-j" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-k" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-l" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-m" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-n" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-o" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-p" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46520, true);
				 api_quest_RewardQuestUser(userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46520, true);
				 api_quest_RewardQuestUser(userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46520, true);
				 api_quest_RewardQuestUser(userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46520, true);
				 api_quest_RewardQuestUser(userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46520, true);
				 api_quest_RewardQuestUser(userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46520, true);
				 api_quest_RewardQuestUser(userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46520, true);
				 api_quest_RewardQuestUser(userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46520, true);
				 api_quest_RewardQuestUser(userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46520, true);
				 api_quest_RewardQuestUser(userObjID, 46520, questID, 1);
			 end 
	end
	if npc_talk_index == "n1103_trader_phara-4-q" then 

				if api_quest_HasQuestItem(userObjID, 400474, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400474, api_quest_HasQuestItem(userObjID, 400474, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1797_sadari--------------------------------------------------------------------------------
function sqc16_4652_17th_princess_OnTalk_n1797_sadari(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1797_sadari-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1797_sadari-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1797_sadari-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1798_book--------------------------------------------------------------------------------
function sqc16_4652_17th_princess_OnTalk_n1798_book(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1798_book-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1798_book-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1798_book-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400474, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400474, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4652_17th_princess_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4652);
end

function sqc16_4652_17th_princess_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4652);
end

function sqc16_4652_17th_princess_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4652);
	local questID=4652;
end

function sqc16_4652_17th_princess_OnRemoteStart( userObjID, questID )
end

function sqc16_4652_17th_princess_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc16_4652_17th_princess_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4652, 1);
				api_quest_SetJournalStep(userObjID,4652, 1);
				api_quest_SetQuestStep(userObjID,4652, 1);
				npc_talk_index = "n1103_trader_phara-1";
end

</VillageServer>

<GameServer>
function sqc16_4652_17th_princess_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1103 then
		sqc16_4652_17th_princess_OnTalk_n1103_trader_phara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1797 then
		sqc16_4652_17th_princess_OnTalk_n1797_sadari( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1798 then
		sqc16_4652_17th_princess_OnTalk_n1798_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1103_trader_phara--------------------------------------------------------------------------------
function sqc16_4652_17th_princess_OnTalk_n1103_trader_phara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1103_trader_phara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1103_trader_phara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1103_trader_phara-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1103_trader_phara-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1103_trader_phara-accepting-g" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, false);
			 end 

	end
	if npc_talk_index == "n1103_trader_phara-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4652, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4652, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4652, 1);
				npc_talk_index = "n1103_trader_phara-1";

	end
	if npc_talk_index == "n1103_trader_phara-3-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-e" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-f" then 
	end
	if npc_talk_index == "n1103_trader_phara-3-g" then 
	end
	if npc_talk_index == "n1103_trader_phara-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n1103_trader_phara-4-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-c" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-d" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-e" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-f" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-g" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-h" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-i" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-j" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-k" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-l" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-m" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-n" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-o" then 
	end
	if npc_talk_index == "n1103_trader_phara-4-p" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46520, questID, 1);
			 end 
	end
	if npc_talk_index == "n1103_trader_phara-4-q" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400474, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400474, api_quest_HasQuestItem( pRoom, userObjID, 400474, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1797_sadari--------------------------------------------------------------------------------
function sqc16_4652_17th_princess_OnTalk_n1797_sadari( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1797_sadari-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1797_sadari-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1797_sadari-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1798_book--------------------------------------------------------------------------------
function sqc16_4652_17th_princess_OnTalk_n1798_book( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1798_book-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1798_book-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1798_book-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400474, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400474, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4652_17th_princess_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4652);
end

function sqc16_4652_17th_princess_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4652);
end

function sqc16_4652_17th_princess_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4652);
	local questID=4652;
end

function sqc16_4652_17th_princess_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc16_4652_17th_princess_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc16_4652_17th_princess_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4652, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4652, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4652, 1);
				npc_talk_index = "n1103_trader_phara-1";
end

</GameServer>