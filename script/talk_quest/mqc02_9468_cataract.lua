<VillageServer>

function mqc02_9468_cataract_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1686 then
		mqc02_9468_cataract_OnTalk_n1686_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1687 then
		mqc02_9468_cataract_OnTalk_n1687_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 23 then
		mqc02_9468_cataract_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mqc02_9468_cataract_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1686_eltia--------------------------------------------------------------------------------
function mqc02_9468_cataract_OnTalk_n1686_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1686_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1686_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1686_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9468, 2);
				api_quest_SetJournalStep(userObjID,9468, 1);
				api_quest_SetQuestStep(userObjID,9468, 1);
				npc_talk_index = "n1686_eltia-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1687_kaye--------------------------------------------------------------------------------
function mqc02_9468_cataract_OnTalk_n1687_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1687_kaye-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1687_kaye-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1687_kaye-1-b" then 
	end
	if npc_talk_index == "n1687_kaye-1-c" then 
	end
	if npc_talk_index == "n1687_kaye-1-d" then 
	end
	if npc_talk_index == "n1687_kaye-1-e" then 
	end
	if npc_talk_index == "n1687_kaye-1-f" then 
	end
	if npc_talk_index == "n1687_kaye-1-f" then 
	end
	if npc_talk_index == "n1687_kaye-1-g" then 
	end
	if npc_talk_index == "n1687_kaye-1-h" then 
	end
	if npc_talk_index == "n1687_kaye-1-i" then 
	end
	if npc_talk_index == "n1687_kaye-1-j" then 
	end
	if npc_talk_index == "n1687_kaye-1-k" then 
	end
	if npc_talk_index == "n1687_kaye-1-l" then 
	end
	if npc_talk_index == "n1687_kaye-1-m" then 
	end
	if npc_talk_index == "n1687_kaye-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n023_ranger_fugus--------------------------------------------------------------------------------
function mqc02_9468_cataract_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n023_ranger_fugus-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n023_ranger_fugus-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n023_ranger_fugus-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n023_ranger_fugus-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n023_ranger_fugus-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n023_ranger_fugus-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n023_ranger_fugus-2-a" then 

				if api_quest_HasQuestItem(userObjID, 300012, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300012, api_quest_HasQuestItem(userObjID, 300012, 1));
				end
	end
	if npc_talk_index == "n023_ranger_fugus-2-b" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n023_ranger_fugus-4-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-c" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-d" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-e" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-f" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-g" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-h" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-i" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-j" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-k" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-l" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-m" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 350, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300015, 1);
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end
	if npc_talk_index == "n023_ranger_fugus-6-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-6-c" then 
	end
	if npc_talk_index == "n023_ranger_fugus-6-d" then 
	end
	if npc_talk_index == "n023_ranger_fugus-6-e" then 
	end
	if npc_talk_index == "n023_ranger_fugus-6-f" then 
	end
	if npc_talk_index == "n023_ranger_fugus-6-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94680, true);
				 api_quest_RewardQuestUser(userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94680, true);
				 api_quest_RewardQuestUser(userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94680, true);
				 api_quest_RewardQuestUser(userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94680, true);
				 api_quest_RewardQuestUser(userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94680, true);
				 api_quest_RewardQuestUser(userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94680, true);
				 api_quest_RewardQuestUser(userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94680, true);
				 api_quest_RewardQuestUser(userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94680, true);
				 api_quest_RewardQuestUser(userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94680, true);
				 api_quest_RewardQuestUser(userObjID, 94680, questID, 1);
			 end 
	end
	if npc_talk_index == "n023_ranger_fugus-6-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9469, 2);
					api_quest_SetQuestStep(userObjID, 9469, 1);
					api_quest_SetJournalStep(userObjID, 9469, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300089, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300089, api_quest_HasQuestItem(userObjID, 300089, 1));
				end
	end
	if npc_talk_index == "n023_ranger_fugus-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n023_ranger_fugus-1", "mqc02_9469_for_her.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mqc02_9468_cataract_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-g" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-h" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-i" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-j" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-k" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-l" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-m" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-n" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-o" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-p" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-q" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-r" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc02_9468_cataract_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9468);
	if qstep == 5 and CountIndex == 350 then
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
				 local TableItem = { 
				 { 300089, 1 }  , 
				 { 300015, 1 }  
 				} 
				if api_quest_CheckQuestInvenForAddItemList(userObjID, TableItem) == 1 then
						api_quest_AddQuestItem(userObjID, 300089, 1, questID);
						api_quest_AddQuestItem(userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 5 and CountIndex == 300015 then

	end
end

function mqc02_9468_cataract_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9468);
	if qstep == 5 and CountIndex == 350 and Count >= TargetCount  then

	end
	if qstep == 5 and CountIndex == 300015 and Count >= TargetCount  then

	end
end

function mqc02_9468_cataract_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9468);
	local questID=9468;
end

function mqc02_9468_cataract_OnRemoteStart( userObjID, questID )
end

function mqc02_9468_cataract_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc02_9468_cataract_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9468, 2);
				api_quest_SetJournalStep(userObjID,9468, 1);
				api_quest_SetQuestStep(userObjID,9468, 1);
				npc_talk_index = "n1686_eltia-1";
end

</VillageServer>

<GameServer>
function mqc02_9468_cataract_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1686 then
		mqc02_9468_cataract_OnTalk_n1686_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1687 then
		mqc02_9468_cataract_OnTalk_n1687_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 23 then
		mqc02_9468_cataract_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mqc02_9468_cataract_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1686_eltia--------------------------------------------------------------------------------
function mqc02_9468_cataract_OnTalk_n1686_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1686_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1686_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1686_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9468, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9468, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9468, 1);
				npc_talk_index = "n1686_eltia-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1687_kaye--------------------------------------------------------------------------------
function mqc02_9468_cataract_OnTalk_n1687_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1687_kaye-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1687_kaye-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1687_kaye-1-b" then 
	end
	if npc_talk_index == "n1687_kaye-1-c" then 
	end
	if npc_talk_index == "n1687_kaye-1-d" then 
	end
	if npc_talk_index == "n1687_kaye-1-e" then 
	end
	if npc_talk_index == "n1687_kaye-1-f" then 
	end
	if npc_talk_index == "n1687_kaye-1-f" then 
	end
	if npc_talk_index == "n1687_kaye-1-g" then 
	end
	if npc_talk_index == "n1687_kaye-1-h" then 
	end
	if npc_talk_index == "n1687_kaye-1-i" then 
	end
	if npc_talk_index == "n1687_kaye-1-j" then 
	end
	if npc_talk_index == "n1687_kaye-1-k" then 
	end
	if npc_talk_index == "n1687_kaye-1-l" then 
	end
	if npc_talk_index == "n1687_kaye-1-m" then 
	end
	if npc_talk_index == "n1687_kaye-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n023_ranger_fugus--------------------------------------------------------------------------------
function mqc02_9468_cataract_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n023_ranger_fugus-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n023_ranger_fugus-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n023_ranger_fugus-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n023_ranger_fugus-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n023_ranger_fugus-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n023_ranger_fugus-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n023_ranger_fugus-2-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300012, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300012, api_quest_HasQuestItem( pRoom, userObjID, 300012, 1));
				end
	end
	if npc_talk_index == "n023_ranger_fugus-2-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n023_ranger_fugus-4-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-c" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-d" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-e" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-f" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-g" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-h" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-i" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-j" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-k" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-l" then 
	end
	if npc_talk_index == "n023_ranger_fugus-4-m" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 350, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300015, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end
	if npc_talk_index == "n023_ranger_fugus-6-b" then 
	end
	if npc_talk_index == "n023_ranger_fugus-6-c" then 
	end
	if npc_talk_index == "n023_ranger_fugus-6-d" then 
	end
	if npc_talk_index == "n023_ranger_fugus-6-e" then 
	end
	if npc_talk_index == "n023_ranger_fugus-6-f" then 
	end
	if npc_talk_index == "n023_ranger_fugus-6-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94680, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94680, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94680, questID, 1);
			 end 
	end
	if npc_talk_index == "n023_ranger_fugus-6-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9469, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9469, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9469, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300089, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300089, api_quest_HasQuestItem( pRoom, userObjID, 300089, 1));
				end
	end
	if npc_talk_index == "n023_ranger_fugus-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n023_ranger_fugus-1", "mqc02_9469_for_her.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mqc02_9468_cataract_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-g" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-h" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-i" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-j" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-k" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-l" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-m" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-n" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-o" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-p" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-q" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-r" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc02_9468_cataract_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9468);
	if qstep == 5 and CountIndex == 350 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				 local TableItem = { 
				 { 300089, 1 }  , 
				 { 300015, 1 }  
 				} 
				if api_quest_CheckQuestInvenForAddItemList( pRoom, userObjID, TableItem) == 1 then
						api_quest_AddQuestItem( pRoom, userObjID, 300089, 1, questID);
						api_quest_AddQuestItem( pRoom, userObjID, 300015, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 5 and CountIndex == 300015 then

	end
end

function mqc02_9468_cataract_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9468);
	if qstep == 5 and CountIndex == 350 and Count >= TargetCount  then

	end
	if qstep == 5 and CountIndex == 300015 and Count >= TargetCount  then

	end
end

function mqc02_9468_cataract_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9468);
	local questID=9468;
end

function mqc02_9468_cataract_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc02_9468_cataract_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc02_9468_cataract_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9468, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9468, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9468, 1);
				npc_talk_index = "n1686_eltia-1";
end

</GameServer>