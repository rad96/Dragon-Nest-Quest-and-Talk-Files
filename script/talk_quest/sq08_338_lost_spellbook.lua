<VillageServer>

function sq08_338_lost_spellbook_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 35 then
		sq08_338_lost_spellbook_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_338_lost_spellbook_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3381, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3382, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3383, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3384, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3385, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3386, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3387, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3387, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3387, false);
			 end 

	end
	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest(userObjID,338, 1);
				api_quest_SetJournalStep(userObjID,338, 1);
				api_quest_SetQuestStep(userObjID,338, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 324, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200324, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400166, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-2-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3381, true);
				 api_quest_RewardQuestUser(userObjID, 3381, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3382, true);
				 api_quest_RewardQuestUser(userObjID, 3382, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3383, true);
				 api_quest_RewardQuestUser(userObjID, 3383, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3384, true);
				 api_quest_RewardQuestUser(userObjID, 3384, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3385, true);
				 api_quest_RewardQuestUser(userObjID, 3385, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3386, true);
				 api_quest_RewardQuestUser(userObjID, 3386, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3387, true);
				 api_quest_RewardQuestUser(userObjID, 3387, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3387, true);
				 api_quest_RewardQuestUser(userObjID, 3387, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3387, true);
				 api_quest_RewardQuestUser(userObjID, 3387, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-d" then 

				if api_quest_HasQuestItem(userObjID, 400166, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400166, api_quest_HasQuestItem(userObjID, 400166, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_338_lost_spellbook_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 338);
	if qstep == 1 and CountIndex == 324 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400166, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 400166 then

	end
	if qstep == 1 and CountIndex == 200324 then
				if math.random(1,1000) <= 200 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400166, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

				else
				end

	end
end

function sq08_338_lost_spellbook_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 338);
	if qstep == 1 and CountIndex == 324 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400166 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200324 and Count >= TargetCount  then

	end
end

function sq08_338_lost_spellbook_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 338);
	local questID=338;
end

function sq08_338_lost_spellbook_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 324, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200324, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400166, 1);
end

function sq08_338_lost_spellbook_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_338_lost_spellbook_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,338, 1);
				api_quest_SetJournalStep(userObjID,338, 1);
				api_quest_SetQuestStep(userObjID,338, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 324, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200324, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400166, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</VillageServer>

<GameServer>
function sq08_338_lost_spellbook_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 35 then
		sq08_338_lost_spellbook_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_338_lost_spellbook_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3381, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3382, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3383, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3384, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3385, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3386, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3387, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3387, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3387, false);
			 end 

	end
	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,338, 1);
				api_quest_SetJournalStep( pRoom, userObjID,338, 1);
				api_quest_SetQuestStep( pRoom, userObjID,338, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 324, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200324, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400166, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-2-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3381, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3381, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3382, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3382, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3383, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3383, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3384, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3384, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3385, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3385, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3386, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3386, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3387, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3387, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3387, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3387, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3387, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3387, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-2-d" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400166, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400166, api_quest_HasQuestItem( pRoom, userObjID, 400166, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_338_lost_spellbook_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 338);
	if qstep == 1 and CountIndex == 324 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400166, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 400166 then

	end
	if qstep == 1 and CountIndex == 200324 then
				if math.random(1,1000) <= 200 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400166, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400166, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

				else
				end

	end
end

function sq08_338_lost_spellbook_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 338);
	if qstep == 1 and CountIndex == 324 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400166 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200324 and Count >= TargetCount  then

	end
end

function sq08_338_lost_spellbook_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 338);
	local questID=338;
end

function sq08_338_lost_spellbook_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 324, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200324, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400166, 1);
end

function sq08_338_lost_spellbook_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_338_lost_spellbook_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,338, 1);
				api_quest_SetJournalStep( pRoom, userObjID,338, 1);
				api_quest_SetQuestStep( pRoom, userObjID,338, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 324, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200324, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400166, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</GameServer>