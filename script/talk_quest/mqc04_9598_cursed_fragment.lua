<VillageServer>

function mqc04_9598_cursed_fragment_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 212 then
		mqc04_9598_cursed_fragment_OnTalk_n212_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mqc04_9598_cursed_fragment_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n212_argenta--------------------------------------------------------------------------------
function mqc04_9598_cursed_fragment_OnTalk_n212_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n212_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n212_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n212_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n212_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n212_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n212_argenta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9598, 2);
				api_quest_SetJournalStep(userObjID,9598, 1);
				api_quest_SetQuestStep(userObjID,9598, 1);
				npc_talk_index = "n212_argenta-1";

	end
	if npc_talk_index == "n212_argenta-1-b" then 
	end
	if npc_talk_index == "n212_argenta-1-c" then 
	end
	if npc_talk_index == "n212_argenta-1-d" then 
	end
	if npc_talk_index == "n212_argenta-1-e" then 
	end
	if npc_talk_index == "n212_argenta-1-f" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300020, 10);
	end
	if npc_talk_index == "n212_argenta-3-b" then 
	end
	if npc_talk_index == "n212_argenta-3-c" then 
	end
	if npc_talk_index == "n212_argenta-3-d" then 
	end
	if npc_talk_index == "n212_argenta-3-e" then 
	end
	if npc_talk_index == "n212_argenta-3-f" then 
	end
	if npc_talk_index == "n212_argenta-3-g" then 
	end
	if npc_talk_index == "n212_argenta-3-h" then 
	end
	if npc_talk_index == "n212_argenta-3-i" then 
	end
	if npc_talk_index == "n212_argenta-3-j" then 
	end
	if npc_talk_index == "n212_argenta-3-k" then 
	end
	if npc_talk_index == "n212_argenta-3-l" then 
	end
	if npc_talk_index == "n212_argenta-3-m" then 
	end
	if npc_talk_index == "n212_argenta-3-n" then 

				if api_quest_HasQuestItem(userObjID, 300020, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300020, api_quest_HasQuestItem(userObjID, 300020, 1));
				end
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc04_9598_cursed_fragment_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n039_bishop_ignasio-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n039_bishop_ignasio-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-4-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-h" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-i" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-j" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-k" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-l" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-m" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95980, true);
				 api_quest_RewardQuestUser(userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95980, true);
				 api_quest_RewardQuestUser(userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95980, true);
				 api_quest_RewardQuestUser(userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95980, true);
				 api_quest_RewardQuestUser(userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95980, true);
				 api_quest_RewardQuestUser(userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95980, true);
				 api_quest_RewardQuestUser(userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95980, true);
				 api_quest_RewardQuestUser(userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95980, true);
				 api_quest_RewardQuestUser(userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95980, true);
				 api_quest_RewardQuestUser(userObjID, 95980, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-n" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 407, 2);
					api_quest_SetQuestStep(userObjID, 407, 1);
					api_quest_SetJournalStep(userObjID, 407, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-4-o" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n039_bishop_ignasio-1", "mq11_407_the_zero_hour.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc04_9598_cursed_fragment_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9598);
	if qstep == 2 and CountIndex == 300020 then

	end
end

function mqc04_9598_cursed_fragment_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9598);
	if qstep == 2 and CountIndex == 300020 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function mqc04_9598_cursed_fragment_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9598);
	local questID=9598;
end

function mqc04_9598_cursed_fragment_OnRemoteStart( userObjID, questID )
end

function mqc04_9598_cursed_fragment_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc04_9598_cursed_fragment_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9598, 2);
				api_quest_SetJournalStep(userObjID,9598, 1);
				api_quest_SetQuestStep(userObjID,9598, 1);
				npc_talk_index = "n212_argenta-1";
end

</VillageServer>

<GameServer>
function mqc04_9598_cursed_fragment_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 212 then
		mqc04_9598_cursed_fragment_OnTalk_n212_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mqc04_9598_cursed_fragment_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n212_argenta--------------------------------------------------------------------------------
function mqc04_9598_cursed_fragment_OnTalk_n212_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n212_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n212_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n212_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n212_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n212_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n212_argenta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9598, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9598, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9598, 1);
				npc_talk_index = "n212_argenta-1";

	end
	if npc_talk_index == "n212_argenta-1-b" then 
	end
	if npc_talk_index == "n212_argenta-1-c" then 
	end
	if npc_talk_index == "n212_argenta-1-d" then 
	end
	if npc_talk_index == "n212_argenta-1-e" then 
	end
	if npc_talk_index == "n212_argenta-1-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300020, 10);
	end
	if npc_talk_index == "n212_argenta-3-b" then 
	end
	if npc_talk_index == "n212_argenta-3-c" then 
	end
	if npc_talk_index == "n212_argenta-3-d" then 
	end
	if npc_talk_index == "n212_argenta-3-e" then 
	end
	if npc_talk_index == "n212_argenta-3-f" then 
	end
	if npc_talk_index == "n212_argenta-3-g" then 
	end
	if npc_talk_index == "n212_argenta-3-h" then 
	end
	if npc_talk_index == "n212_argenta-3-i" then 
	end
	if npc_talk_index == "n212_argenta-3-j" then 
	end
	if npc_talk_index == "n212_argenta-3-k" then 
	end
	if npc_talk_index == "n212_argenta-3-l" then 
	end
	if npc_talk_index == "n212_argenta-3-m" then 
	end
	if npc_talk_index == "n212_argenta-3-n" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300020, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300020, api_quest_HasQuestItem( pRoom, userObjID, 300020, 1));
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mqc04_9598_cursed_fragment_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n039_bishop_ignasio-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n039_bishop_ignasio-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-4-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-g" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-h" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-i" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-j" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-k" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-l" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-m" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95980, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-n" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 407, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 407, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 407, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n039_bishop_ignasio-4-o" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-1", "mq11_407_the_zero_hour.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc04_9598_cursed_fragment_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9598);
	if qstep == 2 and CountIndex == 300020 then

	end
end

function mqc04_9598_cursed_fragment_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9598);
	if qstep == 2 and CountIndex == 300020 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function mqc04_9598_cursed_fragment_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9598);
	local questID=9598;
end

function mqc04_9598_cursed_fragment_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc04_9598_cursed_fragment_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc04_9598_cursed_fragment_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9598, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9598, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9598, 1);
				npc_talk_index = "n212_argenta-1";
end

</GameServer>