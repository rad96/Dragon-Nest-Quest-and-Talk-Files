<VillageServer>

function sq15_754_as_you_wish_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 199 then
		sq15_754_as_you_wish_OnTalk_n199_darklair_castia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 458 then
		sq15_754_as_you_wish_OnTalk_n458_sword_of_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 460 then
		sq15_754_as_you_wish_OnTalk_n460_eclair(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 714 then
		sq15_754_as_you_wish_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n199_darklair_castia--------------------------------------------------------------------------------
function sq15_754_as_you_wish_OnTalk_n199_darklair_castia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n199_darklair_castia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n199_darklair_castia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n199_darklair_castia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n199_darklair_castia-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n199_darklair_castia-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n199_darklair_castia-accepting-d" then
				if api_user_GetUserJobID(userObjID) == 41   or api_user_GetUserJobID(userObjID) == 42   or api_user_GetUserJobID(userObjID) == 43   or api_user_GetUserJobID(userObjID) == 44  then
									npc_talk_index = "n199_darklair_castia-accepting-f";

				else
									npc_talk_index = "n199_darklair_castia-accepting-d";

				end

	end
	if npc_talk_index == "n199_darklair_castia-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 end 

	end
	if npc_talk_index == "n199_darklair_castia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,754, 1);
				api_quest_SetJournalStep(userObjID,754, 1);
				api_quest_SetQuestStep(userObjID,754, 1);
				npc_talk_index = "n199_darklair_castia-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300447, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300447, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n199_darklair_castia-accepting-g" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7540, false);
			 end 

	end
	if npc_talk_index == "n199_darklair_castia-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,754, 1);
				api_quest_SetJournalStep(userObjID,754, 1);
				api_quest_SetQuestStep(userObjID,754, 1);
				npc_talk_index = "9_darklair_castia-7";
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 7);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300447, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300447, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n199_darklair_castia-7-b" then 
	end
	if npc_talk_index == "n199_darklair_castia-7-b" then 
	end
	if npc_talk_index == "n199_darklair_castia-7-c" then 
	end
	if npc_talk_index == "n199_darklair_castia-7-d" then 
	end
	if npc_talk_index == "n199_darklair_castia-7-e" then 
				api_quest_SetQuestStep(userObjID, questID,8);
	end
	if npc_talk_index == "n199_darklair_castia-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n458_sword_of_geraint--------------------------------------------------------------------------------
function sq15_754_as_you_wish_OnTalk_n458_sword_of_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n458_sword_of_geraint-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n458_sword_of_geraint-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n458_sword_of_geraint-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n458_sword_of_geraint-1-levelcheck" then 
				if api_user_GetUserLevel(userObjID) >= 45 then
									if api_user_GetUserJobID(userObjID) == 20   or api_user_GetUserJobID(userObjID) == 22  then
									npc_talk_index = "n458_sword_of_geraint-1-c";

				else
									npc_talk_index = "n458_sword_of_geraint-1-a";

				end

				else
									npc_talk_index = "n458_sword_of_geraint-1-a";

				end
	end
	if npc_talk_index == "n458_sword_of_geraint-1-partyc11" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n458_sword_of_geraint-1-d";

				else
									npc_talk_index = "n458_sword_of_geraint-1-b";

				end
	end
	if npc_talk_index == "n458_sword_of_geraint-1-move1" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_user_ChangeMap(userObjID,13512,1);
	end
	if npc_talk_index == "n458_sword_of_geraint-2-party222" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n458_sword_of_geraint-2-a";

				else
									npc_talk_index = "n458_sword_of_geraint-2-b";

				end
	end
	if npc_talk_index == "n458_sword_of_geraint-2-movest2" then 
				api_user_ChangeMap(userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n460_eclair--------------------------------------------------------------------------------
function sq15_754_as_you_wish_OnTalk_n460_eclair(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n460_eclair-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n460_eclair-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n460_eclair-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n460_eclair-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n460_eclair-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n460_eclair-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n460_eclair-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n460_eclair-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n460_eclair-2-b" then 
	end
	if npc_talk_index == "n460_eclair-2-c" then 
	end
	if npc_talk_index == "n460_eclair-2-c" then 
	end
	if npc_talk_index == "n460_eclair-2-d" then 
	end
	if npc_talk_index == "n460_eclair-2-e" then 
	end
	if npc_talk_index == "n460_eclair-2-f" then 
	end
	if npc_talk_index == "n460_eclair-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n460_eclair-3-i" then 
	end
	if npc_talk_index == "n460_eclair-3-class2" then 
				if api_user_GetUserJobID(userObjID) == 20  then
									npc_talk_index = "n460_eclair-3-c";

				else
									npc_talk_index = "n460_eclair-3-f";

				end
	end
	if npc_talk_index == "n460_eclair-3-d" then 
	end
	if npc_talk_index == "n460_eclair-3-e" then 
	end
	if npc_talk_index == "n460_eclair-3-c" then 
	end
	if npc_talk_index == "n460_eclair-3-guardian1" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( userObjID, 41 );
	end
	if npc_talk_index == "n460_eclair-3-c" then 
	end
	if npc_talk_index == "n460_eclair-3-crusader1" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( userObjID, 42 );
	end
	if npc_talk_index == "n460_eclair-3-g" then 
	end
	if npc_talk_index == "n460_eclair-3-h" then 
	end
	if npc_talk_index == "n460_eclair-3-f" then 
	end
	if npc_talk_index == "n460_eclair-3-saint1" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( userObjID, 43 );
	end
	if npc_talk_index == "n460_eclair-3-f" then 
	end
	if npc_talk_index == "n460_eclair-3-inquisitor1" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( userObjID, 44 );
	end
	if npc_talk_index == "n460_eclair-3-j" then 
	end
	if npc_talk_index == "n460_eclair-3-open_ui3" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n460_eclair-5-b" then 
	end
	if npc_talk_index == "n460_eclair-5-class3" then 
				if api_user_GetUserJobID(userObjID) == 20  then
									npc_talk_index = "n460_eclair-5-d";

				else
									npc_talk_index = "n460_eclair-5-e";

				end
	end
	if npc_talk_index == "n460_eclair-5-c" then 
	end
	if npc_talk_index == "n460_eclair-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n460_eclair-5-guardian2" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( userObjID, 41 );
	end
	if npc_talk_index == "n460_eclair-5-crusader2" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( userObjID, 42 );
	end
	if npc_talk_index == "n460_eclair-5-b" then 
	end
	if npc_talk_index == "n460_eclair-5-saint2" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( userObjID, 43 );
	end
	if npc_talk_index == "n460_eclair-5-inqui2" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( userObjID, 44 );
	end
	if npc_talk_index == "n460_eclair-5-b" then 
	end
	if npc_talk_index == "n460_eclair-5-g" then 
	end
	if npc_talk_index == "n460_eclair-5-h" then 
	end
	if npc_talk_index == "n460_eclair-5-open_ui5" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n460_eclair-6-b" then 
	end
	if npc_talk_index == "n460_eclair-6-c" then 
	end
	if npc_talk_index == "n460_eclair-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 7);

				if api_quest_HasQuestItem(userObjID, 300447, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300447, api_quest_HasQuestItem(userObjID, 300447, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n714_cleric_yohan--------------------------------------------------------------------------------
function sq15_754_as_you_wish_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n714_cleric_yohan-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-8-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-chg_guardian" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 41);
				npc_talk_index = "n714_cleric_yohan-8-h";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n714_cleric_yohan-8-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-chg_crusader" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n714_cleric_yohan-8-h";
				api_user_SetUserJobID(userObjID, 42);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n714_cleric_yohan-8-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-chg_saint" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n714_cleric_yohan-8-h";
				api_user_SetUserJobID(userObjID, 43);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n714_cleric_yohan-8-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-chg_inqui" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n714_cleric_yohan-8-h";
				api_user_SetUserJobID(userObjID, 44);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n714_cleric_yohan-8-j" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7540, true);
				 api_quest_RewardQuestUser(userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-l" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n714_cleric_yohan-8-h" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_754_as_you_wish_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 754);
end

function sq15_754_as_you_wish_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 754);
end

function sq15_754_as_you_wish_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 754);
	local questID=754;
end

function sq15_754_as_you_wish_OnRemoteStart( userObjID, questID )
end

function sq15_754_as_you_wish_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_754_as_you_wish_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,754, 1);
				api_quest_SetJournalStep(userObjID,754, 1);
				api_quest_SetQuestStep(userObjID,754, 1);
				npc_talk_index = "n199_darklair_castia-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300447, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300447, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
end

</VillageServer>

<GameServer>
function sq15_754_as_you_wish_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 199 then
		sq15_754_as_you_wish_OnTalk_n199_darklair_castia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 458 then
		sq15_754_as_you_wish_OnTalk_n458_sword_of_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 460 then
		sq15_754_as_you_wish_OnTalk_n460_eclair( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 714 then
		sq15_754_as_you_wish_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n199_darklair_castia--------------------------------------------------------------------------------
function sq15_754_as_you_wish_OnTalk_n199_darklair_castia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n199_darklair_castia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n199_darklair_castia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n199_darklair_castia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n199_darklair_castia-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n199_darklair_castia-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n199_darklair_castia-accepting-d" then
				if api_user_GetUserJobID( pRoom, userObjID) == 41   or api_user_GetUserJobID( pRoom, userObjID) == 42   or api_user_GetUserJobID( pRoom, userObjID) == 43   or api_user_GetUserJobID( pRoom, userObjID) == 44  then
									npc_talk_index = "n199_darklair_castia-accepting-f";

				else
									npc_talk_index = "n199_darklair_castia-accepting-d";

				end

	end
	if npc_talk_index == "n199_darklair_castia-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 end 

	end
	if npc_talk_index == "n199_darklair_castia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,754, 1);
				api_quest_SetJournalStep( pRoom, userObjID,754, 1);
				api_quest_SetQuestStep( pRoom, userObjID,754, 1);
				npc_talk_index = "n199_darklair_castia-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300447, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300447, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n199_darklair_castia-accepting-g" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, false);
			 end 

	end
	if npc_talk_index == "n199_darklair_castia-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,754, 1);
				api_quest_SetJournalStep( pRoom, userObjID,754, 1);
				api_quest_SetQuestStep( pRoom, userObjID,754, 1);
				npc_talk_index = "9_darklair_castia-7";
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300447, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300447, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n199_darklair_castia-7-b" then 
	end
	if npc_talk_index == "n199_darklair_castia-7-b" then 
	end
	if npc_talk_index == "n199_darklair_castia-7-c" then 
	end
	if npc_talk_index == "n199_darklair_castia-7-d" then 
	end
	if npc_talk_index == "n199_darklair_castia-7-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
	end
	if npc_talk_index == "n199_darklair_castia-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n458_sword_of_geraint--------------------------------------------------------------------------------
function sq15_754_as_you_wish_OnTalk_n458_sword_of_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n458_sword_of_geraint-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n458_sword_of_geraint-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n458_sword_of_geraint-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n458_sword_of_geraint-1-levelcheck" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 45 then
									if api_user_GetUserJobID( pRoom, userObjID) == 20   or api_user_GetUserJobID( pRoom, userObjID) == 22  then
									npc_talk_index = "n458_sword_of_geraint-1-c";

				else
									npc_talk_index = "n458_sword_of_geraint-1-a";

				end

				else
									npc_talk_index = "n458_sword_of_geraint-1-a";

				end
	end
	if npc_talk_index == "n458_sword_of_geraint-1-partyc11" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n458_sword_of_geraint-1-d";

				else
									npc_talk_index = "n458_sword_of_geraint-1-b";

				end
	end
	if npc_talk_index == "n458_sword_of_geraint-1-move1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end
	if npc_talk_index == "n458_sword_of_geraint-2-party222" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n458_sword_of_geraint-2-a";

				else
									npc_talk_index = "n458_sword_of_geraint-2-b";

				end
	end
	if npc_talk_index == "n458_sword_of_geraint-2-movest2" then 
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n460_eclair--------------------------------------------------------------------------------
function sq15_754_as_you_wish_OnTalk_n460_eclair( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n460_eclair-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n460_eclair-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n460_eclair-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n460_eclair-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n460_eclair-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n460_eclair-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n460_eclair-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n460_eclair-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n460_eclair-2-b" then 
	end
	if npc_talk_index == "n460_eclair-2-c" then 
	end
	if npc_talk_index == "n460_eclair-2-c" then 
	end
	if npc_talk_index == "n460_eclair-2-d" then 
	end
	if npc_talk_index == "n460_eclair-2-e" then 
	end
	if npc_talk_index == "n460_eclair-2-f" then 
	end
	if npc_talk_index == "n460_eclair-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n460_eclair-3-i" then 
	end
	if npc_talk_index == "n460_eclair-3-class2" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 20  then
									npc_talk_index = "n460_eclair-3-c";

				else
									npc_talk_index = "n460_eclair-3-f";

				end
	end
	if npc_talk_index == "n460_eclair-3-d" then 
	end
	if npc_talk_index == "n460_eclair-3-e" then 
	end
	if npc_talk_index == "n460_eclair-3-c" then 
	end
	if npc_talk_index == "n460_eclair-3-guardian1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 41 );
	end
	if npc_talk_index == "n460_eclair-3-c" then 
	end
	if npc_talk_index == "n460_eclair-3-crusader1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 42 );
	end
	if npc_talk_index == "n460_eclair-3-g" then 
	end
	if npc_talk_index == "n460_eclair-3-h" then 
	end
	if npc_talk_index == "n460_eclair-3-f" then 
	end
	if npc_talk_index == "n460_eclair-3-saint1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 43 );
	end
	if npc_talk_index == "n460_eclair-3-f" then 
	end
	if npc_talk_index == "n460_eclair-3-inquisitor1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 44 );
	end
	if npc_talk_index == "n460_eclair-3-j" then 
	end
	if npc_talk_index == "n460_eclair-3-open_ui3" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n460_eclair-5-b" then 
	end
	if npc_talk_index == "n460_eclair-5-class3" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 20  then
									npc_talk_index = "n460_eclair-5-d";

				else
									npc_talk_index = "n460_eclair-5-e";

				end
	end
	if npc_talk_index == "n460_eclair-5-c" then 
	end
	if npc_talk_index == "n460_eclair-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n460_eclair-5-guardian2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 41 );
	end
	if npc_talk_index == "n460_eclair-5-crusader2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 42 );
	end
	if npc_talk_index == "n460_eclair-5-b" then 
	end
	if npc_talk_index == "n460_eclair-5-saint2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 43 );
	end
	if npc_talk_index == "n460_eclair-5-inqui2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n460_eclair-4";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 44 );
	end
	if npc_talk_index == "n460_eclair-5-b" then 
	end
	if npc_talk_index == "n460_eclair-5-g" then 
	end
	if npc_talk_index == "n460_eclair-5-h" then 
	end
	if npc_talk_index == "n460_eclair-5-open_ui5" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n460_eclair-6-b" then 
	end
	if npc_talk_index == "n460_eclair-6-c" then 
	end
	if npc_talk_index == "n460_eclair-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);

				if api_quest_HasQuestItem( pRoom, userObjID, 300447, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300447, api_quest_HasQuestItem( pRoom, userObjID, 300447, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n714_cleric_yohan--------------------------------------------------------------------------------
function sq15_754_as_you_wish_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n714_cleric_yohan-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-8-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-chg_guardian" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 41);
				npc_talk_index = "n714_cleric_yohan-8-h";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n714_cleric_yohan-8-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-chg_crusader" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n714_cleric_yohan-8-h";
				api_user_SetUserJobID( pRoom, userObjID, 42);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n714_cleric_yohan-8-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-chg_saint" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n714_cleric_yohan-8-h";
				api_user_SetUserJobID( pRoom, userObjID, 43);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n714_cleric_yohan-8-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-chg_inqui" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									npc_talk_index = "n714_cleric_yohan-8-h";
				api_user_SetUserJobID( pRoom, userObjID, 44);


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n714_cleric_yohan-8-j" then 
	end
	if npc_talk_index == "n714_cleric_yohan-8-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7540, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7540, questID, 1);
			 end 
	end
	if npc_talk_index == "n714_cleric_yohan-8-l" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n714_cleric_yohan-8-h" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_754_as_you_wish_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 754);
end

function sq15_754_as_you_wish_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 754);
end

function sq15_754_as_you_wish_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 754);
	local questID=754;
end

function sq15_754_as_you_wish_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_754_as_you_wish_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_754_as_you_wish_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,754, 1);
				api_quest_SetJournalStep( pRoom, userObjID,754, 1);
				api_quest_SetQuestStep( pRoom, userObjID,754, 1);
				npc_talk_index = "n199_darklair_castia-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300447, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300447, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
end

</GameServer>