<VillageServer>

function sqc16_4696_red_twilight_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 822 then
		sqc16_4696_red_twilight_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 956 then
		sqc16_4696_red_twilight_OnTalk_n956_dragon_buff(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 999 then
		sqc16_4696_red_twilight_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n822_teramai--------------------------------------------------------------------------------
function sqc16_4696_red_twilight_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-1-b" then 
	end
	if npc_talk_index == "n822_teramai-1-c" then 
	end
	if npc_talk_index == "n822_teramai-1-d" then 
	end
	if npc_talk_index == "n822_teramai-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n956_dragon_buff--------------------------------------------------------------------------------
function sqc16_4696_red_twilight_OnTalk_n956_dragon_buff(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n956_dragon_buff-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n956_dragon_buff-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n956_dragon_buff-2-buffcheck" then 
				if api_quest_WorldBuffCheck(268457378) == 1  then
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46960, true);
				 api_quest_RewardQuestUser(userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46960, true);
				 api_quest_RewardQuestUser(userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46960, true);
				 api_quest_RewardQuestUser(userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46960, true);
				 api_quest_RewardQuestUser(userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46960, true);
				 api_quest_RewardQuestUser(userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46960, true);
				 api_quest_RewardQuestUser(userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46960, true);
				 api_quest_RewardQuestUser(userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46960, true);
				 api_quest_RewardQuestUser(userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46960, true);
				 api_quest_RewardQuestUser(userObjID, 46960, questID, 1);
			 end 
				npc_talk_index = "n956_dragon_buff-2-a";

				else
									npc_talk_index = "n956_dragon_buff-2-b";

				end
	end
	if npc_talk_index == "n956_dragon_buff-2-buffcheck2" then 
				if api_quest_WorldBuffCheck(268457378) == 1  then
									npc_talk_index = "n956_dragon_buff-2-c";

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_quest_ApplyWorldBuff(userObjID, 268457378, 11);


				else
					npc_talk_index = "_full_inventory";

				end

				else
									npc_talk_index = "n956_dragon_buff-2-b";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n999_remote--------------------------------------------------------------------------------
function sqc16_4696_red_twilight_OnTalk_n999_remote(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n999_remote-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n999_remote-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n999_remote-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4696, 1);
				api_quest_SetJournalStep(userObjID,4696, 1);
				api_quest_SetQuestStep(userObjID,4696, 1);
				api_quest_SetQuestStep(userObjID, questID,1);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4696_red_twilight_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4696);
end

function sqc16_4696_red_twilight_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4696);
end

function sqc16_4696_red_twilight_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4696);
	local questID=4696;
end

function sqc16_4696_red_twilight_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,1);
end

function sqc16_4696_red_twilight_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc16_4696_red_twilight_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4696, 1);
				api_quest_SetJournalStep(userObjID,4696, 1);
				api_quest_SetQuestStep(userObjID,4696, 1);
				api_quest_SetQuestStep(userObjID, questID,1);
end

</VillageServer>

<GameServer>
function sqc16_4696_red_twilight_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 822 then
		sqc16_4696_red_twilight_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 956 then
		sqc16_4696_red_twilight_OnTalk_n956_dragon_buff( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 999 then
		sqc16_4696_red_twilight_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n822_teramai--------------------------------------------------------------------------------
function sqc16_4696_red_twilight_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-1-b" then 
	end
	if npc_talk_index == "n822_teramai-1-c" then 
	end
	if npc_talk_index == "n822_teramai-1-d" then 
	end
	if npc_talk_index == "n822_teramai-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n956_dragon_buff--------------------------------------------------------------------------------
function sqc16_4696_red_twilight_OnTalk_n956_dragon_buff( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n956_dragon_buff-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n956_dragon_buff-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n956_dragon_buff-2-buffcheck" then 
				if api_quest_WorldBuffCheck( pRoom, 268457378) == 1  then
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46960, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46960, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46960, questID, 1);
			 end 
				npc_talk_index = "n956_dragon_buff-2-a";

				else
									npc_talk_index = "n956_dragon_buff-2-b";

				end
	end
	if npc_talk_index == "n956_dragon_buff-2-buffcheck2" then 
				if api_quest_WorldBuffCheck( pRoom, 268457378) == 1  then
									npc_talk_index = "n956_dragon_buff-2-c";

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_quest_ApplyWorldBuff( pRoom, userObjID, 268457378, 11);


				else
					npc_talk_index = "_full_inventory";

				end

				else
									npc_talk_index = "n956_dragon_buff-2-b";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n999_remote--------------------------------------------------------------------------------
function sqc16_4696_red_twilight_OnTalk_n999_remote( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n999_remote-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n999_remote-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n999_remote-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4696, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4696, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4696, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4696_red_twilight_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4696);
end

function sqc16_4696_red_twilight_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4696);
end

function sqc16_4696_red_twilight_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4696);
	local questID=4696;
end

function sqc16_4696_red_twilight_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
end

function sqc16_4696_red_twilight_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc16_4696_red_twilight_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4696, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4696, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4696, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
end

</GameServer>