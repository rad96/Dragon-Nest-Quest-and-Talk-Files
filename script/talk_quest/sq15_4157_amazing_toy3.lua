<VillageServer>

function sq15_4157_amazing_toy3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 252 then
		sq15_4157_amazing_toy3_OnTalk_n252_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		sq15_4157_amazing_toy3_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n252_saint_guard--------------------------------------------------------------------------------
function sq15_4157_amazing_toy3_OnTalk_n252_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n252_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n252_saint_guard-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n252_saint_guard-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41570, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41570, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41570, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41570, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41570, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41570, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41570, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41570, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41570, false);
			 end 

	end
	if npc_talk_index == "n252_saint_guard-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4157, 1);
				api_quest_SetJournalStep(userObjID,4157, 1);
				api_quest_SetQuestStep(userObjID,4157, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400362, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700607, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700608, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 600607, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 600608, 30000);
				npc_talk_index = "n252_saint_guard-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function sq15_4157_amazing_toy3_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n041_duke_stwart-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-2-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-e" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-g" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-h" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41570, true);
				 api_quest_RewardQuestUser(userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41570, true);
				 api_quest_RewardQuestUser(userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41570, true);
				 api_quest_RewardQuestUser(userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41570, true);
				 api_quest_RewardQuestUser(userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41570, true);
				 api_quest_RewardQuestUser(userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41570, true);
				 api_quest_RewardQuestUser(userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41570, true);
				 api_quest_RewardQuestUser(userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41570, true);
				 api_quest_RewardQuestUser(userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41570, true);
				 api_quest_RewardQuestUser(userObjID, 41570, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-2-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400362, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400362, api_quest_HasQuestItem(userObjID, 400362, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4157_amazing_toy3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4157);
	if qstep == 1 and CountIndex == 400362 then

	end
	if qstep == 1 and CountIndex == 700607 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400362, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400362, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700608 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400362, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400362, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600607 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400362, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400362, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600608 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400362, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400362, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4157_amazing_toy3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4157);
	if qstep == 1 and CountIndex == 400362 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 700607 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700608 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600607 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600608 and Count >= TargetCount  then

	end
end

function sq15_4157_amazing_toy3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4157);
	local questID=4157;
end

function sq15_4157_amazing_toy3_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400362, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700607, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700608, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 600607, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 600608, 30000);
end

function sq15_4157_amazing_toy3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4157_amazing_toy3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4157, 1);
				api_quest_SetJournalStep(userObjID,4157, 1);
				api_quest_SetQuestStep(userObjID,4157, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400362, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700607, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 700608, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 600607, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 600608, 30000);
				npc_talk_index = "n252_saint_guard-1";
end

</VillageServer>

<GameServer>
function sq15_4157_amazing_toy3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 252 then
		sq15_4157_amazing_toy3_OnTalk_n252_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		sq15_4157_amazing_toy3_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n252_saint_guard--------------------------------------------------------------------------------
function sq15_4157_amazing_toy3_OnTalk_n252_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n252_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n252_saint_guard-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n252_saint_guard-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, false);
			 end 

	end
	if npc_talk_index == "n252_saint_guard-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4157, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4157, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4157, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400362, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700607, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700608, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 600607, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 600608, 30000);
				npc_talk_index = "n252_saint_guard-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function sq15_4157_amazing_toy3_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n041_duke_stwart-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-2-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-e" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-g" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-h" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41570, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-2-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400362, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400362, api_quest_HasQuestItem( pRoom, userObjID, 400362, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4157_amazing_toy3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4157);
	if qstep == 1 and CountIndex == 400362 then

	end
	if qstep == 1 and CountIndex == 700607 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400362, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400362, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 700608 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400362, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400362, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600607 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400362, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400362, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 600608 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400362, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400362, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq15_4157_amazing_toy3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4157);
	if qstep == 1 and CountIndex == 400362 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 700607 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 700608 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600607 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 600608 and Count >= TargetCount  then

	end
end

function sq15_4157_amazing_toy3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4157);
	local questID=4157;
end

function sq15_4157_amazing_toy3_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400362, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700607, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700608, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 600607, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 600608, 30000);
end

function sq15_4157_amazing_toy3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4157_amazing_toy3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4157, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4157, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4157, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400362, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700607, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 700608, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 600607, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 600608, 30000);
				npc_talk_index = "n252_saint_guard-1";
end

</GameServer>