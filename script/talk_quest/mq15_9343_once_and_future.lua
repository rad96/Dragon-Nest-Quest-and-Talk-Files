<VillageServer>

function mq15_9343_once_and_future_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1177 then
		mq15_9343_once_and_future_OnTalk_n1177_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1181 then
		mq15_9343_once_and_future_OnTalk_n1181_novice_man(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 713 then
		mq15_9343_once_and_future_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		mq15_9343_once_and_future_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1177_lunaria--------------------------------------------------------------------------------
function mq15_9343_once_and_future_OnTalk_n1177_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1177_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1177_lunaria-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1177_lunaria-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1177_lunaria-4-b" then 
	end
	if npc_talk_index == "n1177_lunaria-4-c" then 
	end
	if npc_talk_index == "n1177_lunaria-4-d" then 
	end
	if npc_talk_index == "n1177_lunaria-4-e" then 
	end
	if npc_talk_index == "n1177_lunaria-4-f" then 
	end
	if npc_talk_index == "n1177_lunaria-4-g" then 
	end
	if npc_talk_index == "n1177_lunaria-4-h" then 
	end
	if npc_talk_index == "n1177_lunaria-4-i" then 
	end
	if npc_talk_index == "n1177_lunaria-4-j" then 
	end
	if npc_talk_index == "n1177_lunaria-4-k" then 
	end
	if npc_talk_index == "n1177_lunaria-4-l" then 
	end
	if npc_talk_index == "n1177_lunaria-4-m" then 
	end
	if npc_talk_index == "n1177_lunaria-4-n" then 
	end
	if npc_talk_index == "n1177_lunaria-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1181_novice_man--------------------------------------------------------------------------------
function mq15_9343_once_and_future_OnTalk_n1181_novice_man(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1181_novice_man-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1181_novice_man-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1181_novice_man-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1181_novice_man-3-b" then 
	end
	if npc_talk_index == "n1181_novice_man-3-c" then 
	end
	if npc_talk_index == "n1181_novice_man-3-d" then 
	end
	if npc_talk_index == "n1181_novice_man-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n713_soceress_tamara--------------------------------------------------------------------------------
function mq15_9343_once_and_future_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9343, 2);
				api_quest_SetJournalStep(userObjID,9343, 1);
				api_quest_SetQuestStep(userObjID,9343, 1);
				npc_talk_index = "n713_soceress_tamara-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function mq15_9343_once_and_future_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n754_cian-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n754_cian-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-1-b" then 
	end
	if npc_talk_index == "n754_cian-1-c" then 
	end
	if npc_talk_index == "n754_cian-1-d" then 
	end
	if npc_talk_index == "n754_cian-1-e" then 
	end
	if npc_talk_index == "n754_cian-1-f" then 
	end
	if npc_talk_index == "n754_cian-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 22016, 30000);
	end
	if npc_talk_index == "n754_cian-5-b" then 
	end
	if npc_talk_index == "n754_cian-5-c" then 
	end
	if npc_talk_index == "n754_cian-5-d" then 
	end
	if npc_talk_index == "n754_cian-5-e" then 
	end
	if npc_talk_index == "n754_cian-5-f" then 
	end
	if npc_talk_index == "n754_cian-5-g" then 
	end
	if npc_talk_index == "n754_cian-5-h" then 
	end
	if npc_talk_index == "n754_cian-5-i" then 
	end
	if npc_talk_index == "n754_cian-5-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93430, true);
				 api_quest_RewardQuestUser(userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93430, true);
				 api_quest_RewardQuestUser(userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93430, true);
				 api_quest_RewardQuestUser(userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93430, true);
				 api_quest_RewardQuestUser(userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93430, true);
				 api_quest_RewardQuestUser(userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93430, true);
				 api_quest_RewardQuestUser(userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93430, true);
				 api_quest_RewardQuestUser(userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93430, true);
				 api_quest_RewardQuestUser(userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93430, true);
				 api_quest_RewardQuestUser(userObjID, 93430, questID, 1);
			 end 
	end
	if npc_talk_index == "n754_cian-5-k" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9344, 2);
					api_quest_SetQuestStep(userObjID, 9344, 1);
					api_quest_SetJournalStep(userObjID, 9344, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n754_cian-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n754_cian-1", "mq15_9344_irregular_time.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9343_once_and_future_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9343);
	if qstep == 2 and CountIndex == 22016 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function mq15_9343_once_and_future_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9343);
	if qstep == 2 and CountIndex == 22016 and Count >= TargetCount  then

	end
end

function mq15_9343_once_and_future_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9343);
	local questID=9343;
end

function mq15_9343_once_and_future_OnRemoteStart( userObjID, questID )
end

function mq15_9343_once_and_future_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9343_once_and_future_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9343, 2);
				api_quest_SetJournalStep(userObjID,9343, 1);
				api_quest_SetQuestStep(userObjID,9343, 1);
				npc_talk_index = "n713_soceress_tamara-1";
end

</VillageServer>

<GameServer>
function mq15_9343_once_and_future_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1177 then
		mq15_9343_once_and_future_OnTalk_n1177_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1181 then
		mq15_9343_once_and_future_OnTalk_n1181_novice_man( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 713 then
		mq15_9343_once_and_future_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		mq15_9343_once_and_future_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1177_lunaria--------------------------------------------------------------------------------
function mq15_9343_once_and_future_OnTalk_n1177_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1177_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1177_lunaria-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1177_lunaria-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1177_lunaria-4-b" then 
	end
	if npc_talk_index == "n1177_lunaria-4-c" then 
	end
	if npc_talk_index == "n1177_lunaria-4-d" then 
	end
	if npc_talk_index == "n1177_lunaria-4-e" then 
	end
	if npc_talk_index == "n1177_lunaria-4-f" then 
	end
	if npc_talk_index == "n1177_lunaria-4-g" then 
	end
	if npc_talk_index == "n1177_lunaria-4-h" then 
	end
	if npc_talk_index == "n1177_lunaria-4-i" then 
	end
	if npc_talk_index == "n1177_lunaria-4-j" then 
	end
	if npc_talk_index == "n1177_lunaria-4-k" then 
	end
	if npc_talk_index == "n1177_lunaria-4-l" then 
	end
	if npc_talk_index == "n1177_lunaria-4-m" then 
	end
	if npc_talk_index == "n1177_lunaria-4-n" then 
	end
	if npc_talk_index == "n1177_lunaria-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1181_novice_man--------------------------------------------------------------------------------
function mq15_9343_once_and_future_OnTalk_n1181_novice_man( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1181_novice_man-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1181_novice_man-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1181_novice_man-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1181_novice_man-3-b" then 
	end
	if npc_talk_index == "n1181_novice_man-3-c" then 
	end
	if npc_talk_index == "n1181_novice_man-3-d" then 
	end
	if npc_talk_index == "n1181_novice_man-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n713_soceress_tamara--------------------------------------------------------------------------------
function mq15_9343_once_and_future_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9343, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9343, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9343, 1);
				npc_talk_index = "n713_soceress_tamara-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function mq15_9343_once_and_future_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n754_cian-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n754_cian-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-1-b" then 
	end
	if npc_talk_index == "n754_cian-1-c" then 
	end
	if npc_talk_index == "n754_cian-1-d" then 
	end
	if npc_talk_index == "n754_cian-1-e" then 
	end
	if npc_talk_index == "n754_cian-1-f" then 
	end
	if npc_talk_index == "n754_cian-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 22016, 30000);
	end
	if npc_talk_index == "n754_cian-5-b" then 
	end
	if npc_talk_index == "n754_cian-5-c" then 
	end
	if npc_talk_index == "n754_cian-5-d" then 
	end
	if npc_talk_index == "n754_cian-5-e" then 
	end
	if npc_talk_index == "n754_cian-5-f" then 
	end
	if npc_talk_index == "n754_cian-5-g" then 
	end
	if npc_talk_index == "n754_cian-5-h" then 
	end
	if npc_talk_index == "n754_cian-5-i" then 
	end
	if npc_talk_index == "n754_cian-5-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93430, questID, 1);
			 end 
	end
	if npc_talk_index == "n754_cian-5-k" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9344, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9344, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9344, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n754_cian-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n754_cian-1", "mq15_9344_irregular_time.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9343_once_and_future_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9343);
	if qstep == 2 and CountIndex == 22016 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function mq15_9343_once_and_future_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9343);
	if qstep == 2 and CountIndex == 22016 and Count >= TargetCount  then

	end
end

function mq15_9343_once_and_future_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9343);
	local questID=9343;
end

function mq15_9343_once_and_future_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9343_once_and_future_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9343_once_and_future_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9343, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9343, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9343, 1);
				npc_talk_index = "n713_soceress_tamara-1";
end

</GameServer>