<VillageServer>

function mq08_195_aggravate_error_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1101 then
		mq08_195_aggravate_error_OnTalk_n1101_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		mq08_195_aggravate_error_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 553 then
		mq08_195_aggravate_error_OnTalk_n553_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1101_lunaria--------------------------------------------------------------------------------
function mq08_195_aggravate_error_OnTalk_n1101_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1101_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1101_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function mq08_195_aggravate_error_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest(userObjID,195, 2);
				api_quest_SetJournalStep(userObjID,195, 1);
				api_quest_SetQuestStep(userObjID,195, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-f" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n553_cian--------------------------------------------------------------------------------
function mq08_195_aggravate_error_OnTalk_n553_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n553_cian-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n553_cian-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n553_cian-2-b" then 
	end
	if npc_talk_index == "n553_cian-2-c" then 
	end
	if npc_talk_index == "n553_cian-2-c" then 
	end
	if npc_talk_index == "n553_cian-2-c" then 
	end
	if npc_talk_index == "n553_cian-2-d" then 
	end
	if npc_talk_index == "n553_cian-2-e" then 
	end
	if npc_talk_index == "n553_cian-2-f" then 
	end
	if npc_talk_index == "n553_cian-2-g" then 
	end
	if npc_talk_index == "n553_cian-2-h" then 
	end
	if npc_talk_index == "n553_cian-2-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1950, true);
				 api_quest_RewardQuestUser(userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1950, true);
				 api_quest_RewardQuestUser(userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1950, true);
				 api_quest_RewardQuestUser(userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1950, true);
				 api_quest_RewardQuestUser(userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1950, true);
				 api_quest_RewardQuestUser(userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1950, true);
				 api_quest_RewardQuestUser(userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1950, true);
				 api_quest_RewardQuestUser(userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1950, true);
				 api_quest_RewardQuestUser(userObjID, 1950, questID, 1);
			 end 
	end
	if npc_talk_index == "n553_cian-2-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 196, 2);
					api_quest_SetQuestStep(userObjID, 196, 1);
					api_quest_SetJournalStep(userObjID, 196, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n553_cian-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n553_cian-1", "mq08_196_stronghold.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_195_aggravate_error_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 195);
end

function mq08_195_aggravate_error_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 195);
end

function mq08_195_aggravate_error_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 195);
	local questID=195;
end

function mq08_195_aggravate_error_OnRemoteStart( userObjID, questID )
end

function mq08_195_aggravate_error_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_195_aggravate_error_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,195, 2);
				api_quest_SetJournalStep(userObjID,195, 1);
				api_quest_SetQuestStep(userObjID,195, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</VillageServer>

<GameServer>
function mq08_195_aggravate_error_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1101 then
		mq08_195_aggravate_error_OnTalk_n1101_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		mq08_195_aggravate_error_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 553 then
		mq08_195_aggravate_error_OnTalk_n553_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1101_lunaria--------------------------------------------------------------------------------
function mq08_195_aggravate_error_OnTalk_n1101_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1101_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1101_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function mq08_195_aggravate_error_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,195, 2);
				api_quest_SetJournalStep( pRoom, userObjID,195, 1);
				api_quest_SetQuestStep( pRoom, userObjID,195, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n553_cian--------------------------------------------------------------------------------
function mq08_195_aggravate_error_OnTalk_n553_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n553_cian-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n553_cian-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n553_cian-2-b" then 
	end
	if npc_talk_index == "n553_cian-2-c" then 
	end
	if npc_talk_index == "n553_cian-2-c" then 
	end
	if npc_talk_index == "n553_cian-2-c" then 
	end
	if npc_talk_index == "n553_cian-2-d" then 
	end
	if npc_talk_index == "n553_cian-2-e" then 
	end
	if npc_talk_index == "n553_cian-2-f" then 
	end
	if npc_talk_index == "n553_cian-2-g" then 
	end
	if npc_talk_index == "n553_cian-2-h" then 
	end
	if npc_talk_index == "n553_cian-2-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1950, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1950, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1950, questID, 1);
			 end 
	end
	if npc_talk_index == "n553_cian-2-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 196, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 196, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 196, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n553_cian-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n553_cian-1", "mq08_196_stronghold.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_195_aggravate_error_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 195);
end

function mq08_195_aggravate_error_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 195);
end

function mq08_195_aggravate_error_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 195);
	local questID=195;
end

function mq08_195_aggravate_error_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq08_195_aggravate_error_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_195_aggravate_error_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,195, 2);
				api_quest_SetJournalStep( pRoom, userObjID,195, 1);
				api_quest_SetQuestStep( pRoom, userObjID,195, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</GameServer>