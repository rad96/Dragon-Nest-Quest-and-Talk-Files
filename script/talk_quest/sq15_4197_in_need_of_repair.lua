<VillageServer>

function sq15_4197_in_need_of_repair_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 894 then
		sq15_4197_in_need_of_repair_OnTalk_n894_mecharoid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n894_mecharoid--------------------------------------------------------------------------------
function sq15_4197_in_need_of_repair_OnTalk_n894_mecharoid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n894_mecharoid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n894_mecharoid-1";
				api_npc_Disappoint(userObjID, 894 );
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n894_mecharoid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n894_mecharoid-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n894_mecharoid-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41970, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41970, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41970, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41970, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41970, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41970, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41970, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41970, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41970, false);
			 end 

	end
	if npc_talk_index == "n894_mecharoid-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4197, 1);
				api_quest_SetJournalStep(userObjID,4197, 1);
				api_quest_SetQuestStep(userObjID,4197, 1);
				npc_talk_index = "n894_mecharoid-1";

	end
	if npc_talk_index == "n894_mecharoid-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 600653, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 700653, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400375, 1);
	end
	if npc_talk_index == "n894_mecharoid-3-b" then 
	end
	if npc_talk_index == "n894_mecharoid-3-c" then 
	end
	if npc_talk_index == "n894_mecharoid-3-d" then 
	end
	if npc_talk_index == "n894_mecharoid-3-e" then 
	end
	if npc_talk_index == "n894_mecharoid-3-f" then 
	end
	if npc_talk_index == "n894_mecharoid-3-g" then 
	end
	if npc_talk_index == "n894_mecharoid-3-h" then 
	end
	if npc_talk_index == "n894_mecharoid-3-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41970, true);
				 api_quest_RewardQuestUser(userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41970, true);
				 api_quest_RewardQuestUser(userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41970, true);
				 api_quest_RewardQuestUser(userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41970, true);
				 api_quest_RewardQuestUser(userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41970, true);
				 api_quest_RewardQuestUser(userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41970, true);
				 api_quest_RewardQuestUser(userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41970, true);
				 api_quest_RewardQuestUser(userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41970, true);
				 api_quest_RewardQuestUser(userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41970, true);
				 api_quest_RewardQuestUser(userObjID, 41970, questID, 1);
			 end 
	end
	if npc_talk_index == "n894_mecharoid-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4198, 1);
					api_quest_SetQuestStep(userObjID, 4198, 1);
					api_quest_SetJournalStep(userObjID, 4198, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400375, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400375, api_quest_HasQuestItem(userObjID, 400375, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n894_mecharoid-1", "sq15_4198_hungry.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4197_in_need_of_repair_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4197);
	if qstep == 2 and CountIndex == 600653 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400375, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400375, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 700653 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400375, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400375, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 400375 then

	end
end

function sq15_4197_in_need_of_repair_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4197);
	if qstep == 2 and CountIndex == 600653 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700653 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400375 and Count >= TargetCount  then

	end
end

function sq15_4197_in_need_of_repair_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4197);
	local questID=4197;
end

function sq15_4197_in_need_of_repair_OnRemoteStart( userObjID, questID )
end

function sq15_4197_in_need_of_repair_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4197_in_need_of_repair_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4197, 1);
				api_quest_SetJournalStep(userObjID,4197, 1);
				api_quest_SetQuestStep(userObjID,4197, 1);
				npc_talk_index = "n894_mecharoid-1";
end

</VillageServer>

<GameServer>
function sq15_4197_in_need_of_repair_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 894 then
		sq15_4197_in_need_of_repair_OnTalk_n894_mecharoid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n894_mecharoid--------------------------------------------------------------------------------
function sq15_4197_in_need_of_repair_OnTalk_n894_mecharoid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n894_mecharoid-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n894_mecharoid-1";
				api_npc_Disappoint( pRoom, userObjID, 894 );
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n894_mecharoid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n894_mecharoid-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n894_mecharoid-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, false);
			 end 

	end
	if npc_talk_index == "n894_mecharoid-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4197, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4197, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4197, 1);
				npc_talk_index = "n894_mecharoid-1";

	end
	if npc_talk_index == "n894_mecharoid-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 600653, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 700653, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400375, 1);
	end
	if npc_talk_index == "n894_mecharoid-3-b" then 
	end
	if npc_talk_index == "n894_mecharoid-3-c" then 
	end
	if npc_talk_index == "n894_mecharoid-3-d" then 
	end
	if npc_talk_index == "n894_mecharoid-3-e" then 
	end
	if npc_talk_index == "n894_mecharoid-3-f" then 
	end
	if npc_talk_index == "n894_mecharoid-3-g" then 
	end
	if npc_talk_index == "n894_mecharoid-3-h" then 
	end
	if npc_talk_index == "n894_mecharoid-3-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41970, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41970, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41970, questID, 1);
			 end 
	end
	if npc_talk_index == "n894_mecharoid-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4198, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4198, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4198, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400375, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400375, api_quest_HasQuestItem( pRoom, userObjID, 400375, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n894_mecharoid-1", "sq15_4198_hungry.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4197_in_need_of_repair_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4197);
	if qstep == 2 and CountIndex == 600653 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400375, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400375, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 700653 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400375, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400375, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 400375 then

	end
end

function sq15_4197_in_need_of_repair_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4197);
	if qstep == 2 and CountIndex == 600653 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 700653 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400375 and Count >= TargetCount  then

	end
end

function sq15_4197_in_need_of_repair_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4197);
	local questID=4197;
end

function sq15_4197_in_need_of_repair_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4197_in_need_of_repair_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4197_in_need_of_repair_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4197, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4197, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4197, 1);
				npc_talk_index = "n894_mecharoid-1";
end

</GameServer>