<VillageServer>

function sq15_4072_new_power_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 703 then
		sq15_4072_new_power_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 710 then
		sq15_4072_new_power_OnTalk_n710_scholar_hancock(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		sq15_4072_new_power_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n703_book_doctor--------------------------------------------------------------------------------
function sq15_4072_new_power_OnTalk_n703_book_doctor(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n703_book_doctor-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n703_book_doctor-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n703_book_doctor-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n703_book_doctor-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n703_book_doctor-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40720, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40720, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40720, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40720, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40720, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40720, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40720, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40720, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40720, false);
			 end 

	end
	if npc_talk_index == "n703_book_doctor-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4072, 1);
				api_quest_SetJournalStep(userObjID,4072, 1);
				api_quest_SetQuestStep(userObjID,4072, 1);
				npc_talk_index = "n703_book_doctor-1";

	end
	if npc_talk_index == "n703_book_doctor-1-b" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 201611, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1611, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300425, 1);
	end
	if npc_talk_index == "n703_book_doctor-3-b" then 
	end
	if npc_talk_index == "n703_book_doctor-3-c" then 
	end
	if npc_talk_index == "n703_book_doctor-3-d" then 
	end
	if npc_talk_index == "n703_book_doctor-3-e" then 
	end
	if npc_talk_index == "n703_book_doctor-3-f" then 
	end
	if npc_talk_index == "n703_book_doctor-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300548, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300548, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n703_book_doctor-6-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 40720, true);
				 api_quest_RewardQuestUser(userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 40720, true);
				 api_quest_RewardQuestUser(userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 40720, true);
				 api_quest_RewardQuestUser(userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 40720, true);
				 api_quest_RewardQuestUser(userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 40720, true);
				 api_quest_RewardQuestUser(userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 40720, true);
				 api_quest_RewardQuestUser(userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 40720, true);
				 api_quest_RewardQuestUser(userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 40720, true);
				 api_quest_RewardQuestUser(userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 40720, true);
				 api_quest_RewardQuestUser(userObjID, 40720, questID, 1);
			 end 
	end
	if npc_talk_index == "n703_book_doctor-6-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4073, 1);
					api_quest_SetQuestStep(userObjID, 4073, 1);
					api_quest_SetJournalStep(userObjID, 4073, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 500459, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 500459, api_quest_HasQuestItem(userObjID, 500459, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n703_book_doctor-1", "sq15_4073_master_of_power.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n710_scholar_hancock--------------------------------------------------------------------------------
function sq15_4072_new_power_OnTalk_n710_scholar_hancock(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n710_scholar_hancock-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n710_scholar_hancock-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n710_scholar_hancock-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n710_scholar_hancock-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n710_scholar_hancock-4-b" then 
	end
	if npc_talk_index == "n710_scholar_hancock-4-c" then 
	end
	if npc_talk_index == "n710_scholar_hancock-4-d" then 
	end
	if npc_talk_index == "n710_scholar_hancock-4-e" then 
	end
	if npc_talk_index == "n710_scholar_hancock-4-f" then 
	end
	if npc_talk_index == "n710_scholar_hancock-5" then 

				if api_quest_HasQuestItem(userObjID, 300548, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300548, api_quest_HasQuestItem(userObjID, 300548, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300425, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300425, api_quest_HasQuestItem(userObjID, 300425, 1));
				end
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300549, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300549, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function sq15_4072_new_power_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n754_cian-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n754_cian-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n754_cian-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-5-b" then 
	end
	if npc_talk_index == "n754_cian-5-c" then 
	end
	if npc_talk_index == "n754_cian-5-d" then 
	end
	if npc_talk_index == "n754_cian-5-e" then 
	end
	if npc_talk_index == "n754_cian-5-f" then 
	end
	if npc_talk_index == "n754_cian-5-g" then 
	end
	if npc_talk_index == "n754_cian-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4072_new_power_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4072);
	if qstep == 2 and CountIndex == 201611 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300425, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300425, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 1611 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300425, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300425, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300425 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function sq15_4072_new_power_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4072);
	if qstep == 2 and CountIndex == 201611 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1611 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300425 and Count >= TargetCount  then

	end
end

function sq15_4072_new_power_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4072);
	local questID=4072;
end

function sq15_4072_new_power_OnRemoteStart( userObjID, questID )
end

function sq15_4072_new_power_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4072_new_power_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4072, 1);
				api_quest_SetJournalStep(userObjID,4072, 1);
				api_quest_SetQuestStep(userObjID,4072, 1);
				npc_talk_index = "n703_book_doctor-1";
end

</VillageServer>

<GameServer>
function sq15_4072_new_power_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 703 then
		sq15_4072_new_power_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 710 then
		sq15_4072_new_power_OnTalk_n710_scholar_hancock( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		sq15_4072_new_power_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n703_book_doctor--------------------------------------------------------------------------------
function sq15_4072_new_power_OnTalk_n703_book_doctor( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n703_book_doctor-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n703_book_doctor-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n703_book_doctor-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n703_book_doctor-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n703_book_doctor-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n703_book_doctor-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n703_book_doctor-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n703_book_doctor-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, false);
			 end 

	end
	if npc_talk_index == "n703_book_doctor-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4072, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4072, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4072, 1);
				npc_talk_index = "n703_book_doctor-1";

	end
	if npc_talk_index == "n703_book_doctor-1-b" then 
	end
	if npc_talk_index == "n703_book_doctor-1-c" then 
	end
	if npc_talk_index == "n703_book_doctor-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 201611, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1611, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300425, 1);
	end
	if npc_talk_index == "n703_book_doctor-3-b" then 
	end
	if npc_talk_index == "n703_book_doctor-3-c" then 
	end
	if npc_talk_index == "n703_book_doctor-3-d" then 
	end
	if npc_talk_index == "n703_book_doctor-3-e" then 
	end
	if npc_talk_index == "n703_book_doctor-3-f" then 
	end
	if npc_talk_index == "n703_book_doctor-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300548, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300548, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n703_book_doctor-6-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40720, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 40720, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 40720, questID, 1);
			 end 
	end
	if npc_talk_index == "n703_book_doctor-6-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4073, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4073, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4073, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 500459, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 500459, api_quest_HasQuestItem( pRoom, userObjID, 500459, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n703_book_doctor-1", "sq15_4073_master_of_power.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n710_scholar_hancock--------------------------------------------------------------------------------
function sq15_4072_new_power_OnTalk_n710_scholar_hancock( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n710_scholar_hancock-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n710_scholar_hancock-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n710_scholar_hancock-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n710_scholar_hancock-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n710_scholar_hancock-4-b" then 
	end
	if npc_talk_index == "n710_scholar_hancock-4-c" then 
	end
	if npc_talk_index == "n710_scholar_hancock-4-d" then 
	end
	if npc_talk_index == "n710_scholar_hancock-4-e" then 
	end
	if npc_talk_index == "n710_scholar_hancock-4-f" then 
	end
	if npc_talk_index == "n710_scholar_hancock-5" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300548, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300548, api_quest_HasQuestItem( pRoom, userObjID, 300548, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300425, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300425, api_quest_HasQuestItem( pRoom, userObjID, 300425, 1));
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300549, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300549, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function sq15_4072_new_power_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n754_cian-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n754_cian-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n754_cian-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-5-b" then 
	end
	if npc_talk_index == "n754_cian-5-c" then 
	end
	if npc_talk_index == "n754_cian-5-d" then 
	end
	if npc_talk_index == "n754_cian-5-e" then 
	end
	if npc_talk_index == "n754_cian-5-f" then 
	end
	if npc_talk_index == "n754_cian-5-g" then 
	end
	if npc_talk_index == "n754_cian-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4072_new_power_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4072);
	if qstep == 2 and CountIndex == 201611 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300425, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300425, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 1611 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300425, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300425, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300425 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function sq15_4072_new_power_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4072);
	if qstep == 2 and CountIndex == 201611 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1611 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300425 and Count >= TargetCount  then

	end
end

function sq15_4072_new_power_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4072);
	local questID=4072;
end

function sq15_4072_new_power_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4072_new_power_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4072_new_power_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4072, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4072, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4072, 1);
				npc_talk_index = "n703_book_doctor-1";
end

</GameServer>