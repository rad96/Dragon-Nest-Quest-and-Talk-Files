<VillageServer>

function sq11_529_decayed_field1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 44 then
		sq11_529_decayed_field1_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq11_529_decayed_field1_OnTalk_n044_archer_master_ishilien(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n044_archer_master_ishilien-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5291, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5292, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5293, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5294, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5295, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5296, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5297, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5298, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5299, false);
			 end 

	end
	if npc_talk_index == "n044_archer_master_ishilien-accepting-acceptted" then
				api_quest_AddQuest(userObjID,529, 1);
				api_quest_SetJournalStep(userObjID,529, 1);
				api_quest_SetQuestStep(userObjID,529, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 906, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200906, 30000);

	end
	if npc_talk_index == "n044_archer_master_ishilien-2-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5291, true);
				 api_quest_RewardQuestUser(userObjID, 5291, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5292, true);
				 api_quest_RewardQuestUser(userObjID, 5292, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5293, true);
				 api_quest_RewardQuestUser(userObjID, 5293, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5294, true);
				 api_quest_RewardQuestUser(userObjID, 5294, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5295, true);
				 api_quest_RewardQuestUser(userObjID, 5295, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5296, true);
				 api_quest_RewardQuestUser(userObjID, 5296, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5297, true);
				 api_quest_RewardQuestUser(userObjID, 5297, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5298, true);
				 api_quest_RewardQuestUser(userObjID, 5298, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5299, true);
				 api_quest_RewardQuestUser(userObjID, 5299, questID, 1);
			 end 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_529_decayed_field1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 529);
	if qstep == 1 and CountIndex == 906 then

	end
	if qstep == 1 and CountIndex == 200906 then
				api_quest_IncCounting(userObjID, 2, 906);

	end
end

function sq11_529_decayed_field1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 529);
	if qstep == 1 and CountIndex == 906 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200906 and Count >= TargetCount  then

	end
end

function sq11_529_decayed_field1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 529);
	local questID=529;
end

function sq11_529_decayed_field1_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 906, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200906, 30000);
end

function sq11_529_decayed_field1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_529_decayed_field1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,529, 1);
				api_quest_SetJournalStep(userObjID,529, 1);
				api_quest_SetQuestStep(userObjID,529, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 906, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200906, 30000);
end

</VillageServer>

<GameServer>
function sq11_529_decayed_field1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 44 then
		sq11_529_decayed_field1_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n044_archer_master_ishilien--------------------------------------------------------------------------------
function sq11_529_decayed_field1_OnTalk_n044_archer_master_ishilien( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n044_archer_master_ishilien-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n044_archer_master_ishilien-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n044_archer_master_ishilien-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5291, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5292, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5293, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5294, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5295, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5296, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5297, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5298, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5299, false);
			 end 

	end
	if npc_talk_index == "n044_archer_master_ishilien-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,529, 1);
				api_quest_SetJournalStep( pRoom, userObjID,529, 1);
				api_quest_SetQuestStep( pRoom, userObjID,529, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 906, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200906, 30000);

	end
	if npc_talk_index == "n044_archer_master_ishilien-2-b" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-c" then 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5291, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5291, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5292, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5292, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5293, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5293, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5294, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5294, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5295, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5295, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5296, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5296, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5297, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5297, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5298, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5298, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5299, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5299, questID, 1);
			 end 
	end
	if npc_talk_index == "n044_archer_master_ishilien-2-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_529_decayed_field1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 529);
	if qstep == 1 and CountIndex == 906 then

	end
	if qstep == 1 and CountIndex == 200906 then
				api_quest_IncCounting( pRoom, userObjID, 2, 906);

	end
end

function sq11_529_decayed_field1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 529);
	if qstep == 1 and CountIndex == 906 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200906 and Count >= TargetCount  then

	end
end

function sq11_529_decayed_field1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 529);
	local questID=529;
end

function sq11_529_decayed_field1_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 906, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200906, 30000);
end

function sq11_529_decayed_field1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_529_decayed_field1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,529, 1);
				api_quest_SetJournalStep( pRoom, userObjID,529, 1);
				api_quest_SetQuestStep( pRoom, userObjID,529, 1);
				npc_talk_index = "n044_archer_master_ishilien-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 906, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200906, 30000);
end

</GameServer>