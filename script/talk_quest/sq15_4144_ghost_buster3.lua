<VillageServer>

function sq15_4144_ghost_buster3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 45 then
		sq15_4144_ghost_buster3_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 46 then
		sq15_4144_ghost_buster3_OnTalk_n046_cleric_master_enoc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4144_ghost_buster3_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n045_soceress_master_stella-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_DelQuestItem(userObjID, 400349, 1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n046_cleric_master_enoc--------------------------------------------------------------------------------
function sq15_4144_ghost_buster3_OnTalk_n046_cleric_master_enoc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
						if api_quest_HasQuestItem(userObjID, 400349, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400349, 10) == 1 then 
					api_quest_AddQuestItem(userObjID, 400349, 10, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end

			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n046_cleric_master_enoc-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n046_cleric_master_enoc-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n046_cleric_master_enoc-accepting-h" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41440, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41440, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41440, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41440, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41440, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41440, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41440, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41440, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41440, false);
			 end 

	end
	if npc_talk_index == "n046_cleric_master_enoc-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4144, 1);
				api_quest_SetJournalStep(userObjID,4144, 1);
				api_quest_SetQuestStep(userObjID,4144, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_quest_DelQuestItem(userObjID, 400349, 1);

	end
	if npc_talk_index == "n046_cleric_master_enoc-2-b" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41440, true);
				 api_quest_RewardQuestUser(userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41440, true);
				 api_quest_RewardQuestUser(userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41440, true);
				 api_quest_RewardQuestUser(userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41440, true);
				 api_quest_RewardQuestUser(userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41440, true);
				 api_quest_RewardQuestUser(userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41440, true);
				 api_quest_RewardQuestUser(userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41440, true);
				 api_quest_RewardQuestUser(userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41440, true);
				 api_quest_RewardQuestUser(userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41440, true);
				 api_quest_RewardQuestUser(userObjID, 41440, questID, 1);
			 end 
	end
	if npc_talk_index == "n046_cleric_master_enoc-2-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400349, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400349, api_quest_HasQuestItem(userObjID, 400349, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4144_ghost_buster3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4144);
end

function sq15_4144_ghost_buster3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4144);
end

function sq15_4144_ghost_buster3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4144);
	local questID=4144;
end

function sq15_4144_ghost_buster3_OnRemoteStart( userObjID, questID )
end

function sq15_4144_ghost_buster3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4144_ghost_buster3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4144, 1);
				api_quest_SetJournalStep(userObjID,4144, 1);
				api_quest_SetQuestStep(userObjID,4144, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_quest_DelQuestItem(userObjID, 400349, 1);
end

</VillageServer>

<GameServer>
function sq15_4144_ghost_buster3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 45 then
		sq15_4144_ghost_buster3_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 46 then
		sq15_4144_ghost_buster3_OnTalk_n046_cleric_master_enoc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq15_4144_ghost_buster3_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n045_soceress_master_stella-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_DelQuestItem( pRoom, userObjID, 400349, 1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n046_cleric_master_enoc--------------------------------------------------------------------------------
function sq15_4144_ghost_buster3_OnTalk_n046_cleric_master_enoc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
						if api_quest_HasQuestItem( pRoom, userObjID, 400349, 1) >= 1 then
				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400349, 10) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400349, 10, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end

			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n046_cleric_master_enoc-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n046_cleric_master_enoc-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n046_cleric_master_enoc-accepting-h" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, false);
			 end 

	end
	if npc_talk_index == "n046_cleric_master_enoc-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4144, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4144, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4144, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_quest_DelQuestItem( pRoom, userObjID, 400349, 1);

	end
	if npc_talk_index == "n046_cleric_master_enoc-2-b" then 
	end
	if npc_talk_index == "n046_cleric_master_enoc-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41440, questID, 1);
			 end 
	end
	if npc_talk_index == "n046_cleric_master_enoc-2-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400349, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400349, api_quest_HasQuestItem( pRoom, userObjID, 400349, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4144_ghost_buster3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4144);
end

function sq15_4144_ghost_buster3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4144);
end

function sq15_4144_ghost_buster3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4144);
	local questID=4144;
end

function sq15_4144_ghost_buster3_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_4144_ghost_buster3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4144_ghost_buster3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4144, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4144, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4144, 1);
				npc_talk_index = "n046_cleric_master_enoc-1";
				api_quest_DelQuestItem( pRoom, userObjID, 400349, 1);
end

</GameServer>