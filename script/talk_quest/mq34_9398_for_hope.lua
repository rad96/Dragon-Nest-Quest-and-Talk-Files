<VillageServer>

function mq34_9398_for_hope_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		mq34_9398_for_hope_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1233 then
		mq34_9398_for_hope_OnTalk_n1233_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1234 then
		mq34_9398_for_hope_OnTalk_n1234_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1250 then
		mq34_9398_for_hope_OnTalk_n1250_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mq34_9398_for_hope_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq34_9398_for_hope_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1067_elder_elf-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-9-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93980, true);
				 api_quest_RewardQuestUser(userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93980, true);
				 api_quest_RewardQuestUser(userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93980, true);
				 api_quest_RewardQuestUser(userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93980, true);
				 api_quest_RewardQuestUser(userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93980, true);
				 api_quest_RewardQuestUser(userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93980, true);
				 api_quest_RewardQuestUser(userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93980, true);
				 api_quest_RewardQuestUser(userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93980, true);
				 api_quest_RewardQuestUser(userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93980, true);
				 api_quest_RewardQuestUser(userObjID, 93980, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-9-c" then 
				api_quest_SetJournalStep(userObjID, questID, 8);

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1233_white_dragon--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n1233_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1233_white_dragon-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1233_white_dragon-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1233_white_dragon-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1233_white_dragon-5-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-n" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
	end
	if npc_talk_index == "n1233_white_dragon-6-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-n" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-p" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-q" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-r" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-s" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-t" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-u" then 
	end
	if npc_talk_index == "n1233_white_dragon-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1234_white_dragon--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n1234_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1234_white_dragon-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1234_white_dragon-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1234_white_dragon-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1234_white_dragon-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1234_white_dragon-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9398, 2);
				api_quest_SetJournalStep(userObjID,9398, 1);
				api_quest_SetQuestStep(userObjID,9398, 1);
				npc_talk_index = "n1234_white_dragon-1";

	end
	if npc_talk_index == "n1234_white_dragon-4-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1250_for_memory--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n1250_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1250_for_memory-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1250_for_memory-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1250_for_memory-8-b" then 
	end
	if npc_talk_index == "n1250_for_memory-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n038_royal_magician_kalaen-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-g" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-h" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-j" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-k" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-l" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-m" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n707_sidel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-1-d" then 
	end
	if npc_talk_index == "n707_sidel-1-e" then 
	end
	if npc_talk_index == "n707_sidel-1-f" then 
	end
	if npc_talk_index == "n707_sidel-1-g" then 
	end
	if npc_talk_index == "n707_sidel-1-h" then 
	end
	if npc_talk_index == "n707_sidel-1-i" then 
	end
	if npc_talk_index == "n707_sidel-1-j" then 
	end
	if npc_talk_index == "n707_sidel-1-k" then 
	end
	if npc_talk_index == "n707_sidel-1-k" then 
	end
	if npc_talk_index == "n707_sidel-1-l" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9398_for_hope_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9398);
end

function mq34_9398_for_hope_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9398);
end

function mq34_9398_for_hope_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9398);
	local questID=9398;
end

function mq34_9398_for_hope_OnRemoteStart( userObjID, questID )
end

function mq34_9398_for_hope_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9398_for_hope_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9398, 2);
				api_quest_SetJournalStep(userObjID,9398, 1);
				api_quest_SetQuestStep(userObjID,9398, 1);
				npc_talk_index = "n1234_white_dragon-1";
end

</VillageServer>

<GameServer>
function mq34_9398_for_hope_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		mq34_9398_for_hope_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1233 then
		mq34_9398_for_hope_OnTalk_n1233_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1234 then
		mq34_9398_for_hope_OnTalk_n1234_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1250 then
		mq34_9398_for_hope_OnTalk_n1250_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mq34_9398_for_hope_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq34_9398_for_hope_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1067_elder_elf-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-9-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93980, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-9-c" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1233_white_dragon--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n1233_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1233_white_dragon-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1233_white_dragon-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1233_white_dragon-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1233_white_dragon-5-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-n" then 
	end
	if npc_talk_index == "n1233_white_dragon-5-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
	end
	if npc_talk_index == "n1233_white_dragon-6-b" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-c" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-d" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-e" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-f" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-g" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-h" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-i" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-j" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-k" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-l" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-m" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-n" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-o" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-p" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-q" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-r" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-s" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-t" then 
	end
	if npc_talk_index == "n1233_white_dragon-6-u" then 
	end
	if npc_talk_index == "n1233_white_dragon-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1234_white_dragon--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n1234_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1234_white_dragon-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1234_white_dragon-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1234_white_dragon-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1234_white_dragon-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1234_white_dragon-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9398, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9398, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9398, 1);
				npc_talk_index = "n1234_white_dragon-1";

	end
	if npc_talk_index == "n1234_white_dragon-4-b" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-c" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-d" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-4-e" then 
	end
	if npc_talk_index == "n1234_white_dragon-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1250_for_memory--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n1250_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1250_for_memory-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1250_for_memory-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1250_for_memory-8-b" then 
	end
	if npc_talk_index == "n1250_for_memory-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n038_royal_magician_kalaen-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-g" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-h" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-i" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-j" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-k" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-l" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-m" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq34_9398_for_hope_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n707_sidel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-1-d" then 
	end
	if npc_talk_index == "n707_sidel-1-e" then 
	end
	if npc_talk_index == "n707_sidel-1-f" then 
	end
	if npc_talk_index == "n707_sidel-1-g" then 
	end
	if npc_talk_index == "n707_sidel-1-h" then 
	end
	if npc_talk_index == "n707_sidel-1-i" then 
	end
	if npc_talk_index == "n707_sidel-1-j" then 
	end
	if npc_talk_index == "n707_sidel-1-k" then 
	end
	if npc_talk_index == "n707_sidel-1-k" then 
	end
	if npc_talk_index == "n707_sidel-1-l" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9398_for_hope_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9398);
end

function mq34_9398_for_hope_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9398);
end

function mq34_9398_for_hope_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9398);
	local questID=9398;
end

function mq34_9398_for_hope_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9398_for_hope_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9398_for_hope_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9398, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9398, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9398, 1);
				npc_talk_index = "n1234_white_dragon-1";
end

</GameServer>