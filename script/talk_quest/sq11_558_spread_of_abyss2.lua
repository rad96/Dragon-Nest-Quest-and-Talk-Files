<VillageServer>

function sq11_558_spread_of_abyss2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 88 then
		sq11_558_spread_of_abyss2_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_558_spread_of_abyss2_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5581, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5582, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5583, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5584, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5585, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5586, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5587, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5587, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5587, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,558, 1);
				api_quest_SetJournalStep(userObjID,558, 1);
				api_quest_SetQuestStep(userObjID,558, 1);
				api_npc_NextTalk(userObjID, npcObjID, "n088_scholar_starshy-1-b", npc_talk_target);

	end
	if npc_talk_index == "n088_scholar_starshy-1-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 201081, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201119, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201215, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201168, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 201159, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 3, 300268, 1);
				api_quest_SetCountingInfo(userObjID, questID, 6, 3, 300269, 1);
				api_quest_SetCountingInfo(userObjID, questID, 7, 3, 300270, 1);
				api_quest_SetCountingInfo(userObjID, questID, 8, 3, 300271, 1);
				api_quest_SetCountingInfo(userObjID, questID, 9, 3, 300272, 1);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5581, true);
				 api_quest_RewardQuestUser(userObjID, 5581, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5582, true);
				 api_quest_RewardQuestUser(userObjID, 5582, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5583, true);
				 api_quest_RewardQuestUser(userObjID, 5583, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5584, true);
				 api_quest_RewardQuestUser(userObjID, 5584, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5585, true);
				 api_quest_RewardQuestUser(userObjID, 5585, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5586, true);
				 api_quest_RewardQuestUser(userObjID, 5586, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5587, true);
				 api_quest_RewardQuestUser(userObjID, 5587, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5587, true);
				 api_quest_RewardQuestUser(userObjID, 5587, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5587, true);
				 api_quest_RewardQuestUser(userObjID, 5587, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-3-h" then 

				if api_quest_HasQuestItem(userObjID, 300268, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300268, api_quest_HasQuestItem(userObjID, 300268, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300269, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300269, api_quest_HasQuestItem(userObjID, 300269, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300270, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300270, api_quest_HasQuestItem(userObjID, 300270, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300271, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300271, api_quest_HasQuestItem(userObjID, 300271, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300272, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300272, api_quest_HasQuestItem(userObjID, 300272, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_558_spread_of_abyss2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 558);
	if qstep == 2 and CountIndex == 201081 then
				if api_quest_HasQuestItem(userObjID, 300268, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300268, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300268, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201119 then
				if api_quest_HasQuestItem(userObjID, 300269, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300269, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300269, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201215 then
				if api_quest_HasQuestItem(userObjID, 300270, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300270, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300270, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201168 then
				if api_quest_HasQuestItem(userObjID, 300271, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300271, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300271, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201159 then
				if api_quest_HasQuestItem(userObjID, 300272, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300272, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300272, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 300268 then

	end
	if qstep == 2 and CountIndex == 300269 then

	end
	if qstep == 2 and CountIndex == 300270 then

	end
	if qstep == 2 and CountIndex == 300271 then

	end
	if qstep == 2 and CountIndex == 300272 then

	end
end

function sq11_558_spread_of_abyss2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 558);
	if qstep == 2 and CountIndex == 201081 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201119 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201215 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201168 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201159 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300268 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300269 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300270 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300271 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300272 and Count >= TargetCount  then

	end
end

function sq11_558_spread_of_abyss2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 558);
	local questID=558;
end

function sq11_558_spread_of_abyss2_OnRemoteStart( userObjID, questID )
end

function sq11_558_spread_of_abyss2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_558_spread_of_abyss2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,558, 1);
				api_quest_SetJournalStep(userObjID,558, 1);
				api_quest_SetQuestStep(userObjID,558, 1);
				api_npc_NextTalk(userObjID, npcObjID, "n088_scholar_starshy-1-b", npc_talk_target);
end

</VillageServer>

<GameServer>
function sq11_558_spread_of_abyss2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 88 then
		sq11_558_spread_of_abyss2_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_558_spread_of_abyss2_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5581, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5582, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5583, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5584, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5585, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5586, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5587, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5587, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5587, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,558, 1);
				api_quest_SetJournalStep( pRoom, userObjID,558, 1);
				api_quest_SetQuestStep( pRoom, userObjID,558, 1);
				api_npc_NextTalk( pRoom, userObjID, npcObjID, "n088_scholar_starshy-1-b", npc_talk_target);

	end
	if npc_talk_index == "n088_scholar_starshy-1-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 201081, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201119, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201215, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201168, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 201159, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 3, 300268, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 3, 300269, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 3, 300270, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 3, 300271, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 9, 3, 300272, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5581, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5581, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5582, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5582, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5583, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5583, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5584, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5584, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5585, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5585, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5586, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5586, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5587, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5587, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5587, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5587, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5587, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5587, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-3-h" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300268, api_quest_HasQuestItem( pRoom, userObjID, 300268, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300269, api_quest_HasQuestItem( pRoom, userObjID, 300269, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300270, api_quest_HasQuestItem( pRoom, userObjID, 300270, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300271, api_quest_HasQuestItem( pRoom, userObjID, 300271, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300272, api_quest_HasQuestItem( pRoom, userObjID, 300272, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_558_spread_of_abyss2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 558);
	if qstep == 2 and CountIndex == 201081 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300268, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300268, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201119 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300269, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300269, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201215 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300270, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300270, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201168 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300271, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300271, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201159 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300272, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300272, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300268, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300269, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300270, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300271, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300272, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 300268 then

	end
	if qstep == 2 and CountIndex == 300269 then

	end
	if qstep == 2 and CountIndex == 300270 then

	end
	if qstep == 2 and CountIndex == 300271 then

	end
	if qstep == 2 and CountIndex == 300272 then

	end
end

function sq11_558_spread_of_abyss2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 558);
	if qstep == 2 and CountIndex == 201081 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201119 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201215 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201168 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201159 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300268 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300269 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300270 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300271 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300272 and Count >= TargetCount  then

	end
end

function sq11_558_spread_of_abyss2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 558);
	local questID=558;
end

function sq11_558_spread_of_abyss2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_558_spread_of_abyss2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_558_spread_of_abyss2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,558, 1);
				api_quest_SetJournalStep( pRoom, userObjID,558, 1);
				api_quest_SetQuestStep( pRoom, userObjID,558, 1);
				api_npc_NextTalk( pRoom, userObjID, npcObjID, "n088_scholar_starshy-1-b", npc_talk_target);
end

</GameServer>