<VillageServer>

function sq08_318_beautiful_siver_blonde_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 164 then
		sq08_318_beautiful_siver_blonde_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 26 then
		sq08_318_beautiful_siver_blonde_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_318_beautiful_siver_blonde_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n164_argenta_wounded-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				if api_quest_GetQuestMemo(userObjID, questID, 1) == 1  then
									if api_user_HasItem(userObjID, 335545217, 1) >= 1 then
									npc_talk_index = "n164_argenta_wounded-1-i";

				else
									npc_talk_index = "n164_argenta_wounded-1-h";

				end

				else
									npc_talk_index = "n164_argenta_wounded-1";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n164_argenta_wounded-2";
				if api_quest_GetQuestMemo(userObjID, questID, 1) == 2  then
									npc_talk_index = "n164_argenta_wounded-2";

				else
									npc_talk_index = "n164_argenta_wounded-2-a";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-1-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-g" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-h" then 
				api_quest_SetQuestMemo(userObjID, questID, 1, 1);
	end
	if npc_talk_index == "n164_argenta_wounded-1-j" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-k" then 
				api_quest_ClearCountingInfo(userObjID, questID);
				api_user_DelItem(userObjID, 335545217, 1, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400123, 4) == 1 then 
					api_quest_AddQuestItem(userObjID, 400123, 4, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400124, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400124, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestMemo(userObjID, questID, 1, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n026_trader_may--------------------------------------------------------------------------------
function sq08_318_beautiful_siver_blonde_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				if api_quest_GetQuestMemo(userObjID, questID, 1) == 2  then
									npc_talk_index = "n026_trader_may-2";

				else
									npc_talk_index = "n026_trader_may-2-b";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3180, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3180, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3180, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3180, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3180, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3180, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3180, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3180, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3180, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-f" then
				api_quest_AddQuest(userObjID,318, 1);
				api_quest_SetJournalStep(userObjID,318, 1);
				api_quest_SetQuestStep(userObjID,318, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 608, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200608, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400123, 5);

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted_1" then
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted_2" then
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-2-reward_1" then 
				npc_talk_index = "n026_trader_may-2-c";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3181, true);
				 api_quest_RewardQuestUser(userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3181, true);
				 api_quest_RewardQuestUser(userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3181, true);
				 api_quest_RewardQuestUser(userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3181, true);
				 api_quest_RewardQuestUser(userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3181, true);
				 api_quest_RewardQuestUser(userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3181, true);
				 api_quest_RewardQuestUser(userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3181, true);
				 api_quest_RewardQuestUser(userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3181, true);
				 api_quest_RewardQuestUser(userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3181, true);
				 api_quest_RewardQuestUser(userObjID, 3181, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-2-reward_2" then 
				npc_talk_index = "n026_trader_may-2-c";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3180, true);
				 api_quest_RewardQuestUser(userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3180, true);
				 api_quest_RewardQuestUser(userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3180, true);
				 api_quest_RewardQuestUser(userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3180, true);
				 api_quest_RewardQuestUser(userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3180, true);
				 api_quest_RewardQuestUser(userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3180, true);
				 api_quest_RewardQuestUser(userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3180, true);
				 api_quest_RewardQuestUser(userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3180, true);
				 api_quest_RewardQuestUser(userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3180, true);
				 api_quest_RewardQuestUser(userObjID, 3180, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400123, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400123, api_quest_HasQuestItem(userObjID, 400123, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400124, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400124, api_quest_HasQuestItem(userObjID, 400124, 1));
				end
	end
	if npc_talk_index == "n026_trader_may-2-e" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_318_beautiful_siver_blonde_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 318);
	if qstep == 1 and CountIndex == 608 then
				if api_quest_HasQuestItem(userObjID, 400123, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400123, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400123, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400123, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 400123 then

	end
	if qstep == 1 and CountIndex == 200608 then
				if api_quest_HasQuestItem(userObjID, 400123, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400123, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400123, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400123, 5) >= 5 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end

				end

	end
end

function sq08_318_beautiful_siver_blonde_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 318);
	if qstep == 1 and CountIndex == 608 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400123 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200608 and Count >= TargetCount  then

	end
end

function sq08_318_beautiful_siver_blonde_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 318);
	local questID=318;
end

function sq08_318_beautiful_siver_blonde_OnRemoteStart( userObjID, questID )
end

function sq08_318_beautiful_siver_blonde_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_318_beautiful_siver_blonde_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq08_318_beautiful_siver_blonde_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 164 then
		sq08_318_beautiful_siver_blonde_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 26 then
		sq08_318_beautiful_siver_blonde_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_318_beautiful_siver_blonde_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n164_argenta_wounded-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				if api_quest_GetQuestMemo( pRoom, userObjID, questID, 1) == 1  then
									if api_user_HasItem( pRoom, userObjID, 335545217, 1) >= 1 then
									npc_talk_index = "n164_argenta_wounded-1-i";

				else
									npc_talk_index = "n164_argenta_wounded-1-h";

				end

				else
									npc_talk_index = "n164_argenta_wounded-1";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n164_argenta_wounded-2";
				if api_quest_GetQuestMemo( pRoom, userObjID, questID, 1) == 2  then
									npc_talk_index = "n164_argenta_wounded-2";

				else
									npc_talk_index = "n164_argenta_wounded-2-a";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-1-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-c" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-d" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-e" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-f" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-g" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-h" then 
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 1);
	end
	if npc_talk_index == "n164_argenta_wounded-1-j" then 
	end
	if npc_talk_index == "n164_argenta_wounded-1-k" then 
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_user_DelItem( pRoom, userObjID, 335545217, 1, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400123, 4) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400123, 4, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400124, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400124, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n026_trader_may--------------------------------------------------------------------------------
function sq08_318_beautiful_siver_blonde_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				if api_quest_GetQuestMemo( pRoom, userObjID, questID, 1) == 2  then
									npc_talk_index = "n026_trader_may-2";

				else
									npc_talk_index = "n026_trader_may-2-b";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-f" then
				api_quest_AddQuest( pRoom, userObjID,318, 1);
				api_quest_SetJournalStep( pRoom, userObjID,318, 1);
				api_quest_SetQuestStep( pRoom, userObjID,318, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 608, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200608, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400123, 5);

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted_1" then
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted_2" then
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-2-reward_1" then 
				npc_talk_index = "n026_trader_may-2-c";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3181, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3181, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3181, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-2-reward_2" then 
				npc_talk_index = "n026_trader_may-2-c";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3180, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3180, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3180, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400123, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400123, api_quest_HasQuestItem( pRoom, userObjID, 400123, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400124, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400124, api_quest_HasQuestItem( pRoom, userObjID, 400124, 1));
				end
	end
	if npc_talk_index == "n026_trader_may-2-e" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_318_beautiful_siver_blonde_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 318);
	if qstep == 1 and CountIndex == 608 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400123, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400123, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400123, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400123, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				end

	end
	if qstep == 1 and CountIndex == 400123 then

	end
	if qstep == 1 and CountIndex == 200608 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400123, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400123, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400123, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400123, 5) >= 5 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end

				end

	end
end

function sq08_318_beautiful_siver_blonde_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 318);
	if qstep == 1 and CountIndex == 608 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400123 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200608 and Count >= TargetCount  then

	end
end

function sq08_318_beautiful_siver_blonde_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 318);
	local questID=318;
end

function sq08_318_beautiful_siver_blonde_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_318_beautiful_siver_blonde_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_318_beautiful_siver_blonde_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>