<VillageServer>

function sq15_834_something_legendary_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 711 then
		sq15_834_something_legendary_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_834_something_legendary_OnTalk_n711_warrior_fedro(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n711_warrior_fedro-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8340, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8340, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8340, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8340, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8340, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8340, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8340, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8340, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8340, false);
			 end 

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest(userObjID,834, 1);
				api_quest_SetJournalStep(userObjID,834, 1);
				api_quest_SetQuestStep(userObjID,834, 1);
				npc_talk_index = "n711_warrior_fedro-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1611, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201611, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300460, 30000);

	end
	if npc_talk_index == "n711_warrior_fedro-2-b" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8340, true);
				 api_quest_RewardQuestUser(userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8340, true);
				 api_quest_RewardQuestUser(userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8340, true);
				 api_quest_RewardQuestUser(userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8340, true);
				 api_quest_RewardQuestUser(userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8340, true);
				 api_quest_RewardQuestUser(userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8340, true);
				 api_quest_RewardQuestUser(userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8340, true);
				 api_quest_RewardQuestUser(userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8340, true);
				 api_quest_RewardQuestUser(userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8340, true);
				 api_quest_RewardQuestUser(userObjID, 8340, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-2-d" then 

				if api_quest_HasQuestItem(userObjID, 300460, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300460, api_quest_HasQuestItem(userObjID, 300460, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_834_something_legendary_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 834);
	if qstep == 1 and CountIndex == 1611 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300460, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300460, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201611 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300460, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300460, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300460 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function sq15_834_something_legendary_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 834);
	if qstep == 1 and CountIndex == 1611 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201611 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300460 and Count >= TargetCount  then

	end
end

function sq15_834_something_legendary_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 834);
	local questID=834;
end

function sq15_834_something_legendary_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1611, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201611, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300460, 30000);
end

function sq15_834_something_legendary_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_834_something_legendary_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,834, 1);
				api_quest_SetJournalStep(userObjID,834, 1);
				api_quest_SetQuestStep(userObjID,834, 1);
				npc_talk_index = "n711_warrior_fedro-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1611, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201611, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300460, 30000);
end

</VillageServer>

<GameServer>
function sq15_834_something_legendary_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 711 then
		sq15_834_something_legendary_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n711_warrior_fedro--------------------------------------------------------------------------------
function sq15_834_something_legendary_OnTalk_n711_warrior_fedro( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n711_warrior_fedro-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n711_warrior_fedro-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n711_warrior_fedro-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n711_warrior_fedro-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, false);
			 end 

	end
	if npc_talk_index == "n711_warrior_fedro-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,834, 1);
				api_quest_SetJournalStep( pRoom, userObjID,834, 1);
				api_quest_SetQuestStep( pRoom, userObjID,834, 1);
				npc_talk_index = "n711_warrior_fedro-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1611, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201611, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300460, 30000);

	end
	if npc_talk_index == "n711_warrior_fedro-2-b" then 
	end
	if npc_talk_index == "n711_warrior_fedro-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8340, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8340, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8340, questID, 1);
			 end 
	end
	if npc_talk_index == "n711_warrior_fedro-2-d" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300460, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300460, api_quest_HasQuestItem( pRoom, userObjID, 300460, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_834_something_legendary_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 834);
	if qstep == 1 and CountIndex == 1611 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300460, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300460, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 201611 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300460, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300460, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300460 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function sq15_834_something_legendary_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 834);
	if qstep == 1 and CountIndex == 1611 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201611 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300460 and Count >= TargetCount  then

	end
end

function sq15_834_something_legendary_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 834);
	local questID=834;
end

function sq15_834_something_legendary_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1611, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201611, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300460, 30000);
end

function sq15_834_something_legendary_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_834_something_legendary_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,834, 1);
				api_quest_SetJournalStep( pRoom, userObjID,834, 1);
				api_quest_SetQuestStep( pRoom, userObjID,834, 1);
				npc_talk_index = "n711_warrior_fedro-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1611, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201611, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300460, 30000);
end

</GameServer>