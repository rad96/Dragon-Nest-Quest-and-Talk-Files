<VillageServer>

function sq_2126_jumping_event_7_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 101 then
		sq_2126_jumping_event_7_OnTalk_n101_event_tracey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n101_event_tracey--------------------------------------------------------------------------------
function sq_2126_jumping_event_7_OnTalk_n101_event_tracey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
					 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21261, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21262, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21263, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21264, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21265, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21266, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21267, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21268, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21260, false);
			 end 

			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n101_event_tracey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n101_event_tracey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n101_event_tracey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2126, 1);
				api_quest_SetJournalStep(userObjID,2126, 1);
				api_quest_SetQuestStep(userObjID,2126, 1);
				npc_talk_index = "n101_event_tracey-1";

	end
	if npc_talk_index == "n101_event_tracey-1-b" then 
	end
	if npc_talk_index == "n101_event_tracey-1-c" then 
	end
	if npc_talk_index == "n101_event_tracey-1-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21261, true);
				 api_quest_RewardQuestUser(userObjID, 21261, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21262, true);
				 api_quest_RewardQuestUser(userObjID, 21262, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21263, true);
				 api_quest_RewardQuestUser(userObjID, 21263, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21264, true);
				 api_quest_RewardQuestUser(userObjID, 21264, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21265, true);
				 api_quest_RewardQuestUser(userObjID, 21265, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21266, true);
				 api_quest_RewardQuestUser(userObjID, 21266, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21267, true);
				 api_quest_RewardQuestUser(userObjID, 21267, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21268, true);
				 api_quest_RewardQuestUser(userObjID, 21268, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21260, true);
				 api_quest_RewardQuestUser(userObjID, 21260, questID, 1);
			 end 
	end
	if npc_talk_index == "n101_event_tracey-1-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 2127, 1);
					api_quest_SetQuestStep(userObjID, 2127, 1);
					api_quest_SetJournalStep(userObjID, 2127, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n101_event_tracey-1-open_inven" then 
				api_ui_OpenInventory(userObjID);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2126_jumping_event_7_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2126);
end

function sq_2126_jumping_event_7_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2126);
end

function sq_2126_jumping_event_7_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2126);
	local questID=2126;
end

function sq_2126_jumping_event_7_OnRemoteStart( userObjID, questID )
end

function sq_2126_jumping_event_7_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_2126_jumping_event_7_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2126, 1);
				api_quest_SetJournalStep(userObjID,2126, 1);
				api_quest_SetQuestStep(userObjID,2126, 1);
				npc_talk_index = "n101_event_tracey-1";
end

</VillageServer>

<GameServer>
function sq_2126_jumping_event_7_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 101 then
		sq_2126_jumping_event_7_OnTalk_n101_event_tracey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n101_event_tracey--------------------------------------------------------------------------------
function sq_2126_jumping_event_7_OnTalk_n101_event_tracey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
					 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21261, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21262, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21263, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21264, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21265, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21266, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21267, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21268, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21260, false);
			 end 

			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n101_event_tracey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n101_event_tracey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n101_event_tracey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2126, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2126, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2126, 1);
				npc_talk_index = "n101_event_tracey-1";

	end
	if npc_talk_index == "n101_event_tracey-1-b" then 
	end
	if npc_talk_index == "n101_event_tracey-1-c" then 
	end
	if npc_talk_index == "n101_event_tracey-1-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21261, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21261, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21262, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21262, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21263, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21263, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21264, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21264, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21265, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21265, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21266, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21266, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21267, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21267, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21268, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21268, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21260, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21260, questID, 1);
			 end 
	end
	if npc_talk_index == "n101_event_tracey-1-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 2127, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 2127, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 2127, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n101_event_tracey-1-open_inven" then 
				api_ui_OpenInventory( pRoom, userObjID);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2126_jumping_event_7_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2126);
end

function sq_2126_jumping_event_7_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2126);
end

function sq_2126_jumping_event_7_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2126);
	local questID=2126;
end

function sq_2126_jumping_event_7_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_2126_jumping_event_7_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_2126_jumping_event_7_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2126, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2126, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2126, 1);
				npc_talk_index = "n101_event_tracey-1";
end

</GameServer>