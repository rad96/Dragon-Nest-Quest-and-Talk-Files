<VillageServer>

function dqc17_1305_take_all_n735_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 735 then
		dqc17_1305_take_all_n735_OnTalk_n735_darklair_mocha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n735_darklair_mocha--------------------------------------------------------------------------------
function dqc17_1305_take_all_n735_OnTalk_n735_darklair_mocha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n735_darklair_mocha-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n735_darklair_mocha-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n735_darklair_mocha-accepting-acceptted" then
				api_quest_AddDailyQuestAll(userObjID, npcObjID);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dqc17_1305_take_all_n735_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1305);
end

function dqc17_1305_take_all_n735_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1305);
end

function dqc17_1305_take_all_n735_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1305);
	local questID=1305;
end

function dqc17_1305_take_all_n735_OnRemoteStart( userObjID, questID )
end

function dqc17_1305_take_all_n735_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function dqc17_1305_take_all_n735_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddDailyQuestAll(userObjID, npcObjID);
end

</VillageServer>

<GameServer>
function dqc17_1305_take_all_n735_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 735 then
		dqc17_1305_take_all_n735_OnTalk_n735_darklair_mocha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n735_darklair_mocha--------------------------------------------------------------------------------
function dqc17_1305_take_all_n735_OnTalk_n735_darklair_mocha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n735_darklair_mocha-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n735_darklair_mocha-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n735_darklair_mocha-accepting-acceptted" then
				api_quest_AddDailyQuestAll( pRoom, userObjID, npcObjID);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dqc17_1305_take_all_n735_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1305);
end

function dqc17_1305_take_all_n735_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1305);
end

function dqc17_1305_take_all_n735_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1305);
	local questID=1305;
end

function dqc17_1305_take_all_n735_OnRemoteStart( pRoom,  userObjID, questID )
end

function dqc17_1305_take_all_n735_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function dqc17_1305_take_all_n735_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddDailyQuestAll( pRoom, userObjID, npcObjID);
end

</GameServer>