<VillageServer>

function mqc14_9410_the_outbreak_of_war_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1505 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1505_elf_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1506 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1506_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1507 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1507_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1551 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1551_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 249 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1047_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-4-b" then 
	end
	if npc_talk_index == "n1047_argenta-4-c" then 
	end
	if npc_talk_index == "n1047_argenta-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94100, true);
				 api_quest_RewardQuestUser(userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94100, true);
				 api_quest_RewardQuestUser(userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94100, true);
				 api_quest_RewardQuestUser(userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94100, true);
				 api_quest_RewardQuestUser(userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94100, true);
				 api_quest_RewardQuestUser(userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94100, true);
				 api_quest_RewardQuestUser(userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94100, true);
				 api_quest_RewardQuestUser(userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94100, true);
				 api_quest_RewardQuestUser(userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94100, true);
				 api_quest_RewardQuestUser(userObjID, 94100, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-4-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9411, 2);
					api_quest_SetQuestStep(userObjID, 9411, 1);
					api_quest_SetJournalStep(userObjID, 9411, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1047_argenta-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1047_argenta-1", "mqc14_9411_entrance_of_monolith.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1065_geraint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1065_geraint_kid-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1067_elder_elf-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-2-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-2-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-2-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-2-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-2-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				if api_quest_IsMarkingCompleteQuest(userObjID, 9459) == 1 then
				else
									api_quest_ForceCompleteQuest(userObjID, 9459, 0, 1, 1, 0);

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1505_elf_king--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1505_elf_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1505_elf_king-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1505_elf_king-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1505_elf_king-3-b" then 
	end
	if npc_talk_index == "n1505_elf_king-3-c" then 
	end
	if npc_talk_index == "n1505_elf_king-3-c" then 
	end
	if npc_talk_index == "n1505_elf_king-3-c" then 
	end
	if npc_talk_index == "n1505_elf_king-3-d" then 
	end
	if npc_talk_index == "n1505_elf_king-3-e" then 
	end
	if npc_talk_index == "n1505_elf_king-3-f" then 
	end
	if npc_talk_index == "n1505_elf_king-3-g" then 
	end
	if npc_talk_index == "n1505_elf_king-3-h" then 
	end
	if npc_talk_index == "n1505_elf_king-3-i" then 
	end
	if npc_talk_index == "n1505_elf_king-3-j" then 
	end
	if npc_talk_index == "n1505_elf_king-3-k" then 
	end
	if npc_talk_index == "n1505_elf_king-3-l" then 
	end
	if npc_talk_index == "n1505_elf_king-3-l" then 
	end
	if npc_talk_index == "n1505_elf_king-3-l" then 
	end
	if npc_talk_index == "n1505_elf_king-3-m" then 
	end
	if npc_talk_index == "n1505_elf_king-3-m" then 
	end
	if npc_talk_index == "n1505_elf_king-3-m" then 
	end
	if npc_talk_index == "n1505_elf_king-3-n" then 
	end
	if npc_talk_index == "n1505_elf_king-3-o" then 
	end
	if npc_talk_index == "n1505_elf_king-3-p" then 
	end
	if npc_talk_index == "n1505_elf_king-3-q" then 
	end
	if npc_talk_index == "n1505_elf_king-3-r" then 
	end
	if npc_talk_index == "n1505_elf_king-3-s" then 
	end
	if npc_talk_index == "n1505_elf_king-3-t" then 
	end
	if npc_talk_index == "n1505_elf_king-3-t" then 
	end
	if npc_talk_index == "n1505_elf_king-3-t" then 
	end
	if npc_talk_index == "n1505_elf_king-3-u" then 
	end
	if npc_talk_index == "n1505_elf_king-3-v" then 
	end
	if npc_talk_index == "n1505_elf_king-3-w" then 
	end
	if npc_talk_index == "n1505_elf_king-3-w" then 
	end
	if npc_talk_index == "n1505_elf_king-3-w" then 
	end
	if npc_talk_index == "n1505_elf_king-3-x" then 
	end
	if npc_talk_index == "n1505_elf_king-3-y" then 
	end
	if npc_talk_index == "n1505_elf_king-3-z" then 
	end
	if npc_talk_index == "n1505_elf_king-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1506_argenta--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1506_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1506_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1506_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1507_gereint_kid--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1507_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1507_gereint_kid-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1551_elder_elf--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1551_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1551_elder_elf-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1551_elder_elf-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n249_saint_guard--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n249_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n249_saint_guard-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n249_saint_guard-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9410, 2);
				api_quest_SetJournalStep(userObjID,9410, 1);
				api_quest_SetQuestStep(userObjID,9410, 1);
				npc_talk_index = "n249_saint_guard-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n040_king_casius-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n040_king_casius-1-b" then 
	end
	if npc_talk_index == "n040_king_casius-1-c" then 
	end
	if npc_talk_index == "n040_king_casius-1-d" then 
	end
	if npc_talk_index == "n040_king_casius-1-e" then 
	end
	if npc_talk_index == "n040_king_casius-1-f" then 
	end
	if npc_talk_index == "n040_king_casius-1-g" then 
	end
	if npc_talk_index == "n040_king_casius-1-h" then 
	end
	if npc_talk_index == "n040_king_casius-1-i" then 
	end
	if npc_talk_index == "n040_king_casius-1-j" then 
	end
	if npc_talk_index == "n040_king_casius-1-k" then 
	end
	if npc_talk_index == "n040_king_casius-1-l" then 
	end
	if npc_talk_index == "n040_king_casius-1-l" then 
	end
	if npc_talk_index == "n040_king_casius-1-l" then 
	end
	if npc_talk_index == "n040_king_casius-1-m" then 
	end
	if npc_talk_index == "n040_king_casius-1-n" then 
	end
	if npc_talk_index == "n040_king_casius-1-o" then 
	end
	if npc_talk_index == "n040_king_casius-1-p" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9410_the_outbreak_of_war_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9410);
end

function mqc14_9410_the_outbreak_of_war_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9410);
end

function mqc14_9410_the_outbreak_of_war_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9410);
	local questID=9410;
end

function mqc14_9410_the_outbreak_of_war_OnRemoteStart( userObjID, questID )
end

function mqc14_9410_the_outbreak_of_war_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9410_the_outbreak_of_war_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9410, 2);
				api_quest_SetJournalStep(userObjID,9410, 1);
				api_quest_SetQuestStep(userObjID,9410, 1);
				npc_talk_index = "n249_saint_guard-1";
end

</VillageServer>

<GameServer>
function mqc14_9410_the_outbreak_of_war_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1065 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1505 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1505_elf_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1506 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1506_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1507 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1507_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1551 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n1551_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 249 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mqc14_9410_the_outbreak_of_war_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1047_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-4-b" then 
	end
	if npc_talk_index == "n1047_argenta-4-c" then 
	end
	if npc_talk_index == "n1047_argenta-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94100, questID, 1);
			 end 
	end
	if npc_talk_index == "n1047_argenta-4-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9411, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9411, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9411, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1047_argenta-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1047_argenta-1", "mqc14_9411_entrance_of_monolith.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1065_geraint_kid--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1065_geraint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1065_geraint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1065_geraint_kid-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1067_elder_elf-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-2-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-2-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-2-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-2-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-2-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9459) == 1 then
				else
									api_quest_ForceCompleteQuest( pRoom, userObjID, 9459, 0, 1, 1, 0);

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1505_elf_king--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1505_elf_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1505_elf_king-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1505_elf_king-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1505_elf_king-3-b" then 
	end
	if npc_talk_index == "n1505_elf_king-3-c" then 
	end
	if npc_talk_index == "n1505_elf_king-3-c" then 
	end
	if npc_talk_index == "n1505_elf_king-3-c" then 
	end
	if npc_talk_index == "n1505_elf_king-3-d" then 
	end
	if npc_talk_index == "n1505_elf_king-3-e" then 
	end
	if npc_talk_index == "n1505_elf_king-3-f" then 
	end
	if npc_talk_index == "n1505_elf_king-3-g" then 
	end
	if npc_talk_index == "n1505_elf_king-3-h" then 
	end
	if npc_talk_index == "n1505_elf_king-3-i" then 
	end
	if npc_talk_index == "n1505_elf_king-3-j" then 
	end
	if npc_talk_index == "n1505_elf_king-3-k" then 
	end
	if npc_talk_index == "n1505_elf_king-3-l" then 
	end
	if npc_talk_index == "n1505_elf_king-3-l" then 
	end
	if npc_talk_index == "n1505_elf_king-3-l" then 
	end
	if npc_talk_index == "n1505_elf_king-3-m" then 
	end
	if npc_talk_index == "n1505_elf_king-3-m" then 
	end
	if npc_talk_index == "n1505_elf_king-3-m" then 
	end
	if npc_talk_index == "n1505_elf_king-3-n" then 
	end
	if npc_talk_index == "n1505_elf_king-3-o" then 
	end
	if npc_talk_index == "n1505_elf_king-3-p" then 
	end
	if npc_talk_index == "n1505_elf_king-3-q" then 
	end
	if npc_talk_index == "n1505_elf_king-3-r" then 
	end
	if npc_talk_index == "n1505_elf_king-3-s" then 
	end
	if npc_talk_index == "n1505_elf_king-3-t" then 
	end
	if npc_talk_index == "n1505_elf_king-3-t" then 
	end
	if npc_talk_index == "n1505_elf_king-3-t" then 
	end
	if npc_talk_index == "n1505_elf_king-3-u" then 
	end
	if npc_talk_index == "n1505_elf_king-3-v" then 
	end
	if npc_talk_index == "n1505_elf_king-3-w" then 
	end
	if npc_talk_index == "n1505_elf_king-3-w" then 
	end
	if npc_talk_index == "n1505_elf_king-3-w" then 
	end
	if npc_talk_index == "n1505_elf_king-3-x" then 
	end
	if npc_talk_index == "n1505_elf_king-3-y" then 
	end
	if npc_talk_index == "n1505_elf_king-3-z" then 
	end
	if npc_talk_index == "n1505_elf_king-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1506_argenta--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1506_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1506_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1506_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1507_gereint_kid--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1507_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1507_gereint_kid-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1551_elder_elf--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n1551_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1551_elder_elf-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1551_elder_elf-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n249_saint_guard--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n249_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n249_saint_guard-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n249_saint_guard-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9410, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9410, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9410, 1);
				npc_talk_index = "n249_saint_guard-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n040_king_casius-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n040_king_casius-1-b" then 
	end
	if npc_talk_index == "n040_king_casius-1-c" then 
	end
	if npc_talk_index == "n040_king_casius-1-d" then 
	end
	if npc_talk_index == "n040_king_casius-1-e" then 
	end
	if npc_talk_index == "n040_king_casius-1-f" then 
	end
	if npc_talk_index == "n040_king_casius-1-g" then 
	end
	if npc_talk_index == "n040_king_casius-1-h" then 
	end
	if npc_talk_index == "n040_king_casius-1-i" then 
	end
	if npc_talk_index == "n040_king_casius-1-j" then 
	end
	if npc_talk_index == "n040_king_casius-1-k" then 
	end
	if npc_talk_index == "n040_king_casius-1-l" then 
	end
	if npc_talk_index == "n040_king_casius-1-l" then 
	end
	if npc_talk_index == "n040_king_casius-1-l" then 
	end
	if npc_talk_index == "n040_king_casius-1-m" then 
	end
	if npc_talk_index == "n040_king_casius-1-n" then 
	end
	if npc_talk_index == "n040_king_casius-1-o" then 
	end
	if npc_talk_index == "n040_king_casius-1-p" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mqc14_9410_the_outbreak_of_war_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9410_the_outbreak_of_war_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9410);
end

function mqc14_9410_the_outbreak_of_war_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9410);
end

function mqc14_9410_the_outbreak_of_war_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9410);
	local questID=9410;
end

function mqc14_9410_the_outbreak_of_war_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9410_the_outbreak_of_war_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9410_the_outbreak_of_war_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9410, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9410, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9410, 1);
				npc_talk_index = "n249_saint_guard-1";
end

</GameServer>