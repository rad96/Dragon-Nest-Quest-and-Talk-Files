<VillageServer>

function mq01_014_scattered_evidence_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 4 then
		mq01_014_scattered_evidence_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 8 then
		mq01_014_scattered_evidence_OnTalk_n008_guard_timose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n004_guard_steave--------------------------------------------------------------------------------
function mq01_014_scattered_evidence_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n004_guard_steave-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n004_guard_steave-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-accepting-acceptted" then
				api_quest_AddQuest(userObjID,14, 2);
				api_quest_SetJournalStep(userObjID,14, 1);
				api_quest_SetQuestStep(userObjID,14, 1);
				npc_talk_index = "n004_guard_steave-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n008_guard_timose--------------------------------------------------------------------------------
function mq01_014_scattered_evidence_OnTalk_n008_guard_timose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n008_guard_timose-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n008_guard_timose-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n008_guard_timose-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n008_guard_timose-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n008_guard_timose-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 102, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 104, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300057, 30002);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n008_guard_timose-3-b" then 
	end
	if npc_talk_index == "n008_guard_timose-4" then 

				if api_quest_HasQuestItem(userObjID, 300057, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300057, api_quest_HasQuestItem(userObjID, 300057, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300058, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300058, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n008_guard_timose-4-b" then 
	end
	if npc_talk_index == "n008_guard_timose-4-c" then 
	end
	if npc_talk_index == "n008_guard_timose-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 141, true);
				 api_quest_RewardQuestUser(userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 142, true);
				 api_quest_RewardQuestUser(userObjID, 142, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 141, true);
				 api_quest_RewardQuestUser(userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 141, true);
				 api_quest_RewardQuestUser(userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 141, true);
				 api_quest_RewardQuestUser(userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 141, true);
				 api_quest_RewardQuestUser(userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 141, true);
				 api_quest_RewardQuestUser(userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 141, true);
				 api_quest_RewardQuestUser(userObjID, 141, questID, 1);
			 end 
	end
	if npc_talk_index == "n008_guard_timose-4-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									if api_user_GetUserClassID(userObjID) == 1 then
									api_quest_AddQuest(userObjID,15, 2);
				api_quest_SetJournalStep(userObjID,15, 1);
				api_quest_SetQuestStep(userObjID,15, 1);

				else
									api_quest_AddQuest(userObjID,16, 2);
				api_quest_SetJournalStep(userObjID,16, 1);
				api_quest_SetQuestStep(userObjID,16, 1);

				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_014_scattered_evidence_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 14);
	if qstep == 2 and CountIndex == 102 then
				if api_quest_HasQuestItem(userObjID, 300057, 3) == -3 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300057, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300057, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 2 and CountIndex == 104 then
				if api_quest_HasQuestItem(userObjID, 300057, 3) >= 3 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300057, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300057, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 300057 then

	end
end

function mq01_014_scattered_evidence_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 14);
	if qstep == 2 and CountIndex == 102 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 104 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300057 and Count >= TargetCount  then

	end
end

function mq01_014_scattered_evidence_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 14);
	local questID=14;
end

function mq01_014_scattered_evidence_OnRemoteStart( userObjID, questID )
end

function mq01_014_scattered_evidence_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq01_014_scattered_evidence_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,14, 2);
				api_quest_SetJournalStep(userObjID,14, 1);
				api_quest_SetQuestStep(userObjID,14, 1);
				npc_talk_index = "n004_guard_steave-1";
end

</VillageServer>

<GameServer>
function mq01_014_scattered_evidence_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 4 then
		mq01_014_scattered_evidence_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 8 then
		mq01_014_scattered_evidence_OnTalk_n008_guard_timose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n004_guard_steave--------------------------------------------------------------------------------
function mq01_014_scattered_evidence_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n004_guard_steave-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n004_guard_steave-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,14, 2);
				api_quest_SetJournalStep( pRoom, userObjID,14, 1);
				api_quest_SetQuestStep( pRoom, userObjID,14, 1);
				npc_talk_index = "n004_guard_steave-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n008_guard_timose--------------------------------------------------------------------------------
function mq01_014_scattered_evidence_OnTalk_n008_guard_timose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n008_guard_timose-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n008_guard_timose-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n008_guard_timose-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n008_guard_timose-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n008_guard_timose-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 102, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 104, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300057, 30002);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n008_guard_timose-3-b" then 
	end
	if npc_talk_index == "n008_guard_timose-4" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300057, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300057, api_quest_HasQuestItem( pRoom, userObjID, 300057, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300058, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300058, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n008_guard_timose-4-b" then 
	end
	if npc_talk_index == "n008_guard_timose-4-c" then 
	end
	if npc_talk_index == "n008_guard_timose-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 141, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 142, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 142, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 141, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 141, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 141, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 141, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 141, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 141, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 141, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 141, questID, 1);
			 end 
	end
	if npc_talk_index == "n008_guard_timose-4-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									api_quest_AddQuest( pRoom, userObjID,15, 2);
				api_quest_SetJournalStep( pRoom, userObjID,15, 1);
				api_quest_SetQuestStep( pRoom, userObjID,15, 1);

				else
									api_quest_AddQuest( pRoom, userObjID,16, 2);
				api_quest_SetJournalStep( pRoom, userObjID,16, 1);
				api_quest_SetQuestStep( pRoom, userObjID,16, 1);

				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq01_014_scattered_evidence_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 14);
	if qstep == 2 and CountIndex == 102 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300057, 3) == -3 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300057, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300057, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 2 and CountIndex == 104 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300057, 3) >= 3 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300057, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300057, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 300057 then

	end
end

function mq01_014_scattered_evidence_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 14);
	if qstep == 2 and CountIndex == 102 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 104 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300057 and Count >= TargetCount  then

	end
end

function mq01_014_scattered_evidence_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 14);
	local questID=14;
end

function mq01_014_scattered_evidence_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq01_014_scattered_evidence_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq01_014_scattered_evidence_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,14, 2);
				api_quest_SetJournalStep( pRoom, userObjID,14, 1);
				api_quest_SetQuestStep( pRoom, userObjID,14, 1);
				npc_talk_index = "n004_guard_steave-1";
end

</GameServer>