<VillageServer>

function sq11_567_prince_of_kalaen1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 38 then
		sq11_567_prince_of_kalaen1_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function sq11_567_prince_of_kalaen1_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5671, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5672, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5673, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5674, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5675, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5676, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5677, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5678, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5679, false);
			 end 

	end
	if npc_talk_index == "n038_royal_magician_kalaen-accepting-d" then
				api_quest_AddQuest(userObjID,567, 1);
				api_quest_SetJournalStep(userObjID,567, 1);
				api_quest_SetQuestStep(userObjID,567, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 931, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200931, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300251, 1);

	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5671, true);
				 api_quest_RewardQuestUser(userObjID, 5671, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5672, true);
				 api_quest_RewardQuestUser(userObjID, 5672, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5673, true);
				 api_quest_RewardQuestUser(userObjID, 5673, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5674, true);
				 api_quest_RewardQuestUser(userObjID, 5674, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5675, true);
				 api_quest_RewardQuestUser(userObjID, 5675, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5676, true);
				 api_quest_RewardQuestUser(userObjID, 5676, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5677, true);
				 api_quest_RewardQuestUser(userObjID, 5677, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5678, true);
				 api_quest_RewardQuestUser(userObjID, 5678, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5679, true);
				 api_quest_RewardQuestUser(userObjID, 5679, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem(userObjID, 300251, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300251, api_quest_HasQuestItem(userObjID, 300251, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 568, 1);
					api_quest_SetQuestStep(userObjID, 568, 1);
					api_quest_SetJournalStep(userObjID, 568, 1);

									api_npc_AddFavorPoint( userObjID, 38, 300 );


				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n038_royal_magician_kalaen-1", "sq11_568_prince_of_kalaen2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_567_prince_of_kalaen1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 567);
	if qstep == 1 and CountIndex == 931 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300251 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200931 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_567_prince_of_kalaen1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 567);
	if qstep == 1 and CountIndex == 931 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300251 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200931 and Count >= TargetCount  then

	end
end

function sq11_567_prince_of_kalaen1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 567);
	local questID=567;
end

function sq11_567_prince_of_kalaen1_OnRemoteStart( userObjID, questID )
end

function sq11_567_prince_of_kalaen1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_567_prince_of_kalaen1_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_567_prince_of_kalaen1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 38 then
		sq11_567_prince_of_kalaen1_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function sq11_567_prince_of_kalaen1_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n038_royal_magician_kalaen-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5671, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5672, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5673, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5674, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5675, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5676, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5677, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5678, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5679, false);
			 end 

	end
	if npc_talk_index == "n038_royal_magician_kalaen-accepting-d" then
				api_quest_AddQuest( pRoom, userObjID,567, 1);
				api_quest_SetJournalStep( pRoom, userObjID,567, 1);
				api_quest_SetQuestStep( pRoom, userObjID,567, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 931, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200931, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300251, 1);

	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5671, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5671, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5672, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5672, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5673, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5673, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5674, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5674, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5675, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5675, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5676, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5676, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5677, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5677, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5678, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5678, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5679, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5679, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem( pRoom, userObjID, 300251, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300251, api_quest_HasQuestItem( pRoom, userObjID, 300251, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 568, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 568, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 568, 1);

									api_npc_AddFavorPoint( pRoom,  userObjID, 38, 300 );


				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n038_royal_magician_kalaen-1", "sq11_568_prince_of_kalaen2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_567_prince_of_kalaen1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 567);
	if qstep == 1 and CountIndex == 931 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 300251 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 200931 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300251, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_567_prince_of_kalaen1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 567);
	if qstep == 1 and CountIndex == 931 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 300251 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200931 and Count >= TargetCount  then

	end
end

function sq11_567_prince_of_kalaen1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 567);
	local questID=567;
end

function sq11_567_prince_of_kalaen1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_567_prince_of_kalaen1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_567_prince_of_kalaen1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>