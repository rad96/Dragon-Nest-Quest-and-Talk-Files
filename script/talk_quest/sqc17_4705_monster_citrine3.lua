<VillageServer>

function sqc17_4705_monster_citrine3_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1835 then
		sqc17_4705_monster_citrine3_OnTalk_n1835_conscience_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1835_conscience_pope--------------------------------------------------------------------------------
function sqc17_4705_monster_citrine3_OnTalk_n1835_conscience_pope(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1835_conscience_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1835_conscience_pope-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1835_conscience_pope-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1835_conscience_pope-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1835_conscience_pope-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47050, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47050, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47050, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47050, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47050, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47050, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47050, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47050, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47050, false);
			 end 

	end
	if npc_talk_index == "n1835_conscience_pope-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4705, 1);
				api_quest_SetJournalStep(userObjID,4705, 1);
				api_quest_SetQuestStep(userObjID,4705, 1);
				npc_talk_index = "n1835_conscience_pope-1";

	end
	if npc_talk_index == "n1835_conscience_pope-1-acceptted" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 603279, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 703279, 30000);
	end
	if npc_talk_index == "n1835_conscience_pope-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47050, true);
				 api_quest_RewardQuestUser(userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47050, true);
				 api_quest_RewardQuestUser(userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47050, true);
				 api_quest_RewardQuestUser(userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47050, true);
				 api_quest_RewardQuestUser(userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47050, true);
				 api_quest_RewardQuestUser(userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47050, true);
				 api_quest_RewardQuestUser(userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47050, true);
				 api_quest_RewardQuestUser(userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47050, true);
				 api_quest_RewardQuestUser(userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47050, true);
				 api_quest_RewardQuestUser(userObjID, 47050, questID, 1);
			 end 
	end
	if npc_talk_index == "n1835_conscience_pope-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4706, 1);
					api_quest_SetQuestStep(userObjID, 4706, 1);
					api_quest_SetJournalStep(userObjID, 4706, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1835_conscience_pope-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1835_conscience_pope-1", "sqc17_4706_monster_citrine4.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4705_monster_citrine3_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4705);
	if qstep == 2 and CountIndex == 603279 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 703279 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function sqc17_4705_monster_citrine3_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4705);
	if qstep == 2 and CountIndex == 603279 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 703279 and Count >= TargetCount  then

	end
end

function sqc17_4705_monster_citrine3_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4705);
	local questID=4705;
end

function sqc17_4705_monster_citrine3_OnRemoteStart( userObjID, questID )
end

function sqc17_4705_monster_citrine3_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc17_4705_monster_citrine3_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4705, 1);
				api_quest_SetJournalStep(userObjID,4705, 1);
				api_quest_SetQuestStep(userObjID,4705, 1);
				npc_talk_index = "n1835_conscience_pope-1";
end

</VillageServer>

<GameServer>
function sqc17_4705_monster_citrine3_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1835 then
		sqc17_4705_monster_citrine3_OnTalk_n1835_conscience_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1835_conscience_pope--------------------------------------------------------------------------------
function sqc17_4705_monster_citrine3_OnTalk_n1835_conscience_pope( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1835_conscience_pope-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1835_conscience_pope-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1835_conscience_pope-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1835_conscience_pope-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1835_conscience_pope-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, false);
			 end 

	end
	if npc_talk_index == "n1835_conscience_pope-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4705, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4705, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4705, 1);
				npc_talk_index = "n1835_conscience_pope-1";

	end
	if npc_talk_index == "n1835_conscience_pope-1-acceptted" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 603279, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 703279, 30000);
	end
	if npc_talk_index == "n1835_conscience_pope-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47050, questID, 1);
			 end 
	end
	if npc_talk_index == "n1835_conscience_pope-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4706, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4706, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4706, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1835_conscience_pope-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1835_conscience_pope-1", "sqc17_4706_monster_citrine4.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4705_monster_citrine3_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4705);
	if qstep == 2 and CountIndex == 603279 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 703279 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function sqc17_4705_monster_citrine3_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4705);
	if qstep == 2 and CountIndex == 603279 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 703279 and Count >= TargetCount  then

	end
end

function sqc17_4705_monster_citrine3_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4705);
	local questID=4705;
end

function sqc17_4705_monster_citrine3_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc17_4705_monster_citrine3_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc17_4705_monster_citrine3_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4705, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4705, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4705, 1);
				npc_talk_index = "n1835_conscience_pope-1";
end

</GameServer>