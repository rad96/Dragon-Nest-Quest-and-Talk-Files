<VillageServer>

function mq08_211_red_army_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 164 then
		mq08_211_red_army_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mq08_211_red_army_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function mq08_211_red_army_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n164_argenta_wounded-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n164_argenta_wounded-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_211_red_army_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest(userObjID,211, 2);
				api_quest_SetJournalStep(userObjID,211, 1);
				api_quest_SetQuestStep(userObjID,211, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-levelcheck" then 
				if api_quest_IsMarkingCompleteQuest(userObjID, 238) == 1  or api_quest_IsMarkingCompleteQuest(userObjID, 239) == 1  or api_quest_IsMarkingCompleteQuest(userObjID, 240) == 1  or api_quest_IsMarkingCompleteQuest(userObjID, 241) == 1  or api_quest_IsMarkingCompleteQuest(userObjID, 242) == 1  or api_quest_IsMarkingCompleteQuest(userObjID, 243) == 1  or api_quest_IsMarkingCompleteQuest(userObjID, 244) == 1  or api_quest_IsMarkingCompleteQuest(userObjID, 245) == 1 then
									if api_user_GetUserLevel(userObjID) < 16 then
									npc_talk_index = "n024_adventurer_guildmaster_decud-1-a";

				else
									npc_talk_index = "n024_adventurer_guildmaster_decud-1-b";

				end

				else
									npc_talk_index = "n024_adventurer_guildmaster_decud-1-g";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 492, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200492, 30000);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2111, true);
				 api_quest_RewardQuestUser(userObjID, 2111, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2112, true);
				 api_quest_RewardQuestUser(userObjID, 2112, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2113, true);
				 api_quest_RewardQuestUser(userObjID, 2113, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2114, true);
				 api_quest_RewardQuestUser(userObjID, 2114, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2116, true);
				 api_quest_RewardQuestUser(userObjID, 2116, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2116, true);
				 api_quest_RewardQuestUser(userObjID, 2116, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2111, true);
				 api_quest_RewardQuestUser(userObjID, 2111, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2118, true);
				 api_quest_RewardQuestUser(userObjID, 2118, questID, 1);
			 end 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 212, 2);
					api_quest_SetQuestStep(userObjID, 212, 1);
					api_quest_SetJournalStep(userObjID, 212, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n024_adventurer_guildmaster_decud-1", "mq08_212_elimination_hobgoblin.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_211_red_army_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 211);
	if qstep == 2 and CountIndex == 492 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 200492 then
				api_quest_IncCounting(userObjID, 2, 492);

	end
end

function mq08_211_red_army_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 211);
	if qstep == 2 and CountIndex == 492 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200492 and Count >= TargetCount  then

	end
end

function mq08_211_red_army_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 211);
	local questID=211;
end

function mq08_211_red_army_OnRemoteStart( userObjID, questID )
end

function mq08_211_red_army_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_211_red_army_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,211, 2);
				api_quest_SetJournalStep(userObjID,211, 1);
				api_quest_SetQuestStep(userObjID,211, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</VillageServer>

<GameServer>
function mq08_211_red_army_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 164 then
		mq08_211_red_army_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		mq08_211_red_army_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n164_argenta_wounded--------------------------------------------------------------------------------
function mq08_211_red_army_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n164_argenta_wounded-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n164_argenta_wounded-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_211_red_army_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,211, 2);
				api_quest_SetJournalStep( pRoom, userObjID,211, 1);
				api_quest_SetQuestStep( pRoom, userObjID,211, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-levelcheck" then 
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 238) == 1  or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 239) == 1  or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 240) == 1  or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 241) == 1  or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 242) == 1  or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 243) == 1  or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 244) == 1  or api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 245) == 1 then
									if api_user_GetUserLevel( pRoom, userObjID) < 16 then
									npc_talk_index = "n024_adventurer_guildmaster_decud-1-a";

				else
									npc_talk_index = "n024_adventurer_guildmaster_decud-1-b";

				end

				else
									npc_talk_index = "n024_adventurer_guildmaster_decud-1-g";

				end
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 492, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200492, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2111, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2111, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2112, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2112, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2113, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2113, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2114, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2114, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2116, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2116, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2116, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2116, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2111, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2111, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2118, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2118, questID, 1);
			 end 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 212, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 212, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 212, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n024_adventurer_guildmaster_decud-1", "mq08_212_elimination_hobgoblin.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_211_red_army_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 211);
	if qstep == 2 and CountIndex == 492 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 200492 then
				api_quest_IncCounting( pRoom, userObjID, 2, 492);

	end
end

function mq08_211_red_army_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 211);
	if qstep == 2 and CountIndex == 492 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200492 and Count >= TargetCount  then

	end
end

function mq08_211_red_army_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 211);
	local questID=211;
end

function mq08_211_red_army_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq08_211_red_army_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_211_red_army_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,211, 2);
				api_quest_SetJournalStep( pRoom, userObjID,211, 1);
				api_quest_SetQuestStep( pRoom, userObjID,211, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
end

</GameServer>