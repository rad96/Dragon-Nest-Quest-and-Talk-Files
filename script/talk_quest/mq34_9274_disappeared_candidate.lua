<VillageServer>

function mq34_9274_disappeared_candidate_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9274_disappeared_candidate_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1058 then
		mq34_9274_disappeared_candidate_OnTalk_n1058_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1059 then
		mq34_9274_disappeared_candidate_OnTalk_n1059_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mq34_9274_disappeared_candidate_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1074 then
		mq34_9274_disappeared_candidate_OnTalk_n1074_azelia_dead(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9274_disappeared_candidate_OnTalk_n1047_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1047_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1047_argenta-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-4-b" then 
	end
	if npc_talk_index == "n1047_argenta-4-b" then 
	end
	if npc_talk_index == "n1047_argenta-4-c" then 
	end
	if npc_talk_index == "n1047_argenta-4-d" then 
	end
	if npc_talk_index == "n1047_argenta-4-e" then 
	end
	if npc_talk_index == "n1047_argenta-4-f" then 
	end
	if npc_talk_index == "n1047_argenta-4-g" then 
	end
	if npc_talk_index == "n1047_argenta-4-h" then 
	end
	if npc_talk_index == "n1047_argenta-4-i" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1058_yuvenciel--------------------------------------------------------------------------------
function mq34_9274_disappeared_candidate_OnTalk_n1058_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1058_yuvenciel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1058_yuvenciel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1058_yuvenciel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1058_yuvenciel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1058_yuvenciel-3-b" then 
	end
	if npc_talk_index == "n1058_yuvenciel-3-c" then 
	end
	if npc_talk_index == "n1058_yuvenciel-3-d" then 
	end
	if npc_talk_index == "n1058_yuvenciel-3-e" then 
	end
	if npc_talk_index == "n1058_yuvenciel-3-e" then 
	end
	if npc_talk_index == "n1058_yuvenciel-3-f" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1059_yuvenciel--------------------------------------------------------------------------------
function mq34_9274_disappeared_candidate_OnTalk_n1059_yuvenciel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1059_yuvenciel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1059_yuvenciel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9274_disappeared_candidate_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1067_elder_elf-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9274, 2);
				api_quest_SetJournalStep(userObjID,9274, 1);
				api_quest_SetQuestStep(userObjID,9274, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-m" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-5-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-5-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-5-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-5-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-5-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-5-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92740, true);
				 api_quest_RewardQuestUser(userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92740, true);
				 api_quest_RewardQuestUser(userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92740, true);
				 api_quest_RewardQuestUser(userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92740, true);
				 api_quest_RewardQuestUser(userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92740, true);
				 api_quest_RewardQuestUser(userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92740, true);
				 api_quest_RewardQuestUser(userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92740, true);
				 api_quest_RewardQuestUser(userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92740, true);
				 api_quest_RewardQuestUser(userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92740, true);
				 api_quest_RewardQuestUser(userObjID, 92740, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9275, 2);
					api_quest_SetQuestStep(userObjID, 9275, 1);
					api_quest_SetJournalStep(userObjID, 9275, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1067_elder_elf-1", "mq34_9275_clue_that_she_left.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1074_azelia_dead--------------------------------------------------------------------------------
function mq34_9274_disappeared_candidate_OnTalk_n1074_azelia_dead(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1074_azelia_dead-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1074_azelia_dead-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1074_azelia_dead-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1074_azelia_dead-2-b" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9274_disappeared_candidate_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9274);
end

function mq34_9274_disappeared_candidate_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9274);
end

function mq34_9274_disappeared_candidate_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9274);
	local questID=9274;
end

function mq34_9274_disappeared_candidate_OnRemoteStart( userObjID, questID )
end

function mq34_9274_disappeared_candidate_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9274_disappeared_candidate_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9274, 2);
				api_quest_SetJournalStep(userObjID,9274, 1);
				api_quest_SetQuestStep(userObjID,9274, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</VillageServer>

<GameServer>
function mq34_9274_disappeared_candidate_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1047 then
		mq34_9274_disappeared_candidate_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1058 then
		mq34_9274_disappeared_candidate_OnTalk_n1058_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1059 then
		mq34_9274_disappeared_candidate_OnTalk_n1059_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mq34_9274_disappeared_candidate_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1074 then
		mq34_9274_disappeared_candidate_OnTalk_n1074_azelia_dead( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1047_argenta--------------------------------------------------------------------------------
function mq34_9274_disappeared_candidate_OnTalk_n1047_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1047_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1047_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1047_argenta-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1047_argenta-4-b" then 
	end
	if npc_talk_index == "n1047_argenta-4-b" then 
	end
	if npc_talk_index == "n1047_argenta-4-c" then 
	end
	if npc_talk_index == "n1047_argenta-4-d" then 
	end
	if npc_talk_index == "n1047_argenta-4-e" then 
	end
	if npc_talk_index == "n1047_argenta-4-f" then 
	end
	if npc_talk_index == "n1047_argenta-4-g" then 
	end
	if npc_talk_index == "n1047_argenta-4-h" then 
	end
	if npc_talk_index == "n1047_argenta-4-i" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1058_yuvenciel--------------------------------------------------------------------------------
function mq34_9274_disappeared_candidate_OnTalk_n1058_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1058_yuvenciel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1058_yuvenciel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1058_yuvenciel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1058_yuvenciel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1058_yuvenciel-3-b" then 
	end
	if npc_talk_index == "n1058_yuvenciel-3-c" then 
	end
	if npc_talk_index == "n1058_yuvenciel-3-d" then 
	end
	if npc_talk_index == "n1058_yuvenciel-3-e" then 
	end
	if npc_talk_index == "n1058_yuvenciel-3-e" then 
	end
	if npc_talk_index == "n1058_yuvenciel-3-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1059_yuvenciel--------------------------------------------------------------------------------
function mq34_9274_disappeared_candidate_OnTalk_n1059_yuvenciel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1059_yuvenciel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1059_yuvenciel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9274_disappeared_candidate_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1067_elder_elf-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9274, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9274, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9274, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-m" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-5-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-5-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-5-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-5-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-5-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-5-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92740, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92740, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92740, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9275, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9275, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9275, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1067_elder_elf-1", "mq34_9275_clue_that_she_left.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1074_azelia_dead--------------------------------------------------------------------------------
function mq34_9274_disappeared_candidate_OnTalk_n1074_azelia_dead( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1074_azelia_dead-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1074_azelia_dead-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1074_azelia_dead-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1074_azelia_dead-2-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9274_disappeared_candidate_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9274);
end

function mq34_9274_disappeared_candidate_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9274);
end

function mq34_9274_disappeared_candidate_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9274);
	local questID=9274;
end

function mq34_9274_disappeared_candidate_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9274_disappeared_candidate_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9274_disappeared_candidate_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9274, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9274, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9274, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</GameServer>