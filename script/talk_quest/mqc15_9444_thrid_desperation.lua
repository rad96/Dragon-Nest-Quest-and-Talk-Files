<VillageServer>

function mqc15_9444_thrid_desperation_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1493 then
		mqc15_9444_thrid_desperation_OnTalk_n1493_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1494 then
		mqc15_9444_thrid_desperation_OnTalk_n1494_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1558 then
		mqc15_9444_thrid_desperation_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1566 then
		mqc15_9444_thrid_desperation_OnTalk_n1566_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1571 then
		mqc15_9444_thrid_desperation_OnTalk_n1571_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1583 then
		mqc15_9444_thrid_desperation_OnTalk_n1583_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1587 then
		mqc15_9444_thrid_desperation_OnTalk_n1587_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1588 then
		mqc15_9444_thrid_desperation_OnTalk_n1588_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1493_argenta--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1493_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1493_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1493_argenta-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1493_argenta-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1493_argenta-4-b" then 
	end
	if npc_talk_index == "n1493_argenta-4-c" then 
	end
	if npc_talk_index == "n1493_argenta-4-d" then 
	end
	if npc_talk_index == "n1493_argenta-4-e" then 
	end
	if npc_talk_index == "n1493_argenta-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1494_pether--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1494_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1494_pether-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1494_pether-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1494_pether-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1494_pether-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1494_pether-6-b" then 
	end
	if npc_talk_index == "n1494_pether-6-c" then 
	end
	if npc_talk_index == "n1494_pether-6-d" then 
	end
	if npc_talk_index == "n1494_pether-6-e" then 
	end
	if npc_talk_index == "n1494_pether-6-f" then 
	end
	if npc_talk_index == "n1494_pether-6-g" then 
	end
	if npc_talk_index == "n1494_pether-6-h" then 
	end
	if npc_talk_index == "n1494_pether-6-i" then 
	end
	if npc_talk_index == "n1494_pether-6-j" then 
	end
	if npc_talk_index == "n1494_pether-6-k" then 
	end
	if npc_talk_index == "n1494_pether-6-l" then 
	end
	if npc_talk_index == "n1494_pether-6-m" then 
	end
	if npc_talk_index == "n1494_pether-6-n" then 
	end
	if npc_talk_index == "n1494_pether-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
	end
	if npc_talk_index == "n1494_pether-8-b" then 
	end
	if npc_talk_index == "n1494_pether-8-c" then 
	end
	if npc_talk_index == "n1494_pether-8-d" then 
	end
	if npc_talk_index == "n1494_pether-8-e" then 
	end
	if npc_talk_index == "n1494_pether-8-f" then 
	end
	if npc_talk_index == "n1494_pether-8-g" then 
	end
	if npc_talk_index == "n1494_pether-8-h" then 
	end
	if npc_talk_index == "n1494_pether-8-i" then 
	end
	if npc_talk_index == "n1494_pether-8-j" then 
	end
	if npc_talk_index == "n1494_pether-8-k" then 
	end
	if npc_talk_index == "n1494_pether-8-l" then 
	end
	if npc_talk_index == "n1494_pether-8-m" then 
	end
	if npc_talk_index == "n1494_pether-8-n" then 
	end
	if npc_talk_index == "n1494_pether-8-o" then 
	end
	if npc_talk_index == "n1494_pether-8-p" then 
	end
	if npc_talk_index == "n1494_pether-8-q" then 
	end
	if npc_talk_index == "n1494_pether-8-r" then 
	end
	if npc_talk_index == "n1494_pether-9" then 
				api_quest_SetQuestStep(userObjID, questID,9);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1558_gaharam-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1558_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1558_gaharam-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9444, 2);
				api_quest_SetJournalStep(userObjID,9444, 1);
				api_quest_SetQuestStep(userObjID,9444, 1);
				npc_talk_index = "n1558_gaharam-1";

	end
	if npc_talk_index == "n1558_gaharam-1-b" then 
	end
	if npc_talk_index == "n1558_gaharam-1-c" then 
	end
	if npc_talk_index == "n1558_gaharam-1-d" then 
	end
	if npc_talk_index == "n1558_gaharam-1-e" then 
	end
	if npc_talk_index == "n1558_gaharam-1-f" then 
	end
	if npc_talk_index == "n1558_gaharam-1-g" then 
	end
	if npc_talk_index == "n1558_gaharam-1-h" then 
	end
	if npc_talk_index == "n1558_gaharam-1-i" then 
	end
	if npc_talk_index == "n1558_gaharam-1-j" then 
	end
	if npc_talk_index == "n1558_gaharam-1-k" then 
	end
	if npc_talk_index == "n1558_gaharam-1-l" then 
	end
	if npc_talk_index == "n1558_gaharam-1-m" then 
	end
	if npc_talk_index == "n1558_gaharam-1-n" then 
	end
	if npc_talk_index == "n1558_gaharam-1-o" then 
	end
	if npc_talk_index == "n1558_gaharam-1-p" then 
	end
	if npc_talk_index == "n1558_gaharam-1-q" then 
	end
	if npc_talk_index == "n1558_gaharam-1-r" then 
	end
	if npc_talk_index == "n1558_gaharam-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1566_pether--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1566_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1566_pether-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1566_pether-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1566_pether-2-b" then 
	end
	if npc_talk_index == "n1566_pether-2-c" then 
	end
	if npc_talk_index == "n1566_pether-2-d" then 
	end
	if npc_talk_index == "n1566_pether-2-e" then 
	end
	if npc_talk_index == "n1566_pether-2-f" then 
	end
	if npc_talk_index == "n1566_pether-2-g" then 
	end
	if npc_talk_index == "n1566_pether-2-h" then 
	end
	if npc_talk_index == "n1566_pether-2-i" then 
	end
	if npc_talk_index == "n1566_pether-2-j" then 
	end
	if npc_talk_index == "n1566_pether-2-k" then 
	end
	if npc_talk_index == "n1566_pether-2-l" then 
	end
	if npc_talk_index == "n1566_pether-2-m" then 
	end
	if npc_talk_index == "n1566_pether-2-n" then 
	end
	if npc_talk_index == "n1566_pether-2-o" then 
	end
	if npc_talk_index == "n1566_pether-2-p" then 
	end
	if npc_talk_index == "n1566_pether-2-q" then 
	end
	if npc_talk_index == "n1566_pether-2-r" then 
	end
	if npc_talk_index == "n1566_pether-2-s" then 
	end
	if npc_talk_index == "n1566_pether-2-t" then 
	end
	if npc_talk_index == "n1566_pether-2-u" then 
	end
	if npc_talk_index == "n1566_pether-2-v" then 
	end
	if npc_talk_index == "n1566_pether-2-w" then 
	end
	if npc_talk_index == "n1566_pether-2-x" then 
	end
	if npc_talk_index == "n1566_pether-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1571_meriendel--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1571_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1571_meriendel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1583_argenta--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1583_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1587_for_memory--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1587_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n1587_for_memory-10";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1587_for_memory-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1587_for_memory-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1587_for_memory-10-b" then 
	end
	if npc_talk_index == "n1587_for_memory-10-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94440, true);
				 api_quest_RewardQuestUser(userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94440, true);
				 api_quest_RewardQuestUser(userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94440, true);
				 api_quest_RewardQuestUser(userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94440, true);
				 api_quest_RewardQuestUser(userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94440, true);
				 api_quest_RewardQuestUser(userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94440, true);
				 api_quest_RewardQuestUser(userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94440, true);
				 api_quest_RewardQuestUser(userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94440, true);
				 api_quest_RewardQuestUser(userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94440, true);
				 api_quest_RewardQuestUser(userObjID, 94440, questID, 1);
			 end 
	end
	if npc_talk_index == "n1587_for_memory-10-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9445, 2);
					api_quest_SetQuestStep(userObjID, 9445, 1);
					api_quest_SetJournalStep(userObjID, 9445, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1587_for_memory-10-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1587_for_memory-1", "mqc15_9445_forgiveness.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1588_pether--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1588_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9444_thrid_desperation_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9444);
end

function mqc15_9444_thrid_desperation_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9444);
end

function mqc15_9444_thrid_desperation_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9444);
	local questID=9444;
end

function mqc15_9444_thrid_desperation_OnRemoteStart( userObjID, questID )
end

function mqc15_9444_thrid_desperation_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9444_thrid_desperation_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9444, 2);
				api_quest_SetJournalStep(userObjID,9444, 1);
				api_quest_SetQuestStep(userObjID,9444, 1);
				npc_talk_index = "n1558_gaharam-1";
end

</VillageServer>

<GameServer>
function mqc15_9444_thrid_desperation_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1493 then
		mqc15_9444_thrid_desperation_OnTalk_n1493_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1494 then
		mqc15_9444_thrid_desperation_OnTalk_n1494_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1558 then
		mqc15_9444_thrid_desperation_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1566 then
		mqc15_9444_thrid_desperation_OnTalk_n1566_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1571 then
		mqc15_9444_thrid_desperation_OnTalk_n1571_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1583 then
		mqc15_9444_thrid_desperation_OnTalk_n1583_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1587 then
		mqc15_9444_thrid_desperation_OnTalk_n1587_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1588 then
		mqc15_9444_thrid_desperation_OnTalk_n1588_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1493_argenta--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1493_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1493_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1493_argenta-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1493_argenta-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1493_argenta-4-b" then 
	end
	if npc_talk_index == "n1493_argenta-4-c" then 
	end
	if npc_talk_index == "n1493_argenta-4-d" then 
	end
	if npc_talk_index == "n1493_argenta-4-e" then 
	end
	if npc_talk_index == "n1493_argenta-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1494_pether--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1494_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1494_pether-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1494_pether-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1494_pether-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1494_pether-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1494_pether-6-b" then 
	end
	if npc_talk_index == "n1494_pether-6-c" then 
	end
	if npc_talk_index == "n1494_pether-6-d" then 
	end
	if npc_talk_index == "n1494_pether-6-e" then 
	end
	if npc_talk_index == "n1494_pether-6-f" then 
	end
	if npc_talk_index == "n1494_pether-6-g" then 
	end
	if npc_talk_index == "n1494_pether-6-h" then 
	end
	if npc_talk_index == "n1494_pether-6-i" then 
	end
	if npc_talk_index == "n1494_pether-6-j" then 
	end
	if npc_talk_index == "n1494_pether-6-k" then 
	end
	if npc_talk_index == "n1494_pether-6-l" then 
	end
	if npc_talk_index == "n1494_pether-6-m" then 
	end
	if npc_talk_index == "n1494_pether-6-n" then 
	end
	if npc_talk_index == "n1494_pether-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
	end
	if npc_talk_index == "n1494_pether-8-b" then 
	end
	if npc_talk_index == "n1494_pether-8-c" then 
	end
	if npc_talk_index == "n1494_pether-8-d" then 
	end
	if npc_talk_index == "n1494_pether-8-e" then 
	end
	if npc_talk_index == "n1494_pether-8-f" then 
	end
	if npc_talk_index == "n1494_pether-8-g" then 
	end
	if npc_talk_index == "n1494_pether-8-h" then 
	end
	if npc_talk_index == "n1494_pether-8-i" then 
	end
	if npc_talk_index == "n1494_pether-8-j" then 
	end
	if npc_talk_index == "n1494_pether-8-k" then 
	end
	if npc_talk_index == "n1494_pether-8-l" then 
	end
	if npc_talk_index == "n1494_pether-8-m" then 
	end
	if npc_talk_index == "n1494_pether-8-n" then 
	end
	if npc_talk_index == "n1494_pether-8-o" then 
	end
	if npc_talk_index == "n1494_pether-8-p" then 
	end
	if npc_talk_index == "n1494_pether-8-q" then 
	end
	if npc_talk_index == "n1494_pether-8-r" then 
	end
	if npc_talk_index == "n1494_pether-9" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1558_gaharam-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1558_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1558_gaharam-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9444, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9444, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9444, 1);
				npc_talk_index = "n1558_gaharam-1";

	end
	if npc_talk_index == "n1558_gaharam-1-b" then 
	end
	if npc_talk_index == "n1558_gaharam-1-c" then 
	end
	if npc_talk_index == "n1558_gaharam-1-d" then 
	end
	if npc_talk_index == "n1558_gaharam-1-e" then 
	end
	if npc_talk_index == "n1558_gaharam-1-f" then 
	end
	if npc_talk_index == "n1558_gaharam-1-g" then 
	end
	if npc_talk_index == "n1558_gaharam-1-h" then 
	end
	if npc_talk_index == "n1558_gaharam-1-i" then 
	end
	if npc_talk_index == "n1558_gaharam-1-j" then 
	end
	if npc_talk_index == "n1558_gaharam-1-k" then 
	end
	if npc_talk_index == "n1558_gaharam-1-l" then 
	end
	if npc_talk_index == "n1558_gaharam-1-m" then 
	end
	if npc_talk_index == "n1558_gaharam-1-n" then 
	end
	if npc_talk_index == "n1558_gaharam-1-o" then 
	end
	if npc_talk_index == "n1558_gaharam-1-p" then 
	end
	if npc_talk_index == "n1558_gaharam-1-q" then 
	end
	if npc_talk_index == "n1558_gaharam-1-r" then 
	end
	if npc_talk_index == "n1558_gaharam-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1566_pether--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1566_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1566_pether-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1566_pether-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1566_pether-2-b" then 
	end
	if npc_talk_index == "n1566_pether-2-c" then 
	end
	if npc_talk_index == "n1566_pether-2-d" then 
	end
	if npc_talk_index == "n1566_pether-2-e" then 
	end
	if npc_talk_index == "n1566_pether-2-f" then 
	end
	if npc_talk_index == "n1566_pether-2-g" then 
	end
	if npc_talk_index == "n1566_pether-2-h" then 
	end
	if npc_talk_index == "n1566_pether-2-i" then 
	end
	if npc_talk_index == "n1566_pether-2-j" then 
	end
	if npc_talk_index == "n1566_pether-2-k" then 
	end
	if npc_talk_index == "n1566_pether-2-l" then 
	end
	if npc_talk_index == "n1566_pether-2-m" then 
	end
	if npc_talk_index == "n1566_pether-2-n" then 
	end
	if npc_talk_index == "n1566_pether-2-o" then 
	end
	if npc_talk_index == "n1566_pether-2-p" then 
	end
	if npc_talk_index == "n1566_pether-2-q" then 
	end
	if npc_talk_index == "n1566_pether-2-r" then 
	end
	if npc_talk_index == "n1566_pether-2-s" then 
	end
	if npc_talk_index == "n1566_pether-2-t" then 
	end
	if npc_talk_index == "n1566_pether-2-u" then 
	end
	if npc_talk_index == "n1566_pether-2-v" then 
	end
	if npc_talk_index == "n1566_pether-2-w" then 
	end
	if npc_talk_index == "n1566_pether-2-x" then 
	end
	if npc_talk_index == "n1566_pether-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1571_meriendel--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1571_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1571_meriendel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1583_argenta--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1583_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1587_for_memory--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1587_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n1587_for_memory-10";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1587_for_memory-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1587_for_memory-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1587_for_memory-10-b" then 
	end
	if npc_talk_index == "n1587_for_memory-10-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94440, questID, 1);
			 end 
	end
	if npc_talk_index == "n1587_for_memory-10-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9445, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9445, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9445, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1587_for_memory-10-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1587_for_memory-1", "mqc15_9445_forgiveness.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1588_pether--------------------------------------------------------------------------------
function mqc15_9444_thrid_desperation_OnTalk_n1588_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9444_thrid_desperation_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9444);
end

function mqc15_9444_thrid_desperation_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9444);
end

function mqc15_9444_thrid_desperation_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9444);
	local questID=9444;
end

function mqc15_9444_thrid_desperation_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9444_thrid_desperation_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9444_thrid_desperation_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9444, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9444, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9444, 1);
				npc_talk_index = "n1558_gaharam-1";
end

</GameServer>