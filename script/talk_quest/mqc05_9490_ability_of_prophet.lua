<VillageServer>

function mqc05_9490_ability_of_prophet_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1737 then
		mqc05_9490_ability_of_prophet_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1750 then
		mqc05_9490_ability_of_prophet_OnTalk_n1750_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1737_eltia--------------------------------------------------------------------------------
function mqc05_9490_ability_of_prophet_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1737_eltia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9490, 2);
				api_quest_SetJournalStep(userObjID,9490, 1);
				api_quest_SetQuestStep(userObjID,9490, 1);
				npc_talk_index = "n1737_eltia-1";

	end
	if npc_talk_index == "n1737_eltia-1-check_level32" then 
				if api_user_GetUserLevel(userObjID) >= 32 then
									npc_talk_index = "n1737_eltia-1-b";

				else
									npc_talk_index = "n1737_eltia-1-a";

				end
	end
	if npc_talk_index == "n1737_eltia-1-c" then 
	end
	if npc_talk_index == "n1737_eltia-1-d" then 
	end
	if npc_talk_index == "n1737_eltia-1-e" then 
	end
	if npc_talk_index == "n1737_eltia-1-f" then 
	end
	if npc_talk_index == "n1737_eltia-1-g" then 
	end
	if npc_talk_index == "n1737_eltia-1-h" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1737_eltia-3-b" then 
	end
	if npc_talk_index == "n1737_eltia-3-c" then 
	end
	if npc_talk_index == "n1737_eltia-3-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94900, true);
				 api_quest_RewardQuestUser(userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94900, true);
				 api_quest_RewardQuestUser(userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94900, true);
				 api_quest_RewardQuestUser(userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94900, true);
				 api_quest_RewardQuestUser(userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94900, true);
				 api_quest_RewardQuestUser(userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94900, true);
				 api_quest_RewardQuestUser(userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94900, true);
				 api_quest_RewardQuestUser(userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94900, true);
				 api_quest_RewardQuestUser(userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94900, true);
				 api_quest_RewardQuestUser(userObjID, 94900, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-3-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9491, 2);
					api_quest_SetQuestStep(userObjID, 9491, 1);
					api_quest_SetJournalStep(userObjID, 9491, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-3-f" then 
	end
	if npc_talk_index == "n1737_eltia-3-g" then 
	end
	if npc_talk_index == "n1737_eltia-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1737_eltia-1", "mqc05_9491_comelina.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1750_kaye--------------------------------------------------------------------------------
function mqc05_9490_ability_of_prophet_OnTalk_n1750_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1750_kaye-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1750_kaye-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1750_kaye-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1750_kaye-2-b" then 
	end
	if npc_talk_index == "n1750_kaye-2-c" then 
	end
	if npc_talk_index == "n1750_kaye-2-d" then 
	end
	if npc_talk_index == "n1750_kaye-2-e" then 
	end
	if npc_talk_index == "n1750_kaye-2-f" then 
	end
	if npc_talk_index == "n1750_kaye-2-g" then 
	end
	if npc_talk_index == "n1750_kaye-2-h" then 
	end
	if npc_talk_index == "n1750_kaye-2-i" then 
	end
	if npc_talk_index == "n1750_kaye-2-j" then 
	end
	if npc_talk_index == "n1750_kaye-2-k" then 
	end
	if npc_talk_index == "n1750_kaye-2-l" then 
	end
	if npc_talk_index == "n1750_kaye-2-m" then 
	end
	if npc_talk_index == "n1750_kaye-2-n" then 
	end
	if npc_talk_index == "n1750_kaye-2-o" then 
	end
	if npc_talk_index == "n1750_kaye-2-p" then 
	end
	if npc_talk_index == "n1750_kaye-2-q" then 
	end
	if npc_talk_index == "n1750_kaye-2-r" then 
	end
	if npc_talk_index == "n1750_kaye-2-s" then 
	end
	if npc_talk_index == "n1750_kaye-2-t" then 
	end
	if npc_talk_index == "n1750_kaye-2-u" then 
	end
	if npc_talk_index == "n1750_kaye-2-v" then 
	end
	if npc_talk_index == "n1750_kaye-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9490_ability_of_prophet_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9490);
end

function mqc05_9490_ability_of_prophet_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9490);
end

function mqc05_9490_ability_of_prophet_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9490);
	local questID=9490;
end

function mqc05_9490_ability_of_prophet_OnRemoteStart( userObjID, questID )
end

function mqc05_9490_ability_of_prophet_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc05_9490_ability_of_prophet_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9490, 2);
				api_quest_SetJournalStep(userObjID,9490, 1);
				api_quest_SetQuestStep(userObjID,9490, 1);
				npc_talk_index = "n1737_eltia-1";
end

</VillageServer>

<GameServer>
function mqc05_9490_ability_of_prophet_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1737 then
		mqc05_9490_ability_of_prophet_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1750 then
		mqc05_9490_ability_of_prophet_OnTalk_n1750_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1737_eltia--------------------------------------------------------------------------------
function mqc05_9490_ability_of_prophet_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1737_eltia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9490, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9490, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9490, 1);
				npc_talk_index = "n1737_eltia-1";

	end
	if npc_talk_index == "n1737_eltia-1-check_level32" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 32 then
									npc_talk_index = "n1737_eltia-1-b";

				else
									npc_talk_index = "n1737_eltia-1-a";

				end
	end
	if npc_talk_index == "n1737_eltia-1-c" then 
	end
	if npc_talk_index == "n1737_eltia-1-d" then 
	end
	if npc_talk_index == "n1737_eltia-1-e" then 
	end
	if npc_talk_index == "n1737_eltia-1-f" then 
	end
	if npc_talk_index == "n1737_eltia-1-g" then 
	end
	if npc_talk_index == "n1737_eltia-1-h" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1737_eltia-3-b" then 
	end
	if npc_talk_index == "n1737_eltia-3-c" then 
	end
	if npc_talk_index == "n1737_eltia-3-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94900, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-3-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9491, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9491, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9491, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-3-f" then 
	end
	if npc_talk_index == "n1737_eltia-3-g" then 
	end
	if npc_talk_index == "n1737_eltia-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1737_eltia-1", "mqc05_9491_comelina.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1750_kaye--------------------------------------------------------------------------------
function mqc05_9490_ability_of_prophet_OnTalk_n1750_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1750_kaye-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1750_kaye-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1750_kaye-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1750_kaye-2-b" then 
	end
	if npc_talk_index == "n1750_kaye-2-c" then 
	end
	if npc_talk_index == "n1750_kaye-2-d" then 
	end
	if npc_talk_index == "n1750_kaye-2-e" then 
	end
	if npc_talk_index == "n1750_kaye-2-f" then 
	end
	if npc_talk_index == "n1750_kaye-2-g" then 
	end
	if npc_talk_index == "n1750_kaye-2-h" then 
	end
	if npc_talk_index == "n1750_kaye-2-i" then 
	end
	if npc_talk_index == "n1750_kaye-2-j" then 
	end
	if npc_talk_index == "n1750_kaye-2-k" then 
	end
	if npc_talk_index == "n1750_kaye-2-l" then 
	end
	if npc_talk_index == "n1750_kaye-2-m" then 
	end
	if npc_talk_index == "n1750_kaye-2-n" then 
	end
	if npc_talk_index == "n1750_kaye-2-o" then 
	end
	if npc_talk_index == "n1750_kaye-2-p" then 
	end
	if npc_talk_index == "n1750_kaye-2-q" then 
	end
	if npc_talk_index == "n1750_kaye-2-r" then 
	end
	if npc_talk_index == "n1750_kaye-2-s" then 
	end
	if npc_talk_index == "n1750_kaye-2-t" then 
	end
	if npc_talk_index == "n1750_kaye-2-u" then 
	end
	if npc_talk_index == "n1750_kaye-2-v" then 
	end
	if npc_talk_index == "n1750_kaye-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc05_9490_ability_of_prophet_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9490);
end

function mqc05_9490_ability_of_prophet_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9490);
end

function mqc05_9490_ability_of_prophet_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9490);
	local questID=9490;
end

function mqc05_9490_ability_of_prophet_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc05_9490_ability_of_prophet_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc05_9490_ability_of_prophet_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9490, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9490, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9490, 1);
				npc_talk_index = "n1737_eltia-1";
end

</GameServer>