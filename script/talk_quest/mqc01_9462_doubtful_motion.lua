<VillageServer>

function mqc01_9462_doubtful_motion_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 11 then
		mqc01_9462_doubtful_motion_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1686 then
		mqc01_9462_doubtful_motion_OnTalk_n1686_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 534 then
		mqc01_9462_doubtful_motion_OnTalk_n534_edan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function mqc01_9462_doubtful_motion_OnTalk_n011_priest_leonardo(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n011_priest_leonardo-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-4-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-4-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94620, true);
				 api_quest_RewardQuestUser(userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94620, true);
				 api_quest_RewardQuestUser(userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94620, true);
				 api_quest_RewardQuestUser(userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94620, true);
				 api_quest_RewardQuestUser(userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94620, true);
				 api_quest_RewardQuestUser(userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94620, true);
				 api_quest_RewardQuestUser(userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94620, true);
				 api_quest_RewardQuestUser(userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94620, true);
				 api_quest_RewardQuestUser(userObjID, 94620, questID, 1);
			 end 
	end
	if npc_talk_index == "n011_priest_leonardo-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9463, 2);
					api_quest_SetQuestStep(userObjID, 9463, 1);
					api_quest_SetJournalStep(userObjID, 9463, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n011_priest_leonardo-1", "mqc01_9463_orb_to_find.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1686_eltia--------------------------------------------------------------------------------
function mqc01_9462_doubtful_motion_OnTalk_n1686_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1686_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1686_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1686_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1686_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9462, 2);
				api_quest_SetJournalStep(userObjID,9462, 1);
				api_quest_SetQuestStep(userObjID,9462, 1);
				npc_talk_index = "n1686_eltia-1";

	end
	if npc_talk_index == "n1686_eltia-1-b" then 

				if api_quest_HasQuestItem(userObjID, 400453, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400453, api_quest_HasQuestItem(userObjID, 400453, 1));
				end
	end
	if npc_talk_index == "n1686_eltia-1-c" then 
	end
	if npc_talk_index == "n1686_eltia-1-d" then 
	end
	if npc_talk_index == "n1686_eltia-1-e" then 
	end
	if npc_talk_index == "n1686_eltia-1-f" then 
	end
	if npc_talk_index == "n1686_eltia-1-g" then 
	end
	if npc_talk_index == "n1686_eltia-1-h" then 
	end
	if npc_talk_index == "n1686_eltia-1-i" then 
	end
	if npc_talk_index == "n1686_eltia-1-j" then 
	end
	if npc_talk_index == "n1686_eltia-1-k" then 
	end
	if npc_talk_index == "n1686_eltia-1-l" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n534_edan--------------------------------------------------------------------------------
function mqc01_9462_doubtful_motion_OnTalk_n534_edan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n534_edan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n534_edan-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n534_edan-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n534_edan-3-b" then 
	end
	if npc_talk_index == "n534_edan-3-c" then 
	end
	if npc_talk_index == "n534_edan-3-d" then 
	end
	if npc_talk_index == "n534_edan-3-e" then 
	end
	if npc_talk_index == "n534_edan-3-f" then 
	end
	if npc_talk_index == "n534_edan-3-g" then 
	end
	if npc_talk_index == "n534_edan-3-h" then 
	end
	if npc_talk_index == "n534_edan-3-i" then 
	end
	if npc_talk_index == "n534_edan-3-j" then 
	end
	if npc_talk_index == "n534_edan-3-k" then 
	end
	if npc_talk_index == "n534_edan-3-l" then 
	end
	if npc_talk_index == "n534_edan-3-m" then 
	end
	if npc_talk_index == "n534_edan-3-n" then 
	end
	if npc_talk_index == "n534_edan-3-o" then 
	end
	if npc_talk_index == "n534_edan-3-p" then 
	end
	if npc_talk_index == "n534_edan-3-q" then 
	end
	if npc_talk_index == "n534_edan-3-r" then 
	end
	if npc_talk_index == "n534_edan-3-s" then 
	end
	if npc_talk_index == "n534_edan-3-t" then 
	end
	if npc_talk_index == "n534_edan-3-u" then 
	end
	if npc_talk_index == "n534_edan-3-v" then 
	end
	if npc_talk_index == "n534_edan-3-w" then 
	end
	if npc_talk_index == "n534_edan-3-x" then 
	end
	if npc_talk_index == "n534_edan-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9462_doubtful_motion_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9462);
end

function mqc01_9462_doubtful_motion_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9462);
end

function mqc01_9462_doubtful_motion_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9462);
	local questID=9462;
end

function mqc01_9462_doubtful_motion_OnRemoteStart( userObjID, questID )
end

function mqc01_9462_doubtful_motion_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc01_9462_doubtful_motion_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9462, 2);
				api_quest_SetJournalStep(userObjID,9462, 1);
				api_quest_SetQuestStep(userObjID,9462, 1);
				npc_talk_index = "n1686_eltia-1";
end

</VillageServer>

<GameServer>
function mqc01_9462_doubtful_motion_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 11 then
		mqc01_9462_doubtful_motion_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1686 then
		mqc01_9462_doubtful_motion_OnTalk_n1686_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 534 then
		mqc01_9462_doubtful_motion_OnTalk_n534_edan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n011_priest_leonardo--------------------------------------------------------------------------------
function mqc01_9462_doubtful_motion_OnTalk_n011_priest_leonardo( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n011_priest_leonardo-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n011_priest_leonardo-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n011_priest_leonardo-4-b" then 
	end
	if npc_talk_index == "n011_priest_leonardo-4-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94620, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94620, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94620, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94620, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94620, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94620, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94620, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94620, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94620, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94620, questID, 1);
			 end 
	end
	if npc_talk_index == "n011_priest_leonardo-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9463, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9463, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9463, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n011_priest_leonardo-1", "mqc01_9463_orb_to_find.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1686_eltia--------------------------------------------------------------------------------
function mqc01_9462_doubtful_motion_OnTalk_n1686_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1686_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1686_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1686_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1686_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9462, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9462, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9462, 1);
				npc_talk_index = "n1686_eltia-1";

	end
	if npc_talk_index == "n1686_eltia-1-b" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400453, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400453, api_quest_HasQuestItem( pRoom, userObjID, 400453, 1));
				end
	end
	if npc_talk_index == "n1686_eltia-1-c" then 
	end
	if npc_talk_index == "n1686_eltia-1-d" then 
	end
	if npc_talk_index == "n1686_eltia-1-e" then 
	end
	if npc_talk_index == "n1686_eltia-1-f" then 
	end
	if npc_talk_index == "n1686_eltia-1-g" then 
	end
	if npc_talk_index == "n1686_eltia-1-h" then 
	end
	if npc_talk_index == "n1686_eltia-1-i" then 
	end
	if npc_talk_index == "n1686_eltia-1-j" then 
	end
	if npc_talk_index == "n1686_eltia-1-k" then 
	end
	if npc_talk_index == "n1686_eltia-1-l" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n534_edan--------------------------------------------------------------------------------
function mqc01_9462_doubtful_motion_OnTalk_n534_edan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n534_edan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n534_edan-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n534_edan-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n534_edan-3-b" then 
	end
	if npc_talk_index == "n534_edan-3-c" then 
	end
	if npc_talk_index == "n534_edan-3-d" then 
	end
	if npc_talk_index == "n534_edan-3-e" then 
	end
	if npc_talk_index == "n534_edan-3-f" then 
	end
	if npc_talk_index == "n534_edan-3-g" then 
	end
	if npc_talk_index == "n534_edan-3-h" then 
	end
	if npc_talk_index == "n534_edan-3-i" then 
	end
	if npc_talk_index == "n534_edan-3-j" then 
	end
	if npc_talk_index == "n534_edan-3-k" then 
	end
	if npc_talk_index == "n534_edan-3-l" then 
	end
	if npc_talk_index == "n534_edan-3-m" then 
	end
	if npc_talk_index == "n534_edan-3-n" then 
	end
	if npc_talk_index == "n534_edan-3-o" then 
	end
	if npc_talk_index == "n534_edan-3-p" then 
	end
	if npc_talk_index == "n534_edan-3-q" then 
	end
	if npc_talk_index == "n534_edan-3-r" then 
	end
	if npc_talk_index == "n534_edan-3-s" then 
	end
	if npc_talk_index == "n534_edan-3-t" then 
	end
	if npc_talk_index == "n534_edan-3-u" then 
	end
	if npc_talk_index == "n534_edan-3-v" then 
	end
	if npc_talk_index == "n534_edan-3-w" then 
	end
	if npc_talk_index == "n534_edan-3-x" then 
	end
	if npc_talk_index == "n534_edan-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9462_doubtful_motion_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9462);
end

function mqc01_9462_doubtful_motion_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9462);
end

function mqc01_9462_doubtful_motion_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9462);
	local questID=9462;
end

function mqc01_9462_doubtful_motion_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc01_9462_doubtful_motion_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc01_9462_doubtful_motion_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9462, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9462, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9462, 1);
				npc_talk_index = "n1686_eltia-1";
end

</GameServer>