<VillageServer>

function mqc01_9576_seriousness_direction_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1 then
		mqc01_9576_seriousness_direction_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 4 then
		mqc01_9576_seriousness_direction_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mqc01_9576_seriousness_direction_OnTalk_n001_elder_harold(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n001_elder_harold-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n001_elder_harold-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n001_elder_harold-4-b" then 
	end
	if npc_talk_index == "n001_elder_harold-4-c" then 
	end
	if npc_talk_index == "n001_elder_harold-4-d" then 
	end
	if npc_talk_index == "n001_elder_harold-4-e" then 
	end
	if npc_talk_index == "n001_elder_harold-4-f" then 
	end
	if npc_talk_index == "n001_elder_harold-4-g" then 
	end
	if npc_talk_index == "n001_elder_harold-4-h" then 
	end
	if npc_talk_index == "n001_elder_harold-4-i" then 
	end
	if npc_talk_index == "n001_elder_harold-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n004_guard_steave--------------------------------------------------------------------------------
function mqc01_9576_seriousness_direction_OnTalk_n004_guard_steave(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n004_guard_steave-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n004_guard_steave-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n004_guard_steave-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n004_guard_steave-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n004_guard_steave-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n004_guard_steave-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9576, 2);
				api_quest_SetJournalStep(userObjID,9576, 1);
				api_quest_SetQuestStep(userObjID,9576, 1);
				npc_talk_index = "n004_guard_steave-1";

	end
	if npc_talk_index == "n004_guard_steave-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n004_guard_steave-3-b" then 
	end
	if npc_talk_index == "n004_guard_steave-3-c" then 
	end
	if npc_talk_index == "n004_guard_steave-3-d" then 
	end
	if npc_talk_index == "n004_guard_steave-3-e" then 
	end
	if npc_talk_index == "n004_guard_steave-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n004_guard_steave-5-b" then 
	end
	if npc_talk_index == "n004_guard_steave-5-c" then 
	end
	if npc_talk_index == "n004_guard_steave-5-d" then 
	end
	if npc_talk_index == "n004_guard_steave-5-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95760, true);
				 api_quest_RewardQuestUser(userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95760, true);
				 api_quest_RewardQuestUser(userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95760, true);
				 api_quest_RewardQuestUser(userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95760, true);
				 api_quest_RewardQuestUser(userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95760, true);
				 api_quest_RewardQuestUser(userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95760, true);
				 api_quest_RewardQuestUser(userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95760, true);
				 api_quest_RewardQuestUser(userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95760, true);
				 api_quest_RewardQuestUser(userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95760, true);
				 api_quest_RewardQuestUser(userObjID, 95760, questID, 1);
			 end 
	end
	if npc_talk_index == "n004_guard_steave-5-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9577, 2);
					api_quest_SetQuestStep(userObjID, 9577, 1);
					api_quest_SetJournalStep(userObjID, 9577, 1);

									if api_quest_CheckQuestInvenForAddItem(userObjID, 300012, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300012, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n004_guard_steave-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n004_guard_steave-1", "mqc02_9577_entry_to_the_uninvited_guest.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9576_seriousness_direction_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9576);
end

function mqc01_9576_seriousness_direction_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9576);
end

function mqc01_9576_seriousness_direction_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9576);
	local questID=9576;
end

function mqc01_9576_seriousness_direction_OnRemoteStart( userObjID, questID )
end

function mqc01_9576_seriousness_direction_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc01_9576_seriousness_direction_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9576, 2);
				api_quest_SetJournalStep(userObjID,9576, 1);
				api_quest_SetQuestStep(userObjID,9576, 1);
				npc_talk_index = "n004_guard_steave-1";
end

</VillageServer>

<GameServer>
function mqc01_9576_seriousness_direction_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1 then
		mqc01_9576_seriousness_direction_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 4 then
		mqc01_9576_seriousness_direction_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n001_elder_harold--------------------------------------------------------------------------------
function mqc01_9576_seriousness_direction_OnTalk_n001_elder_harold( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n001_elder_harold-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n001_elder_harold-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n001_elder_harold-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n001_elder_harold-4-b" then 
	end
	if npc_talk_index == "n001_elder_harold-4-c" then 
	end
	if npc_talk_index == "n001_elder_harold-4-d" then 
	end
	if npc_talk_index == "n001_elder_harold-4-e" then 
	end
	if npc_talk_index == "n001_elder_harold-4-f" then 
	end
	if npc_talk_index == "n001_elder_harold-4-g" then 
	end
	if npc_talk_index == "n001_elder_harold-4-h" then 
	end
	if npc_talk_index == "n001_elder_harold-4-i" then 
	end
	if npc_talk_index == "n001_elder_harold-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n004_guard_steave--------------------------------------------------------------------------------
function mqc01_9576_seriousness_direction_OnTalk_n004_guard_steave( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n004_guard_steave-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n004_guard_steave-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n004_guard_steave-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n004_guard_steave-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n004_guard_steave-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n004_guard_steave-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n004_guard_steave-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9576, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9576, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9576, 1);
				npc_talk_index = "n004_guard_steave-1";

	end
	if npc_talk_index == "n004_guard_steave-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n004_guard_steave-3-b" then 
	end
	if npc_talk_index == "n004_guard_steave-3-c" then 
	end
	if npc_talk_index == "n004_guard_steave-3-d" then 
	end
	if npc_talk_index == "n004_guard_steave-3-e" then 
	end
	if npc_talk_index == "n004_guard_steave-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n004_guard_steave-5-b" then 
	end
	if npc_talk_index == "n004_guard_steave-5-c" then 
	end
	if npc_talk_index == "n004_guard_steave-5-d" then 
	end
	if npc_talk_index == "n004_guard_steave-5-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95760, questID, 1);
			 end 
	end
	if npc_talk_index == "n004_guard_steave-5-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9577, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9577, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9577, 1);

									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300012, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300012, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n004_guard_steave-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n004_guard_steave-1", "mqc02_9577_entry_to_the_uninvited_guest.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9576_seriousness_direction_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9576);
end

function mqc01_9576_seriousness_direction_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9576);
end

function mqc01_9576_seriousness_direction_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9576);
	local questID=9576;
end

function mqc01_9576_seriousness_direction_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc01_9576_seriousness_direction_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc01_9576_seriousness_direction_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9576, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9576, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9576, 1);
				npc_talk_index = "n004_guard_steave-1";
end

</GameServer>