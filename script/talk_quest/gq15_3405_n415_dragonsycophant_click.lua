<VillageServer>

function gq15_3405_n415_dragonsycophant_click_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 415 then
		gq15_3405_n415_dragonsycophant_click_OnTalk_n415_guild_board(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n415_guild_board--------------------------------------------------------------------------------
function gq15_3405_n415_dragonsycophant_click_OnTalk_n415_guild_board(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n415_guild_board-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n415_guild_board-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n415_guild_board-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n415_guild_board-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 34050, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 34050, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 34050, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 34050, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 34050, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 34050, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 34050, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 34050, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 34050, false);
			 end 

	end
	if npc_talk_index == "n415_guild_board-accepting-e" then
				api_quest_AddQuest(userObjID,3405, 1);
				api_quest_SetJournalStep(userObjID,3405, 1);
				api_quest_SetQuestStep(userObjID,3405, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1452, 5);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1457, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201452, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201457, 30000);

	end
	if npc_talk_index == "n415_guild_board-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 34050, true);
				 api_quest_RewardQuestUser(userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 34050, true);
				 api_quest_RewardQuestUser(userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 34050, true);
				 api_quest_RewardQuestUser(userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 34050, true);
				 api_quest_RewardQuestUser(userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 34050, true);
				 api_quest_RewardQuestUser(userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 34050, true);
				 api_quest_RewardQuestUser(userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 34050, true);
				 api_quest_RewardQuestUser(userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 34050, true);
				 api_quest_RewardQuestUser(userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 34050, true);
				 api_quest_RewardQuestUser(userObjID, 34050, questID, 1);
			 end 
	end
	if npc_talk_index == "n415_guild_board-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function gq15_3405_n415_dragonsycophant_click_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3405);
	if qstep == 1 and CountIndex == 1452 then

	end
	if qstep == 1 and CountIndex == 1457 then
				api_quest_IncCounting(userObjID, 2, 1452);

	end
	if qstep == 1 and CountIndex == 201452 then
				api_quest_IncCounting(userObjID, 2, 1452);

	end
	if qstep == 1 and CountIndex == 201457 then
				api_quest_IncCounting(userObjID, 2, 1452);

	end
end

function gq15_3405_n415_dragonsycophant_click_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3405);
	if qstep == 1 and CountIndex == 1452 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 1457 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201452 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201457 and Count >= TargetCount  then

	end
end

function gq15_3405_n415_dragonsycophant_click_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 3405);
	local questID=3405;
end

function gq15_3405_n415_dragonsycophant_click_OnRemoteStart( userObjID, questID )
end

function gq15_3405_n415_dragonsycophant_click_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function gq15_3405_n415_dragonsycophant_click_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function gq15_3405_n415_dragonsycophant_click_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 415 then
		gq15_3405_n415_dragonsycophant_click_OnTalk_n415_guild_board( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n415_guild_board--------------------------------------------------------------------------------
function gq15_3405_n415_dragonsycophant_click_OnTalk_n415_guild_board( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n415_guild_board-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n415_guild_board-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n415_guild_board-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n415_guild_board-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, false);
			 end 

	end
	if npc_talk_index == "n415_guild_board-accepting-e" then
				api_quest_AddQuest( pRoom, userObjID,3405, 1);
				api_quest_SetJournalStep( pRoom, userObjID,3405, 1);
				api_quest_SetQuestStep( pRoom, userObjID,3405, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1452, 5);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1457, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201452, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201457, 30000);

	end
	if npc_talk_index == "n415_guild_board-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 34050, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 34050, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 34050, questID, 1);
			 end 
	end
	if npc_talk_index == "n415_guild_board-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function gq15_3405_n415_dragonsycophant_click_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3405);
	if qstep == 1 and CountIndex == 1452 then

	end
	if qstep == 1 and CountIndex == 1457 then
				api_quest_IncCounting( pRoom, userObjID, 2, 1452);

	end
	if qstep == 1 and CountIndex == 201452 then
				api_quest_IncCounting( pRoom, userObjID, 2, 1452);

	end
	if qstep == 1 and CountIndex == 201457 then
				api_quest_IncCounting( pRoom, userObjID, 2, 1452);

	end
end

function gq15_3405_n415_dragonsycophant_click_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3405);
	if qstep == 1 and CountIndex == 1452 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 1457 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201452 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 201457 and Count >= TargetCount  then

	end
end

function gq15_3405_n415_dragonsycophant_click_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 3405);
	local questID=3405;
end

function gq15_3405_n415_dragonsycophant_click_OnRemoteStart( pRoom,  userObjID, questID )
end

function gq15_3405_n415_dragonsycophant_click_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function gq15_3405_n415_dragonsycophant_click_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>