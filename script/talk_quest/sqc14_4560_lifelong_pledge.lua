<VillageServer>

function sqc14_4560_lifelong_pledge_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 133 then
		sqc14_4560_lifelong_pledge_OnTalk_n133_lena_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1601 then
		sqc14_4560_lifelong_pledge_OnTalk_n1601_lena_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1607 then
		sqc14_4560_lifelong_pledge_OnTalk_n1607_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n133_lena_duglars--------------------------------------------------------------------------------
function sqc14_4560_lifelong_pledge_OnTalk_n133_lena_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n133_lena_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n133_lena_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n133_lena_duglars-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45600, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45600, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45600, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45600, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45600, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45600, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45600, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45600, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45600, false);
			 end 

	end
	if npc_talk_index == "n133_lena_duglars-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4560, 1);
				api_quest_SetJournalStep(userObjID,4560, 1);
				api_quest_SetQuestStep(userObjID,4560, 1);
				npc_talk_index = "n133_lena_duglars-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1601_lena_duglars--------------------------------------------------------------------------------
function sqc14_4560_lifelong_pledge_OnTalk_n1601_lena_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1601_lena_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1601_lena_duglars-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1601_lena_duglars-1-b" then 
	end
	if npc_talk_index == "n1601_lena_duglars-1-c" then 
	end
	if npc_talk_index == "n1601_lena_duglars-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1607_general_duglars--------------------------------------------------------------------------------
function sqc14_4560_lifelong_pledge_OnTalk_n1607_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1607_general_duglars-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1607_general_duglars-2-b" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-c" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-d" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-e" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-f" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-g" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-h" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45600, true);
				 api_quest_RewardQuestUser(userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45600, true);
				 api_quest_RewardQuestUser(userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45600, true);
				 api_quest_RewardQuestUser(userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45600, true);
				 api_quest_RewardQuestUser(userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45600, true);
				 api_quest_RewardQuestUser(userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45600, true);
				 api_quest_RewardQuestUser(userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45600, true);
				 api_quest_RewardQuestUser(userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45600, true);
				 api_quest_RewardQuestUser(userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45600, true);
				 api_quest_RewardQuestUser(userObjID, 45600, questID, 1);
			 end 
	end
	if npc_talk_index == "n1607_general_duglars-2-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4560_lifelong_pledge_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4560);
end

function sqc14_4560_lifelong_pledge_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4560);
end

function sqc14_4560_lifelong_pledge_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4560);
	local questID=4560;
end

function sqc14_4560_lifelong_pledge_OnRemoteStart( userObjID, questID )
end

function sqc14_4560_lifelong_pledge_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc14_4560_lifelong_pledge_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4560, 1);
				api_quest_SetJournalStep(userObjID,4560, 1);
				api_quest_SetQuestStep(userObjID,4560, 1);
				npc_talk_index = "n133_lena_duglars-1";
end

</VillageServer>

<GameServer>
function sqc14_4560_lifelong_pledge_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 133 then
		sqc14_4560_lifelong_pledge_OnTalk_n133_lena_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1601 then
		sqc14_4560_lifelong_pledge_OnTalk_n1601_lena_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1607 then
		sqc14_4560_lifelong_pledge_OnTalk_n1607_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n133_lena_duglars--------------------------------------------------------------------------------
function sqc14_4560_lifelong_pledge_OnTalk_n133_lena_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n133_lena_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n133_lena_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n133_lena_duglars-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, false);
			 end 

	end
	if npc_talk_index == "n133_lena_duglars-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4560, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4560, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4560, 1);
				npc_talk_index = "n133_lena_duglars-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1601_lena_duglars--------------------------------------------------------------------------------
function sqc14_4560_lifelong_pledge_OnTalk_n1601_lena_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1601_lena_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1601_lena_duglars-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1601_lena_duglars-1-b" then 
	end
	if npc_talk_index == "n1601_lena_duglars-1-c" then 
	end
	if npc_talk_index == "n1601_lena_duglars-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1607_general_duglars--------------------------------------------------------------------------------
function sqc14_4560_lifelong_pledge_OnTalk_n1607_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1607_general_duglars-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1607_general_duglars-2-b" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-c" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-d" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-e" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-f" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-g" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-h" then 
	end
	if npc_talk_index == "n1607_general_duglars-2-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45600, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45600, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45600, questID, 1);
			 end 
	end
	if npc_talk_index == "n1607_general_duglars-2-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4560_lifelong_pledge_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4560);
end

function sqc14_4560_lifelong_pledge_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4560);
end

function sqc14_4560_lifelong_pledge_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4560);
	local questID=4560;
end

function sqc14_4560_lifelong_pledge_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc14_4560_lifelong_pledge_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc14_4560_lifelong_pledge_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4560, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4560, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4560, 1);
				npc_talk_index = "n133_lena_duglars-1";
end

</GameServer>