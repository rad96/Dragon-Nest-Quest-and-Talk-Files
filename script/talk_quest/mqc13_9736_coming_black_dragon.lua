<VillageServer>

function mqc13_9736_coming_black_dragon_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1067 then
		mqc13_9736_coming_black_dragon_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mqc13_9736_coming_black_dragon_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 954 then
		mqc13_9736_coming_black_dragon_OnTalk_n954_darklair_rosetta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc13_9736_coming_black_dragon_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mqc13_9736_coming_black_dragon_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n040_king_casius-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n040_king_casius-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n040_king_casius-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9736, 2);
				api_quest_SetJournalStep(userObjID,9736, 1);
				api_quest_SetQuestStep(userObjID,9736, 1);
				npc_talk_index = "n040_king_casius-1";

	end
	if npc_talk_index == "n040_king_casius-1-lvchk" then 
				if api_user_GetUserLevel(userObjID) >= 70 then
									npc_talk_index = "n040_king_casius-1-a";

				else
									npc_talk_index = "n040_king_casius-1-j";

				end
	end
	if npc_talk_index == "n040_king_casius-1-b" then 
	end
	if npc_talk_index == "n040_king_casius-1-c" then 
	end
	if npc_talk_index == "n040_king_casius-1-d" then 
	end
	if npc_talk_index == "n040_king_casius-1-e" then 
	end
	if npc_talk_index == "n040_king_casius-1-f" then 
	end
	if npc_talk_index == "n040_king_casius-1-g" then 
	end
	if npc_talk_index == "n040_king_casius-1-h" then 
	end
	if npc_talk_index == "n040_king_casius-1-i" then 
	end
	if npc_talk_index == "n040_king_casius-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n954_darklair_rosetta--------------------------------------------------------------------------------
function mqc13_9736_coming_black_dragon_OnTalk_n954_darklair_rosetta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n954_darklair_rosetta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n954_darklair_rosetta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n954_darklair_rosetta-2-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-h" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-i" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97360, true);
				 api_quest_RewardQuestUser(userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97360, true);
				 api_quest_RewardQuestUser(userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97360, true);
				 api_quest_RewardQuestUser(userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97360, true);
				 api_quest_RewardQuestUser(userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97360, true);
				 api_quest_RewardQuestUser(userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97360, true);
				 api_quest_RewardQuestUser(userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97360, true);
				 api_quest_RewardQuestUser(userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97360, true);
				 api_quest_RewardQuestUser(userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97360, true);
				 api_quest_RewardQuestUser(userObjID, 97360, questID, 1);
			 end 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-k" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9737, 2);
					api_quest_SetQuestStep(userObjID, 9737, 1);
					api_quest_SetJournalStep(userObjID, 9737, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n954_darklair_rosetta-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n954_darklair_rosetta-1", "mqc13_9737_end_of_black_nightmare.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc13_9736_coming_black_dragon_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9736);
end

function mqc13_9736_coming_black_dragon_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9736);
end

function mqc13_9736_coming_black_dragon_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9736);
	local questID=9736;
end

function mqc13_9736_coming_black_dragon_OnRemoteStart( userObjID, questID )
end

function mqc13_9736_coming_black_dragon_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc13_9736_coming_black_dragon_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9736, 2);
				api_quest_SetJournalStep(userObjID,9736, 1);
				api_quest_SetQuestStep(userObjID,9736, 1);
				npc_talk_index = "n040_king_casius-1";
end

</VillageServer>

<GameServer>
function mqc13_9736_coming_black_dragon_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1067 then
		mqc13_9736_coming_black_dragon_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mqc13_9736_coming_black_dragon_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 954 then
		mqc13_9736_coming_black_dragon_OnTalk_n954_darklair_rosetta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1067_elder_elf--------------------------------------------------------------------------------
function mqc13_9736_coming_black_dragon_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mqc13_9736_coming_black_dragon_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n040_king_casius-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n040_king_casius-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n040_king_casius-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9736, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9736, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9736, 1);
				npc_talk_index = "n040_king_casius-1";

	end
	if npc_talk_index == "n040_king_casius-1-lvchk" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 70 then
									npc_talk_index = "n040_king_casius-1-a";

				else
									npc_talk_index = "n040_king_casius-1-j";

				end
	end
	if npc_talk_index == "n040_king_casius-1-b" then 
	end
	if npc_talk_index == "n040_king_casius-1-c" then 
	end
	if npc_talk_index == "n040_king_casius-1-d" then 
	end
	if npc_talk_index == "n040_king_casius-1-e" then 
	end
	if npc_talk_index == "n040_king_casius-1-f" then 
	end
	if npc_talk_index == "n040_king_casius-1-g" then 
	end
	if npc_talk_index == "n040_king_casius-1-h" then 
	end
	if npc_talk_index == "n040_king_casius-1-i" then 
	end
	if npc_talk_index == "n040_king_casius-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n954_darklair_rosetta--------------------------------------------------------------------------------
function mqc13_9736_coming_black_dragon_OnTalk_n954_darklair_rosetta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n954_darklair_rosetta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n954_darklair_rosetta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n954_darklair_rosetta-2-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-e" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-f" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-g" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-h" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-i" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97360, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97360, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97360, questID, 1);
			 end 
	end
	if npc_talk_index == "n954_darklair_rosetta-2-k" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9737, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9737, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9737, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n954_darklair_rosetta-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n954_darklair_rosetta-1", "mqc13_9737_end_of_black_nightmare.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc13_9736_coming_black_dragon_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9736);
end

function mqc13_9736_coming_black_dragon_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9736);
end

function mqc13_9736_coming_black_dragon_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9736);
	local questID=9736;
end

function mqc13_9736_coming_black_dragon_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc13_9736_coming_black_dragon_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc13_9736_coming_black_dragon_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9736, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9736, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9736, 1);
				npc_talk_index = "n040_king_casius-1";
end

</GameServer>