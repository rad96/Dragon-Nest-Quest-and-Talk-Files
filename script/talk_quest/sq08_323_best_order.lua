<VillageServer>

function sq08_323_best_order_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 26 then
		sq08_323_best_order_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_323_best_order_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-g" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3231, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3232, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3233, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3234, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3235, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3236, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3237, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3238, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3239, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest(userObjID,323, 1);
				api_quest_SetJournalStep(userObjID,323, 1);
				api_quest_SetQuestStep(userObjID,323, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 389, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 385, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200389, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200385, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 400112, 3);
				api_quest_SetCountingInfo(userObjID, questID, 5, 3, 400113, 5);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 384, 30000);
	end
	if npc_talk_index == "n026_trader_may-3-b" then 
	end
	if npc_talk_index == "n026_trader_may-3-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3231, true);
				 api_quest_RewardQuestUser(userObjID, 3231, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3232, true);
				 api_quest_RewardQuestUser(userObjID, 3232, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3233, true);
				 api_quest_RewardQuestUser(userObjID, 3233, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3234, true);
				 api_quest_RewardQuestUser(userObjID, 3234, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3235, true);
				 api_quest_RewardQuestUser(userObjID, 3235, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3236, true);
				 api_quest_RewardQuestUser(userObjID, 3236, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3237, true);
				 api_quest_RewardQuestUser(userObjID, 3237, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3238, true);
				 api_quest_RewardQuestUser(userObjID, 3238, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3239, true);
				 api_quest_RewardQuestUser(userObjID, 3239, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-3-d" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400132, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400132, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 324, 1);
					api_quest_SetQuestStep(userObjID, 324, 1);
					api_quest_SetJournalStep(userObjID, 324, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400112, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400112, api_quest_HasQuestItem(userObjID, 400112, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400113, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400113, api_quest_HasQuestItem(userObjID, 400113, 1));
				end
	end
	if npc_talk_index == "n026_trader_may-3-e" then 
	end
	if npc_talk_index == "n026_trader_may-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n026_trader_may-1", "sq08_324_huberts_recipe.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_323_best_order_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 323);
	if qstep == 2 and CountIndex == 389 then
				if api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400112, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400112, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 385 then
				if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5 then
									if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400113, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400113, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 400112 then

	end
	if qstep == 2 and CountIndex == 400113 then

	end
	if qstep == 2 and CountIndex == 384 then
				if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5 then
									if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400113, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400113, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200389 then
				if api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400112, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400112, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200385 then
				if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5 then
									if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400113, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400113, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem(userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
end

function sq08_323_best_order_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 323);
	if qstep == 2 and CountIndex == 389 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 385 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400112 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400113 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 384 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200389 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200385 and Count >= TargetCount  then

	end
end

function sq08_323_best_order_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 323);
	local questID=323;
end

function sq08_323_best_order_OnRemoteStart( userObjID, questID )
end

function sq08_323_best_order_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_323_best_order_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,323, 1);
				api_quest_SetJournalStep(userObjID,323, 1);
				api_quest_SetQuestStep(userObjID,323, 1);
				npc_talk_index = "n026_trader_may-1";
end

</VillageServer>

<GameServer>
function sq08_323_best_order_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 26 then
		sq08_323_best_order_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_323_best_order_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-g" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3231, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3232, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3233, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3234, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3235, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3236, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3237, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3238, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3239, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,323, 1);
				api_quest_SetJournalStep( pRoom, userObjID,323, 1);
				api_quest_SetQuestStep( pRoom, userObjID,323, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 389, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 385, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200389, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200385, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 400112, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 3, 400113, 5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 384, 30000);
	end
	if npc_talk_index == "n026_trader_may-3-b" then 
	end
	if npc_talk_index == "n026_trader_may-3-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3231, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3231, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3232, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3232, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3233, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3233, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3234, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3234, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3235, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3235, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3236, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3236, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3237, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3237, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3238, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3238, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3239, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3239, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-3-d" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400132, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400132, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 324, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 324, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 324, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400112, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400112, api_quest_HasQuestItem( pRoom, userObjID, 400112, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400113, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400113, api_quest_HasQuestItem( pRoom, userObjID, 400113, 1));
				end
	end
	if npc_talk_index == "n026_trader_may-3-e" then 
	end
	if npc_talk_index == "n026_trader_may-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n026_trader_may-1", "sq08_324_huberts_recipe.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_323_best_order_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 323);
	if qstep == 2 and CountIndex == 389 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400112, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400112, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 385 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400113, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400113, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 400112 then

	end
	if qstep == 2 and CountIndex == 400113 then

	end
	if qstep == 2 and CountIndex == 384 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400113, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400113, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200389 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400112, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400112, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200385 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5 then
									if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 800 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400113, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400113, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400113, 5) >= 5  and api_quest_HasQuestItem( pRoom, userObjID, 400112, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
end

function sq08_323_best_order_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 323);
	if qstep == 2 and CountIndex == 389 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 385 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400112 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400113 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 384 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200389 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200385 and Count >= TargetCount  then

	end
end

function sq08_323_best_order_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 323);
	local questID=323;
end

function sq08_323_best_order_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_323_best_order_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_323_best_order_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,323, 1);
				api_quest_SetJournalStep( pRoom, userObjID,323, 1);
				api_quest_SetQuestStep( pRoom, userObjID,323, 1);
				npc_talk_index = "n026_trader_may-1";
end

</GameServer>