<VillageServer>

function sq11_484_catastrophic_rain4_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 88 then
		sq11_484_catastrophic_rain4_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_484_catastrophic_rain4_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4841, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4842, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4843, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4844, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4845, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4846, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4847, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4848, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4849, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,484, 1);
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400193, 5);
				api_quest_SetJournalStep(userObjID, questID, 1);
				npc_talk_index = "n088_scholar_starshy-1";
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 987, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200987, 30000);

	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4841, true);
				 api_quest_RewardQuestUser(userObjID, 4841, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4842, true);
				 api_quest_RewardQuestUser(userObjID, 4842, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4843, true);
				 api_quest_RewardQuestUser(userObjID, 4843, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4844, true);
				 api_quest_RewardQuestUser(userObjID, 4844, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4845, true);
				 api_quest_RewardQuestUser(userObjID, 4845, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4846, true);
				 api_quest_RewardQuestUser(userObjID, 4846, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4847, true);
				 api_quest_RewardQuestUser(userObjID, 4847, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4848, true);
				 api_quest_RewardQuestUser(userObjID, 4848, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 4849, true);
				 api_quest_RewardQuestUser(userObjID, 4849, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-2-i" then 

				if api_quest_HasQuestItem(userObjID, 400193, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400193, api_quest_HasQuestItem(userObjID, 400193, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_484_catastrophic_rain4_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 484);
	if qstep == 1 and CountIndex == 400193 then

	end
	if qstep == 1 and CountIndex == 987 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400193, 5) == 1 then 
					api_quest_AddQuestItem(userObjID, 400193, 5, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200987 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400193, 5) == 1 then 
					api_quest_AddQuestItem(userObjID, 400193, 5, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_484_catastrophic_rain4_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 484);
	if qstep == 1 and CountIndex == 400193 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 987 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200987 and Count >= TargetCount  then

	end
end

function sq11_484_catastrophic_rain4_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 484);
	local questID=484;
end

function sq11_484_catastrophic_rain4_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400193, 5);
				api_quest_SetJournalStep(userObjID, questID, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 987, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200987, 30000);
end

function sq11_484_catastrophic_rain4_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_484_catastrophic_rain4_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,484, 1);
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400193, 5);
				api_quest_SetJournalStep(userObjID, questID, 1);
				npc_talk_index = "n088_scholar_starshy-1";
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 987, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200987, 30000);
end

</VillageServer>

<GameServer>
function sq11_484_catastrophic_rain4_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 88 then
		sq11_484_catastrophic_rain4_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_484_catastrophic_rain4_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4841, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4842, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4843, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4844, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4845, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4846, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4847, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4848, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4849, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,484, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400193, 5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				npc_talk_index = "n088_scholar_starshy-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 987, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200987, 30000);

	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4841, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4841, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4842, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4842, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4843, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4843, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4844, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4844, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4845, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4845, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4846, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4846, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4847, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4847, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4848, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4848, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4849, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4849, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-2-i" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400193, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400193, api_quest_HasQuestItem( pRoom, userObjID, 400193, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_484_catastrophic_rain4_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 484);
	if qstep == 1 and CountIndex == 400193 then

	end
	if qstep == 1 and CountIndex == 987 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400193, 5) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400193, 5, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200987 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400193, 5) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400193, 5, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_484_catastrophic_rain4_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 484);
	if qstep == 1 and CountIndex == 400193 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if qstep == 1 and CountIndex == 987 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200987 and Count >= TargetCount  then

	end
end

function sq11_484_catastrophic_rain4_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 484);
	local questID=484;
end

function sq11_484_catastrophic_rain4_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400193, 5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 987, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200987, 30000);
end

function sq11_484_catastrophic_rain4_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_484_catastrophic_rain4_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,484, 1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400193, 5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 1);
				npc_talk_index = "n088_scholar_starshy-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 987, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200987, 30000);
end

</GameServer>