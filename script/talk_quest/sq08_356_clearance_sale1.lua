<VillageServer>

function sq08_356_clearance_sale1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 24 then
		sq08_356_clearance_sale1_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 26 then
		sq08_356_clearance_sale1_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function sq08_356_clearance_sale1_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n024_adventurer_guildmaster_decud-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n026_trader_may--------------------------------------------------------------------------------
function sq08_356_clearance_sale1_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n026_trader_may-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3561, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3562, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3563, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3564, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3565, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3566, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3567, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3568, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3569, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest(userObjID,356, 1);
				api_quest_SetJournalStep(userObjID,356, 1);
				api_quest_SetQuestStep(userObjID,356, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-2-b" then 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 
	end
	if npc_talk_index == "n026_trader_may-2-e" then 
	end
	if npc_talk_index == "n026_trader_may-2-f" then 
	end
	if npc_talk_index == "n026_trader_may-2-g" then 
	end
	if npc_talk_index == "n026_trader_may-2-h" then 
	end
	if npc_talk_index == "n026_trader_may-2-i" then 
	end
	if npc_talk_index == "n026_trader_may-2-j" then 
	end
	if npc_talk_index == "n026_trader_may-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 346, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 400251, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200346, 30000);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n026_trader_may-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3561, true);
				 api_quest_RewardQuestUser(userObjID, 3561, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3562, true);
				 api_quest_RewardQuestUser(userObjID, 3562, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3563, true);
				 api_quest_RewardQuestUser(userObjID, 3563, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3564, true);
				 api_quest_RewardQuestUser(userObjID, 3564, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3565, true);
				 api_quest_RewardQuestUser(userObjID, 3565, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3566, true);
				 api_quest_RewardQuestUser(userObjID, 3566, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3567, true);
				 api_quest_RewardQuestUser(userObjID, 3567, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3568, true);
				 api_quest_RewardQuestUser(userObjID, 3568, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3569, true);
				 api_quest_RewardQuestUser(userObjID, 3569, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-4-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 26, 600 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_356_clearance_sale1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 356);
	if qstep == 3 and CountIndex == 346 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400251, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 400251 then

	end
	if qstep == 3 and CountIndex == 200346 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400251, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_356_clearance_sale1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 356);
	if qstep == 3 and CountIndex == 346 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 400251 and Count >= TargetCount  then
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 3 and CountIndex == 200346 and Count >= TargetCount  then

	end
end

function sq08_356_clearance_sale1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 356);
	local questID=356;
end

function sq08_356_clearance_sale1_OnRemoteStart( userObjID, questID )
end

function sq08_356_clearance_sale1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_356_clearance_sale1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,356, 1);
				api_quest_SetJournalStep(userObjID,356, 1);
				api_quest_SetQuestStep(userObjID,356, 1);
				npc_talk_index = "n026_trader_may-1";
end

</VillageServer>

<GameServer>
function sq08_356_clearance_sale1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 24 then
		sq08_356_clearance_sale1_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 26 then
		sq08_356_clearance_sale1_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function sq08_356_clearance_sale1_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n024_adventurer_guildmaster_decud-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n026_trader_may--------------------------------------------------------------------------------
function sq08_356_clearance_sale1_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n026_trader_may-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3561, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3562, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3563, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3564, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3565, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3566, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3567, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3568, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3569, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,356, 1);
				api_quest_SetJournalStep( pRoom, userObjID,356, 1);
				api_quest_SetQuestStep( pRoom, userObjID,356, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-2-b" then 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 
	end
	if npc_talk_index == "n026_trader_may-2-e" then 
	end
	if npc_talk_index == "n026_trader_may-2-f" then 
	end
	if npc_talk_index == "n026_trader_may-2-g" then 
	end
	if npc_talk_index == "n026_trader_may-2-h" then 
	end
	if npc_talk_index == "n026_trader_may-2-i" then 
	end
	if npc_talk_index == "n026_trader_may-2-j" then 
	end
	if npc_talk_index == "n026_trader_may-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 346, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 400251, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200346, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n026_trader_may-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3561, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3561, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3562, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3562, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3563, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3563, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3564, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3564, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3565, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3565, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3566, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3566, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3567, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3567, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3568, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3568, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3569, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3569, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-4-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 26, 600 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_356_clearance_sale1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 356);
	if qstep == 3 and CountIndex == 346 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400251, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 400251 then

	end
	if qstep == 3 and CountIndex == 200346 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400251, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400251, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_356_clearance_sale1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 356);
	if qstep == 3 and CountIndex == 346 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 400251 and Count >= TargetCount  then
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 3 and CountIndex == 200346 and Count >= TargetCount  then

	end
end

function sq08_356_clearance_sale1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 356);
	local questID=356;
end

function sq08_356_clearance_sale1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_356_clearance_sale1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_356_clearance_sale1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,356, 1);
				api_quest_SetJournalStep( pRoom, userObjID,356, 1);
				api_quest_SetQuestStep( pRoom, userObjID,356, 1);
				npc_talk_index = "n026_trader_may-1";
end

</GameServer>