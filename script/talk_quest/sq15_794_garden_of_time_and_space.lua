<VillageServer>

function sq15_794_garden_of_time_and_space_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 714 then
		sq15_794_garden_of_time_and_space_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 775 then
		sq15_794_garden_of_time_and_space_OnTalk_n775_cleric_ilizer(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		sq15_794_garden_of_time_and_space_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n714_cleric_yohan--------------------------------------------------------------------------------
function sq15_794_garden_of_time_and_space_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n714_cleric_yohan-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n714_cleric_yohan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n714_cleric_yohan-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7940, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7940, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7940, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7940, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7940, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7940, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7940, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7940, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7940, false);
			 end 

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-acceptted" then
				api_quest_AddQuest(userObjID,794, 1);
				api_quest_SetJournalStep(userObjID,794, 1);
				api_quest_SetQuestStep(userObjID,794, 1);
				npc_talk_index = "n714_cleric_yohan-1";

	end
	if npc_talk_index == "n714_cleric_yohan-1-b" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-d" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-e" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-f" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-g" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-h" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-i" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-j" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n775_cleric_ilizer--------------------------------------------------------------------------------
function sq15_794_garden_of_time_and_space_OnTalk_n775_cleric_ilizer(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n775_cleric_ilizer-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n775_cleric_ilizer-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n775_cleric_ilizer-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n775_cleric_ilizer-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n775_cleric_ilizer-2-b" then 
	end
	if npc_talk_index == "n775_cleric_ilizer-2-c" then 
	end
	if npc_talk_index == "n775_cleric_ilizer-2-d" then 
	end
	if npc_talk_index == "n775_cleric_ilizer-2-e" then 
	end
	if npc_talk_index == "n775_cleric_ilizer-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function sq15_794_garden_of_time_and_space_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n776_darklair_mardlen-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n776_darklair_mardlen-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-3-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7940, true);
				 api_quest_RewardQuestUser(userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7940, true);
				 api_quest_RewardQuestUser(userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7940, true);
				 api_quest_RewardQuestUser(userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7940, true);
				 api_quest_RewardQuestUser(userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7940, true);
				 api_quest_RewardQuestUser(userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7940, true);
				 api_quest_RewardQuestUser(userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7940, true);
				 api_quest_RewardQuestUser(userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7940, true);
				 api_quest_RewardQuestUser(userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7940, true);
				 api_quest_RewardQuestUser(userObjID, 7940, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_794_garden_of_time_and_space_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 794);
end

function sq15_794_garden_of_time_and_space_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 794);
end

function sq15_794_garden_of_time_and_space_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 794);
	local questID=794;
end

function sq15_794_garden_of_time_and_space_OnRemoteStart( userObjID, questID )
end

function sq15_794_garden_of_time_and_space_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_794_garden_of_time_and_space_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,794, 1);
				api_quest_SetJournalStep(userObjID,794, 1);
				api_quest_SetQuestStep(userObjID,794, 1);
				npc_talk_index = "n714_cleric_yohan-1";
end

</VillageServer>

<GameServer>
function sq15_794_garden_of_time_and_space_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 714 then
		sq15_794_garden_of_time_and_space_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 775 then
		sq15_794_garden_of_time_and_space_OnTalk_n775_cleric_ilizer( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		sq15_794_garden_of_time_and_space_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n714_cleric_yohan--------------------------------------------------------------------------------
function sq15_794_garden_of_time_and_space_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n714_cleric_yohan-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n714_cleric_yohan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n714_cleric_yohan-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, false);
			 end 

	end
	if npc_talk_index == "n714_cleric_yohan-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,794, 1);
				api_quest_SetJournalStep( pRoom, userObjID,794, 1);
				api_quest_SetQuestStep( pRoom, userObjID,794, 1);
				npc_talk_index = "n714_cleric_yohan-1";

	end
	if npc_talk_index == "n714_cleric_yohan-1-b" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-d" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-e" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-f" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-g" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-h" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-i" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-j" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n775_cleric_ilizer--------------------------------------------------------------------------------
function sq15_794_garden_of_time_and_space_OnTalk_n775_cleric_ilizer( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n775_cleric_ilizer-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n775_cleric_ilizer-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n775_cleric_ilizer-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n775_cleric_ilizer-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n775_cleric_ilizer-2-b" then 
	end
	if npc_talk_index == "n775_cleric_ilizer-2-c" then 
	end
	if npc_talk_index == "n775_cleric_ilizer-2-d" then 
	end
	if npc_talk_index == "n775_cleric_ilizer-2-e" then 
	end
	if npc_talk_index == "n775_cleric_ilizer-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function sq15_794_garden_of_time_and_space_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n776_darklair_mardlen-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n776_darklair_mardlen-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-3-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-c" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-d" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-e" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-f" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7940, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7940, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7940, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_794_garden_of_time_and_space_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 794);
end

function sq15_794_garden_of_time_and_space_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 794);
end

function sq15_794_garden_of_time_and_space_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 794);
	local questID=794;
end

function sq15_794_garden_of_time_and_space_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_794_garden_of_time_and_space_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_794_garden_of_time_and_space_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,794, 1);
				api_quest_SetJournalStep( pRoom, userObjID,794, 1);
				api_quest_SetQuestStep( pRoom, userObjID,794, 1);
				npc_talk_index = "n714_cleric_yohan-1";
end

</GameServer>