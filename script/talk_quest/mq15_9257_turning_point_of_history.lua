<VillageServer>

function mq15_9257_turning_point_of_history_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1048 then
		mq15_9257_turning_point_of_history_OnTalk_n1048_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		mq15_9257_turning_point_of_history_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq15_9257_turning_point_of_history_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 836 then
		mq15_9257_turning_point_of_history_OnTalk_n836_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1048_academic--------------------------------------------------------------------------------
function mq15_9257_turning_point_of_history_OnTalk_n1048_academic(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1048_academic-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1048_academic-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1048_academic-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function mq15_9257_turning_point_of_history_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n045_soceress_master_stella-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9257, 2);
				api_quest_SetJournalStep(userObjID,9257, 1);
				api_quest_SetQuestStep(userObjID,9257, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-j" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-l" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-m" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-n" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n045_soceress_master_stella-7-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-7-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 92570, true);
				 api_quest_RewardQuestUser(userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 92570, true);
				 api_quest_RewardQuestUser(userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 92570, true);
				 api_quest_RewardQuestUser(userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 92570, true);
				 api_quest_RewardQuestUser(userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 92570, true);
				 api_quest_RewardQuestUser(userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 92570, true);
				 api_quest_RewardQuestUser(userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 92570, true);
				 api_quest_RewardQuestUser(userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 92570, true);
				 api_quest_RewardQuestUser(userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 92570, true);
				 api_quest_RewardQuestUser(userObjID, 92570, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-7-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9258, 2);
					api_quest_SetQuestStep(userObjID, 9258, 1);
					api_quest_SetJournalStep(userObjID, 9258, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-7-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n045_soceress_master_stella-1", "mq15_9258_restart.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq15_9257_turning_point_of_history_OnTalk_n822_teramai(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n822_teramai-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n822_teramai-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n822_teramai-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-2-b" then 
	end
	if npc_talk_index == "n822_teramai-2-c" then 
	end
	if npc_talk_index == "n822_teramai-2-d" then 
	end
	if npc_talk_index == "n822_teramai-2-e" then 
	end
	if npc_talk_index == "n822_teramai-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n822_teramai-6-b" then 
	end
	if npc_talk_index == "n822_teramai-6-c" then 
	end
	if npc_talk_index == "n822_teramai-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n836_velskud--------------------------------------------------------------------------------
function mq15_9257_turning_point_of_history_OnTalk_n836_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n836_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n836_velskud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n836_velskud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n836_velskud-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n836_velskud-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n836_velskud-3-b" then 
	end
	if npc_talk_index == "n836_velskud-3-c" then 
	end
	if npc_talk_index == "n836_velskud-3-d" then 
	end
	if npc_talk_index == "n836_velskud-3-e" then 
	end
	if npc_talk_index == "n836_velskud-3-f" then 
	end
	if npc_talk_index == "n836_velskud-3-g" then 
	end
	if npc_talk_index == "n836_velskud-3-h" then 
	end
	if npc_talk_index == "n836_velskud-3-i" then 
	end
	if npc_talk_index == "n836_velskud-3-j" then 
	end
	if npc_talk_index == "n836_velskud-3-k" then 
	end
	if npc_talk_index == "n836_velskud-3-l" then 
	end
	if npc_talk_index == "n836_velskud-3-m" then 
	end
	if npc_talk_index == "n836_velskud-3-n" then 
	end
	if npc_talk_index == "n836_velskud-3-o" then 
	end
	if npc_talk_index == "n836_velskud-3-p" then 
	end
	if npc_talk_index == "n836_velskud-3-q" then 
	end
	if npc_talk_index == "n836_velskud-3-r" then 
	end
	if npc_talk_index == "n836_velskud-3-s" then 
	end
	if npc_talk_index == "n836_velskud-3-t" then 
	end
	if npc_talk_index == "n836_velskud-3-u" then 
	end
	if npc_talk_index == "n836_velskud-3-v" then 
	end
	if npc_talk_index == "n836_velskud-3-w" then 
	end
	if npc_talk_index == "n836_velskud-3-x" then 
	end
	if npc_talk_index == "n836_velskud-3-x" then 
	end
	if npc_talk_index == "n836_velskud-3-x" then 
	end
	if npc_talk_index == "n836_velskud-3-cutscene" then 
				api_user_ChangeMap(userObjID,13516,1);
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n836_velskud-5-b" then 
	end
	if npc_talk_index == "n836_velskud-5-c" then 
	end
	if npc_talk_index == "n836_velskud-5-d" then 
	end
	if npc_talk_index == "n836_velskud-5-d" then 
	end
	if npc_talk_index == "n836_velskud-5-d" then 
	end
	if npc_talk_index == "n836_velskud-5-e" then 
	end
	if npc_talk_index == "n836_velskud-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9257_turning_point_of_history_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9257);
end

function mq15_9257_turning_point_of_history_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9257);
end

function mq15_9257_turning_point_of_history_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9257);
	local questID=9257;
end

function mq15_9257_turning_point_of_history_OnRemoteStart( userObjID, questID )
end

function mq15_9257_turning_point_of_history_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9257_turning_point_of_history_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9257, 2);
				api_quest_SetJournalStep(userObjID,9257, 1);
				api_quest_SetQuestStep(userObjID,9257, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
end

</VillageServer>

<GameServer>
function mq15_9257_turning_point_of_history_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1048 then
		mq15_9257_turning_point_of_history_OnTalk_n1048_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		mq15_9257_turning_point_of_history_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 822 then
		mq15_9257_turning_point_of_history_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 836 then
		mq15_9257_turning_point_of_history_OnTalk_n836_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1048_academic--------------------------------------------------------------------------------
function mq15_9257_turning_point_of_history_OnTalk_n1048_academic( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1048_academic-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1048_academic-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1048_academic-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function mq15_9257_turning_point_of_history_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n045_soceress_master_stella-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9257, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9257, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9257, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-i" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-j" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-k" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-l" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-m" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-n" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n045_soceress_master_stella-7-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-7-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92570, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 92570, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 92570, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-7-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9258, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9258, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9258, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n045_soceress_master_stella-7-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-7-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-1", "mq15_9258_restart.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n822_teramai--------------------------------------------------------------------------------
function mq15_9257_turning_point_of_history_OnTalk_n822_teramai( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n822_teramai-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n822_teramai-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n822_teramai-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n822_teramai-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n822_teramai-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n822_teramai-2-b" then 
	end
	if npc_talk_index == "n822_teramai-2-c" then 
	end
	if npc_talk_index == "n822_teramai-2-d" then 
	end
	if npc_talk_index == "n822_teramai-2-e" then 
	end
	if npc_talk_index == "n822_teramai-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n822_teramai-6-b" then 
	end
	if npc_talk_index == "n822_teramai-6-c" then 
	end
	if npc_talk_index == "n822_teramai-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n836_velskud--------------------------------------------------------------------------------
function mq15_9257_turning_point_of_history_OnTalk_n836_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n836_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n836_velskud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n836_velskud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n836_velskud-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n836_velskud-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n836_velskud-3-b" then 
	end
	if npc_talk_index == "n836_velskud-3-c" then 
	end
	if npc_talk_index == "n836_velskud-3-d" then 
	end
	if npc_talk_index == "n836_velskud-3-e" then 
	end
	if npc_talk_index == "n836_velskud-3-f" then 
	end
	if npc_talk_index == "n836_velskud-3-g" then 
	end
	if npc_talk_index == "n836_velskud-3-h" then 
	end
	if npc_talk_index == "n836_velskud-3-i" then 
	end
	if npc_talk_index == "n836_velskud-3-j" then 
	end
	if npc_talk_index == "n836_velskud-3-k" then 
	end
	if npc_talk_index == "n836_velskud-3-l" then 
	end
	if npc_talk_index == "n836_velskud-3-m" then 
	end
	if npc_talk_index == "n836_velskud-3-n" then 
	end
	if npc_talk_index == "n836_velskud-3-o" then 
	end
	if npc_talk_index == "n836_velskud-3-p" then 
	end
	if npc_talk_index == "n836_velskud-3-q" then 
	end
	if npc_talk_index == "n836_velskud-3-r" then 
	end
	if npc_talk_index == "n836_velskud-3-s" then 
	end
	if npc_talk_index == "n836_velskud-3-t" then 
	end
	if npc_talk_index == "n836_velskud-3-u" then 
	end
	if npc_talk_index == "n836_velskud-3-v" then 
	end
	if npc_talk_index == "n836_velskud-3-w" then 
	end
	if npc_talk_index == "n836_velskud-3-x" then 
	end
	if npc_talk_index == "n836_velskud-3-x" then 
	end
	if npc_talk_index == "n836_velskud-3-x" then 
	end
	if npc_talk_index == "n836_velskud-3-cutscene" then 
				api_user_ChangeMap( pRoom, userObjID,13516,1);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n836_velskud-5-b" then 
	end
	if npc_talk_index == "n836_velskud-5-c" then 
	end
	if npc_talk_index == "n836_velskud-5-d" then 
	end
	if npc_talk_index == "n836_velskud-5-d" then 
	end
	if npc_talk_index == "n836_velskud-5-d" then 
	end
	if npc_talk_index == "n836_velskud-5-e" then 
	end
	if npc_talk_index == "n836_velskud-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9257_turning_point_of_history_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9257);
end

function mq15_9257_turning_point_of_history_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9257);
end

function mq15_9257_turning_point_of_history_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9257);
	local questID=9257;
end

function mq15_9257_turning_point_of_history_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9257_turning_point_of_history_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9257_turning_point_of_history_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9257, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9257, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9257, 1);
				npc_talk_index = "n045_soceress_master_stella-1";
end

</GameServer>