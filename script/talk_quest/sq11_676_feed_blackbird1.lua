<VillageServer>

function sq11_676_feed_blackbird1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 338 then
		sq11_676_feed_blackbird1_OnTalk_n338_littlegirl_daisy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 340 then
		sq11_676_feed_blackbird1_OnTalk_n340_mystery_man(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n338_littlegirl_daisy--------------------------------------------------------------------------------
function sq11_676_feed_blackbird1_OnTalk_n338_littlegirl_daisy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n338_littlegirl_daisy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n338_littlegirl_daisy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n338_littlegirl_daisy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n338_littlegirl_daisy-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n338_littlegirl_daisy-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6760, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6760, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6760, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6760, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6760, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6760, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6760, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6760, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6760, false);
			 end 

	end
	if npc_talk_index == "n338_littlegirl_daisy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,676, 1);
				api_quest_SetJournalStep(userObjID,676, 1);
				api_quest_SetQuestStep(userObjID,676, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";

	end
	if npc_talk_index == "n338_littlegirl_daisy-1-b" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-c" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400298, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 921, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200921, 30000);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-b" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-c" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-d" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-e" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6760, true);
				 api_quest_RewardQuestUser(userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6760, true);
				 api_quest_RewardQuestUser(userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6760, true);
				 api_quest_RewardQuestUser(userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6760, true);
				 api_quest_RewardQuestUser(userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6760, true);
				 api_quest_RewardQuestUser(userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6760, true);
				 api_quest_RewardQuestUser(userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6760, true);
				 api_quest_RewardQuestUser(userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6760, true);
				 api_quest_RewardQuestUser(userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6760, true);
				 api_quest_RewardQuestUser(userObjID, 6760, questID, 1);
			 end 
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 677, 1);
					api_quest_SetQuestStep(userObjID, 677, 1);
					api_quest_SetJournalStep(userObjID, 677, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n338_littlegirl_daisy-1", "sq11_677_feed_blackbird2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n340_mystery_man--------------------------------------------------------------------------------
function sq11_676_feed_blackbird1_OnTalk_n340_mystery_man(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n340_mystery_man-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n340_mystery_man-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n340_mystery_man-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n340_mystery_man-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n340_mystery_man-3-b" then 
	end
	if npc_talk_index == "n340_mystery_man-3-c" then 
	end
	if npc_talk_index == "n340_mystery_man-3-d" then 
	end
	if npc_talk_index == "n340_mystery_man-3-e" then 
	end
	if npc_talk_index == "n340_mystery_man-3-chk_job1" then 
				if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n340_mystery_man-3-i";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n340_mystery_man-3-i";

				else
									npc_talk_index = "n340_mystery_man-3-f";

				end

				end
	end
	if npc_talk_index == "n340_mystery_man-3-g" then 
	end
	if npc_talk_index == "n340_mystery_man-3-h" then 
	end
	if npc_talk_index == "n340_mystery_man-3-ck_jo3" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n340_mystery_man-4";
	end
	if npc_talk_index == "n340_mystery_man-3-j" then 
	end
	if npc_talk_index == "n340_mystery_man-3-k" then 
	end
	if npc_talk_index == "n340_mystery_man-3-ck_jo2" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
				npc_talk_index = "n340_mystery_man-4";
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_676_feed_blackbird1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 676);
	if qstep == 2 and CountIndex == 400298 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 921 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400298, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400298, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200921 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400298, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400298, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_676_feed_blackbird1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 676);
	if qstep == 2 and CountIndex == 400298 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 921 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200921 and Count >= TargetCount  then

	end
end

function sq11_676_feed_blackbird1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 676);
	local questID=676;
end

function sq11_676_feed_blackbird1_OnRemoteStart( userObjID, questID )
end

function sq11_676_feed_blackbird1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_676_feed_blackbird1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,676, 1);
				api_quest_SetJournalStep(userObjID,676, 1);
				api_quest_SetQuestStep(userObjID,676, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";
end

</VillageServer>

<GameServer>
function sq11_676_feed_blackbird1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 338 then
		sq11_676_feed_blackbird1_OnTalk_n338_littlegirl_daisy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 340 then
		sq11_676_feed_blackbird1_OnTalk_n340_mystery_man( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n338_littlegirl_daisy--------------------------------------------------------------------------------
function sq11_676_feed_blackbird1_OnTalk_n338_littlegirl_daisy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n338_littlegirl_daisy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n338_littlegirl_daisy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n338_littlegirl_daisy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n338_littlegirl_daisy-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n338_littlegirl_daisy-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, false);
			 end 

	end
	if npc_talk_index == "n338_littlegirl_daisy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,676, 1);
				api_quest_SetJournalStep( pRoom, userObjID,676, 1);
				api_quest_SetQuestStep( pRoom, userObjID,676, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";

	end
	if npc_talk_index == "n338_littlegirl_daisy-1-b" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-c" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400298, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 921, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200921, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-b" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-c" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-d" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-e" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6760, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6760, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6760, questID, 1);
			 end 
	end
	if npc_talk_index == "n338_littlegirl_daisy-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 677, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 677, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 677, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n338_littlegirl_daisy-1", "sq11_677_feed_blackbird2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n340_mystery_man--------------------------------------------------------------------------------
function sq11_676_feed_blackbird1_OnTalk_n340_mystery_man( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n340_mystery_man-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n340_mystery_man-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n340_mystery_man-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n340_mystery_man-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n340_mystery_man-3-b" then 
	end
	if npc_talk_index == "n340_mystery_man-3-c" then 
	end
	if npc_talk_index == "n340_mystery_man-3-d" then 
	end
	if npc_talk_index == "n340_mystery_man-3-e" then 
	end
	if npc_talk_index == "n340_mystery_man-3-chk_job1" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n340_mystery_man-3-i";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n340_mystery_man-3-i";

				else
									npc_talk_index = "n340_mystery_man-3-f";

				end

				end
	end
	if npc_talk_index == "n340_mystery_man-3-g" then 
	end
	if npc_talk_index == "n340_mystery_man-3-h" then 
	end
	if npc_talk_index == "n340_mystery_man-3-ck_jo3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n340_mystery_man-4";
	end
	if npc_talk_index == "n340_mystery_man-3-j" then 
	end
	if npc_talk_index == "n340_mystery_man-3-k" then 
	end
	if npc_talk_index == "n340_mystery_man-3-ck_jo2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
				npc_talk_index = "n340_mystery_man-4";
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_676_feed_blackbird1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 676);
	if qstep == 2 and CountIndex == 400298 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 921 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400298, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400298, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 200921 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400298, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400298, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq11_676_feed_blackbird1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 676);
	if qstep == 2 and CountIndex == 400298 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 921 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200921 and Count >= TargetCount  then

	end
end

function sq11_676_feed_blackbird1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 676);
	local questID=676;
end

function sq11_676_feed_blackbird1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_676_feed_blackbird1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_676_feed_blackbird1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,676, 1);
				api_quest_SetJournalStep( pRoom, userObjID,676, 1);
				api_quest_SetQuestStep( pRoom, userObjID,676, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";
end

</GameServer>