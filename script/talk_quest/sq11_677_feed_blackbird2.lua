<VillageServer>

function sq11_677_feed_blackbird2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 338 then
		sq11_677_feed_blackbird2_OnTalk_n338_littlegirl_daisy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n338_littlegirl_daisy--------------------------------------------------------------------------------
function sq11_677_feed_blackbird2_OnTalk_n338_littlegirl_daisy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n338_littlegirl_daisy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n338_littlegirl_daisy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n338_littlegirl_daisy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n338_littlegirl_daisy-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n338_littlegirl_daisy-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6770, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6770, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6770, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6770, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6770, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6770, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6770, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6770, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6770, false);
			 end 

	end
	if npc_talk_index == "n338_littlegirl_daisy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,677, 1);
				api_quest_SetJournalStep(userObjID,677, 1);
				api_quest_SetQuestStep(userObjID,677, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";

	end
	if npc_talk_index == "n338_littlegirl_daisy-1-b" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-c" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-d" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1029, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1036, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 201029, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201036, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 400299, 3);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n338_littlegirl_daisy-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6770, true);
				 api_quest_RewardQuestUser(userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6770, true);
				 api_quest_RewardQuestUser(userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6770, true);
				 api_quest_RewardQuestUser(userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6770, true);
				 api_quest_RewardQuestUser(userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6770, true);
				 api_quest_RewardQuestUser(userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6770, true);
				 api_quest_RewardQuestUser(userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6770, true);
				 api_quest_RewardQuestUser(userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6770, true);
				 api_quest_RewardQuestUser(userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6770, true);
				 api_quest_RewardQuestUser(userObjID, 6770, questID, 1);
			 end 
	end
	if npc_talk_index == "n338_littlegirl_daisy-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_677_feed_blackbird2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 677);
	if qstep == 2 and CountIndex == 1029 then
				if api_quest_HasQuestItem(userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400299, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400299, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end
				if api_quest_HasQuestItem(userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 1036 then
				if api_quest_HasQuestItem(userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400299, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400299, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end
				if api_quest_HasQuestItem(userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 201029 then
				if api_quest_HasQuestItem(userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400299, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400299, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end
				if api_quest_HasQuestItem(userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 201036 then
				if api_quest_HasQuestItem(userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400299, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400299, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				end
				if api_quest_HasQuestItem(userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 400299 then

	end
end

function sq11_677_feed_blackbird2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 677);
	if qstep == 2 and CountIndex == 1029 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1036 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201029 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201036 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400299 and Count >= TargetCount  then

	end
end

function sq11_677_feed_blackbird2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 677);
	local questID=677;
end

function sq11_677_feed_blackbird2_OnRemoteStart( userObjID, questID )
end

function sq11_677_feed_blackbird2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_677_feed_blackbird2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,677, 1);
				api_quest_SetJournalStep(userObjID,677, 1);
				api_quest_SetQuestStep(userObjID,677, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";
end

</VillageServer>

<GameServer>
function sq11_677_feed_blackbird2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 338 then
		sq11_677_feed_blackbird2_OnTalk_n338_littlegirl_daisy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n338_littlegirl_daisy--------------------------------------------------------------------------------
function sq11_677_feed_blackbird2_OnTalk_n338_littlegirl_daisy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n338_littlegirl_daisy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n338_littlegirl_daisy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n338_littlegirl_daisy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n338_littlegirl_daisy-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n338_littlegirl_daisy-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, false);
			 end 

	end
	if npc_talk_index == "n338_littlegirl_daisy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,677, 1);
				api_quest_SetJournalStep( pRoom, userObjID,677, 1);
				api_quest_SetQuestStep( pRoom, userObjID,677, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";

	end
	if npc_talk_index == "n338_littlegirl_daisy-1-b" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-c" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-1-d" then 
	end
	if npc_talk_index == "n338_littlegirl_daisy-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1029, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1036, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 201029, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201036, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 400299, 3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n338_littlegirl_daisy-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6770, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6770, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6770, questID, 1);
			 end 
	end
	if npc_talk_index == "n338_littlegirl_daisy-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_677_feed_blackbird2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 677);
	if qstep == 2 and CountIndex == 1029 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400299, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400299, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 1036 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400299, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400299, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 201029 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400299, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400299, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 201036 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400299, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400299, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400299, 3) >= 3 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

	end
	if qstep == 2 and CountIndex == 400299 then

	end
end

function sq11_677_feed_blackbird2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 677);
	if qstep == 2 and CountIndex == 1029 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1036 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201029 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201036 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400299 and Count >= TargetCount  then

	end
end

function sq11_677_feed_blackbird2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 677);
	local questID=677;
end

function sq11_677_feed_blackbird2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_677_feed_blackbird2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_677_feed_blackbird2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,677, 1);
				api_quest_SetJournalStep( pRoom, userObjID,677, 1);
				api_quest_SetQuestStep( pRoom, userObjID,677, 1);
				npc_talk_index = "n338_littlegirl_daisy-1";
end

</GameServer>