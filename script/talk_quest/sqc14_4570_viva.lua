<VillageServer>

function sqc14_4570_viva_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1619 then
		sqc14_4570_viva_OnTalk_n1619_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1610 then
		sqc14_4570_viva_OnTalk_n1610_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1609 then
		sqc14_4570_viva_OnTalk_n1609_sorceress_pochetel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1619_sidel--------------------------------------------------------------------------------
function sqc14_4570_viva_OnTalk_n1619_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1619_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1619_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1619_sidel-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45700, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45700, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45700, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45700, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45700, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45700, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45700, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45700, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45700, false);
			 end 

	end
	if npc_talk_index == "n1619_sidel-accepting-b" then
				api_quest_AddQuest(userObjID,4570, 1);
				api_quest_SetJournalStep(userObjID,4570, 1);
				api_quest_SetQuestStep(userObjID,4570, 1);
				npc_talk_index = "n1619_sidel-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400436, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400436, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400437, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400437, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1610_royal_magician_kalaen--------------------------------------------------------------------------------
function sqc14_4570_viva_OnTalk_n1610_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1610_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1610_royal_magician_kalaen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1610_royal_magician_kalaen-1-a" then 

				if api_quest_HasQuestItem(userObjID, 400437, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400437, api_quest_HasQuestItem(userObjID, 400437, 1));
				end

				if api_quest_HasQuestItem(userObjID, 400436, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400436, api_quest_HasQuestItem(userObjID, 400436, 1));
				end
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-b" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-c" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-d" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-e" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-f" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-g" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-h" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-i" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-j" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-k" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1609_sorceress_pochetel--------------------------------------------------------------------------------
function sqc14_4570_viva_OnTalk_n1609_sorceress_pochetel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1609_sorceress_pochetel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1609_sorceress_pochetel-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 45700, true);
				 api_quest_RewardQuestUser(userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 45700, true);
				 api_quest_RewardQuestUser(userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 45700, true);
				 api_quest_RewardQuestUser(userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 45700, true);
				 api_quest_RewardQuestUser(userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 45700, true);
				 api_quest_RewardQuestUser(userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 45700, true);
				 api_quest_RewardQuestUser(userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 45700, true);
				 api_quest_RewardQuestUser(userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 45700, true);
				 api_quest_RewardQuestUser(userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 45700, true);
				 api_quest_RewardQuestUser(userObjID, 45700, questID, 1);
			 end 
	end
	if npc_talk_index == "n1609_sorceress_pochetel-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4570_viva_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4570);
end

function sqc14_4570_viva_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4570);
end

function sqc14_4570_viva_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4570);
	local questID=4570;
end

function sqc14_4570_viva_OnRemoteStart( userObjID, questID )
end

function sqc14_4570_viva_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc14_4570_viva_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sqc14_4570_viva_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1619 then
		sqc14_4570_viva_OnTalk_n1619_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1610 then
		sqc14_4570_viva_OnTalk_n1610_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1609 then
		sqc14_4570_viva_OnTalk_n1609_sorceress_pochetel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1619_sidel--------------------------------------------------------------------------------
function sqc14_4570_viva_OnTalk_n1619_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1619_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1619_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1619_sidel-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, false);
			 end 

	end
	if npc_talk_index == "n1619_sidel-accepting-b" then
				api_quest_AddQuest( pRoom, userObjID,4570, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4570, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4570, 1);
				npc_talk_index = "n1619_sidel-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400436, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400436, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400437, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400437, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1610_royal_magician_kalaen--------------------------------------------------------------------------------
function sqc14_4570_viva_OnTalk_n1610_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1610_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1610_royal_magician_kalaen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1610_royal_magician_kalaen-1-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400437, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400437, api_quest_HasQuestItem( pRoom, userObjID, 400437, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400436, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400436, api_quest_HasQuestItem( pRoom, userObjID, 400436, 1));
				end
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-b" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-c" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-d" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-e" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-f" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-g" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-h" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-i" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-j" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-1-k" then 
	end
	if npc_talk_index == "n1610_royal_magician_kalaen-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1609_sorceress_pochetel--------------------------------------------------------------------------------
function sqc14_4570_viva_OnTalk_n1609_sorceress_pochetel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1609_sorceress_pochetel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1609_sorceress_pochetel-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45700, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 45700, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 45700, questID, 1);
			 end 
	end
	if npc_talk_index == "n1609_sorceress_pochetel-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc14_4570_viva_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4570);
end

function sqc14_4570_viva_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4570);
end

function sqc14_4570_viva_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4570);
	local questID=4570;
end

function sqc14_4570_viva_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc14_4570_viva_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc14_4570_viva_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>