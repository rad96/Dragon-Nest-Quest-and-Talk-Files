<VillageServer>

function mq11_9314_devoured_things_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1139 then
		mq11_9314_devoured_things_OnTalk_n1139_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1153 then
		mq11_9314_devoured_things_OnTalk_n1153_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1154 then
		mq11_9314_devoured_things_OnTalk_n1154_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mq11_9314_devoured_things_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mq11_9314_devoured_things_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1139_lunaria--------------------------------------------------------------------------------
function mq11_9314_devoured_things_OnTalk_n1139_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1139_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1153_lunaria--------------------------------------------------------------------------------
function mq11_9314_devoured_things_OnTalk_n1153_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1153_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1153_lunaria-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1154_lunaria--------------------------------------------------------------------------------
function mq11_9314_devoured_things_OnTalk_n1154_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1154_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1154_lunaria-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1154_lunaria-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1154_lunaria-4-b" then 
	end
	if npc_talk_index == "n1154_lunaria-4-c" then 
	end
	if npc_talk_index == "n1154_lunaria-4-d" then 
	end
	if npc_talk_index == "n1154_lunaria-4-e" then 
	end
	if npc_talk_index == "n1154_lunaria-4-f" then 
	end
	if npc_talk_index == "n1154_lunaria-4-g" then 
	end
	if npc_talk_index == "n1154_lunaria-4-g" then 
	end
	if npc_talk_index == "n1154_lunaria-4-h" then 
	end
	if npc_talk_index == "n1154_lunaria-4-i" then 
	end
	if npc_talk_index == "n1154_lunaria-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mq11_9314_devoured_things_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n038_royal_magician_kalaen-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n038_royal_magician_kalaen-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-g" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-h" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93140, true);
				 api_quest_RewardQuestUser(userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93140, true);
				 api_quest_RewardQuestUser(userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93140, true);
				 api_quest_RewardQuestUser(userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93140, true);
				 api_quest_RewardQuestUser(userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93140, true);
				 api_quest_RewardQuestUser(userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93140, true);
				 api_quest_RewardQuestUser(userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93140, true);
				 api_quest_RewardQuestUser(userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93140, true);
				 api_quest_RewardQuestUser(userObjID, 93140, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem(userObjID, 300251, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300251, api_quest_HasQuestItem(userObjID, 300251, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300083, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300083, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9315, 2);
					api_quest_SetQuestStep(userObjID, 9315, 1);
					api_quest_SetJournalStep(userObjID, 9315, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n038_royal_magician_kalaen-1", "mq11_9315_in_the_dream.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mq11_9314_devoured_things_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9314, 2);
				api_quest_SetJournalStep(userObjID,9314, 1);
				api_quest_SetQuestStep(userObjID,9314, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-ck_level_32" then 
				if api_user_GetUserLevel(userObjID) >= 32 then
									npc_talk_index = "n042_general_duglars-1-a";

				else
									npc_talk_index = "n042_general_duglars-1-d";

				end
	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
	end
	if npc_talk_index == "n042_general_duglars-1-c" then 
	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9314_devoured_things_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9314);
end

function mq11_9314_devoured_things_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9314);
end

function mq11_9314_devoured_things_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9314);
	local questID=9314;
end

function mq11_9314_devoured_things_OnRemoteStart( userObjID, questID )
end

function mq11_9314_devoured_things_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_9314_devoured_things_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9314, 2);
				api_quest_SetJournalStep(userObjID,9314, 1);
				api_quest_SetQuestStep(userObjID,9314, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</VillageServer>

<GameServer>
function mq11_9314_devoured_things_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1139 then
		mq11_9314_devoured_things_OnTalk_n1139_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1153 then
		mq11_9314_devoured_things_OnTalk_n1153_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1154 then
		mq11_9314_devoured_things_OnTalk_n1154_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mq11_9314_devoured_things_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mq11_9314_devoured_things_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1139_lunaria--------------------------------------------------------------------------------
function mq11_9314_devoured_things_OnTalk_n1139_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1139_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1153_lunaria--------------------------------------------------------------------------------
function mq11_9314_devoured_things_OnTalk_n1153_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1153_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1153_lunaria-1-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1154_lunaria--------------------------------------------------------------------------------
function mq11_9314_devoured_things_OnTalk_n1154_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1154_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1154_lunaria-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1154_lunaria-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1154_lunaria-4-b" then 
	end
	if npc_talk_index == "n1154_lunaria-4-c" then 
	end
	if npc_talk_index == "n1154_lunaria-4-d" then 
	end
	if npc_talk_index == "n1154_lunaria-4-e" then 
	end
	if npc_talk_index == "n1154_lunaria-4-f" then 
	end
	if npc_talk_index == "n1154_lunaria-4-g" then 
	end
	if npc_talk_index == "n1154_lunaria-4-g" then 
	end
	if npc_talk_index == "n1154_lunaria-4-h" then 
	end
	if npc_talk_index == "n1154_lunaria-4-i" then 
	end
	if npc_talk_index == "n1154_lunaria-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mq11_9314_devoured_things_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n038_royal_magician_kalaen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n038_royal_magician_kalaen-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n038_royal_magician_kalaen-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-2-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-b" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-c" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-d" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-e" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-f" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-g" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-h" then 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93140, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93140, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93140, questID, 1);
			 end 
	end
	if npc_talk_index == "n038_royal_magician_kalaen-5-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem( pRoom, userObjID, 300251, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300251, api_quest_HasQuestItem( pRoom, userObjID, 300251, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300083, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300083, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9315, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9315, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9315, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n038_royal_magician_kalaen-1", "mq11_9315_in_the_dream.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mq11_9314_devoured_things_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9314, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9314, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9314, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-ck_level_32" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 32 then
									npc_talk_index = "n042_general_duglars-1-a";

				else
									npc_talk_index = "n042_general_duglars-1-d";

				end
	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
	end
	if npc_talk_index == "n042_general_duglars-1-c" then 
	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_9314_devoured_things_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9314);
end

function mq11_9314_devoured_things_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9314);
end

function mq11_9314_devoured_things_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9314);
	local questID=9314;
end

function mq11_9314_devoured_things_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_9314_devoured_things_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_9314_devoured_things_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9314, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9314, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9314, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</GameServer>