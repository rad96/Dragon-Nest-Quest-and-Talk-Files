<VillageServer>

function mqc15_9457_say_goodbye_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1587 then
		mqc15_9457_say_goodbye_OnTalk_n1587_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1599 then
		mqc15_9457_say_goodbye_OnTalk_n1599_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1589 then
		mqc15_9457_say_goodbye_OnTalk_n1589_gereint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1584 then
		mqc15_9457_say_goodbye_OnTalk_n1584_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mqc15_9457_say_goodbye_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1505 then
		mqc15_9457_say_goodbye_OnTalk_n1505_elf_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1551 then
		mqc15_9457_say_goodbye_OnTalk_n1551_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1499 then
		mqc15_9457_say_goodbye_OnTalk_n1499_gereint_sword(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1587_for_memory--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1587_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1587_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1599_pether--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1599_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1599_pether-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1599_pether-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1599_pether-2-b" then 
	end
	if npc_talk_index == "n1599_pether-2-c" then 
	end
	if npc_talk_index == "n1599_pether-2-d" then 
	end
	if npc_talk_index == "n1599_pether-2-e" then 
	end
	if npc_talk_index == "n1599_pether-2-f" then 
	end
	if npc_talk_index == "n1599_pether-2-g" then 
	end
	if npc_talk_index == "n1599_pether-2-h" then 
	end
	if npc_talk_index == "n1599_pether-2-i" then 
	end
	if npc_talk_index == "n1599_pether-2-j" then 
	end
	if npc_talk_index == "n1599_pether-2-k" then 
	end
	if npc_talk_index == "n1599_pether-2-l" then 
	end
	if npc_talk_index == "n1599_pether-2-m" then 
	end
	if npc_talk_index == "n1599_pether-2-n" then 
	end
	if npc_talk_index == "n1599_pether-2-o" then 
	end
	if npc_talk_index == "n1599_pether-2-p" then 
	end
	if npc_talk_index == "n1599_pether-2-q" then 
	end
	if npc_talk_index == "n1599_pether-2-r" then 
	end
	if npc_talk_index == "n1599_pether-2-s" then 
	end
	if npc_talk_index == "n1599_pether-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1589_gereint--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1589_gereint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1589_gereint-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1589_gereint-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1589_gereint-4-b" then 
	end
	if npc_talk_index == "n1589_gereint-4-c" then 
	end
	if npc_talk_index == "n1589_gereint-4-d" then 
	end
	if npc_talk_index == "n1589_gereint-4-e" then 
	end
	if npc_talk_index == "n1589_gereint-4-f" then 
	end
	if npc_talk_index == "n1589_gereint-4-g" then 
	end
	if npc_talk_index == "n1589_gereint-4-h" then 
	end
	if npc_talk_index == "n1589_gereint-4-i" then 
	end
	if npc_talk_index == "n1589_gereint-4-j" then 
	end
	if npc_talk_index == "n1589_gereint-4-k" then 
	end
	if npc_talk_index == "n1589_gereint-4-l" then 
	end
	if npc_talk_index == "n1589_gereint-4-m" then 
	end
	if npc_talk_index == "n1589_gereint-4-m" then 
	end
	if npc_talk_index == "n1589_gereint-4-m" then 
	end
	if npc_talk_index == "n1589_gereint-4-n" then 
	end
	if npc_talk_index == "n1589_gereint-4-n" then 
	end
	if npc_talk_index == "n1589_gereint-4-n" then 
	end
	if npc_talk_index == "n1589_gereint-4-o" then 
	end
	if npc_talk_index == "n1589_gereint-4-p" then 
	end
	if npc_talk_index == "n1589_gereint-4-q" then 
	end
	if npc_talk_index == "n1589_gereint-4-r" then 
	end
	if npc_talk_index == "n1589_gereint-4-s" then 
	end
	if npc_talk_index == "n1589_gereint-4-s" then 
	end
	if npc_talk_index == "n1589_gereint-4-t" then 
	end
	if npc_talk_index == "n1589_gereint-4-u" then 
	end
	if npc_talk_index == "n1589_gereint-4-v" then 
	end
	if npc_talk_index == "n1589_gereint-4-w" then 
	end
	if npc_talk_index == "n1589_gereint-4-w" then 
	end
	if npc_talk_index == "n1589_gereint-4-w" then 
	end
	if npc_talk_index == "n1589_gereint-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1584_gereint_kid--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1584_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1584_gereint_kid-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1584_gereint_kid-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1584_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-d" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-d" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-e" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-f" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-g" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-h" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-i" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-j" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-k" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-l" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-l" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-l" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-m" then 
	end
	if npc_talk_index == "n1584_gereint_kid-7" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1067_elder_elf-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1067_elder_elf-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9457, 2);
				api_quest_SetJournalStep(userObjID,9457, 1);
				api_quest_SetQuestStep(userObjID,9457, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-8" then 
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1505_elf_king--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1505_elf_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1505_elf_king-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1505_elf_king-8-b" then 
	end
	if npc_talk_index == "n1505_elf_king-8-c" then 
	end
	if npc_talk_index == "n1505_elf_king-8-d" then 
	end
	if npc_talk_index == "n1505_elf_king-8-e" then 
	end
	if npc_talk_index == "n1505_elf_king-8-f" then 
	end
	if npc_talk_index == "n1505_elf_king-8-g" then 
	end
	if npc_talk_index == "n1505_elf_king-8-h" then 
	end
	if npc_talk_index == "n1505_elf_king-8-i" then 
	end
	if npc_talk_index == "n1505_elf_king-8-j" then 
	end
	if npc_talk_index == "n1505_elf_king-8-k" then 
	end
	if npc_talk_index == "n1505_elf_king-8-l" then 
	end
	if npc_talk_index == "n1505_elf_king-8-m" then 
	end
	if npc_talk_index == "n1505_elf_king-8-n" then 
	end
	if npc_talk_index == "n1505_elf_king-8-o" then 
	end
	if npc_talk_index == "n1505_elf_king-8-p" then 
	end
	if npc_talk_index == "n1505_elf_king-8-q" then 
	end
	if npc_talk_index == "n1505_elf_king-8-r" then 
	end
	if npc_talk_index == "n1505_elf_king-8-s" then 
	end
	if npc_talk_index == "n1505_elf_king-8-t" then 
	end
	if npc_talk_index == "n1505_elf_king-8-u" then 
	end
	if npc_talk_index == "n1505_elf_king-8-v" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94571, true);
				 api_quest_RewardQuestUser(userObjID, 94571, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94572, true);
				 api_quest_RewardQuestUser(userObjID, 94572, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94573, true);
				 api_quest_RewardQuestUser(userObjID, 94573, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94574, true);
				 api_quest_RewardQuestUser(userObjID, 94574, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94575, true);
				 api_quest_RewardQuestUser(userObjID, 94575, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94576, true);
				 api_quest_RewardQuestUser(userObjID, 94576, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94577, true);
				 api_quest_RewardQuestUser(userObjID, 94577, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94578, true);
				 api_quest_RewardQuestUser(userObjID, 94578, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94579, true);
				 api_quest_RewardQuestUser(userObjID, 94579, questID, 1);
			 end 
	end
	if npc_talk_index == "n1505_elf_king-8-w" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9458, 2);
					api_quest_SetQuestStep(userObjID, 9458, 1);
					api_quest_SetJournalStep(userObjID, 9458, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1505_elf_king-8-x" then 
	end
	if npc_talk_index == "n1505_elf_king-8-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1505_elf_king-1", "mqc15_9458_the_story_of_prophecy.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1551_elder_elf--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1551_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1551_elder_elf-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1499_gereint_sword--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1499_gereint_sword(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9457_say_goodbye_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9457);
end

function mqc15_9457_say_goodbye_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9457);
end

function mqc15_9457_say_goodbye_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9457);
	local questID=9457;
end

function mqc15_9457_say_goodbye_OnRemoteStart( userObjID, questID )
end

function mqc15_9457_say_goodbye_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9457_say_goodbye_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9457, 2);
				api_quest_SetJournalStep(userObjID,9457, 1);
				api_quest_SetQuestStep(userObjID,9457, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</VillageServer>

<GameServer>
function mqc15_9457_say_goodbye_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1587 then
		mqc15_9457_say_goodbye_OnTalk_n1587_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1599 then
		mqc15_9457_say_goodbye_OnTalk_n1599_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1589 then
		mqc15_9457_say_goodbye_OnTalk_n1589_gereint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1584 then
		mqc15_9457_say_goodbye_OnTalk_n1584_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mqc15_9457_say_goodbye_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1505 then
		mqc15_9457_say_goodbye_OnTalk_n1505_elf_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1551 then
		mqc15_9457_say_goodbye_OnTalk_n1551_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1499 then
		mqc15_9457_say_goodbye_OnTalk_n1499_gereint_sword( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1587_for_memory--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1587_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1587_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1599_pether--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1599_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1599_pether-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1599_pether-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1599_pether-2-b" then 
	end
	if npc_talk_index == "n1599_pether-2-c" then 
	end
	if npc_talk_index == "n1599_pether-2-d" then 
	end
	if npc_talk_index == "n1599_pether-2-e" then 
	end
	if npc_talk_index == "n1599_pether-2-f" then 
	end
	if npc_talk_index == "n1599_pether-2-g" then 
	end
	if npc_talk_index == "n1599_pether-2-h" then 
	end
	if npc_talk_index == "n1599_pether-2-i" then 
	end
	if npc_talk_index == "n1599_pether-2-j" then 
	end
	if npc_talk_index == "n1599_pether-2-k" then 
	end
	if npc_talk_index == "n1599_pether-2-l" then 
	end
	if npc_talk_index == "n1599_pether-2-m" then 
	end
	if npc_talk_index == "n1599_pether-2-n" then 
	end
	if npc_talk_index == "n1599_pether-2-o" then 
	end
	if npc_talk_index == "n1599_pether-2-p" then 
	end
	if npc_talk_index == "n1599_pether-2-q" then 
	end
	if npc_talk_index == "n1599_pether-2-r" then 
	end
	if npc_talk_index == "n1599_pether-2-s" then 
	end
	if npc_talk_index == "n1599_pether-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1589_gereint--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1589_gereint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1589_gereint-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1589_gereint-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1589_gereint-4-b" then 
	end
	if npc_talk_index == "n1589_gereint-4-c" then 
	end
	if npc_talk_index == "n1589_gereint-4-d" then 
	end
	if npc_talk_index == "n1589_gereint-4-e" then 
	end
	if npc_talk_index == "n1589_gereint-4-f" then 
	end
	if npc_talk_index == "n1589_gereint-4-g" then 
	end
	if npc_talk_index == "n1589_gereint-4-h" then 
	end
	if npc_talk_index == "n1589_gereint-4-i" then 
	end
	if npc_talk_index == "n1589_gereint-4-j" then 
	end
	if npc_talk_index == "n1589_gereint-4-k" then 
	end
	if npc_talk_index == "n1589_gereint-4-l" then 
	end
	if npc_talk_index == "n1589_gereint-4-m" then 
	end
	if npc_talk_index == "n1589_gereint-4-m" then 
	end
	if npc_talk_index == "n1589_gereint-4-m" then 
	end
	if npc_talk_index == "n1589_gereint-4-n" then 
	end
	if npc_talk_index == "n1589_gereint-4-n" then 
	end
	if npc_talk_index == "n1589_gereint-4-n" then 
	end
	if npc_talk_index == "n1589_gereint-4-o" then 
	end
	if npc_talk_index == "n1589_gereint-4-p" then 
	end
	if npc_talk_index == "n1589_gereint-4-q" then 
	end
	if npc_talk_index == "n1589_gereint-4-r" then 
	end
	if npc_talk_index == "n1589_gereint-4-s" then 
	end
	if npc_talk_index == "n1589_gereint-4-s" then 
	end
	if npc_talk_index == "n1589_gereint-4-t" then 
	end
	if npc_talk_index == "n1589_gereint-4-u" then 
	end
	if npc_talk_index == "n1589_gereint-4-v" then 
	end
	if npc_talk_index == "n1589_gereint-4-w" then 
	end
	if npc_talk_index == "n1589_gereint-4-w" then 
	end
	if npc_talk_index == "n1589_gereint-4-w" then 
	end
	if npc_talk_index == "n1589_gereint-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1584_gereint_kid--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1584_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1584_gereint_kid-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1584_gereint_kid-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1584_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-b" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-c" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-d" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-d" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-e" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-f" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-g" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-h" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-i" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-j" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-k" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-l" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-l" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-l" then 
	end
	if npc_talk_index == "n1584_gereint_kid-6-m" then 
	end
	if npc_talk_index == "n1584_gereint_kid-7" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1067_elder_elf-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1067_elder_elf-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9457, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9457, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9457, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-7-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-8" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1505_elf_king--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1505_elf_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1505_elf_king-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1505_elf_king-8-b" then 
	end
	if npc_talk_index == "n1505_elf_king-8-c" then 
	end
	if npc_talk_index == "n1505_elf_king-8-d" then 
	end
	if npc_talk_index == "n1505_elf_king-8-e" then 
	end
	if npc_talk_index == "n1505_elf_king-8-f" then 
	end
	if npc_talk_index == "n1505_elf_king-8-g" then 
	end
	if npc_talk_index == "n1505_elf_king-8-h" then 
	end
	if npc_talk_index == "n1505_elf_king-8-i" then 
	end
	if npc_talk_index == "n1505_elf_king-8-j" then 
	end
	if npc_talk_index == "n1505_elf_king-8-k" then 
	end
	if npc_talk_index == "n1505_elf_king-8-l" then 
	end
	if npc_talk_index == "n1505_elf_king-8-m" then 
	end
	if npc_talk_index == "n1505_elf_king-8-n" then 
	end
	if npc_talk_index == "n1505_elf_king-8-o" then 
	end
	if npc_talk_index == "n1505_elf_king-8-p" then 
	end
	if npc_talk_index == "n1505_elf_king-8-q" then 
	end
	if npc_talk_index == "n1505_elf_king-8-r" then 
	end
	if npc_talk_index == "n1505_elf_king-8-s" then 
	end
	if npc_talk_index == "n1505_elf_king-8-t" then 
	end
	if npc_talk_index == "n1505_elf_king-8-u" then 
	end
	if npc_talk_index == "n1505_elf_king-8-v" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94571, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94571, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94572, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94572, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94573, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94573, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94574, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94574, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94575, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94575, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94576, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94576, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94577, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94577, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94578, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94578, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94579, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94579, questID, 1);
			 end 
	end
	if npc_talk_index == "n1505_elf_king-8-w" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9458, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9458, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9458, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1505_elf_king-8-x" then 
	end
	if npc_talk_index == "n1505_elf_king-8-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1505_elf_king-1", "mqc15_9458_the_story_of_prophecy.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1551_elder_elf--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1551_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n1551_elder_elf-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1499_gereint_sword--------------------------------------------------------------------------------
function mqc15_9457_say_goodbye_OnTalk_n1499_gereint_sword( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9457_say_goodbye_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9457);
end

function mqc15_9457_say_goodbye_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9457);
end

function mqc15_9457_say_goodbye_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9457);
	local questID=9457;
end

function mqc15_9457_say_goodbye_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9457_say_goodbye_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9457_say_goodbye_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9457, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9457, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9457, 1);
				npc_talk_index = "n1067_elder_elf-1";
end

</GameServer>