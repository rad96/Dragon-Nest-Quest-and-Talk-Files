<VillageServer>

function nq12_7204_bellin_talk_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 91 then
		nq12_7204_bellin_talk_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n091_trader_bellin--------------------------------------------------------------------------------
function nq12_7204_bellin_talk_OnTalk_n091_trader_bellin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-a" then
				api_npc_AddFavorPoint( userObjID, 91, 100 );
				api_quest_ForceCompleteQuest(userObjID, 7204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n091_trader_bellin-accepting-b" then
				api_npc_AddFavorPoint( userObjID, 91, 200 );
				api_quest_ForceCompleteQuest(userObjID, 7204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n091_trader_bellin-accepting-c" then
				api_npc_AddFavorPoint( userObjID, 91, 400 );
				api_quest_ForceCompleteQuest(userObjID, 7204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n091_trader_bellin-accepting-d" then
				api_npc_AddMalicePoint( userObjID, 91, 400 );
				api_quest_ForceCompleteQuest(userObjID, 7204, 0, 1, 1, 0);

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq12_7204_bellin_talk_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7204);
end

function nq12_7204_bellin_talk_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7204);
end

function nq12_7204_bellin_talk_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7204);
	local questID=7204;
end

function nq12_7204_bellin_talk_OnRemoteStart( userObjID, questID )
end

function nq12_7204_bellin_talk_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq12_7204_bellin_talk_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq12_7204_bellin_talk_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 91 then
		nq12_7204_bellin_talk_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n091_trader_bellin--------------------------------------------------------------------------------
function nq12_7204_bellin_talk_OnTalk_n091_trader_bellin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n091_trader_bellin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n091_trader_bellin-accepting-a" then
				api_npc_AddFavorPoint( pRoom,  userObjID, 91, 100 );
				api_quest_ForceCompleteQuest( pRoom, userObjID, 7204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n091_trader_bellin-accepting-b" then
				api_npc_AddFavorPoint( pRoom,  userObjID, 91, 200 );
				api_quest_ForceCompleteQuest( pRoom, userObjID, 7204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n091_trader_bellin-accepting-c" then
				api_npc_AddFavorPoint( pRoom,  userObjID, 91, 400 );
				api_quest_ForceCompleteQuest( pRoom, userObjID, 7204, 0, 1, 1, 0);

	end
	if npc_talk_index == "n091_trader_bellin-accepting-d" then
				api_npc_AddMalicePoint( pRoom,  userObjID, 91, 400 );
				api_quest_ForceCompleteQuest( pRoom, userObjID, 7204, 0, 1, 1, 0);

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq12_7204_bellin_talk_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7204);
end

function nq12_7204_bellin_talk_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7204);
end

function nq12_7204_bellin_talk_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7204);
	local questID=7204;
end

function nq12_7204_bellin_talk_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq12_7204_bellin_talk_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq12_7204_bellin_talk_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>