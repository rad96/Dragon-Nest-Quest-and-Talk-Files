<VillageServer>

function mq05_107_internal_enemy_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 13 then
		mq05_107_internal_enemy_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 156 then
		mq05_107_internal_enemy_OnTalk_n156_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n013_cleric_tomas--------------------------------------------------------------------------------
function mq05_107_internal_enemy_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n013_cleric_tomas-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n013_cleric_tomas-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n013_cleric_tomas-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted" then
				api_quest_AddQuest(userObjID,107, 2);
				api_quest_SetJournalStep(userObjID,107, 1);
				api_quest_SetQuestStep(userObjID,107, 1);
				npc_talk_index = "n013_cleric_tomas-1";

	end
	if npc_talk_index == "n013_cleric_tomas-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);

				if api_quest_HasQuestItem(userObjID, 300059, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300059, api_quest_HasQuestItem(userObjID, 300059, 1));
				end
	end
	if npc_talk_index == "n013_cleric_tomas-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n013_cleric_tomas-5-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-5-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-5-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1070, true);
				 api_quest_RewardQuestUser(userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1070, true);
				 api_quest_RewardQuestUser(userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1070, true);
				 api_quest_RewardQuestUser(userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1070, true);
				 api_quest_RewardQuestUser(userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1070, true);
				 api_quest_RewardQuestUser(userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1070, true);
				 api_quest_RewardQuestUser(userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1070, true);
				 api_quest_RewardQuestUser(userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1070, true);
				 api_quest_RewardQuestUser(userObjID, 1070, questID, 1);
			 end 
	end
	if npc_talk_index == "n013_cleric_tomas-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 108, 2);
					api_quest_SetQuestStep(userObjID, 108, 1);
					api_quest_SetJournalStep(userObjID, 108, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n013_cleric_tomas-1", "mq05_108_chase_and_chased.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n156_cleric_jake--------------------------------------------------------------------------------
function mq05_107_internal_enemy_OnTalk_n156_cleric_jake(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n156_cleric_jake-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n156_cleric_jake-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n156_cleric_jake-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n156_cleric_jake-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n156_cleric_jake-4" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 269, 30001);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_107_internal_enemy_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 107);
	if qstep == 4 and CountIndex == 269 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 4);

	end
end

function mq05_107_internal_enemy_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 107);
	if qstep == 4 and CountIndex == 269 and Count >= TargetCount  then

	end
end

function mq05_107_internal_enemy_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 107);
	local questID=107;
end

function mq05_107_internal_enemy_OnRemoteStart( userObjID, questID )
end

function mq05_107_internal_enemy_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq05_107_internal_enemy_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,107, 2);
				api_quest_SetJournalStep(userObjID,107, 1);
				api_quest_SetQuestStep(userObjID,107, 1);
				npc_talk_index = "n013_cleric_tomas-1";
end

</VillageServer>

<GameServer>
function mq05_107_internal_enemy_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 13 then
		mq05_107_internal_enemy_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 156 then
		mq05_107_internal_enemy_OnTalk_n156_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n013_cleric_tomas--------------------------------------------------------------------------------
function mq05_107_internal_enemy_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n013_cleric_tomas-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n013_cleric_tomas-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n013_cleric_tomas-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,107, 2);
				api_quest_SetJournalStep( pRoom, userObjID,107, 1);
				api_quest_SetQuestStep( pRoom, userObjID,107, 1);
				npc_talk_index = "n013_cleric_tomas-1";

	end
	if npc_talk_index == "n013_cleric_tomas-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);

				if api_quest_HasQuestItem( pRoom, userObjID, 300059, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300059, api_quest_HasQuestItem( pRoom, userObjID, 300059, 1));
				end
	end
	if npc_talk_index == "n013_cleric_tomas-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n013_cleric_tomas-5-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-5-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-5-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1070, questID, 1);
			 end 
	end
	if npc_talk_index == "n013_cleric_tomas-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 108, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 108, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 108, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n013_cleric_tomas-1", "mq05_108_chase_and_chased.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n156_cleric_jake--------------------------------------------------------------------------------
function mq05_107_internal_enemy_OnTalk_n156_cleric_jake( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n156_cleric_jake-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n156_cleric_jake-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n156_cleric_jake-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n156_cleric_jake-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n156_cleric_jake-4" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 269, 30001);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_107_internal_enemy_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 107);
	if qstep == 4 and CountIndex == 269 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

	end
end

function mq05_107_internal_enemy_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 107);
	if qstep == 4 and CountIndex == 269 and Count >= TargetCount  then

	end
end

function mq05_107_internal_enemy_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 107);
	local questID=107;
end

function mq05_107_internal_enemy_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq05_107_internal_enemy_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq05_107_internal_enemy_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,107, 2);
				api_quest_SetJournalStep( pRoom, userObjID,107, 1);
				api_quest_SetQuestStep( pRoom, userObjID,107, 1);
				npc_talk_index = "n013_cleric_tomas-1";
end

</GameServer>