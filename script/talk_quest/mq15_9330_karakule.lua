<VillageServer>

function mq15_9330_karakule_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1147 then
		mq15_9330_karakule_OnTalk_n1147_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 147 then
		mq15_9330_karakule_OnTalk_n147_mailbox(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		mq15_9330_karakule_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1147_lunaria--------------------------------------------------------------------------------
function mq15_9330_karakule_OnTalk_n1147_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1147_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n147_mailbox--------------------------------------------------------------------------------
function mq15_9330_karakule_OnTalk_n147_mailbox(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n147_mailbox-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n147_mailbox-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n147_mailbox-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n147_mailbox-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9330, 2);
				api_quest_SetJournalStep(userObjID,9330, 1);
				api_quest_SetQuestStep(userObjID,9330, 1);
				npc_talk_index = "n147_mailbox-1";

	end
	if npc_talk_index == "n147_mailbox-1-b" then 
	end
	if npc_talk_index == "n147_mailbox-1-letter_0" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n147_mailbox-2";
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function mq15_9330_karakule_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n754_cian-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-1-cian_0" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				npc_talk_index = "n754_cian-2";
	end
	if npc_talk_index == "n754_cian-2-b" then 
	end
	if npc_talk_index == "n754_cian-2-c" then 
	end
	if npc_talk_index == "n754_cian-2-d" then 
	end
	if npc_talk_index == "n754_cian-2-e" then 
	end
	if npc_talk_index == "n754_cian-2-f" then 
	end
	if npc_talk_index == "n754_cian-2-g" then 
	end
	if npc_talk_index == "n754_cian-2-h" then 
	end
	if npc_talk_index == "n754_cian-2-i" then 
	end
	if npc_talk_index == "n754_cian-2-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93300, true);
				 api_quest_RewardQuestUser(userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93300, true);
				 api_quest_RewardQuestUser(userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93300, true);
				 api_quest_RewardQuestUser(userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93300, true);
				 api_quest_RewardQuestUser(userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93300, true);
				 api_quest_RewardQuestUser(userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93300, true);
				 api_quest_RewardQuestUser(userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93300, true);
				 api_quest_RewardQuestUser(userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93300, true);
				 api_quest_RewardQuestUser(userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93300, true);
				 api_quest_RewardQuestUser(userObjID, 93300, questID, 1);
			 end 
	end
	if npc_talk_index == "n754_cian-2-k" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9331, 2);
					api_quest_SetQuestStep(userObjID, 9331, 1);
					api_quest_SetJournalStep(userObjID, 9331, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n754_cian-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n754_cian-1", "mq15_9331_old_fellowship.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9330_karakule_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9330);
end

function mq15_9330_karakule_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9330);
end

function mq15_9330_karakule_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9330);
	local questID=9330;
end

function mq15_9330_karakule_OnRemoteStart( userObjID, questID )
end

function mq15_9330_karakule_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9330_karakule_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9330, 2);
				api_quest_SetJournalStep(userObjID,9330, 1);
				api_quest_SetQuestStep(userObjID,9330, 1);
				npc_talk_index = "n147_mailbox-1";
end

</VillageServer>

<GameServer>
function mq15_9330_karakule_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1147 then
		mq15_9330_karakule_OnTalk_n1147_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 147 then
		mq15_9330_karakule_OnTalk_n147_mailbox( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		mq15_9330_karakule_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1147_lunaria--------------------------------------------------------------------------------
function mq15_9330_karakule_OnTalk_n1147_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1147_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n147_mailbox--------------------------------------------------------------------------------
function mq15_9330_karakule_OnTalk_n147_mailbox( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n147_mailbox-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n147_mailbox-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n147_mailbox-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n147_mailbox-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9330, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9330, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9330, 1);
				npc_talk_index = "n147_mailbox-1";

	end
	if npc_talk_index == "n147_mailbox-1-b" then 
	end
	if npc_talk_index == "n147_mailbox-1-letter_0" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n147_mailbox-2";
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function mq15_9330_karakule_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n754_cian-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-1-cian_0" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				npc_talk_index = "n754_cian-2";
	end
	if npc_talk_index == "n754_cian-2-b" then 
	end
	if npc_talk_index == "n754_cian-2-c" then 
	end
	if npc_talk_index == "n754_cian-2-d" then 
	end
	if npc_talk_index == "n754_cian-2-e" then 
	end
	if npc_talk_index == "n754_cian-2-f" then 
	end
	if npc_talk_index == "n754_cian-2-g" then 
	end
	if npc_talk_index == "n754_cian-2-h" then 
	end
	if npc_talk_index == "n754_cian-2-i" then 
	end
	if npc_talk_index == "n754_cian-2-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93300, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93300, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93300, questID, 1);
			 end 
	end
	if npc_talk_index == "n754_cian-2-k" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9331, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9331, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9331, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n754_cian-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n754_cian-1", "mq15_9331_old_fellowship.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9330_karakule_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9330);
end

function mq15_9330_karakule_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9330);
end

function mq15_9330_karakule_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9330);
	local questID=9330;
end

function mq15_9330_karakule_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9330_karakule_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9330_karakule_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9330, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9330, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9330, 1);
				npc_talk_index = "n147_mailbox-1";
end

</GameServer>