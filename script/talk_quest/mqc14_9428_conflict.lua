<VillageServer>

function mqc14_9428_conflict_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1551 then
		mqc14_9428_conflict_OnTalk_n1551_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1505 then
		mqc14_9428_conflict_OnTalk_n1505_elf_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		mqc14_9428_conflict_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1552 then
		mqc14_9428_conflict_OnTalk_n1552_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1553 then
		mqc14_9428_conflict_OnTalk_n1553_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1551_elder_elf--------------------------------------------------------------------------------
function mqc14_9428_conflict_OnTalk_n1551_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1551_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1551_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1551_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-m" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-p" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-q" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-r" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-s" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-t" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-u" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-v" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-v" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-v" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-w" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-x" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-y" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-z" then 
	end
	if npc_talk_index == "n1551_elder_elf-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1505_elf_king--------------------------------------------------------------------------------
function mqc14_9428_conflict_OnTalk_n1505_elf_king(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1505_elf_king-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function mqc14_9428_conflict_OnTalk_n1138_elf_guard_sitredel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1138_elf_guard_sitredel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1138_elf_guard_sitredel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9428, 2);
				api_quest_SetJournalStep(userObjID,9428, 1);
				api_quest_SetQuestStep(userObjID,9428, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-f" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-h" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-i" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 1011, 1 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1552_gaharam--------------------------------------------------------------------------------
function mqc14_9428_conflict_OnTalk_n1552_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1552_gaharam-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1552_gaharam-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1552_gaharam-4-check_class01" then 
				if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n1552_gaharam-4-b";

				else
									npc_talk_index = "n1552_gaharam-4-a";

				end
	end
	if npc_talk_index == "n1552_gaharam-4-c" then 
	end
	if npc_talk_index == "n1552_gaharam-4-c" then 
	end
	if npc_talk_index == "n1552_gaharam-4-d" then 
	end
	if npc_talk_index == "n1552_gaharam-4-e" then 
	end
	if npc_talk_index == "n1552_gaharam-4-f" then 
	end
	if npc_talk_index == "n1552_gaharam-4-g" then 
	end
	if npc_talk_index == "n1552_gaharam-4-h" then 
	end
	if npc_talk_index == "n1552_gaharam-4-i" then 
	end
	if npc_talk_index == "n1552_gaharam-4-j" then 
	end
	if npc_talk_index == "n1552_gaharam-4-k" then 
	end
	if npc_talk_index == "n1552_gaharam-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1553_for_memory--------------------------------------------------------------------------------
function mqc14_9428_conflict_OnTalk_n1553_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1553_for_memory-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1553_for_memory-5-b" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400452, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400452, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1553_for_memory-5-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94280, true);
				 api_quest_RewardQuestUser(userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94280, true);
				 api_quest_RewardQuestUser(userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94280, true);
				 api_quest_RewardQuestUser(userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94280, true);
				 api_quest_RewardQuestUser(userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94280, true);
				 api_quest_RewardQuestUser(userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94280, true);
				 api_quest_RewardQuestUser(userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94280, true);
				 api_quest_RewardQuestUser(userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94280, true);
				 api_quest_RewardQuestUser(userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94280, true);
				 api_quest_RewardQuestUser(userObjID, 94280, questID, 1);
			 end 
	end
	if npc_talk_index == "n1553_for_memory-5-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9429, 2);
					api_quest_SetQuestStep(userObjID, 9429, 1);
					api_quest_SetJournalStep(userObjID, 9429, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1553_for_memory-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1553_for_memory-1", "mqc14_9429_trap.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9428_conflict_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9428);
	 local qstep= 2; 
	 if qstep == 2 then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
		 end 
	 end 
end

function mqc14_9428_conflict_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9428);
end

function mqc14_9428_conflict_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9428);
	local questID=9428;
end

function mqc14_9428_conflict_OnRemoteStart( userObjID, questID )
end

function mqc14_9428_conflict_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9428_conflict_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9428, 2);
				api_quest_SetJournalStep(userObjID,9428, 1);
				api_quest_SetQuestStep(userObjID,9428, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
end

</VillageServer>

<GameServer>
function mqc14_9428_conflict_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1551 then
		mqc14_9428_conflict_OnTalk_n1551_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1505 then
		mqc14_9428_conflict_OnTalk_n1505_elf_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1138 then
		mqc14_9428_conflict_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1552 then
		mqc14_9428_conflict_OnTalk_n1552_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1553 then
		mqc14_9428_conflict_OnTalk_n1553_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1551_elder_elf--------------------------------------------------------------------------------
function mqc14_9428_conflict_OnTalk_n1551_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1551_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1551_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1551_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-l" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-m" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-p" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-q" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-r" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-s" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-t" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-u" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-v" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-v" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-v" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-w" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-x" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-y" then 
	end
	if npc_talk_index == "n1551_elder_elf-1-z" then 
	end
	if npc_talk_index == "n1551_elder_elf-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1505_elf_king--------------------------------------------------------------------------------
function mqc14_9428_conflict_OnTalk_n1505_elf_king( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1505_elf_king-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1138_elf_guard_sitredel--------------------------------------------------------------------------------
function mqc14_9428_conflict_OnTalk_n1138_elf_guard_sitredel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1138_elf_guard_sitredel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1138_elf_guard_sitredel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1138_elf_guard_sitredel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1138_elf_guard_sitredel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1138_elf_guard_sitredel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9428, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9428, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9428, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";

	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-b" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-c" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-d" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-e" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-f" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-g" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-h" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-2-i" then 
	end
	if npc_talk_index == "n1138_elf_guard_sitredel-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 1011, 1 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1552_gaharam--------------------------------------------------------------------------------
function mqc14_9428_conflict_OnTalk_n1552_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1552_gaharam-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1552_gaharam-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1552_gaharam-4-check_class01" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n1552_gaharam-4-b";

				else
									npc_talk_index = "n1552_gaharam-4-a";

				end
	end
	if npc_talk_index == "n1552_gaharam-4-c" then 
	end
	if npc_talk_index == "n1552_gaharam-4-c" then 
	end
	if npc_talk_index == "n1552_gaharam-4-d" then 
	end
	if npc_talk_index == "n1552_gaharam-4-e" then 
	end
	if npc_talk_index == "n1552_gaharam-4-f" then 
	end
	if npc_talk_index == "n1552_gaharam-4-g" then 
	end
	if npc_talk_index == "n1552_gaharam-4-h" then 
	end
	if npc_talk_index == "n1552_gaharam-4-i" then 
	end
	if npc_talk_index == "n1552_gaharam-4-j" then 
	end
	if npc_talk_index == "n1552_gaharam-4-k" then 
	end
	if npc_talk_index == "n1552_gaharam-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1553_for_memory--------------------------------------------------------------------------------
function mqc14_9428_conflict_OnTalk_n1553_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1553_for_memory-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1553_for_memory-5-b" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400452, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400452, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1553_for_memory-5-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94280, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94280, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94280, questID, 1);
			 end 
	end
	if npc_talk_index == "n1553_for_memory-5-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9429, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9429, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9429, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1553_for_memory-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1553_for_memory-1", "mqc14_9429_trap.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9428_conflict_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9428);
	 local qstep= 2; 
	 if qstep == 2 then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
		 end 
	 end 
end

function mqc14_9428_conflict_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9428);
end

function mqc14_9428_conflict_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9428);
	local questID=9428;
end

function mqc14_9428_conflict_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9428_conflict_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9428_conflict_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9428, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9428, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9428, 1);
				npc_talk_index = "n1138_elf_guard_sitredel-1";
end

</GameServer>