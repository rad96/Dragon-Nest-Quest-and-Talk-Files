<VillageServer>

function nq01_5101_how_to_sale_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 26 then
		nq01_5101_how_to_sale_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function nq01_5101_how_to_sale_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest(userObjID,5101, 1);
				api_quest_SetJournalStep(userObjID,5101, 1);
				api_quest_SetQuestStep(userObjID,5101, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-1-b" then 
	end
	if npc_talk_index == "n026_trader_may-1-c" then 
	end
	if npc_talk_index == "n026_trader_may-1-d" then 
				api_npc_Rage( userObjID, 26 );
	end
	if npc_talk_index == "n026_trader_may-1-e" then 
	end
	if npc_talk_index == "n026_trader_may-1-f" then 
				api_npc_Rage( userObjID, 26 );
	end
	if npc_talk_index == "n026_trader_may-1-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 26, 400 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq01_5101_how_to_sale_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5101);
end

function nq01_5101_how_to_sale_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5101);
end

function nq01_5101_how_to_sale_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5101);
	local questID=5101;
end

function nq01_5101_how_to_sale_OnRemoteStart( userObjID, questID )
end

function nq01_5101_how_to_sale_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq01_5101_how_to_sale_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,5101, 1);
				api_quest_SetJournalStep(userObjID,5101, 1);
				api_quest_SetQuestStep(userObjID,5101, 1);
				npc_talk_index = "n026_trader_may-1";
end

</VillageServer>

<GameServer>
function nq01_5101_how_to_sale_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 26 then
		nq01_5101_how_to_sale_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function nq01_5101_how_to_sale_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,5101, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5101, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5101, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-1-b" then 
	end
	if npc_talk_index == "n026_trader_may-1-c" then 
	end
	if npc_talk_index == "n026_trader_may-1-d" then 
				api_npc_Rage( pRoom,  userObjID, 26 );
	end
	if npc_talk_index == "n026_trader_may-1-e" then 
	end
	if npc_talk_index == "n026_trader_may-1-f" then 
				api_npc_Rage( pRoom,  userObjID, 26 );
	end
	if npc_talk_index == "n026_trader_may-1-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 26, 400 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq01_5101_how_to_sale_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5101);
end

function nq01_5101_how_to_sale_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5101);
end

function nq01_5101_how_to_sale_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5101);
	local questID=5101;
end

function nq01_5101_how_to_sale_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq01_5101_how_to_sale_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq01_5101_how_to_sale_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,5101, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5101, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5101, 1);
				npc_talk_index = "n026_trader_may-1";
end

</GameServer>