<VillageServer>

function sqc16_4653_red_and_silver_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1103 then
		sqc16_4653_red_and_silver_OnTalk_n1103_trader_phara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1801 then
		sqc16_4653_red_and_silver_OnTalk_n1801_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1788 then
		sqc16_4653_red_and_silver_OnTalk_n1788_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1103_trader_phara--------------------------------------------------------------------------------
function sqc16_4653_red_and_silver_OnTalk_n1103_trader_phara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1103_trader_phara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1103_trader_phara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1103_trader_phara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1103_trader_phara-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1103_trader_phara-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1103_trader_phara-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46530, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46530, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46530, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46530, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46530, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46530, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46530, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46530, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46530, false);
			 end 

	end
	if npc_talk_index == "n1103_trader_phara-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4653, 1);
				api_quest_SetJournalStep(userObjID,4653, 1);
				api_quest_SetQuestStep(userObjID,4653, 1);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400475, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400475, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n1103_trader_phara-1";

	end
	if npc_talk_index == "n1103_trader_phara-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n1103_trader_phara-7-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-7-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-7-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46530, true);
				 api_quest_RewardQuestUser(userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46530, true);
				 api_quest_RewardQuestUser(userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46530, true);
				 api_quest_RewardQuestUser(userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46530, true);
				 api_quest_RewardQuestUser(userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46530, true);
				 api_quest_RewardQuestUser(userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46530, true);
				 api_quest_RewardQuestUser(userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46530, true);
				 api_quest_RewardQuestUser(userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46530, true);
				 api_quest_RewardQuestUser(userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46530, true);
				 api_quest_RewardQuestUser(userObjID, 46530, questID, 1);
			 end 
	end
	if npc_talk_index == "n1103_trader_phara-7-d" then 

				if api_quest_HasQuestItem(userObjID, 400475, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400475, api_quest_HasQuestItem(userObjID, 400475, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1801_argenta--------------------------------------------------------------------------------
function sqc16_4653_red_and_silver_OnTalk_n1801_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1801_argenta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1788_rubinat--------------------------------------------------------------------------------
function sqc16_4653_red_and_silver_OnTalk_n1788_rubinat(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1788_rubinat-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1788_rubinat-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1788_rubinat-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1788_rubinat-4-b" then 
	end
	if npc_talk_index == "n1788_rubinat-4-c" then 
	end
	if npc_talk_index == "n1788_rubinat-4-d" then 
	end
	if npc_talk_index == "n1788_rubinat-4-e" then 
	end
	if npc_talk_index == "n1788_rubinat-4-f" then 
	end
	if npc_talk_index == "n1788_rubinat-4-g" then 
	end
	if npc_talk_index == "n1788_rubinat-4-h" then 
	end
	if npc_talk_index == "n1788_rubinat-4-i" then 
	end
	if npc_talk_index == "n1788_rubinat-4-j" then 
	end
	if npc_talk_index == "n1788_rubinat-4-k" then 
	end
	if npc_talk_index == "n1788_rubinat-4-l" then 
	end
	if npc_talk_index == "n1788_rubinat-4-m" then 
	end
	if npc_talk_index == "n1788_rubinat-4-n" then 
	end
	if npc_talk_index == "n1788_rubinat-4-o" then 
	end
	if npc_talk_index == "n1788_rubinat-4-p" then 
	end
	if npc_talk_index == "n1788_rubinat-4-q" then 
	end
	if npc_talk_index == "n1788_rubinat-4-r" then 
	end
	if npc_talk_index == "n1788_rubinat-4-s" then 
	end
	if npc_talk_index == "n1788_rubinat-4-t" then 
	end
	if npc_talk_index == "n1788_rubinat-4-u" then 
	end
	if npc_talk_index == "n1788_rubinat-4-v" then 
	end
	if npc_talk_index == "n1788_rubinat-5" then 
				api_quest_SetQuestStep(userObjID, questID,5);
	end
	if npc_talk_index == "n1788_rubinat-5-b" then 
	end
	if npc_talk_index == "n1788_rubinat-5-c" then 
	end
	if npc_talk_index == "n1788_rubinat-5-d" then 
	end
	if npc_talk_index == "n1788_rubinat-5-e" then 
	end
	if npc_talk_index == "n1788_rubinat-5-f" then 
	end
	if npc_talk_index == "n1788_rubinat-5-g" then 
	end
	if npc_talk_index == "n1788_rubinat-5-h" then 
	end
	if npc_talk_index == "n1788_rubinat-5-i" then 
	end
	if npc_talk_index == "n1788_rubinat-5-j" then 
	end
	if npc_talk_index == "n1788_rubinat-5-k" then 
	end
	if npc_talk_index == "n1788_rubinat-5-l" then 
	end
	if npc_talk_index == "n1788_rubinat-5-m" then 
	end
	if npc_talk_index == "n1788_rubinat-5-n" then 
	end
	if npc_talk_index == "n1788_rubinat-5-o" then 
	end
	if npc_talk_index == "n1788_rubinat-5-p" then 
	end
	if npc_talk_index == "n1788_rubinat-5-q" then 
	end
	if npc_talk_index == "n1788_rubinat-5-r" then 
	end
	if npc_talk_index == "n1788_rubinat-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4653_red_and_silver_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4653);
end

function sqc16_4653_red_and_silver_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4653);
end

function sqc16_4653_red_and_silver_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4653);
	local questID=4653;
end

function sqc16_4653_red_and_silver_OnRemoteStart( userObjID, questID )
end

function sqc16_4653_red_and_silver_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc16_4653_red_and_silver_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4653, 1);
				api_quest_SetJournalStep(userObjID,4653, 1);
				api_quest_SetQuestStep(userObjID,4653, 1);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400475, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400475, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n1103_trader_phara-1";
end

</VillageServer>

<GameServer>
function sqc16_4653_red_and_silver_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1103 then
		sqc16_4653_red_and_silver_OnTalk_n1103_trader_phara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1801 then
		sqc16_4653_red_and_silver_OnTalk_n1801_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1788 then
		sqc16_4653_red_and_silver_OnTalk_n1788_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1103_trader_phara--------------------------------------------------------------------------------
function sqc16_4653_red_and_silver_OnTalk_n1103_trader_phara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1103_trader_phara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1103_trader_phara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1103_trader_phara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1103_trader_phara-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1103_trader_phara-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1103_trader_phara-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, false);
			 end 

	end
	if npc_talk_index == "n1103_trader_phara-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4653, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4653, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4653, 1);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400475, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400475, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n1103_trader_phara-1";

	end
	if npc_talk_index == "n1103_trader_phara-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n1103_trader_phara-7-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-7-b" then 
	end
	if npc_talk_index == "n1103_trader_phara-7-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46530, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46530, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46530, questID, 1);
			 end 
	end
	if npc_talk_index == "n1103_trader_phara-7-d" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400475, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400475, api_quest_HasQuestItem( pRoom, userObjID, 400475, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1801_argenta--------------------------------------------------------------------------------
function sqc16_4653_red_and_silver_OnTalk_n1801_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1801_argenta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1788_rubinat--------------------------------------------------------------------------------
function sqc16_4653_red_and_silver_OnTalk_n1788_rubinat( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1788_rubinat-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1788_rubinat-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1788_rubinat-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1788_rubinat-4-b" then 
	end
	if npc_talk_index == "n1788_rubinat-4-c" then 
	end
	if npc_talk_index == "n1788_rubinat-4-d" then 
	end
	if npc_talk_index == "n1788_rubinat-4-e" then 
	end
	if npc_talk_index == "n1788_rubinat-4-f" then 
	end
	if npc_talk_index == "n1788_rubinat-4-g" then 
	end
	if npc_talk_index == "n1788_rubinat-4-h" then 
	end
	if npc_talk_index == "n1788_rubinat-4-i" then 
	end
	if npc_talk_index == "n1788_rubinat-4-j" then 
	end
	if npc_talk_index == "n1788_rubinat-4-k" then 
	end
	if npc_talk_index == "n1788_rubinat-4-l" then 
	end
	if npc_talk_index == "n1788_rubinat-4-m" then 
	end
	if npc_talk_index == "n1788_rubinat-4-n" then 
	end
	if npc_talk_index == "n1788_rubinat-4-o" then 
	end
	if npc_talk_index == "n1788_rubinat-4-p" then 
	end
	if npc_talk_index == "n1788_rubinat-4-q" then 
	end
	if npc_talk_index == "n1788_rubinat-4-r" then 
	end
	if npc_talk_index == "n1788_rubinat-4-s" then 
	end
	if npc_talk_index == "n1788_rubinat-4-t" then 
	end
	if npc_talk_index == "n1788_rubinat-4-u" then 
	end
	if npc_talk_index == "n1788_rubinat-4-v" then 
	end
	if npc_talk_index == "n1788_rubinat-5" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
	end
	if npc_talk_index == "n1788_rubinat-5-b" then 
	end
	if npc_talk_index == "n1788_rubinat-5-c" then 
	end
	if npc_talk_index == "n1788_rubinat-5-d" then 
	end
	if npc_talk_index == "n1788_rubinat-5-e" then 
	end
	if npc_talk_index == "n1788_rubinat-5-f" then 
	end
	if npc_talk_index == "n1788_rubinat-5-g" then 
	end
	if npc_talk_index == "n1788_rubinat-5-h" then 
	end
	if npc_talk_index == "n1788_rubinat-5-i" then 
	end
	if npc_talk_index == "n1788_rubinat-5-j" then 
	end
	if npc_talk_index == "n1788_rubinat-5-k" then 
	end
	if npc_talk_index == "n1788_rubinat-5-l" then 
	end
	if npc_talk_index == "n1788_rubinat-5-m" then 
	end
	if npc_talk_index == "n1788_rubinat-5-n" then 
	end
	if npc_talk_index == "n1788_rubinat-5-o" then 
	end
	if npc_talk_index == "n1788_rubinat-5-p" then 
	end
	if npc_talk_index == "n1788_rubinat-5-q" then 
	end
	if npc_talk_index == "n1788_rubinat-5-r" then 
	end
	if npc_talk_index == "n1788_rubinat-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc16_4653_red_and_silver_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4653);
end

function sqc16_4653_red_and_silver_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4653);
end

function sqc16_4653_red_and_silver_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4653);
	local questID=4653;
end

function sqc16_4653_red_and_silver_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc16_4653_red_and_silver_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc16_4653_red_and_silver_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4653, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4653, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4653, 1);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400475, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400475, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n1103_trader_phara-1";
end

</GameServer>