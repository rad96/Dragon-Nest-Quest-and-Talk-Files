<VillageServer>

function mq08_224_she_meets_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 24 then
		mq08_224_she_meets_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 27 then
		mq08_224_she_meets_OnTalk_n027_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 28 then
		mq08_224_she_meets_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_224_she_meets_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest(userObjID,224, 2);
				api_quest_SetJournalStep(userObjID,224, 1);
				api_quest_SetQuestStep(userObjID,224, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-g" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-h" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-f" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n027_argenta--------------------------------------------------------------------------------
function mq08_224_she_meets_OnTalk_n027_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n027_argenta-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n027_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n027_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n027_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n027_argenta-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n027_argenta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,224, 2);
				api_quest_SetJournalStep(userObjID,224, 1);
				api_quest_SetQuestStep(userObjID,224, 1);
				npc_talk_index = "n027_argenta-1";

	end
	if npc_talk_index == "n027_argenta-1-b" then 
	end
	if npc_talk_index == "n027_argenta-1-c" then 
	end
	if npc_talk_index == "n027_argenta-1-d" then 
	end
	if npc_talk_index == "n027_argenta-1-e" then 
	end
	if npc_talk_index == "n027_argenta-1-f" then 
	end
	if npc_talk_index == "n027_argenta-1-g" then 
	end
	if npc_talk_index == "n027_argenta-1-h" then 
	end
	if npc_talk_index == "n027_argenta-1-i" then 
	end
	if npc_talk_index == "n027_argenta-1-j" then 
	end
	if npc_talk_index == "n027_argenta-1-k" then 
	end
	if npc_talk_index == "n027_argenta-1-l" then 
	end
	if npc_talk_index == "n027_argenta-1-m" then 
	end
	if npc_talk_index == "n027_argenta-1-n" then 
	end
	if npc_talk_index == "n027_argenta-1-o" then 
	end
	if npc_talk_index == "n027_argenta-1-p" then 
	end
	if npc_talk_index == "n027_argenta-1-q" then 
	end
	if npc_talk_index == "n027_argenta-1-r" then 
	end
	if npc_talk_index == "n027_argenta-1-s" then 
	end
	if npc_talk_index == "n027_argenta-1-t" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n027_argenta-2-b" then 
	end
	if npc_talk_index == "n027_argenta-2-c" then 
	end
	if npc_talk_index == "n027_argenta-2-d" then 
	end
	if npc_talk_index == "n027_argenta-2-e" then 
	end
	if npc_talk_index == "n027_argenta-2-f" then 
	end
	if npc_talk_index == "n027_argenta-2-g" then 
	end
	if npc_talk_index == "n027_argenta-2-h" then 
	end
	if npc_talk_index == "n027_argenta-2-i" then 
	end
	if npc_talk_index == "n027_argenta-2-j" then 
	end
	if npc_talk_index == "n027_argenta-2-k" then 
	end
	if npc_talk_index == "n027_argenta-2-l" then 
	end
	if npc_talk_index == "n027_argenta-2-m" then 
	end
	if npc_talk_index == "n027_argenta-2-n" then 
	end
	if npc_talk_index == "n027_argenta-2-o" then 
	end
	if npc_talk_index == "n027_argenta-2-p" then 
	end
	if npc_talk_index == "n027_argenta-2-q" then 
	end
	if npc_talk_index == "n027_argenta-2-s" then 
	end
	if npc_talk_index == "n027_argenta-2-t" then 
	end
	if npc_talk_index == "n027_argenta-2-u" then 
	end
	if npc_talk_index == "n027_argenta-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n028_scholar_bailey--------------------------------------------------------------------------------
function mq08_224_she_meets_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n028_scholar_bailey-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-4-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-4-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2240, true);
				 api_quest_RewardQuestUser(userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2240, true);
				 api_quest_RewardQuestUser(userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2240, true);
				 api_quest_RewardQuestUser(userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2240, true);
				 api_quest_RewardQuestUser(userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2240, true);
				 api_quest_RewardQuestUser(userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2240, true);
				 api_quest_RewardQuestUser(userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2240, true);
				 api_quest_RewardQuestUser(userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2240, true);
				 api_quest_RewardQuestUser(userObjID, 2240, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 225, 2);
					api_quest_SetQuestStep(userObjID, 225, 1);
					api_quest_SetJournalStep(userObjID, 225, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n028_scholar_bailey-1", "mq08_225_get_back.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_224_she_meets_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 224);
end

function mq08_224_she_meets_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 224);
end

function mq08_224_she_meets_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 224);
	local questID=224;
end

function mq08_224_she_meets_OnRemoteStart( userObjID, questID )
				api_quest_SetJournalStep(userObjID, questID, 2);
end

function mq08_224_she_meets_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_224_she_meets_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,224, 2);
				api_quest_SetJournalStep(userObjID,224, 1);
				api_quest_SetQuestStep(userObjID,224, 1);
				npc_talk_index = "n027_argenta-1";
end

</VillageServer>

<GameServer>
function mq08_224_she_meets_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 24 then
		mq08_224_she_meets_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 27 then
		mq08_224_she_meets_OnTalk_n027_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 28 then
		mq08_224_she_meets_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function mq08_224_she_meets_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n024_adventurer_guildmaster_decud-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,224, 2);
				api_quest_SetJournalStep( pRoom, userObjID,224, 1);
				api_quest_SetQuestStep( pRoom, userObjID,224, 1);
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-f" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-g" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-1-h" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-c" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-d" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-e" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-3-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n027_argenta--------------------------------------------------------------------------------
function mq08_224_she_meets_OnTalk_n027_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n027_argenta-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n027_argenta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n027_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n027_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n027_argenta-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n027_argenta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,224, 2);
				api_quest_SetJournalStep( pRoom, userObjID,224, 1);
				api_quest_SetQuestStep( pRoom, userObjID,224, 1);
				npc_talk_index = "n027_argenta-1";

	end
	if npc_talk_index == "n027_argenta-1-b" then 
	end
	if npc_talk_index == "n027_argenta-1-c" then 
	end
	if npc_talk_index == "n027_argenta-1-d" then 
	end
	if npc_talk_index == "n027_argenta-1-e" then 
	end
	if npc_talk_index == "n027_argenta-1-f" then 
	end
	if npc_talk_index == "n027_argenta-1-g" then 
	end
	if npc_talk_index == "n027_argenta-1-h" then 
	end
	if npc_talk_index == "n027_argenta-1-i" then 
	end
	if npc_talk_index == "n027_argenta-1-j" then 
	end
	if npc_talk_index == "n027_argenta-1-k" then 
	end
	if npc_talk_index == "n027_argenta-1-l" then 
	end
	if npc_talk_index == "n027_argenta-1-m" then 
	end
	if npc_talk_index == "n027_argenta-1-n" then 
	end
	if npc_talk_index == "n027_argenta-1-o" then 
	end
	if npc_talk_index == "n027_argenta-1-p" then 
	end
	if npc_talk_index == "n027_argenta-1-q" then 
	end
	if npc_talk_index == "n027_argenta-1-r" then 
	end
	if npc_talk_index == "n027_argenta-1-s" then 
	end
	if npc_talk_index == "n027_argenta-1-t" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n027_argenta-2-b" then 
	end
	if npc_talk_index == "n027_argenta-2-c" then 
	end
	if npc_talk_index == "n027_argenta-2-d" then 
	end
	if npc_talk_index == "n027_argenta-2-e" then 
	end
	if npc_talk_index == "n027_argenta-2-f" then 
	end
	if npc_talk_index == "n027_argenta-2-g" then 
	end
	if npc_talk_index == "n027_argenta-2-h" then 
	end
	if npc_talk_index == "n027_argenta-2-i" then 
	end
	if npc_talk_index == "n027_argenta-2-j" then 
	end
	if npc_talk_index == "n027_argenta-2-k" then 
	end
	if npc_talk_index == "n027_argenta-2-l" then 
	end
	if npc_talk_index == "n027_argenta-2-m" then 
	end
	if npc_talk_index == "n027_argenta-2-n" then 
	end
	if npc_talk_index == "n027_argenta-2-o" then 
	end
	if npc_talk_index == "n027_argenta-2-p" then 
	end
	if npc_talk_index == "n027_argenta-2-q" then 
	end
	if npc_talk_index == "n027_argenta-2-s" then 
	end
	if npc_talk_index == "n027_argenta-2-t" then 
	end
	if npc_talk_index == "n027_argenta-2-u" then 
	end
	if npc_talk_index == "n027_argenta-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n028_scholar_bailey--------------------------------------------------------------------------------
function mq08_224_she_meets_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n028_scholar_bailey-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n028_scholar_bailey-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-4-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-4-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2240, questID, 1);
			 end 
	end
	if npc_talk_index == "n028_scholar_bailey-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 225, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 225, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 225, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n028_scholar_bailey-1", "mq08_225_get_back.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_224_she_meets_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 224);
end

function mq08_224_she_meets_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 224);
end

function mq08_224_she_meets_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 224);
	local questID=224;
end

function mq08_224_she_meets_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
end

function mq08_224_she_meets_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_224_she_meets_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,224, 2);
				api_quest_SetJournalStep( pRoom, userObjID,224, 1);
				api_quest_SetQuestStep( pRoom, userObjID,224, 1);
				npc_talk_index = "n027_argenta-1";
end

</GameServer>