<VillageServer>

function mqc15_9443_truth_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1578 then
		mqc15_9443_truth_OnTalk_n1578_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1570 then
		mqc15_9443_truth_OnTalk_n1570_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1585 then
		mqc15_9443_truth_OnTalk_n1585_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1558 then
		mqc15_9443_truth_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1571 then
		mqc15_9443_truth_OnTalk_n1571_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1578_for_memory--------------------------------------------------------------------------------
function mqc15_9443_truth_OnTalk_n1578_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1578_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1578_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1578_for_memory-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9443, 2);
				api_quest_SetJournalStep(userObjID,9443, 1);
				api_quest_SetQuestStep(userObjID,9443, 1);
				npc_talk_index = "n1578_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1570_meriendel--------------------------------------------------------------------------------
function mqc15_9443_truth_OnTalk_n1570_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1570_meriendel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1570_meriendel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1570_meriendel-1-b" then 
	end
	if npc_talk_index == "n1570_meriendel-1-c" then 
	end
	if npc_talk_index == "n1570_meriendel-1-d" then 
	end
	if npc_talk_index == "n1570_meriendel-1-e" then 
	end
	if npc_talk_index == "n1570_meriendel-1-f" then 
	end
	if npc_talk_index == "n1570_meriendel-1-g" then 
	end
	if npc_talk_index == "n1570_meriendel-1-h" then 
	end
	if npc_talk_index == "n1570_meriendel-1-i" then 
	end
	if npc_talk_index == "n1570_meriendel-1-j" then 
	end
	if npc_talk_index == "n1570_meriendel-1-k" then 
	end
	if npc_talk_index == "n1570_meriendel-1-l" then 
	end
	if npc_talk_index == "n1570_meriendel-1-m" then 
	end
	if npc_talk_index == "n1570_meriendel-1-n" then 
	end
	if npc_talk_index == "n1570_meriendel-1-o" then 
	end
	if npc_talk_index == "n1570_meriendel-1-p" then 
	end
	if npc_talk_index == "n1570_meriendel-1-q" then 
	end
	if npc_talk_index == "n1570_meriendel-1-r" then 
	end
	if npc_talk_index == "n1570_meriendel-1-s" then 
	end
	if npc_talk_index == "n1570_meriendel-1-t" then 
	end
	if npc_talk_index == "n1570_meriendel-1-u" then 
	end
	if npc_talk_index == "n1570_meriendel-1-u" then 
	end
	if npc_talk_index == "n1570_meriendel-1-u" then 
	end
	if npc_talk_index == "n1570_meriendel-1-v" then 
	end
	if npc_talk_index == "n1570_meriendel-1-w" then 
	end
	if npc_talk_index == "n1570_meriendel-1-x" then 
	end
	if npc_talk_index == "n1570_meriendel-1-y" then 
	end
	if npc_talk_index == "n1570_meriendel-1-z" then 
	end
	if npc_talk_index == "n1570_meriendel-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end
	if npc_talk_index == "n1570_meriendel-2-b" then 
	end
	if npc_talk_index == "n1570_meriendel-2-c" then 
	end
	if npc_talk_index == "n1570_meriendel-2-d" then 
	end
	if npc_talk_index == "n1570_meriendel-2-e" then 
	end
	if npc_talk_index == "n1570_meriendel-2-f" then 
	end
	if npc_talk_index == "n1570_meriendel-2-f" then 
	end
	if npc_talk_index == "n1570_meriendel-2-g" then 
	end
	if npc_talk_index == "n1570_meriendel-2-h" then 
	end
	if npc_talk_index == "n1570_meriendel-2-i" then 
	end
	if npc_talk_index == "n1570_meriendel-2-j" then 
	end
	if npc_talk_index == "n1570_meriendel-2-j" then 
	end
	if npc_talk_index == "n1570_meriendel-2-j" then 
	end
	if npc_talk_index == "n1570_meriendel-2-k" then 
	end
	if npc_talk_index == "n1570_meriendel-2-l" then 
	end
	if npc_talk_index == "n1570_meriendel-2-m" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1585_for_memory--------------------------------------------------------------------------------
function mqc15_9443_truth_OnTalk_n1585_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1585_for_memory-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1585_for_memory-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1585_for_memory-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1585_for_memory-3-b" then 
	end
	if npc_talk_index == "n1585_for_memory-3-c" then 
	end
	if npc_talk_index == "n1585_for_memory-3-class_check_0" then 
				if api_user_GetUserClassID(userObjID) == 1 then
									npc_talk_index = "n1585_for_memory-3-d";

				else
									if api_user_GetUserClassID(userObjID) == 2 then
									npc_talk_index = "n1585_for_memory-3-f";

				else
									if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1585_for_memory-3-h";

				else
									if api_user_GetUserClassID(userObjID) == 4 then
									npc_talk_index = "n1585_for_memory-3-j";

				else
									if api_user_GetUserClassID(userObjID) == 5 then
									npc_talk_index = "n1585_for_memory-3-l";

				else
									if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n1585_for_memory-3-n";

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									npc_talk_index = "n1585_for_memory-3-p";

				else
									npc_talk_index = "n1585_for_memory-3-r";

				end

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n1585_for_memory-3-e" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-g" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-i" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-k" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-m" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-o" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-q" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9443_truth_OnTalk_n1558_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1558_gaharam-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94430, true);
				 api_quest_RewardQuestUser(userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94430, true);
				 api_quest_RewardQuestUser(userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94430, true);
				 api_quest_RewardQuestUser(userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94430, true);
				 api_quest_RewardQuestUser(userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94430, true);
				 api_quest_RewardQuestUser(userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94430, true);
				 api_quest_RewardQuestUser(userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94430, true);
				 api_quest_RewardQuestUser(userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94430, true);
				 api_quest_RewardQuestUser(userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94430, true);
				 api_quest_RewardQuestUser(userObjID, 94430, questID, 1);
			 end 
	end
	if npc_talk_index == "n1558_gaharam-4-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9444, 2);
					api_quest_SetQuestStep(userObjID, 9444, 1);
					api_quest_SetJournalStep(userObjID, 9444, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1558_gaharam-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1558_gaharam-1", "mqc15_9444_thrid_desperation.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1571_meriendel--------------------------------------------------------------------------------
function mqc15_9443_truth_OnTalk_n1571_meriendel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1571_meriendel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9443_truth_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9443);
end

function mqc15_9443_truth_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9443);
end

function mqc15_9443_truth_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9443);
	local questID=9443;
end

function mqc15_9443_truth_OnRemoteStart( userObjID, questID )
end

function mqc15_9443_truth_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc15_9443_truth_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9443, 2);
				api_quest_SetJournalStep(userObjID,9443, 1);
				api_quest_SetQuestStep(userObjID,9443, 1);
				npc_talk_index = "n1578_for_memory-1";
end

</VillageServer>

<GameServer>
function mqc15_9443_truth_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1578 then
		mqc15_9443_truth_OnTalk_n1578_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1570 then
		mqc15_9443_truth_OnTalk_n1570_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1585 then
		mqc15_9443_truth_OnTalk_n1585_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1558 then
		mqc15_9443_truth_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1571 then
		mqc15_9443_truth_OnTalk_n1571_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1578_for_memory--------------------------------------------------------------------------------
function mqc15_9443_truth_OnTalk_n1578_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1578_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1578_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1578_for_memory-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9443, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9443, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9443, 1);
				npc_talk_index = "n1578_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1570_meriendel--------------------------------------------------------------------------------
function mqc15_9443_truth_OnTalk_n1570_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1570_meriendel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1570_meriendel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1570_meriendel-1-b" then 
	end
	if npc_talk_index == "n1570_meriendel-1-c" then 
	end
	if npc_talk_index == "n1570_meriendel-1-d" then 
	end
	if npc_talk_index == "n1570_meriendel-1-e" then 
	end
	if npc_talk_index == "n1570_meriendel-1-f" then 
	end
	if npc_talk_index == "n1570_meriendel-1-g" then 
	end
	if npc_talk_index == "n1570_meriendel-1-h" then 
	end
	if npc_talk_index == "n1570_meriendel-1-i" then 
	end
	if npc_talk_index == "n1570_meriendel-1-j" then 
	end
	if npc_talk_index == "n1570_meriendel-1-k" then 
	end
	if npc_talk_index == "n1570_meriendel-1-l" then 
	end
	if npc_talk_index == "n1570_meriendel-1-m" then 
	end
	if npc_talk_index == "n1570_meriendel-1-n" then 
	end
	if npc_talk_index == "n1570_meriendel-1-o" then 
	end
	if npc_talk_index == "n1570_meriendel-1-p" then 
	end
	if npc_talk_index == "n1570_meriendel-1-q" then 
	end
	if npc_talk_index == "n1570_meriendel-1-r" then 
	end
	if npc_talk_index == "n1570_meriendel-1-s" then 
	end
	if npc_talk_index == "n1570_meriendel-1-t" then 
	end
	if npc_talk_index == "n1570_meriendel-1-u" then 
	end
	if npc_talk_index == "n1570_meriendel-1-u" then 
	end
	if npc_talk_index == "n1570_meriendel-1-u" then 
	end
	if npc_talk_index == "n1570_meriendel-1-v" then 
	end
	if npc_talk_index == "n1570_meriendel-1-w" then 
	end
	if npc_talk_index == "n1570_meriendel-1-x" then 
	end
	if npc_talk_index == "n1570_meriendel-1-y" then 
	end
	if npc_talk_index == "n1570_meriendel-1-z" then 
	end
	if npc_talk_index == "n1570_meriendel-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end
	if npc_talk_index == "n1570_meriendel-2-b" then 
	end
	if npc_talk_index == "n1570_meriendel-2-c" then 
	end
	if npc_talk_index == "n1570_meriendel-2-d" then 
	end
	if npc_talk_index == "n1570_meriendel-2-e" then 
	end
	if npc_talk_index == "n1570_meriendel-2-f" then 
	end
	if npc_talk_index == "n1570_meriendel-2-f" then 
	end
	if npc_talk_index == "n1570_meriendel-2-g" then 
	end
	if npc_talk_index == "n1570_meriendel-2-h" then 
	end
	if npc_talk_index == "n1570_meriendel-2-i" then 
	end
	if npc_talk_index == "n1570_meriendel-2-j" then 
	end
	if npc_talk_index == "n1570_meriendel-2-j" then 
	end
	if npc_talk_index == "n1570_meriendel-2-j" then 
	end
	if npc_talk_index == "n1570_meriendel-2-k" then 
	end
	if npc_talk_index == "n1570_meriendel-2-l" then 
	end
	if npc_talk_index == "n1570_meriendel-2-m" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1585_for_memory--------------------------------------------------------------------------------
function mqc15_9443_truth_OnTalk_n1585_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1585_for_memory-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1585_for_memory-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1585_for_memory-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1585_for_memory-3-b" then 
	end
	if npc_talk_index == "n1585_for_memory-3-c" then 
	end
	if npc_talk_index == "n1585_for_memory-3-class_check_0" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									npc_talk_index = "n1585_for_memory-3-d";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									npc_talk_index = "n1585_for_memory-3-f";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1585_for_memory-3-h";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									npc_talk_index = "n1585_for_memory-3-j";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									npc_talk_index = "n1585_for_memory-3-l";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n1585_for_memory-3-n";

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									npc_talk_index = "n1585_for_memory-3-p";

				else
									npc_talk_index = "n1585_for_memory-3-r";

				end

				end

				end

				end

				end

				end

				end
	end
	if npc_talk_index == "n1585_for_memory-3-e" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-g" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-i" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-k" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-m" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-o" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-3-q" then 
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n1585_for_memory-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1558_gaharam--------------------------------------------------------------------------------
function mqc15_9443_truth_OnTalk_n1558_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1558_gaharam-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1558_gaharam-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94430, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94430, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94430, questID, 1);
			 end 
	end
	if npc_talk_index == "n1558_gaharam-4-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9444, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9444, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9444, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1558_gaharam-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1558_gaharam-1", "mqc15_9444_thrid_desperation.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1571_meriendel--------------------------------------------------------------------------------
function mqc15_9443_truth_OnTalk_n1571_meriendel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1571_meriendel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc15_9443_truth_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9443);
end

function mqc15_9443_truth_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9443);
end

function mqc15_9443_truth_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9443);
	local questID=9443;
end

function mqc15_9443_truth_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc15_9443_truth_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc15_9443_truth_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9443, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9443, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9443, 1);
				npc_talk_index = "n1578_for_memory-1";
end

</GameServer>