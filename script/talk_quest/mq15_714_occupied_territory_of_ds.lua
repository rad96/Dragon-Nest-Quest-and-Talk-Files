<VillageServer>

function mq15_714_occupied_territory_of_ds_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 493 then
		mq15_714_occupied_territory_of_ds_OnTalk_n493_charty_grandfather(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 709 then
		mq15_714_occupied_territory_of_ds_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 725 then
		mq15_714_occupied_territory_of_ds_OnTalk_n725_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n493_charty_grandfather--------------------------------------------------------------------------------
function mq15_714_occupied_territory_of_ds_OnTalk_n493_charty_grandfather(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n493_charty_grandfather-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n493_charty_grandfather-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n493_charty_grandfather-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n493_charty_grandfather-3-b" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-c" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-d" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-e" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-f" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-g" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-h" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-i" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-j" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7141, true);
				 api_quest_RewardQuestUser(userObjID, 7141, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7142, true);
				 api_quest_RewardQuestUser(userObjID, 7142, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7143, true);
				 api_quest_RewardQuestUser(userObjID, 7143, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7144, true);
				 api_quest_RewardQuestUser(userObjID, 7144, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7145, true);
				 api_quest_RewardQuestUser(userObjID, 7145, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7146, true);
				 api_quest_RewardQuestUser(userObjID, 7146, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7141, true);
				 api_quest_RewardQuestUser(userObjID, 7141, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7141, true);
				 api_quest_RewardQuestUser(userObjID, 7141, questID, 1);
			 end 
	end
	if npc_talk_index == "n493_charty_grandfather-3-l" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 715, 2);
					api_quest_SetQuestStep(userObjID, 715, 1);
					api_quest_SetJournalStep(userObjID, 715, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n709_trader_lucita--------------------------------------------------------------------------------
function mq15_714_occupied_territory_of_ds_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n709_trader_lucita-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n709_trader_lucita-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n709_trader_lucita-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n709_trader_lucita-accepting-acceptted" then
				api_quest_AddQuest(userObjID,714, 2);
				api_quest_SetJournalStep(userObjID,714, 1);
				api_quest_SetQuestStep(userObjID,714, 1);
				npc_talk_index = "n709_trader_lucita-1";

	end
	if npc_talk_index == "n709_trader_lucita-1-b" then 
	end
	if npc_talk_index == "n709_trader_lucita-1-c" then 
	end
	if npc_talk_index == "n709_trader_lucita-1-d" then 
	end
	if npc_talk_index == "n709_trader_lucita-1-e" then 
	end
	if npc_talk_index == "n709_trader_lucita-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300386, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300386, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n725_charty--------------------------------------------------------------------------------
function mq15_714_occupied_territory_of_ds_OnTalk_n725_charty(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n725_charty-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n725_charty-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n725_charty-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n725_charty-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n725_charty-2-b" then 
	end
	if npc_talk_index == "n725_charty-2-c" then 
	end
	if npc_talk_index == "n725_charty-2-d" then 
	end
	if npc_talk_index == "n725_charty-2-e" then 
	end
	if npc_talk_index == "n725_charty-2-f" then 
	end
	if npc_talk_index == "n725_charty-2-g" then 
	end
	if npc_talk_index == "n725_charty-2-h" then 
	end
	if npc_talk_index == "n725_charty-2-i" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				if api_quest_HasQuestItem(userObjID, 300386, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300386, api_quest_HasQuestItem(userObjID, 300386, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_714_occupied_territory_of_ds_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 714);
end

function mq15_714_occupied_territory_of_ds_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 714);
end

function mq15_714_occupied_territory_of_ds_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 714);
	local questID=714;
end

function mq15_714_occupied_territory_of_ds_OnRemoteStart( userObjID, questID )
end

function mq15_714_occupied_territory_of_ds_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_714_occupied_territory_of_ds_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,714, 2);
				api_quest_SetJournalStep(userObjID,714, 1);
				api_quest_SetQuestStep(userObjID,714, 1);
				npc_talk_index = "n709_trader_lucita-1";
end

</VillageServer>

<GameServer>
function mq15_714_occupied_territory_of_ds_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 493 then
		mq15_714_occupied_territory_of_ds_OnTalk_n493_charty_grandfather( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 709 then
		mq15_714_occupied_territory_of_ds_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 725 then
		mq15_714_occupied_territory_of_ds_OnTalk_n725_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n493_charty_grandfather--------------------------------------------------------------------------------
function mq15_714_occupied_territory_of_ds_OnTalk_n493_charty_grandfather( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n493_charty_grandfather-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n493_charty_grandfather-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n493_charty_grandfather-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n493_charty_grandfather-3-b" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-c" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-d" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-e" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-f" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-g" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-h" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-i" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-j" then 
	end
	if npc_talk_index == "n493_charty_grandfather-3-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7141, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7141, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7142, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7142, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7143, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7143, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7144, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7144, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7145, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7145, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7146, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7146, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7141, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7141, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7141, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7141, questID, 1);
			 end 
	end
	if npc_talk_index == "n493_charty_grandfather-3-l" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 715, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 715, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 715, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n709_trader_lucita--------------------------------------------------------------------------------
function mq15_714_occupied_territory_of_ds_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n709_trader_lucita-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n709_trader_lucita-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n709_trader_lucita-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n709_trader_lucita-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,714, 2);
				api_quest_SetJournalStep( pRoom, userObjID,714, 1);
				api_quest_SetQuestStep( pRoom, userObjID,714, 1);
				npc_talk_index = "n709_trader_lucita-1";

	end
	if npc_talk_index == "n709_trader_lucita-1-b" then 
	end
	if npc_talk_index == "n709_trader_lucita-1-c" then 
	end
	if npc_talk_index == "n709_trader_lucita-1-d" then 
	end
	if npc_talk_index == "n709_trader_lucita-1-e" then 
	end
	if npc_talk_index == "n709_trader_lucita-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300386, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300386, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n725_charty--------------------------------------------------------------------------------
function mq15_714_occupied_territory_of_ds_OnTalk_n725_charty( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n725_charty-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n725_charty-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n725_charty-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n725_charty-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n725_charty-2-b" then 
	end
	if npc_talk_index == "n725_charty-2-c" then 
	end
	if npc_talk_index == "n725_charty-2-d" then 
	end
	if npc_talk_index == "n725_charty-2-e" then 
	end
	if npc_talk_index == "n725_charty-2-f" then 
	end
	if npc_talk_index == "n725_charty-2-g" then 
	end
	if npc_talk_index == "n725_charty-2-h" then 
	end
	if npc_talk_index == "n725_charty-2-i" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				if api_quest_HasQuestItem( pRoom, userObjID, 300386, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300386, api_quest_HasQuestItem( pRoom, userObjID, 300386, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_714_occupied_territory_of_ds_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 714);
end

function mq15_714_occupied_territory_of_ds_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 714);
end

function mq15_714_occupied_territory_of_ds_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 714);
	local questID=714;
end

function mq15_714_occupied_territory_of_ds_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_714_occupied_territory_of_ds_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_714_occupied_territory_of_ds_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,714, 2);
				api_quest_SetJournalStep( pRoom, userObjID,714, 1);
				api_quest_SetQuestStep( pRoom, userObjID,714, 1);
				npc_talk_index = "n709_trader_lucita-1";
end

</GameServer>