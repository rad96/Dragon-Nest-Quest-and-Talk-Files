<VillageServer>

function sq_2127_jumping_event_8_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 101 then
		sq_2127_jumping_event_8_OnTalk_n101_event_tracey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n101_event_tracey--------------------------------------------------------------------------------
function sq_2127_jumping_event_8_OnTalk_n101_event_tracey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
					 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21271, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21272, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21273, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21274, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21275, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21276, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21267, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21268, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21270, false);
			 end 

			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n101_event_tracey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n101_event_tracey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n101_event_tracey-2";
				if api_user_IsMissionGained(userObjID, 25) == 1  then
									npc_talk_index = "n101_event_tracey-2-a";

				else
									if api_user_IsMissionGained(userObjID, 13) == 1  then
									npc_talk_index = "n101_event_tracey-2-a";

				else
									npc_talk_index = "n101_event_tracey-2";

				end

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n101_event_tracey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2127, 1);
				api_quest_SetJournalStep(userObjID,2127, 1);
				api_quest_SetQuestStep(userObjID,2127, 1);
				npc_talk_index = "n101_event_tracey-1";

	end
	if npc_talk_index == "n101_event_tracey-1-b" then 
	end
	if npc_talk_index == "n101_event_tracey-1-c" then 
	end
	if npc_talk_index == "n101_event_tracey-1-d" then 
	end
	if npc_talk_index == "n101_event_tracey-1-get_item" then 
				if api_user_HasItem(userObjID, 10003, 0) > 0 then
									npc_talk_index = "n101_event_tracey-2";
				api_quest_SetQuestStep(userObjID, questID,2);

				else
									if api_user_GetUserInvenBlankCount(userObjID) >= 1 then
									if api_user_CheckInvenForAddItem(userObjID, 10003, 1) == 1 then
					api_user_AddItem(userObjID, 10003, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n101_event_tracey-2";
				api_quest_SetQuestStep(userObjID, questID,2);

				else
									npc_talk_index = "n101_event_tracey-1-e";

				end

				end
	end
	if npc_talk_index == "n101_event_tracey-2-itemupgrade" then 
				api_ui_OpenUpgradeItem(userObjID);
	end
	if npc_talk_index == "n101_event_tracey-2-b" then 
	end
	if npc_talk_index == "n101_event_tracey-2-c" then 
	end
	if npc_talk_index == "n101_event_tracey-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 21271, true);
				 api_quest_RewardQuestUser(userObjID, 21271, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 21272, true);
				 api_quest_RewardQuestUser(userObjID, 21272, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 21273, true);
				 api_quest_RewardQuestUser(userObjID, 21273, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 21274, true);
				 api_quest_RewardQuestUser(userObjID, 21274, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 21275, true);
				 api_quest_RewardQuestUser(userObjID, 21275, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 21276, true);
				 api_quest_RewardQuestUser(userObjID, 21276, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 21267, true);
				 api_quest_RewardQuestUser(userObjID, 21267, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 21268, true);
				 api_quest_RewardQuestUser(userObjID, 21268, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 21270, true);
				 api_quest_RewardQuestUser(userObjID, 21270, questID, 1);
			 end 
	end
	if npc_talk_index == "n101_event_tracey-2-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 2128, 1);
					api_quest_SetQuestStep(userObjID, 2128, 1);
					api_quest_SetJournalStep(userObjID, 2128, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2127_jumping_event_8_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2127);
end

function sq_2127_jumping_event_8_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2127);
end

function sq_2127_jumping_event_8_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2127);
	local questID=2127;
end

function sq_2127_jumping_event_8_OnRemoteStart( userObjID, questID )
end

function sq_2127_jumping_event_8_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_2127_jumping_event_8_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2127, 1);
				api_quest_SetJournalStep(userObjID,2127, 1);
				api_quest_SetQuestStep(userObjID,2127, 1);
				npc_talk_index = "n101_event_tracey-1";
end

</VillageServer>

<GameServer>
function sq_2127_jumping_event_8_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 101 then
		sq_2127_jumping_event_8_OnTalk_n101_event_tracey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n101_event_tracey--------------------------------------------------------------------------------
function sq_2127_jumping_event_8_OnTalk_n101_event_tracey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
					 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21271, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21272, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21273, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21274, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21275, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21276, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21267, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21268, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21270, false);
			 end 

			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n101_event_tracey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n101_event_tracey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n101_event_tracey-2";
				if api_user_IsMissionGained( pRoom, userObjID, 25) == 1  then
									npc_talk_index = "n101_event_tracey-2-a";

				else
									if api_user_IsMissionGained( pRoom, userObjID, 13) == 1  then
									npc_talk_index = "n101_event_tracey-2-a";

				else
									npc_talk_index = "n101_event_tracey-2";

				end

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n101_event_tracey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2127, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2127, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2127, 1);
				npc_talk_index = "n101_event_tracey-1";

	end
	if npc_talk_index == "n101_event_tracey-1-b" then 
	end
	if npc_talk_index == "n101_event_tracey-1-c" then 
	end
	if npc_talk_index == "n101_event_tracey-1-d" then 
	end
	if npc_talk_index == "n101_event_tracey-1-get_item" then 
				if api_user_HasItem( pRoom, userObjID, 10003, 0) > 0 then
									npc_talk_index = "n101_event_tracey-2";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);

				else
									if api_user_GetUserInvenBlankCount( pRoom, userObjID) >= 1 then
									if api_user_CheckInvenForAddItem( pRoom, userObjID, 10003, 1) == 1 then
					api_user_AddItem( pRoom, userObjID, 10003, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				npc_talk_index = "n101_event_tracey-2";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);

				else
									npc_talk_index = "n101_event_tracey-1-e";

				end

				end
	end
	if npc_talk_index == "n101_event_tracey-2-itemupgrade" then 
				api_ui_OpenUpgradeItem( pRoom, userObjID);
	end
	if npc_talk_index == "n101_event_tracey-2-b" then 
	end
	if npc_talk_index == "n101_event_tracey-2-c" then 
	end
	if npc_talk_index == "n101_event_tracey-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21271, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21271, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21272, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21272, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21273, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21273, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21274, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21274, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21275, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21275, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21276, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21276, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21267, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21267, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21268, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21268, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 21270, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 21270, questID, 1);
			 end 
	end
	if npc_talk_index == "n101_event_tracey-2-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 2128, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 2128, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 2128, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_2127_jumping_event_8_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2127);
end

function sq_2127_jumping_event_8_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2127);
end

function sq_2127_jumping_event_8_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2127);
	local questID=2127;
end

function sq_2127_jumping_event_8_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_2127_jumping_event_8_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_2127_jumping_event_8_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2127, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2127, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2127, 1);
				npc_talk_index = "n101_event_tracey-1";
end

</GameServer>