<VillageServer>

function sq11_611_warning_again_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 45 then
		sq11_611_warning_again_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_611_warning_again_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-checkq" then
				if api_quest_IsMarkingCompleteQuest(userObjID, 610 ) == 1  then
									npc_talk_index = "n045_soceress_master_stella-accepting-a";

				else
									npc_talk_index = "n045_soceress_master_stella-accepting-g";

				end

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6110, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6110, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6110, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6110, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6110, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6110, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6110, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6110, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6110, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceppted" then
				api_quest_AddQuest(userObjID,611, 1);
				api_quest_SetJournalStep(userObjID,611, 1);
				api_quest_SetQuestStep(userObjID,611, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-have" then
				if api_user_HasItem(userObjID, 337838339, 1) >= 1  then
									npc_talk_index = "n045_soceress_master_stella-accepting-g";

				else
									npc_talk_index = "n045_soceress_master_stella-accepting-h";

				end

	end
	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_611_warning_again_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 611);
end

function sq11_611_warning_again_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 611);
end

function sq11_611_warning_again_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 611);
	local questID=611;
end

function sq11_611_warning_again_OnRemoteStart( userObjID, questID )
end

function sq11_611_warning_again_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_611_warning_again_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_611_warning_again_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 45 then
		sq11_611_warning_again_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_611_warning_again_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-accepting-checkq" then
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 610 ) == 1  then
									npc_talk_index = "n045_soceress_master_stella-accepting-a";

				else
									npc_talk_index = "n045_soceress_master_stella-accepting-g";

				end

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6110, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6110, false);
			 end 

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,611, 1);
				api_quest_SetJournalStep( pRoom, userObjID,611, 1);
				api_quest_SetQuestStep( pRoom, userObjID,611, 1);
				npc_talk_index = "n045_soceress_master_stella-1";

	end
	if npc_talk_index == "n045_soceress_master_stella-accepting-have" then
				if api_user_HasItem( pRoom, userObjID, 337838339, 1) >= 1  then
									npc_talk_index = "n045_soceress_master_stella-accepting-g";

				else
									npc_talk_index = "n045_soceress_master_stella-accepting-h";

				end

	end
	if npc_talk_index == "n045_soceress_master_stella-1-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-1-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_611_warning_again_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 611);
end

function sq11_611_warning_again_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 611);
end

function sq11_611_warning_again_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 611);
	local questID=611;
end

function sq11_611_warning_again_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_611_warning_again_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_611_warning_again_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>