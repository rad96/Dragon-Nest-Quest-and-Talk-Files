<VillageServer>

function sq08_268_fame_of_david_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1267 then
		sq08_268_fame_of_david_OnTalk_n1267_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 164 then
		sq08_268_fame_of_david_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 202 then
		sq08_268_fame_of_david_OnTalk_n202_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 23 then
		sq08_268_fame_of_david_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		sq08_268_fame_of_david_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 26 then
		sq08_268_fame_of_david_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		sq08_268_fame_of_david_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1267_ranger_fugus--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n1267_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1267_ranger_fugus-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1267_ranger_fugus-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1267_ranger_fugus-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1267_ranger_fugus-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1267_ranger_fugus-6-b" then 
	end
	if npc_talk_index == "n1267_ranger_fugus-6-c" then 
	end
	if npc_talk_index == "n1267_ranger_fugus-6-d" then 
				api_quest_SetQuestStep(userObjID, questID,7);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end
	if npc_talk_index == "n1267_ranger_fugus-9-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n164_argenta_wounded(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n164_argenta_wounded-10";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-g" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2681, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2682, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2683, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2684, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2685, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2686, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2687, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2688, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2689, false);
			 end 

	end
	if npc_talk_index == "n164_argenta_wounded-accepting-acceptted" then
				api_quest_AddQuest(userObjID,268, 1);
				api_quest_SetJournalStep(userObjID,268, 1);
				api_quest_SetQuestStep(userObjID,268, 1);
				npc_talk_index = "n164_argenta_wounded-1";

	end
	if npc_talk_index == "n164_argenta_wounded-10-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-10-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2681, true);
				 api_quest_RewardQuestUser(userObjID, 2681, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2682, true);
				 api_quest_RewardQuestUser(userObjID, 2682, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2683, true);
				 api_quest_RewardQuestUser(userObjID, 2683, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2684, true);
				 api_quest_RewardQuestUser(userObjID, 2684, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2685, true);
				 api_quest_RewardQuestUser(userObjID, 2685, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2686, true);
				 api_quest_RewardQuestUser(userObjID, 2686, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2687, true);
				 api_quest_RewardQuestUser(userObjID, 2687, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2688, true);
				 api_quest_RewardQuestUser(userObjID, 2688, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2689, true);
				 api_quest_RewardQuestUser(userObjID, 2689, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-10-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n202_slave_david--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n202_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n202_slave_david-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n202_slave_david-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n202_slave_david-10";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n202_slave_david-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n202_slave_david-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n202_slave_david-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n202_slave_david-7";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n202_slave_david-8";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n202_slave_david-9";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n202_slave_david-1-b" then 
	end
	if npc_talk_index == "n202_slave_david-1-c" then 
	end
	if npc_talk_index == "n202_slave_david-1-d" then 
	end
	if npc_talk_index == "n202_slave_david-1-e" then 
	end
	if npc_talk_index == "n202_slave_david-1-f" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n202_slave_david-5-b" then 
	end
	if npc_talk_index == "n202_slave_david-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end
	if npc_talk_index == "n202_slave_david-7-b" then 
	end
	if npc_talk_index == "n202_slave_david-7-c" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 712, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200712, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 21014, 30000);
				api_quest_SetQuestStep(userObjID, questID,8);
				api_quest_SetJournalStep(userObjID, questID, 8);
	end
	if npc_talk_index == "n202_slave_david-9-b" then 
	end
	if npc_talk_index == "n202_slave_david-9-c" then 
	end
	if npc_talk_index == "n202_slave_david-9-d" then 
				api_quest_SetQuestStep(userObjID, questID,10);
				api_quest_SetJournalStep(userObjID, questID, 10);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n023_ranger_fugus--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n023_ranger_fugus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n023_ranger_fugus-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n024_adventurer_guildmaster_decud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n024_adventurer_guildmaster_decud-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-c" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n026_trader_may--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n026_trader_may-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n026_trader_may-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n026_trader_may-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-3-b" then 
	end
	if npc_talk_index == "n026_trader_may-3-c" then 
	end
	if npc_talk_index == "n026_trader_may-3-d" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n035_soceress_master_tiana-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n035_soceress_master_tiana-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n035_soceress_master_tiana-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-4-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-e" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_268_fame_of_david_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 268);
	if qstep == 8 and CountIndex == 712 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,9);
				api_quest_SetJournalStep(userObjID, questID, 9);

	end
	if qstep == 8 and CountIndex == 200712 then
				api_quest_IncCounting(userObjID, 2, 712);

	end
	if qstep == 8 and CountIndex == 21014 then
				api_quest_IncCounting(userObjID, 2, 712);

	end
end

function sq08_268_fame_of_david_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 268);
	if qstep == 8 and CountIndex == 712 and Count >= TargetCount  then

	end
	if qstep == 8 and CountIndex == 200712 and Count >= TargetCount  then

	end
	if qstep == 8 and CountIndex == 21014 and Count >= TargetCount  then

	end
end

function sq08_268_fame_of_david_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 268);
	local questID=268;
end

function sq08_268_fame_of_david_OnRemoteStart( userObjID, questID )
end

function sq08_268_fame_of_david_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_268_fame_of_david_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,268, 1);
				api_quest_SetJournalStep(userObjID,268, 1);
				api_quest_SetQuestStep(userObjID,268, 1);
				npc_talk_index = "n164_argenta_wounded-1";
end

</VillageServer>

<GameServer>
function sq08_268_fame_of_david_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1267 then
		sq08_268_fame_of_david_OnTalk_n1267_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 164 then
		sq08_268_fame_of_david_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 202 then
		sq08_268_fame_of_david_OnTalk_n202_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 23 then
		sq08_268_fame_of_david_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 24 then
		sq08_268_fame_of_david_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 26 then
		sq08_268_fame_of_david_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		sq08_268_fame_of_david_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1267_ranger_fugus--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n1267_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1267_ranger_fugus-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1267_ranger_fugus-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n1267_ranger_fugus-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n1267_ranger_fugus-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1267_ranger_fugus-6-b" then 
	end
	if npc_talk_index == "n1267_ranger_fugus-6-c" then 
	end
	if npc_talk_index == "n1267_ranger_fugus-6-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,7);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end
	if npc_talk_index == "n1267_ranger_fugus-9-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n164_argenta_wounded--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n164_argenta_wounded( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n164_argenta_wounded-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n164_argenta_wounded-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n164_argenta_wounded-10";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n164_argenta_wounded-accepting-g" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2681, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2682, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2683, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2684, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2685, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2686, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2687, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2688, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2689, false);
			 end 

	end
	if npc_talk_index == "n164_argenta_wounded-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,268, 1);
				api_quest_SetJournalStep( pRoom, userObjID,268, 1);
				api_quest_SetQuestStep( pRoom, userObjID,268, 1);
				npc_talk_index = "n164_argenta_wounded-1";

	end
	if npc_talk_index == "n164_argenta_wounded-10-b" then 
	end
	if npc_talk_index == "n164_argenta_wounded-10-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2681, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2681, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2682, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2682, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2683, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2683, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2684, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2684, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2685, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2685, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2686, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2686, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2687, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2687, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2688, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2688, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2689, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2689, questID, 1);
			 end 
	end
	if npc_talk_index == "n164_argenta_wounded-10-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n202_slave_david--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n202_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n202_slave_david-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n202_slave_david-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 10 then
				npc_talk_index = "n202_slave_david-10";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n202_slave_david-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n202_slave_david-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n202_slave_david-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 7 then
				npc_talk_index = "n202_slave_david-7";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 8 then
				npc_talk_index = "n202_slave_david-8";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 9 then
				npc_talk_index = "n202_slave_david-9";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n202_slave_david-1-b" then 
	end
	if npc_talk_index == "n202_slave_david-1-c" then 
	end
	if npc_talk_index == "n202_slave_david-1-d" then 
	end
	if npc_talk_index == "n202_slave_david-1-e" then 
	end
	if npc_talk_index == "n202_slave_david-1-f" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n202_slave_david-5-b" then 
	end
	if npc_talk_index == "n202_slave_david-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end
	if npc_talk_index == "n202_slave_david-7-b" then 
	end
	if npc_talk_index == "n202_slave_david-7-c" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 712, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200712, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 21014, 30000);
				api_quest_SetQuestStep( pRoom, userObjID, questID,8);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);
	end
	if npc_talk_index == "n202_slave_david-9-b" then 
	end
	if npc_talk_index == "n202_slave_david-9-c" then 
	end
	if npc_talk_index == "n202_slave_david-9-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,10);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 10);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n023_ranger_fugus--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n023_ranger_fugus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n023_ranger_fugus-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n024_adventurer_guildmaster_decud--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n024_adventurer_guildmaster_decud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n024_adventurer_guildmaster_decud-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n024_adventurer_guildmaster_decud-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-b" then 
	end
	if npc_talk_index == "n024_adventurer_guildmaster_decud-2-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n026_trader_may--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n026_trader_may-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n026_trader_may-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n026_trader_may-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-3-b" then 
	end
	if npc_talk_index == "n026_trader_may-3-c" then 
	end
	if npc_talk_index == "n026_trader_may-3-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_268_fame_of_david_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n035_soceress_master_tiana-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n035_soceress_master_tiana-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n035_soceress_master_tiana-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-4-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_268_fame_of_david_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 268);
	if qstep == 8 and CountIndex == 712 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,9);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 9);

	end
	if qstep == 8 and CountIndex == 200712 then
				api_quest_IncCounting( pRoom, userObjID, 2, 712);

	end
	if qstep == 8 and CountIndex == 21014 then
				api_quest_IncCounting( pRoom, userObjID, 2, 712);

	end
end

function sq08_268_fame_of_david_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 268);
	if qstep == 8 and CountIndex == 712 and Count >= TargetCount  then

	end
	if qstep == 8 and CountIndex == 200712 and Count >= TargetCount  then

	end
	if qstep == 8 and CountIndex == 21014 and Count >= TargetCount  then

	end
end

function sq08_268_fame_of_david_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 268);
	local questID=268;
end

function sq08_268_fame_of_david_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_268_fame_of_david_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_268_fame_of_david_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,268, 1);
				api_quest_SetJournalStep( pRoom, userObjID,268, 1);
				api_quest_SetQuestStep( pRoom, userObjID,268, 1);
				npc_talk_index = "n164_argenta_wounded-1";
end

</GameServer>