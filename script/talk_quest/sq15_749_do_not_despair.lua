<VillageServer>

function sq15_749_do_not_despair_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 394 then
		sq15_749_do_not_despair_OnTalk_n394_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 799 then
		sq15_749_do_not_despair_OnTalk_n799_academin_npc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n394_academic_station--------------------------------------------------------------------------------
function sq15_749_do_not_despair_OnTalk_n394_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n394_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n394_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n394_academic_station-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n394_academic_station-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n394_academic_station-accepting-b" then
				api_quest_AddQuest(userObjID,749, 1);
				api_quest_SetJournalStep(userObjID,749, 1);
				api_quest_SetQuestStep(userObjID,749, 1);

	end
	if npc_talk_index == "n394_academic_station-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7490, true);
				 api_quest_RewardQuestUser(userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7490, true);
				 api_quest_RewardQuestUser(userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7490, true);
				 api_quest_RewardQuestUser(userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7490, true);
				 api_quest_RewardQuestUser(userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7490, true);
				 api_quest_RewardQuestUser(userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7490, true);
				 api_quest_RewardQuestUser(userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7490, true);
				 api_quest_RewardQuestUser(userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7490, true);
				 api_quest_RewardQuestUser(userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7490, true);
				 api_quest_RewardQuestUser(userObjID, 7490, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 750, 1);
					api_quest_SetQuestStep(userObjID, 750, 1);
					api_quest_SetJournalStep(userObjID, 750, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300498, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300498, api_quest_HasQuestItem(userObjID, 300498, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300499, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300499, api_quest_HasQuestItem(userObjID, 300499, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300500, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300500, api_quest_HasQuestItem(userObjID, 300500, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300501, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300501, api_quest_HasQuestItem(userObjID, 300501, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300502, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300502, api_quest_HasQuestItem(userObjID, 300502, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300503, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300503, api_quest_HasQuestItem(userObjID, 300503, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n394_academic_station-1", "sq15_750_life_that_someone_desire.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n799_academin_npc--------------------------------------------------------------------------------
function sq15_749_do_not_despair_OnTalk_n799_academin_npc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n799_academin_npc-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n799_academin_npc-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n799_academin_npc-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n799_academin_npc-1-b" then 
	end
	if npc_talk_index == "n799_academin_npc-1-c" then 
	end
	if npc_talk_index == "n799_academin_npc-1-d" then 
	end
	if npc_talk_index == "n799_academin_npc-1-e" then 
	end
	if npc_talk_index == "n799_academin_npc-1-f" then 
	end
	if npc_talk_index == "n799_academin_npc-1-g" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1611, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201611, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300503, 1);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300498, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300498, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_749_do_not_despair_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 749);
	if qstep == 2 and CountIndex == 1611 then
				api_quest_ClearCountingInfo(userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300503, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300503, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 201611 then
				api_quest_ClearCountingInfo(userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300503, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300503, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300503 then

	end
end

function sq15_749_do_not_despair_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 749);
	if qstep == 2 and CountIndex == 1611 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201611 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300503 and Count >= TargetCount  then

	end
end

function sq15_749_do_not_despair_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 749);
	local questID=749;
end

function sq15_749_do_not_despair_OnRemoteStart( userObjID, questID )
end

function sq15_749_do_not_despair_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_749_do_not_despair_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq15_749_do_not_despair_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 394 then
		sq15_749_do_not_despair_OnTalk_n394_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 799 then
		sq15_749_do_not_despair_OnTalk_n799_academin_npc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n394_academic_station--------------------------------------------------------------------------------
function sq15_749_do_not_despair_OnTalk_n394_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n394_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n394_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n394_academic_station-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n394_academic_station-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n394_academic_station-accepting-b" then
				api_quest_AddQuest( pRoom, userObjID,749, 1);
				api_quest_SetJournalStep( pRoom, userObjID,749, 1);
				api_quest_SetQuestStep( pRoom, userObjID,749, 1);

	end
	if npc_talk_index == "n394_academic_station-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7490, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7490, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7490, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-3-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 750, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 750, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 750, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300498, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300498, api_quest_HasQuestItem( pRoom, userObjID, 300498, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300499, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300499, api_quest_HasQuestItem( pRoom, userObjID, 300499, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300500, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300500, api_quest_HasQuestItem( pRoom, userObjID, 300500, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300501, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300501, api_quest_HasQuestItem( pRoom, userObjID, 300501, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300502, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300502, api_quest_HasQuestItem( pRoom, userObjID, 300502, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300503, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300503, api_quest_HasQuestItem( pRoom, userObjID, 300503, 1));
				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n394_academic_station-1", "sq15_750_life_that_someone_desire.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n799_academin_npc--------------------------------------------------------------------------------
function sq15_749_do_not_despair_OnTalk_n799_academin_npc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n799_academin_npc-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n799_academin_npc-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n799_academin_npc-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n799_academin_npc-1-b" then 
	end
	if npc_talk_index == "n799_academin_npc-1-c" then 
	end
	if npc_talk_index == "n799_academin_npc-1-d" then 
	end
	if npc_talk_index == "n799_academin_npc-1-e" then 
	end
	if npc_talk_index == "n799_academin_npc-1-f" then 
	end
	if npc_talk_index == "n799_academin_npc-1-g" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1611, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201611, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300503, 1);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300498, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300498, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_749_do_not_despair_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 749);
	if qstep == 2 and CountIndex == 1611 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300503, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300503, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 201611 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300503, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300503, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300503 then

	end
end

function sq15_749_do_not_despair_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 749);
	if qstep == 2 and CountIndex == 1611 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201611 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300503 and Count >= TargetCount  then

	end
end

function sq15_749_do_not_despair_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 749);
	local questID=749;
end

function sq15_749_do_not_despair_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_749_do_not_despair_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_749_do_not_despair_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>