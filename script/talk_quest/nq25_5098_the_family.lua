<VillageServer>

function nq25_5098_the_family_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 100 then
		nq25_5098_the_family_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function nq25_5098_the_family_OnTalk_n100_event_ilyn(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n100_event_ilyn-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n100_event_ilyn-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 50980, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 50980, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 50980, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 50980, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 50980, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 50980, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 50980, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 50980, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 50980, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-acceppted" then
				api_quest_AddQuest(userObjID,5098, 1);
				api_quest_SetJournalStep(userObjID,5098, 1);
				api_quest_SetQuestStep(userObjID,5098, 1);
				npc_talk_index = "n100_event_ilyn-1";

	end
	if npc_talk_index == "n100_event_ilyn-1-a" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1023, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 1024, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 1025, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 201023, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 201024, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 201025, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 3, 300322, 3);
				api_quest_SetCountingInfo(userObjID, questID, 7, 3, 300323, 3);
				api_quest_SetCountingInfo(userObjID, questID, 8, 3, 300324, 1);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n100_event_ilyn-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 50980, true);
				 api_quest_RewardQuestUser(userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 50980, true);
				 api_quest_RewardQuestUser(userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 50980, true);
				 api_quest_RewardQuestUser(userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 50980, true);
				 api_quest_RewardQuestUser(userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 50980, true);
				 api_quest_RewardQuestUser(userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 50980, true);
				 api_quest_RewardQuestUser(userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 50980, true);
				 api_quest_RewardQuestUser(userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 50980, true);
				 api_quest_RewardQuestUser(userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 50980, true);
				 api_quest_RewardQuestUser(userObjID, 50980, questID, 1);
			 end 
	end
	if npc_talk_index == "n100_event_ilyn-3-b" then 

				if api_quest_HasQuestItem(userObjID, 300322, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300322, api_quest_HasQuestItem(userObjID, 300322, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300323, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300323, api_quest_HasQuestItem(userObjID, 300323, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300324, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300324, api_quest_HasQuestItem(userObjID, 300324, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 100, 600 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq25_5098_the_family_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5098);
	if qstep == 2 and CountIndex == 1023 then
				if api_quest_HasQuestItem(userObjID, 300322, 3) == -3 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300322, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300322, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 2 and CountIndex == 1024 then
				if api_quest_HasQuestItem(userObjID, 300323, 3) == -3 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300323, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300323, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 2 and CountIndex == 1025 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300324, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300324, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300322 then

	end
	if qstep == 2 and CountIndex == 300323 then

	end
	if qstep == 2 and CountIndex == 300324 then

	end
	if qstep == 2 and CountIndex == 201023 then
				if api_quest_HasQuestItem(userObjID, 300322, 3) == -3 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300322, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300322, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 2 and CountIndex == 201024 then
				if api_quest_HasQuestItem(userObjID, 300323, 3) == -3 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300323, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300323, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 2 and CountIndex == 201025 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300324, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300324, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function nq25_5098_the_family_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5098);
	if qstep == 2 and CountIndex == 1023 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1024 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1025 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300322 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300323 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300324 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201023 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201024 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201025 and Count >= TargetCount  then

	end
end

function nq25_5098_the_family_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5098);
	local questID=5098;
	if qstep == 2 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
end

function nq25_5098_the_family_OnRemoteStart( userObjID, questID )
end

function nq25_5098_the_family_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq25_5098_the_family_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq25_5098_the_family_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 100 then
		nq25_5098_the_family_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n100_event_ilyn--------------------------------------------------------------------------------
function nq25_5098_the_family_OnTalk_n100_event_ilyn( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n100_event_ilyn-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n100_event_ilyn-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n100_event_ilyn-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n100_event_ilyn-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n100_event_ilyn-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, false);
			 end 

	end
	if npc_talk_index == "n100_event_ilyn-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,5098, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5098, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5098, 1);
				npc_talk_index = "n100_event_ilyn-1";

	end
	if npc_talk_index == "n100_event_ilyn-1-a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1023, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 1024, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 1025, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 201023, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 201024, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 201025, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 3, 300322, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 3, 300323, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 3, 300324, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n100_event_ilyn-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 50980, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 50980, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 50980, questID, 1);
			 end 
	end
	if npc_talk_index == "n100_event_ilyn-3-b" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300322, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300322, api_quest_HasQuestItem( pRoom, userObjID, 300322, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300323, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300323, api_quest_HasQuestItem( pRoom, userObjID, 300323, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300324, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300324, api_quest_HasQuestItem( pRoom, userObjID, 300324, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 100, 600 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq25_5098_the_family_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5098);
	if qstep == 2 and CountIndex == 1023 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300322, 3) == -3 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300322, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300322, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 2 and CountIndex == 1024 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300323, 3) == -3 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300323, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300323, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 2 and CountIndex == 1025 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300324, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300324, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 300322 then

	end
	if qstep == 2 and CountIndex == 300323 then

	end
	if qstep == 2 and CountIndex == 300324 then

	end
	if qstep == 2 and CountIndex == 201023 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300322, 3) == -3 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300322, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300322, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 2 and CountIndex == 201024 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300323, 3) == -3 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300323, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300323, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

				else
				end

	end
	if qstep == 2 and CountIndex == 201025 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300324, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300324, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function nq25_5098_the_family_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5098);
	if qstep == 2 and CountIndex == 1023 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1024 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1025 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300322 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300323 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300324 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201023 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201024 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201025 and Count >= TargetCount  then

	end
end

function nq25_5098_the_family_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5098);
	local questID=5098;
	if qstep == 2 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
end

function nq25_5098_the_family_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq25_5098_the_family_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq25_5098_the_family_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>