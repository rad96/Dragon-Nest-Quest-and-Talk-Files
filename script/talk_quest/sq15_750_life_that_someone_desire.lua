<VillageServer>

function sq15_750_life_that_someone_desire_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 394 then
		sq15_750_life_that_someone_desire_OnTalk_n394_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 803 then
		sq15_750_life_that_someone_desire_OnTalk_n803_academin_npc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 806 then
		sq15_750_life_that_someone_desire_OnTalk_n806_academin_npc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 808 then
		sq15_750_life_that_someone_desire_OnTalk_n808_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n394_academic_station--------------------------------------------------------------------------------
function sq15_750_life_that_someone_desire_OnTalk_n394_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n394_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n394_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n394_academic_station-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n394_academic_station-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n394_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,750, 1);
				api_quest_SetJournalStep(userObjID,750, 1);
				api_quest_SetQuestStep(userObjID,750, 1);
				npc_talk_index = "n394_academic_station-1";

	end
	if npc_talk_index == "n394_academic_station-1-partycheck" then 
				if api_user_IsPartymember(userObjID) == 0  then
									if api_user_GetUserLevel(userObjID) >= 45 then
									if api_user_GetUserJobID(userObjID) == 46   or api_user_GetUserJobID(userObjID) == 49  then
									npc_talk_index = "n394_academic_station-1-b";

				else
									if api_user_GetUserJobID(userObjID) == 47   or api_user_GetUserJobID(userObjID) == 48   or api_user_GetUserJobID(userObjID) == 50   or api_user_GetUserJobID(userObjID) == 51  then
									api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 5);
				npc_talk_index = "n394_academic_station-5";

				else
									npc_talk_index = "n394_academic_station-1-c";

				end

				end

				else
									npc_talk_index = "n394_academic_station-1-c";

				end

				else
									npc_talk_index = "n394_academic_station-1-a";

				end
	end
	if npc_talk_index == "n394_academic_station-1-changemap" then 
				api_user_ChangeMap(userObjID,13512,1);
	end
	if npc_talk_index == "n394_academic_station-4-parytcheck_4" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n394_academic_station-4-m";

				else
									npc_talk_index = "n394_academic_station-4-l";

				end
	end
	if npc_talk_index == "n394_academic_station-4" then 
	end
	if npc_talk_index == "n394_academic_station-4-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-4" then 
	end
	if npc_talk_index == "n394_academic_station-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-4-change_class1" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 47);
				if api_quest_GetQuestStep(userObjID, 738) == 1  then
									api_quest_SetQuestStep(userObjID, 738, 2);
				api_quest_SetJournalStep(userObjID, 738, 2);

				else
				end
				npc_talk_index = "n394_academic_station-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-4-change_class2" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 48);
				if api_quest_GetQuestStep(userObjID, 738) == 1  then
									api_quest_SetQuestStep(userObjID, 738, 2);
				api_quest_SetJournalStep(userObjID, 738, 2);

				else
				end
				npc_talk_index = "n394_academic_station-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-4-f" then 
	end
	if npc_talk_index == "n394_academic_station-4-h" then 
	end
	if npc_talk_index == "n394_academic_station-4-i" then 
	end
	if npc_talk_index == "n394_academic_station-4-g" then 
	end
	if npc_talk_index == "n394_academic_station-4-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-4-g" then 
	end
	if npc_talk_index == "n394_academic_station-4-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-4-change_class3" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 50);
				if api_quest_GetQuestStep(userObjID, 738) == 1  then
									api_quest_SetQuestStep(userObjID, 738, 2);
				api_quest_SetJournalStep(userObjID, 738, 2);

				else
				end
				npc_talk_index = "n394_academic_station-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-4-change_class4" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 51);
				if api_quest_GetQuestStep(userObjID, 738) == 1  then
									api_quest_SetQuestStep(userObjID, 738, 2);
				api_quest_SetJournalStep(userObjID, 738, 2);

				else
				end
				npc_talk_index = "n394_academic_station-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-4-changemap_4" then 
				api_user_ChangeMap(userObjID,13512,1);
	end
	if npc_talk_index == "n394_academic_station-5-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7500, true);
				 api_quest_RewardQuestUser(userObjID, 7500, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-5-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-5-c" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n803_academin_npc--------------------------------------------------------------------------------
function sq15_750_life_that_someone_desire_OnTalk_n803_academin_npc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n803_academin_npc-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n803_academin_npc-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n803_academin_npc-1-cl_check" then 
				if api_user_GetUserJobID(userObjID) == 46  then
									npc_talk_index = "n803_academin_npc-1-b";

				else
									if api_user_GetUserJobID(userObjID) == 49  then
									npc_talk_index = "n803_academin_npc-1-l";

				else
									npc_talk_index = "n803_academin_npc-1-l";

				end

				end
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end
	if npc_talk_index == "n803_academin_npc-1-b" then 
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end
	if npc_talk_index == "n803_academin_npc-1-b" then 
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end
	if npc_talk_index == "n803_academin_npc-1-f" then 
	end
	if npc_talk_index == "n803_academin_npc-1-g" then 
	end
	if npc_talk_index == "n803_academin_npc-1-h" then 
	end
	if npc_talk_index == "n803_academin_npc-1-i" then 
	end
	if npc_talk_index == "n803_academin_npc-1-j" then 
	end
	if npc_talk_index == "n803_academin_npc-1-k" then 
	end
	if npc_talk_index == "n803_academin_npc-1-attack" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end
	if npc_talk_index == "n803_academin_npc-1-l" then 
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end
	if npc_talk_index == "n803_academin_npc-1-l" then 
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n806_academin_npc--------------------------------------------------------------------------------
function sq15_750_life_that_someone_desire_OnTalk_n806_academin_npc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n806_academin_npc-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n806_academin_npc-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n806_academin_npc-3-b" then 
	end
	if npc_talk_index == "n806_academin_npc-3-c" then 
	end
	if npc_talk_index == "n806_academin_npc-3-d" then 
	end
	if npc_talk_index == "n806_academin_npc-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n808_academic_station--------------------------------------------------------------------------------
function sq15_750_life_that_someone_desire_OnTalk_n808_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n808_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n808_academic_station-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n808_academic_station-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n808_academic_station-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n808_academic_station-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n808_academic_station-2-open_ui2" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n808_academic_station-2-job_test3" then 
				api_user_ClearSecondJobSkill( userObjID );
				npc_talk_index = "n808_academic_station-2-a";
				api_user_SetSecondJobSkill( userObjID, 50 );
	end
	if npc_talk_index == "n808_academic_station-2-job_test4" then 
				api_user_ClearSecondJobSkill( userObjID );
				npc_talk_index = "n808_academic_station-2-a";
				api_user_SetSecondJobSkill( userObjID, 51 );
	end
	if npc_talk_index == "n808_academic_station-4-open_ui4" then 
				api_ui_OpenJobChange(userObjID)
	end
	if npc_talk_index == "n808_academic_station-4" then 
	end
	if npc_talk_index == "n808_academic_station-4-retrun_lotus" then 
				api_user_ChangeMap(userObjID,15,1);
	end
	if npc_talk_index == "n808_academic_station-4" then 
	end
	if npc_talk_index == "n808_academic_station-4-reset1" then 
				npc_talk_index = "n808_academic_station-2";
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n808_academic_station-4" then 
	end
	if npc_talk_index == "n808_academic_station-4-reset2" then 
				api_npc_NextTalk(userObjID, npcObjID, "n808_academic_station-2-b", npc_talk_target);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_750_life_that_someone_desire_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 750);
end

function sq15_750_life_that_someone_desire_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 750);
end

function sq15_750_life_that_someone_desire_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 750);
	local questID=750;
end

function sq15_750_life_that_someone_desire_OnRemoteStart( userObjID, questID )
end

function sq15_750_life_that_someone_desire_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_750_life_that_someone_desire_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,750, 1);
				api_quest_SetJournalStep(userObjID,750, 1);
				api_quest_SetQuestStep(userObjID,750, 1);
				npc_talk_index = "n394_academic_station-1";
end

</VillageServer>

<GameServer>
function sq15_750_life_that_someone_desire_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 394 then
		sq15_750_life_that_someone_desire_OnTalk_n394_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 803 then
		sq15_750_life_that_someone_desire_OnTalk_n803_academin_npc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 806 then
		sq15_750_life_that_someone_desire_OnTalk_n806_academin_npc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 808 then
		sq15_750_life_that_someone_desire_OnTalk_n808_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n394_academic_station--------------------------------------------------------------------------------
function sq15_750_life_that_someone_desire_OnTalk_n394_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n394_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n394_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n394_academic_station-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n394_academic_station-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n394_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,750, 1);
				api_quest_SetJournalStep( pRoom, userObjID,750, 1);
				api_quest_SetQuestStep( pRoom, userObjID,750, 1);
				npc_talk_index = "n394_academic_station-1";

	end
	if npc_talk_index == "n394_academic_station-1-partycheck" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									if api_user_GetUserLevel( pRoom, userObjID) >= 45 then
									if api_user_GetUserJobID( pRoom, userObjID) == 46   or api_user_GetUserJobID( pRoom, userObjID) == 49  then
									npc_talk_index = "n394_academic_station-1-b";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 47   or api_user_GetUserJobID( pRoom, userObjID) == 48   or api_user_GetUserJobID( pRoom, userObjID) == 50   or api_user_GetUserJobID( pRoom, userObjID) == 51  then
									api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				npc_talk_index = "n394_academic_station-5";

				else
									npc_talk_index = "n394_academic_station-1-c";

				end

				end

				else
									npc_talk_index = "n394_academic_station-1-c";

				end

				else
									npc_talk_index = "n394_academic_station-1-a";

				end
	end
	if npc_talk_index == "n394_academic_station-1-changemap" then 
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end
	if npc_talk_index == "n394_academic_station-4-parytcheck_4" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n394_academic_station-4-m";

				else
									npc_talk_index = "n394_academic_station-4-l";

				end
	end
	if npc_talk_index == "n394_academic_station-4" then 
	end
	if npc_talk_index == "n394_academic_station-4-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-4" then 
	end
	if npc_talk_index == "n394_academic_station-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-4-change_class1" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 47);
				if api_quest_GetQuestStep( pRoom, userObjID, 738) == 1  then
									api_quest_SetQuestStep( pRoom, userObjID, 738, 2);
				api_quest_SetJournalStep( pRoom, userObjID, 738, 2);

				else
				end
				npc_talk_index = "n394_academic_station-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-4-change_class2" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 48);
				if api_quest_GetQuestStep( pRoom, userObjID, 738) == 1  then
									api_quest_SetQuestStep( pRoom, userObjID, 738, 2);
				api_quest_SetJournalStep( pRoom, userObjID, 738, 2);

				else
				end
				npc_talk_index = "n394_academic_station-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-4-f" then 
	end
	if npc_talk_index == "n394_academic_station-4-h" then 
	end
	if npc_talk_index == "n394_academic_station-4-i" then 
	end
	if npc_talk_index == "n394_academic_station-4-g" then 
	end
	if npc_talk_index == "n394_academic_station-4-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-4-g" then 
	end
	if npc_talk_index == "n394_academic_station-4-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-4-change_class3" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 50);
				if api_quest_GetQuestStep( pRoom, userObjID, 738) == 1  then
									api_quest_SetQuestStep( pRoom, userObjID, 738, 2);
				api_quest_SetJournalStep( pRoom, userObjID, 738, 2);

				else
				end
				npc_talk_index = "n394_academic_station-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-4-change_class4" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 51);
				if api_quest_GetQuestStep( pRoom, userObjID, 738) == 1  then
									api_quest_SetQuestStep( pRoom, userObjID, 738, 2);
				api_quest_SetJournalStep( pRoom, userObjID, 738, 2);

				else
				end
				npc_talk_index = "n394_academic_station-4-e";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-4-changemap_4" then 
				api_user_ChangeMap( pRoom, userObjID,13512,1);
	end
	if npc_talk_index == "n394_academic_station-5-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7500, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-5-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-5-c" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n803_academin_npc--------------------------------------------------------------------------------
function sq15_750_life_that_someone_desire_OnTalk_n803_academin_npc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n803_academin_npc-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n803_academin_npc-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n803_academin_npc-1-cl_check" then 
				if api_user_GetUserJobID( pRoom, userObjID) == 46  then
									npc_talk_index = "n803_academin_npc-1-b";

				else
									if api_user_GetUserJobID( pRoom, userObjID) == 49  then
									npc_talk_index = "n803_academin_npc-1-l";

				else
									npc_talk_index = "n803_academin_npc-1-l";

				end

				end
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end
	if npc_talk_index == "n803_academin_npc-1-b" then 
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end
	if npc_talk_index == "n803_academin_npc-1-b" then 
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end
	if npc_talk_index == "n803_academin_npc-1-f" then 
	end
	if npc_talk_index == "n803_academin_npc-1-g" then 
	end
	if npc_talk_index == "n803_academin_npc-1-h" then 
	end
	if npc_talk_index == "n803_academin_npc-1-i" then 
	end
	if npc_talk_index == "n803_academin_npc-1-j" then 
	end
	if npc_talk_index == "n803_academin_npc-1-k" then 
	end
	if npc_talk_index == "n803_academin_npc-1-attack" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end
	if npc_talk_index == "n803_academin_npc-1-l" then 
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end
	if npc_talk_index == "n803_academin_npc-1-l" then 
	end
	if npc_talk_index == "n803_academin_npc-1-e" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n806_academin_npc--------------------------------------------------------------------------------
function sq15_750_life_that_someone_desire_OnTalk_n806_academin_npc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n806_academin_npc-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n806_academin_npc-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n806_academin_npc-3-b" then 
	end
	if npc_talk_index == "n806_academin_npc-3-c" then 
	end
	if npc_talk_index == "n806_academin_npc-3-d" then 
	end
	if npc_talk_index == "n806_academin_npc-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n808_academic_station--------------------------------------------------------------------------------
function sq15_750_life_that_someone_desire_OnTalk_n808_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n808_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n808_academic_station-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n808_academic_station-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n808_academic_station-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n808_academic_station-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n808_academic_station-2-open_ui2" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n808_academic_station-2-job_test3" then 
				api_user_ClearSecondJobSkill( pRoom,  userObjID );
				npc_talk_index = "n808_academic_station-2-a";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 50 );
	end
	if npc_talk_index == "n808_academic_station-2-job_test4" then 
				api_user_ClearSecondJobSkill( pRoom,  userObjID );
				npc_talk_index = "n808_academic_station-2-a";
				api_user_SetSecondJobSkill( pRoom,  userObjID, 51 );
	end
	if npc_talk_index == "n808_academic_station-4-open_ui4" then 
				api_ui_OpenJobChange( pRoom, userObjID)
	end
	if npc_talk_index == "n808_academic_station-4" then 
	end
	if npc_talk_index == "n808_academic_station-4-retrun_lotus" then 
				api_user_ChangeMap( pRoom, userObjID,15,1);
	end
	if npc_talk_index == "n808_academic_station-4" then 
	end
	if npc_talk_index == "n808_academic_station-4-reset1" then 
				npc_talk_index = "n808_academic_station-2";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n808_academic_station-4" then 
	end
	if npc_talk_index == "n808_academic_station-4-reset2" then 
				api_npc_NextTalk( pRoom, userObjID, npcObjID, "n808_academic_station-2-b", npc_talk_target);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_750_life_that_someone_desire_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 750);
end

function sq15_750_life_that_someone_desire_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 750);
end

function sq15_750_life_that_someone_desire_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 750);
	local questID=750;
end

function sq15_750_life_that_someone_desire_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_750_life_that_someone_desire_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_750_life_that_someone_desire_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,750, 1);
				api_quest_SetJournalStep( pRoom, userObjID,750, 1);
				api_quest_SetQuestStep( pRoom, userObjID,750, 1);
				npc_talk_index = "n394_academic_station-1";
end

</GameServer>