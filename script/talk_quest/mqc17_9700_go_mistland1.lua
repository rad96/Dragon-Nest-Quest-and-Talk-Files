<VillageServer>

function mqc17_9700_go_mistland1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 775 then
		mqc17_9700_go_mistland1_OnTalk_n775_cleric_ilizer(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		mqc17_9700_go_mistland1_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1839 then
		mqc17_9700_go_mistland1_OnTalk_n1839_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1840 then
		mqc17_9700_go_mistland1_OnTalk_n1840_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n775_cleric_ilizer--------------------------------------------------------------------------------
function mqc17_9700_go_mistland1_OnTalk_n775_cleric_ilizer(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n775_cleric_ilizer-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n775_cleric_ilizer-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n775_cleric_ilizer-accepting-party_check" then
				if api_user_GetPartymemberCount(userObjID) == 1  then
									npc_talk_index = "n775_cleric_ilizer-accepting-d";

				else
									npc_talk_index = "n775_cleric_ilizer-accepting-e";

				end

	end
	if npc_talk_index == "n775_cleric_ilizer-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9700, 2);
				api_quest_SetJournalStep(userObjID,9700, 1);
				api_quest_SetQuestStep(userObjID,9700, 1);
				npc_talk_index = "n775_cleric_ilizer-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function mqc17_9700_go_mistland1_OnTalk_n776_darklair_mardlen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n776_darklair_mardlen-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-1-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n776_darklair_mardlen-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97000, true);
				 api_quest_RewardQuestUser(userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97000, true);
				 api_quest_RewardQuestUser(userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97000, true);
				 api_quest_RewardQuestUser(userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97000, true);
				 api_quest_RewardQuestUser(userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97000, true);
				 api_quest_RewardQuestUser(userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97000, true);
				 api_quest_RewardQuestUser(userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97000, true);
				 api_quest_RewardQuestUser(userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97000, true);
				 api_quest_RewardQuestUser(userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97000, true);
				 api_quest_RewardQuestUser(userObjID, 97000, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9701, 2);
					api_quest_SetQuestStep(userObjID, 9701, 1);
					api_quest_SetJournalStep(userObjID, 9701, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n776_darklair_mardlen-1", "mqc17_9701_go_mistland2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1839_gereint_kid--------------------------------------------------------------------------------
function mqc17_9700_go_mistland1_OnTalk_n1839_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1839_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1839_gereint_kid-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1839_gereint_kid-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1839_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-f" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-g" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-h" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-i" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-j" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-k" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-l" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-m" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-n" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-o" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-p" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-q" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-r" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-s" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-t" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-u" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-v" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-w" then 
	end
	if npc_talk_index == "n1839_gereint_kid-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1840_velskud--------------------------------------------------------------------------------
function mqc17_9700_go_mistland1_OnTalk_n1840_velskud(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1840_velskud-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9700_go_mistland1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9700);
end

function mqc17_9700_go_mistland1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9700);
end

function mqc17_9700_go_mistland1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9700);
	local questID=9700;
end

function mqc17_9700_go_mistland1_OnRemoteStart( userObjID, questID )
end

function mqc17_9700_go_mistland1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc17_9700_go_mistland1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9700, 2);
				api_quest_SetJournalStep(userObjID,9700, 1);
				api_quest_SetQuestStep(userObjID,9700, 1);
				npc_talk_index = "n775_cleric_ilizer-1";
end

</VillageServer>

<GameServer>
function mqc17_9700_go_mistland1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 775 then
		mqc17_9700_go_mistland1_OnTalk_n775_cleric_ilizer( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 776 then
		mqc17_9700_go_mistland1_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1839 then
		mqc17_9700_go_mistland1_OnTalk_n1839_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1840 then
		mqc17_9700_go_mistland1_OnTalk_n1840_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n775_cleric_ilizer--------------------------------------------------------------------------------
function mqc17_9700_go_mistland1_OnTalk_n775_cleric_ilizer( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n775_cleric_ilizer-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n775_cleric_ilizer-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n775_cleric_ilizer-accepting-party_check" then
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									npc_talk_index = "n775_cleric_ilizer-accepting-d";

				else
									npc_talk_index = "n775_cleric_ilizer-accepting-e";

				end

	end
	if npc_talk_index == "n775_cleric_ilizer-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9700, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9700, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9700, 1);
				npc_talk_index = "n775_cleric_ilizer-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n776_darklair_mardlen--------------------------------------------------------------------------------
function mqc17_9700_go_mistland1_OnTalk_n776_darklair_mardlen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n776_darklair_mardlen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n776_darklair_mardlen-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n776_darklair_mardlen-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n776_darklair_mardlen-1-b" then 
	end
	if npc_talk_index == "n776_darklair_mardlen-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n776_darklair_mardlen-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97000, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97000, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97000, questID, 1);
			 end 
	end
	if npc_talk_index == "n776_darklair_mardlen-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9701, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9701, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9701, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n776_darklair_mardlen-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n776_darklair_mardlen-1", "mqc17_9701_go_mistland2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1839_gereint_kid--------------------------------------------------------------------------------
function mqc17_9700_go_mistland1_OnTalk_n1839_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1839_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1839_gereint_kid-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1839_gereint_kid-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1839_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-e" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-f" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-g" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-h" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-i" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-j" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-k" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-l" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-m" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-n" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-o" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-p" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-q" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-r" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-s" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-t" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-u" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-v" then 
	end
	if npc_talk_index == "n1839_gereint_kid-2-w" then 
	end
	if npc_talk_index == "n1839_gereint_kid-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1840_velskud--------------------------------------------------------------------------------
function mqc17_9700_go_mistland1_OnTalk_n1840_velskud( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1840_velskud-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc17_9700_go_mistland1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9700);
end

function mqc17_9700_go_mistland1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9700);
end

function mqc17_9700_go_mistland1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9700);
	local questID=9700;
end

function mqc17_9700_go_mistland1_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc17_9700_go_mistland1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc17_9700_go_mistland1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9700, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9700, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9700, 1);
				npc_talk_index = "n775_cleric_ilizer-1";
end

</GameServer>