<VillageServer>

function mq08_217_blessed_stone_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 35 then
		mq08_217_blessed_stone_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n035_soceress_master_tiana--------------------------------------------------------------------------------
function mq08_217_blessed_stone_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n035_soceress_master_tiana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest(userObjID,217, 2);
				api_quest_SetJournalStep(userObjID,217, 1);
				api_quest_SetQuestStep(userObjID,217, 1);

				if api_quest_HasQuestItem(userObjID, 300004, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300004, api_quest_HasQuestItem(userObjID, 300004, 1));
				end
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 21014, 30000);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2171, true);
				 api_quest_RewardQuestUser(userObjID, 2171, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2172, true);
				 api_quest_RewardQuestUser(userObjID, 2172, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2173, true);
				 api_quest_RewardQuestUser(userObjID, 2173, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2174, true);
				 api_quest_RewardQuestUser(userObjID, 2174, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2176, true);
				 api_quest_RewardQuestUser(userObjID, 2176, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2176, true);
				 api_quest_RewardQuestUser(userObjID, 2176, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2171, true);
				 api_quest_RewardQuestUser(userObjID, 2171, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2178, true);
				 api_quest_RewardQuestUser(userObjID, 2178, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-comp_joball" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_quest_AddQuest(userObjID,218, 2);
				api_quest_SetJournalStep(userObjID,218, 1);
				api_quest_SetQuestStep(userObjID,218, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-comp_job8" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_quest_AddQuest(userObjID,9477, 2);
				api_quest_SetJournalStep(userObjID,9477, 1);
				api_quest_SetQuestStep(userObjID,9477, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_217_blessed_stone_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 217);
	if qstep == 2 and CountIndex == 21014 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300064, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300064, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq08_217_blessed_stone_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 217);
	if qstep == 2 and CountIndex == 21014 and Count >= TargetCount  then

	end
end

function mq08_217_blessed_stone_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 217);
	local questID=217;
end

function mq08_217_blessed_stone_OnRemoteStart( userObjID, questID )
end

function mq08_217_blessed_stone_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq08_217_blessed_stone_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,217, 2);
				api_quest_SetJournalStep(userObjID,217, 1);
				api_quest_SetQuestStep(userObjID,217, 1);

				if api_quest_HasQuestItem(userObjID, 300004, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300004, api_quest_HasQuestItem(userObjID, 300004, 1));
				end
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</VillageServer>

<GameServer>
function mq08_217_blessed_stone_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 35 then
		mq08_217_blessed_stone_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n035_soceress_master_tiana--------------------------------------------------------------------------------
function mq08_217_blessed_stone_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n035_soceress_master_tiana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,217, 2);
				api_quest_SetJournalStep( pRoom, userObjID,217, 1);
				api_quest_SetQuestStep( pRoom, userObjID,217, 1);

				if api_quest_HasQuestItem( pRoom, userObjID, 300004, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300004, api_quest_HasQuestItem( pRoom, userObjID, 300004, 1));
				end
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 21014, 30000);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2171, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2171, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2172, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2172, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2173, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2173, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2174, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2174, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2176, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2176, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2176, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2176, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2171, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2171, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2178, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2178, questID, 1);
			 end 
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-comp_joball" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_quest_AddQuest( pRoom, userObjID,218, 2);
				api_quest_SetJournalStep( pRoom, userObjID,218, 1);
				api_quest_SetQuestStep( pRoom, userObjID,218, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-comp_job8" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_quest_AddQuest( pRoom, userObjID,9477, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9477, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9477, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
	end
	if npc_talk_index == "n035_soceress_master_tiana-3-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq08_217_blessed_stone_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 217);
	if qstep == 2 and CountIndex == 21014 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300064, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300064, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function mq08_217_blessed_stone_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 217);
	if qstep == 2 and CountIndex == 21014 and Count >= TargetCount  then

	end
end

function mq08_217_blessed_stone_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 217);
	local questID=217;
end

function mq08_217_blessed_stone_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq08_217_blessed_stone_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq08_217_blessed_stone_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,217, 2);
				api_quest_SetJournalStep( pRoom, userObjID,217, 1);
				api_quest_SetQuestStep( pRoom, userObjID,217, 1);

				if api_quest_HasQuestItem( pRoom, userObjID, 300004, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300004, api_quest_HasQuestItem( pRoom, userObjID, 300004, 1));
				end
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</GameServer>