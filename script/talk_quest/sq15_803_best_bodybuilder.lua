<VillageServer>

function sq15_803_best_bodybuilder_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 714 then
		sq15_803_best_bodybuilder_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 715 then
		sq15_803_best_bodybuilder_OnTalk_n715_adventurer_guildmaster_hose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n714_cleric_yohan--------------------------------------------------------------------------------
function sq15_803_best_bodybuilder_OnTalk_n714_cleric_yohan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n714_cleric_yohan-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n714_cleric_yohan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n714_cleric_yohan-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-1-b" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n715_adventurer_guildmaster_hose--------------------------------------------------------------------------------
function sq15_803_best_bodybuilder_OnTalk_n715_adventurer_guildmaster_hose(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n715_adventurer_guildmaster_hose-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n715_adventurer_guildmaster_hose-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n715_adventurer_guildmaster_hose-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n715_adventurer_guildmaster_hose-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n715_adventurer_guildmaster_hose-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n715_adventurer_guildmaster_hose-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8030, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8030, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8030, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8030, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8030, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8030, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8030, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8030, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8030, false);
			 end 

	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-accepting-acceptted" then
				api_quest_AddQuest(userObjID,803, 1);
				api_quest_SetJournalStep(userObjID,803, 1);
				api_quest_SetQuestStep(userObjID,803, 1);
				npc_talk_index = "n715_adventurer_guildmaster_hose-1";

	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-2-b" then 
	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-2-c" then 
	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1405, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201405, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300433, 5);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-4-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8030, true);
				 api_quest_RewardQuestUser(userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8030, true);
				 api_quest_RewardQuestUser(userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8030, true);
				 api_quest_RewardQuestUser(userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8030, true);
				 api_quest_RewardQuestUser(userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8030, true);
				 api_quest_RewardQuestUser(userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8030, true);
				 api_quest_RewardQuestUser(userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8030, true);
				 api_quest_RewardQuestUser(userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8030, true);
				 api_quest_RewardQuestUser(userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8030, true);
				 api_quest_RewardQuestUser(userObjID, 8030, questID, 1);
			 end 
	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-4-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 300433, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300433, api_quest_HasQuestItem(userObjID, 300433, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_803_best_bodybuilder_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 803);
	if qstep == 3 and CountIndex == 1405 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300433, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300433, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 201405 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300433, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300433, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300433 then

	end
end

function sq15_803_best_bodybuilder_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 803);
	if qstep == 3 and CountIndex == 1405 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 201405 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300433 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

	end
end

function sq15_803_best_bodybuilder_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 803);
	local questID=803;
end

function sq15_803_best_bodybuilder_OnRemoteStart( userObjID, questID )
end

function sq15_803_best_bodybuilder_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_803_best_bodybuilder_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,803, 1);
				api_quest_SetJournalStep(userObjID,803, 1);
				api_quest_SetQuestStep(userObjID,803, 1);
				npc_talk_index = "n715_adventurer_guildmaster_hose-1";
end

</VillageServer>

<GameServer>
function sq15_803_best_bodybuilder_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 714 then
		sq15_803_best_bodybuilder_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 715 then
		sq15_803_best_bodybuilder_OnTalk_n715_adventurer_guildmaster_hose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n714_cleric_yohan--------------------------------------------------------------------------------
function sq15_803_best_bodybuilder_OnTalk_n714_cleric_yohan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n714_cleric_yohan-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n714_cleric_yohan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n714_cleric_yohan-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n714_cleric_yohan-1-b" then 
	end
	if npc_talk_index == "n714_cleric_yohan-1-c" then 
	end
	if npc_talk_index == "n714_cleric_yohan-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n715_adventurer_guildmaster_hose--------------------------------------------------------------------------------
function sq15_803_best_bodybuilder_OnTalk_n715_adventurer_guildmaster_hose( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n715_adventurer_guildmaster_hose-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n715_adventurer_guildmaster_hose-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n715_adventurer_guildmaster_hose-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n715_adventurer_guildmaster_hose-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n715_adventurer_guildmaster_hose-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n715_adventurer_guildmaster_hose-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, false);
			 end 

	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,803, 1);
				api_quest_SetJournalStep( pRoom, userObjID,803, 1);
				api_quest_SetQuestStep( pRoom, userObjID,803, 1);
				npc_talk_index = "n715_adventurer_guildmaster_hose-1";

	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-2-b" then 
	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-2-c" then 
	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1405, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201405, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300433, 5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-4-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8030, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8030, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8030, questID, 1);
			 end 
	end
	if npc_talk_index == "n715_adventurer_guildmaster_hose-4-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300433, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300433, api_quest_HasQuestItem( pRoom, userObjID, 300433, 1));
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_803_best_bodybuilder_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 803);
	if qstep == 3 and CountIndex == 1405 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300433, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300433, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 201405 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300433, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300433, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300433 then

	end
end

function sq15_803_best_bodybuilder_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 803);
	if qstep == 3 and CountIndex == 1405 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 201405 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300433 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

	end
end

function sq15_803_best_bodybuilder_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 803);
	local questID=803;
end

function sq15_803_best_bodybuilder_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_803_best_bodybuilder_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_803_best_bodybuilder_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,803, 1);
				api_quest_SetJournalStep( pRoom, userObjID,803, 1);
				api_quest_SetQuestStep( pRoom, userObjID,803, 1);
				npc_talk_index = "n715_adventurer_guildmaster_hose-1";
end

</GameServer>