<VillageServer>

function sq34_4315_the_man_cooking_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1046 then
		sq34_4315_the_man_cooking_OnTalk_n1046_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1046_slave_david--------------------------------------------------------------------------------
function sq34_4315_the_man_cooking_OnTalk_n1046_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1046_slave_david-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1046_slave_david-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1046_slave_david-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1046_slave_david-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1046_slave_david-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43150, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43150, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43150, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43150, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43150, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43150, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43150, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43150, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43150, false);
			 end 

	end
	if npc_talk_index == "n1046_slave_david-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4315, 1);
				api_quest_SetJournalStep(userObjID,4315, 1);
				api_quest_SetQuestStep(userObjID,4315, 1);
				npc_talk_index = "n1046_slave_david-1";

	end
	if npc_talk_index == "n1046_slave_david-1-b" then 
	end
	if npc_talk_index == "n1046_slave_david-1-c" then 
	end
	if npc_talk_index == "n1046_slave_david-1-d" then 
	end
	if npc_talk_index == "n1046_slave_david-1-e" then 
	end
	if npc_talk_index == "n1046_slave_david-1-f" then 
	end
	if npc_talk_index == "n1046_slave_david-1-g" then 
	end
	if npc_talk_index == "n1046_slave_david-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400385, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 601401, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 701401, 30000);
	end
	if npc_talk_index == "n1046_slave_david-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43150, true);
				 api_quest_RewardQuestUser(userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43150, true);
				 api_quest_RewardQuestUser(userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43150, true);
				 api_quest_RewardQuestUser(userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43150, true);
				 api_quest_RewardQuestUser(userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43150, true);
				 api_quest_RewardQuestUser(userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43150, true);
				 api_quest_RewardQuestUser(userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43150, true);
				 api_quest_RewardQuestUser(userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43150, true);
				 api_quest_RewardQuestUser(userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43150, true);
				 api_quest_RewardQuestUser(userObjID, 43150, questID, 1);
			 end 
	end
	if npc_talk_index == "n1046_slave_david-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem(userObjID, 400385, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400385, api_quest_HasQuestItem(userObjID, 400385, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4315_the_man_cooking_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4315);
	if qstep == 2 and CountIndex == 400385 then
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 2 and CountIndex == 601401 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400385, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400385, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 701401 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400385, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400385, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq34_4315_the_man_cooking_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4315);
	if qstep == 2 and CountIndex == 400385 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 601401 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701401 and Count >= TargetCount  then

	end
end

function sq34_4315_the_man_cooking_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4315);
	local questID=4315;
end

function sq34_4315_the_man_cooking_OnRemoteStart( userObjID, questID )
end

function sq34_4315_the_man_cooking_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq34_4315_the_man_cooking_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4315, 1);
				api_quest_SetJournalStep(userObjID,4315, 1);
				api_quest_SetQuestStep(userObjID,4315, 1);
				npc_talk_index = "n1046_slave_david-1";
end

</VillageServer>

<GameServer>
function sq34_4315_the_man_cooking_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1046 then
		sq34_4315_the_man_cooking_OnTalk_n1046_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1046_slave_david--------------------------------------------------------------------------------
function sq34_4315_the_man_cooking_OnTalk_n1046_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1046_slave_david-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1046_slave_david-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1046_slave_david-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1046_slave_david-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1046_slave_david-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, false);
			 end 

	end
	if npc_talk_index == "n1046_slave_david-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4315, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4315, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4315, 1);
				npc_talk_index = "n1046_slave_david-1";

	end
	if npc_talk_index == "n1046_slave_david-1-b" then 
	end
	if npc_talk_index == "n1046_slave_david-1-c" then 
	end
	if npc_talk_index == "n1046_slave_david-1-d" then 
	end
	if npc_talk_index == "n1046_slave_david-1-e" then 
	end
	if npc_talk_index == "n1046_slave_david-1-f" then 
	end
	if npc_talk_index == "n1046_slave_david-1-g" then 
	end
	if npc_talk_index == "n1046_slave_david-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400385, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 601401, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 701401, 30000);
	end
	if npc_talk_index == "n1046_slave_david-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43150, questID, 1);
			 end 
	end
	if npc_talk_index == "n1046_slave_david-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem( pRoom, userObjID, 400385, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400385, api_quest_HasQuestItem( pRoom, userObjID, 400385, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4315_the_man_cooking_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4315);
	if qstep == 2 and CountIndex == 400385 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 2 and CountIndex == 601401 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400385, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400385, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 701401 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400385, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400385, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq34_4315_the_man_cooking_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4315);
	if qstep == 2 and CountIndex == 400385 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 601401 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 701401 and Count >= TargetCount  then

	end
end

function sq34_4315_the_man_cooking_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4315);
	local questID=4315;
end

function sq34_4315_the_man_cooking_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq34_4315_the_man_cooking_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq34_4315_the_man_cooking_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4315, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4315, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4315, 1);
				npc_talk_index = "n1046_slave_david-1";
end

</GameServer>