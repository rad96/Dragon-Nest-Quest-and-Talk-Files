<VillageServer>

function mq05_093_review_the_current_time_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 12 then
		mq05_093_review_the_current_time_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 13 then
		mq05_093_review_the_current_time_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 391 then
		mq05_093_review_the_current_time_OnTalk_n391_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_093_review_the_current_time_OnTalk_n012_sorceress_master_cynthia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n012_sorceress_master_cynthia-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n012_sorceress_master_cynthia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-3-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 930, true);
				 api_quest_RewardQuestUser(userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 930, true);
				 api_quest_RewardQuestUser(userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 930, true);
				 api_quest_RewardQuestUser(userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 930, true);
				 api_quest_RewardQuestUser(userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 930, true);
				 api_quest_RewardQuestUser(userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 930, true);
				 api_quest_RewardQuestUser(userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 930, true);
				 api_quest_RewardQuestUser(userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 930, true);
				 api_quest_RewardQuestUser(userObjID, 930, questID, 1);
			 end 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-f" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 94, 2);
					api_quest_SetQuestStep(userObjID, 94, 1);
					api_quest_SetJournalStep(userObjID, 94, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-g" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n012_sorceress_master_cynthia-1", "mq05_094_track_the_location.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n013_cleric_tomas--------------------------------------------------------------------------------
function mq05_093_review_the_current_time_OnTalk_n013_cleric_tomas(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n013_cleric_tomas-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n013_cleric_tomas-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-2-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-d" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-e" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-f" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-g" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-g" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-h" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-i" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n391_academic_station--------------------------------------------------------------------------------
function mq05_093_review_the_current_time_OnTalk_n391_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n391_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n391_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n391_academic_station-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n391_academic_station-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n391_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,93, 2);
				api_quest_SetJournalStep(userObjID,93, 1);
				api_quest_SetQuestStep(userObjID,93, 1);
				npc_talk_index = "n391_academic_station-1";

	end
	if npc_talk_index == "n391_academic_station-1-b" then 
	end
	if npc_talk_index == "n391_academic_station-1-b" then 
	end
	if npc_talk_index == "n391_academic_station-1-c" then 
	end
	if npc_talk_index == "n391_academic_station-1-d" then 
	end
	if npc_talk_index == "n391_academic_station-1-e" then 
	end
	if npc_talk_index == "n391_academic_station-1-f" then 
	end
	if npc_talk_index == "n391_academic_station-1-g" then 
	end
	if npc_talk_index == "n391_academic_station-1-h" then 
	end
	if npc_talk_index == "n391_academic_station-1-i" then 
	end
	if npc_talk_index == "n391_academic_station-1-j" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_093_review_the_current_time_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 93);
end

function mq05_093_review_the_current_time_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 93);
end

function mq05_093_review_the_current_time_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 93);
	local questID=93;
end

function mq05_093_review_the_current_time_OnRemoteStart( userObjID, questID )
end

function mq05_093_review_the_current_time_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq05_093_review_the_current_time_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,93, 2);
				api_quest_SetJournalStep(userObjID,93, 1);
				api_quest_SetQuestStep(userObjID,93, 1);
				npc_talk_index = "n391_academic_station-1";
end

</VillageServer>

<GameServer>
function mq05_093_review_the_current_time_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 12 then
		mq05_093_review_the_current_time_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 13 then
		mq05_093_review_the_current_time_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 391 then
		mq05_093_review_the_current_time_OnTalk_n391_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n012_sorceress_master_cynthia--------------------------------------------------------------------------------
function mq05_093_review_the_current_time_OnTalk_n012_sorceress_master_cynthia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n012_sorceress_master_cynthia-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n012_sorceress_master_cynthia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n012_sorceress_master_cynthia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n012_sorceress_master_cynthia-3-b" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-c" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-d" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 930, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 930, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 930, questID, 1);
			 end 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-f" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 94, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 94, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 94, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-g" then 
	end
	if npc_talk_index == "n012_sorceress_master_cynthia-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n012_sorceress_master_cynthia-1", "mq05_094_track_the_location.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n013_cleric_tomas--------------------------------------------------------------------------------
function mq05_093_review_the_current_time_OnTalk_n013_cleric_tomas( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n013_cleric_tomas-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n013_cleric_tomas-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n013_cleric_tomas-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n013_cleric_tomas-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n013_cleric_tomas-2-b" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-c" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-d" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-e" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-f" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-g" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-g" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-h" then 
	end
	if npc_talk_index == "n013_cleric_tomas-2-i" then 
	end
	if npc_talk_index == "n013_cleric_tomas-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n391_academic_station--------------------------------------------------------------------------------
function mq05_093_review_the_current_time_OnTalk_n391_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n391_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n391_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n391_academic_station-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n391_academic_station-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n391_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,93, 2);
				api_quest_SetJournalStep( pRoom, userObjID,93, 1);
				api_quest_SetQuestStep( pRoom, userObjID,93, 1);
				npc_talk_index = "n391_academic_station-1";

	end
	if npc_talk_index == "n391_academic_station-1-b" then 
	end
	if npc_talk_index == "n391_academic_station-1-b" then 
	end
	if npc_talk_index == "n391_academic_station-1-c" then 
	end
	if npc_talk_index == "n391_academic_station-1-d" then 
	end
	if npc_talk_index == "n391_academic_station-1-e" then 
	end
	if npc_talk_index == "n391_academic_station-1-f" then 
	end
	if npc_talk_index == "n391_academic_station-1-g" then 
	end
	if npc_talk_index == "n391_academic_station-1-h" then 
	end
	if npc_talk_index == "n391_academic_station-1-i" then 
	end
	if npc_talk_index == "n391_academic_station-1-j" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq05_093_review_the_current_time_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 93);
end

function mq05_093_review_the_current_time_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 93);
end

function mq05_093_review_the_current_time_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 93);
	local questID=93;
end

function mq05_093_review_the_current_time_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq05_093_review_the_current_time_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq05_093_review_the_current_time_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,93, 2);
				api_quest_SetJournalStep( pRoom, userObjID,93, 1);
				api_quest_SetQuestStep( pRoom, userObjID,93, 1);
				npc_talk_index = "n391_academic_station-1";
end

</GameServer>