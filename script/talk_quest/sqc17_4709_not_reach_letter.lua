<VillageServer>

function sqc17_4709_not_reach_letter_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1836 then
		sqc17_4709_not_reach_letter_OnTalk_n1836_sarah(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1879 then
		sqc17_4709_not_reach_letter_OnTalk_n1879_injured_youth(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1816 then
		sqc17_4709_not_reach_letter_OnTalk_n1816_vernicka(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1836_sarah--------------------------------------------------------------------------------
function sqc17_4709_not_reach_letter_OnTalk_n1836_sarah(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1836_sarah-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1836_sarah-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1836_sarah-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1836_sarah-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47090, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47090, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47090, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47090, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47090, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47090, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47090, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47090, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47090, false);
			 end 

	end
	if npc_talk_index == "n1836_sarah-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4709, 1);
				api_quest_SetJournalStep(userObjID,4709, 1);
				api_quest_SetQuestStep(userObjID,4709, 1);
				npc_talk_index = "n1836_sarah-1";

	end
	if npc_talk_index == "n1836_sarah-1-a" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400487, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400487, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1836_sarah-1-b" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400491, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400491, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1836_sarah-1-c" then 
	end
	if npc_talk_index == "n1836_sarah-1-d" then 
	end
	if npc_talk_index == "n1836_sarah-1-e" then 
	end
	if npc_talk_index == "n1836_sarah-1-f" then 
	end
	if npc_talk_index == "n1836_sarah-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1879_injured_youth--------------------------------------------------------------------------------
function sqc17_4709_not_reach_letter_OnTalk_n1879_injured_youth(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1879_injured_youth-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1879_injured_youth-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1879_injured_youth-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1879_injured_youth-2-a" then 

				if api_quest_HasQuestItem(userObjID, 400487, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400487, api_quest_HasQuestItem(userObjID, 400487, 1));
				end
	end
	if npc_talk_index == "n1879_injured_youth-2-b" then 
	end
	if npc_talk_index == "n1879_injured_youth-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1816_vernicka--------------------------------------------------------------------------------
function sqc17_4709_not_reach_letter_OnTalk_n1816_vernicka(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1816_vernicka-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1816_vernicka-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1816_vernicka-3-a" then 

				if api_quest_HasQuestItem(userObjID, 400491, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400491, api_quest_HasQuestItem(userObjID, 400491, 1));
				end
	end
	if npc_talk_index == "n1816_vernicka-3-b" then 
	end
	if npc_talk_index == "n1816_vernicka-3-c" then 
	end
	if npc_talk_index == "n1816_vernicka-3-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47090, true);
				 api_quest_RewardQuestUser(userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47090, true);
				 api_quest_RewardQuestUser(userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47090, true);
				 api_quest_RewardQuestUser(userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47090, true);
				 api_quest_RewardQuestUser(userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47090, true);
				 api_quest_RewardQuestUser(userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47090, true);
				 api_quest_RewardQuestUser(userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47090, true);
				 api_quest_RewardQuestUser(userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47090, true);
				 api_quest_RewardQuestUser(userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47090, true);
				 api_quest_RewardQuestUser(userObjID, 47090, questID, 1);
			 end 
	end
	if npc_talk_index == "n1816_vernicka-3-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4709_not_reach_letter_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4709);
end

function sqc17_4709_not_reach_letter_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4709);
end

function sqc17_4709_not_reach_letter_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4709);
	local questID=4709;
end

function sqc17_4709_not_reach_letter_OnRemoteStart( userObjID, questID )
end

function sqc17_4709_not_reach_letter_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc17_4709_not_reach_letter_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4709, 1);
				api_quest_SetJournalStep(userObjID,4709, 1);
				api_quest_SetQuestStep(userObjID,4709, 1);
				npc_talk_index = "n1836_sarah-1";
end

</VillageServer>

<GameServer>
function sqc17_4709_not_reach_letter_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1836 then
		sqc17_4709_not_reach_letter_OnTalk_n1836_sarah( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1879 then
		sqc17_4709_not_reach_letter_OnTalk_n1879_injured_youth( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1816 then
		sqc17_4709_not_reach_letter_OnTalk_n1816_vernicka( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1836_sarah--------------------------------------------------------------------------------
function sqc17_4709_not_reach_letter_OnTalk_n1836_sarah( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1836_sarah-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1836_sarah-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1836_sarah-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1836_sarah-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, false);
			 end 

	end
	if npc_talk_index == "n1836_sarah-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4709, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4709, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4709, 1);
				npc_talk_index = "n1836_sarah-1";

	end
	if npc_talk_index == "n1836_sarah-1-a" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400487, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400487, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1836_sarah-1-b" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400491, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400491, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n1836_sarah-1-c" then 
	end
	if npc_talk_index == "n1836_sarah-1-d" then 
	end
	if npc_talk_index == "n1836_sarah-1-e" then 
	end
	if npc_talk_index == "n1836_sarah-1-f" then 
	end
	if npc_talk_index == "n1836_sarah-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1879_injured_youth--------------------------------------------------------------------------------
function sqc17_4709_not_reach_letter_OnTalk_n1879_injured_youth( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1879_injured_youth-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1879_injured_youth-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1879_injured_youth-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1879_injured_youth-2-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400487, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400487, api_quest_HasQuestItem( pRoom, userObjID, 400487, 1));
				end
	end
	if npc_talk_index == "n1879_injured_youth-2-b" then 
	end
	if npc_talk_index == "n1879_injured_youth-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1816_vernicka--------------------------------------------------------------------------------
function sqc17_4709_not_reach_letter_OnTalk_n1816_vernicka( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1816_vernicka-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1816_vernicka-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1816_vernicka-3-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400491, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400491, api_quest_HasQuestItem( pRoom, userObjID, 400491, 1));
				end
	end
	if npc_talk_index == "n1816_vernicka-3-b" then 
	end
	if npc_talk_index == "n1816_vernicka-3-c" then 
	end
	if npc_talk_index == "n1816_vernicka-3-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47090, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47090, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47090, questID, 1);
			 end 
	end
	if npc_talk_index == "n1816_vernicka-3-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4709_not_reach_letter_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4709);
end

function sqc17_4709_not_reach_letter_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4709);
end

function sqc17_4709_not_reach_letter_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4709);
	local questID=4709;
end

function sqc17_4709_not_reach_letter_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc17_4709_not_reach_letter_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc17_4709_not_reach_letter_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4709, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4709, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4709, 1);
				npc_talk_index = "n1836_sarah-1";
end

</GameServer>