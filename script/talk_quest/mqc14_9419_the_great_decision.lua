<VillageServer>

function mqc14_9419_the_great_decision_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1528 then
		mqc14_9419_the_great_decision_OnTalk_n1528_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9419_the_great_decision_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1532 then
		mqc14_9419_the_great_decision_OnTalk_n1532_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1533 then
		mqc14_9419_the_great_decision_OnTalk_n1533_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1526 then
		mqc14_9419_the_great_decision_OnTalk_n1526_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1528_for_memory--------------------------------------------------------------------------------
function mqc14_9419_the_great_decision_OnTalk_n1528_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1528_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9419_the_great_decision_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9419, 2);
				api_quest_SetJournalStep(userObjID,9419, 1);
				api_quest_SetQuestStep(userObjID,9419, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1532_gaharam--------------------------------------------------------------------------------
function mqc14_9419_the_great_decision_OnTalk_n1532_gaharam(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1532_gaharam-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1532_gaharam-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1532_gaharam-1-b" then 
	end
	if npc_talk_index == "n1532_gaharam-1-c" then 
	end
	if npc_talk_index == "n1532_gaharam-1-d" then 
	end
	if npc_talk_index == "n1532_gaharam-1-e" then 
	end
	if npc_talk_index == "n1532_gaharam-1-f" then 
	end
	if npc_talk_index == "n1532_gaharam-1-g" then 
	end
	if npc_talk_index == "n1532_gaharam-1-h" then 
	end
	if npc_talk_index == "n1532_gaharam-1-i" then 
	end
	if npc_talk_index == "n1532_gaharam-1-j" then 
	end
	if npc_talk_index == "n1532_gaharam-1-k" then 
	end
	if npc_talk_index == "n1532_gaharam-1-l" then 
	end
	if npc_talk_index == "n1532_gaharam-1-m" then 
	end
	if npc_talk_index == "n1532_gaharam-1-n" then 
	end
	if npc_talk_index == "n1532_gaharam-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1533_pether--------------------------------------------------------------------------------
function mqc14_9419_the_great_decision_OnTalk_n1533_pether(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1533_pether-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1533_pether-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1533_pether-3-b" then 
	end
	if npc_talk_index == "n1533_pether-3-c" then 
	end
	if npc_talk_index == "n1533_pether-3-d" then 
	end
	if npc_talk_index == "n1533_pether-3-e" then 
	end
	if npc_talk_index == "n1533_pether-3-f" then 
	end
	if npc_talk_index == "n1533_pether-3-g" then 
	end
	if npc_talk_index == "n1533_pether-3-h" then 
	end
	if npc_talk_index == "n1533_pether-3-i" then 
	end
	if npc_talk_index == "n1533_pether-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1526_for_memory--------------------------------------------------------------------------------
function mqc14_9419_the_great_decision_OnTalk_n1526_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1526_for_memory-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1526_for_memory-5-chk_class05" then 
				if api_user_GetUserClassID(userObjID) == 6 then
									npc_talk_index = "n1526_for_memory-5-c";

				else
									npc_talk_index = "n1526_for_memory-5-b";

				end
	end
	if npc_talk_index == "n1526_for_memory-5-d" then 
	end
	if npc_talk_index == "n1526_for_memory-5-d" then 
	end
	if npc_talk_index == "n1526_for_memory-5-e" then 
	end
	if npc_talk_index == "n1526_for_memory-5-f" then 
	end
	if npc_talk_index == "n1526_for_memory-5-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94191, true);
				 api_quest_RewardQuestUser(userObjID, 94191, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94192, true);
				 api_quest_RewardQuestUser(userObjID, 94192, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94193, true);
				 api_quest_RewardQuestUser(userObjID, 94193, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94194, true);
				 api_quest_RewardQuestUser(userObjID, 94194, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94195, true);
				 api_quest_RewardQuestUser(userObjID, 94195, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94196, true);
				 api_quest_RewardQuestUser(userObjID, 94196, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94197, true);
				 api_quest_RewardQuestUser(userObjID, 94197, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94198, true);
				 api_quest_RewardQuestUser(userObjID, 94198, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94199, true);
				 api_quest_RewardQuestUser(userObjID, 94199, questID, 1);
			 end 
	end
	if npc_talk_index == "n1526_for_memory-5-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9420, 2);
					api_quest_SetQuestStep(userObjID, 9420, 1);
					api_quest_SetJournalStep(userObjID, 9420, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1526_for_memory-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1526_for_memory-1", "mqc14_9420_the_man_of_worry.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9419_the_great_decision_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9419);
end

function mqc14_9419_the_great_decision_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9419);
end

function mqc14_9419_the_great_decision_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9419);
	local questID=9419;
end

function mqc14_9419_the_great_decision_OnRemoteStart( userObjID, questID )
end

function mqc14_9419_the_great_decision_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9419_the_great_decision_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9419, 2);
				api_quest_SetJournalStep(userObjID,9419, 1);
				api_quest_SetQuestStep(userObjID,9419, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</VillageServer>

<GameServer>
function mqc14_9419_the_great_decision_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1528 then
		mqc14_9419_the_great_decision_OnTalk_n1528_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9419_the_great_decision_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1532 then
		mqc14_9419_the_great_decision_OnTalk_n1532_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1533 then
		mqc14_9419_the_great_decision_OnTalk_n1533_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1526 then
		mqc14_9419_the_great_decision_OnTalk_n1526_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1528_for_memory--------------------------------------------------------------------------------
function mqc14_9419_the_great_decision_OnTalk_n1528_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1528_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9419_the_great_decision_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9419, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9419, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9419, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1532_gaharam--------------------------------------------------------------------------------
function mqc14_9419_the_great_decision_OnTalk_n1532_gaharam( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1532_gaharam-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1532_gaharam-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1532_gaharam-1-b" then 
	end
	if npc_talk_index == "n1532_gaharam-1-c" then 
	end
	if npc_talk_index == "n1532_gaharam-1-d" then 
	end
	if npc_talk_index == "n1532_gaharam-1-e" then 
	end
	if npc_talk_index == "n1532_gaharam-1-f" then 
	end
	if npc_talk_index == "n1532_gaharam-1-g" then 
	end
	if npc_talk_index == "n1532_gaharam-1-h" then 
	end
	if npc_talk_index == "n1532_gaharam-1-i" then 
	end
	if npc_talk_index == "n1532_gaharam-1-j" then 
	end
	if npc_talk_index == "n1532_gaharam-1-k" then 
	end
	if npc_talk_index == "n1532_gaharam-1-l" then 
	end
	if npc_talk_index == "n1532_gaharam-1-m" then 
	end
	if npc_talk_index == "n1532_gaharam-1-n" then 
	end
	if npc_talk_index == "n1532_gaharam-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1533_pether--------------------------------------------------------------------------------
function mqc14_9419_the_great_decision_OnTalk_n1533_pether( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1533_pether-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1533_pether-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1533_pether-3-b" then 
	end
	if npc_talk_index == "n1533_pether-3-c" then 
	end
	if npc_talk_index == "n1533_pether-3-d" then 
	end
	if npc_talk_index == "n1533_pether-3-e" then 
	end
	if npc_talk_index == "n1533_pether-3-f" then 
	end
	if npc_talk_index == "n1533_pether-3-g" then 
	end
	if npc_talk_index == "n1533_pether-3-h" then 
	end
	if npc_talk_index == "n1533_pether-3-i" then 
	end
	if npc_talk_index == "n1533_pether-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1526_for_memory--------------------------------------------------------------------------------
function mqc14_9419_the_great_decision_OnTalk_n1526_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1526_for_memory-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1526_for_memory-5-chk_class05" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									npc_talk_index = "n1526_for_memory-5-c";

				else
									npc_talk_index = "n1526_for_memory-5-b";

				end
	end
	if npc_talk_index == "n1526_for_memory-5-d" then 
	end
	if npc_talk_index == "n1526_for_memory-5-d" then 
	end
	if npc_talk_index == "n1526_for_memory-5-e" then 
	end
	if npc_talk_index == "n1526_for_memory-5-f" then 
	end
	if npc_talk_index == "n1526_for_memory-5-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94191, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94191, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94192, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94192, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94193, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94193, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94194, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94194, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94195, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94195, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94196, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94196, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94197, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94197, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94198, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94198, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94199, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94199, questID, 1);
			 end 
	end
	if npc_talk_index == "n1526_for_memory-5-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9420, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9420, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9420, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1526_for_memory-5-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1526_for_memory-1", "mqc14_9420_the_man_of_worry.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9419_the_great_decision_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9419);
end

function mqc14_9419_the_great_decision_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9419);
end

function mqc14_9419_the_great_decision_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9419);
	local questID=9419;
end

function mqc14_9419_the_great_decision_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9419_the_great_decision_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9419_the_great_decision_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9419, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9419, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9419, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</GameServer>