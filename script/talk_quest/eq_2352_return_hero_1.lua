<VillageServer>

function eq_2352_return_hero_1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1635 then
		eq_2352_return_hero_1_OnTalk_n1635_return_hero(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1635_return_hero--------------------------------------------------------------------------------
function eq_2352_return_hero_1_OnTalk_n1635_return_hero(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1635_return_hero-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1635_return_hero-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1635_return_hero-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23520, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23520, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23520, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23520, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23520, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23520, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23520, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 23520, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 23520, false);
			 end 

	end
	if npc_talk_index == "n1635_return_hero-accepting-acceptted" then
				api_quest_AddQuest(userObjID,2352, 1);
				api_quest_SetJournalStep(userObjID,2352, 1);
				api_quest_SetQuestStep(userObjID,2352, 1);
				npc_talk_index = "n1635_return_hero-1";

	end
	if npc_talk_index == "n1635_return_hero-1-b" then 
				api_quest_SkipMainQuest(userObjID,-1);
	end
	if npc_talk_index == "n1635_return_hero-1-c" then 
	end
	if npc_talk_index == "n1635_return_hero-1-end1" then 
				npc_talk_index = "n1635_return_hero-1-d";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23520, true);
				 api_quest_RewardQuestUser(userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23520, true);
				 api_quest_RewardQuestUser(userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23520, true);
				 api_quest_RewardQuestUser(userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23520, true);
				 api_quest_RewardQuestUser(userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23520, true);
				 api_quest_RewardQuestUser(userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23520, true);
				 api_quest_RewardQuestUser(userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23520, true);
				 api_quest_RewardQuestUser(userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 23520, true);
				 api_quest_RewardQuestUser(userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 23520, true);
				 api_quest_RewardQuestUser(userObjID, 23520, questID, 1);
			 end 
	end
	if npc_talk_index == "n1635_return_hero-1-end2" then 
				npc_talk_index = "n1635_return_hero-1-d";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 23521, true);
				 api_quest_RewardQuestUser(userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 23521, true);
				 api_quest_RewardQuestUser(userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 23521, true);
				 api_quest_RewardQuestUser(userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 23521, true);
				 api_quest_RewardQuestUser(userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 23521, true);
				 api_quest_RewardQuestUser(userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 23521, true);
				 api_quest_RewardQuestUser(userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 23521, true);
				 api_quest_RewardQuestUser(userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 23521, true);
				 api_quest_RewardQuestUser(userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 23521, true);
				 api_quest_RewardQuestUser(userObjID, 23521, questID, 1);
			 end 
	end
	if npc_talk_index == "n1635_return_hero-1-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 2353, 1);
					api_quest_SetQuestStep(userObjID, 2353, 1);
					api_quest_SetJournalStep(userObjID, 2353, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1635_return_hero-1-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1635_return_hero-1", "eq_2353_return_hero_2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2352_return_hero_1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2352);
end

function eq_2352_return_hero_1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2352);
end

function eq_2352_return_hero_1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 2352);
	local questID=2352;
end

function eq_2352_return_hero_1_OnRemoteStart( userObjID, questID )
end

function eq_2352_return_hero_1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function eq_2352_return_hero_1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,2352, 1);
				api_quest_SetJournalStep(userObjID,2352, 1);
				api_quest_SetQuestStep(userObjID,2352, 1);
				npc_talk_index = "n1635_return_hero-1";
end

</VillageServer>

<GameServer>
function eq_2352_return_hero_1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1635 then
		eq_2352_return_hero_1_OnTalk_n1635_return_hero( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1635_return_hero--------------------------------------------------------------------------------
function eq_2352_return_hero_1_OnTalk_n1635_return_hero( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1635_return_hero-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1635_return_hero-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1635_return_hero-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, false);
			 end 

	end
	if npc_talk_index == "n1635_return_hero-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,2352, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2352, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2352, 1);
				npc_talk_index = "n1635_return_hero-1";

	end
	if npc_talk_index == "n1635_return_hero-1-b" then 
				api_quest_SkipMainQuest( pRoom, userObjID,-1);
	end
	if npc_talk_index == "n1635_return_hero-1-c" then 
	end
	if npc_talk_index == "n1635_return_hero-1-end1" then 
				npc_talk_index = "n1635_return_hero-1-d";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23520, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23520, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23520, questID, 1);
			 end 
	end
	if npc_talk_index == "n1635_return_hero-1-end2" then 
				npc_talk_index = "n1635_return_hero-1-d";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23521, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 23521, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 23521, questID, 1);
			 end 
	end
	if npc_talk_index == "n1635_return_hero-1-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 2353, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 2353, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 2353, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1635_return_hero-1-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1635_return_hero-1", "eq_2353_return_hero_2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function eq_2352_return_hero_1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2352);
end

function eq_2352_return_hero_1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2352);
end

function eq_2352_return_hero_1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 2352);
	local questID=2352;
end

function eq_2352_return_hero_1_OnRemoteStart( pRoom,  userObjID, questID )
end

function eq_2352_return_hero_1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function eq_2352_return_hero_1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,2352, 1);
				api_quest_SetJournalStep( pRoom, userObjID,2352, 1);
				api_quest_SetQuestStep( pRoom, userObjID,2352, 1);
				npc_talk_index = "n1635_return_hero-1";
end

</GameServer>