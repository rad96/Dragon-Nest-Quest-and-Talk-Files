<VillageServer>

function sq15_848_unexpected_effect_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 713 then
		sq15_848_unexpected_effect_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n713_soceress_tamara--------------------------------------------------------------------------------
function sq15_848_unexpected_effect_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n713_soceress_tamara-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n713_soceress_tamara-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n713_soceress_tamara-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8480, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8480, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8480, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8480, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8480, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8480, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8480, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8480, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8480, false);
			 end 

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted!" then
				api_quest_AddQuest(userObjID,848, 1);
				api_quest_SetJournalStep(userObjID,848, 1);
				api_quest_SetQuestStep(userObjID,848, 1);
				npc_talk_index = "n713_soceress_tamara-1";

	end
	if npc_talk_index == "n713_soceress_tamara-1-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-c" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-d" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-e" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-f" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-g" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-h" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-i" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-j" then 
	end
	if npc_talk_index == "n713_soceress_tamara-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 1576, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 201576, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300475, 1);
	end
	if npc_talk_index == "n713_soceress_tamara-3-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-3-c" then 

				if api_quest_HasQuestItem(userObjID, 300475, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300475, api_quest_HasQuestItem(userObjID, 300475, 1));
				end
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 8480, true);
				 api_quest_RewardQuestUser(userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 8480, true);
				 api_quest_RewardQuestUser(userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 8480, true);
				 api_quest_RewardQuestUser(userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 8480, true);
				 api_quest_RewardQuestUser(userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 8480, true);
				 api_quest_RewardQuestUser(userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 8480, true);
				 api_quest_RewardQuestUser(userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 8480, true);
				 api_quest_RewardQuestUser(userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 8480, true);
				 api_quest_RewardQuestUser(userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 8480, true);
				 api_quest_RewardQuestUser(userObjID, 8480, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-3-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_848_unexpected_effect_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 848);
	if qstep == 2 and CountIndex == 1576 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300475, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300475, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201576 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300475, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300475, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300475 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function sq15_848_unexpected_effect_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 848);
	if qstep == 2 and CountIndex == 1576 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201576 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300475 and Count >= TargetCount  then

	end
end

function sq15_848_unexpected_effect_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 848);
	local questID=848;
end

function sq15_848_unexpected_effect_OnRemoteStart( userObjID, questID )
end

function sq15_848_unexpected_effect_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_848_unexpected_effect_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq15_848_unexpected_effect_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 713 then
		sq15_848_unexpected_effect_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n713_soceress_tamara--------------------------------------------------------------------------------
function sq15_848_unexpected_effect_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n713_soceress_tamara-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n713_soceress_tamara-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n713_soceress_tamara-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, false);
			 end 

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-acceptted!" then
				api_quest_AddQuest( pRoom, userObjID,848, 1);
				api_quest_SetJournalStep( pRoom, userObjID,848, 1);
				api_quest_SetQuestStep( pRoom, userObjID,848, 1);
				npc_talk_index = "n713_soceress_tamara-1";

	end
	if npc_talk_index == "n713_soceress_tamara-1-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-c" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-d" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-e" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-f" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-g" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-h" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-i" then 
	end
	if npc_talk_index == "n713_soceress_tamara-1-j" then 
	end
	if npc_talk_index == "n713_soceress_tamara-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 1576, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 201576, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300475, 1);
	end
	if npc_talk_index == "n713_soceress_tamara-3-b" then 
	end
	if npc_talk_index == "n713_soceress_tamara-3-c" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300475, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300475, api_quest_HasQuestItem( pRoom, userObjID, 300475, 1));
				end
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 8480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 8480, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-3-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_848_unexpected_effect_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 848);
	if qstep == 2 and CountIndex == 1576 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300475, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300475, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 201576 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300475, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300475, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300475 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function sq15_848_unexpected_effect_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 848);
	if qstep == 2 and CountIndex == 1576 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201576 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300475 and Count >= TargetCount  then

	end
end

function sq15_848_unexpected_effect_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 848);
	local questID=848;
end

function sq15_848_unexpected_effect_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_848_unexpected_effect_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_848_unexpected_effect_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>