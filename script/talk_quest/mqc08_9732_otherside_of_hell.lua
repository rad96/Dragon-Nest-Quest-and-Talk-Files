<VillageServer>

function mqc08_9732_otherside_of_hell_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 707 then
		mqc08_9732_otherside_of_hell_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mqc08_9732_otherside_of_hell_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9732, 2);
				api_quest_SetJournalStep(userObjID,9732, 1);
				api_quest_SetQuestStep(userObjID,9732, 1);
				npc_talk_index = "n707_sidel-1";

	end
	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-1-d" then 
	end
	if npc_talk_index == "n707_sidel-1-e" then 
	end
	if npc_talk_index == "n707_sidel-1-f" then 
	end
	if npc_talk_index == "n707_sidel-1-g" then 
	end
	if npc_talk_index == "n707_sidel-1-h" then 
	end
	if npc_talk_index == "n707_sidel-1-i" then 
	end
	if npc_talk_index == "n707_sidel-1-j" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 505401, 1);
	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-3-h" then 
	end
	if npc_talk_index == "n707_sidel-3-i" then 
	end
	if npc_talk_index == "n707_sidel-3-j" then 
	end
	if npc_talk_index == "n707_sidel-3-k" then 
	end
	if npc_talk_index == "n707_sidel-3-l" then 
	end
	if npc_talk_index == "n707_sidel-3-m" then 
	end
	if npc_talk_index == "n707_sidel-3-n" then 
	end
	if npc_talk_index == "n707_sidel-3-o" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97320, true);
				 api_quest_RewardQuestUser(userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97320, true);
				 api_quest_RewardQuestUser(userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97320, true);
				 api_quest_RewardQuestUser(userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97320, true);
				 api_quest_RewardQuestUser(userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97320, true);
				 api_quest_RewardQuestUser(userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97320, true);
				 api_quest_RewardQuestUser(userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97320, true);
				 api_quest_RewardQuestUser(userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97320, true);
				 api_quest_RewardQuestUser(userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97320, true);
				 api_quest_RewardQuestUser(userObjID, 97320, questID, 1);
			 end 
	end
	if npc_talk_index == "n707_sidel-3-p" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									if api_quest_IsMarkingCompleteQuest(userObjID, 9210) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 9363) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 9530) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest(userObjID, 9644) == 1 then
				else
									if api_user_GetUserClassID(userObjID) == 1 then
									api_quest_AddQuest(userObjID,9210, 2);
				api_quest_SetJournalStep(userObjID,9210, 1);
				api_quest_SetQuestStep(userObjID,9210, 1);

				else
									if api_user_GetUserClassID(userObjID) == 2 then
									api_quest_AddQuest(userObjID,9210, 2);
				api_quest_SetJournalStep(userObjID,9210, 1);
				api_quest_SetQuestStep(userObjID,9210, 1);

				else
									if api_user_GetUserClassID(userObjID) == 3 then
									api_quest_AddQuest(userObjID,9210, 2);
				api_quest_SetJournalStep(userObjID,9210, 1);
				api_quest_SetQuestStep(userObjID,9210, 1);

				else
									if api_user_GetUserClassID(userObjID) == 4 then
									api_quest_AddQuest(userObjID,9210, 2);
				api_quest_SetJournalStep(userObjID,9210, 1);
				api_quest_SetQuestStep(userObjID,9210, 1);

				else
									if api_user_GetUserClassID(userObjID) == 5 then
									api_quest_AddQuest(userObjID,9210, 2);
				api_quest_SetJournalStep(userObjID,9210, 1);
				api_quest_SetQuestStep(userObjID,9210, 1);

				else
									if api_user_GetUserClassID(userObjID) == 6 then
									api_quest_AddQuest(userObjID,9210, 2);
				api_quest_SetJournalStep(userObjID,9210, 1);
				api_quest_SetQuestStep(userObjID,9210, 1);

				else
									if api_user_GetUserClassID(userObjID) == 7 then
									api_quest_AddQuest(userObjID,9363, 2);
				api_quest_SetJournalStep(userObjID,9363, 1);
				api_quest_SetQuestStep(userObjID,9363, 1);

				else
									if api_user_GetUserClassID(userObjID) == 8 then
									api_quest_AddQuest(userObjID,9530, 2);
				api_quest_SetJournalStep(userObjID,9530, 1);
				api_quest_SetQuestStep(userObjID,9530, 1);

				else
									if api_user_GetUserClassID(userObjID) == 9 then
									api_quest_AddQuest(userObjID,9644, 2);
				api_quest_SetJournalStep(userObjID,9644, 1);
				api_quest_SetQuestStep(userObjID,9644, 1);

				else
									api_quest_AddQuest(userObjID,9644, 2);
				api_quest_SetJournalStep(userObjID,9644, 1);
				api_quest_SetQuestStep(userObjID,9644, 1);

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n707_sidel-3-q" then 
	end
	if npc_talk_index == "n707_sidel-3-r" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc08_9732_otherside_of_hell_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9732);
	if qstep == 2 and CountIndex == 505401 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function mqc08_9732_otherside_of_hell_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9732);
	if qstep == 2 and CountIndex == 505401 and Count >= TargetCount  then

	end
end

function mqc08_9732_otherside_of_hell_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9732);
	local questID=9732;
end

function mqc08_9732_otherside_of_hell_OnRemoteStart( userObjID, questID )
end

function mqc08_9732_otherside_of_hell_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc08_9732_otherside_of_hell_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9732, 2);
				api_quest_SetJournalStep(userObjID,9732, 1);
				api_quest_SetQuestStep(userObjID,9732, 1);
				npc_talk_index = "n707_sidel-1";
end

</VillageServer>

<GameServer>
function mqc08_9732_otherside_of_hell_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 707 then
		mqc08_9732_otherside_of_hell_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n707_sidel--------------------------------------------------------------------------------
function mqc08_9732_otherside_of_hell_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n707_sidel-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9732, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9732, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9732, 1);
				npc_talk_index = "n707_sidel-1";

	end
	if npc_talk_index == "n707_sidel-1-b" then 
	end
	if npc_talk_index == "n707_sidel-1-c" then 
	end
	if npc_talk_index == "n707_sidel-1-d" then 
	end
	if npc_talk_index == "n707_sidel-1-e" then 
	end
	if npc_talk_index == "n707_sidel-1-f" then 
	end
	if npc_talk_index == "n707_sidel-1-g" then 
	end
	if npc_talk_index == "n707_sidel-1-h" then 
	end
	if npc_talk_index == "n707_sidel-1-i" then 
	end
	if npc_talk_index == "n707_sidel-1-j" then 
	end
	if npc_talk_index == "n707_sidel-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 505401, 1);
	end
	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-3-h" then 
	end
	if npc_talk_index == "n707_sidel-3-i" then 
	end
	if npc_talk_index == "n707_sidel-3-j" then 
	end
	if npc_talk_index == "n707_sidel-3-k" then 
	end
	if npc_talk_index == "n707_sidel-3-l" then 
	end
	if npc_talk_index == "n707_sidel-3-m" then 
	end
	if npc_talk_index == "n707_sidel-3-n" then 
	end
	if npc_talk_index == "n707_sidel-3-o" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97320, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97320, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97320, questID, 1);
			 end 
	end
	if npc_talk_index == "n707_sidel-3-p" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9210) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9363) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9530) == 1 then
				else
									if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 9644) == 1 then
				else
									if api_user_GetUserClassID( pRoom, userObjID) == 1 then
									api_quest_AddQuest( pRoom, userObjID,9210, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9210, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9210, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									api_quest_AddQuest( pRoom, userObjID,9210, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9210, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9210, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									api_quest_AddQuest( pRoom, userObjID,9210, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9210, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9210, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 4 then
									api_quest_AddQuest( pRoom, userObjID,9210, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9210, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9210, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 5 then
									api_quest_AddQuest( pRoom, userObjID,9210, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9210, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9210, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 6 then
									api_quest_AddQuest( pRoom, userObjID,9210, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9210, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9210, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 7 then
									api_quest_AddQuest( pRoom, userObjID,9363, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9363, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9363, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 8 then
									api_quest_AddQuest( pRoom, userObjID,9530, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9530, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9530, 1);

				else
									if api_user_GetUserClassID( pRoom, userObjID) == 9 then
									api_quest_AddQuest( pRoom, userObjID,9644, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9644, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9644, 1);

				else
									api_quest_AddQuest( pRoom, userObjID,9644, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9644, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9644, 1);

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end

				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n707_sidel-3-q" then 
	end
	if npc_talk_index == "n707_sidel-3-r" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc08_9732_otherside_of_hell_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9732);
	if qstep == 2 and CountIndex == 505401 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function mqc08_9732_otherside_of_hell_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9732);
	if qstep == 2 and CountIndex == 505401 and Count >= TargetCount  then

	end
end

function mqc08_9732_otherside_of_hell_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9732);
	local questID=9732;
end

function mqc08_9732_otherside_of_hell_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc08_9732_otherside_of_hell_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc08_9732_otherside_of_hell_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9732, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9732, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9732, 1);
				npc_talk_index = "n707_sidel-1";
end

</GameServer>