<VillageServer>

function sq15_4188_road_to_home_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 775 then
		sq15_4188_road_to_home_OnTalk_n775_cleric_ilizer(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 886 then
		sq15_4188_road_to_home_OnTalk_n886_lost_boy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n775_cleric_ilizer--------------------------------------------------------------------------------
function sq15_4188_road_to_home_OnTalk_n775_cleric_ilizer(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n775_cleric_ilizer-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n775_cleric_ilizer-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n775_cleric_ilizer-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 end 

	end
	if npc_talk_index == "n775_cleric_ilizer-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4188, 1);
				api_quest_SetJournalStep(userObjID,4188, 1);
				api_quest_SetQuestStep(userObjID,4188, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 14196, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 400368, 1);
				npc_talk_index = "n775_cleric_ilizer-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n886_lost_boy--------------------------------------------------------------------------------
function sq15_4188_road_to_home_OnTalk_n886_lost_boy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n886_lost_boy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n886_lost_boy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n886_lost_boy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n886_lost_boy-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41880, false);
			 end 

	end
	if npc_talk_index == "n886_lost_boy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4188, 1);
				api_quest_SetJournalStep(userObjID,4188, 1);
				api_quest_SetQuestStep(userObjID,4188, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 5, 14196, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 400368, 1);
				npc_talk_index = "n886_lost_boy-1";

	end
	if npc_talk_index == "n886_lost_boy-2-a" then 

				if api_quest_HasQuestItem(userObjID, 400368, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400368, api_quest_HasQuestItem(userObjID, 400368, 1));
				end
	end
	if npc_talk_index == "n886_lost_boy-2-b" then 
	end
	if npc_talk_index == "n886_lost_boy-2-c" then 
	end
	if npc_talk_index == "n886_lost_boy-2-d" then 
	end
	if npc_talk_index == "n886_lost_boy-2-e" then 
	end
	if npc_talk_index == "n886_lost_boy-2-e" then 
	end
	if npc_talk_index == "n886_lost_boy-2-e" then 
	end
	if npc_talk_index == "n886_lost_boy-2-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 41880, true);
				 api_quest_RewardQuestUser(userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 41880, true);
				 api_quest_RewardQuestUser(userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 41880, true);
				 api_quest_RewardQuestUser(userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 41880, true);
				 api_quest_RewardQuestUser(userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 41880, true);
				 api_quest_RewardQuestUser(userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 41880, true);
				 api_quest_RewardQuestUser(userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 41880, true);
				 api_quest_RewardQuestUser(userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 41880, true);
				 api_quest_RewardQuestUser(userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 41880, true);
				 api_quest_RewardQuestUser(userObjID, 41880, questID, 1);
			 end 
	end
	if npc_talk_index == "n886_lost_boy-2-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4188_road_to_home_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4188);
	if qstep == 1 and CountIndex == 236132 then

	end
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400368, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400368, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
		 end 
	 end 
	if qstep == 1 and CountIndex == 400368 then

	end
end

function sq15_4188_road_to_home_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4188);
	if qstep == 1 and CountIndex == 236132 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400368 and Count >= TargetCount  then

	end
end

function sq15_4188_road_to_home_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4188);
	local questID=4188;
end

function sq15_4188_road_to_home_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 14196, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 400368, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 5, 14196, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 400368, 1);
end

function sq15_4188_road_to_home_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_4188_road_to_home_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4188, 1);
				api_quest_SetJournalStep(userObjID,4188, 1);
				api_quest_SetQuestStep(userObjID,4188, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 5, 14196, 1 );
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 400368, 1);
				npc_talk_index = "n886_lost_boy-1";
end

</VillageServer>

<GameServer>
function sq15_4188_road_to_home_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 775 then
		sq15_4188_road_to_home_OnTalk_n775_cleric_ilizer( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 886 then
		sq15_4188_road_to_home_OnTalk_n886_lost_boy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n775_cleric_ilizer--------------------------------------------------------------------------------
function sq15_4188_road_to_home_OnTalk_n775_cleric_ilizer( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n775_cleric_ilizer-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n775_cleric_ilizer-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n775_cleric_ilizer-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 end 

	end
	if npc_talk_index == "n775_cleric_ilizer-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4188, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4188, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4188, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 14196, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 400368, 1);
				npc_talk_index = "n775_cleric_ilizer-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n886_lost_boy--------------------------------------------------------------------------------
function sq15_4188_road_to_home_OnTalk_n886_lost_boy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n886_lost_boy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n886_lost_boy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n886_lost_boy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n886_lost_boy-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, false);
			 end 

	end
	if npc_talk_index == "n886_lost_boy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4188, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4188, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4188, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 5, 14196, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 400368, 1);
				npc_talk_index = "n886_lost_boy-1";

	end
	if npc_talk_index == "n886_lost_boy-2-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400368, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400368, api_quest_HasQuestItem( pRoom, userObjID, 400368, 1));
				end
	end
	if npc_talk_index == "n886_lost_boy-2-b" then 
	end
	if npc_talk_index == "n886_lost_boy-2-c" then 
	end
	if npc_talk_index == "n886_lost_boy-2-d" then 
	end
	if npc_talk_index == "n886_lost_boy-2-e" then 
	end
	if npc_talk_index == "n886_lost_boy-2-e" then 
	end
	if npc_talk_index == "n886_lost_boy-2-e" then 
	end
	if npc_talk_index == "n886_lost_boy-2-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 41880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 41880, questID, 1);
			 end 
	end
	if npc_talk_index == "n886_lost_boy-2-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_4188_road_to_home_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4188);
	if qstep == 1 and CountIndex == 236132 then

	end
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400368, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400368, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
		 end 
	 end 
	if qstep == 1 and CountIndex == 400368 then

	end
end

function sq15_4188_road_to_home_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4188);
	if qstep == 1 and CountIndex == 236132 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400368 and Count >= TargetCount  then

	end
end

function sq15_4188_road_to_home_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4188);
	local questID=4188;
end

function sq15_4188_road_to_home_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 14196, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 400368, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 5, 14196, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 400368, 1);
end

function sq15_4188_road_to_home_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_4188_road_to_home_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4188, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4188, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4188, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 5, 14196, 1 );
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 400368, 1);
				npc_talk_index = "n886_lost_boy-1";
end

</GameServer>