<VillageServer>

function sq15_748_what_am_i_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 394 then
		sq15_748_what_am_i_OnTalk_n394_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		sq15_748_what_am_i_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 796 then
		sq15_748_what_am_i_OnTalk_n796_academin_npc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n394_academic_station--------------------------------------------------------------------------------
function sq15_748_what_am_i_OnTalk_n394_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n394_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n394_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n394_academic_station-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n394_academic_station-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n394_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,748, 1);
				api_quest_SetJournalStep(userObjID,748, 1);
				api_quest_SetQuestStep(userObjID,748, 1);
				npc_talk_index = "n394_academic_station-1";

	end
	if npc_talk_index == "n394_academic_station-3-b" then 
	end
	if npc_talk_index == "n394_academic_station-3-c" then 
	end
	if npc_talk_index == "n394_academic_station-3-d" then 
	end
	if npc_talk_index == "n394_academic_station-3-e" then 
	end
	if npc_talk_index == "n394_academic_station-3-f" then 
	end
	if npc_talk_index == "n394_academic_station-3-g" then 
	end
	if npc_talk_index == "n394_academic_station-3-h" then 
	end
	if npc_talk_index == "n394_academic_station-3-i" then 
	end
	if npc_talk_index == "n394_academic_station-3-j" then 
	end
	if npc_talk_index == "n394_academic_station-3-k" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7480, true);
				 api_quest_RewardQuestUser(userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7480, true);
				 api_quest_RewardQuestUser(userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7480, true);
				 api_quest_RewardQuestUser(userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7480, true);
				 api_quest_RewardQuestUser(userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7480, true);
				 api_quest_RewardQuestUser(userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7480, true);
				 api_quest_RewardQuestUser(userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7480, true);
				 api_quest_RewardQuestUser(userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7480, true);
				 api_quest_RewardQuestUser(userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 7480, true);
				 api_quest_RewardQuestUser(userObjID, 7480, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-3-l" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 749, 1);
					api_quest_SetQuestStep(userObjID, 749, 1);
					api_quest_SetJournalStep(userObjID, 749, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function sq15_748_what_am_i_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n701_oldkarakule-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-2-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n796_academin_npc--------------------------------------------------------------------------------
function sq15_748_what_am_i_OnTalk_n796_academin_npc(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n796_academin_npc-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n796_academin_npc-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n796_academin_npc-1-b" then 
	end
	if npc_talk_index == "n796_academin_npc-1-c" then 
	end
	if npc_talk_index == "n796_academin_npc-1-d" then 
	end
	if npc_talk_index == "n796_academin_npc-1-e" then 
	end
	if npc_talk_index == "n796_academin_npc-1-f" then 
	end
	if npc_talk_index == "n796_academin_npc-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_748_what_am_i_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 748);
end

function sq15_748_what_am_i_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 748);
end

function sq15_748_what_am_i_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 748);
	local questID=748;
end

function sq15_748_what_am_i_OnRemoteStart( userObjID, questID )
end

function sq15_748_what_am_i_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq15_748_what_am_i_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,748, 1);
				api_quest_SetJournalStep(userObjID,748, 1);
				api_quest_SetQuestStep(userObjID,748, 1);
				npc_talk_index = "n394_academic_station-1";
end

</VillageServer>

<GameServer>
function sq15_748_what_am_i_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 394 then
		sq15_748_what_am_i_OnTalk_n394_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		sq15_748_what_am_i_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 796 then
		sq15_748_what_am_i_OnTalk_n796_academin_npc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n394_academic_station--------------------------------------------------------------------------------
function sq15_748_what_am_i_OnTalk_n394_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n394_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n394_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n394_academic_station-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n394_academic_station-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n394_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,748, 1);
				api_quest_SetJournalStep( pRoom, userObjID,748, 1);
				api_quest_SetQuestStep( pRoom, userObjID,748, 1);
				npc_talk_index = "n394_academic_station-1";

	end
	if npc_talk_index == "n394_academic_station-3-b" then 
	end
	if npc_talk_index == "n394_academic_station-3-c" then 
	end
	if npc_talk_index == "n394_academic_station-3-d" then 
	end
	if npc_talk_index == "n394_academic_station-3-e" then 
	end
	if npc_talk_index == "n394_academic_station-3-f" then 
	end
	if npc_talk_index == "n394_academic_station-3-g" then 
	end
	if npc_talk_index == "n394_academic_station-3-h" then 
	end
	if npc_talk_index == "n394_academic_station-3-i" then 
	end
	if npc_talk_index == "n394_academic_station-3-j" then 
	end
	if npc_talk_index == "n394_academic_station-3-k" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7480, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7480, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7480, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-3-l" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 749, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 749, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 749, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function sq15_748_what_am_i_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n701_oldkarakule-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-2-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-2-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n796_academin_npc--------------------------------------------------------------------------------
function sq15_748_what_am_i_OnTalk_n796_academin_npc( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n796_academin_npc-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n796_academin_npc-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n796_academin_npc-1-b" then 
	end
	if npc_talk_index == "n796_academin_npc-1-c" then 
	end
	if npc_talk_index == "n796_academin_npc-1-d" then 
	end
	if npc_talk_index == "n796_academin_npc-1-e" then 
	end
	if npc_talk_index == "n796_academin_npc-1-f" then 
	end
	if npc_talk_index == "n796_academin_npc-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq15_748_what_am_i_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 748);
end

function sq15_748_what_am_i_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 748);
end

function sq15_748_what_am_i_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 748);
	local questID=748;
end

function sq15_748_what_am_i_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq15_748_what_am_i_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq15_748_what_am_i_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,748, 1);
				api_quest_SetJournalStep( pRoom, userObjID,748, 1);
				api_quest_SetQuestStep( pRoom, userObjID,748, 1);
				npc_talk_index = "n394_academic_station-1";
end

</GameServer>