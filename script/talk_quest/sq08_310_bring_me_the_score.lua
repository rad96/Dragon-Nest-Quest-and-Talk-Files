<VillageServer>

function sq08_310_bring_me_the_score_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 26 then
		sq08_310_bring_me_the_score_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_310_bring_me_the_score_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-l" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3101, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3102, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3103, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3104, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3105, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3106, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3107, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3108, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3109, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-m" then
				api_quest_AddQuest(userObjID,310, 1);
				api_quest_SetJournalStep(userObjID,310, 1);
				api_quest_SetQuestStep(userObjID,310, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 349, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 350, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 200349, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 200350, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 400117, 1);

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-2-b" then 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3101, true);
				 api_quest_RewardQuestUser(userObjID, 3101, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3102, true);
				 api_quest_RewardQuestUser(userObjID, 3102, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3103, true);
				 api_quest_RewardQuestUser(userObjID, 3103, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3104, true);
				 api_quest_RewardQuestUser(userObjID, 3104, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3105, true);
				 api_quest_RewardQuestUser(userObjID, 3105, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3106, true);
				 api_quest_RewardQuestUser(userObjID, 3106, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3107, true);
				 api_quest_RewardQuestUser(userObjID, 3107, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3108, true);
				 api_quest_RewardQuestUser(userObjID, 3108, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3109, true);
				 api_quest_RewardQuestUser(userObjID, 3109, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 311, 1);
					api_quest_SetQuestStep(userObjID, 311, 1);
					api_quest_SetJournalStep(userObjID, 311, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400117, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400117, api_quest_HasQuestItem(userObjID, 400117, 1));
				end
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400118, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400118, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n026_trader_may-2-e" then 
	end
	if npc_talk_index == "n026_trader_may-2-f" then 
	end
	if npc_talk_index == "n026_trader_may-2-g" then 
	end
	if npc_talk_index == "n026_trader_may-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n026_trader_may-1", "sq08_311_strange_score.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_310_bring_me_the_score_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 310);
	if qstep == 1 and CountIndex == 349 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400117, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400117, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 350 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400117, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400117, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400117 then
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 200349 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400117, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400117, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200350 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400117, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400117, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_310_bring_me_the_score_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 310);
	if qstep == 1 and CountIndex == 349 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 350 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400117 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200349 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200350 and Count >= TargetCount  then

	end
end

function sq08_310_bring_me_the_score_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 310);
	local questID=310;
end

function sq08_310_bring_me_the_score_OnRemoteStart( userObjID, questID )
end

function sq08_310_bring_me_the_score_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_310_bring_me_the_score_ForceAccept( userObjID, npcObjID, questID )
				npc_talk_index = "n026_trader_may-1";
end

</VillageServer>

<GameServer>
function sq08_310_bring_me_the_score_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 26 then
		sq08_310_bring_me_the_score_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_310_bring_me_the_score_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-l" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3101, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3102, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3103, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3104, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3105, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3106, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3107, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3108, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3109, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-m" then
				api_quest_AddQuest( pRoom, userObjID,310, 1);
				api_quest_SetJournalStep( pRoom, userObjID,310, 1);
				api_quest_SetQuestStep( pRoom, userObjID,310, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 349, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 350, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 200349, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 200350, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 400117, 1);

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-2-b" then 
	end
	if npc_talk_index == "n026_trader_may-2-c" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3101, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3101, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3102, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3102, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3103, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3103, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3104, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3104, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3105, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3105, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3106, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3106, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3107, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3107, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3108, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3108, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3109, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3109, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-2-d" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 311, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 311, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 311, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400117, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400117, api_quest_HasQuestItem( pRoom, userObjID, 400117, 1));
				end
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400118, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400118, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end
	if npc_talk_index == "n026_trader_may-2-e" then 
	end
	if npc_talk_index == "n026_trader_may-2-f" then 
	end
	if npc_talk_index == "n026_trader_may-2-g" then 
	end
	if npc_talk_index == "n026_trader_may-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n026_trader_may-1", "sq08_311_strange_score.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_310_bring_me_the_score_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 310);
	if qstep == 1 and CountIndex == 349 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400117, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400117, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 350 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400117, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400117, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 400117 then
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 200349 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400117, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400117, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 1 and CountIndex == 200350 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400117, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400117, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_310_bring_me_the_score_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 310);
	if qstep == 1 and CountIndex == 349 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 350 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 400117 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200349 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 200350 and Count >= TargetCount  then

	end
end

function sq08_310_bring_me_the_score_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 310);
	local questID=310;
end

function sq08_310_bring_me_the_score_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_310_bring_me_the_score_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_310_bring_me_the_score_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				npc_talk_index = "n026_trader_may-1";
end

</GameServer>