<VillageServer>

function sq34_4424_behind_memory_1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 36 then
		sq34_4424_behind_memory_1_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1236 then
		sq34_4424_behind_memory_1_OnTalk_n1236_wanderer_elf_riana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq34_4424_behind_memory_1_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 44240, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 44240, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 44240, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 44240, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 44240, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 44240, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 44240, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 44240, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 44240, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4424, 1);
				api_quest_SetJournalStep(userObjID,4424, 1);
				api_quest_SetQuestStep(userObjID,4424, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400416, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400416, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 44240, true);
				 api_quest_RewardQuestUser(userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 44240, true);
				 api_quest_RewardQuestUser(userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 44240, true);
				 api_quest_RewardQuestUser(userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 44240, true);
				 api_quest_RewardQuestUser(userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 44240, true);
				 api_quest_RewardQuestUser(userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 44240, true);
				 api_quest_RewardQuestUser(userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 44240, true);
				 api_quest_RewardQuestUser(userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 44240, true);
				 api_quest_RewardQuestUser(userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 44240, true);
				 api_quest_RewardQuestUser(userObjID, 44240, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4427, 1);
					api_quest_SetQuestStep(userObjID, 4427, 1);
					api_quest_SetJournalStep(userObjID, 4427, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400416, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400416, api_quest_HasQuestItem(userObjID, 400416, 1));
				end
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-1", "sq34_4427_behind_memory_2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1236_wanderer_elf_riana--------------------------------------------------------------------------------
function sq34_4424_behind_memory_1_OnTalk_n1236_wanderer_elf_riana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1236_wanderer_elf_riana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1236_wanderer_elf_riana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1236_wanderer_elf_riana-1-b" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-c" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-d" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-e" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-f" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-g" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-h" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4424_behind_memory_1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4424);
end

function sq34_4424_behind_memory_1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4424);
end

function sq34_4424_behind_memory_1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4424);
	local questID=4424;
end

function sq34_4424_behind_memory_1_OnRemoteStart( userObjID, questID )
end

function sq34_4424_behind_memory_1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq34_4424_behind_memory_1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4424, 1);
				api_quest_SetJournalStep(userObjID,4424, 1);
				api_quest_SetQuestStep(userObjID,4424, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400416, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400416, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
end

</VillageServer>

<GameServer>
function sq34_4424_behind_memory_1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 36 then
		sq34_4424_behind_memory_1_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1236 then
		sq34_4424_behind_memory_1_OnTalk_n1236_wanderer_elf_riana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq34_4424_behind_memory_1_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4424, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4424, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4424, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400416, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400416, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44240, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44240, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44240, questID, 1);
			 end 
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4427, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4427, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4427, 1);

				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400416, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400416, api_quest_HasQuestItem( pRoom, userObjID, 400416, 1));
				end
	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-1", "sq34_4427_behind_memory_2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1236_wanderer_elf_riana--------------------------------------------------------------------------------
function sq34_4424_behind_memory_1_OnTalk_n1236_wanderer_elf_riana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1236_wanderer_elf_riana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1236_wanderer_elf_riana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1236_wanderer_elf_riana-1-b" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-c" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-d" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-e" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-f" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-g" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-1-h" then 
	end
	if npc_talk_index == "n1236_wanderer_elf_riana-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4424_behind_memory_1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4424);
end

function sq34_4424_behind_memory_1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4424);
end

function sq34_4424_behind_memory_1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4424);
	local questID=4424;
end

function sq34_4424_behind_memory_1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq34_4424_behind_memory_1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq34_4424_behind_memory_1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4424, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4424, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4424, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400416, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400416, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
end

</GameServer>