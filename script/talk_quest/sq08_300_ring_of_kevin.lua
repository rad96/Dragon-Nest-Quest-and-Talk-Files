<VillageServer>

function sq08_300_ring_of_kevin_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 29 then
		sq08_300_ring_of_kevin_OnTalk_n029_crew_kevin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n029_crew_kevin--------------------------------------------------------------------------------
function sq08_300_ring_of_kevin_OnTalk_n029_crew_kevin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n029_crew_kevin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n029_crew_kevin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n029_crew_kevin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n029_crew_kevin-accepting-f" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3001, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3002, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3003, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3004, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3005, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3006, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3007, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3008, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3009, false);
			 end 

	end
	if npc_talk_index == "n029_crew_kevin-accepting-acceptted" then
				npc_talk_index = "n029_crew_kevin-1";
				api_quest_AddQuest(userObjID,300, 1);
				api_quest_SetJournalStep(userObjID,300, 1);
				api_quest_SetQuestStep(userObjID,300, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300213, 1);

	end
	if npc_talk_index == "n029_crew_kevin-2-b" then 
	end
	if npc_talk_index == "n029_crew_kevin-2-c" then 
	end
	if npc_talk_index == "n029_crew_kevin-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3001, true);
				 api_quest_RewardQuestUser(userObjID, 3001, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3002, true);
				 api_quest_RewardQuestUser(userObjID, 3002, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3003, true);
				 api_quest_RewardQuestUser(userObjID, 3003, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3004, true);
				 api_quest_RewardQuestUser(userObjID, 3004, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3005, true);
				 api_quest_RewardQuestUser(userObjID, 3005, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3006, true);
				 api_quest_RewardQuestUser(userObjID, 3006, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3007, true);
				 api_quest_RewardQuestUser(userObjID, 3007, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3008, true);
				 api_quest_RewardQuestUser(userObjID, 3008, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3009, true);
				 api_quest_RewardQuestUser(userObjID, 3009, questID, 1);
			 end 
	end
	if npc_talk_index == "n029_crew_kevin-2-e" then 

				if api_quest_HasQuestItem(userObjID, 300213, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300213, api_quest_HasQuestItem(userObjID, 300213, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_300_ring_of_kevin_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 300);
	if qstep == 1 and CountIndex == 300213 then

	end
end

function sq08_300_ring_of_kevin_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 300);
	if qstep == 1 and CountIndex == 300213 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function sq08_300_ring_of_kevin_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 300);
	local questID=300;
end

function sq08_300_ring_of_kevin_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300213, 1);
end

function sq08_300_ring_of_kevin_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_300_ring_of_kevin_ForceAccept( userObjID, npcObjID, questID )
				npc_talk_index = "n029_crew_kevin-1";
				api_quest_AddQuest(userObjID,300, 1);
				api_quest_SetJournalStep(userObjID,300, 1);
				api_quest_SetQuestStep(userObjID,300, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300213, 1);
end

</VillageServer>

<GameServer>
function sq08_300_ring_of_kevin_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 29 then
		sq08_300_ring_of_kevin_OnTalk_n029_crew_kevin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n029_crew_kevin--------------------------------------------------------------------------------
function sq08_300_ring_of_kevin_OnTalk_n029_crew_kevin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n029_crew_kevin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n029_crew_kevin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n029_crew_kevin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n029_crew_kevin-accepting-f" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3001, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3002, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3003, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3004, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3005, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3006, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3007, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3008, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3009, false);
			 end 

	end
	if npc_talk_index == "n029_crew_kevin-accepting-acceptted" then
				npc_talk_index = "n029_crew_kevin-1";
				api_quest_AddQuest( pRoom, userObjID,300, 1);
				api_quest_SetJournalStep( pRoom, userObjID,300, 1);
				api_quest_SetQuestStep( pRoom, userObjID,300, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300213, 1);

	end
	if npc_talk_index == "n029_crew_kevin-2-b" then 
	end
	if npc_talk_index == "n029_crew_kevin-2-c" then 
	end
	if npc_talk_index == "n029_crew_kevin-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3001, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3001, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3002, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3002, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3003, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3003, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3004, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3004, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3005, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3005, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3006, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3006, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3007, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3007, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3008, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3008, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3009, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3009, questID, 1);
			 end 
	end
	if npc_talk_index == "n029_crew_kevin-2-e" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300213, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300213, api_quest_HasQuestItem( pRoom, userObjID, 300213, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_300_ring_of_kevin_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 300);
	if qstep == 1 and CountIndex == 300213 then

	end
end

function sq08_300_ring_of_kevin_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 300);
	if qstep == 1 and CountIndex == 300213 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function sq08_300_ring_of_kevin_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 300);
	local questID=300;
end

function sq08_300_ring_of_kevin_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300213, 1);
end

function sq08_300_ring_of_kevin_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_300_ring_of_kevin_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				npc_talk_index = "n029_crew_kevin-1";
				api_quest_AddQuest( pRoom, userObjID,300, 1);
				api_quest_SetJournalStep( pRoom, userObjID,300, 1);
				api_quest_SetQuestStep( pRoom, userObjID,300, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300213, 1);
end

</GameServer>