<VillageServer>

function sqc17_4715_army_of_disaster4_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1880 then
		sqc17_4715_army_of_disaster4_OnTalk_n1880_guard_seef(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1880_guard_seef--------------------------------------------------------------------------------
function sqc17_4715_army_of_disaster4_OnTalk_n1880_guard_seef(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1880_guard_seef-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1880_guard_seef-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1880_guard_seef-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1880_guard_seef-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1880_guard_seef-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47150, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47150, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47150, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47150, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47150, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47150, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47150, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47150, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47150, false);
			 end 

	end
	if npc_talk_index == "n1880_guard_seef-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4715, 1);
				api_quest_SetJournalStep(userObjID,4715, 1);
				api_quest_SetQuestStep(userObjID,4715, 1);
				npc_talk_index = "n1880_guard_seef-1";

	end
	if npc_talk_index == "n1880_guard_seef-1-b" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-c" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-d" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-e" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-f" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-g" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-h" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-i" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-j" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-k" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-l" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-m" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-n" then 
	end
	if npc_talk_index == "n1880_guard_seef-2" then 
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400489, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400489, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 400488, 1);
	end
	if npc_talk_index == "n1880_guard_seef-3-a" then 

				if api_quest_HasQuestItem(userObjID, 400488, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400488, api_quest_HasQuestItem(userObjID, 400488, 1));
				end
	end
	if npc_talk_index == "n1880_guard_seef-3-b" then 
	end
	if npc_talk_index == "n1880_guard_seef-3-c" then 
	end
	if npc_talk_index == "n1880_guard_seef-3-d" then 
	end
	if npc_talk_index == "n1880_guard_seef-3-e" then 
	end
	if npc_talk_index == "n1880_guard_seef-3-f" then 
	end
	if npc_talk_index == "n1880_guard_seef-3-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 47150, true);
				 api_quest_RewardQuestUser(userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 47150, true);
				 api_quest_RewardQuestUser(userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 47150, true);
				 api_quest_RewardQuestUser(userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 47150, true);
				 api_quest_RewardQuestUser(userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 47150, true);
				 api_quest_RewardQuestUser(userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 47150, true);
				 api_quest_RewardQuestUser(userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 47150, true);
				 api_quest_RewardQuestUser(userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 47150, true);
				 api_quest_RewardQuestUser(userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 47150, true);
				 api_quest_RewardQuestUser(userObjID, 47150, questID, 1);
			 end 
	end
	if npc_talk_index == "n1880_guard_seef-3-h" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4715_army_of_disaster4_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4715);
	if qstep == 2 and CountIndex == 400488 then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
end

function sqc17_4715_army_of_disaster4_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4715);
	if qstep == 2 and CountIndex == 400488 and Count >= TargetCount  then

	end
end

function sqc17_4715_army_of_disaster4_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4715);
	local questID=4715;
end

function sqc17_4715_army_of_disaster4_OnRemoteStart( userObjID, questID )
end

function sqc17_4715_army_of_disaster4_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc17_4715_army_of_disaster4_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4715, 1);
				api_quest_SetJournalStep(userObjID,4715, 1);
				api_quest_SetQuestStep(userObjID,4715, 1);
				npc_talk_index = "n1880_guard_seef-1";
end

</VillageServer>

<GameServer>
function sqc17_4715_army_of_disaster4_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1880 then
		sqc17_4715_army_of_disaster4_OnTalk_n1880_guard_seef( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1880_guard_seef--------------------------------------------------------------------------------
function sqc17_4715_army_of_disaster4_OnTalk_n1880_guard_seef( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1880_guard_seef-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1880_guard_seef-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1880_guard_seef-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1880_guard_seef-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1880_guard_seef-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, false);
			 end 

	end
	if npc_talk_index == "n1880_guard_seef-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4715, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4715, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4715, 1);
				npc_talk_index = "n1880_guard_seef-1";

	end
	if npc_talk_index == "n1880_guard_seef-1-b" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-c" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-d" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-e" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-f" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-g" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-h" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-i" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-j" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-k" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-l" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-m" then 
	end
	if npc_talk_index == "n1880_guard_seef-1-n" then 
	end
	if npc_talk_index == "n1880_guard_seef-2" then 
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400489, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400489, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 400488, 1);
	end
	if npc_talk_index == "n1880_guard_seef-3-a" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 400488, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400488, api_quest_HasQuestItem( pRoom, userObjID, 400488, 1));
				end
	end
	if npc_talk_index == "n1880_guard_seef-3-b" then 
	end
	if npc_talk_index == "n1880_guard_seef-3-c" then 
	end
	if npc_talk_index == "n1880_guard_seef-3-d" then 
	end
	if npc_talk_index == "n1880_guard_seef-3-e" then 
	end
	if npc_talk_index == "n1880_guard_seef-3-f" then 
	end
	if npc_talk_index == "n1880_guard_seef-3-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 47150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 47150, questID, 1);
			 end 
	end
	if npc_talk_index == "n1880_guard_seef-3-h" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc17_4715_army_of_disaster4_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4715);
	if qstep == 2 and CountIndex == 400488 then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
end

function sqc17_4715_army_of_disaster4_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4715);
	if qstep == 2 and CountIndex == 400488 and Count >= TargetCount  then

	end
end

function sqc17_4715_army_of_disaster4_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4715);
	local questID=4715;
end

function sqc17_4715_army_of_disaster4_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc17_4715_army_of_disaster4_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc17_4715_army_of_disaster4_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4715, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4715, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4715, 1);
				npc_talk_index = "n1880_guard_seef-1";
end

</GameServer>