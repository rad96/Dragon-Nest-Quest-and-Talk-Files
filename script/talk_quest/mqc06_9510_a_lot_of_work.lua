<VillageServer>

function mqc06_9510_a_lot_of_work_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1691 then
		mqc06_9510_a_lot_of_work_OnTalk_n1691_eltia_lotus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1699 then
		mqc06_9510_a_lot_of_work_OnTalk_n1699_kaye_tera_river(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1700 then
		mqc06_9510_a_lot_of_work_OnTalk_n1700_kaye_east(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1701 then
		mqc06_9510_a_lot_of_work_OnTalk_n1701_teramai_east(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1691_eltia_lotus--------------------------------------------------------------------------------
function mqc06_9510_a_lot_of_work_OnTalk_n1691_eltia_lotus(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1691_eltia_lotus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1691_eltia_lotus-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1691_eltia_lotus-4";
				if api_user_GetUserLevel(userObjID) >= 43 then
									npc_talk_index = "n1691_eltia_lotus-4-a";

				else
									npc_talk_index = "n1691_eltia_lotus-4";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1691_eltia_lotus-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9510, 2);
				api_quest_SetJournalStep(userObjID,9510, 1);
				api_quest_SetQuestStep(userObjID,9510, 1);

	end
	if npc_talk_index == "n1691_eltia_lotus-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95100, true);
				 api_quest_RewardQuestUser(userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95100, true);
				 api_quest_RewardQuestUser(userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95100, true);
				 api_quest_RewardQuestUser(userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95100, true);
				 api_quest_RewardQuestUser(userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95100, true);
				 api_quest_RewardQuestUser(userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95100, true);
				 api_quest_RewardQuestUser(userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95100, true);
				 api_quest_RewardQuestUser(userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95100, true);
				 api_quest_RewardQuestUser(userObjID, 95100, questID, 1);
			 end 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9511, 2);
					api_quest_SetQuestStep(userObjID, 9511, 1);
					api_quest_SetJournalStep(userObjID, 9511, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1691_eltia_lotus-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1691_eltia_lotus-1", "mqc06_9511_weirdo.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1699_kaye_tera_river--------------------------------------------------------------------------------
function mqc06_9510_a_lot_of_work_OnTalk_n1699_kaye_tera_river(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1699_kaye_tera_river-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1700_kaye_east--------------------------------------------------------------------------------
function mqc06_9510_a_lot_of_work_OnTalk_n1700_kaye_east(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1700_kaye_east-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1700_kaye_east-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1700_kaye_east-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1700_kaye_east-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1700_kaye_east-2-b" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-c" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-d" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-e" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-f" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-g" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-h" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-i" then 
	end
	if npc_talk_index == "n1700_kaye_east-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1701_teramai_east--------------------------------------------------------------------------------
function mqc06_9510_a_lot_of_work_OnTalk_n1701_teramai_east(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1701_teramai_east-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1701_teramai_east-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1701_teramai_east-3-b" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-c" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-d" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-e" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-f" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-g" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-h" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-i" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-j" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-k" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-l" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-m" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-n" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-o" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-p" then 
	end
	if npc_talk_index == "n1701_teramai_east-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc06_9510_a_lot_of_work_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9510);
end

function mqc06_9510_a_lot_of_work_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9510);
end

function mqc06_9510_a_lot_of_work_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9510);
	local questID=9510;
end

function mqc06_9510_a_lot_of_work_OnRemoteStart( userObjID, questID )
end

function mqc06_9510_a_lot_of_work_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc06_9510_a_lot_of_work_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9510, 2);
				api_quest_SetJournalStep(userObjID,9510, 1);
				api_quest_SetQuestStep(userObjID,9510, 1);
end

</VillageServer>

<GameServer>
function mqc06_9510_a_lot_of_work_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1691 then
		mqc06_9510_a_lot_of_work_OnTalk_n1691_eltia_lotus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1699 then
		mqc06_9510_a_lot_of_work_OnTalk_n1699_kaye_tera_river( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1700 then
		mqc06_9510_a_lot_of_work_OnTalk_n1700_kaye_east( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1701 then
		mqc06_9510_a_lot_of_work_OnTalk_n1701_teramai_east( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1691_eltia_lotus--------------------------------------------------------------------------------
function mqc06_9510_a_lot_of_work_OnTalk_n1691_eltia_lotus( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1691_eltia_lotus-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1691_eltia_lotus-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1691_eltia_lotus-4";
				if api_user_GetUserLevel( pRoom, userObjID) >= 43 then
									npc_talk_index = "n1691_eltia_lotus-4-a";

				else
									npc_talk_index = "n1691_eltia_lotus-4";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1691_eltia_lotus-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9510, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9510, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9510, 1);

	end
	if npc_talk_index == "n1691_eltia_lotus-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95100, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95100, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95100, questID, 1);
			 end 
	end
	if npc_talk_index == "n1691_eltia_lotus-4-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9511, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9511, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9511, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1691_eltia_lotus-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1691_eltia_lotus-1", "mqc06_9511_weirdo.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1699_kaye_tera_river--------------------------------------------------------------------------------
function mqc06_9510_a_lot_of_work_OnTalk_n1699_kaye_tera_river( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1699_kaye_tera_river-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1700_kaye_east--------------------------------------------------------------------------------
function mqc06_9510_a_lot_of_work_OnTalk_n1700_kaye_east( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1700_kaye_east-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1700_kaye_east-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1700_kaye_east-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1700_kaye_east-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1700_kaye_east-2-b" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-c" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-d" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-e" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-f" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-g" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-h" then 
	end
	if npc_talk_index == "n1700_kaye_east-2-i" then 
	end
	if npc_talk_index == "n1700_kaye_east-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1701_teramai_east--------------------------------------------------------------------------------
function mqc06_9510_a_lot_of_work_OnTalk_n1701_teramai_east( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1701_teramai_east-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1701_teramai_east-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1701_teramai_east-3-b" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-c" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-d" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-e" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-f" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-g" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-h" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-i" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-j" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-k" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-l" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-m" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-n" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-o" then 
	end
	if npc_talk_index == "n1701_teramai_east-3-p" then 
	end
	if npc_talk_index == "n1701_teramai_east-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc06_9510_a_lot_of_work_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9510);
end

function mqc06_9510_a_lot_of_work_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9510);
end

function mqc06_9510_a_lot_of_work_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9510);
	local questID=9510;
end

function mqc06_9510_a_lot_of_work_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc06_9510_a_lot_of_work_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc06_9510_a_lot_of_work_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9510, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9510, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9510, 1);
end

</GameServer>