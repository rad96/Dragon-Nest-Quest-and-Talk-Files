<VillageServer>

function sq05_165_enter_adventure_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 342 then
		sq05_165_enter_adventure_OnTalk_n342_guild_gorden(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 400 then
		sq05_165_enter_adventure_OnTalk_n400_guild_board(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n342_guild_gorden--------------------------------------------------------------------------------
function sq05_165_enter_adventure_OnTalk_n342_guild_gorden(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n342_guild_gorden-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n342_guild_gorden-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n342_guild_gorden-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n342_guild_gorden-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1651, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1651, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1651, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1652, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1653, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1651, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1651, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1651, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1651, false);
			 end 

	end
	if npc_talk_index == "n342_guild_gorden-accepting-acceptted" then
				api_quest_AddQuest(userObjID,165, 1);
				api_quest_SetJournalStep(userObjID,165, 1);
				api_quest_SetQuestStep(userObjID,165, 1);
				npc_talk_index = "n342_guild_gorden-2";
				api_quest_SetQuestStep(userObjID, questID,2);

	end
	if npc_talk_index == "n342_guild_gorden-2-b" then 
	end
	if npc_talk_index == "n342_guild_gorden-2-c" then 
	end
	if npc_talk_index == "n342_guild_gorden-2-d" then 
	end
	if npc_talk_index == "n342_guild_gorden-2-e" then 
	end
	if npc_talk_index == "n342_guild_gorden-1" then 
				api_quest_SetQuestStep(userObjID, questID,1);
				api_quest_SetJournalStep(userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300227, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300227, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n400_guild_board--------------------------------------------------------------------------------
function sq05_165_enter_adventure_OnTalk_n400_guild_board(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n400_guild_board-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n400_guild_board-1-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 1651, true);
				 api_quest_RewardQuestUser(userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 1651, true);
				 api_quest_RewardQuestUser(userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 1651, true);
				 api_quest_RewardQuestUser(userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 1652, true);
				 api_quest_RewardQuestUser(userObjID, 1652, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 1653, true);
				 api_quest_RewardQuestUser(userObjID, 1653, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 1651, true);
				 api_quest_RewardQuestUser(userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 1651, true);
				 api_quest_RewardQuestUser(userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 1651, true);
				 api_quest_RewardQuestUser(userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 1651, true);
				 api_quest_RewardQuestUser(userObjID, 1651, questID, 1);
			 end 
	end
	if npc_talk_index == "n400_guild_board-1-b" then 

				if api_quest_HasQuestItem(userObjID, 300227, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300227, api_quest_HasQuestItem(userObjID, 300227, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_165_enter_adventure_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 165);
end

function sq05_165_enter_adventure_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 165);
end

function sq05_165_enter_adventure_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 165);
	local questID=165;
end

function sq05_165_enter_adventure_OnRemoteStart( userObjID, questID )
				api_quest_SetQuestStep(userObjID, questID,2);
end

function sq05_165_enter_adventure_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq05_165_enter_adventure_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,165, 1);
				api_quest_SetJournalStep(userObjID,165, 1);
				api_quest_SetQuestStep(userObjID,165, 1);
				npc_talk_index = "n342_guild_gorden-2";
				api_quest_SetQuestStep(userObjID, questID,2);
end

</VillageServer>

<GameServer>
function sq05_165_enter_adventure_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 342 then
		sq05_165_enter_adventure_OnTalk_n342_guild_gorden( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 400 then
		sq05_165_enter_adventure_OnTalk_n400_guild_board( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n342_guild_gorden--------------------------------------------------------------------------------
function sq05_165_enter_adventure_OnTalk_n342_guild_gorden( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n342_guild_gorden-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n342_guild_gorden-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n342_guild_gorden-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n342_guild_gorden-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1652, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1653, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, false);
			 end 

	end
	if npc_talk_index == "n342_guild_gorden-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,165, 1);
				api_quest_SetJournalStep( pRoom, userObjID,165, 1);
				api_quest_SetQuestStep( pRoom, userObjID,165, 1);
				npc_talk_index = "n342_guild_gorden-2";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);

	end
	if npc_talk_index == "n342_guild_gorden-2-b" then 
	end
	if npc_talk_index == "n342_guild_gorden-2-c" then 
	end
	if npc_talk_index == "n342_guild_gorden-2-d" then 
	end
	if npc_talk_index == "n342_guild_gorden-2-e" then 
	end
	if npc_talk_index == "n342_guild_gorden-1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300227, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300227, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n400_guild_board--------------------------------------------------------------------------------
function sq05_165_enter_adventure_OnTalk_n400_guild_board( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n400_guild_board-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n400_guild_board-1-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1652, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1652, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1653, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1653, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1651, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 1651, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 1651, questID, 1);
			 end 
	end
	if npc_talk_index == "n400_guild_board-1-b" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300227, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300227, api_quest_HasQuestItem( pRoom, userObjID, 300227, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq05_165_enter_adventure_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 165);
end

function sq05_165_enter_adventure_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 165);
end

function sq05_165_enter_adventure_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 165);
	local questID=165;
end

function sq05_165_enter_adventure_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
end

function sq05_165_enter_adventure_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq05_165_enter_adventure_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,165, 1);
				api_quest_SetJournalStep( pRoom, userObjID,165, 1);
				api_quest_SetQuestStep( pRoom, userObjID,165, 1);
				npc_talk_index = "n342_guild_gorden-2";
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
end

</GameServer>