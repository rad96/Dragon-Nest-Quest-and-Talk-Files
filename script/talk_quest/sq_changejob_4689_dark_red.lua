<VillageServer>

function sq_changejob_4689_dark_red_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 458 then
		sq_changejob_4689_dark_red_OnTalk_n458_sword_of_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 954 then
		sq_changejob_4689_dark_red_OnTalk_n954_darklair_rosetta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 460 then
		sq_changejob_4689_dark_red_OnTalk_n460_eclair(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n458_sword_of_geraint--------------------------------------------------------------------------------
function sq_changejob_4689_dark_red_OnTalk_n458_sword_of_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n458_sword_of_geraint-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n458_sword_of_geraint-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n458_sword_of_geraint-1-b" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-c" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-d" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-e" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-f" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-g" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-h" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-i" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-j" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-k" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-move1" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									api_user_ChangeMap(userObjID,13035,2);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
									npc_talk_index = "n458_sword_of_geraint-1-l";

				end
	end
	if npc_talk_index == "n458_sword_of_geraint-2-move2" then 
				if api_user_GetPartymemberCount(userObjID) == 1  then
									api_user_ChangeMap(userObjID,13035,2);

				else
									npc_talk_index = "n458_sword_of_geraint-2-a";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n954_darklair_rosetta--------------------------------------------------------------------------------
function sq_changejob_4689_dark_red_OnTalk_n954_darklair_rosetta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n954_darklair_rosetta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n954_darklair_rosetta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n954_darklair_rosetta-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n954_darklair_rosetta-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46890, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46890, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46890, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46890, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46890, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46890, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46890, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46890, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46890, false);
			 end 

	end
	if npc_talk_index == "n954_darklair_rosetta-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4689, 1);
				api_quest_SetJournalStep(userObjID,4689, 1);
				api_quest_SetQuestStep(userObjID,4689, 1);
				npc_talk_index = "n954_darklair_rosetta-1";

	end
	if npc_talk_index == "n954_darklair_rosetta-4-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-e" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 46890, true);
				 api_quest_RewardQuestUser(userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 46890, true);
				 api_quest_RewardQuestUser(userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 46890, true);
				 api_quest_RewardQuestUser(userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 46890, true);
				 api_quest_RewardQuestUser(userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 46890, true);
				 api_quest_RewardQuestUser(userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 46890, true);
				 api_quest_RewardQuestUser(userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 46890, true);
				 api_quest_RewardQuestUser(userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 46890, true);
				 api_quest_RewardQuestUser(userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 46890, true);
				 api_quest_RewardQuestUser(userObjID, 46890, questID, 1);
			 end 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-f" then 
				api_user_SetUserJobID(userObjID, 76);

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n460_eclair--------------------------------------------------------------------------------
function sq_changejob_4689_dark_red_OnTalk_n460_eclair(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n460_eclair-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n460_eclair-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n460_eclair-2-b" then 
	end
	if npc_talk_index == "n460_eclair-2-c" then 
	end
	if npc_talk_index == "n460_eclair-2-d" then 
	end
	if npc_talk_index == "n460_eclair-2-e" then 
	end
	if npc_talk_index == "n460_eclair-2-f" then 
	end
	if npc_talk_index == "n460_eclair-2-g" then 
	end
	if npc_talk_index == "n460_eclair-2-h" then 
	end
	if npc_talk_index == "n460_eclair-2-i" then 
	end
	if npc_talk_index == "n460_eclair-2-j" then 
	end
	if npc_talk_index == "n460_eclair-2-k" then 
	end
	if npc_talk_index == "n460_eclair-2-l" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_changejob_4689_dark_red_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4689);
end

function sq_changejob_4689_dark_red_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4689);
end

function sq_changejob_4689_dark_red_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4689);
	local questID=4689;
end

function sq_changejob_4689_dark_red_OnRemoteStart( userObjID, questID )
end

function sq_changejob_4689_dark_red_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq_changejob_4689_dark_red_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4689, 1);
				api_quest_SetJournalStep(userObjID,4689, 1);
				api_quest_SetQuestStep(userObjID,4689, 1);
				npc_talk_index = "n954_darklair_rosetta-1";
end

</VillageServer>

<GameServer>
function sq_changejob_4689_dark_red_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 458 then
		sq_changejob_4689_dark_red_OnTalk_n458_sword_of_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 954 then
		sq_changejob_4689_dark_red_OnTalk_n954_darklair_rosetta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 460 then
		sq_changejob_4689_dark_red_OnTalk_n460_eclair( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n458_sword_of_geraint--------------------------------------------------------------------------------
function sq_changejob_4689_dark_red_OnTalk_n458_sword_of_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n458_sword_of_geraint-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n458_sword_of_geraint-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n458_sword_of_geraint-1-b" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-c" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-d" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-e" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-f" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-g" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-h" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-i" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-j" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-k" then 
	end
	if npc_talk_index == "n458_sword_of_geraint-1-move1" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									api_user_ChangeMap( pRoom, userObjID,13035,2);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
									npc_talk_index = "n458_sword_of_geraint-1-l";

				end
	end
	if npc_talk_index == "n458_sword_of_geraint-2-move2" then 
				if api_user_GetPartymemberCount( pRoom, userObjID) == 1  then
									api_user_ChangeMap( pRoom, userObjID,13035,2);

				else
									npc_talk_index = "n458_sword_of_geraint-2-a";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n954_darklair_rosetta--------------------------------------------------------------------------------
function sq_changejob_4689_dark_red_OnTalk_n954_darklair_rosetta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n954_darklair_rosetta-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n954_darklair_rosetta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n954_darklair_rosetta-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n954_darklair_rosetta-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, false);
			 end 

	end
	if npc_talk_index == "n954_darklair_rosetta-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4689, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4689, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4689, 1);
				npc_talk_index = "n954_darklair_rosetta-1";

	end
	if npc_talk_index == "n954_darklair_rosetta-4-b" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-c" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-d" then 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-e" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46890, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 46890, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 46890, questID, 1);
			 end 
	end
	if npc_talk_index == "n954_darklair_rosetta-4-f" then 
				api_user_SetUserJobID( pRoom, userObjID, 76);

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n460_eclair--------------------------------------------------------------------------------
function sq_changejob_4689_dark_red_OnTalk_n460_eclair( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n460_eclair-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n460_eclair-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n460_eclair-2-b" then 
	end
	if npc_talk_index == "n460_eclair-2-c" then 
	end
	if npc_talk_index == "n460_eclair-2-d" then 
	end
	if npc_talk_index == "n460_eclair-2-e" then 
	end
	if npc_talk_index == "n460_eclair-2-f" then 
	end
	if npc_talk_index == "n460_eclair-2-g" then 
	end
	if npc_talk_index == "n460_eclair-2-h" then 
	end
	if npc_talk_index == "n460_eclair-2-i" then 
	end
	if npc_talk_index == "n460_eclair-2-j" then 
	end
	if npc_talk_index == "n460_eclair-2-k" then 
	end
	if npc_talk_index == "n460_eclair-2-l" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq_changejob_4689_dark_red_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4689);
end

function sq_changejob_4689_dark_red_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4689);
end

function sq_changejob_4689_dark_red_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4689);
	local questID=4689;
end

function sq_changejob_4689_dark_red_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq_changejob_4689_dark_red_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq_changejob_4689_dark_red_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4689, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4689, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4689, 1);
				npc_talk_index = "n954_darklair_rosetta-1";
end

</GameServer>