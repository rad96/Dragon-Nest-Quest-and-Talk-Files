<VillageServer>

function mq15_744_record_end_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 394 then
		mq15_744_record_end_OnTalk_n394_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 528 then
		mq15_744_record_end_OnTalk_n528_acamedic_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_744_record_end_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_744_record_end_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n394_academic_station--------------------------------------------------------------------------------
function mq15_744_record_end_OnTalk_n394_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n394_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n394_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n394_academic_station-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n394_academic_station-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n394_academic_station-accepting-acceptted" then
				api_quest_AddQuest(userObjID,744, 2);
				api_quest_SetJournalStep(userObjID,744, 1);
				api_quest_SetQuestStep(userObjID,744, 1);
				npc_talk_index = "n394_academic_station-1";

	end
	if npc_talk_index == "n394_academic_station-4-b" then 
	end
	if npc_talk_index == "n394_academic_station-4-c" then 
	end
	if npc_talk_index == "n394_academic_station-4-d" then 
	end
	if npc_talk_index == "n394_academic_station-4-e" then 
	end
	if npc_talk_index == "n394_academic_station-4-f" then 
	end
	if npc_talk_index == "n394_academic_station-4-g" then 
	end
	if npc_talk_index == "n394_academic_station-4-h" then 
	end
	if npc_talk_index == "n394_academic_station-4-i" then 
	end
	if npc_talk_index == "n394_academic_station-4-j" then 
	end
	if npc_talk_index == "n394_academic_station-4-k" then 
	end
	if npc_talk_index == "n394_academic_station-4-l" then 
	end
	if npc_talk_index == "n394_academic_station-4-m" then 
	end
	if npc_talk_index == "n394_academic_station-4-n" then 
	end
	if npc_talk_index == "n394_academic_station-4-o" then 
	end
	if npc_talk_index == "n394_academic_station-4-p" then 
	end
	if npc_talk_index == "n394_academic_station-4-q" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7440, true);
				 api_quest_RewardQuestUser(userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7440, true);
				 api_quest_RewardQuestUser(userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7440, true);
				 api_quest_RewardQuestUser(userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7440, true);
				 api_quest_RewardQuestUser(userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7440, true);
				 api_quest_RewardQuestUser(userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7440, true);
				 api_quest_RewardQuestUser(userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7440, true);
				 api_quest_RewardQuestUser(userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7440, true);
				 api_quest_RewardQuestUser(userObjID, 7440, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-4-r" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9200, 2);
					api_quest_SetQuestStep(userObjID, 9200, 1);
					api_quest_SetJournalStep(userObjID, 9200, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-4-s" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n528_acamedic_jasmin--------------------------------------------------------------------------------
function mq15_744_record_end_OnTalk_n528_acamedic_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_744_record_end_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n701_oldkarakule-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_744_record_end_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n707_sidel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-3-h" then 
	end
	if npc_talk_index == "n707_sidel-3-i" then 
	end
	if npc_talk_index == "n707_sidel-3-j" then 
	end
	if npc_talk_index == "n707_sidel-3-k" then 
	end
	if npc_talk_index == "n707_sidel-3-l" then 
	end
	if npc_talk_index == "n707_sidel-3-m" then 
	end
	if npc_talk_index == "n707_sidel-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_744_record_end_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 744);
end

function mq15_744_record_end_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 744);
end

function mq15_744_record_end_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 744);
	local questID=744;
end

function mq15_744_record_end_OnRemoteStart( userObjID, questID )
end

function mq15_744_record_end_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_744_record_end_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,744, 2);
				api_quest_SetJournalStep(userObjID,744, 1);
				api_quest_SetQuestStep(userObjID,744, 1);
				npc_talk_index = "n394_academic_station-1";
end

</VillageServer>

<GameServer>
function mq15_744_record_end_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 394 then
		mq15_744_record_end_OnTalk_n394_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 528 then
		mq15_744_record_end_OnTalk_n528_acamedic_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 701 then
		mq15_744_record_end_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_744_record_end_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n394_academic_station--------------------------------------------------------------------------------
function mq15_744_record_end_OnTalk_n394_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n394_academic_station-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n394_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n394_academic_station-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n394_academic_station-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n394_academic_station-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,744, 2);
				api_quest_SetJournalStep( pRoom, userObjID,744, 1);
				api_quest_SetQuestStep( pRoom, userObjID,744, 1);
				npc_talk_index = "n394_academic_station-1";

	end
	if npc_talk_index == "n394_academic_station-4-b" then 
	end
	if npc_talk_index == "n394_academic_station-4-c" then 
	end
	if npc_talk_index == "n394_academic_station-4-d" then 
	end
	if npc_talk_index == "n394_academic_station-4-e" then 
	end
	if npc_talk_index == "n394_academic_station-4-f" then 
	end
	if npc_talk_index == "n394_academic_station-4-g" then 
	end
	if npc_talk_index == "n394_academic_station-4-h" then 
	end
	if npc_talk_index == "n394_academic_station-4-i" then 
	end
	if npc_talk_index == "n394_academic_station-4-j" then 
	end
	if npc_talk_index == "n394_academic_station-4-k" then 
	end
	if npc_talk_index == "n394_academic_station-4-l" then 
	end
	if npc_talk_index == "n394_academic_station-4-m" then 
	end
	if npc_talk_index == "n394_academic_station-4-n" then 
	end
	if npc_talk_index == "n394_academic_station-4-o" then 
	end
	if npc_talk_index == "n394_academic_station-4-p" then 
	end
	if npc_talk_index == "n394_academic_station-4-q" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7440, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7440, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7440, questID, 1);
			 end 
	end
	if npc_talk_index == "n394_academic_station-4-r" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9200, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9200, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9200, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n394_academic_station-4-s" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n528_acamedic_jasmin--------------------------------------------------------------------------------
function mq15_744_record_end_OnTalk_n528_acamedic_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_744_record_end_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n701_oldkarakule-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_744_record_end_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n707_sidel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-3-h" then 
	end
	if npc_talk_index == "n707_sidel-3-i" then 
	end
	if npc_talk_index == "n707_sidel-3-j" then 
	end
	if npc_talk_index == "n707_sidel-3-k" then 
	end
	if npc_talk_index == "n707_sidel-3-l" then 
	end
	if npc_talk_index == "n707_sidel-3-m" then 
	end
	if npc_talk_index == "n707_sidel-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_744_record_end_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 744);
end

function mq15_744_record_end_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 744);
end

function mq15_744_record_end_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 744);
	local questID=744;
end

function mq15_744_record_end_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_744_record_end_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_744_record_end_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,744, 2);
				api_quest_SetJournalStep( pRoom, userObjID,744, 1);
				api_quest_SetQuestStep( pRoom, userObjID,744, 1);
				npc_talk_index = "n394_academic_station-1";
end

</GameServer>