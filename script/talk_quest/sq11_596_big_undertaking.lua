<VillageServer>

function sq11_596_big_undertaking_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 41 then
		sq11_596_big_undertaking_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function sq11_596_big_undertaking_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n041_duke_stwart-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-accepting-d" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5961, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5962, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5963, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5964, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5965, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5966, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5967, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5968, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5969, false);
			 end 

	end
	if npc_talk_index == "n041_duke_stwart-accepting-acceppted" then
				api_quest_AddQuest(userObjID,596, 1);
				api_quest_SetJournalStep(userObjID,596, 1);
				api_quest_SetQuestStep(userObjID,596, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 453, 1 );
				npc_talk_index = "n041_duke_stwart-1";

	end
	if npc_talk_index == "n041_duke_stwart-2-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-e" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-g" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-h" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-i" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5961, true);
				 api_quest_RewardQuestUser(userObjID, 5961, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5962, true);
				 api_quest_RewardQuestUser(userObjID, 5962, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5963, true);
				 api_quest_RewardQuestUser(userObjID, 5963, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5964, true);
				 api_quest_RewardQuestUser(userObjID, 5964, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5965, true);
				 api_quest_RewardQuestUser(userObjID, 5965, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5966, true);
				 api_quest_RewardQuestUser(userObjID, 5966, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5967, true);
				 api_quest_RewardQuestUser(userObjID, 5967, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5968, true);
				 api_quest_RewardQuestUser(userObjID, 5968, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5969, true);
				 api_quest_RewardQuestUser(userObjID, 5969, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-2-j" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 41, 450 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_596_big_undertaking_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 596);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( userObjID ) >= 0 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end
		 end 
	 end 
end

function sq11_596_big_undertaking_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 596);
end

function sq11_596_big_undertaking_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 596);
	local questID=596;
end

function sq11_596_big_undertaking_OnRemoteStart( userObjID, questID )
end

function sq11_596_big_undertaking_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_596_big_undertaking_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_596_big_undertaking_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 41 then
		sq11_596_big_undertaking_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n041_duke_stwart--------------------------------------------------------------------------------
function sq11_596_big_undertaking_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n041_duke_stwart-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n041_duke_stwart-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-accepting-d" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5961, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5962, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5963, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5964, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5965, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5966, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5967, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5968, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5969, false);
			 end 

	end
	if npc_talk_index == "n041_duke_stwart-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,596, 1);
				api_quest_SetJournalStep( pRoom, userObjID,596, 1);
				api_quest_SetQuestStep( pRoom, userObjID,596, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 453, 1 );
				npc_talk_index = "n041_duke_stwart-1";

	end
	if npc_talk_index == "n041_duke_stwart-2-b" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-c" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-d" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-e" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-f" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-g" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-h" then 
	end
	if npc_talk_index == "n041_duke_stwart-2-i" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5961, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5961, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5962, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5962, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5963, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5963, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5964, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5964, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5965, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5965, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5966, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5966, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5967, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5967, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5968, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5968, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5969, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5969, questID, 1);
			 end 
	end
	if npc_talk_index == "n041_duke_stwart-2-j" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 41, 450 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_596_big_undertaking_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 596);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( pRoom,  userObjID ) >= 0 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end
		 end 
	 end 
end

function sq11_596_big_undertaking_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 596);
end

function sq11_596_big_undertaking_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 596);
	local questID=596;
end

function sq11_596_big_undertaking_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_596_big_undertaking_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_596_big_undertaking_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>