<VillageServer>

function nq38_7602_tamara_talk_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 713 then
		nq38_7602_tamara_talk_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n713_soceress_tamara--------------------------------------------------------------------------------
function nq38_7602_tamara_talk_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-b" then
				api_quest_ForceCompleteQuest(userObjID,7602, 0, 1, 1, 0);
				api_npc_AddFavorPoint( userObjID, 713, 100 );

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-c" then
				api_quest_ForceCompleteQuest(userObjID,7602, 0, 1, 1, 0);
				api_npc_AddFavorPoint( userObjID, 713, 300 );

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-d" then
				api_quest_ForceCompleteQuest(userObjID,7602, 0, 1, 1, 0);
				api_npc_AddFavorPoint( userObjID, 713, 500 );

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-e" then
				api_quest_ForceCompleteQuest(userObjID,7602, 0, 1, 1, 0);
				api_npc_AddFavorPoint( userObjID, 713, 50 );

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq38_7602_tamara_talk_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7602);
end

function nq38_7602_tamara_talk_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7602);
end

function nq38_7602_tamara_talk_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 7602);
	local questID=7602;
end

function nq38_7602_tamara_talk_OnRemoteStart( userObjID, questID )
end

function nq38_7602_tamara_talk_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq38_7602_tamara_talk_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq38_7602_tamara_talk_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 713 then
		nq38_7602_tamara_talk_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n713_soceress_tamara--------------------------------------------------------------------------------
function nq38_7602_tamara_talk_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n713_soceress_tamara-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-accepting-b" then
				api_quest_ForceCompleteQuest( pRoom, userObjID,7602, 0, 1, 1, 0);
				api_npc_AddFavorPoint( pRoom,  userObjID, 713, 100 );

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-c" then
				api_quest_ForceCompleteQuest( pRoom, userObjID,7602, 0, 1, 1, 0);
				api_npc_AddFavorPoint( pRoom,  userObjID, 713, 300 );

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-d" then
				api_quest_ForceCompleteQuest( pRoom, userObjID,7602, 0, 1, 1, 0);
				api_npc_AddFavorPoint( pRoom,  userObjID, 713, 500 );

	end
	if npc_talk_index == "n713_soceress_tamara-accepting-e" then
				api_quest_ForceCompleteQuest( pRoom, userObjID,7602, 0, 1, 1, 0);
				api_npc_AddFavorPoint( pRoom,  userObjID, 713, 50 );

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq38_7602_tamara_talk_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7602);
end

function nq38_7602_tamara_talk_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7602);
end

function nq38_7602_tamara_talk_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 7602);
	local questID=7602;
end

function nq38_7602_tamara_talk_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq38_7602_tamara_talk_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq38_7602_tamara_talk_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>