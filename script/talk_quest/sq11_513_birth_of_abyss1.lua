<VillageServer>

function sq11_513_birth_of_abyss1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 36 then
		sq11_513_birth_of_abyss1_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		sq11_513_birth_of_abyss1_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_513_birth_of_abyss1_OnTalk_n036_adventurer_guildmaster_gunter(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5131, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5132, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5133, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5134, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5135, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5136, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5137, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5137, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5137, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest(userObjID,513, 1);
				api_quest_SetJournalStep(userObjID,513, 1);
				api_quest_SetQuestStep(userObjID,513, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_513_birth_of_abyss1_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n088_scholar_starshy-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-1-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-g" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300216, 1);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300217, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300218, 1);
				api_quest_SetCountingInfo(userObjID, questID, 3, 3, 300219, 1);
				api_quest_SetCountingInfo(userObjID, questID, 4, 3, 300220, 1);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 200806, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 200889, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 200870, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 8, 2, 200827, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 9, 2, 200858, 30000);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 5131, true);
				 api_quest_RewardQuestUser(userObjID, 5131, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 5132, true);
				 api_quest_RewardQuestUser(userObjID, 5132, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 5133, true);
				 api_quest_RewardQuestUser(userObjID, 5133, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 5134, true);
				 api_quest_RewardQuestUser(userObjID, 5134, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 5135, true);
				 api_quest_RewardQuestUser(userObjID, 5135, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 5136, true);
				 api_quest_RewardQuestUser(userObjID, 5136, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 5137, true);
				 api_quest_RewardQuestUser(userObjID, 5137, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 5137, true);
				 api_quest_RewardQuestUser(userObjID, 5137, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 5137, true);
				 api_quest_RewardQuestUser(userObjID, 5137, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-3-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem(userObjID, 300216, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300216, api_quest_HasQuestItem(userObjID, 300216, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300217, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300217, api_quest_HasQuestItem(userObjID, 300217, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300218, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300218, api_quest_HasQuestItem(userObjID, 300218, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300219, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300219, api_quest_HasQuestItem(userObjID, 300219, 1));
				end

				if api_quest_HasQuestItem(userObjID, 300220, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300220, api_quest_HasQuestItem(userObjID, 300220, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 514, 1);
					api_quest_SetQuestStep(userObjID, 514, 1);
					api_quest_SetJournalStep(userObjID, 514, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n088_scholar_starshy-1", "sq11_514_birth_of_abyss2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_513_birth_of_abyss1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 513);
	if qstep == 2 and CountIndex == 200806 then
				if api_quest_HasQuestItem(userObjID, 300216, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300216, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300216, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200889 then
				if api_quest_HasQuestItem(userObjID, 300217, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300217, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300217, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200870 then
				if api_quest_HasQuestItem(userObjID, 300218, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300218, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300218, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200827 then
				if api_quest_HasQuestItem(userObjID, 300219, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300219, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300219, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200858 then
				if api_quest_HasQuestItem(userObjID, 300220, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300220, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300220, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 300216 then

	end
	if qstep == 2 and CountIndex == 300217 then

	end
	if qstep == 2 and CountIndex == 300218 then

	end
	if qstep == 2 and CountIndex == 300219 then

	end
	if qstep == 2 and CountIndex == 300220 then

	end
end

function sq11_513_birth_of_abyss1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 513);
	if qstep == 2 and CountIndex == 200806 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200889 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200870 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200827 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200858 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300216 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300217 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300218 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300219 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300220 and Count >= TargetCount  then

	end
end

function sq11_513_birth_of_abyss1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 513);
	local questID=513;
end

function sq11_513_birth_of_abyss1_OnRemoteStart( userObjID, questID )
end

function sq11_513_birth_of_abyss1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_513_birth_of_abyss1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,513, 1);
				api_quest_SetJournalStep(userObjID,513, 1);
				api_quest_SetQuestStep(userObjID,513, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
end

</VillageServer>

<GameServer>
function sq11_513_birth_of_abyss1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 36 then
		sq11_513_birth_of_abyss1_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 88 then
		sq11_513_birth_of_abyss1_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n036_adventurer_guildmaster_gunter--------------------------------------------------------------------------------
function sq11_513_birth_of_abyss1_OnTalk_n036_adventurer_guildmaster_gunter( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n036_adventurer_guildmaster_gunter-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n036_adventurer_guildmaster_gunter-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5131, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5132, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5133, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5134, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5135, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5136, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5137, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5137, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5137, false);
			 end 

	end
	if npc_talk_index == "n036_adventurer_guildmaster_gunter-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,513, 1);
				api_quest_SetJournalStep( pRoom, userObjID,513, 1);
				api_quest_SetQuestStep( pRoom, userObjID,513, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n088_scholar_starshy--------------------------------------------------------------------------------
function sq11_513_birth_of_abyss1_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n088_scholar_starshy-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n088_scholar_starshy-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-1-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-1-g" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300216, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300217, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300218, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 3, 300219, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 3, 300220, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 200806, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 200889, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 200870, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 2, 200827, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 9, 2, 200858, 30000);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5131, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5131, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5132, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5132, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5133, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5133, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5134, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5134, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5135, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5135, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5136, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5136, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5137, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5137, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5137, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5137, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 5137, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 5137, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-3-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300216, api_quest_HasQuestItem( pRoom, userObjID, 300216, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300217, api_quest_HasQuestItem( pRoom, userObjID, 300217, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300218, api_quest_HasQuestItem( pRoom, userObjID, 300218, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300219, api_quest_HasQuestItem( pRoom, userObjID, 300219, 1));
				end

				if api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300220, api_quest_HasQuestItem( pRoom, userObjID, 300220, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 514, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 514, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 514, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n088_scholar_starshy-1", "sq11_514_birth_of_abyss2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_513_birth_of_abyss1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 513);
	if qstep == 2 and CountIndex == 200806 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300216, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300216, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200889 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300217, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300217, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200870 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300218, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300218, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200827 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300219, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300219, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 200858 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300220, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300220, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300216, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300217, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300218, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300219, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300220, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 300216 then

	end
	if qstep == 2 and CountIndex == 300217 then

	end
	if qstep == 2 and CountIndex == 300218 then

	end
	if qstep == 2 and CountIndex == 300219 then

	end
	if qstep == 2 and CountIndex == 300220 then

	end
end

function sq11_513_birth_of_abyss1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 513);
	if qstep == 2 and CountIndex == 200806 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200889 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200870 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200827 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200858 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300216 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300217 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300218 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300219 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300220 and Count >= TargetCount  then

	end
end

function sq11_513_birth_of_abyss1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 513);
	local questID=513;
end

function sq11_513_birth_of_abyss1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_513_birth_of_abyss1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_513_birth_of_abyss1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,513, 1);
				api_quest_SetJournalStep( pRoom, userObjID,513, 1);
				api_quest_SetQuestStep( pRoom, userObjID,513, 1);
				npc_talk_index = "n036_adventurer_guildmaster_gunter-1";
end

</GameServer>