<VillageServer>

function mqc04_9483_nest_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 42 then
		mqc04_9483_nest_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1739 then
		mqc04_9483_nest_OnTalk_n1739_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1737 then
		mqc04_9483_nest_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 37 then
		mqc04_9483_nest_OnTalk_n037_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function mqc04_9483_nest_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n042_general_duglars-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9483, 2);
				api_quest_SetJournalStep(userObjID,9483, 1);
				api_quest_SetQuestStep(userObjID,9483, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
	end
	if npc_talk_index == "n042_general_duglars-1-c" then 
	end
	if npc_talk_index == "n042_general_duglars-1-d" then 
	end
	if npc_talk_index == "n042_general_duglars-1-e" then 
	end
	if npc_talk_index == "n042_general_duglars-1-level_chk" then 
				if api_user_GetUserLevel(userObjID) >= 28 then
									npc_talk_index = "n042_general_duglars-1-f";

				else
									npc_talk_index = "n042_general_duglars-1-i";

				end
	end
	if npc_talk_index == "n042_general_duglars-1-g" then 
	end
	if npc_talk_index == "n042_general_duglars-1-h" then 
	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1739_kaye--------------------------------------------------------------------------------
function mqc04_9483_nest_OnTalk_n1739_kaye(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1739_kaye-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1739_kaye-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1739_kaye-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1739_kaye-3-b" then 
	end
	if npc_talk_index == "n1739_kaye-3-c" then 
	end
	if npc_talk_index == "n1739_kaye-3-d" then 
	end
	if npc_talk_index == "n1739_kaye-3-e" then 
	end
	if npc_talk_index == "n1739_kaye-3-f" then 
	end
	if npc_talk_index == "n1739_kaye-3-g" then 
	end
	if npc_talk_index == "n1739_kaye-3-h" then 
	end
	if npc_talk_index == "n1739_kaye-3-i" then 
	end
	if npc_talk_index == "n1739_kaye-3-j" then 
	end
	if npc_talk_index == "n1739_kaye-3-k" then 
	end
	if npc_talk_index == "n1739_kaye-3-l" then 
	end
	if npc_talk_index == "n1739_kaye-3-m" then 
	end
	if npc_talk_index == "n1739_kaye-3-n" then 
	end
	if npc_talk_index == "n1739_kaye-3-o" then 
	end
	if npc_talk_index == "n1739_kaye-3-p" then 
	end
	if npc_talk_index == "n1739_kaye-3-q" then 
	end
	if npc_talk_index == "n1739_kaye-3-r" then 
	end
	if npc_talk_index == "n1739_kaye-3-s" then 
	end
	if npc_talk_index == "n1739_kaye-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1737_eltia--------------------------------------------------------------------------------
function mqc04_9483_nest_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1737_eltia-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-4-b" then 
	end
	if npc_talk_index == "n1737_eltia-4-c" then 
	end
	if npc_talk_index == "n1737_eltia-4-d" then 
	end
	if npc_talk_index == "n1737_eltia-4-e" then 
	end
	if npc_talk_index == "n1737_eltia-4-f" then 
	end
	if npc_talk_index == "n1737_eltia-4-g" then 
	end
	if npc_talk_index == "n1737_eltia-4-h" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94830, true);
				 api_quest_RewardQuestUser(userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94830, true);
				 api_quest_RewardQuestUser(userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94830, true);
				 api_quest_RewardQuestUser(userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94830, true);
				 api_quest_RewardQuestUser(userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94830, true);
				 api_quest_RewardQuestUser(userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94830, true);
				 api_quest_RewardQuestUser(userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94830, true);
				 api_quest_RewardQuestUser(userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94830, true);
				 api_quest_RewardQuestUser(userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94830, true);
				 api_quest_RewardQuestUser(userObjID, 94830, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-4-i" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9484, 2);
					api_quest_SetQuestStep(userObjID, 9484, 1);
					api_quest_SetJournalStep(userObjID, 9484, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-4-j" then 
	end
	if npc_talk_index == "n1737_eltia-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1737_eltia-1", "mqc04_9484_for_close.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n037_geraint--------------------------------------------------------------------------------
function mqc04_9483_nest_OnTalk_n037_geraint(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n037_geraint-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n037_geraint-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n037_geraint-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n037_geraint-2-b" then 
	end
	if npc_talk_index == "n037_geraint-2-c" then 
				api_quest_SetQuestStep(userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc04_9483_nest_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9483);
end

function mqc04_9483_nest_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9483);
end

function mqc04_9483_nest_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9483);
	local questID=9483;
end

function mqc04_9483_nest_OnRemoteStart( userObjID, questID )
end

function mqc04_9483_nest_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc04_9483_nest_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9483, 2);
				api_quest_SetJournalStep(userObjID,9483, 1);
				api_quest_SetQuestStep(userObjID,9483, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</VillageServer>

<GameServer>
function mqc04_9483_nest_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 42 then
		mqc04_9483_nest_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1739 then
		mqc04_9483_nest_OnTalk_n1739_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1737 then
		mqc04_9483_nest_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 37 then
		mqc04_9483_nest_OnTalk_n037_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n042_general_duglars--------------------------------------------------------------------------------
function mqc04_9483_nest_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n042_general_duglars-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n042_general_duglars-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9483, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9483, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9483, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-1-b" then 
	end
	if npc_talk_index == "n042_general_duglars-1-c" then 
	end
	if npc_talk_index == "n042_general_duglars-1-d" then 
	end
	if npc_talk_index == "n042_general_duglars-1-e" then 
	end
	if npc_talk_index == "n042_general_duglars-1-level_chk" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 28 then
									npc_talk_index = "n042_general_duglars-1-f";

				else
									npc_talk_index = "n042_general_duglars-1-i";

				end
	end
	if npc_talk_index == "n042_general_duglars-1-g" then 
	end
	if npc_talk_index == "n042_general_duglars-1-h" then 
	end
	if npc_talk_index == "n042_general_duglars-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1739_kaye--------------------------------------------------------------------------------
function mqc04_9483_nest_OnTalk_n1739_kaye( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1739_kaye-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1739_kaye-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1739_kaye-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1739_kaye-3-b" then 
	end
	if npc_talk_index == "n1739_kaye-3-c" then 
	end
	if npc_talk_index == "n1739_kaye-3-d" then 
	end
	if npc_talk_index == "n1739_kaye-3-e" then 
	end
	if npc_talk_index == "n1739_kaye-3-f" then 
	end
	if npc_talk_index == "n1739_kaye-3-g" then 
	end
	if npc_talk_index == "n1739_kaye-3-h" then 
	end
	if npc_talk_index == "n1739_kaye-3-i" then 
	end
	if npc_talk_index == "n1739_kaye-3-j" then 
	end
	if npc_talk_index == "n1739_kaye-3-k" then 
	end
	if npc_talk_index == "n1739_kaye-3-l" then 
	end
	if npc_talk_index == "n1739_kaye-3-m" then 
	end
	if npc_talk_index == "n1739_kaye-3-n" then 
	end
	if npc_talk_index == "n1739_kaye-3-o" then 
	end
	if npc_talk_index == "n1739_kaye-3-p" then 
	end
	if npc_talk_index == "n1739_kaye-3-q" then 
	end
	if npc_talk_index == "n1739_kaye-3-r" then 
	end
	if npc_talk_index == "n1739_kaye-3-s" then 
	end
	if npc_talk_index == "n1739_kaye-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1737_eltia--------------------------------------------------------------------------------
function mqc04_9483_nest_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1737_eltia-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-4-b" then 
	end
	if npc_talk_index == "n1737_eltia-4-c" then 
	end
	if npc_talk_index == "n1737_eltia-4-d" then 
	end
	if npc_talk_index == "n1737_eltia-4-e" then 
	end
	if npc_talk_index == "n1737_eltia-4-f" then 
	end
	if npc_talk_index == "n1737_eltia-4-g" then 
	end
	if npc_talk_index == "n1737_eltia-4-h" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94830, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-4-i" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9484, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9484, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9484, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-4-j" then 
	end
	if npc_talk_index == "n1737_eltia-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1737_eltia-1", "mqc04_9484_for_close.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n037_geraint--------------------------------------------------------------------------------
function mqc04_9483_nest_OnTalk_n037_geraint( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n037_geraint-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n037_geraint-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n037_geraint-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n037_geraint-2-b" then 
	end
	if npc_talk_index == "n037_geraint-2-c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc04_9483_nest_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9483);
end

function mqc04_9483_nest_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9483);
end

function mqc04_9483_nest_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9483);
	local questID=9483;
end

function mqc04_9483_nest_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc04_9483_nest_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc04_9483_nest_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9483, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9483, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9483, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</GameServer>