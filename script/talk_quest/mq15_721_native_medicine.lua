<VillageServer>

function mq15_721_native_medicine_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 701 then
		mq15_721_native_medicine_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_721_native_medicine_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 713 then
		mq15_721_native_medicine_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_721_native_medicine_OnTalk_n701_oldkarakule(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest(userObjID,721, 2);
				api_quest_SetJournalStep(userObjID,721, 1);
				api_quest_SetQuestStep(userObjID,721, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-i" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300383, 3);
				api_quest_SetCountingInfo(userObjID, questID, 1, 3, 300382, 1);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300384, 1);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 1559, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 201559, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 1402, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 201402, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 1391, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 8, 2, 201391, 30000);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_721_native_medicine_OnTalk_n707_sidel(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n707_sidel-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-3-h" then 
	end
	if npc_talk_index == "n707_sidel-3-i" then 
	end
	if npc_talk_index == "n707_sidel-3-j" then 
	end
	if npc_talk_index == "n707_sidel-3-k" then 
	end
	if npc_talk_index == "n707_sidel-3-l" then 
	end
	if npc_talk_index == "n707_sidel-3-m" then 
	end
	if npc_talk_index == "n707_sidel-3-n" then 
	end
	if npc_talk_index == "n707_sidel-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n713_soceress_tamara--------------------------------------------------------------------------------
function mq15_721_native_medicine_OnTalk_n713_soceress_tamara(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n713_soceress_tamara-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n713_soceress_tamara-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-4-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 7211, true);
				 api_quest_RewardQuestUser(userObjID, 7211, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 7212, true);
				 api_quest_RewardQuestUser(userObjID, 7212, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 7213, true);
				 api_quest_RewardQuestUser(userObjID, 7213, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 7214, true);
				 api_quest_RewardQuestUser(userObjID, 7214, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 7215, true);
				 api_quest_RewardQuestUser(userObjID, 7215, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 7216, true);
				 api_quest_RewardQuestUser(userObjID, 7216, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 7211, true);
				 api_quest_RewardQuestUser(userObjID, 7211, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 7211, true);
				 api_quest_RewardQuestUser(userObjID, 7211, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 722, 2);
					api_quest_SetQuestStep(userObjID, 722, 1);
					api_quest_SetJournalStep(userObjID, 722, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n713_soceress_tamara-1", "mq15_722_regret.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_721_native_medicine_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 721);
	if qstep == 2 and CountIndex == 300383 then

	end
	if qstep == 2 and CountIndex == 300382 then

	end
	if qstep == 2 and CountIndex == 300384 then

	end
	if qstep == 2 and CountIndex == 1559 then
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201559 then
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 1402 then
				if api_quest_HasQuestItem(userObjID, 300383, 3) >= 3 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 500 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300383, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300383, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201402 then
				if api_quest_HasQuestItem(userObjID, 300383, 3) >= 3 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 500 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300383, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300383, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 1391 then
				if api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300384, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300384, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201391 then
				if api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem(userObjID, 300384, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300384, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem(userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem(userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				end

	end
end

function mq15_721_native_medicine_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 721);
	if qstep == 2 and CountIndex == 300383 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300382 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300384 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1559 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201559 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1402 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201402 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1391 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201391 and Count >= TargetCount  then

	end
end

function mq15_721_native_medicine_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 721);
	local questID=721;
end

function mq15_721_native_medicine_OnRemoteStart( userObjID, questID )
end

function mq15_721_native_medicine_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_721_native_medicine_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,721, 2);
				api_quest_SetJournalStep(userObjID,721, 1);
				api_quest_SetQuestStep(userObjID,721, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</VillageServer>

<GameServer>
function mq15_721_native_medicine_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 701 then
		mq15_721_native_medicine_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 707 then
		mq15_721_native_medicine_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 713 then
		mq15_721_native_medicine_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n701_oldkarakule--------------------------------------------------------------------------------
function mq15_721_native_medicine_OnTalk_n701_oldkarakule( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n701_oldkarakule-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n701_oldkarakule-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n701_oldkarakule-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n701_oldkarakule-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n701_oldkarakule-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,721, 2);
				api_quest_SetJournalStep( pRoom, userObjID,721, 1);
				api_quest_SetQuestStep( pRoom, userObjID,721, 1);
				npc_talk_index = "n701_oldkarakule-1";

	end
	if npc_talk_index == "n701_oldkarakule-1-b" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-c" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-d" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-e" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-f" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-g" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-h" then 
	end
	if npc_talk_index == "n701_oldkarakule-1-i" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300383, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 3, 300382, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300384, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 1559, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 201559, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 1402, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 201402, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 1391, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 8, 2, 201391, 30000);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n707_sidel--------------------------------------------------------------------------------
function mq15_721_native_medicine_OnTalk_n707_sidel( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n707_sidel-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n707_sidel-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n707_sidel-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n707_sidel-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n707_sidel-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n707_sidel-3-b" then 
	end
	if npc_talk_index == "n707_sidel-3-c" then 
	end
	if npc_talk_index == "n707_sidel-3-d" then 
	end
	if npc_talk_index == "n707_sidel-3-e" then 
	end
	if npc_talk_index == "n707_sidel-3-f" then 
	end
	if npc_talk_index == "n707_sidel-3-g" then 
	end
	if npc_talk_index == "n707_sidel-3-h" then 
	end
	if npc_talk_index == "n707_sidel-3-i" then 
	end
	if npc_talk_index == "n707_sidel-3-j" then 
	end
	if npc_talk_index == "n707_sidel-3-k" then 
	end
	if npc_talk_index == "n707_sidel-3-l" then 
	end
	if npc_talk_index == "n707_sidel-3-m" then 
	end
	if npc_talk_index == "n707_sidel-3-n" then 
	end
	if npc_talk_index == "n707_sidel-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n713_soceress_tamara--------------------------------------------------------------------------------
function mq15_721_native_medicine_OnTalk_n713_soceress_tamara( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n713_soceress_tamara-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n713_soceress_tamara-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n713_soceress_tamara-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n713_soceress_tamara-4-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7211, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7211, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7212, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7212, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7213, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7213, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7214, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7214, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7215, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7215, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7216, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7216, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7211, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7211, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 7211, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 7211, questID, 1);
			 end 
	end
	if npc_talk_index == "n713_soceress_tamara-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 722, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 722, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 722, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n713_soceress_tamara-1", "mq15_722_regret.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_721_native_medicine_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 721);
	if qstep == 2 and CountIndex == 300383 then

	end
	if qstep == 2 and CountIndex == 300382 then

	end
	if qstep == 2 and CountIndex == 300384 then

	end
	if qstep == 2 and CountIndex == 1559 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201559 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300382, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300382, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 1402 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 500 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300383, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300383, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201402 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if math.random(1,1000) <= 500 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300383, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300383, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 1391 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300384, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300384, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 201391 then
				if api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300384, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300384, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 300382, 1) >= 1  and api_quest_HasQuestItem( pRoom, userObjID, 300383, 3) >= 3  and api_quest_HasQuestItem( pRoom, userObjID, 300384, 1) >= 1 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				end

	end
end

function mq15_721_native_medicine_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 721);
	if qstep == 2 and CountIndex == 300383 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300382 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300384 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1559 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201559 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1402 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201402 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 1391 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 201391 and Count >= TargetCount  then

	end
end

function mq15_721_native_medicine_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 721);
	local questID=721;
end

function mq15_721_native_medicine_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_721_native_medicine_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_721_native_medicine_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,721, 2);
				api_quest_SetJournalStep( pRoom, userObjID,721, 1);
				api_quest_SetQuestStep( pRoom, userObjID,721, 1);
				npc_talk_index = "n701_oldkarakule-1";
end

</GameServer>