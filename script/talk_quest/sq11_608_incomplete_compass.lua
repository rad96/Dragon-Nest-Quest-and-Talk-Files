<VillageServer>

function sq11_608_incomplete_compass_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 112 then
		sq11_608_incomplete_compass_OnTalk_n112_wisp_kevin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		sq11_608_incomplete_compass_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n112_wisp_kevin--------------------------------------------------------------------------------
function sq11_608_incomplete_compass_OnTalk_n112_wisp_kevin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n112_wisp_kevin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n112_wisp_kevin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n112_wisp_kevin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n112_wisp_kevin-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6080, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6080, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6080, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6080, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6080, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6080, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6080, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6080, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6080, false);
			 end 

	end
	if npc_talk_index == "n112_wisp_kevin-accepting-acceppted" then
				api_quest_AddQuest(userObjID,608, 1);
				api_quest_SetJournalStep(userObjID,608, 1);
				api_quest_SetQuestStep(userObjID,608, 1);
				api_npc_NextTalk(userObjID, npcObjID, "n112_wisp_kevin-1-a", npc_talk_target);

	end
	if npc_talk_index == "n112_wisp_kevin-1-b" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-c" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-d" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-e" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-f" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-g" then 
	end
	if npc_talk_index == "n112_wisp_kevin-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300299, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300299, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_608_incomplete_compass_OnTalk_n045_soceress_master_stella(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n045_soceress_master_stella-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);

				if api_quest_HasQuestItem(userObjID, 300299, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300299, api_quest_HasQuestItem(userObjID, 300299, 1));
				end
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 300300, 5);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end
	if npc_talk_index == "n045_soceress_master_stella-4-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 6080, true);
				 api_quest_RewardQuestUser(userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 6080, true);
				 api_quest_RewardQuestUser(userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 6080, true);
				 api_quest_RewardQuestUser(userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 6080, true);
				 api_quest_RewardQuestUser(userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 6080, true);
				 api_quest_RewardQuestUser(userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 6080, true);
				 api_quest_RewardQuestUser(userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 6080, true);
				 api_quest_RewardQuestUser(userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 6080, true);
				 api_quest_RewardQuestUser(userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 6080, true);
				 api_quest_RewardQuestUser(userObjID, 6080, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem(userObjID, 300300, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300300, api_quest_HasQuestItem(userObjID, 300300, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 609, 1);
					api_quest_SetQuestStep(userObjID, 609, 1);
					api_quest_SetJournalStep(userObjID, 609, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n045_soceress_master_stella-1", "sq11_609_foretell_compass.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_608_incomplete_compass_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 608);
	if qstep == 3 and CountIndex == 300300 then

	end
end

function sq11_608_incomplete_compass_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 608);
	if qstep == 3 and CountIndex == 300300 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

	end
end

function sq11_608_incomplete_compass_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 608);
	local questID=608;
end

function sq11_608_incomplete_compass_OnRemoteStart( userObjID, questID )
end

function sq11_608_incomplete_compass_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq11_608_incomplete_compass_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function sq11_608_incomplete_compass_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 112 then
		sq11_608_incomplete_compass_OnTalk_n112_wisp_kevin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 45 then
		sq11_608_incomplete_compass_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n112_wisp_kevin--------------------------------------------------------------------------------
function sq11_608_incomplete_compass_OnTalk_n112_wisp_kevin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n112_wisp_kevin-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n112_wisp_kevin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n112_wisp_kevin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n112_wisp_kevin-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, false);
			 end 

	end
	if npc_talk_index == "n112_wisp_kevin-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,608, 1);
				api_quest_SetJournalStep( pRoom, userObjID,608, 1);
				api_quest_SetQuestStep( pRoom, userObjID,608, 1);
				api_npc_NextTalk( pRoom, userObjID, npcObjID, "n112_wisp_kevin-1-a", npc_talk_target);

	end
	if npc_talk_index == "n112_wisp_kevin-1-b" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-c" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-d" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-e" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-f" then 
	end
	if npc_talk_index == "n112_wisp_kevin-1-g" then 
	end
	if npc_talk_index == "n112_wisp_kevin-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300299, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300299, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n045_soceress_master_stella--------------------------------------------------------------------------------
function sq11_608_incomplete_compass_OnTalk_n045_soceress_master_stella( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n045_soceress_master_stella-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n045_soceress_master_stella-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n045_soceress_master_stella-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n045_soceress_master_stella-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n045_soceress_master_stella-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-d" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-e" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-f" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-g" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-2-h" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);

				if api_quest_HasQuestItem( pRoom, userObjID, 300299, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300299, api_quest_HasQuestItem( pRoom, userObjID, 300299, 1));
				end
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 300300, 5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end
	if npc_talk_index == "n045_soceress_master_stella-4-b" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-c" then 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6080, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 6080, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 6080, questID, 1);
			 end 
	end
	if npc_talk_index == "n045_soceress_master_stella-4-!next" then 
		local cqresult = 1

				if api_quest_HasQuestItem( pRoom, userObjID, 300300, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300300, api_quest_HasQuestItem( pRoom, userObjID, 300300, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 609, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 609, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 609, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n045_soceress_master_stella-1", "sq11_609_foretell_compass.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq11_608_incomplete_compass_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 608);
	if qstep == 3 and CountIndex == 300300 then

	end
end

function sq11_608_incomplete_compass_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 608);
	if qstep == 3 and CountIndex == 300300 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

	end
end

function sq11_608_incomplete_compass_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 608);
	local questID=608;
end

function sq11_608_incomplete_compass_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq11_608_incomplete_compass_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq11_608_incomplete_compass_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>