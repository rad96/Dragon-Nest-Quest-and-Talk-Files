<VillageServer>

function mqc14_9421_swallowed_word_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1537 then
		mqc14_9421_swallowed_word_OnTalk_n1537_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9421_swallowed_word_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1516 then
		mqc14_9421_swallowed_word_OnTalk_n1516_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1537_gereint_kid--------------------------------------------------------------------------------
function mqc14_9421_swallowed_word_OnTalk_n1537_gereint_kid(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1537_gereint_kid-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9421_swallowed_word_OnTalk_n1515_for_memory(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9421, 2);
				api_quest_SetJournalStep(userObjID,9421, 1);
				api_quest_SetQuestStep(userObjID,9421, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1516_argenta--------------------------------------------------------------------------------
function mqc14_9421_swallowed_word_OnTalk_n1516_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1516_argenta-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1516_argenta-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1516_argenta-1-b" then 
	end
	if npc_talk_index == "n1516_argenta-1-c" then 
	end
	if npc_talk_index == "n1516_argenta-1-c" then 
	end
	if npc_talk_index == "n1516_argenta-1-c" then 
	end
	if npc_talk_index == "n1516_argenta-1-d" then 
	end
	if npc_talk_index == "n1516_argenta-1-e" then 
	end
	if npc_talk_index == "n1516_argenta-1-f" then 
	end
	if npc_talk_index == "n1516_argenta-1-g" then 
	end
	if npc_talk_index == "n1516_argenta-1-h" then 
	end
	if npc_talk_index == "n1516_argenta-1-h" then 
	end
	if npc_talk_index == "n1516_argenta-1-h" then 
	end
	if npc_talk_index == "n1516_argenta-1-i" then 
	end
	if npc_talk_index == "n1516_argenta-1-j" then 
	end
	if npc_talk_index == "n1516_argenta-1-j" then 
	end
	if npc_talk_index == "n1516_argenta-1-j" then 
	end
	if npc_talk_index == "n1516_argenta-1-k" then 
	end
	if npc_talk_index == "n1516_argenta-1-l" then 
	end
	if npc_talk_index == "n1516_argenta-1-m" then 
	end
	if npc_talk_index == "n1516_argenta-1-n" then 
	end
	if npc_talk_index == "n1516_argenta-1-o" then 
	end
	if npc_talk_index == "n1516_argenta-1-p" then 
	end
	if npc_talk_index == "n1516_argenta-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1516_argenta-2-b" then 
	end
	if npc_talk_index == "n1516_argenta-2-c" then 
	end
	if npc_talk_index == "n1516_argenta-2-d" then 
	end
	if npc_talk_index == "n1516_argenta-2-e" then 
	end
	if npc_talk_index == "n1516_argenta-2-f" then 
	end
	if npc_talk_index == "n1516_argenta-2-g" then 
	end
	if npc_talk_index == "n1516_argenta-2-h" then 
	end
	if npc_talk_index == "n1516_argenta-2-i" then 
	end
	if npc_talk_index == "n1516_argenta-2-j" then 
	end
	if npc_talk_index == "n1516_argenta-2-k" then 
	end
	if npc_talk_index == "n1516_argenta-2-l" then 
	end
	if npc_talk_index == "n1516_argenta-2-m" then 
	end
	if npc_talk_index == "n1516_argenta-2-n" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 94210, true);
				 api_quest_RewardQuestUser(userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 94210, true);
				 api_quest_RewardQuestUser(userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 94210, true);
				 api_quest_RewardQuestUser(userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 94210, true);
				 api_quest_RewardQuestUser(userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 94210, true);
				 api_quest_RewardQuestUser(userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 94210, true);
				 api_quest_RewardQuestUser(userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 94210, true);
				 api_quest_RewardQuestUser(userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 94210, true);
				 api_quest_RewardQuestUser(userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 94210, true);
				 api_quest_RewardQuestUser(userObjID, 94210, questID, 1);
			 end 
	end
	if npc_talk_index == "n1516_argenta-2-o" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9422, 2);
					api_quest_SetQuestStep(userObjID, 9422, 1);
					api_quest_SetJournalStep(userObjID, 9422, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1516_argenta-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1516_argenta-1", "mqc14_9422_desitny.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9421_swallowed_word_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9421);
end

function mqc14_9421_swallowed_word_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9421);
end

function mqc14_9421_swallowed_word_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9421);
	local questID=9421;
end

function mqc14_9421_swallowed_word_OnRemoteStart( userObjID, questID )
end

function mqc14_9421_swallowed_word_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc14_9421_swallowed_word_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9421, 2);
				api_quest_SetJournalStep(userObjID,9421, 1);
				api_quest_SetQuestStep(userObjID,9421, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</VillageServer>

<GameServer>
function mqc14_9421_swallowed_word_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1537 then
		mqc14_9421_swallowed_word_OnTalk_n1537_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1515 then
		mqc14_9421_swallowed_word_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1516 then
		mqc14_9421_swallowed_word_OnTalk_n1516_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1537_gereint_kid--------------------------------------------------------------------------------
function mqc14_9421_swallowed_word_OnTalk_n1537_gereint_kid( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1537_gereint_kid-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1515_for_memory--------------------------------------------------------------------------------
function mqc14_9421_swallowed_word_OnTalk_n1515_for_memory( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1515_for_memory-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1515_for_memory-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1515_for_memory-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9421, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9421, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9421, 1);
				npc_talk_index = "n1515_for_memory-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1516_argenta--------------------------------------------------------------------------------
function mqc14_9421_swallowed_word_OnTalk_n1516_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1516_argenta-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1516_argenta-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1516_argenta-1-b" then 
	end
	if npc_talk_index == "n1516_argenta-1-c" then 
	end
	if npc_talk_index == "n1516_argenta-1-c" then 
	end
	if npc_talk_index == "n1516_argenta-1-c" then 
	end
	if npc_talk_index == "n1516_argenta-1-d" then 
	end
	if npc_talk_index == "n1516_argenta-1-e" then 
	end
	if npc_talk_index == "n1516_argenta-1-f" then 
	end
	if npc_talk_index == "n1516_argenta-1-g" then 
	end
	if npc_talk_index == "n1516_argenta-1-h" then 
	end
	if npc_talk_index == "n1516_argenta-1-h" then 
	end
	if npc_talk_index == "n1516_argenta-1-h" then 
	end
	if npc_talk_index == "n1516_argenta-1-i" then 
	end
	if npc_talk_index == "n1516_argenta-1-j" then 
	end
	if npc_talk_index == "n1516_argenta-1-j" then 
	end
	if npc_talk_index == "n1516_argenta-1-j" then 
	end
	if npc_talk_index == "n1516_argenta-1-k" then 
	end
	if npc_talk_index == "n1516_argenta-1-l" then 
	end
	if npc_talk_index == "n1516_argenta-1-m" then 
	end
	if npc_talk_index == "n1516_argenta-1-n" then 
	end
	if npc_talk_index == "n1516_argenta-1-o" then 
	end
	if npc_talk_index == "n1516_argenta-1-p" then 
	end
	if npc_talk_index == "n1516_argenta-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1516_argenta-2-b" then 
	end
	if npc_talk_index == "n1516_argenta-2-c" then 
	end
	if npc_talk_index == "n1516_argenta-2-d" then 
	end
	if npc_talk_index == "n1516_argenta-2-e" then 
	end
	if npc_talk_index == "n1516_argenta-2-f" then 
	end
	if npc_talk_index == "n1516_argenta-2-g" then 
	end
	if npc_talk_index == "n1516_argenta-2-h" then 
	end
	if npc_talk_index == "n1516_argenta-2-i" then 
	end
	if npc_talk_index == "n1516_argenta-2-j" then 
	end
	if npc_talk_index == "n1516_argenta-2-k" then 
	end
	if npc_talk_index == "n1516_argenta-2-l" then 
	end
	if npc_talk_index == "n1516_argenta-2-m" then 
	end
	if npc_talk_index == "n1516_argenta-2-n" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94210, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 94210, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 94210, questID, 1);
			 end 
	end
	if npc_talk_index == "n1516_argenta-2-o" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9422, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9422, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9422, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1516_argenta-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1516_argenta-1", "mqc14_9422_desitny.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc14_9421_swallowed_word_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9421);
end

function mqc14_9421_swallowed_word_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9421);
end

function mqc14_9421_swallowed_word_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9421);
	local questID=9421;
end

function mqc14_9421_swallowed_word_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc14_9421_swallowed_word_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc14_9421_swallowed_word_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9421, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9421, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9421, 1);
				npc_talk_index = "n1515_for_memory-1";
end

</GameServer>