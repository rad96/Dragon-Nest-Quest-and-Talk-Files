<VillageServer>

function sq34_4422_foolish_history_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 88 then
		sq34_4422_foolish_history_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq34_4422_foolish_history_OnTalk_n088_scholar_starshy(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-c" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 44220, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 44220, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 44220, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 44220, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 44220, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 44220, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 44220, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 44220, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 44220, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4422, 1);
				api_quest_SetJournalStep(userObjID,4422, 1);
				api_quest_SetQuestStep(userObjID,4422, 1);
				npc_talk_index = "n088_scholar_starshy-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 601866, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 701866, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 601875, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 601876, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 601877, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 701875, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 701876, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 701877, 30000);

	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-k" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-l" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 44220, true);
				 api_quest_RewardQuestUser(userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 44220, true);
				 api_quest_RewardQuestUser(userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 44220, true);
				 api_quest_RewardQuestUser(userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 44220, true);
				 api_quest_RewardQuestUser(userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 44220, true);
				 api_quest_RewardQuestUser(userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 44220, true);
				 api_quest_RewardQuestUser(userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 44220, true);
				 api_quest_RewardQuestUser(userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 44220, true);
				 api_quest_RewardQuestUser(userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 44220, true);
				 api_quest_RewardQuestUser(userObjID, 44220, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-2-o" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem(userObjID, 400410, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400410, api_quest_HasQuestItem(userObjID, 400410, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n088_scholar_starshy-2-n" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "-1", ".xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4422_foolish_history_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4422);
	if qstep == 1 and CountIndex == 601866 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 701866 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 601875 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 601876 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 601877 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 701875 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 701876 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
	if qstep == 1 and CountIndex == 701877 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_ClearCountingInfo(userObjID, questID);

	end
end

function sq34_4422_foolish_history_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4422);
	if qstep == 1 and CountIndex == 601866 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 701866 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 601875 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 601876 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 601877 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 701875 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 701876 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 701877 and Count >= TargetCount  then

	end
end

function sq34_4422_foolish_history_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4422);
	local questID=4422;
end

function sq34_4422_foolish_history_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 601866, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 701866, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 601875, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 601876, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 601877, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 701875, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 701876, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 701877, 30000);
end

function sq34_4422_foolish_history_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq34_4422_foolish_history_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4422, 1);
				api_quest_SetJournalStep(userObjID,4422, 1);
				api_quest_SetQuestStep(userObjID,4422, 1);
				npc_talk_index = "n088_scholar_starshy-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 601866, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 701866, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 2, 601875, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 3, 2, 601876, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 4, 2, 601877, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 5, 2, 701875, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 6, 2, 701876, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 7, 2, 701877, 30000);
end

</VillageServer>

<GameServer>
function sq34_4422_foolish_history_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 88 then
		sq34_4422_foolish_history_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n088_scholar_starshy--------------------------------------------------------------------------------
function sq34_4422_foolish_history_OnTalk_n088_scholar_starshy( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n088_scholar_starshy-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n088_scholar_starshy-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n088_scholar_starshy-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n088_scholar_starshy-accepting-c" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, false);
			 end 

	end
	if npc_talk_index == "n088_scholar_starshy-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4422, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4422, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4422, 1);
				npc_talk_index = "n088_scholar_starshy-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 601866, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 701866, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 601875, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 601876, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 601877, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 701875, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 701876, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 701877, 30000);

	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-b" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-c" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-d" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-e" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-f" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-g" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-h" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-i" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-j" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-k" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-l" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44220, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44220, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44220, questID, 1);
			 end 
	end
	if npc_talk_index == "n088_scholar_starshy-2-o" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					
				if api_quest_HasQuestItem( pRoom, userObjID, 400410, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400410, api_quest_HasQuestItem( pRoom, userObjID, 400410, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n088_scholar_starshy-2-n" then 
	end
	if npc_talk_index == "n088_scholar_starshy-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "-1", ".xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq34_4422_foolish_history_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4422);
	if qstep == 1 and CountIndex == 601866 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 701866 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 601875 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 601876 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 601877 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 701875 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 701876 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
	if qstep == 1 and CountIndex == 701877 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400410, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400410, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);

	end
end

function sq34_4422_foolish_history_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4422);
	if qstep == 1 and CountIndex == 601866 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 701866 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 601875 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 601876 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 601877 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 701875 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 701876 and Count >= TargetCount  then

	end
	if qstep == 1 and CountIndex == 701877 and Count >= TargetCount  then

	end
end

function sq34_4422_foolish_history_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4422);
	local questID=4422;
end

function sq34_4422_foolish_history_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 601866, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 701866, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 601875, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 601876, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 601877, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 701875, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 701876, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 701877, 30000);
end

function sq34_4422_foolish_history_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq34_4422_foolish_history_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4422, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4422, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4422, 1);
				npc_talk_index = "n088_scholar_starshy-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 601866, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 701866, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 2, 601875, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 3, 2, 601876, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 4, 2, 601877, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 5, 2, 701875, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 6, 2, 701876, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 7, 2, 701877, 30000);
end

</GameServer>