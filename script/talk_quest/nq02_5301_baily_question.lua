<VillageServer>

function nq02_5301_baily_question_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 28 then
		nq02_5301_baily_question_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function nq02_5301_baily_question_OnTalk_n028_scholar_bailey(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest(userObjID,5301, 1);
				api_quest_SetJournalStep(userObjID,5301, 1);
				api_quest_SetQuestStep(userObjID,5301, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-e" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-f" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-f" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 28, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq02_5301_baily_question_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5301);
end

function nq02_5301_baily_question_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5301);
end

function nq02_5301_baily_question_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5301);
	local questID=5301;
end

function nq02_5301_baily_question_OnRemoteStart( userObjID, questID )
end

function nq02_5301_baily_question_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq02_5301_baily_question_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,5301, 1);
				api_quest_SetJournalStep(userObjID,5301, 1);
				api_quest_SetQuestStep(userObjID,5301, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</VillageServer>

<GameServer>
function nq02_5301_baily_question_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 28 then
		nq02_5301_baily_question_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n028_scholar_bailey--------------------------------------------------------------------------------
function nq02_5301_baily_question_OnTalk_n028_scholar_bailey( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n028_scholar_bailey-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n028_scholar_bailey-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n028_scholar_bailey-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,5301, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5301, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5301, 1);
				npc_talk_index = "n028_scholar_bailey-1";

	end
	if npc_talk_index == "n028_scholar_bailey-1-b" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-c" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-d" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-e" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-f" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-f" then 
	end
	if npc_talk_index == "n028_scholar_bailey-1-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 28, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq02_5301_baily_question_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5301);
end

function nq02_5301_baily_question_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5301);
end

function nq02_5301_baily_question_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5301);
	local questID=5301;
end

function nq02_5301_baily_question_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq02_5301_baily_question_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq02_5301_baily_question_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,5301, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5301, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5301, 1);
				npc_talk_index = "n028_scholar_bailey-1";
end

</GameServer>