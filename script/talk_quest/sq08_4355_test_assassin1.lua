<VillageServer>

function sq08_4355_test_assassin1_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1193 then
		sq08_4355_test_assassin1_OnTalk_n1193_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1194 then
		sq08_4355_test_assassin1_OnTalk_n1194_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 573 then
		sq08_4355_test_assassin1_OnTalk_n573_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1193_illusion--------------------------------------------------------------------------------
function sq08_4355_test_assassin1_OnTalk_n1193_illusion(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1193_illusion-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1193_illusion-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1193_illusion-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1193_illusion-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1193_illusion-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1193_illusion-3-b" then 
	end
	if npc_talk_index == "n1193_illusion-3-c" then 
	end
	if npc_talk_index == "n1193_illusion-3-d" then 
	end
	if npc_talk_index == "n1193_illusion-3-e" then 
	end
	if npc_talk_index == "n1193_illusion-3-f" then 
	end
	if npc_talk_index == "n1193_illusion-3-g" then 
	end
	if npc_talk_index == "n1193_illusion-3-h" then 
	end
	if npc_talk_index == "n1193_illusion-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-c" then 
	end
	if npc_talk_index == "n1193_illusion-5-d" then 
	end
	if npc_talk_index == "n1193_illusion-5-e" then 
	end
	if npc_talk_index == "n1193_illusion-5-e" then 
	end
	if npc_talk_index == "n1193_illusion-5-f" then 
	end
	if npc_talk_index == "n1193_illusion-5-g" then 
	end
	if npc_talk_index == "n1193_illusion-6" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1194_lunaria--------------------------------------------------------------------------------
function sq08_4355_test_assassin1_OnTalk_n1194_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1194_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1194_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1194_lunaria-1-b" then 
	end
	if npc_talk_index == "n1194_lunaria-1-c" then 
	end
	if npc_talk_index == "n1194_lunaria-1-d" then 
	end
	if npc_talk_index == "n1194_lunaria-1-e" then 
	end
	if npc_talk_index == "n1194_lunaria-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n573_shadow_meow--------------------------------------------------------------------------------
function sq08_4355_test_assassin1_OnTalk_n573_shadow_meow(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n573_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n573_shadow_meow-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n573_shadow_meow-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n573_shadow_meow-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43550, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43550, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43550, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43550, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43550, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43550, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43550, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43550, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43550, false);
			 end 

	end
	if npc_talk_index == "n573_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4355, 1);
				api_quest_SetJournalStep(userObjID,4355, 1);
				api_quest_SetQuestStep(userObjID,4355, 1);
				npc_talk_index = "n573_shadow_meow-1";

	end
	if npc_talk_index == "n573_shadow_meow-1-b" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-c" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-d" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-e" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-f" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-member1" then 
				if api_user_IsPartymember(userObjID) == 0  then
									npc_talk_index = "n573_shadow_meow-1-h";

				else
									npc_talk_index = "n573_shadow_meow-1-g";

				end
	end
	if npc_talk_index == "n573_shadow_meow-1-changemap1" then 
				api_user_ChangeMap(userObjID,13517,1);
	end
	if npc_talk_index == "n573_shadow_meow-6-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 43550, true);
				 api_quest_RewardQuestUser(userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 43550, true);
				 api_quest_RewardQuestUser(userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 43550, true);
				 api_quest_RewardQuestUser(userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 43550, true);
				 api_quest_RewardQuestUser(userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 43550, true);
				 api_quest_RewardQuestUser(userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 43550, true);
				 api_quest_RewardQuestUser(userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 43550, true);
				 api_quest_RewardQuestUser(userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 43550, true);
				 api_quest_RewardQuestUser(userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 43550, true);
				 api_quest_RewardQuestUser(userObjID, 43550, questID, 1);
			 end 
	end
	if npc_talk_index == "n573_shadow_meow-6-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 4356, 1);
					api_quest_SetQuestStep(userObjID, 4356, 1);
					api_quest_SetJournalStep(userObjID, 4356, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n573_shadow_meow-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n573_shadow_meow-1", "sq08_4356_test_assassin2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_4355_test_assassin1_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4355);
end

function sq08_4355_test_assassin1_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4355);
end

function sq08_4355_test_assassin1_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4355);
	local questID=4355;
end

function sq08_4355_test_assassin1_OnRemoteStart( userObjID, questID )
end

function sq08_4355_test_assassin1_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_4355_test_assassin1_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4355, 1);
				api_quest_SetJournalStep(userObjID,4355, 1);
				api_quest_SetQuestStep(userObjID,4355, 1);
				npc_talk_index = "n573_shadow_meow-1";
end

</VillageServer>

<GameServer>
function sq08_4355_test_assassin1_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1193 then
		sq08_4355_test_assassin1_OnTalk_n1193_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1194 then
		sq08_4355_test_assassin1_OnTalk_n1194_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 573 then
		sq08_4355_test_assassin1_OnTalk_n573_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1193_illusion--------------------------------------------------------------------------------
function sq08_4355_test_assassin1_OnTalk_n1193_illusion( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1193_illusion-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1193_illusion-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1193_illusion-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n1193_illusion-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n1193_illusion-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1193_illusion-3-b" then 
	end
	if npc_talk_index == "n1193_illusion-3-c" then 
	end
	if npc_talk_index == "n1193_illusion-3-d" then 
	end
	if npc_talk_index == "n1193_illusion-3-e" then 
	end
	if npc_talk_index == "n1193_illusion-3-f" then 
	end
	if npc_talk_index == "n1193_illusion-3-g" then 
	end
	if npc_talk_index == "n1193_illusion-3-h" then 
	end
	if npc_talk_index == "n1193_illusion-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-b" then 
	end
	if npc_talk_index == "n1193_illusion-5-c" then 
	end
	if npc_talk_index == "n1193_illusion-5-d" then 
	end
	if npc_talk_index == "n1193_illusion-5-e" then 
	end
	if npc_talk_index == "n1193_illusion-5-e" then 
	end
	if npc_talk_index == "n1193_illusion-5-f" then 
	end
	if npc_talk_index == "n1193_illusion-5-g" then 
	end
	if npc_talk_index == "n1193_illusion-6" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1194_lunaria--------------------------------------------------------------------------------
function sq08_4355_test_assassin1_OnTalk_n1194_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1194_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1194_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1194_lunaria-1-b" then 
	end
	if npc_talk_index == "n1194_lunaria-1-c" then 
	end
	if npc_talk_index == "n1194_lunaria-1-d" then 
	end
	if npc_talk_index == "n1194_lunaria-1-e" then 
	end
	if npc_talk_index == "n1194_lunaria-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n573_shadow_meow--------------------------------------------------------------------------------
function sq08_4355_test_assassin1_OnTalk_n573_shadow_meow( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n573_shadow_meow-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n573_shadow_meow-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n573_shadow_meow-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n573_shadow_meow-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, false);
			 end 

	end
	if npc_talk_index == "n573_shadow_meow-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4355, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4355, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4355, 1);
				npc_talk_index = "n573_shadow_meow-1";

	end
	if npc_talk_index == "n573_shadow_meow-1-b" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-c" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-d" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-e" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-f" then 
	end
	if npc_talk_index == "n573_shadow_meow-1-member1" then 
				if api_user_IsPartymember( pRoom, userObjID) == 0  then
									npc_talk_index = "n573_shadow_meow-1-h";

				else
									npc_talk_index = "n573_shadow_meow-1-g";

				end
	end
	if npc_talk_index == "n573_shadow_meow-1-changemap1" then 
				api_user_ChangeMap( pRoom, userObjID,13517,1);
	end
	if npc_talk_index == "n573_shadow_meow-6-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43550, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 43550, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 43550, questID, 1);
			 end 
	end
	if npc_talk_index == "n573_shadow_meow-6-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 4356, 1);
					api_quest_SetQuestStep( pRoom, userObjID, 4356, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 4356, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n573_shadow_meow-6-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n573_shadow_meow-1", "sq08_4356_test_assassin2.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_4355_test_assassin1_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4355);
end

function sq08_4355_test_assassin1_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4355);
end

function sq08_4355_test_assassin1_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4355);
	local questID=4355;
end

function sq08_4355_test_assassin1_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_4355_test_assassin1_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_4355_test_assassin1_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4355, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4355, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4355, 1);
				npc_talk_index = "n573_shadow_meow-1";
end

</GameServer>