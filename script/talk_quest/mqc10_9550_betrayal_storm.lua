<VillageServer>

function mqc10_9550_betrayal_storm_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1737 then
		mqc10_9550_betrayal_storm_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1727 then
		mqc10_9550_betrayal_storm_OnTalk_n1727_kaye_noir(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1737_eltia--------------------------------------------------------------------------------
function mqc10_9550_betrayal_storm_OnTalk_n1737_eltia(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1737_eltia-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9550, 2);
				api_quest_SetJournalStep(userObjID,9550, 1);
				api_quest_SetQuestStep(userObjID,9550, 1);
				npc_talk_index = "n1737_eltia-1";

	end
	if npc_talk_index == "n1737_eltia-1-b" then 
	end
	if npc_talk_index == "n1737_eltia-1-c" then 
	end
	if npc_talk_index == "n1737_eltia-1-d" then 
	end
	if npc_talk_index == "n1737_eltia-1-e" then 
	end
	if npc_talk_index == "n1737_eltia-1-f" then 
	end
	if npc_talk_index == "n1737_eltia-1-g" then 
	end
	if npc_talk_index == "n1737_eltia-1-h" then 
	end
	if npc_talk_index == "n1737_eltia-1-i" then 
	end
	if npc_talk_index == "n1737_eltia-1-j" then 
	end
	if npc_talk_index == "n1737_eltia-1-k" then 
	end
	if npc_talk_index == "n1737_eltia-1-l" then 
	end
	if npc_talk_index == "n1737_eltia-1-m" then 
	end
	if npc_talk_index == "n1737_eltia-1-n" then 
	end
	if npc_talk_index == "n1737_eltia-1-o" then 
	end
	if npc_talk_index == "n1737_eltia-1-p" then 
	end
	if npc_talk_index == "n1737_eltia-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1737_eltia-3-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95500, true);
				 api_quest_RewardQuestUser(userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95500, true);
				 api_quest_RewardQuestUser(userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95500, true);
				 api_quest_RewardQuestUser(userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95500, true);
				 api_quest_RewardQuestUser(userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95500, true);
				 api_quest_RewardQuestUser(userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95500, true);
				 api_quest_RewardQuestUser(userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95500, true);
				 api_quest_RewardQuestUser(userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95500, true);
				 api_quest_RewardQuestUser(userObjID, 95500, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-3-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9551, 2);
					api_quest_SetQuestStep(userObjID, 9551, 1);
					api_quest_SetJournalStep(userObjID, 9551, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1737_eltia-1", "mqc10_9551_repeated_mistakes.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1727_kaye_noir--------------------------------------------------------------------------------
function mqc10_9550_betrayal_storm_OnTalk_n1727_kaye_noir(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1727_kaye_noir-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1727_kaye_noir-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1727_kaye_noir-2-b" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-c" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-d" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-e" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-f" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-g" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-h" then 
	end
	if npc_talk_index == "n1727_kaye_noir-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc10_9550_betrayal_storm_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9550);
end

function mqc10_9550_betrayal_storm_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9550);
end

function mqc10_9550_betrayal_storm_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9550);
	local questID=9550;
end

function mqc10_9550_betrayal_storm_OnRemoteStart( userObjID, questID )
end

function mqc10_9550_betrayal_storm_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc10_9550_betrayal_storm_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9550, 2);
				api_quest_SetJournalStep(userObjID,9550, 1);
				api_quest_SetQuestStep(userObjID,9550, 1);
				npc_talk_index = "n1737_eltia-1";
end

</VillageServer>

<GameServer>
function mqc10_9550_betrayal_storm_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1737 then
		mqc10_9550_betrayal_storm_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1727 then
		mqc10_9550_betrayal_storm_OnTalk_n1727_kaye_noir( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1737_eltia--------------------------------------------------------------------------------
function mqc10_9550_betrayal_storm_OnTalk_n1737_eltia( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1737_eltia-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1737_eltia-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1737_eltia-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1737_eltia-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1737_eltia-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9550, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9550, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9550, 1);
				npc_talk_index = "n1737_eltia-1";

	end
	if npc_talk_index == "n1737_eltia-1-b" then 
	end
	if npc_talk_index == "n1737_eltia-1-c" then 
	end
	if npc_talk_index == "n1737_eltia-1-d" then 
	end
	if npc_talk_index == "n1737_eltia-1-e" then 
	end
	if npc_talk_index == "n1737_eltia-1-f" then 
	end
	if npc_talk_index == "n1737_eltia-1-g" then 
	end
	if npc_talk_index == "n1737_eltia-1-h" then 
	end
	if npc_talk_index == "n1737_eltia-1-i" then 
	end
	if npc_talk_index == "n1737_eltia-1-j" then 
	end
	if npc_talk_index == "n1737_eltia-1-k" then 
	end
	if npc_talk_index == "n1737_eltia-1-l" then 
	end
	if npc_talk_index == "n1737_eltia-1-m" then 
	end
	if npc_talk_index == "n1737_eltia-1-n" then 
	end
	if npc_talk_index == "n1737_eltia-1-o" then 
	end
	if npc_talk_index == "n1737_eltia-1-p" then 
	end
	if npc_talk_index == "n1737_eltia-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1737_eltia-3-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95500, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95500, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95500, questID, 1);
			 end 
	end
	if npc_talk_index == "n1737_eltia-3-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9551, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9551, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9551, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1737_eltia-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1737_eltia-1", "mqc10_9551_repeated_mistakes.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1727_kaye_noir--------------------------------------------------------------------------------
function mqc10_9550_betrayal_storm_OnTalk_n1727_kaye_noir( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1727_kaye_noir-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1727_kaye_noir-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1727_kaye_noir-2-b" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-c" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-d" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-e" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-f" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-g" then 
	end
	if npc_talk_index == "n1727_kaye_noir-2-h" then 
	end
	if npc_talk_index == "n1727_kaye_noir-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc10_9550_betrayal_storm_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9550);
end

function mqc10_9550_betrayal_storm_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9550);
end

function mqc10_9550_betrayal_storm_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9550);
	local questID=9550;
end

function mqc10_9550_betrayal_storm_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc10_9550_betrayal_storm_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc10_9550_betrayal_storm_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9550, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9550, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9550, 1);
				npc_talk_index = "n1737_eltia-1";
end

</GameServer>