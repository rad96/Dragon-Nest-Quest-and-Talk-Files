<VillageServer>

function mqc18_9740_meet_again_grele_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1816 then
		mqc18_9740_meet_again_grele_OnTalk_n1816_vernicka(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2027 then
		mqc18_9740_meet_again_grele_OnTalk_n2027_fountain_grele(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2028 then
		mqc18_9740_meet_again_grele_OnTalk_n2028_fountain_answer(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1816_vernicka--------------------------------------------------------------------------------
function mqc18_9740_meet_again_grele_OnTalk_n1816_vernicka(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1816_vernicka-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1816_vernicka-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1816_vernicka-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1816_vernicka-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9740, 2);
				api_quest_SetJournalStep(userObjID,9740, 1);
				api_quest_SetQuestStep(userObjID,9740, 1);
				npc_talk_index = "n1816_vernicka-1";

	end
	if npc_talk_index == "n1816_vernicka-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 97400, true);
				 api_quest_RewardQuestUser(userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 97400, true);
				 api_quest_RewardQuestUser(userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 97400, true);
				 api_quest_RewardQuestUser(userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 97400, true);
				 api_quest_RewardQuestUser(userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 97400, true);
				 api_quest_RewardQuestUser(userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 97400, true);
				 api_quest_RewardQuestUser(userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 97400, true);
				 api_quest_RewardQuestUser(userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 97400, true);
				 api_quest_RewardQuestUser(userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 97400, true);
				 api_quest_RewardQuestUser(userObjID, 97400, questID, 1);
			 end 
	end
	if npc_talk_index == "n1816_vernicka-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9741, 2);
					api_quest_SetQuestStep(userObjID, 9741, 1);
					api_quest_SetJournalStep(userObjID, 9741, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1816_vernicka-3-c" then 
	end
	if npc_talk_index == "n1816_vernicka-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1816_vernicka-1", "mqc18_9741_child_hiver.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2027_fountain_grele--------------------------------------------------------------------------------
function mqc18_9740_meet_again_grele_OnTalk_n2027_fountain_grele(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2027_fountain_grele-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n2027_fountain_grele-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2027_fountain_grele-1-b" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-c" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-d" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-e" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-f" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-g" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-h" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-i" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-j" then 
	end
	if npc_talk_index == "n2027_fountain_grele-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2028_fountain_answer--------------------------------------------------------------------------------
function mqc18_9740_meet_again_grele_OnTalk_n2028_fountain_answer(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2028_fountain_answer-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n2028_fountain_answer-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2028_fountain_answer-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2028_fountain_answer-2-b" then 
	end
	if npc_talk_index == "n2028_fountain_answer-2-c" then 
	end
	if npc_talk_index == "n2028_fountain_answer-2-d" then 
	end
	if npc_talk_index == "n2028_fountain_answer-2-e" then 
	end
	if npc_talk_index == "n2028_fountain_answer-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc18_9740_meet_again_grele_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9740);
end

function mqc18_9740_meet_again_grele_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9740);
end

function mqc18_9740_meet_again_grele_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9740);
	local questID=9740;
end

function mqc18_9740_meet_again_grele_OnRemoteStart( userObjID, questID )
end

function mqc18_9740_meet_again_grele_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc18_9740_meet_again_grele_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9740, 2);
				api_quest_SetJournalStep(userObjID,9740, 1);
				api_quest_SetQuestStep(userObjID,9740, 1);
				npc_talk_index = "n1816_vernicka-1";
end

</VillageServer>

<GameServer>
function mqc18_9740_meet_again_grele_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1816 then
		mqc18_9740_meet_again_grele_OnTalk_n1816_vernicka( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2027 then
		mqc18_9740_meet_again_grele_OnTalk_n2027_fountain_grele( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2028 then
		mqc18_9740_meet_again_grele_OnTalk_n2028_fountain_answer( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1816_vernicka--------------------------------------------------------------------------------
function mqc18_9740_meet_again_grele_OnTalk_n1816_vernicka( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1816_vernicka-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1816_vernicka-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1816_vernicka-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1816_vernicka-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9740, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9740, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9740, 1);
				npc_talk_index = "n1816_vernicka-1";

	end
	if npc_talk_index == "n1816_vernicka-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97400, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 97400, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 97400, questID, 1);
			 end 
	end
	if npc_talk_index == "n1816_vernicka-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9741, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9741, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9741, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1816_vernicka-3-c" then 
	end
	if npc_talk_index == "n1816_vernicka-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1816_vernicka-1", "mqc18_9741_child_hiver.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2027_fountain_grele--------------------------------------------------------------------------------
function mqc18_9740_meet_again_grele_OnTalk_n2027_fountain_grele( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2027_fountain_grele-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n2027_fountain_grele-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2027_fountain_grele-1-b" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-c" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-d" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-e" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-f" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-g" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-h" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-i" then 
	end
	if npc_talk_index == "n2027_fountain_grele-1-j" then 
	end
	if npc_talk_index == "n2027_fountain_grele-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n2028_fountain_answer--------------------------------------------------------------------------------
function mqc18_9740_meet_again_grele_OnTalk_n2028_fountain_answer( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n2028_fountain_answer-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n2028_fountain_answer-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n2028_fountain_answer-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n2028_fountain_answer-2-b" then 
	end
	if npc_talk_index == "n2028_fountain_answer-2-c" then 
	end
	if npc_talk_index == "n2028_fountain_answer-2-d" then 
	end
	if npc_talk_index == "n2028_fountain_answer-2-e" then 
	end
	if npc_talk_index == "n2028_fountain_answer-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc18_9740_meet_again_grele_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9740);
end

function mqc18_9740_meet_again_grele_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9740);
end

function mqc18_9740_meet_again_grele_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9740);
	local questID=9740;
end

function mqc18_9740_meet_again_grele_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc18_9740_meet_again_grele_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc18_9740_meet_again_grele_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9740, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9740, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9740, 1);
				npc_talk_index = "n1816_vernicka-1";
end

</GameServer>