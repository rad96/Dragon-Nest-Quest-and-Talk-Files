<VillageServer>

function mq34_9390_the_man_of_start_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 249 then
		mq34_9390_the_man_of_start_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mq34_9390_the_man_of_start_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1230 then
		mq34_9390_the_man_of_start_OnTalk_n1230_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n249_saint_guard--------------------------------------------------------------------------------
function mq34_9390_the_man_of_start_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n249_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n249_saint_guard-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n249_saint_guard-accepting-acceptted_1" then
				api_quest_AddQuest(userObjID,9390, 2);
				api_quest_SetJournalStep(userObjID,9390, 1);
				api_quest_SetQuestStep(userObjID,9390, 1);
				npc_talk_index = "n249_saint_guard-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9390_the_man_of_start_OnTalk_n1067_elder_elf(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1067_elder_elf-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted_2" then
				api_quest_AddQuest(userObjID,9390, 2);
				api_quest_SetJournalStep(userObjID,9390, 1);
				api_quest_SetQuestStep(userObjID,9390, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-class_check" then 
				if api_user_GetUserClassID(userObjID) == 2 then
									npc_talk_index = "n1067_elder_elf-1-m";

				else
									npc_talk_index = "n1067_elder_elf-1-l";

				end
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-p" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-q" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-r" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-4-b" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93900, true);
				 api_quest_RewardQuestUser(userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93900, true);
				 api_quest_RewardQuestUser(userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93900, true);
				 api_quest_RewardQuestUser(userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93900, true);
				 api_quest_RewardQuestUser(userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93900, true);
				 api_quest_RewardQuestUser(userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93900, true);
				 api_quest_RewardQuestUser(userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93900, true);
				 api_quest_RewardQuestUser(userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93900, true);
				 api_quest_RewardQuestUser(userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93900, true);
				 api_quest_RewardQuestUser(userObjID, 93900, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-4-c" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9391, 2);
					api_quest_SetQuestStep(userObjID, 9391, 1);
					api_quest_SetJournalStep(userObjID, 9391, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1067_elder_elf-4-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1067_elder_elf-1", "mq34_9391_hint_of_ancient_times.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1230_white_dragon--------------------------------------------------------------------------------
function mq34_9390_the_man_of_start_OnTalk_n1230_white_dragon(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1230_white_dragon-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1230_white_dragon-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1230_white_dragon-3-class_check" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1230_white_dragon-3-b";

				else
									npc_talk_index = "n1230_white_dragon-3-a";

				end
	end
	if npc_talk_index == "n1230_white_dragon-3-c" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-c" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-g" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-i" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-i" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-i" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-j" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-k" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-l" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-m" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-n" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-chk_class" then 
				if api_user_GetUserClassID(userObjID) == 3 then
									npc_talk_index = "n1230_white_dragon-3-x";

				else
									npc_talk_index = "n1230_white_dragon-3-o";

				end
	end
	if npc_talk_index == "n1230_white_dragon-3-p" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-q" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-r" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-s" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-t" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-u" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-v" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-w" then 
	end
	if npc_talk_index == "n1230_white_dragon-4" then 
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n1230_white_dragon-3-o" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9390_the_man_of_start_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9390);
end

function mq34_9390_the_man_of_start_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9390);
end

function mq34_9390_the_man_of_start_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9390);
	local questID=9390;
end

function mq34_9390_the_man_of_start_OnRemoteStart( userObjID, questID )
end

function mq34_9390_the_man_of_start_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq34_9390_the_man_of_start_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function mq34_9390_the_man_of_start_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 249 then
		mq34_9390_the_man_of_start_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1067 then
		mq34_9390_the_man_of_start_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1230 then
		mq34_9390_the_man_of_start_OnTalk_n1230_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n249_saint_guard--------------------------------------------------------------------------------
function mq34_9390_the_man_of_start_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n249_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n249_saint_guard-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n249_saint_guard-accepting-acceptted_1" then
				api_quest_AddQuest( pRoom, userObjID,9390, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9390, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9390, 1);
				npc_talk_index = "n249_saint_guard-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1067_elder_elf--------------------------------------------------------------------------------
function mq34_9390_the_man_of_start_OnTalk_n1067_elder_elf( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1067_elder_elf-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1067_elder_elf-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1067_elder_elf-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1067_elder_elf-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1067_elder_elf-accepting-acceptted_2" then
				api_quest_AddQuest( pRoom, userObjID,9390, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9390, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9390, 1);
				npc_talk_index = "n1067_elder_elf-1";

	end
	if npc_talk_index == "n1067_elder_elf-1-b" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-c" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-e" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-f" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-g" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-h" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-i" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-j" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-k" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 2 then
									npc_talk_index = "n1067_elder_elf-1-m";

				else
									npc_talk_index = "n1067_elder_elf-1-l";

				end
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-n" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-o" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-p" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-q" then 
	end
	if npc_talk_index == "n1067_elder_elf-1-r" then 
	end
	if npc_talk_index == "n1067_elder_elf-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1067_elder_elf-4-b" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93900, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93900, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93900, questID, 1);
			 end 
	end
	if npc_talk_index == "n1067_elder_elf-4-c" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9391, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9391, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9391, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1067_elder_elf-4-d" then 
	end
	if npc_talk_index == "n1067_elder_elf-4-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1067_elder_elf-1", "mq34_9391_hint_of_ancient_times.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1230_white_dragon--------------------------------------------------------------------------------
function mq34_9390_the_man_of_start_OnTalk_n1230_white_dragon( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1230_white_dragon-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n1230_white_dragon-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1230_white_dragon-3-class_check" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1230_white_dragon-3-b";

				else
									npc_talk_index = "n1230_white_dragon-3-a";

				end
	end
	if npc_talk_index == "n1230_white_dragon-3-c" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-c" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-f" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-g" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-h" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-i" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-i" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-i" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-j" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-k" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-l" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-m" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-n" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-chk_class" then 
				if api_user_GetUserClassID( pRoom, userObjID) == 3 then
									npc_talk_index = "n1230_white_dragon-3-x";

				else
									npc_talk_index = "n1230_white_dragon-3-o";

				end
	end
	if npc_talk_index == "n1230_white_dragon-3-p" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-q" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-r" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-s" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-t" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-u" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-v" then 
	end
	if npc_talk_index == "n1230_white_dragon-3-w" then 
	end
	if npc_talk_index == "n1230_white_dragon-4" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n1230_white_dragon-3-o" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq34_9390_the_man_of_start_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9390);
end

function mq34_9390_the_man_of_start_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9390);
end

function mq34_9390_the_man_of_start_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9390);
	local questID=9390;
end

function mq34_9390_the_man_of_start_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq34_9390_the_man_of_start_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq34_9390_the_man_of_start_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>