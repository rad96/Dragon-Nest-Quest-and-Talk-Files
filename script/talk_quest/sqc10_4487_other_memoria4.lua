<VillageServer>

function sqc10_4487_other_memoria4_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1034 then
		sqc10_4487_other_memoria4_OnTalk_n1034_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 752 then
		sqc10_4487_other_memoria4_OnTalk_n752_aisha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 753 then
		sqc10_4487_other_memoria4_OnTalk_n753_night_kasius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1034_light_pole--------------------------------------------------------------------------------
function sqc10_4487_other_memoria4_OnTalk_n1034_light_pole(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1034_light_pole-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1034_light_pole-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1034_light_pole-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1034_light_pole-accepting-a" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 44870, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 44870, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 44870, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 44870, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 44870, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 44870, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 44870, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 44870, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 44870, false);
			 end 

	end
	if npc_talk_index == "n1034_light_pole-accepting-acceptted" then
				api_quest_AddQuest(userObjID,4487, 1);
				api_quest_SetJournalStep(userObjID,4487, 1);
				api_quest_SetQuestStep(userObjID,4487, 1);
				npc_talk_index = "n1034_light_pole-1";

	end
	if npc_talk_index == "n1034_light_pole-1-b" then 
	end
	if npc_talk_index == "n1034_light_pole-1-c" then 
	end
	if npc_talk_index == "n1034_light_pole-1-d" then 
	end
	if npc_talk_index == "n1034_light_pole-1-e" then 
	end
	if npc_talk_index == "n1034_light_pole-1-f" then 
	end
	if npc_talk_index == "n1034_light_pole-1-g" then 
	end
	if npc_talk_index == "n1034_light_pole-1-h" then 
	end
	if npc_talk_index == "n1034_light_pole-1-i" then 
	end
	if npc_talk_index == "n1034_light_pole-1-j" then 
	end
	if npc_talk_index == "n1034_light_pole-1-k" then 
	end
	if npc_talk_index == "n1034_light_pole-1-l" then 
	end
	if npc_talk_index == "n1034_light_pole-1-m" then 
	end
	if npc_talk_index == "n1034_light_pole-1-n" then 
	end
	if npc_talk_index == "n1034_light_pole-1-o" then 
	end
	if npc_talk_index == "n1034_light_pole-1-p" then 
	end
	if npc_talk_index == "n1034_light_pole-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n1034_light_pole-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 44870, true);
				 api_quest_RewardQuestUser(userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 44870, true);
				 api_quest_RewardQuestUser(userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 44870, true);
				 api_quest_RewardQuestUser(userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 44870, true);
				 api_quest_RewardQuestUser(userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 44870, true);
				 api_quest_RewardQuestUser(userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 44870, true);
				 api_quest_RewardQuestUser(userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 44870, true);
				 api_quest_RewardQuestUser(userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 44870, true);
				 api_quest_RewardQuestUser(userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 44870, true);
				 api_quest_RewardQuestUser(userObjID, 44870, questID, 1);
			 end 
	end
	if npc_talk_index == "n1034_light_pole-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n752_aisha--------------------------------------------------------------------------------
function sqc10_4487_other_memoria4_OnTalk_n752_aisha(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n753_night_kasius--------------------------------------------------------------------------------
function sqc10_4487_other_memoria4_OnTalk_n753_night_kasius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc10_4487_other_memoria4_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4487);
end

function sqc10_4487_other_memoria4_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4487);
end

function sqc10_4487_other_memoria4_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 4487);
	local questID=4487;
end

function sqc10_4487_other_memoria4_OnRemoteStart( userObjID, questID )
end

function sqc10_4487_other_memoria4_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sqc10_4487_other_memoria4_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,4487, 1);
				api_quest_SetJournalStep(userObjID,4487, 1);
				api_quest_SetQuestStep(userObjID,4487, 1);
				npc_talk_index = "n1034_light_pole-1";
end

</VillageServer>

<GameServer>
function sqc10_4487_other_memoria4_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1034 then
		sqc10_4487_other_memoria4_OnTalk_n1034_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 752 then
		sqc10_4487_other_memoria4_OnTalk_n752_aisha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 753 then
		sqc10_4487_other_memoria4_OnTalk_n753_night_kasius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1034_light_pole--------------------------------------------------------------------------------
function sqc10_4487_other_memoria4_OnTalk_n1034_light_pole( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1034_light_pole-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1034_light_pole-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1034_light_pole-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1034_light_pole-accepting-a" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, false);
			 end 

	end
	if npc_talk_index == "n1034_light_pole-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,4487, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4487, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4487, 1);
				npc_talk_index = "n1034_light_pole-1";

	end
	if npc_talk_index == "n1034_light_pole-1-b" then 
	end
	if npc_talk_index == "n1034_light_pole-1-c" then 
	end
	if npc_talk_index == "n1034_light_pole-1-d" then 
	end
	if npc_talk_index == "n1034_light_pole-1-e" then 
	end
	if npc_talk_index == "n1034_light_pole-1-f" then 
	end
	if npc_talk_index == "n1034_light_pole-1-g" then 
	end
	if npc_talk_index == "n1034_light_pole-1-h" then 
	end
	if npc_talk_index == "n1034_light_pole-1-i" then 
	end
	if npc_talk_index == "n1034_light_pole-1-j" then 
	end
	if npc_talk_index == "n1034_light_pole-1-k" then 
	end
	if npc_talk_index == "n1034_light_pole-1-l" then 
	end
	if npc_talk_index == "n1034_light_pole-1-m" then 
	end
	if npc_talk_index == "n1034_light_pole-1-n" then 
	end
	if npc_talk_index == "n1034_light_pole-1-o" then 
	end
	if npc_talk_index == "n1034_light_pole-1-p" then 
	end
	if npc_talk_index == "n1034_light_pole-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n1034_light_pole-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44870, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 44870, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 44870, questID, 1);
			 end 
	end
	if npc_talk_index == "n1034_light_pole-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n752_aisha--------------------------------------------------------------------------------
function sqc10_4487_other_memoria4_OnTalk_n752_aisha( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n753_night_kasius--------------------------------------------------------------------------------
function sqc10_4487_other_memoria4_OnTalk_n753_night_kasius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sqc10_4487_other_memoria4_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4487);
end

function sqc10_4487_other_memoria4_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4487);
end

function sqc10_4487_other_memoria4_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 4487);
	local questID=4487;
end

function sqc10_4487_other_memoria4_OnRemoteStart( pRoom,  userObjID, questID )
end

function sqc10_4487_other_memoria4_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sqc10_4487_other_memoria4_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,4487, 1);
				api_quest_SetJournalStep( pRoom, userObjID,4487, 1);
				api_quest_SetQuestStep( pRoom, userObjID,4487, 1);
				npc_talk_index = "n1034_light_pole-1";
end

</GameServer>