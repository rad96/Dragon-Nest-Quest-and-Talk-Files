<VillageServer>

function nq32_8607_condition_of_love_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 16 then
		nq32_8607_condition_of_love_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 62 then
		nq32_8607_condition_of_love_OnTalk_n062_trainer_lindsay(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function nq32_8607_condition_of_love_OnTalk_n016_trader_borin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n016_trader_borin-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				if api_user_HasItem(userObjID, 336617728, 5) >= 5 then
									npc_talk_index = "n016_trader_borin-2-b";

				else
									npc_talk_index = "n016_trader_borin-2-a";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n016_trader_borin-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-1-b" then 
	end
	if npc_talk_index == "n016_trader_borin-1-b" then 
	end
	if npc_talk_index == "n016_trader_borin-1-c" then 
	end
	if npc_talk_index == "n016_trader_borin-1-d" then 
	end
	if npc_talk_index == "n016_trader_borin-1-e" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 3, 336617728, 5);
	end
	if npc_talk_index == "n016_trader_borin-2-d" then 
	end
	if npc_talk_index == "n016_trader_borin-2-c" then 
	end
	if npc_talk_index == "n016_trader_borin-2-e" then 
	end
	if npc_talk_index == "n016_trader_borin-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_user_DelItem(userObjID, 336617728, 5, questID);
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300312, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300312, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo(userObjID, questID);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n062_trainer_lindsay--------------------------------------------------------------------------------
function nq32_8607_condition_of_love_OnTalk_n062_trainer_lindsay(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n062_trainer_lindsay-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n062_trainer_lindsay-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n062_trainer_lindsay-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n062_trainer_lindsay-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 86070, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 86070, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 86070, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 86070, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 86070, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 86070, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 86070, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 86070, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 86070, false);
			 end 

	end
	if npc_talk_index == "n062_trainer_lindsay-accepting-acceppted" then
				api_quest_AddQuest(userObjID,8607, 1);
				api_quest_SetJournalStep(userObjID,8607, 1);
				api_quest_SetQuestStep(userObjID,8607, 1);
				npc_talk_index = "n062_trainer_lindsay-1";

	end
	if npc_talk_index == "n062_trainer_lindsay-3-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-c" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-d" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-e" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-f" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-g" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-h" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-i" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-j" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 86070, true);
				 api_quest_RewardQuestUser(userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 86070, true);
				 api_quest_RewardQuestUser(userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 86070, true);
				 api_quest_RewardQuestUser(userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 86070, true);
				 api_quest_RewardQuestUser(userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 86070, true);
				 api_quest_RewardQuestUser(userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 86070, true);
				 api_quest_RewardQuestUser(userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 86070, true);
				 api_quest_RewardQuestUser(userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 86070, true);
				 api_quest_RewardQuestUser(userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 86070, true);
				 api_quest_RewardQuestUser(userObjID, 86070, questID, 1);
			 end 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-k" then 

				if api_quest_HasQuestItem(userObjID, 300312, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300312, api_quest_HasQuestItem(userObjID, 300312, 1));
				end

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( userObjID, 62, 600 );
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq32_8607_condition_of_love_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8607);
	if qstep == 2 and CountIndex == 336617728 then

	end
end

function nq32_8607_condition_of_love_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8607);
	if qstep == 2 and CountIndex == 336617728 and Count >= TargetCount  then

	end
end

function nq32_8607_condition_of_love_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 8607);
	local questID=8607;
end

function nq32_8607_condition_of_love_OnRemoteStart( userObjID, questID )
end

function nq32_8607_condition_of_love_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq32_8607_condition_of_love_ForceAccept( userObjID, npcObjID, questID )
end

</VillageServer>

<GameServer>
function nq32_8607_condition_of_love_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 16 then
		nq32_8607_condition_of_love_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 62 then
		nq32_8607_condition_of_love_OnTalk_n062_trainer_lindsay( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n016_trader_borin--------------------------------------------------------------------------------
function nq32_8607_condition_of_love_OnTalk_n016_trader_borin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n016_trader_borin-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n016_trader_borin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n016_trader_borin-2";
				if api_user_HasItem( pRoom, userObjID, 336617728, 5) >= 5 then
									npc_talk_index = "n016_trader_borin-2-b";

				else
									npc_talk_index = "n016_trader_borin-2-a";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n016_trader_borin-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n016_trader_borin-1-b" then 
	end
	if npc_talk_index == "n016_trader_borin-1-b" then 
	end
	if npc_talk_index == "n016_trader_borin-1-c" then 
	end
	if npc_talk_index == "n016_trader_borin-1-d" then 
	end
	if npc_talk_index == "n016_trader_borin-1-e" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 3, 336617728, 5);
	end
	if npc_talk_index == "n016_trader_borin-2-d" then 
	end
	if npc_talk_index == "n016_trader_borin-2-c" then 
	end
	if npc_talk_index == "n016_trader_borin-2-e" then 
	end
	if npc_talk_index == "n016_trader_borin-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_user_DelItem( pRoom, userObjID, 336617728, 5, questID);
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300312, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300312, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n062_trainer_lindsay--------------------------------------------------------------------------------
function nq32_8607_condition_of_love_OnTalk_n062_trainer_lindsay( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n062_trainer_lindsay-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n062_trainer_lindsay-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n062_trainer_lindsay-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n062_trainer_lindsay-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, false);
			 end 

	end
	if npc_talk_index == "n062_trainer_lindsay-accepting-acceppted" then
				api_quest_AddQuest( pRoom, userObjID,8607, 1);
				api_quest_SetJournalStep( pRoom, userObjID,8607, 1);
				api_quest_SetQuestStep( pRoom, userObjID,8607, 1);
				npc_talk_index = "n062_trainer_lindsay-1";

	end
	if npc_talk_index == "n062_trainer_lindsay-3-b" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-c" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-d" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-e" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-f" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-g" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-h" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-i" then 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-j" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86070, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 86070, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 86070, questID, 1);
			 end 
	end
	if npc_talk_index == "n062_trainer_lindsay-3-k" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300312, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300312, api_quest_HasQuestItem( pRoom, userObjID, 300312, 1));
				end

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_AddFavorPoint( pRoom,  userObjID, 62, 600 );
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq32_8607_condition_of_love_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8607);
	if qstep == 2 and CountIndex == 336617728 then

	end
end

function nq32_8607_condition_of_love_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8607);
	if qstep == 2 and CountIndex == 336617728 and Count >= TargetCount  then

	end
end

function nq32_8607_condition_of_love_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 8607);
	local questID=8607;
end

function nq32_8607_condition_of_love_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq32_8607_condition_of_love_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq32_8607_condition_of_love_ForceAccept( pRoom,  userObjID, npcObjID, questID )
end

</GameServer>