<VillageServer>

function dq15_1215_temple_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 249 then
		dq15_1215_temple_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n249_saint_guard--------------------------------------------------------------------------------
function dq15_1215_temple_OnTalk_n249_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
						if api_quest_IsPlayingQuestMaximum(userObjID) == 1  then
				else
								 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 12150, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 12150, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 12150, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 12150, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 12150, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 12150, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 12150, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 12150, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 12150, false);
			 end 

				end

			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n249_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n249_saint_guard-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n249_saint_guard-2";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 12150, true);
				 api_quest_RewardQuestUser(userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 12150, true);
				 api_quest_RewardQuestUser(userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 12150, true);
				 api_quest_RewardQuestUser(userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 12150, true);
				 api_quest_RewardQuestUser(userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 12150, true);
				 api_quest_RewardQuestUser(userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 12150, true);
				 api_quest_RewardQuestUser(userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 12150, true);
				 api_quest_RewardQuestUser(userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 12150, true);
				 api_quest_RewardQuestUser(userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 12150, true);
				 api_quest_RewardQuestUser(userObjID, 12150, questID, 1);
			 end 
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n249_saint_guard-accepting-acceptted" then
				api_quest_AddQuest(userObjID,1215, 1);
				api_quest_SetJournalStep(userObjID,1215, 1);
				api_quest_SetQuestStep(userObjID,1215, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 751, 1 );
				npc_talk_index = "n249_saint_guard-1";

	end
	if npc_talk_index == "n249_saint_guard-1-back_npc" then 
				api_npc_NextScript(userObjID, npcObjID, "quest", "n249_saint_guard.xml");
	end
	if npc_talk_index == "n249_saint_guard-2-complete_quest" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_NextScript(userObjID, npcObjID, "quest", "n249_saint_guard.xml");
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq15_1215_temple_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1215);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( userObjID ) >= 4 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

				else
				end
		 end 
	 end 
end

function dq15_1215_temple_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1215);
end

function dq15_1215_temple_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1215);
	local questID=1215;
end

function dq15_1215_temple_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 751, 1 );
end

function dq15_1215_temple_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function dq15_1215_temple_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,1215, 1);
				api_quest_SetJournalStep(userObjID,1215, 1);
				api_quest_SetQuestStep(userObjID,1215, 1);
				api_quest_SetCountingInfo(userObjID, questID, 0, 5, 751, 1 );
				npc_talk_index = "n249_saint_guard-1";
end

</VillageServer>

<GameServer>
function dq15_1215_temple_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 249 then
		dq15_1215_temple_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n249_saint_guard--------------------------------------------------------------------------------
function dq15_1215_temple_OnTalk_n249_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
						if api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1  then
				else
								 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, false);
			 end 

				end

			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n249_saint_guard-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n249_saint_guard-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n249_saint_guard-2";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12150, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12150, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12150, questID, 1);
			 end 
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n249_saint_guard-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,1215, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1215, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1215, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 751, 1 );
				npc_talk_index = "n249_saint_guard-1";

	end
	if npc_talk_index == "n249_saint_guard-1-back_npc" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "quest", "n249_saint_guard.xml");
	end
	if npc_talk_index == "n249_saint_guard-2-complete_quest" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
				api_npc_NextScript( pRoom, userObjID, npcObjID, "quest", "n249_saint_guard.xml");
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq15_1215_temple_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1215);
	 local qstep= accepting; 
	 if qstep == accepting then 
		 if api_user_GetLastStageClearRank( pRoom,  userObjID ) <= 6 then 
				if  api_user_GetStageConstructionLevel( pRoom,  userObjID ) >= 4 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

				else
				end
		 end 
	 end 
end

function dq15_1215_temple_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1215);
end

function dq15_1215_temple_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1215);
	local questID=1215;
end

function dq15_1215_temple_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 751, 1 );
end

function dq15_1215_temple_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function dq15_1215_temple_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,1215, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1215, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1215, 1);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 5, 751, 1 );
				npc_talk_index = "n249_saint_guard-1";
end

</GameServer>