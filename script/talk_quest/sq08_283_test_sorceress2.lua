<VillageServer>

function sq08_283_test_sorceress2_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 35 then
		sq08_283_test_sorceress2_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_283_test_sorceress2_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n035_soceress_master_tiana-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n035_soceress_master_tiana-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n035_soceress_master_tiana-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n035_soceress_master_tiana-6";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2830, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2830, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2830, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2830, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2830, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2830, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2830, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2830, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2830, false);
			 end 

	end
	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest(userObjID,283, 1);
				api_quest_SetJournalStep(userObjID,283, 1);
				api_quest_SetQuestStep(userObjID,283, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-g" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200413, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300032, 1);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n035_soceress_master_tiana-4" then 

				if api_quest_HasQuestItem(userObjID, 300032, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300032, api_quest_HasQuestItem(userObjID, 300032, 1));
				end
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-a_a" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				npc_talk_index = "n035_soceress_master_tiana-5";
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-b_b" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				npc_talk_index = "n035_soceress_master_tiana-5";
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-c_c" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				npc_talk_index = "n035_soceress_master_tiana-5";
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-d_d" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				npc_talk_index = "n035_soceress_master_tiana-5";
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-choice1" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
				api_quest_SetQuestMemo(userObjID, questID, 1, 1);
				npc_talk_index = "n035_soceress_master_tiana-6";
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-choice2" then 
				api_quest_SetQuestStep(userObjID, questID,6);
				api_quest_SetJournalStep(userObjID, questID, 6);
				api_quest_SetQuestMemo(userObjID, questID, 1, 2);
				npc_talk_index = "n035_soceress_master_tiana-6";
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-check_memo" then 
				if api_quest_GetQuestMemo(userObjID, questID, 1) == 1  then
									npc_talk_index = "n035_soceress_master_tiana-6-d";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 end 

				else
									if api_quest_GetQuestMemo(userObjID, questID, 1) == 2  then
									npc_talk_index = "n035_soceress_master_tiana-6-e";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 end 

				else
									if api_quest_GetQuestMemo(userObjID, questID, 1) == 3  then
									npc_talk_index = "n035_soceress_master_tiana-6-f";
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 2830, true);
				 api_quest_RewardQuestUser(userObjID, 2830, questID, 1);
			 end 

				else
				end

				end

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-5" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-change_class1" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 17);
				npc_talk_index = "n035_soceress_master_tiana-6-g";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-change_class2" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID(userObjID, 18);
				npc_talk_index = "n035_soceress_master_tiana-6-g";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-next_complete_check" then 
				if api_quest_IsMarkingCompleteQuest(userObjID, 342) == 1  then
									npc_talk_index = "n035_soceress_master_tiana-6-h";

				else
									api_quest_AddQuest(userObjID,342, 1);
				api_quest_SetJournalStep(userObjID,342, 1);
				api_quest_SetQuestStep(userObjID,342, 1);
				api_npc_NextScript(userObjID, npcObjID, "n035_soceress_master_tiana-1", "sq08_342_change_job3.xml");

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_283_test_sorceress2_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 283);
	if qstep == 2 and CountIndex == 413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300032, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300032, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300032 then

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300032, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300032, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_283_test_sorceress2_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 283);
	if qstep == 2 and CountIndex == 413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300032 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
end

function sq08_283_test_sorceress2_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 283);
	local questID=283;
end

function sq08_283_test_sorceress2_OnRemoteStart( userObjID, questID )
end

function sq08_283_test_sorceress2_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_283_test_sorceress2_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,283, 1);
				api_quest_SetJournalStep(userObjID,283, 1);
				api_quest_SetQuestStep(userObjID,283, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</VillageServer>

<GameServer>
function sq08_283_test_sorceress2_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 35 then
		sq08_283_test_sorceress2_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n035_soceress_master_tiana--------------------------------------------------------------------------------
function sq08_283_test_sorceress2_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n035_soceress_master_tiana-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n035_soceress_master_tiana-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n035_soceress_master_tiana-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n035_soceress_master_tiana-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 6 then
				npc_talk_index = "n035_soceress_master_tiana-6";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n035_soceress_master_tiana-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, false);
			 end 

	end
	if npc_talk_index == "n035_soceress_master_tiana-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,283, 1);
				api_quest_SetJournalStep( pRoom, userObjID,283, 1);
				api_quest_SetQuestStep( pRoom, userObjID,283, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";

	end
	if npc_talk_index == "n035_soceress_master_tiana-1-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-e" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-f" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-1-g" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200413, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300032, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n035_soceress_master_tiana-4" then 

				if api_quest_HasQuestItem( pRoom, userObjID, 300032, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300032, api_quest_HasQuestItem( pRoom, userObjID, 300032, 1));
				end
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-a_a" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				npc_talk_index = "n035_soceress_master_tiana-5";
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-d" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-b_b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				npc_talk_index = "n035_soceress_master_tiana-5";
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-c_c" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				npc_talk_index = "n035_soceress_master_tiana-5";
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-4-d_d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				npc_talk_index = "n035_soceress_master_tiana-5";
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-choice1" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 1);
				npc_talk_index = "n035_soceress_master_tiana-6";
	end
	if npc_talk_index == "n035_soceress_master_tiana-5-choice2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,6);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 2);
				npc_talk_index = "n035_soceress_master_tiana-6";
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-b" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-c" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-check_memo" then 
				if api_quest_GetQuestMemo( pRoom, userObjID, questID, 1) == 1  then
									npc_talk_index = "n035_soceress_master_tiana-6-d";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 end 

				else
									if api_quest_GetQuestMemo( pRoom, userObjID, questID, 1) == 2  then
									npc_talk_index = "n035_soceress_master_tiana-6-e";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 end 

				else
									if api_quest_GetQuestMemo( pRoom, userObjID, questID, 1) == 3  then
									npc_talk_index = "n035_soceress_master_tiana-6-f";
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 2830, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 2830, questID, 1);
			 end 

				else
				end

				end

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-5" then 
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-change_class1" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 17);
				npc_talk_index = "n035_soceress_master_tiana-6-g";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-change_class2" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_user_SetUserJobID( pRoom, userObjID, 18);
				npc_talk_index = "n035_soceress_master_tiana-6-g";


				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n035_soceress_master_tiana-6-next_complete_check" then 
				if api_quest_IsMarkingCompleteQuest( pRoom, userObjID, 342) == 1  then
									npc_talk_index = "n035_soceress_master_tiana-6-h";

				else
									api_quest_AddQuest( pRoom, userObjID,342, 1);
				api_quest_SetJournalStep( pRoom, userObjID,342, 1);
				api_quest_SetQuestStep( pRoom, userObjID,342, 1);
				api_npc_NextScript( pRoom, userObjID, npcObjID, "n035_soceress_master_tiana-1", "sq08_342_change_job3.xml");

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_283_test_sorceress2_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 283);
	if qstep == 2 and CountIndex == 413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300032, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300032, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 2 and CountIndex == 300032 then

	end
	if qstep == 2 and CountIndex == 200413 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300032, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300032, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
end

function sq08_283_test_sorceress2_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 283);
	if qstep == 2 and CountIndex == 413 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 300032 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

	end
	if qstep == 2 and CountIndex == 200413 and Count >= TargetCount  then

	end
end

function sq08_283_test_sorceress2_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 283);
	local questID=283;
end

function sq08_283_test_sorceress2_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_283_test_sorceress2_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_283_test_sorceress2_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,283, 1);
				api_quest_SetJournalStep( pRoom, userObjID,283, 1);
				api_quest_SetQuestStep( pRoom, userObjID,283, 1);
				npc_talk_index = "n035_soceress_master_tiana-1";
end

</GameServer>