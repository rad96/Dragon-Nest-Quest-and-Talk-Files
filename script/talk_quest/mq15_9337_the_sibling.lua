<VillageServer>

function mq15_9337_the_sibling_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1169 then
		mq15_9337_the_sibling_OnTalk_n1169_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1173 then
		mq15_9337_the_sibling_OnTalk_n1173_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1174 then
		mq15_9337_the_sibling_OnTalk_n1174_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 472 then
		mq15_9337_the_sibling_OnTalk_n472_karahan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 709 then
		mq15_9337_the_sibling_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		mq15_9337_the_sibling_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1169_lunaria--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n1169_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1169_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1169_lunaria-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1169_lunaria-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1169_lunaria-3-b" then 
	end
	if npc_talk_index == "n1169_lunaria-3-c" then 
	end
	if npc_talk_index == "n1169_lunaria-3-d" then 
	end
	if npc_talk_index == "n1169_lunaria-3-e" then 
	end
	if npc_talk_index == "n1169_lunaria-3-f" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 93370, true);
				 api_quest_RewardQuestUser(userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 93370, true);
				 api_quest_RewardQuestUser(userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 93370, true);
				 api_quest_RewardQuestUser(userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 93370, true);
				 api_quest_RewardQuestUser(userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 93370, true);
				 api_quest_RewardQuestUser(userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 93370, true);
				 api_quest_RewardQuestUser(userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 93370, true);
				 api_quest_RewardQuestUser(userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 93370, true);
				 api_quest_RewardQuestUser(userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 93370, true);
				 api_quest_RewardQuestUser(userObjID, 93370, questID, 1);
			 end 
	end
	if npc_talk_index == "n1169_lunaria-3-g" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9338, 2);
					api_quest_SetQuestStep(userObjID, 9338, 1);
					api_quest_SetJournalStep(userObjID, 9338, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1169_lunaria-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n1169_lunaria-1", "mq15_9338_the_other_times.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1173_lunaria--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n1173_lunaria(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1173_lunaria-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1174_angelica--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n1174_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n472_karahan--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n472_karahan(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n472_karahan-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n472_karahan-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n472_karahan-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n472_karahan-2-b" then 
	end
	if npc_talk_index == "n472_karahan-2-c" then 
	end
	if npc_talk_index == "n472_karahan-2-d" then 
	end
	if npc_talk_index == "n472_karahan-2-e" then 
	end
	if npc_talk_index == "n472_karahan-2-f" then 
	end
	if npc_talk_index == "n472_karahan-2-g" then 
	end
	if npc_talk_index == "n472_karahan-2-h" then 
	end
	if npc_talk_index == "n472_karahan-2-i" then 
	end
	if npc_talk_index == "n472_karahan-2-j" then 
	end
	if npc_talk_index == "n472_karahan-2-k" then 
	end
	if npc_talk_index == "n472_karahan-2-l" then 
	end
	if npc_talk_index == "n472_karahan-2-m" then 
	end
	if npc_talk_index == "n472_karahan-2-n" then 
	end
	if npc_talk_index == "n472_karahan-2-n" then 
	end
	if npc_talk_index == "n472_karahan-2-o" then 
	end
	if npc_talk_index == "n472_karahan-3" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n709_trader_lucita--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n709_trader_lucita(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n709_trader_lucita-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n709_trader_lucita-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n709_trader_lucita-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9337, 2);
				api_quest_SetJournalStep(userObjID,9337, 1);
				api_quest_SetQuestStep(userObjID,9337, 1);
				npc_talk_index = "n709_trader_lucita-1";

	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n754_cian(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n754_cian-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-1-b" then 
	end
	if npc_talk_index == "n754_cian-1-c" then 
	end
	if npc_talk_index == "n754_cian-1-d" then 
	end
	if npc_talk_index == "n754_cian-1-e" then 
	end
	if npc_talk_index == "n754_cian-1-f" then 
	end
	if npc_talk_index == "n754_cian-1-g" then 
	end
	if npc_talk_index == "n754_cian-1-h" then 
	end
	if npc_talk_index == "n754_cian-1-i" then 
	end
	if npc_talk_index == "n754_cian-1-j" then 
	end
	if npc_talk_index == "n754_cian-1-k" then 
	end
	if npc_talk_index == "n754_cian-1-l" then 
	end
	if npc_talk_index == "n754_cian-1-m" then 
	end
	if npc_talk_index == "n754_cian-1-n" then 
	end
	if npc_talk_index == "n754_cian-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9337_the_sibling_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9337);
end

function mq15_9337_the_sibling_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9337);
end

function mq15_9337_the_sibling_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9337);
	local questID=9337;
end

function mq15_9337_the_sibling_OnRemoteStart( userObjID, questID )
end

function mq15_9337_the_sibling_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq15_9337_the_sibling_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9337, 2);
				api_quest_SetJournalStep(userObjID,9337, 1);
				api_quest_SetQuestStep(userObjID,9337, 1);
				npc_talk_index = "n709_trader_lucita-1";
end

</VillageServer>

<GameServer>
function mq15_9337_the_sibling_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1169 then
		mq15_9337_the_sibling_OnTalk_n1169_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1173 then
		mq15_9337_the_sibling_OnTalk_n1173_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1174 then
		mq15_9337_the_sibling_OnTalk_n1174_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 472 then
		mq15_9337_the_sibling_OnTalk_n472_karahan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 709 then
		mq15_9337_the_sibling_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 754 then
		mq15_9337_the_sibling_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1169_lunaria--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n1169_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1169_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1169_lunaria-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n1169_lunaria-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1169_lunaria-3-b" then 
	end
	if npc_talk_index == "n1169_lunaria-3-c" then 
	end
	if npc_talk_index == "n1169_lunaria-3-d" then 
	end
	if npc_talk_index == "n1169_lunaria-3-e" then 
	end
	if npc_talk_index == "n1169_lunaria-3-f" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 93370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 93370, questID, 1);
			 end 
	end
	if npc_talk_index == "n1169_lunaria-3-g" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9338, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9338, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9338, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n1169_lunaria-3-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n1169_lunaria-1", "mq15_9338_the_other_times.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1173_lunaria--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n1173_lunaria( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1173_lunaria-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1174_angelica--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n1174_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n472_karahan--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n472_karahan( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n472_karahan-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n472_karahan-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n472_karahan-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n472_karahan-2-b" then 
	end
	if npc_talk_index == "n472_karahan-2-c" then 
	end
	if npc_talk_index == "n472_karahan-2-d" then 
	end
	if npc_talk_index == "n472_karahan-2-e" then 
	end
	if npc_talk_index == "n472_karahan-2-f" then 
	end
	if npc_talk_index == "n472_karahan-2-g" then 
	end
	if npc_talk_index == "n472_karahan-2-h" then 
	end
	if npc_talk_index == "n472_karahan-2-i" then 
	end
	if npc_talk_index == "n472_karahan-2-j" then 
	end
	if npc_talk_index == "n472_karahan-2-k" then 
	end
	if npc_talk_index == "n472_karahan-2-l" then 
	end
	if npc_talk_index == "n472_karahan-2-m" then 
	end
	if npc_talk_index == "n472_karahan-2-n" then 
	end
	if npc_talk_index == "n472_karahan-2-n" then 
	end
	if npc_talk_index == "n472_karahan-2-o" then 
	end
	if npc_talk_index == "n472_karahan-3" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n709_trader_lucita--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n709_trader_lucita( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n709_trader_lucita-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n709_trader_lucita-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n709_trader_lucita-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9337, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9337, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9337, 1);
				npc_talk_index = "n709_trader_lucita-1";

	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n754_cian--------------------------------------------------------------------------------
function mq15_9337_the_sibling_OnTalk_n754_cian( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n754_cian-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n754_cian-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n754_cian-1-b" then 
	end
	if npc_talk_index == "n754_cian-1-c" then 
	end
	if npc_talk_index == "n754_cian-1-d" then 
	end
	if npc_talk_index == "n754_cian-1-e" then 
	end
	if npc_talk_index == "n754_cian-1-f" then 
	end
	if npc_talk_index == "n754_cian-1-g" then 
	end
	if npc_talk_index == "n754_cian-1-h" then 
	end
	if npc_talk_index == "n754_cian-1-i" then 
	end
	if npc_talk_index == "n754_cian-1-j" then 
	end
	if npc_talk_index == "n754_cian-1-k" then 
	end
	if npc_talk_index == "n754_cian-1-l" then 
	end
	if npc_talk_index == "n754_cian-1-m" then 
	end
	if npc_talk_index == "n754_cian-1-n" then 
	end
	if npc_talk_index == "n754_cian-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq15_9337_the_sibling_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9337);
end

function mq15_9337_the_sibling_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9337);
end

function mq15_9337_the_sibling_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9337);
	local questID=9337;
end

function mq15_9337_the_sibling_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq15_9337_the_sibling_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq15_9337_the_sibling_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9337, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9337, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9337, 1);
				npc_talk_index = "n709_trader_lucita-1";
end

</GameServer>