<VillageServer>

function mq11_437_cooperatively_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 393 then
		mq11_437_cooperatively_OnTalk_n393_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mq11_437_cooperatively_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 520 then
		mq11_437_cooperatively_OnTalk_n520_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 528 then
		mq11_437_cooperatively_OnTalk_n528_acamedic_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n393_academic_station--------------------------------------------------------------------------------
function mq11_437_cooperatively_OnTalk_n393_academic_station(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n393_academic_station-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n393_academic_station-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n393_academic_station-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n393_academic_station-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n393_academic_station-1-b" then 
	end
	if npc_talk_index == "n393_academic_station-1-c" then 
	end
	if npc_talk_index == "n393_academic_station-1-d" then 
	end
	if npc_talk_index == "n393_academic_station-1-e" then 
	end
	if npc_talk_index == "n393_academic_station-1-f" then 
	end
	if npc_talk_index == "n393_academic_station-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mq11_437_cooperatively_OnTalk_n042_general_duglars(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n042_general_duglars-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest(userObjID,437, 2);
				api_quest_SetJournalStep(userObjID,437, 1);
				api_quest_SetQuestStep(userObjID,437, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-4-b" then 
	end
	if npc_talk_index == "n042_general_duglars-4-c" then 
	end
	if npc_talk_index == "n042_general_duglars-4-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4370, true);
				 api_quest_RewardQuestUser(userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4370, true);
				 api_quest_RewardQuestUser(userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4370, true);
				 api_quest_RewardQuestUser(userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4370, true);
				 api_quest_RewardQuestUser(userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4370, true);
				 api_quest_RewardQuestUser(userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4370, true);
				 api_quest_RewardQuestUser(userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4370, true);
				 api_quest_RewardQuestUser(userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4370, true);
				 api_quest_RewardQuestUser(userObjID, 4370, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 438, 2);
					api_quest_SetQuestStep(userObjID, 438, 1);
					api_quest_SetJournalStep(userObjID, 438, 1);

					
				if api_quest_HasQuestItem(userObjID, 300479, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 300479, api_quest_HasQuestItem(userObjID, 300479, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n042_general_duglars-1", "mq11_438_fleet_of_foot.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n520_angelica--------------------------------------------------------------------------------
function mq11_437_cooperatively_OnTalk_n520_angelica(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 npc_talk_index = "n520_angelica-noquest";
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n520_angelica-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n520_angelica-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n520_angelica-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n520_angelica-2-b" then 
	end
	if npc_talk_index == "n520_angelica-2-c" then 
	end
	if npc_talk_index == "n520_angelica-2-d" then 
	end
	if npc_talk_index == "n520_angelica-2-e" then 
	end
	if npc_talk_index == "n520_angelica-2-f" then 
	end
	if npc_talk_index == "n520_angelica-2-g" then 
	end
	if npc_talk_index == "n520_angelica-2-h" then 
	end
	if npc_talk_index == "n520_angelica-2-i" then 
	end
	if npc_talk_index == "n520_angelica-2-j" then 
	end
	if npc_talk_index == "n520_angelica-2-k" then 
	end
	if npc_talk_index == "n520_angelica-2-l" then 
	end
	if npc_talk_index == "n520_angelica-2-m" then 
	end
	if npc_talk_index == "n520_angelica-2-n" then 
	end
	if npc_talk_index == "n520_angelica-2-o" then 
	end
	if npc_talk_index == "n520_angelica-2-p" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 863, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200863, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 300479, 5);
	end
	if npc_talk_index == "n520_angelica-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n528_acamedic_jasmin--------------------------------------------------------------------------------
function mq11_437_cooperatively_OnTalk_n528_acamedic_jasmin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_437_cooperatively_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 437);
	if qstep == 3 and CountIndex == 863 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300479, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300479, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200863 then
				if api_quest_CheckQuestInvenForAddItem(userObjID, 300479, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 300479, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300479 then

	end
end

function mq11_437_cooperatively_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 437);
	if qstep == 3 and CountIndex == 863 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200863 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300479 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,4);
				api_quest_SetJournalStep(userObjID, questID, 4);

	end
end

function mq11_437_cooperatively_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 437);
	local questID=437;
end

function mq11_437_cooperatively_OnRemoteStart( userObjID, questID )
end

function mq11_437_cooperatively_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_437_cooperatively_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,437, 2);
				api_quest_SetJournalStep(userObjID,437, 1);
				api_quest_SetQuestStep(userObjID,437, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</VillageServer>

<GameServer>
function mq11_437_cooperatively_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 393 then
		mq11_437_cooperatively_OnTalk_n393_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 42 then
		mq11_437_cooperatively_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 520 then
		mq11_437_cooperatively_OnTalk_n520_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 528 then
		mq11_437_cooperatively_OnTalk_n528_acamedic_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n393_academic_station--------------------------------------------------------------------------------
function mq11_437_cooperatively_OnTalk_n393_academic_station( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n393_academic_station-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n393_academic_station-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n393_academic_station-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n393_academic_station-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n393_academic_station-1-b" then 
	end
	if npc_talk_index == "n393_academic_station-1-c" then 
	end
	if npc_talk_index == "n393_academic_station-1-d" then 
	end
	if npc_talk_index == "n393_academic_station-1-e" then 
	end
	if npc_talk_index == "n393_academic_station-1-f" then 
	end
	if npc_talk_index == "n393_academic_station-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n042_general_duglars--------------------------------------------------------------------------------
function mq11_437_cooperatively_OnTalk_n042_general_duglars( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n042_general_duglars-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n042_general_duglars-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n042_general_duglars-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n042_general_duglars-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,437, 2);
				api_quest_SetJournalStep( pRoom, userObjID,437, 1);
				api_quest_SetQuestStep( pRoom, userObjID,437, 1);
				npc_talk_index = "n042_general_duglars-1";

	end
	if npc_talk_index == "n042_general_duglars-4-b" then 
	end
	if npc_talk_index == "n042_general_duglars-4-c" then 
	end
	if npc_talk_index == "n042_general_duglars-4-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4370, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4370, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4370, questID, 1);
			 end 
	end
	if npc_talk_index == "n042_general_duglars-4-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 438, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 438, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 438, 1);

					
				if api_quest_HasQuestItem( pRoom, userObjID, 300479, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 300479, api_quest_HasQuestItem( pRoom, userObjID, 300479, 1));
				end


				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n042_general_duglars-1", "mq11_438_fleet_of_foot.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n520_angelica--------------------------------------------------------------------------------
function mq11_437_cooperatively_OnTalk_n520_angelica( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 npc_talk_index = "n520_angelica-noquest";
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
			 end 
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
		 end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n520_angelica-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n520_angelica-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n520_angelica-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n520_angelica-2-b" then 
	end
	if npc_talk_index == "n520_angelica-2-c" then 
	end
	if npc_talk_index == "n520_angelica-2-d" then 
	end
	if npc_talk_index == "n520_angelica-2-e" then 
	end
	if npc_talk_index == "n520_angelica-2-f" then 
	end
	if npc_talk_index == "n520_angelica-2-g" then 
	end
	if npc_talk_index == "n520_angelica-2-h" then 
	end
	if npc_talk_index == "n520_angelica-2-i" then 
	end
	if npc_talk_index == "n520_angelica-2-j" then 
	end
	if npc_talk_index == "n520_angelica-2-k" then 
	end
	if npc_talk_index == "n520_angelica-2-l" then 
	end
	if npc_talk_index == "n520_angelica-2-m" then 
	end
	if npc_talk_index == "n520_angelica-2-n" then 
	end
	if npc_talk_index == "n520_angelica-2-o" then 
	end
	if npc_talk_index == "n520_angelica-2-p" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 863, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200863, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 300479, 5);
	end
	if npc_talk_index == "n520_angelica-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n528_acamedic_jasmin--------------------------------------------------------------------------------
function mq11_437_cooperatively_OnTalk_n528_acamedic_jasmin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_437_cooperatively_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 437);
	if qstep == 3 and CountIndex == 863 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300479, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300479, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 200863 then
				if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 300479, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 300479, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end

	end
	if qstep == 3 and CountIndex == 300479 then

	end
end

function mq11_437_cooperatively_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 437);
	if qstep == 3 and CountIndex == 863 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 200863 and Count >= TargetCount  then

	end
	if qstep == 3 and CountIndex == 300479 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

	end
end

function mq11_437_cooperatively_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 437);
	local questID=437;
end

function mq11_437_cooperatively_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_437_cooperatively_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_437_cooperatively_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,437, 2);
				api_quest_SetJournalStep( pRoom, userObjID,437, 1);
				api_quest_SetQuestStep( pRoom, userObjID,437, 1);
				npc_talk_index = "n042_general_duglars-1";
end

</GameServer>