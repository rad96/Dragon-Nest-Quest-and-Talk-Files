<VillageServer>

function dq_guild_1288_take_like_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 996 then
		dq_guild_1288_take_like_OnTalk_n996_guild_quest_helper(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n996_guild_quest_helper--------------------------------------------------------------------------------
function dq_guild_1288_take_like_OnTalk_n996_guild_quest_helper(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n996_guild_quest_helper-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n996_guild_quest_helper-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n996_guild_quest_helper-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n996_guild_quest_helper-accepting-b" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 12880, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 12880, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 12880, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 12880, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 12880, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 12880, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 12880, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 12880, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 12880, false);
			 end 

	end
	if npc_talk_index == "n996_guild_quest_helper-accepting-acceptted" then
				api_quest_AddQuest(userObjID,1288, 1);
				api_quest_SetJournalStep(userObjID,1288, 1);
				api_quest_SetQuestStep(userObjID,1288, 1);
				npc_talk_index = "n996_guild_quest_helper-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 8, 8, 5);

	end
	if npc_talk_index == "n996_guild_quest_helper-2-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 12880, true);
				 api_quest_RewardQuestUser(userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 12880, true);
				 api_quest_RewardQuestUser(userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 12880, true);
				 api_quest_RewardQuestUser(userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 12880, true);
				 api_quest_RewardQuestUser(userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 12880, true);
				 api_quest_RewardQuestUser(userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 12880, true);
				 api_quest_RewardQuestUser(userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 12880, true);
				 api_quest_RewardQuestUser(userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 12880, true);
				 api_quest_RewardQuestUser(userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 12880, true);
				 api_quest_RewardQuestUser(userObjID, 12880, questID, 1);
			 end 
	end
	if npc_talk_index == "n996_guild_quest_helper-2-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n996_guild_quest_helper-2-back" then 
				api_npc_NextScript(userObjID, npcObjID, "talk", "n996_guild_quest_helper.xml");
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq_guild_1288_take_like_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1288);
	if qstep == 1 and CountType == 8 then

	end
end

function dq_guild_1288_take_like_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1288);
	if qstep == 1 and CountingType == 8 and Count >= TargetCount  then
				api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);

	end
end

function dq_guild_1288_take_like_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 1288);
	local questID=1288;
end

function dq_guild_1288_take_like_OnRemoteStart( userObjID, questID )
				api_quest_SetCountingInfo(userObjID, questID, 0, 8, 8, 5);
end

function dq_guild_1288_take_like_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function dq_guild_1288_take_like_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,1288, 1);
				api_quest_SetJournalStep(userObjID,1288, 1);
				api_quest_SetQuestStep(userObjID,1288, 1);
				npc_talk_index = "n996_guild_quest_helper-1";
				api_quest_SetCountingInfo(userObjID, questID, 0, 8, 8, 5);
end

</VillageServer>

<GameServer>
function dq_guild_1288_take_like_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 996 then
		dq_guild_1288_take_like_OnTalk_n996_guild_quest_helper( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n996_guild_quest_helper--------------------------------------------------------------------------------
function dq_guild_1288_take_like_OnTalk_n996_guild_quest_helper( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n996_guild_quest_helper-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n996_guild_quest_helper-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n996_guild_quest_helper-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n996_guild_quest_helper-accepting-b" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, false);
			 end 

	end
	if npc_talk_index == "n996_guild_quest_helper-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,1288, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1288, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1288, 1);
				npc_talk_index = "n996_guild_quest_helper-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 8, 8, 5);

	end
	if npc_talk_index == "n996_guild_quest_helper-2-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12880, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 12880, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 12880, questID, 1);
			 end 
	end
	if npc_talk_index == "n996_guild_quest_helper-2-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n996_guild_quest_helper-2-back" then 
				api_npc_NextScript( pRoom, userObjID, npcObjID, "talk", "n996_guild_quest_helper.xml");
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function dq_guild_1288_take_like_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1288);
	if qstep == 1 and CountType == 8 then

	end
end

function dq_guild_1288_take_like_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1288);
	if qstep == 1 and CountingType == 8 and Count >= TargetCount  then
				api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);

	end
end

function dq_guild_1288_take_like_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 1288);
	local questID=1288;
end

function dq_guild_1288_take_like_OnRemoteStart( pRoom,  userObjID, questID )
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 8, 8, 5);
end

function dq_guild_1288_take_like_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function dq_guild_1288_take_like_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,1288, 1);
				api_quest_SetJournalStep( pRoom, userObjID,1288, 1);
				api_quest_SetQuestStep( pRoom, userObjID,1288, 1);
				npc_talk_index = "n996_guild_quest_helper-1";
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 8, 8, 5);
end

</GameServer>