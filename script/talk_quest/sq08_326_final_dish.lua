<VillageServer>

function sq08_326_final_dish_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 26 then
		sq08_326_final_dish_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_326_final_dish_OnTalk_n026_trader_may(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-e" then
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3261, false);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3262, false);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3263, false);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3264, false);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3265, false);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3266, false);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3267, false);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3268, false);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3269, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest(userObjID,326, 1);
				api_quest_SetJournalStep(userObjID,326, 1);
				api_quest_SetQuestStep(userObjID,326, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-1" then 
	end
	if npc_talk_index == "n026_trader_may-1" then 
	end
	if npc_talk_index == "n026_trader_may-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetCountingInfo(userObjID, questID, 0, 2, 483, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 1, 2, 200483, 30000);
				api_quest_SetCountingInfo(userObjID, questID, 2, 3, 400116, 10);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end
	if npc_talk_index == "n026_trader_may-3-a" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 3261, true);
				 api_quest_RewardQuestUser(userObjID, 3261, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 3262, true);
				 api_quest_RewardQuestUser(userObjID, 3262, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 3263, true);
				 api_quest_RewardQuestUser(userObjID, 3263, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 3264, true);
				 api_quest_RewardQuestUser(userObjID, 3264, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 3265, true);
				 api_quest_RewardQuestUser(userObjID, 3265, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 3266, true);
				 api_quest_RewardQuestUser(userObjID, 3266, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 3267, true);
				 api_quest_RewardQuestUser(userObjID, 3267, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 3268, true);
				 api_quest_RewardQuestUser(userObjID, 3268, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 3269, true);
				 api_quest_RewardQuestUser(userObjID, 3269, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-3-b" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem(userObjID, 400116, 1) > 0 then 
					api_quest_DelQuestItem(userObjID, 400116, api_quest_HasQuestItem(userObjID, 400116, 1));
				end
	end
	if npc_talk_index == "n026_trader_may-3-c" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_326_final_dish_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 326);
	if qstep == 2 and CountIndex == 483 then
				if api_quest_HasQuestItem(userObjID, 400116, 10) >= 10 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400116, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400116, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400116, 10) >= 10 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 400116 then

	end
	if qstep == 2 and CountIndex == 200483 then
				if api_quest_HasQuestItem(userObjID, 400116, 10) >= 10 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem(userObjID, 400116, 1) == 1 then 
					api_quest_AddQuestItem(userObjID, 400116, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage(userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem(userObjID, 400116, 10) >= 10 then
									api_quest_ClearCountingInfo(userObjID, questID);
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
end

function sq08_326_final_dish_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 326);
	if qstep == 2 and CountIndex == 483 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400116 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200483 and Count >= TargetCount  then

	end
end

function sq08_326_final_dish_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 326);
	local questID=326;
end

function sq08_326_final_dish_OnRemoteStart( userObjID, questID )
end

function sq08_326_final_dish_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function sq08_326_final_dish_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,326, 1);
				api_quest_SetJournalStep(userObjID,326, 1);
				api_quest_SetQuestStep(userObjID,326, 1);
				npc_talk_index = "n026_trader_may-1";
end

</VillageServer>

<GameServer>
function sq08_326_final_dish_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 26 then
		sq08_326_final_dish_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n026_trader_may--------------------------------------------------------------------------------
function sq08_326_final_dish_OnTalk_n026_trader_may( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n026_trader_may-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n026_trader_may-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n026_trader_may-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n026_trader_may-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n026_trader_may-accepting-e" then
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3261, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3262, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3263, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3264, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3265, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3266, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3267, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3268, false);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3269, false);
			 end 

	end
	if npc_talk_index == "n026_trader_may-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,326, 1);
				api_quest_SetJournalStep( pRoom, userObjID,326, 1);
				api_quest_SetQuestStep( pRoom, userObjID,326, 1);
				npc_talk_index = "n026_trader_may-1";

	end
	if npc_talk_index == "n026_trader_may-1" then 
	end
	if npc_talk_index == "n026_trader_may-1" then 
	end
	if npc_talk_index == "n026_trader_may-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 0, 2, 483, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 1, 2, 200483, 30000);
				api_quest_SetCountingInfo( pRoom, userObjID, questID, 2, 3, 400116, 10);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end
	if npc_talk_index == "n026_trader_may-3-a" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3261, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3261, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3262, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3262, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3263, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3263, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3264, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3264, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3265, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3265, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3266, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3266, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3267, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3267, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3268, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3268, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 3269, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 3269, questID, 1);
			 end 
	end
	if npc_talk_index == "n026_trader_may-3-b" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
				else
					npc_talk_index = "_full_inventory";

				end

				if api_quest_HasQuestItem( pRoom, userObjID, 400116, 1) > 0 then 
					api_quest_DelQuestItem( pRoom, userObjID, 400116, api_quest_HasQuestItem( pRoom, userObjID, 400116, 1));
				end
	end
	if npc_talk_index == "n026_trader_may-3-c" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function sq08_326_final_dish_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 326);
	if qstep == 2 and CountIndex == 483 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400116, 10) >= 10 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400116, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400116, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400116, 10) >= 10 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
	if qstep == 2 and CountIndex == 400116 then

	end
	if qstep == 2 and CountIndex == 200483 then
				if api_quest_HasQuestItem( pRoom, userObjID, 400116, 10) >= 10 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
									if math.random(1,1000) <= 1000 then
									if api_quest_CheckQuestInvenForAddItem( pRoom, userObjID, 400116, 1) == 1 then 
					api_quest_AddQuestItem( pRoom, userObjID, 400116, 1, questID);
				else
					local Ts = {};
					api_user_UserMessage( pRoom, userObjID,6,1200101, Ts);
					return;
				end
				if api_quest_HasQuestItem( pRoom, userObjID, 400116, 10) >= 10 then
									api_quest_ClearCountingInfo( pRoom, userObjID, questID);
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 3);

				else
				end

				else
				end

				end

	end
end

function sq08_326_final_dish_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 326);
	if qstep == 2 and CountIndex == 483 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 400116 and Count >= TargetCount  then

	end
	if qstep == 2 and CountIndex == 200483 and Count >= TargetCount  then

	end
end

function sq08_326_final_dish_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 326);
	local questID=326;
end

function sq08_326_final_dish_OnRemoteStart( pRoom,  userObjID, questID )
end

function sq08_326_final_dish_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function sq08_326_final_dish_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,326, 1);
				api_quest_SetJournalStep( pRoom, userObjID,326, 1);
				api_quest_SetQuestStep( pRoom, userObjID,326, 1);
				npc_talk_index = "n026_trader_may-1";
end

</GameServer>