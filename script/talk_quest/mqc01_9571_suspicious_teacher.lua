<VillageServer>

function mqc01_9571_suspicious_teacher_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 1842 then
		mqc01_9571_suspicious_teacher_OnTalk_n1842_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1841 then
		mqc01_9571_suspicious_teacher_OnTalk_n1841_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2 then
		mqc01_9571_suspicious_teacher_OnTalk_n002_trader_dorin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1842_shaolong--------------------------------------------------------------------------------
function mqc01_9571_suspicious_teacher_OnTalk_n1842_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1842_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1841_shaolong--------------------------------------------------------------------------------
function mqc01_9571_suspicious_teacher_OnTalk_n1841_shaolong(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n1841_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1841_shaolong-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1841_shaolong-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1841_shaolong-accepting-acceptted" then
				api_quest_AddQuest(userObjID,9571, 2);
				api_quest_SetJournalStep(userObjID,9571, 1);
				api_quest_SetQuestStep(userObjID,9571, 1);
				npc_talk_index = "n1841_shaolong-1";

	end
	if npc_talk_index == "n1841_shaolong-1-b" then 
	end
	if npc_talk_index == "n1841_shaolong-1-c" then 
	end
	if npc_talk_index == "n1841_shaolong-1-d" then 
	end
	if npc_talk_index == "n1841_shaolong-1-e" then 
	end
	if npc_talk_index == "n1841_shaolong-1-f" then 
	end
	if npc_talk_index == "n1841_shaolong-1-g" then 
	end
	if npc_talk_index == "n1841_shaolong-1-h" then 
	end
	if npc_talk_index == "n1841_shaolong-1-i" then 
	end
	if npc_talk_index == "n1841_shaolong-1-j" then 
	end
	if npc_talk_index == "n1841_shaolong-1-k" then 
	end
	if npc_talk_index == "n1841_shaolong-1-l" then 
	end
	if npc_talk_index == "n1841_shaolong-1-m" then 
	end
	if npc_talk_index == "n1841_shaolong-1-n" then 
	end
	if npc_talk_index == "n1841_shaolong-1-o" then 
	end
	if npc_talk_index == "n1841_shaolong-1-p" then 
	end
	if npc_talk_index == "n1841_shaolong-1-q" then 
	end
	if npc_talk_index == "n1841_shaolong-1-r" then 
	end
	if npc_talk_index == "n1841_shaolong-1-s" then 
	end
	if npc_talk_index == "n1841_shaolong-2" then 
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n002_trader_dorin--------------------------------------------------------------------------------
function mqc01_9571_suspicious_teacher_OnTalk_n002_trader_dorin(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n002_trader_dorin-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n002_trader_dorin-2";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n002_trader_dorin-2-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-c" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-d" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 95710, true);
				 api_quest_RewardQuestUser(userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 95710, true);
				 api_quest_RewardQuestUser(userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 95710, true);
				 api_quest_RewardQuestUser(userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 95710, true);
				 api_quest_RewardQuestUser(userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 95710, true);
				 api_quest_RewardQuestUser(userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 95710, true);
				 api_quest_RewardQuestUser(userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 95710, true);
				 api_quest_RewardQuestUser(userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 95710, true);
				 api_quest_RewardQuestUser(userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 9 then 
				 api_ui_OpenQuestReward(userObjID, 95710, true);
				 api_quest_RewardQuestUser(userObjID, 95710, questID, 1);
			 end 
	end
	if npc_talk_index == "n002_trader_dorin-2-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 9572, 2);
					api_quest_SetQuestStep(userObjID, 9572, 1);
					api_quest_SetJournalStep(userObjID, 9572, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n002_trader_dorin-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n002_trader_dorin-1", "mqc01_9572_whereabouts_airship.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9571_suspicious_teacher_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9571);
end

function mqc01_9571_suspicious_teacher_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9571);
end

function mqc01_9571_suspicious_teacher_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 9571);
	local questID=9571;
end

function mqc01_9571_suspicious_teacher_OnRemoteStart( userObjID, questID )
end

function mqc01_9571_suspicious_teacher_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mqc01_9571_suspicious_teacher_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,9571, 2);
				api_quest_SetJournalStep(userObjID,9571, 1);
				api_quest_SetQuestStep(userObjID,9571, 1);
				npc_talk_index = "n1841_shaolong-1";
end

</VillageServer>

<GameServer>
function mqc01_9571_suspicious_teacher_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 1842 then
		mqc01_9571_suspicious_teacher_OnTalk_n1842_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 1841 then
		mqc01_9571_suspicious_teacher_OnTalk_n1841_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 2 then
		mqc01_9571_suspicious_teacher_OnTalk_n002_trader_dorin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n1842_shaolong--------------------------------------------------------------------------------
function mqc01_9571_suspicious_teacher_OnTalk_n1842_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1842_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n1841_shaolong--------------------------------------------------------------------------------
function mqc01_9571_suspicious_teacher_OnTalk_n1841_shaolong( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n1841_shaolong-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n1841_shaolong-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n1841_shaolong-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n1841_shaolong-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,9571, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9571, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9571, 1);
				npc_talk_index = "n1841_shaolong-1";

	end
	if npc_talk_index == "n1841_shaolong-1-b" then 
	end
	if npc_talk_index == "n1841_shaolong-1-c" then 
	end
	if npc_talk_index == "n1841_shaolong-1-d" then 
	end
	if npc_talk_index == "n1841_shaolong-1-e" then 
	end
	if npc_talk_index == "n1841_shaolong-1-f" then 
	end
	if npc_talk_index == "n1841_shaolong-1-g" then 
	end
	if npc_talk_index == "n1841_shaolong-1-h" then 
	end
	if npc_talk_index == "n1841_shaolong-1-i" then 
	end
	if npc_talk_index == "n1841_shaolong-1-j" then 
	end
	if npc_talk_index == "n1841_shaolong-1-k" then 
	end
	if npc_talk_index == "n1841_shaolong-1-l" then 
	end
	if npc_talk_index == "n1841_shaolong-1-m" then 
	end
	if npc_talk_index == "n1841_shaolong-1-n" then 
	end
	if npc_talk_index == "n1841_shaolong-1-o" then 
	end
	if npc_talk_index == "n1841_shaolong-1-p" then 
	end
	if npc_talk_index == "n1841_shaolong-1-q" then 
	end
	if npc_talk_index == "n1841_shaolong-1-r" then 
	end
	if npc_talk_index == "n1841_shaolong-1-s" then 
	end
	if npc_talk_index == "n1841_shaolong-2" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n002_trader_dorin--------------------------------------------------------------------------------
function mqc01_9571_suspicious_teacher_OnTalk_n002_trader_dorin( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n002_trader_dorin-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n002_trader_dorin-2";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n002_trader_dorin-2-b" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-c" then 
	end
	if npc_talk_index == "n002_trader_dorin-2-d" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95710, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 9 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 95710, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 95710, questID, 1);
			 end 
	end
	if npc_talk_index == "n002_trader_dorin-2-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 9572, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 9572, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 9572, 1);

				else
					npc_talk_index = "_full_inventory";

				end
	end
	if npc_talk_index == "n002_trader_dorin-2-!next" then 
		local cqresult = 1

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n002_trader_dorin-1", "mqc01_9572_whereabouts_airship.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mqc01_9571_suspicious_teacher_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9571);
end

function mqc01_9571_suspicious_teacher_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9571);
end

function mqc01_9571_suspicious_teacher_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 9571);
	local questID=9571;
end

function mqc01_9571_suspicious_teacher_OnRemoteStart( pRoom,  userObjID, questID )
end

function mqc01_9571_suspicious_teacher_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mqc01_9571_suspicious_teacher_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,9571, 2);
				api_quest_SetJournalStep( pRoom, userObjID,9571, 1);
				api_quest_SetQuestStep( pRoom, userObjID,9571, 1);
				npc_talk_index = "n1841_shaolong-1";
end

</GameServer>