<VillageServer>

function nq39_5601_gigantic_destiny_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 712 then
		nq39_5601_gigantic_destiny_OnTalk_n712_archer_zenya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n712_archer_zenya--------------------------------------------------------------------------------
function nq39_5601_gigantic_destiny_OnTalk_n712_archer_zenya(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n712_archer_zenya-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n712_archer_zenya-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n712_archer_zenya-accepting-acceptted" then
				api_quest_AddQuest(userObjID,5601, 1);
				api_quest_SetJournalStep(userObjID,5601, 1);
				api_quest_SetQuestStep(userObjID,5601, 1);
				npc_talk_index = "n712_archer_zenya-1";

	end
	if npc_talk_index == "n712_archer_zenya-1-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-d" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-e" then 

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( userObjID, 712, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq39_5601_gigantic_destiny_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5601);
end

function nq39_5601_gigantic_destiny_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5601);
end

function nq39_5601_gigantic_destiny_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 5601);
	local questID=5601;
end

function nq39_5601_gigantic_destiny_OnRemoteStart( userObjID, questID )
end

function nq39_5601_gigantic_destiny_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function nq39_5601_gigantic_destiny_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,5601, 1);
				api_quest_SetJournalStep(userObjID,5601, 1);
				api_quest_SetQuestStep(userObjID,5601, 1);
				npc_talk_index = "n712_archer_zenya-1";
end

</VillageServer>

<GameServer>
function nq39_5601_gigantic_destiny_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 712 then
		nq39_5601_gigantic_destiny_OnTalk_n712_archer_zenya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n712_archer_zenya--------------------------------------------------------------------------------
function nq39_5601_gigantic_destiny_OnTalk_n712_archer_zenya( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n712_archer_zenya-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n712_archer_zenya-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n712_archer_zenya-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,5601, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5601, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5601, 1);
				npc_talk_index = "n712_archer_zenya-1";

	end
	if npc_talk_index == "n712_archer_zenya-1-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-c" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-d" then 
	end
	if npc_talk_index == "n712_archer_zenya-1-e" then 

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
									api_npc_AddFavorPoint( pRoom,  userObjID, 712, 300 );


				else
					npc_talk_index = "_full_inventory";

				end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function nq39_5601_gigantic_destiny_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5601);
end

function nq39_5601_gigantic_destiny_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5601);
end

function nq39_5601_gigantic_destiny_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 5601);
	local questID=5601;
end

function nq39_5601_gigantic_destiny_OnRemoteStart( pRoom,  userObjID, questID )
end

function nq39_5601_gigantic_destiny_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function nq39_5601_gigantic_destiny_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,5601, 1);
				api_quest_SetJournalStep( pRoom, userObjID,5601, 1);
				api_quest_SetQuestStep( pRoom, userObjID,5601, 1);
				npc_talk_index = "n712_archer_zenya-1";
end

</GameServer>