<VillageServer>

function mq11_401_castle_sainthaven_OnTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex(userObjID, npcObjID);

	if npcID == 209 then
		mq11_401_castle_sainthaven_OnTalk_n209_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 252 then
		mq11_401_castle_sainthaven_OnTalk_n252_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 258 then
		mq11_401_castle_sainthaven_OnTalk_n258_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 259 then
		mq11_401_castle_sainthaven_OnTalk_n259_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 260 then
		mq11_401_castle_sainthaven_OnTalk_n260_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		mq11_401_castle_sainthaven_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mq11_401_castle_sainthaven_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mq11_401_castle_sainthaven_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mq11_401_castle_sainthaven_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		mq11_401_castle_sainthaven_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 55 then
		mq11_401_castle_sainthaven_OnTalk_n055_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 69 then
		mq11_401_castle_sainthaven_OnTalk_n069_plane_ticketer_arnene(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 70 then
		mq11_401_castle_sainthaven_OnTalk_n070_plane_ticketer_sorene(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 99 then
		mq11_401_castle_sainthaven_OnTalk_n099_engineer_hubert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n209_elite_soldier--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n209_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n209_elite_soldier-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n209_elite_soldier-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n209_elite_soldier-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n209_elite_soldier-3-b" then 
	end
	if npc_talk_index == "n209_elite_soldier-3-cutscene" then 
				api_user_PlayCutScene(userObjID,npcObjID,119);
				api_quest_SetQuestStep(userObjID, questID,4);
	end
	if npc_talk_index == "n209_elite_soldier-4-b" then 
				api_quest_SetQuestStep(userObjID, questID,5);
				api_quest_SetJournalStep(userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n252_saint_guard--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n252_saint_guard(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n252_saint_guard-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n252_saint_guard-2";
				if api_quest_GetQuestMemo(userObjID, questID, 3) == 3  then
									npc_talk_index = "n252_saint_guard-2";

				else
									npc_talk_index = "n252_saint_guard-2-e";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n252_saint_guard-3";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n252_saint_guard-2-b" then 
	end
	if npc_talk_index == "n252_saint_guard-2-c" then 
	end
	if npc_talk_index == "n252_saint_guard-2-d" then 
				api_quest_SetQuestStep(userObjID, questID,3);
				api_quest_SetJournalStep(userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n258_slave_david--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n258_slave_david(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n258_slave_david-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n259_argenta--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n259_argenta(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n259_argenta-1";
				if api_quest_GetQuestMemo(userObjID, questID, 2) == 1  then
									npc_talk_index = "n259_argenta-1-g";
				api_quest_SetJournalStep(userObjID, questID, 4);

				else
									npc_talk_index = "n259_argenta-1";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n259_argenta-1-b" then 
	end
	if npc_talk_index == "n259_argenta-1-c" then 
	end
	if npc_talk_index == "n259_argenta-1-d" then 
	end
	if npc_talk_index == "n259_argenta-1-e" then 
	end
	if npc_talk_index == "n259_argenta-1-f" then 
				api_quest_SetQuestMemo(userObjID, questID, 2, 1);
				api_quest_SetQuestMemo(userObjID, questID, 3, 1);
				api_quest_SetJournalStep(userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n260_elite_soldier--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n260_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n260_elite_soldier-1";
				if api_quest_GetQuestMemo(userObjID, questID, 3) == 1  then
									npc_talk_index = "n260_elite_soldier-1-a";

				else
									if api_quest_GetQuestMemo(userObjID, questID, 3) == 2  then
									npc_talk_index = "n260_elite_soldier-1-b";

				else
									npc_talk_index = "n260_elite_soldier-1";

				end

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n260_elite_soldier-1-b" then 
	end
	if npc_talk_index == "n260_elite_soldier-1-c" then 
	end
	if npc_talk_index == "n260_elite_soldier-1-go_saint" then 
				api_quest_SetQuestMemo(userObjID, questID, 3, 2);
				api_quest_SetQuestStep(userObjID, questID,2);
				api_quest_SetJournalStep(userObjID, questID, 5);
				api_user_ChangeMap(userObjID,11,1);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n035_soceress_master_tiana(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n038_royal_magician_kalaen(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n038_royal_magician_kalaen-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n039_bishop_ignasio(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n039_bishop_ignasio-5";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-5-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-g" then 
			 if api_user_GetUserClassID(userObjID) == 1 then 
				 api_ui_OpenQuestReward(userObjID, 4011, true);
				 api_quest_RewardQuestUser(userObjID, 4011, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 2 then 
				 api_ui_OpenQuestReward(userObjID, 4012, true);
				 api_quest_RewardQuestUser(userObjID, 4012, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 3 then 
				 api_ui_OpenQuestReward(userObjID, 4013, true);
				 api_quest_RewardQuestUser(userObjID, 4013, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 4 then 
				 api_ui_OpenQuestReward(userObjID, 4014, true);
				 api_quest_RewardQuestUser(userObjID, 4014, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 5 then 
				 api_ui_OpenQuestReward(userObjID, 4014, true);
				 api_quest_RewardQuestUser(userObjID, 4014, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 6 then 
				 api_ui_OpenQuestReward(userObjID, 4016, true);
				 api_quest_RewardQuestUser(userObjID, 4016, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 7 then 
				 api_ui_OpenQuestReward(userObjID, 4011, true);
				 api_quest_RewardQuestUser(userObjID, 4011, questID, 1);
			 elseif api_user_GetUserClassID(userObjID) == 8 then 
				 api_ui_OpenQuestReward(userObjID, 4011, true);
				 api_quest_RewardQuestUser(userObjID, 4011, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest(userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest(userObjID, 402, 2);
					api_quest_SetQuestStep(userObjID, 402, 1);
					api_quest_SetJournalStep(userObjID, 402, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript(userObjID, npcObjID, "n039_bishop_ignasio-1", "mq11_402_town_of_death.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n040_king_casius(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n041_duke_stwart(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n041_duke_stwart-4";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n055_elite_soldier--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n055_elite_soldier(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n055_elite_soldier-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n069_plane_ticketer_arnene--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n069_plane_ticketer_arnene(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n069_plane_ticketer_arnene-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n070_plane_ticketer_sorene--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n070_plane_ticketer_sorene(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n070_plane_ticketer_sorene-1";
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n070_plane_ticketer_sorene-2";
				if api_quest_GetQuestMemo(userObjID, questID, 3) == 2  then
									npc_talk_index = "n070_plane_ticketer_sorene-2";

				else
									npc_talk_index = "n070_plane_ticketer_sorene-2-d";

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n070_plane_ticketer_sorene-2-b" then 
	end
	if npc_talk_index == "n070_plane_ticketer_sorene-2-c" then 
				api_quest_SetJournalStep(userObjID, questID, 6);
				api_quest_SetQuestMemo(userObjID, questID, 3, 3);
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n099_engineer_hubert--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n099_engineer_hubert(userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep(userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest(userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum(userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk(userObjID, npcObjID, "n099_engineer_hubert-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest(userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n099_engineer_hubert-1";
				if api_quest_GetQuestMemo(userObjID, questID, 1) >= 2  then
									npc_talk_index = "n099_engineer_hubert-1-e";

				else
									if api_user_GetUserLevel(userObjID) >= 24 then
									npc_talk_index = "n099_engineer_hubert-1-e";
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestMemo(userObjID, questID, 1, 2);

				else
									npc_talk_index = "n099_engineer_hubert-1-f";

				end

				end
				api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n099_engineer_hubert-accepting-acceptted" then
				api_quest_AddQuest(userObjID,401, 2);
				api_quest_SetJournalStep(userObjID,401, 1);
				api_quest_SetQuestStep(userObjID,401, 1);
				npc_talk_index = "n099_engineer_hubert-1";

	end
	if npc_talk_index == "n099_engineer_hubert-1-level_check" then 
				if api_user_GetUserLevel(userObjID) >= 24 then
									npc_talk_index = "n099_engineer_hubert-1-e";
				api_quest_SetJournalStep(userObjID, questID, 2);
				api_quest_SetQuestMemo(userObjID, questID, 1, 2);

				else
									npc_talk_index = "n099_engineer_hubert-1-a";

				end
	end
	if npc_talk_index == "n099_engineer_hubert-1-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-1-c" then 
	end
	if npc_talk_index == "n099_engineer_hubert-1-d" then 
	end

	---------------------------------
	 api_npc_NextTalk(userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_401_castle_sainthaven_OnCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 401);
end

function mq11_401_castle_sainthaven_OnCompleteCounting( userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 401);
end

function mq11_401_castle_sainthaven_OnCompleteAllCounting( userObjID, questID )
	local qstep=api_quest_GetQuestStep(userObjID, 401);
	local questID=401;
end

function mq11_401_castle_sainthaven_OnRemoteStart( userObjID, questID )
end

function mq11_401_castle_sainthaven_CanRemoteCompleteStep( userObjID, questID, questStep )
	return false;
end

function mq11_401_castle_sainthaven_ForceAccept( userObjID, npcObjID, questID )
				api_quest_AddQuest(userObjID,401, 2);
				api_quest_SetJournalStep(userObjID,401, 1);
				api_quest_SetQuestStep(userObjID,401, 1);
				npc_talk_index = "n099_engineer_hubert-1";
end

</VillageServer>

<GameServer>
function mq11_401_castle_sainthaven_OnTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
-----------------------------------------------------------------------------------------------

	local npcID = api_npc_GetNpcIndex( pRoom, userObjID, npcObjID);

	if npcID == 209 then
		mq11_401_castle_sainthaven_OnTalk_n209_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 252 then
		mq11_401_castle_sainthaven_OnTalk_n252_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 258 then
		mq11_401_castle_sainthaven_OnTalk_n258_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 259 then
		mq11_401_castle_sainthaven_OnTalk_n259_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 260 then
		mq11_401_castle_sainthaven_OnTalk_n260_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 35 then
		mq11_401_castle_sainthaven_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 38 then
		mq11_401_castle_sainthaven_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 39 then
		mq11_401_castle_sainthaven_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 40 then
		mq11_401_castle_sainthaven_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 41 then
		mq11_401_castle_sainthaven_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 55 then
		mq11_401_castle_sainthaven_OnTalk_n055_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 69 then
		mq11_401_castle_sainthaven_OnTalk_n069_plane_ticketer_arnene( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 70 then
		mq11_401_castle_sainthaven_OnTalk_n070_plane_ticketer_sorene( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

	if npcID == 99 then
		mq11_401_castle_sainthaven_OnTalk_n099_engineer_hubert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID);
		return;
	end

end
------------------------------------------------------------------------------------------------

--n209_elite_soldier--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n209_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n209_elite_soldier-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n209_elite_soldier-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n209_elite_soldier-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n209_elite_soldier-3-b" then 
	end
	if npc_talk_index == "n209_elite_soldier-3-cutscene" then 
				api_user_PlayCutScene( pRoom, userObjID,npcObjID,119);
				api_quest_SetQuestStep( pRoom, userObjID, questID,4);
	end
	if npc_talk_index == "n209_elite_soldier-4-b" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,5);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 8);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n252_saint_guard--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n252_saint_guard( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n252_saint_guard-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n252_saint_guard-2";
				if api_quest_GetQuestMemo( pRoom, userObjID, questID, 3) == 3  then
									npc_talk_index = "n252_saint_guard-2";

				else
									npc_talk_index = "n252_saint_guard-2-e";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 3 then
				npc_talk_index = "n252_saint_guard-3";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n252_saint_guard-2-b" then 
	end
	if npc_talk_index == "n252_saint_guard-2-c" then 
	end
	if npc_talk_index == "n252_saint_guard-2-d" then 
				api_quest_SetQuestStep( pRoom, userObjID, questID,3);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 7);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n258_slave_david--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n258_slave_david( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n258_slave_david-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n259_argenta--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n259_argenta( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n259_argenta-1";
				if api_quest_GetQuestMemo( pRoom, userObjID, questID, 2) == 1  then
									npc_talk_index = "n259_argenta-1-g";
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);

				else
									npc_talk_index = "n259_argenta-1";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n259_argenta-1-b" then 
	end
	if npc_talk_index == "n259_argenta-1-c" then 
	end
	if npc_talk_index == "n259_argenta-1-d" then 
	end
	if npc_talk_index == "n259_argenta-1-e" then 
	end
	if npc_talk_index == "n259_argenta-1-f" then 
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 2, 1);
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 3, 1);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 4);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n260_elite_soldier--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n260_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n260_elite_soldier-1";
				if api_quest_GetQuestMemo( pRoom, userObjID, questID, 3) == 1  then
									npc_talk_index = "n260_elite_soldier-1-a";

				else
									if api_quest_GetQuestMemo( pRoom, userObjID, questID, 3) == 2  then
									npc_talk_index = "n260_elite_soldier-1-b";

				else
									npc_talk_index = "n260_elite_soldier-1";

				end

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n260_elite_soldier-1-b" then 
	end
	if npc_talk_index == "n260_elite_soldier-1-c" then 
	end
	if npc_talk_index == "n260_elite_soldier-1-go_saint" then 
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 3, 2);
				api_quest_SetQuestStep( pRoom, userObjID, questID,2);
				api_quest_SetJournalStep( pRoom, userObjID, questID, 5);
				api_user_ChangeMap( pRoom, userObjID,11,1);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n035_soceress_master_tiana--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n035_soceress_master_tiana( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n035_soceress_master_tiana-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n038_royal_magician_kalaen--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n038_royal_magician_kalaen( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n038_royal_magician_kalaen-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n038_royal_magician_kalaen-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n038_royal_magician_kalaen-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n039_bishop_ignasio--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n039_bishop_ignasio( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n039_bishop_ignasio-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 5 then
				npc_talk_index = "n039_bishop_ignasio-5";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n039_bishop_ignasio-5-b" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-c" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-d" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-e" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-f" then 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-g" then 
			 if api_user_GetUserClassID( pRoom, userObjID) == 1 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4011, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4011, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 2 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4012, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4012, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 3 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4013, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4013, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 4 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4014, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4014, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 5 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4014, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4014, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 6 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4016, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4016, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 7 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4011, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4011, questID, 1);
			 elseif api_user_GetUserClassID( pRoom, userObjID) == 8 then 
				 api_ui_OpenQuestReward( pRoom, userObjID, 4011, true);
				 api_quest_RewardQuestUser( pRoom, userObjID, 4011, questID, 1);
			 end 
	end
	if npc_talk_index == "n039_bishop_ignasio-5-!next" then 
		local cqresult = 1

				cqresult = api_quest_CompleteQuest( pRoom, userObjID, questID, true, false);
				if cqresult == 1  then
					api_quest_AddQuest( pRoom, userObjID, 402, 2);
					api_quest_SetQuestStep( pRoom, userObjID, 402, 1);
					api_quest_SetJournalStep( pRoom, userObjID, 402, 1);

				else
					npc_talk_index = "_full_inventory";

				end

		if cqresult == 1 then
			api_npc_NextScript( pRoom, userObjID, npcObjID, "n039_bishop_ignasio-1", "mq11_402_town_of_death.xml");
			return;
		end
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n040_king_casius--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n040_king_casius( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n040_king_casius-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n041_duke_stwart--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n041_duke_stwart( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n041_duke_stwart-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 4 then
				npc_talk_index = "n041_duke_stwart-4";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n041_duke_stwart-4-b" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n055_elite_soldier--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n055_elite_soldier( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n055_elite_soldier-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n069_plane_ticketer_arnene--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n069_plane_ticketer_arnene( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n069_plane_ticketer_arnene-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else


	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n070_plane_ticketer_sorene--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n070_plane_ticketer_sorene( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n070_plane_ticketer_sorene-1";
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 2 then
				npc_talk_index = "n070_plane_ticketer_sorene-2";
				if api_quest_GetQuestMemo( pRoom, userObjID, questID, 3) == 2  then
									npc_talk_index = "n070_plane_ticketer_sorene-2";

				else
									npc_talk_index = "n070_plane_ticketer_sorene-2-d";

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n070_plane_ticketer_sorene-2-b" then 
	end
	if npc_talk_index == "n070_plane_ticketer_sorene-2-c" then 
				api_quest_SetJournalStep( pRoom, userObjID, questID, 6);
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 3, 3);
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end
--n099_engineer_hubert--------------------------------------------------------------------------------
function mq11_401_castle_sainthaven_OnTalk_n099_engineer_hubert( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target, questID)
------------------------------------------------------------------------------------------

	if npc_talk_index == "!quit" then
		return;
	end
	local queststep = api_quest_GetQuestStep( pRoom, userObjID, questID);

	if npc_talk_index == "q_enter" then

		 if api_quest_UserHasQuest( pRoom, userObjID, questID) <= 0 then
			 if  api_quest_IsPlayingQuestMaximum( pRoom, userObjID) == 1 then 
				 npc_talk_index = "_full_quest";
				 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				 return; 
			 end 
			api_npc_NextTalk( pRoom, userObjID, npcObjID, "n099_engineer_hubert-accepting", npc_talk_target);
		end
		while queststep > -1 do 
		---------------------------------

			if api_quest_UserHasQuest( pRoom, userObjID, questID) > 0 and queststep == 1 then
				npc_talk_index = "n099_engineer_hubert-1";
				if api_quest_GetQuestMemo( pRoom, userObjID, questID, 1) >= 2  then
									npc_talk_index = "n099_engineer_hubert-1-e";

				else
									if api_user_GetUserLevel( pRoom, userObjID) >= 24 then
									npc_talk_index = "n099_engineer_hubert-1-e";
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 2);

				else
									npc_talk_index = "n099_engineer_hubert-1-f";

				end

				end
				api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
				break;
			end

			queststep = queststep -1;
		end


	---------------------------------
	else

	if npc_talk_index == "n099_engineer_hubert-accepting-acceptted" then
				api_quest_AddQuest( pRoom, userObjID,401, 2);
				api_quest_SetJournalStep( pRoom, userObjID,401, 1);
				api_quest_SetQuestStep( pRoom, userObjID,401, 1);
				npc_talk_index = "n099_engineer_hubert-1";

	end
	if npc_talk_index == "n099_engineer_hubert-1-level_check" then 
				if api_user_GetUserLevel( pRoom, userObjID) >= 24 then
									npc_talk_index = "n099_engineer_hubert-1-e";
				api_quest_SetJournalStep( pRoom, userObjID, questID, 2);
				api_quest_SetQuestMemo( pRoom, userObjID, questID, 1, 2);

				else
									npc_talk_index = "n099_engineer_hubert-1-a";

				end
	end
	if npc_talk_index == "n099_engineer_hubert-1-b" then 
	end
	if npc_talk_index == "n099_engineer_hubert-1-c" then 
	end
	if npc_talk_index == "n099_engineer_hubert-1-d" then 
	end

	---------------------------------
	 api_npc_NextTalk( pRoom, userObjID, npcObjID, npc_talk_index, npc_talk_target);
	 end
------------------------------------------------------------------------------------------------
end

function mq11_401_castle_sainthaven_OnCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 401);
end

function mq11_401_castle_sainthaven_OnCompleteCounting( pRoom,  userObjID, CountingType, CountIndex, Count, TargetCount, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 401);
end

function mq11_401_castle_sainthaven_OnCompleteAllCounting( pRoom,  userObjID, questID )
	local qstep=api_quest_GetQuestStep( pRoom, userObjID, 401);
	local questID=401;
end

function mq11_401_castle_sainthaven_OnRemoteStart( pRoom,  userObjID, questID )
end

function mq11_401_castle_sainthaven_CanRemoteCompleteStep( pRoom,  userObjID, questID, questStep )
	return false;
end

function mq11_401_castle_sainthaven_ForceAccept( pRoom,  userObjID, npcObjID, questID )
				api_quest_AddQuest( pRoom, userObjID,401, 2);
				api_quest_SetJournalStep( pRoom, userObjID,401, 1);
				api_quest_SetQuestStep( pRoom, userObjID,401, 1);
				npc_talk_index = "n099_engineer_hubert-1";
end

</GameServer>